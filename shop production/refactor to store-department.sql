/*
ry1     mainshop
ry1     pdq
ry1     bodyshop
ry1     detail
ry2     mainshop    
ry2     pdq

ADD store, department AS input parameters

--****** shit needed to make this WORK ***********
store/department specific target parameters  bs 226 hrs/workday
--****** shit needed to make this WORK **********
*/
--< body shop production ------------------------------------------------------<
-- pubsub for estimator summary has to be changed
UPDATE applicationMetaData
SET pubsub = 'btp.manager.estimatorsummary'
WHERE appseq = 101
  AND approle = 'manager'
  AND configMapMarkupKey = 'sublink_bodyshop_est_mgr_summary_html';
  
INSERT INTO applicationMetaData values (
  'teampay', -- appName
  'tp', -- appCode,
  101,-- appSeq
  'manager', -- appRole
  'production summary', -- functionality
  'sublink_bodyshop_production_summary_html',-- configMapMarkupKey
  'btp.manager.productionsummary',-- pubSub
  'leftnavbar',-- navType
  'production summary',-- navText
  100-- navSeq
  ); 
  
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, 13
FROM tpEmployees a, applicationMetaData b
WHERE (
  a.username = 'rsattler@rydellchev.com' OR
  a.username = 'mhuot@rydellchev.com' or
  firstname = 'trent' OR
  firstname = 'jeri' OR
  (firstname = 'benjamin' AND lastname <> 'dalen') OR
  username = 'brian@rydellchev.com' OR
  firstname = 'andrew' or
  lastname = 'steinke' OR
  lastname = 'espelund' OR
  lastname = 'sorum')
  AND b.appcode = 'tp'
  AND b.approle = 'manager' 
  AND b.functionality = 'production summary'
  AND b.navSeq = 100
  AND b.appseq = 101;   
--/> body shop production -----------------------------------------------------/>


ALTER PROCEDURE benGetProductionAtAGlanceMonths( 
      store cichar(3),
      department cichar(12),
      yearMonth Integer OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      target Integer OUTPUT,
      label DATE OUTPUT)
BEGIN      
/*
EXECUTE PROCEDURE benGetProductionAtAGlanceMonths('ry1','bodyshop')
*/
DECLARE @store string;
DECLARE @department string;
@store = (SELECT lower(store) FROM __input);
@department = (
  SELECT 
    CASE (SELECT lower(department) FROM __input)
      WHEN 'mainshop' THEN 'mr'
      WHEN 'bodyshop' THEN 'bs'
     END
   FROM system.iota);   
INSERT INTO __output
SELECT top 12 b.*
FROM (
  SELECT top 12 a.*, 
    (SELECT 
      CASE
        WHEN @store = 'ry1' and @department = 'mr' then COUNT(*) * 200
        WHEN @store = 'ry1' and @department = 'bs' then COUNT(*) * 226
      END 
      FROM dds.day
      WHERE dayofweek BETWEEN 2 AND 6
        AND holiday = false 
        AND yearmonth = a.yearmonth) AS target, 
    (SELECT thedate
	  FROM dds.day
	  WHERE yearMonth = a.yearMonth
	    AND firstDayOfMonth = true)   
  FROM (      
    SELECT yearmonth, round(coalesce(SUM(flaghours), 0),2 ) AS flagHours,
      round(coalesce(SUM(clockHours), 0), 2) AS clockHours
    FROM tmpBen
    WHERE yearmonth BETWEEN (
      SELECT DISTINCT yearmonth
      FROM tmpBen
      WHERE thedate = curdate() - 365) AND (
      SELECT DISTINCT yearmonth
      FROM tmpBen
      WHERE thedate = curdate() - 1)
    AND storecode = @store
    AND flagdeptCode = @department
    GROUP BY yearmonth) a
  ORDER BY yearMonth DESC) b
ORDER BY yearmonth; 
END;


alter PROCEDURE benGetProductionAtAGlancePace( 
      store cichar(3),
      department cichar(12),
      flagHoursToDate DOUBLE ( 15 ) OUTPUT,
      clockHoursToDate DOUBLE ( 15 ) OUTPUT,
      workingDays Integer OUTPUT,
      elapsedWorkingDays Integer OUTPUT,
      flagPace DOUBLE ( 15 ) OUTPUT,
      clockPace DOUBLE ( 15 ) OUTPUT,
      proficiencyToDate DOUBLE ( 15 ) OUTPUT,
      proficiencyPace DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE benGetProductionAtAGlancePace('ry1','bodyshop');
*/   
-- pacing
DECLARE @workingDaysInMonth integer;
DECLARE @elapsedWorkingDaysInMonth integer;
DECLARE @store string;
DECLARE @department string;
@store = (SELECT lower(store) FROM __input);
@department = (
  SELECT 
    CASE (SELECT lower(department) FROM __input)
      WHEN 'mainshop' THEN 'mr'
      WHEN 'bodyshop' THEN 'bs'
     END
   FROM system.iota); 
@workingDaysInMonth = (
  SELECT COUNT(*) -- working days IN month
  FROM dds.day
  WHERE dayOfWeek BETWEEN 2 AND 6 
    AND holiday = false
    AND yearmonth = (
      SELECT yearmonth
      FROM dds.day
      WHERE thedate = curdate() - 1)); 
      
@elapsedWorkingDaysInMonth = (    
  SELECT COUNT(*) 
  FROM dds.day
  WHERE dayOfWeek BETWEEN 2 AND 6 
    AND holiday = false
    AND thedate < curdate() - 1
    AND yearmonth = (
      SELECT yearmonth
      FROM dds.day
      WHERE thedate = curdate() - 1));   
INSERT INTO __output    
SELECT x.*, round(flaghoursToDate/clockHoursToDate, 2),
  round(flagPace/clockPace, 2) 
FROM (     
  SELECT SUM(flagHours) AS flagHoursToDate, SUM(clockHours) clockHoursToDate,
    @workingDaysInMonth AS workingDays, @elapsedWorkingDaysInMonth AS elapsedWorkingDays,
    round(SUM(flagHours) * @workingDaysInMonth/@elapsedWorkingDaysInMonth, 2) AS flagPace,
    round(SUM(clockHours) * @workingDaysInMonth/@elapsedWorkingDaysInMonth, 2) AS clockPace
  FROM tmpBen
  WHERE theDate < curdate()
    AND storecode = @store
    AND flagDeptCode = @department
    AND yearmonth = (
      SELECT yearmonth
      FROM dds.day
      WHERE thedate = curdate() - 1)) x; 
END;

alter PROCEDURE benGetProductionAtAGlanceWeeks( 
      store cichar(3),
      department cichar(12),
      weekStartDate DATE OUTPUT,
      weekEndDate DATE OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      target Integer OUTPUT,
      label CICHAR ( 15 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE benGetProductionAtAGlanceWeeks('ry1','bodyshop');
*/
DECLARE @store string;
DECLARE @department string;
@store = (SELECT lower(store) FROM __input);
@department = (
  SELECT 
    CASE (SELECT lower(department) FROM __input)
      WHEN 'mainshop' THEN 'mr'
      WHEN 'bodyshop' THEN 'bs'
     END
   FROM system.iota); 
INSERT INTO __output  
SELECT top 12 a.*, 
    (SELECT 
      CASE
        WHEN @store = 'ry1' and @department = 'mr' then COUNT(*) * 200
        WHEN @store = 'ry1' and @department = 'bs' then COUNT(*) * 226
      END 
    FROM dds.day
    WHERE dayofweek BETWEEN 2 AND 6
      AND holiday = false 
      AND thedate BETWEEN a.weekStartDate AND a.weekEndDate) AS target,
  (SELECT trim(mmdd) + ' to ' + 
    (select trim(mmdd) 
      from dds.day 
      where thedate = a.weekEndDate) 
    FROM dds.day 
    WHERE thedate = a.weekStartDate) AS label    
FROM (
  SELECT weekStartDate, weekEndDate, coalesce(SUM(flaghours), 0) AS flagHours,
    coalesce(SUM(clockHours), 0) AS clockHours
  FROM tmpBen
  WHERE sundayToSaturdayWeek BETWEEN (
    SELECT DISTINCT sundayToSaturdayWeek - 11
    FROM tmpBen
    WHERE thedate = curdate() - 1) 
    AND (
    SELECT DISTINCT sundayToSaturdayWeek
    FROM tmpBen
    WHERE thedate = curdate() - 1)
  AND storecode = @store  
  AND flagdeptCode = @department
  GROUP BY weekStartDate, weekEndDate) a
ORDER BY weekStartDate;

END;

ALTER PROCEDURE benGetProductionDetailsMonth( 
      store cichar(3),
      department cichar(12),    
      yearMonth Integer,  
      typeInfo CICHAR ( 5 ) OUTPUT,
      team CICHAR ( 25 ) OUTPUT,
      tech CICHAR ( 51 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      shopTime DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      proficiency DOUBLE ( 15 ) OUTPUT,
      currentlyEmployed CICHAR ( 3 ) OUTPUT,
      dateHeader CICHAR ( 24 ) OUTPUT
   ) 
BEGIN 
/*
SELECT typeInfo, LEFT(team, 15) AS team, LEFT(tech, 25) AS tech,
  flaghours, clockhours, proficiency
FROM (  
EXECUTE PROCEDURE benGetProductionDetailsMonth(201407, 'ry1', 'bodyshop')) a;
*/
DECLARE @yearMonth integer;
DECLARE @endDate date;
DECLARE @store string;
DECLARE @department string;
@store = (SELECT lower(store) FROM __input);
@department = (
  SELECT 
    CASE (SELECT lower(department) FROM __input)
      WHEN 'mainshop' THEN 'mr'
      WHEN 'bodyshop' THEN 'bs'
     END
   FROM system.iota); 
@yearMonth = (SELECT yearMonth FROM __input);
@endDate = (
  SELECT MAX(theDate)
  FROM tmpBen
  WHERE yearMonth = @yearMonth);
INSERT INTO __output
SELECT top 500 x.*, y.dateHeader
FROM (
  SELECT 'Team' as typeInfo, team, 
    left(cast(null as sql_char), 51) as tech, 
    SUM(flagHours) AS flagHours, SUM(shopTime) AS shopTime, 
    SUM(clockHours) AS clockHours, 
    CASE SUM(clockHours)
      WHEN 0 THEN 0
      ELSE round(SUM(flagHours + shopTime)/SUM(clockHours), 2) 
    END AS prof, '' AS currentlyEmployed
  FROM tmpBen
  WHERE storecode = @store
    AND flagdeptcode = @department
    AND yearmonth = @yearMonth
  GROUP BY yearmonth, team
  -- tech details
  UNION 
  SELECT 'Tech' AS typeInfo, team, tech, flagHours, shopTime, clockHours, prof,
    coalesce(currentlyEmployed, 'Yes')
  FROM (    
    SELECT employeenumber, team, 
      coalesce(TRIM(firstname) + ' ' + lastname, descName) AS tech, 
      SUM(flagHours) AS flagHours, SUM(shopTime) AS shopTime,
      SUM(clockHours) AS clockHours, 
      CASE SUM(clockHours)
        WHEN 0 THEN 0
        ELSE round(SUM(flagHours + shopTime)/SUM(clockHours), 2) 
      END AS prof 
    FROM tmpBen
    WHERE storecode = @store
      AND flagdeptcode = @department
      AND yearmonth = @yearMonth
    GROUP BY employeenumber, yearmonth, team, 
      coalesce(TRIM(firstname) + ' ' + lastname, descName)) m
  LEFT JOIN ( -- currently employed
    SELECT a.storecode, a.employeenumber, b.termdate, c.flagDeptCode,
      CASE 
        WHEN b.termdate < curdate() THEN 'No'
        ELSE
          CASE 
            WHEN a.flagDeptCode <> c.flagDeptCode THEN 'No' 
            ELSE 'Yes'
          END
      END AS currentlyEmployed      
    FROM tmpBen a
    LEFT JOIN dds.edwEmployeeDim b on a.storecode = b.storecode
      and a.employeeNumber = b.employeenumber
      AND b.currentrow = true
    LEFT JOIN dds.dimTech c on a.storecode = c.storecode
      AND a.employeenumber = c.employeenumber
      AND c.currentrow = true  
    WHERE a.thedate = @endDate
      AND a.storecode = @store
      AND a.employeenumber <> 'na'
      AND a.flagDeptCode = @department) n on m.employeenumber = n.employeenumber ) x
LEFT JOIN (
  SELECT distinct trim(monthName) + ' ' + CAST(year(thedate) AS sql_Char) AS dateHeader
  FROM tmpBen
  WHERE yearMonth = @yearMonth) y on 1 = 1 
ORDER BY team, tech;

END;


ALTER PROCEDURE benGetProductionDetailsWeek(       
      store cichar(3),
      department cichar(12), 
      theDate DATE,     
      typeInfo CICHAR ( 5 ) OUTPUT,
      team CICHAR ( 25 ) OUTPUT,
      tech CICHAR ( 51 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      shopTime DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      proficiency DOUBLE ( 15 ) OUTPUT,
      currentlyEmployed CICHAR ( 3 ) OUTPUT,
      dateHeader CICHAR ( 100 ) OUTPUT
   ) 
BEGIN 
/*
SELECT typeInfo, LEFT(team, 12) AS team, LEFT(tech, 20) AS tech,
  flaghours, clockhours, proficiency
FROM (  
EXECUTE PROCEDURE benGetProductionDetailsWeek( '05/16/2014', 'ry1', 'bodyshop')) a;
*/
DECLARE @sundayToSaturdayWeek integer;
DECLARE @endDate date;
DECLARE @store string;
DECLARE @department string;
@store = (SELECT lower(store) FROM __input);
@department = (
  SELECT 
    CASE (SELECT lower(department) FROM __input)
      WHEN 'mainshop' THEN 'mr'
      WHEN 'bodyshop' THEN 'bs'
     END
   FROM system.iota);   
@sundayToSaturdayWeek = (
  SELECT DISTINCT sundayToSaturdayWeek
  from tmpBen
  where theDate = (
    select theDate
    from __input));
@endDate = (
  SELECT MAX(thedate)
  FROM tmpBen
  WHERE sundayToSaturdayWeek = @sundayToSaturdayWeek);     
INSERT INTO __output
SELECT top 500 x.*, y.SundayToSaturdayWeekSelectFormat
FROM (
  SELECT 'Team' as typeInfo, team, 
    left(cast(null as sql_char), 51) as tech, SUM(flagHours) AS flagHours,
    SUM(shopTime) AS shopTime, 
    SUM(clockHours) AS clockHours, 
    CASE SUM(clockHours)
      WHEN 0 THEN 0
      ELSE round(SUM(flagHours + shopTime)/SUM(clockHours), 2) 
    END AS prof, '' AS currentlyEmployed
  FROM tmpBen
  WHERE storecode = @store
    AND flagdeptcode = @department
    AND sundaytosaturdayweek = @sundayToSaturdayWeek
  GROUP BY weekStartDate, weekEndDate, team
  -- tech details
  UNION 
  SELECT 'Tech' AS typeInfo, team, tech, flagHours, shopTime, clockHours, prof,
    coalesce(currentlyEmployed, 'Yes')
  FROM (   
    SELECT employeenumber, team, 
      coalesce(TRIM(firstname) + ' ' + lastname, descName) AS tech, 
      SUM(flagHours) AS flagHours,
      SUM(shopTime) AS shopTime, 
      SUM(clockHours) AS clockHours, 
      CASE SUM(clockHours)
        WHEN 0 THEN 0
        ELSE round(SUM(flagHours + shopTime)/SUM(clockHours), 2) 
      END AS prof 
    FROM tmpBen
    WHERE storecode = @store
      AND flagdeptcode = @department
      AND sundaytosaturdayweek = @sundayToSaturdayWeek
    GROUP BY employeenumber, weekStartDate, weekEndDate, team, 
      coalesce(TRIM(firstname) + ' ' + lastname, descName)) m
  LEFT JOIN ( -- currently employed
    SELECT a.storecode, a.employeenumber, b.termdate, c.flagDeptCode,
      CASE 
        WHEN b.termdate < curdate() THEN 'No'
        ELSE
          CASE 
            WHEN a.flagDeptCode <> c.flagDeptCode THEN 'No' 
            ELSE 'Yes'
          END
      END AS currentlyEmployed      
    FROM tmpBen a
    LEFT JOIN dds.edwEmployeeDim b on a.storecode = b.storecode
      and a.employeeNumber = b.employeenumber
      AND b.currentrow = true
    LEFT JOIN dds.dimTech c on a.storecode = c.storecode
      AND a.employeenumber = c.employeenumber
      AND c.currentrow = true  
    WHERE a.thedate = @endDate
      AND a.storecode = @store
      AND a.employeenumber <> 'na'
      AND a.flagDeptCode = @department) n on m.employeenumber = n.employeenumber) x      
LEFT JOIN (
  SELECT DISTINCT SundayToSaturdayWeekSelectFormat
  from tmpBen
  WHERE sundayToSaturdayWeek = @sundayToSaturdayWeek) y on 1 = 1   
ORDER BY team, tech;

END;

