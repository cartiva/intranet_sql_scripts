<div class="sidebar">
	<nav class="menu-item">
	  <a id="header-dashboard" class="mainlink" href="../main/dashboard.php"><span class="glyphicon glyphicon-home"></span>Your Dashboard<span class="arrow-right"></span></a>
	  <ul class="submenu" id="/dashboard">
	  	
	  	<!-- ADMIN LIST GOES HERE -->
	  	<li><span class="sublink">Checkout</span></li>
	  	<!-- <li><span class="sublink">Settings</span></li> -->
	  	<li><a href="../main/logout.php"><span class="sublink">All done? Logout</span></a></li>

	  </ul>
	</nav><!-- .menu-item -->

	<div id="sidebar-widgets">
		<!-- Service Stats -->
		<nav class="menu-item">
			<a class="mainlink" href="../servicestats/index.php"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
			<ul class="submenu">
		  	
		  	<li><span class="sublink">View Aged ROs</span></li>
		  	<li><span class="sublink">View Warranty Calls</span></li>

		  	</ul>
		</nav><!-- .menu-item -->

		<!-- Net Promoter -->
		<nav class="menu-item">
			<a class="mainlink" href="../netpromoter/index.php"><span class="glyphicon glyphicon-signal"></span>Net Promoter<span class="arrow-right"></span></a>
			<ul class="submenu">
		  	
		  	<li><span class="sublink">View Score</span></li>
		  	<li><span class="sublink">View Surveys</span></li>

		  	</ul>
		</nav><!-- .menu-item -->

		<!-- Employees -->
		<nav class="menu-item">
			<a class="mainlink employees" href="../main/employees.php">
				<span class="glyphicon glyphicon-user"></span>
				<em>Apps by Employee</em><span class="arrow-right"></span>
			</a>
		</nav><!-- .menu-item -->
	</div>
</div><!-- .sidebar -->

<!-- Le javascript
    ================================================== -->
<script>
  
</script>