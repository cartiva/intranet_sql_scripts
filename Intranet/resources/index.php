<?php
session_start();
require('../webservice/utils.php');
require_login();
?>


<!-- Content section for the sub-pages -->
<div id="content-sub">
<?php
require('resources-sidebar.php');
?>

<div class="stuff">
  <div id="notices">
  </div><!-- #notices -->

  <section class="stats-container">
  	<h4>Resources</h4>
  	<p>You will find all the resources you need here.</p>

  	<div class="summary-box" id="sum-servicewriters">
  		<h5>Service Writers Info Summary</h5>
  		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ornare semper libero ac dictum. Fusce malesuada nisi quis ipsum condimentum, faucibus egestas leo malesuada. Donec sed arcu purus. Nulla tempus mattis nisl, a tempus nulla feugiat ac. Nulla rutrum turpis nec diam condimentum, ac pharetra mauris molestie. Mauris consectetur volutpat leo, at auctor dolor ultrices quis. Donec interdum nibh eget mi porttitor cursus. Sed vulputate mauris ut lorem volutpat, ac suscipit augue porta. Donec sollicitudin purus tellus, eu feugiat erat congue sed. Pellentesque ultricies mollis nunc sed congue. Donec rhoncus vitae justo vitae accumsan. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus volutpat, libero quis consequat aliquam, ipsum lorem tempus urna, quis tincidunt tortor lorem sagittis ligula. Cras lectus dui, condimentum quis lacinia eget, faucibus sit amet turpis.</p>
  	</div>
  	<div class="summary-box" id="sum-netpromoter">
  		<h5>Net Promoter Summary</h5>
  		<p>Sed diam tortor, vulputate ut lacus sed, varius pulvinar enim. Nam in lacus ipsum. Duis vel pellentesque mauris. Mauris mauris erat, ullamcorper vitae mauris dignissim, luctus varius lacus. Fusce a mattis odio. Donec sed viverra nibh, ac auctor felis. Proin neque eros, vulputate in nisl a, eleifend convallis orci. Nam sodales, ante et fermentum mattis, eros neque porttitor ante, eget bibendum magna risus fermentum justo. Ut mauris lectus, lobortis in blandit commodo, ultrices in lacus. Vivamus quis lectus et purus ornare placerat id ullamcorper justo. Nullam ac mollis urna. Mauris sit amet vehicula justo. Sed id orci sagittis, accumsan risus in, cursus ligula.</p>
  	</div>

  </section><!-- /stats container -->
  
</div><!-- .stuff .important-content -->
<div class="clear"></div>
<div id="push"></div><!-- pushes content down to sicky footer -->
</div><!-- #content-sub -->
<?php
require('../includes/footer.php');
?>