<?php
//Hi there
require("utils.php");
define('CONNECTION_STRING', "DataDirectory=\\\\67.135.158.12:6363\\Advantage".
		"\\scotest\\sco.add;TrimTrailingSpaces=True");

//this function provides a database connection
function db_connect() {
	$connection_string = CONNECTION_STRING;

	$connection = ads_connect($connection_string, "AdsSys", "cartiva");
	if (!$connection) {
		throw new Exception('Could not connect to database server');
	} else {
		return $connection;
	}
}

// get the connection string
function get_connection() {
	return CONNECTION_STRING;
}

// get left nav bar items
function get_leftnavbar($username) {
	$params = array();
	$params[0] = $username;
	$data = call_stored_proc("GetLeftNavBar", $params);

	return $data;
}

// get ry2 service calls
function get_ry2_service_calls() {
	$params = array();
	$data = call_stored_proc("GetRY2ROsForCalls", $params, TRUE, "CloseDate");

	return $data;
}


// get ry2 ro to update
function get_ry2_ro($roid) {
	$params = array();
	$params[0] = $roid;

	$data = call_stored_proc("GetRY2ROToUpdate", $params);

	return $data;
}

// get ry2 statuses
function get_ry2_statuses($roid) {
	$params = array();
	$params[0] = $roid;

	$data = call_stored_proc("GetRY2ROCalls", $params, TRUE, "callTS DESC");

	return $data;
}

// get data for an RO of any type
function get_ro($roid) {
	$params = array();
	$params[0] = $roid;

	$data = call_stored_proc("GetRO", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// get some dates
function get_dates($month, $day, $year) {
	$date = "$month/$day/$year";

	$params = array();
	$params[0] = $date;

	$data = call_stored_proc("GetSundayToSaturdayWeek", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// get data for a specific warranty call
function get_call_to_update($roid) {
	$params = array();
	$params[0] = $roid;

	$data = call_stored_proc("GetWarrantyROToUpdate", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// get aged ro statuses
function get_aged_statuses($roid) {
	$params = array();
	$params[0] = $roid;

	$data = call_stored_proc("GetAgedROUpdates", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// get current ro statuses
function get_currentro_statuses($roid) {
	$params = array();
	$params[0] = $roid;

	$data = call_stored_proc("GetCurrentROUpdates", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// get warranty admin ro statuses
function get_warrantyadminro_statuses($roid) {
	$params = array();
	$params[0] = $roid;

	$data = call_stored_proc("GetWarrantyAdminROUpdates", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// get warranty calls
function get_warranty_calls($ee_username) {
	$params = array();
	$params[0] = $ee_username;

	$data = call_stored_proc("GetWarrantyROs", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// get warranty RO statuses
function get_warranty_statuses($roid) {
	$params = array();
	$params[0] = $roid;

	$data = call_stored_proc("GetWarrantyCalls", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// get a user's checkout data
function get_checkout_data($ee_username) {
	$params = array();
	$params[0] = $ee_username;

	$data = call_stored_proc("GetCheckOutStats", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// post a checkout
function post_checkout($_POST, $ee_username) {
  	$csi = $_POST['CSI'];
	$conn = db_connect();
	$query = "EXECUTE PROCEDURE 
		InsertServiceWriterCheckout('$ee_username', curdate(), $csi)";

	$result = ads_do($conn, $query);
	ads_close($conn);
}

// update a warranty call
function update_warranty_call($_POST, $roid) {
	$ee_username = $_POST['ee_username'];
	$description = htmlspecialchars($_POST['description']);
	// fix apostrophes
	$description = str_replace("'", "''", $description);
	$completed = $_POST['completed'];

	$params = array();
	$params[0] = $roid;
	$params[1] = $ee_username;
	$params[2] = $description;
	$params[3] = $completed;

	$data = call_stored_proc("InsertWarrantyCall", $params, FALSE);
}

// update ry2 status
function update_ry2_ro($_POST, $roid) {
	$ee_username = $_POST['ee_username'];
	$status_update = htmlspecialchars($_POST['status_update']);
	// fix apostrophes
	$status_update = str_replace("'", "''", $status_update);
	$completed = $_POST['completed'];

	$params = array();
	$params[0] = $roid;
	$params[1] = $ee_username;
	$params[2] = $status_update;
	$params[3] = $completed;

	$data = call_stored_proc("InsertRy2ServiceCall", $params, FALSE);
}

// returns the checkout status for an ee_user
function get_checkout_status($ee_username) {
	$params = array();
	$params[0] = $ee_username;

	$data = call_stored_proc("GetCheckoutStatus", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return ($data);
}

// some shitty functions
function get_mgr_this($ee_username) {
	$params = array();
	$params[0] = $ee_username;
	$data = call_stored_proc("GetManagerLandingPageCurrentWeek", $params, TRUE, "seq");

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

function get_mgr_last($ee_username) {
	$params = array();
	$params[0] = $ee_username;
	$data = call_stored_proc("GetManagerLandingPagePreviousWeek", $params, TRUE, "seq");

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// update an aged ro
function update_aged_ro($_POST, $roid) {
	$ee_username = $_POST['ee_username'];
	$status_update = $_POST['status_update'];
	// fix apostrophes
	$status_update = str_replace("'", "''", $status_update);
	
	$params = array();
	$params[0] = $roid;
	$params[1] = $ee_username;
	$params[2] = $status_update;

	$data = call_stored_proc("InsertROUpdate", $params, FALSE);
}

// update a current ro
function update_current_ro($_POST, $roid) {
	$ee_username = $_POST['ee_username'];
	$status_update = $_POST['status_update'];
	// fix apostrophes
	$status_update = str_replace("'", "''", $status_update);
	
	$params = array();
	$params[0] = $roid;
	$params[1] = $ee_username;
	$params[2] = $status_update;

	$data = call_stored_proc("InsertROUpdate", $params, FALSE);
}

// update a warranty admin ro
function update_warrantyadmin_ro($_POST, $roid) {
	$ee_username = $_POST['ee_username'];
	$status_update = $_POST['status_update'];
	// fix apostrophes
	$status_update = str_replace("'", "''", $status_update);
	
	$params = array();
	$params[0] = $roid;
	$params[1] = $ee_username;
	$params[2] = $status_update;

	$data = call_stored_proc("InsertROUpdate", $params, FALSE);
}

// update a pdq warranty admin ro
function update_pdqwarrantyadmin_ro($_POST, $roid) {
	$ee_username = $_POST['ee_username'];
	$status_update = $_POST['status_update'];
	// fix apostrophes
	$status_update = str_replace("'", "''", $status_update);
	
	$params = array();
	$params[0] = $roid;
	$params[1] = $ee_username;
	$params[2] = $status_update;

	$data = call_stored_proc("InsertROUpdate", $params, FALSE);
}

// get data for an ro to update
function get_ro_to_update($roid) {
	$params = array();
	$params[0] = $roid;

	$data = call_stored_proc("GetROToUpdate", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// get data for a current ro to update
function get_currentro_to_update($roid) {
	$params = array();
	$params[0] = $roid;

	$data = call_stored_proc("GetCurrentROToUpdate", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// get data for a warranry admin ro to update
/*function get_warrantyadminro_to_update($roid) {
	$params = array();
	$params[0] = $roid;

	$data = call_stored_proc("GetWarrantyAdminROToUpdate", $params);

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}*/

// get aged ro data for a username
function get_aged_ros($ee_username) {
	$params = array();
	$params[0] = $ee_username;

	$data = call_stored_proc("GetAgedROs", $params);

	return $data;
}

// get current ro data for a username
function get_current_ros($ee_username) {
	$params = array();
	$params[0] = $ee_username;

	$data = call_stored_proc("GetCurrentROs", $params);

	return $data;
}

// get warranty admin ro data for a username
function get_warrantyadmin_ros($ee_username) {
	$params = array();
	$params[0] = $ee_username;

	$data = call_stored_proc("GetWarrantyAdminROs", $params);

	return $data;
}

// get pdq warranty admin ro data for a username
function get_pdqwarrantyadmin_ros($ee_username) {
	$params = array();
	$params[0] = $ee_username;

	$data = call_stored_proc("GetWarrantyAdminPdqROs", $params);

	return $data;
}


// get the data for a user's landing page
function get_landing($ee_username) {
	$params = array();
	$params[0] = $ee_username;

	$data = call_stored_proc("GetWriterLandingPage", $params, TRUE, "seq");

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// returns the main actions for a user
function get_main_actions($username) {
	$params = array();
	$params[0] = $username;

	return call_stored_proc("GetMainActions", $params, TRUE);
}

// returns the main actions for a user
function get_sub_actions($username, $appcode) {
	$params = array();
	$params[0] = $username;
	$params[1] = $appcode;

	return call_stored_proc("GetSubActions", $params, TRUE);
}

// returns the info about a user
function get_user_info($username) {
	$params = array();
	$params[0] = $username;

	return call_stored_proc("GetUserData", $params, TRUE);
}


// returns the writers for an ee_user
function get_writers($ee_username) {
	$params = array();
	$params[0] = $ee_username;

	$data = call_stored_proc("GetWritersForManager", $params, TRUE, "writerName");

	if (!empty($data)) {
		$data = json_encode($data);
	}

	return $data;
}

// Get some tasks for a user
function get_tasks($ee_username) {
	$params = array();
	$params[0] = $ee_username;

	$data = call_stored_proc("GetTasksForUser", $params, TRUE);

	return $data;
}

// Get some tasks for a user's employees
function get_employee_tasks($ee_username) {
	$params = array();
	$params[0] = $ee_username;

	$data = call_stored_proc("GetEmployeeTasksForUser", $params, TRUE);

	return $data;
}

//getronew(roid)
function get_ro_new($roid) {
	$params = array();
	$params[0] = $roid;

	//$data = call_stored_proc("GetRoNew", $params, TRUE);
	$data = call_stored_proc("GetRoHeader", $params, TRUE);
	$data[] = call_stored_proc("GetRoDetail", $params, TRUE);

	return $data;
}

//getwriterlaborsalesmonthtodate(username)
function get_sales_mtd($username) {
	$params = array();
	$params[0] = $username;

	$data = call_stored_proc("GetWriterLaborSalesMonthToDate", $params, TRUE);

	return $data;
}

// logging function maybe we'll use this someday
function log_stored_proc($info) {
	$proc = $info['storedProc'];
	$called_by = $info['calledBy'];
	$query_used = $info['queryUsed'];
	$result_data = $info['result'];

	$conn = db_connect();
	$query = "INSERT INTO log (ts, storedProc, calledBy, queryUsed, result)
				VALUES(now(), :proc, :calledBy, :queryUsed, :resultData)";

	$result = ads_prepare($conn, $query);

	$params = array();
	$params[':proc'] = $proc;
	$params[':calledBy'] = $called_by;
	$params[':queryUsed'] = $query_used;
	$params[':resultData'] = $result_data;

	$success = ads_execute($result, $params);
	
	ads_close($conn);
}

// call a stored proc, log it, and return the result
function call_stored_proc($proc_name, $params = array(), $return_data = TRUE,
		$orderby = "") {

	$conn = db_connect();

	if (!empty($params)) {
		$params =  "'" .implode("','", $params) . "'";
	} else {
		$params = "";
	}

	if (!empty($orderby)) {
		$query = "SELECT * FROM (EXECUTE PROCEDURE $proc_name($params)) 
			a ORDER BY $orderby";
	} else {
		$query = "EXECUTE PROCEDURE $proc_name($params)";
	}

	$result = ads_do($conn, $query);
	if (ads_error() != '') {
  	  header("HTTP/1.1 500 Internal Server Error");
	}
	// build the result data
	$data = array();

	if (!empty($result) && $return_data) {
		while($row = ads_fetch_array($result)) {
			$data[] = $row;
		}
	}

	ads_close($conn);

	/* //log the information about the call
	$info = array();
	$info['queryUsed'] = $query;
	$info['calledBy'] = "webservice db_funcs.php";
	$info['storedProc'] = $proc_name;

	if ($return_data) {
		$info['result'] = json_encode($data);
	} else {
		$info['result'] = ("insert");
	} */

	// log_stored_proc($info);

	// return the data
	return $data;
}

function log_db($_POST) {
	$data = serialize($_POST);

	$conn = db_connect();
	$query = "INSERT INTO log(ts, postData) VALUES(now(), '$data')";

	$result = ads_do($conn, $query);

}

?>