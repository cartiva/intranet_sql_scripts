<?php
function require_login() {
	if (!isset($_SESSION['LoggedIn'])) {
		header('Location: ../main/please_login.php');
	} else {
		require('../includes/header.php');	
	}
}

//email stuff re-added after Mike took it out. Without it the pages wouldn't load any data. Decide later if we need it for email purposes.
function is_email($string) {
	if (stripos($string, "@") === false) {
		return false;
	} else {
		return true;
	}
}

function show_form_errors($errors) {
	echo "<ul class='form_errors'>\n";
	foreach ($errors as $error) {
		echo "<li>$error</li>\n";
	}
	echo "</ul>\n";
}

?>
