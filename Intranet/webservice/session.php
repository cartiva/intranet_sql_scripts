<?php
session_start(); // use sessions
// check if they are logged in
$output = array();
if (!isset($_SESSION['FullName'])) {
	$output['loggedIn'] = 'false';
  echo json_encode($output);
} else {
	$output['loggedIn'] = 'true';
	$output['userName'] = $_SESSION['Username'];
	$output['firstName'] = $_SESSION['FirstName'];
	$output['lastName'] = $_SESSION['LastName'];
	$output['fullName'] = $_SESSION['FullName'];
	$output['memberGroup'] = $_SESSION['MemberGroup'];
	$output['storeCode'] = $_SESSION['StoreCode'];
	echo json_encode($output);
}
?>