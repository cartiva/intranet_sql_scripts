<?php
require_once('db_funcs.php');
require_once('utils.php');

// check for path elements
if (!empty($_SERVER['PATH_INFO'])) {
	$path = $_SERVER['PATH_INFO'];
	$path_params = explode("/", $path);
}

// check the http method and perform an appropriate query
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	//log_db($_POST);

	if (!empty($path_params)) {

		switch($path_params[1]) {

			// ry2rostatuses/{roid}
			case 'ry2rostatuses':
				$roid = $path_params[2];
				update_ry2_ro($_POST, $roid);
			break;

			// warrantycall/{roid}
			case 'warrantycall':
				$roid = $path_params[2];
				update_warranty_call($_POST, $roid);
			break;

			// agedros/{roid}
			case 'agedro':
				$roid = $path_params[2];
				update_aged_ro($_POST, $roid);
			break;

			// currentros/{roid}
			case 'currentro':
				$roid = $path_params[2];
				update_current_ro($_POST, $roid);
			break;

			// warrantyadminros/{roid}
			case 'warrantyadminro':
				$roid = $path_params[2];
				update_warrantyadmin_ro($_POST, $roid);
			break;

			// pdqwarrantyadminros/{roid}
			case 'pdqwarrantyadminro':
				$roid = $path_params[2];
				update_pdqwarrantyadmin_ro($_POST, $roid);
			break;

			// checkouts/{ee_username} <--old format
			// checkouts/{session_id}
			case 'checkouts':
				if (!is_email($path_params[2])) {
					$ee_username = get_session_member($path_params[2]);
				} else {
					$ee_username = $path_params[2];
				}
				post_checkout($_POST, $ee_username);
			break;				
		}
	}

} else if ($_SERVER['REQUEST_METHOD'] == 'GET') {

	if (!empty($path_params)) {

		switch($path_params[1]) {

			/*********************
			// Left Nav Bar
			**********************/

			// leftnavbar/{username}
			case 'leftnavbar':
			$leftnavbar = get_leftnavbar($path_params[2]);
			echo json_encode($leftnavbar);
			break;

			// /*********************
			// // NPSCORE STUFF
			// **********************/

			// // npsummaryscores/{username}
			// case 'npsummaryscores':
			// $npsummaryscores = get_npsummaryscores();
			// echo json_encode($npsummarycores);
			// break;

			// // npwriterlist/{username}
			// case 'npwriterlist':
			// $npwriterlist = get_npwriterlist();
			// echo json_encode($npscores);
			// break;

			// // npscores/{department or username}
			// case 'npscores':
			// $npscores = get_npscores();
			// echo json_encode($npscores);
			// break;

			// // npsurveys/{department or username}
			// case 'npsurveys':
			// $npsurveys = get_npsurveys();
			// echo json_encode($npsurveys);
			// break;
             
			/**************************
			// CHECKOUT STUFF
			**************************/

			// connection
			case 'connection':
			$connection = get_connection();
			echo json_encode($connection);
			break;

			// newros/{ro}
			case 'newros':
			$ro = get_ro_new($path_params[2]);
			echo json_encode($ro);
			break;

			// /salesmtd/{username}
			case 'salesmtd':
			$sales = get_sales_mtd($path_params[2]);
			echo json_encode($sales);
			break;

			// /ry2ros/{ro}
			case 'ry2ros':
			$ro = get_ry2_ro($path_params[2]);
			if (!empty($ro)) {
				echo json_encode($ro);
			} else {
				echo json_encode($ro);
			}
			break;

			

			// /ry2rostatuses/{ro}
			case 'ry2rostatuses':
			$statuses = get_ry2_statuses($path_params[2]);
			if (!empty($statuses)) {
				echo json_encode($statuses);
			} else {
				echo json_encode($statuses);
			}
			break;

			// /ry2servicecalls
			case 'ry2servicecalls':
			$service_calls = get_ry2_service_calls();
			if (!empty($service_calls)) {
				echo json_encode($service_calls);
			} else {
				echo json_encode($service_calls);
			}
			break;

			//tasks/{ee_username}

			case 'tasks':
				if (!is_email($path_params[2])) {
					$ee_username = get_session_member($path_params[2]);
				} else {
					$ee_username = $path_params[2];
				}

				$tasks = get_tasks($ee_username);

				if (!empty($tasks)) {
					echo json_encode($tasks);
				} else {
					echo json_encode($tasks);
				}
				break;

			//employeetasks/{ee_username}

			case 'employeetasks':
				if (!is_email($path_params[2])) {
					$ee_username = get_session_member($path_params[2]);
				} else {
					$ee_username = $path_params[2];
				}

				$employeetasks = get_employee_tasks($ee_username);

				if (!empty($employeetasks)) {
					echo json_encode($employeetasks);
				} else {
					echo json_encode($employeetasks);
				}
				break;

			// checkoutstatus/{ee_username} <--old format
			// checkoutstatus/{session_id}
			case 'checkoutstatus':
				if (!is_email($path_params[2])) {
					$ee_username = get_session_member($path_params[2]);
				} else {
					$ee_username = $path_params[2];
				}

				$checked_out = get_checkout_status($ee_username);
				if (!empty($checked_out)) {
					echo $checked_out;
				} else {
					echo $checked_out;
				}
				break;

			// ros/{roid}
			case 'ros':
				$ro_data = get_ro($path_params[2]);
				if (!empty($ro_data)) {
					echo $ro_data;
				} else {
					echo $ro_data;
				}
				break;

			// dates/{month}/{day}/{year}	
			case 'dates':
				$dates = get_dates($path_params[2], $path_params[3], $path_params[4]);
				if (!empty($dates)) {
					echo $dates;
				} else {
					echo $dates;
				}
				break;

			// warrantycall/{roid}
			case 'warrantycall':
				$call_data = get_call_to_update($path_params[2]);
				if (!empty($call_data)) {
					echo $call_data;
				} else {
					echo $call_data;
				}
				break;

			// warrantycalls
			case 'warrantycalls':
				// warrantycalls/statuses/{roid}
				if ($path_params[2] == 'statuses') {
					$status_data = get_warranty_statuses($path_params[3]);
					if (!empty($status_data)) {
						echo $status_data;
					} else {
						echo json_encode($status_data);
					}
				// warrantycalls/{ee_username} <--old format
				// warrantycalls/{session_id}
				} else {

					if (!is_email($path_params[2])) {
						$ee_username = get_session_member($path_params[2]);
					} else {
						$ee_username = $path_params[2];
					}

					$calls_data = get_warranty_calls($ee_username);
					if (!empty($calls_data)) {
						echo $calls_data;
					} else {
						echo json_encode($calls_data);
					}
				}
				break;

			// agedro/{roid}	
			case 'agedro':
				$ro_data = get_ro_to_update($path_params[2]);
				if (!empty($ro_data)) {
					echo $ro_data;
				} else {
					echo $ro_data;
				}
				break;

			// agedros
			case 'agedros':
				// agedros/statuses/{roid}
				if ($path_params[2] == 'statuses') {
					$status_data = get_aged_statuses($path_params[3]);
					if (!empty($status_data)) {
						echo $status_data;
					} else {
						echo json_encode($status_data);
					}
				// agedros/{ee_username} <--old format
				// agedros/{session_id}	
				} else {
					if (!is_email($path_params[2])) {
						$ee_username = get_session_member($path_params[2]);
					} else {
						$ee_username = $path_params[2];
					}

					$ro_data = get_aged_ros($ee_username);
					if (!empty($ro_data)) {
						echo json_encode($ro_data);
					} else {
						echo json_encode($ro_data);
					}
				}
				
				break;

			// currentro/{roid}	
			case 'currentro':
				$ro_data = get_currentro_to_update($path_params[2]);
				if (!empty($ro_data)) {
					echo $ro_data;
				} else {
					echo $ro_data;
				}
				break;

			// currentros
			case 'currentros':
				// currentros/statuses/{roid}
				if ($path_params[2] == 'statuses') {
					$status_data = get_currentro_statuses($path_params[3]);
					if (!empty($status_data)) {
						echo $status_data;
					} else {
						echo json_encode($status_data);
					}
				// currentros/{ee_username} <--old format
				// currentros/{session_id}	
				} else {
					if (!is_email($path_params[2])) {
						$ee_username = get_session_member($path_params[2]);
					} else {
						$ee_username = $path_params[2];
					}

					$ro_data = get_current_ros($ee_username);
					if (!empty($ro_data)) {
						echo json_encode($ro_data);
					} else {
						echo json_encode($ro_data);
					}
				}
				
				break;











			// warrantyadminro/{roid}	
			case 'warrantyadmin':
				$ro_data = get_warrantyadminro_to_update($path_params[2]);
				if (!empty($ro_data)) {
					echo $ro_data;
				} else {
					echo $ro_data;
				}
				break;

			// warrantyadminros
			case 'warrantyadminros':
				// warrantyadminros/statuses/{roid}
				if ($path_params[2] == 'statuses') {
					$status_data = get_warrantyadminro_statuses($path_params[3]);
					if (!empty($status_data)) {
						echo $status_data;
					} else {
						echo json_encode($status_data);
					}
				// warrantyadminros/{ee_username} <--old format
				// warrantyadminros/{session_id}	
				} else {
					if (!is_email($path_params[2])) {
						$ee_username = get_session_member($path_params[2]);
					} else {
						$ee_username = $path_params[2];
					}

					$ro_data = get_warrantyadmin_ros($ee_username);
					if (!empty($ro_data)) {
						echo json_encode($ro_data);
					} else {
						echo json_encode($ro_data);
					}
				}
				
				break;





			// pdqwarrantyadminro/{roid}	
			case 'pdqwarrantyadmin':
				$ro_data = get_pdqwarrantyadminro_to_update($path_params[2]);
				if (!empty($ro_data)) {
					echo $ro_data;
				} else {
					echo $ro_data;
				}
				break;

			// pdqwarrantyadminros
			case 'pdqwarrantyadminros':
				// pdqwarrantyadminros/statuses/{roid}
				if ($path_params[2] == 'statuses') {
					$status_data = get_warrantyadminro_statuses($path_params[3]);
					if (!empty($status_data)) {
						echo $status_data;
					} else {
						echo json_encode($status_data);
					}
				// pdqwarrantyadminros/{ee_username} <--old format
				// pdqwarrantyadminros/{session_id}	
				} else {
					if (!is_email($path_params[2])) {
						$ee_username = get_session_member($path_params[2]);
					} else {
						$ee_username = $path_params[2];
					}

					$ro_data = get_pdqwarrantyadmin_ros($ee_username);
					if (!empty($ro_data)) {
						echo json_encode($ro_data);
					} else {
						echo json_encode($ro_data);
					}
				}
				
				break;












			// checkouts/{ee_username}	<--old format
			// checkouts/{session_id}
			case 'checkouts':
				if (!is_email($path_params[2])) {
					$ee_username = get_session_member($path_params[2]);
				} else {
					$ee_username = $path_params[2];
				}

				$checkout_data = get_checkout_data($ee_username);
				if (!empty($checkout_data)) {
					echo $checkout_data;
				} else {
					echo $checkout_data;
				}
				break;

			// landing/{ee_username} <--old format
			// landing/{session_id}
			case 'landing':
				if (!is_email($path_params[2])) {
					$ee_username = get_session_member($path_params[2]);
				} else {
					$ee_username = $path_params[2];
				}

				$landing_data = get_landing($ee_username);
				if (!empty($landing_data)) {
					echo $landing_data;
				} else {
					echo $landing_data;
				}
				break;

			// mgrlanding/ee_username/{last/this}
			case 'mgrlanding':
				if (!is_email($path_params[2])) {
					$ee_username = get_session_member($path_params[2]);
				} else {
					$ee_username = $path_params[2];
				}

				if ($path_params[3] == 'last') {
					$mgr_data = get_mgr_last($ee_username);
				} else if ($path_params[3] == 'this') {
					$mgr_data = get_mgr_this($ee_username);
				} else {
					$mgr_data = '';
				}

				if (!empty($mgr_data)) {
					echo $mgr_data;
				} else {
					echo $mgr_data;
				}
				break;

			// sessions/{session_id}	
			case 'sessions':
				$ee_username = get_session_member($path_params[2]);
				if (!empty($ee_username)) {
					echo json_encode($ee_username);
				} else {
					echo json_encode($ee_username);
				}
				break;

			// mainactions/{ee_username}	
			case 'mainactions':
				echo json_encode(get_main_actions($path_params[2]));
				break;

			// subactions/{ee_username}/{appcode}
			case 'subactions':
				echo json_encode(get_sub_actions($path_params[2], $path_params[3]));
				break;

			// userinfo/{ee_username}
			case 'userinfo':
				echo json_encode(get_user_info($path_params[2]));
				break;

			// writers/{ee_username}
			case 'writers':
				if (!is_email($path_params[2])) {
					$ee_username = get_session_member($path_params[2]);
				} else {
					$ee_username = $path_params[2];
				}

				$writers_data = get_writers($ee_username);
				if (!empty($writers_data)) {
					echo $writers_data;
				} else {
					echo $writers_data;
				}
				break;

			default: 
				show_error(400);
		}
	}
	
} else if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
	

} else if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {

}



// This function returns an error based on an http status code and optional 
// message
function show_error($status_code, $message = "") {
	$header = "";

	switch ($status_code) {
		case 400:
			$header = "HTTP/1.1 400 Bad Request";
		break;

		case 404:
			$header = "HTTP/1.1 404 Not Found";
		break;

		case 500:
			$header = "HTTP/1.1 500 Internal Server Error";
		break;
	}

	header($header);

}


?>