/*
* car.mod.teampay.splash.js
*
* This module handles the display of the splash screens.
*   Tech Splash
*   Team Leader/Manager Splash
*
* Data needs for module:
*
*   Technician Splash:
*     Tech name
*     Pay period begin date
*     Pay period end date
*     Current pay rate
*
*     Daily pay data tech
*
*     Total pay data for tech
*
*   Manager Splash:
*
*     
* Template for browser feature modules
*/
/*jslint         browser: true,  continue : true,
  devel  : true, indent : 2,     maxerr   : 50,
  newcap : true, nomen  : true,  plusplus : true,
  regexp : true, sloppy : true,  vars     : false,
  white  : true
*/

/*global $, car.mod.teampay.splash */

car.teampay.splash = (function () {

  //--------------------- BEGIN MODULE SCOPE VARIABLES ------------------------
  var
    configMap = {
      
      // When tech is logged in
      tech_summary_html : String()
        + '<h2 class="page-header">Your Pay Breakdown</h2>'
        + '<h4><small>for the pay-period '
        + '  <span id="fromDate">FROMDATE</span> - '
        + '  <span id="thruDate">THRUDATE</span>'
        + '</small></h4>'
        + '<aside>'
        + '  <h5>Team Proficiency: <span>TEAMPROFICIENCY</span></h5>'
        + '  <h5>Regular Pay Rate: <span>REGULARPAYRATE</span></h5>'
        + '  <h5>Other Pay Rate: <span>OTHERPAYRATE</span></h5>'
        + '</aside>',

      tech_daily_html : String()
        + '<h3 class="section-start">Your Regular Pay</h3>'
        + '<table class="splash table table-hover table-striped'
        + '   table-condensed">'
        + '  <thead> '
        + '    <tr class="headings"> '
        + '      <th class="column">Day</th> '
        + '      <th>Flag Hours</th> '
        + '      <th>Clock Hours</th>'
        + '      <th>Team Prof.</th> '
        + '      <th>Pay Hours</th> '
        + '      <th>Regular Pay</th>'
        + '    </tr> '
        + '  </thead> '
        + '  <tbody> ',

      tech_other_html: String()
        + '<h3 class="section-start">Your Other Pay</h3>'
        + '<table class="splash table table-hover table-striped'
        + '   table-condensed">'
        + '  <thead> '
        + '    <tr id="headings"> '
        + '      <th class="column">Day</th> '
        + '      <th>Vacation</th> '
        + '      <th>PTO</th>'
        + '      <th>Holiday</th>'
        + '    </tr> '
        + '  </thead> '
        + '  <tbody> ',

      tech_totalpay_html : String ()
        + '<h3 class="section-start">Total Gross Pay Calculation</h3>'
        + '  <div class="panel panel-default">'
        + '    <div class="panel-body table-responsive">'
        + '      <table class="calc table">'
        + '        <tr class="captions">'
        + '          <td>Team Flag Hrs</td>'
        + '          <td><i>/</i></td>'
        + '          <td>Team Clock Hrs</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Team Proficiency</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>TEAMFLAGHOURS</td>'
        + '          <td><small>/</small></td>'
        + '          <td>TEAMCLOCKHOURS</td>'
        + '          <td><small>=</small></td>'
        + '          <td>TEAMPROFICIENCY</td>'
        + '        </tr>'
        + '      </table>'
        + '      <table class="calc table">'
        + '        <tr class="captions">  '
        + '          <td>Team Proficiency</td>'
        + '          <td><i>x</i></td>'
        + '          <td>Your Clock Hrs</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Your Pay Hrs</td>'
        + '          <td><i>x</i></td>'
        + '          <td>Your Pay Rate</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Your Regular Pay</td>'
        + '        </tr>'
        + '        <tr>  '
        + '          <td>TEAMPROFICIENCY</td>'
        + '          <td><small>x</small></td>'
        + '          <td>YOURCLOCKHOURS</td>'
        + '          <td><small>=</small></td>'
        + '          <td>YOURPAYHOURS</td>'
        + '          <td><small>x</small></td>'
        + '          <td>YOURPAYRATE</td>'
        + '          <td><small>=</small></td>'
        + '          <td>YOURREGULARPAY</td>'
        + '        </tr>'
        + '      </table>'
        + '      <table class="calc table">'
        + '        <tr class="captions">'
        + '          <td>Your Other Pay Hrs</td>'
        + '          <td><i>x</i></td>'
        + '          <td>Your Other Pay Rate</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Your Other Pay</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>YOUROTHERPAYHOURS</td>'
        + '          <td><small>x</small></td>'
        + '          <td>YOUROTHERPAYRATE</td>'
        + '          <td><small>=</small></td>'
        + '          <td>YOUROTHERPAY</td>'
        + '        </tr>'
        + '      </table>'
        + '      <table class="calc table">'
        + '        <tr class="captions">'
        + '          <td>Your Regular Pay</td>'
        + '          <td><i>+</i></td>'
        + '          <td>Your Other Pay</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Total Gross Pay</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>YOURREGULARPAY</td>'
        + '          <td><small>+</small></td>'
        + '          <td>YOUROTHERPAY</td>'
        + '          <td><small>=</small></td>'
        + '          <td>YOURGROSSPAY</td>'
        + '        </tr>'
        + '      </table>'
        + '    </div><!--/.panel-body-->'
        + '  </div><!--/.panel.panel-default-->',

      // When manager logged in
      manager_summary_html : String ()
        + '<h2 class="page-header">Main Shop Totals</h2>'
        + '<h4><small>for the pay-period'
        + '  <span id="fromDate">FROMDATE</span> - '
        + '  <span id="thruDate">THRUDATE</span>'
        + '</small></h4>',

      manager_shop_detail_html : String()
        + '<table class="splash table table-hover table-striped'
        + '   table-condensed">'
        + '  <thead> '
        + '    <tr id="headings"> '
        + '      <th class="column">Teams</th> '
        + '      <th>Flag Hours</th> '
        + '      <th>Clock Hours</th>'
        + '      <th>Team Prof.</th> '
        + '      <th>Pay Hours</th> '
        + '      <th>Regular Pay</th>'
        + '    </tr> '
        + '  </thead> '
        + '  <tbody> ',

      manager_team_detail_html : String()
        + '<h4>Team: TEAMNAME</h4>'
        + '<table class="splash table table-hover table-striped'
        + '   table-condensed">'
        + '  <thead> '
        + '    <tr id="headings"> '
        + '      <th class="column">Employee</th> '
        + '      <th>Flag Hours</th> '
        + '      <th>Clock Hours</th>'
        + '      <th>Team Prof.</th> '
        + '      <th>Pay Hours</th> '
        + '      <th>Regular Pay</th>'
        + '    </tr> '
        + '  </thead> '
        + '  <tbody> ',

      // When a team-lead logged in
      teamlead_summary_html : String ()
        + '<h2 class="page-header">Your Pay Breakdown</h2>'
        + '<h4><small>for the pay-period '
        + '  <span id="fromDate">FROMDATE</span> - '
        + '  <span id="thruDate">THRUDATE</span>'
        + '</small></h4>'
        + '<aside>'
        + '  <h5>Team Proficiency: <span>TEAMPROFICIENCY</span></h5>'
        + '  <h5>Regular Pay Rate: <span>REGULARPAYRATE</span></h5>'
        + '  <h5>Other Pay Rate: <span>OTHERPAYRATE</span></h5>'
        + '</aside>'
        + '<div class="btn-group">'
        + '  <a class="btn teamleadtotals" href="#">See Store / Team Totals</a>'
        + '</div>',

      teamlead_daily_html : String ()
        + '<h3 class="section-start">Your Regular Pay</h3>'
        + '<table class="splash table table-hover table-striped'
        + '   table-condensed"> '
        + '  <thead> '
        + '    <tr id="headings"> '
        + '      <th class="column">Day</th> '
        + '      <th>Flag Hours</th> '
        + '      <th>Clock Hours</th>'
        + '      <th>Team Prof.</th> '
        + '      <th>Pay Hours</th> '
        + '      <th>Regular Pay</th>'
        + '    </tr> '
        + '  </thead> '
        + '  <tbody> ',
      
      teamlead_other_html : String ()
        + '<h3 class="section-start">Your Other Pay</h3>'
        + '<table class="splash table table-hover table-striped'
        + '  table-condensed"> '
        + '<thead> '
        + '  <tr id="headings"> '
        + '    <th class="column">Day</th> '
        + '    <th>Vacation</th> '
        + '    <th>PTO</th>'
        + '    <th>Holiday</th>'
        + '  </tr> '
        + '</thead> '
        + '<tbody> ',

      teamlead_totalpay_html : String ()
        + '<h3 class="section-start">Total Gross Pay Calculation</h3>'
        + '  <div class="panel panel-default">'
        + '    <div class="panel-body table-responsive">'
        + '      <table class="calc table">'
        + '        <tr class="captions">'
        + '          <td>Team Flag Hrs</td>'
        + '          <td><i>/</i></td>'
        + '          <td>Team Clock Hrs</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Team Proficiency</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>TEAMFLAGHOURS</td>'
        + '          <td><small>/</small></td>'
        + '          <td>TEAMCLOCKHOURS</td>'
        + '          <td><small>=</small></td>'
        + '          <td>TEAMPROFICIENCY</td>'
        + '        </tr>'
        + '      </table>'
        + '      <table class="calc table">'
        + '        <tr class="captions">  '
        + '          <td>Team Proficiency</td>'
        + '          <td><i>x</i></td>'
        + '          <td>Your Clock Hrs</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Your Pay Hrs</td>'
        + '          <td><i>x</i></td>'
        + '          <td>Your Pay Rate</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Your Regular Pay</td>'
        + '        </tr>'
        + '        <tr>  '
        + '          <td>TEAMPROFICIENCY</td>'
        + '          <td><small>x</small></td>'
        + '          <td>YOURCLOCKHOURS</td>'
        + '          <td><small>=</small></td>'
        + '          <td>YOURPAYHOURS</td>'
        + '          <td><small>x</small></td>'
        + '          <td>YOURPAYRATE</td>'
        + '          <td><small>=</small></td>'
        + '          <td>YOURREGULARPAY</td>'
        + '        </tr>'
        + '      </table>'
        + '      <table class="calc table">'
        + '        <tr class="captions">'
        + '          <td>Your Other Pay Hrs</td>'
        + '          <td><i>x</i></td>'
        + '          <td>Your Other Pay Rate</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Your Other Pay</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>YOUROTHERPAYHOURS</td>'
        + '          <td><small>x</small></td>'
        + '          <td>YOUROTHERPAYRATE</td>'
        + '          <td><small>=</small></td>'
        + '          <td>YOUROTHERPAY</td>'
        + '        </tr>'
        + '      </table>'
        + '      <table class="calc table">'
        + '        <tr class="captions">'
        + '          <td>Your Regular Pay</td>'
        + '          <td><i>+</i></td>'
        + '          <td>Your Other Pay</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Total Gross Pay</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>YOURREGULARPAY</td>'
        + '          <td><small>+</small></td>'
        + '          <td>YOUROTHERPAY</td>'
        + '          <td><small>=</small></td>'
        + '          <td>YOURGROSSPAY</td>'
        + '        </tr>'
        + '      </table>'
        + '    </div><!--/.panel-body-->'
        + '  </div><!--/.panel.panel-default-->',

      settable_map : {}
    },

    stateMap = { 
      $container            : null,
      loggedIn              : false,
      tech                  : false,
      manager               : false,
      splash_tech_displayed : false,
      splash_mgr_displayed  : false
    },

    jqueryMap = {},
 
    setJqueryMap,         configModule,                  initModule,
    onShowTech,           onFulfilledTechData,           onErrorTechData,
    onShowManager,        onFulfilledManagerData,        onErrorManagerData,
    onShowTeamLead,       onFulfilledTeamLeadData,       onErrorTeamLeadData,
    onShowTeamLeadTotals, onFulfilledTeamLeadTotalsData, onErrorTeamLeadTotalsData
  ;

  //--------------------- END MODULE SCOPE VARIABLES --------------------------
 

  //------------------------ BEGIN UTILITY METHODS ----------------------------
  function replaceAll(str, mapObj){
    var re = new RegExp( Object.keys(mapObj).join("|"), "g");

    return str.replace(re, function(matched){
      return mapObj[matched];
    });
  }
//------------------------- END UTILITY METHODS -----------------------------
 

  //-------------------------- BEGIN DOM METHODS ------------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;
    jqueryMap = { $container : $container };
  };

  // End DOM method /setJqueryMap/
  //-------------------------- END DOM METHODS --------------------------------
 

  //----------------------- BEGIN EVENT HANDLERS ------------------------------

  onShowTech = function( e, techID ) {
    $.gevent.publish('splash.requestTechData', [techID]);
  };

  onFulfilledTechData = function(e, summary, daily, total, otherdaily, othertotal ) {
    var map, header, dailybody = '', totalbody = '', footer = '', otherdailybody = '', othertotalbody = '', otherfooter = '', totalpay = '';

    map = {
      TECHNAME        : summary.techName,
      FROMDATE        : moment(summary.fromDate).format("MM/DD/YYYY"),
      THRUDATE        : moment(summary.thruDate).format("MM/DD/YYYY"),
      TEAMPROFICIENCY : numeral(summary.teamProficiency).format("0.0%"),
      REGULARPAYRATE  : numeral(summary.regularRate).format("$0,0.00"),
      OTHERPAYRATE    : numeral(summary.otherRate).format("$0,0.00")
    };
    header = replaceAll( configMap.tech_summary_html, map );

    $.each(daily, function(i, item) {
      dailybody += '<tr>'
        + '  <td>' + moment(item.date).format("ddd., MMM. DD") + '</td>'
        + '  <td>' + numeral(item.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + item.teamProf + ' <small>%</small>' + '</td>'
        + '  <td>' + numeral(item.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td> ' + numeral(item.regularPay).format("$0,0.00") + '</td>'
        + '</tr>';
    });

    totalbody += '<tr class="total">'
      + '  <td>' + 'Totals' + '</td>'
      + '  <td>' + numeral(total.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + numeral(total.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + total.teamProf + ' <small>%</small>' + '</td>'
      + '  <td>' + numeral(total.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td> ' + numeral(total.regularPay).format("$0,0.00") + '</td>'
      + '</tr>';

    footer += '  </tbody>'
      + '</table>'
      + '<h4>How your pay is calculated</h4>'
      + '<p>--We will put a calculation with an image or markup here--</p>';

    $.each(otherdaily, function(i, item) {
      otherdailybody += '<tr>'
        + '  <td>' + moment(item.theDate).format("ddd., MMM. DD") + '</td>'
        + '  <td>' + numeral(item.vacationHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.ptoHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.holidayHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '</tr>';
    });

    othertotalbody += '<tr class="total">'
      + '  <td>' + 'Totals' + '</td>'
      + '  <td>' + numeral(total.vacationHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + numeral(total.ptoHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + numeral(total.holidayHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '</tr>';

    otherfooter += '  </tbody>'
      + '</table>';

    map = {
      TEAMFLAGHOURS     : numeral(summary.teamFlagHours).format("0,0.00"),
      TEAMCLOCKHOURS    : numeral(summary.teamClockHours).format("0,0.00"),
      TEAMPROFICIENCY   : numeral(summary.teamProficiency).format("0.0%"),
      YOURCLOCKHOURS    : numeral(summary.clockHours).format("0,0.00"),
      YOURPAYHOURS      : numeral(summary.payHours).format("0,0.00"),
      YOURPAYRATE       : numeral(summary.regularRate).format("$0,0.00"),
      YOURREGULARPAY    : numeral(summary.regularPay).format("$0,0.00"),
      YOUROTHERPAYHOURS : numeral(summary.otherHours).format("0,0.00"),
      YOUROTHERPAYRATE  : numeral(summary.otherRate).format("$0,0.00"),
      YOUROTHERPAY      : numeral(summary.otherPay).format("$0,0.00"),
      YOURGROSSPAY      : numeral(summary.totalGrossPay).format("$0,0.00")
    };
    totalpay = replaceAll( configMap.tech_totalpay_html, map );


    jqueryMap.$container.html( header + configMap.tech_daily_html + dailybody + totalbody + footer + configMap.tech_other_html + otherdailybody + othertotalbody + otherfooter + totalpay );
  };

  onErrorTechData = function( e, errorMsg ) {
    jqueryMap.$container.html( errorMsg );
  };

  onShowTeamLead = function( e, teamLeadID ) {
    $.gevent.publish('splash.requestTeamLeadData', [ teamLeadID ]);
  };

  onFulfilledTeamLeadData = function(e, summary, daily, total, otherdaily, othertotal ) {
    var map, header, dailybody = '', totalbody = '', footer = '', otherdailybody = '', othertotalbody = '', otherfooter = '', totalpay = '';

    map = {
      TECHNAME        : summary.techName,
      FROMDATE        : moment(summary.fromDate).format("MM/DD/YYYY"),
      THRUDATE        : moment(summary.thruDate).format("MM/DD/YYYY"),
      TEAMPROFICIENCY : numeral(summary.teamProficiency).format("0.0%"),
      REGULARPAYRATE  : numeral(summary.regularRate).format("$0,0.00"),
      OTHERPAYRATE    : numeral(summary.otherRate).format("$0,0.00")
    };
    header = replaceAll( configMap.teamlead_summary_html, map );

    $.each(daily, function(i, item) {
      dailybody += '<tr>'
        + '  <td>' + moment(item.date).format("ddd., MMM. DD") + '</td>'
        + '  <td>' + numeral(item.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + item.teamProf + ' <small>%</small>' + '</td>'
        + '  <td>' + numeral(item.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td> ' + numeral(item.regularPay).format("$0,0.00") + '</td>'
        + '</tr>';
    });

    totalbody += '<tr class="total">'
      + '  <td>' + 'Totals' + '</td>'
      + '  <td>' + numeral(total.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + numeral(total.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + total.teamProf + ' <small>%</small>' + '</td>'
      + '  <td>' + numeral(total.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td> ' + numeral(total.regularPay).format("$0,0.00") + '</td>'
      + '</tr>';

    footer += '  </tbody>'
      + '</table>';

    $.each(otherdaily, function(i, item) {
      otherdailybody += '<tr>'
        + '  <td>' + moment(item.theDate).format("ddd., MMM. DD") + '</td>'
        + '  <td>' + numeral(item.vacationHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.ptoHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.holidayHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '</tr>';
    });

    othertotalbody += '<tr class="total">'
      + '  <td>' + 'Totals' + '</td>'
      + '  <td>' + numeral(total.vacationHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + numeral(total.ptoHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + numeral(total.holidayHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '</tr>';

    otherfooter += '  </tbody>'
      + '</table>';

    map = {
      TEAMFLAGHOURS     : numeral(summary.teamFlagHours).format("0,0.00"),
      TEAMCLOCKHOURS    : numeral(summary.teamClockHours).format("0,0.00"),
      TEAMPROFICIENCY   : numeral(summary.teamProficiency).format("0.0%"),
      YOURCLOCKHOURS    : numeral(summary.clockHours).format("0,0.00"),
      YOURPAYHOURS      : numeral(summary.payHours).format("0,0.00"),
      YOURPAYRATE       : numeral(summary.regularRate).format("$0,0.00"),
      YOURREGULARPAY    : numeral(summary.regularPay).format("$0,0.00"),
      YOUROTHERPAYHOURS : numeral(summary.otherHours).format("0,0.00"),
      YOUROTHERPAYRATE  : numeral(summary.otherRate).format("$0,0.00"),
      YOUROTHERPAY      : numeral(summary.otherPay).format("$0,0.00"),
      YOURGROSSPAY      : numeral(summary.totalGrossPay).format("$0,0.00")
    };
    
    totalpay = replaceAll( configMap.teamlead_totalpay_html, map );

    jqueryMap.$container.html( header + configMap.teamlead_daily_html + dailybody + totalbody + footer + configMap.teamlead_other_html + otherdailybody + othertotalbody + otherfooter + totalpay );
    jqueryMap.teamLeadTotalsButton = jqueryMap.$container.find( '.teamleadtotals' );
    jqueryMap.teamLeadTotalsButton.on( 'click', function() { $.gevent.publish( 'teampay.showteamleadtotals', [] )});
  };

  onErrorTeamLeadData = function( e, errorMsg ) {
    jqueryMap.$container.html( errorMsg );
  };

  onShowManager = function() {
    $.gevent.publish('splash.requestManagerData', []);
  };

  onFulfilledManagerData = function(e, model ) {
    var map, shopheader, teamheader, 
    shopBody = '', shopFooter = '', 
    teamBody = '', teamFooter = '', 
    lastteam;

    map = {
      FROMDATE : moment(model.summary.fromDate).format("MM/DD/YYYY"),
      THRUDATE : moment(model.summary.thruDate).format("MM/DD/YYYY"),
    };
    shopheader = replaceAll( configMap.manager_summary_html, map );

    $.each(model.shopSummary.teamTotals, function(i, item) {
      shopBody += '<tr>'
        + '  <td>' + item.teamName + '</td>'
        + '  <td>' + numeral(item.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + item.teamProf + ' <small>%</small>' + '</td>'
        + '  <td>' + numeral(item.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.regularPay).format("$0,0.00") + '</td>'
        + '</tr>';
    });

    shopFooter += '<tr class="total">'
      + '  <td>Total</td>'
      + '  <td>' + numeral(model.shopSummary.shopTotal.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + numeral(model.shopSummary.shopTotal.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + model.shopSummary.shopTotal.teamProf + ' <small>%</small>' + '</td>'
      + '  <td>' + numeral(model.shopSummary.shopTotal.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + numeral(model.shopSummary.shopTotal.regularPay).format("$0,0.00") + '</td>'
      + '</tr>'
      + '</tbody>'
      + '</table>'
      + '<h3 class="section-start">Team Totals</h3>';

    $.each(model.teamsSummary, function(i, item) {
      map = { TEAMNAME : item.teamTotal.teamName };
      teamBody += replaceAll( configMap.manager_team_detail_html, map );
      $.each(item.employeeTotals, function(j, emp) {
        teamBody += '<tr>'
          + '  <td>' + emp.tech + '</td>'
          + '  <td>' + numeral(emp.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
          + '  <td>' + numeral(emp.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
          + '  <td>' + emp.teamProf + ' <small>%</small>' + '</td>'
          + '  <td>' + numeral(emp.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
          + '  <td>' + numeral(emp.regularPay).format("$0,0.00") + '</td>'
          + '</tr>';
      });
      teamBody += '<tr class="total">'
        + '  <td>Total</td>'
        + '  <td>' + numeral(item.teamTotal.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.teamTotal.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + item.teamTotal.teamProf + ' <small>%</small>' + '</td>'
        + '  <td>' + numeral(item.teamTotal.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.teamTotal.regularPay).format("$0,0.00") + '</td>'
        + '</tr>'
        + '</tbody>'
        + '</table>';
    });
    // teamFooter += '  </tbody>'
    //   + '</table>';
    jqueryMap.$container.html( shopheader + configMap.manager_shop_detail_html + 
      shopBody + shopFooter + teamBody);
  };

  onErrorManagerData = function( e, errorMsg ) {
    jqueryMap.$container.html( errorMsg );
  };

  onShowTeamLeadTotals = function() {
    $.gevent.publish( 'splash.requestTeamLeadTotalsData', [] );
  };

  onFulfilledTeamLeadTotalsData = function(e, model ) {
    var map, shopheader, teamheader, 
    shopBody = '', shopFooter = '', 
    teamBody = '', teamFooter = '', 
    lastteam;

    map = {
      FROMDATE : moment(model.summary.fromDate).format("MM/DD/YYYY"),
      THRUDATE : moment(model.summary.thruDate).format("MM/DD/YYYY"),
    };
    shopheader = replaceAll( configMap.manager_summary_html, map );

    $.each(model.shopSummary.teamTotals, function(i, item) {
      shopBody += '<tr>'
        + '  <td>' + item.teamName + '</td>'
        + '  <td>' + numeral(item.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + item.teamProf + ' <small>%</small>' + '</td>'
        + '  <td>' + numeral(item.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.regularPay).format("$0,0.00") + '</td>'
        + '</tr>';
    });

    shopFooter += '<tr class="total">'
      + '  <td>Total</td>'
      + '  <td>' + numeral(model.shopSummary.shopTotal.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + numeral(model.shopSummary.shopTotal.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + model.shopSummary.shopTotal.teamProf + ' <small>%</small>' + '</td>'
      + '  <td>' + numeral(model.shopSummary.shopTotal.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
      + '  <td>' + numeral(model.shopSummary.shopTotal.regularPay).format("$0,0.00") + '</td>'
      + '</tr>'
      + '</tbody>'
      + '</table>'
      + '<h3 class="section-start">Team Totals</h3>';

    $.each(model.teamsSummary, function(i, item) {
      map = { TEAMNAME : item.teamTotal.teamName };
      teamBody += replaceAll( configMap.manager_team_detail_html, map );
      $.each(item.employeeTotals, function(j, emp) {
        teamBody += '<tr>'
          + '  <td>' + emp.tech + '</td>'
          + '  <td>' + numeral(emp.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
          + '  <td>' + numeral(emp.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
          + '  <td>' + emp.teamProf + ' <small>%</small>' + '</td>'
          + '  <td>' + numeral(emp.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
          + '  <td>' + numeral(emp.regularPay).format("$0,0.00") + '</td>'
          + '</tr>';
      });
      teamBody += '<tr class="total">'
        + '  <td>Total</td>'
        + '  <td>' + numeral(item.teamTotal.flagHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.teamTotal.clockHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + item.teamTotal.teamProf + ' <small>%</small>' + '</td>'
        + '  <td>' + numeral(item.teamTotal.payHours).format("00.00") + ' <small>hrs</small>' + '</td>'
        + '  <td>' + numeral(item.teamTotal.regularPay).format("$0,0.00") + '</td>'
        + '</tr>'
        + '</tbody>'
        + '</table>';
    });
    // teamFooter += '  </tbody>'
    //   + '</table>';
    jqueryMap.$container.html( shopheader + configMap.manager_shop_detail_html + 
      shopBody + shopFooter + teamBody);
  };

  onErrorTeamLeadTotalsData = function( e, errorMsg ) {
    jqueryMap.$container.html( errorMsg );
  };
  //------------------------ END EVENT HANDLERS -------------------------------
 

  //----------------------- BEGIN PUBLIC METHODS ------------------------------

 
  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys
  // Arguments  : A map of settable keys and values
  //  * color_name - color to use
  // Settings   :
  //  * configMap.settable_map declares allowed keys
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    spa.util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/
 

  // Begin public method /initModule/
  // Purpose   : Initializes module
  // Arguments :
  //  *  the jquery element used by this feature
  // Returns   : true
  // Throws    : nonaccidental
  //
  initModule = function ( $container ) {
    stateMap.$container = $container;
    setJqueryMap();

    $.gevent.subscribe( $container, 'teampay.fulfilledTechData',            onFulfilledTechData );
    $.gevent.subscribe( $container, 'teampay.fulfilledTeamLeadData',        onFulfilledTeamLeadData );
    $.gevent.subscribe( $container, 'teampay.fulfilledManagerData',         onFulfilledManagerData );
    $.gevent.subscribe( $container, 'teampay.fulfilledTeamLeadTotalsData',  onFulfilledTeamLeadTotalsData );
    $.gevent.subscribe( $container, 'teampay.errorTechData',                onErrorTechData );
    $.gevent.subscribe( $container, 'teampay.errorManagerData',             onErrorManagerData );
    $.gevent.subscribe( $container, 'teampay.showtechsplash',               onShowTech );
    $.gevent.subscribe( $container, 'teampay.showteamleadsplash',           onShowTeamLead );
    $.gevent.subscribe( $container, 'teampay.showmanagersplash',            onShowManager );
    $.gevent.subscribe( $container, 'teampay.showteamleadtotals',           onShowTeamLeadTotals ); 
//    $.gevent.publish( 'checksession');
    return true;
  };
  // End public method /initModule/
 

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
  };
  
  //----------------------- END PUBLIC METHODS --------------------------------
}());
 
