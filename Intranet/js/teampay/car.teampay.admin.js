/*
* car.mod.teampay.admin.js
* Template for browser feature modules
*/
/*jslint         browser: true,  continue : true,
  devel  : true, indent : 2,     maxerr   : 50,
  newcap : true, nomen  : true,  plusplus : true,
  regexp : true, sloppy : true,  vars     : false,
  white  : true
*/

/*global $, car.mod.teampay.admin.js */

car.teampay.admin = (function () {

  //--------------------- BEGIN MODULE SCOPE VARIABLES ------------------------
  var
    configMap = {
      // When manager is logged in
      admin_mgr_html : String ()
        + '<h2 class="page-header">Admin Settings</h2>'

        + '<h3 class="section-start">Team Settings</h3>  '
        + '<div class="row teams">'
        + '  <div class="col-md-6">'
        + '    <div class="panel panel-default">'
        + '      <div class="panel-heading">'
        + '        <h5>Team: Duckstad</h5>'
        + '      </div>'
        + '      <div class="panel-body">'
        + '        <div class="tech-line">'
        + '          <h4>Ben Dover'
        + '            <span class="iflead">- Lead</span>'
        + '          </h4>'
        + '          <span class="mem-percent">34%</span>'
        + '          <button type="button" class="btn edit edit-sm">'
        + '            <span>Edit</span>'
        + '          </button>'
        + '        </div><!--/.tech-line-->'
        + '        <div class="tech-line">'
        + '          <h4>Ben Dover'
        + '            <span class="iflead">- Lead</span>'
        + '          </h4>'
        + '          <span class="mem-percent">34%</span>'
        + '          <button type="button" class="btn edit edit-sm">'
        + '            <span>Edit</span>'
        + '          </button>'
        + '        </div><!--/.tech-line-->'
        + '        <div class="grosspool-line">'
        + '          <div class="tech-line">'
        + '            <h4>Team Budget'
        + '            </h4>'
        + '            <span class="mem-percent">$100</span>'
        + '            <button type="button" class="btn edit edit-sm">'
        + '              <span>Edit</span>'
        + '            </button>'
        + '          </div><!--/.tech-line-->'
        + '      </div><!--/.grosspool-line-->'
        + '      </div>'
        + '    </div>'
        + '  </div><!--/.col-md-6-->'
        + '</div><!--/.row-->',

      // When team-lead is logged in
      admin_teamlead_html : String ()
        + '<h2 class="page-header">Admin Settings</h2>'

        + '<h3 class="section-start">Your Team Settings</h3>  '
        + '<div class="row teams">'
        + '  <div class="col-md-12">'
        + '    <div class="panel panel-default">'
        + '      <div class="panel-heading">'
        + '        <h5>Team: Duckstad</h5>'
        + '      </div>'
        + '      <div class="panel-body">'
        + '        <div class="tech-line">'
        + '          <h4>Ben Dover'
        + '            <span class="iflead">- Lead</span>'
        + '          </h4>'
        + '          <span class="mem-percent">34%</span>'
        + '          <button type="button" class="btn edit edit-sm">'
        + '            <span>Edit</span>'
        + '          </button>'
        + '        </div><!--/.tech-line-->'
        + '        <div class="tech-line">'
        + '          <h4>Ben Dover'
        + '            <span class="iflead">- Lead</span>'
        + '          </h4>'
        + '          <span class="mem-percent">34%</span>'
        + '          <button type="button" class="btn edit edit-sm">'
        + '            <span>Edit</span>'
        + '          </button>'
        + '        </div><!--/.tech-line-->'
        + '        <div class="grosspool-line">'
        + '          <div class="tech-line">'
        + '            <h4>Team Budget'
        + '            </h4>'
        + '            <span class="mem-percent">$100</span>'
        + '            <button type="button" class="btn edit edit-sm">'
        + '              <span>Edit</span>'
        + '            </button>'
        + '          </div><!--/.tech-line-->'
        + '      </div><!--/.grosspool-line-->'
        + '      </div>'
        + '    </div>'
        + '  </div><!--/.col-md-12-->'
        + '</div><!--/.row-->',

      settable_map : {}
    },
    stateMap = { $container : null },
    jqueryMap = {},
 

    setJqueryMap, configModule, initModule, 
    onShowAdmin;

  //--------------------- END MODULE SCOPE VARIABLES --------------------------
 

  //------------------------ BEGIN UTILITY METHODS ----------------------------
  //------------------------- END UTILITY METHODS -----------------------------
 

  //-------------------------- BEGIN DOM METHODS ------------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;
    jqueryMap = { $container : $container };
  };

  // End DOM method /setJqueryMap/
  //-------------------------- END DOM METHODS --------------------------------
 

  //----------------------- BEGIN EVENT HANDLERS ------------------------------

  onShowAdmin = function() {
    jqueryMap.$container.html( configMap.admin_mgr_html );
  };

  //------------------------ END EVENT HANDLERS -------------------------------
 

  //----------------------- BEGIN PUBLIC METHODS ------------------------------

  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys
  // Arguments  : A map of settable keys and values
  //  * color_name - color to use
  // Settings   :
  //  * configMap.settable_map declares allowed keys
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    spa.util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/
 

  // Begin public method /initModule/
  // Purpose   : Initializes module
  // Arguments :
  //  *  the jquery element used by this feature
  // Returns   : true
  // Throws    : nonaccidental
  //
  initModule = function ( $container ) {
    console.log( 'teampay.admin initModule' );
    stateMap.$container = $container;
    setJqueryMap();
    $.gevent.subscribe( $container, 'teampay.showadmin',  onShowAdmin);
    return true;
  };
  // End public method /initModule/
 

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
  };
  //----------------------- END PUBLIC METHODS --------------------------------
}());
 
