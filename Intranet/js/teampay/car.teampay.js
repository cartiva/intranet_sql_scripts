/*
 * car.teampay.js
 * Namespace module for TeamPay
 *
 * Augments left nav bar to access functionality
*/

/*jslint           browser : true,   continue : true,
  devel  : true,    indent : 2,       maxerr  : 50,
  newcap : true,     nomen : true,   plusplus : true,
  regexp : true,    sloppy : true,       vars : false,
  white  : true
*/
/*global $, spa */

car.teampay = (function () {
  'use strict';

  var
    configMap = {
      // Name of 'App' to the right of the Intranet logo
      app_name : String()
        + '<a class="navbar-brand" href="#">'
        + '  <span>Something clever that we can change later</span>'
        + '</a>',

      // Tech Navigation
      mainlink_tp_tech_html : String()  
        + '<li class="mainlink">'
        + '  <a class="techsplash" href="#">'
        + '    <span class="glyphicon glyphicon-signal"></span>Team Pay'
        + '    <span class="arrow-right"></span>'
        + '  </a>'
        + '</li>',

      sublink_tp_tech_ros_html : String()
        + '    <li class="sublink techros">'
        + '      <a href="#">Pay Period ROs</a>'
        + '    </li>',

      sublink_tp_tech_hpw_html : String()
        + '    <li class="sublink techhpw">'
        + '      <a href="#">How Plan Works</a>'
        + '    </li>',


      // Team Lead Navigation
      mainlink_tp_teamlead_html : String()
        + '<li class="mainlink">'
        + '  <a class="teamleadsplash" href="#">'
        + '    <span class="glyphicon glyphicon-signal"></span>Team Pay'
        + '    <span class="arrow-right"></span>'
        + '  </a>'
        + '</li>',

      sublink_tp_teamlead_admin_html : String ()
        + '    <li class="sublink admin">'
        + '      <a href="#">Admin</a>'
        + '    </li>',

      sublink_tp_teamlead_teamtotals_html : String ()
        + '    <li class="sublink teamleadtotals">'
        + '      <a href="#">Store / Team Totals</a>'
        + '    </li>',

      sublink_tp_teamlead_ros_html : String ()
        + '    <li class="sublink teamleadros">'
        + '      <a href="#">Pay Period ROs</a>'
        + '    </li>',

      sublink_tp_teamlead_hpw_html : String ()
        + '    <li class="sublink mgrhpw">'
        + '      <a href="#">How Plan Works</a>'
        + '    </li>',

      // Manager Navigation
      mainlink_tp_mgr_html : String()
        + '<li class="mainlink">'
        + '  <a class="mgrsplash" href="#">'
        + '    <span class="glyphicon glyphicon-signal"></span>Team Pay'
        + '    <span class="arrow-right"></span>'
        + '  </a>'
        + '</li>',

      sublink_tp_mgr_admin_html : String ()
        + '    <li class="sublink admin">'
        + '      <a href="#">Admin</a>'
        + '    </li>',

      sublink_tp_mgr_ros_html : String ()
        + '    <li class="sublink mgrros">'
        + '      <a href="#">Pay Period ROs</a>'
        + '    </li>',

      sublink_tp_mgr_hpw_html : String ()
        + '    <li class="sublink mgrhpw">'
        + '      <a href="#">How Plan Works</a>'
        + '    </li>',

      settable_map : {}
    },
    stateMap = { $container : null },
    jqueryMap = {},
 
    setJqueryMap,       configModule,          initModule,
    onLoadNavBar,       onInitModules,
    onTechSplashClick,  onTeamLeadSplashClick, onMgrSplashClick,
    onAdminClick,       onTechHPWClick,        onMgrHPWClick,
    onPayCalcClick, 
    onTechROsClick,     onTeamLeadROsClick,    onManagerROsClick,
    onTeamLeadTotalsClick
  ;

  //--------------------- END MODULE SCOPE VARIABLES --------------------------
  //-------------------------- BEGIN DOM METHODS ------------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;
    jqueryMap = { $container : $container };
  };

  onLoadNavBar = function( e, appNav ) {
    var navItems = '';

    jqueryMap.$navBar = jqueryMap.$container.find('.app-tp');
    $.each(appNav, function(i, item) {
      if ( item.appCode == 'tp' && item.navType == 'leftnavbar' ) {
        navItems += configMap[item.configMapMarkupKey];
      };
    });
    jqueryMap.$navBar.append( navItems );
    jqueryMap.$techSplash      = $( '.techsplash' );
    jqueryMap.$teamLeadSplash  = $( '.teamleadsplash' );
    jqueryMap.$mgrSplash       = $( '.mgrsplash' );
    jqueryMap.$teamLeadTotals  = $( '.teamleadtotals' );
    jqueryMap.$admin           = $( '.admin' );
    jqueryMap.$techhpw         = $( '.techhpw' );
    jqueryMap.$mgrhpw          = $( '.mgrhpw' );
    jqueryMap.$paycalc         = $( '.paycalc' );
    jqueryMap.$techROs         = $( '.techros' );
    jqueryMap.$teamLeadROs     = $( '.teamleadros' );
    jqueryMap.$managerROs      = $( '.mgrros' );

    jqueryMap.$techSplash.on(     'click', onTechSplashClick );
    jqueryMap.$teamLeadSplash.on( 'click', onTeamLeadSplashClick );
    jqueryMap.$mgrSplash.on(      'click', onMgrSplashClick );
    jqueryMap.$teamLeadTotals.on( 'click', onTeamLeadTotalsClick );
    jqueryMap.$admin.on(          'click', onAdminClick );
    jqueryMap.$techhpw.on(        'click', onTechHPWClick );
    jqueryMap.$mgrhpw.on(         'click', onMgrHPWClick );
    jqueryMap.$paycalc.on(        'click', onPayCalcClick );
    jqueryMap.$techROs.on(        'click', onTechROsClick );
    jqueryMap.$teamLeadROs.on(    'click', onTeamLeadROsClick );
    jqueryMap.$managerROs.on(     'click', onManagerROsClick );
  };

  onInitModules = function() {
    car.teampay.data.initModule ( jqueryMap.$container );
    car.teampay.splash.initModule( $( '.page-content-main' ) );
    car.teampay.admin.initModule(  $( '.page-content-main' ) );
    car.teampay.hpw.initModule(    $( '.page-content-main' ) );
    car.teampay.ros.initModule(    $( '.page-content-main' ) );
  }

  // End DOM method /setJqueryMap/
  //-------------------------------- END DOM METHODS ------------------------------

  //----------------------------- Begin Event Handlers ----------------------------
  
  onTechSplashClick = function() {     $.gevent.publish( 'teampay.showtechsplash' ); };

  onTeamLeadSplashClick = function() { $.gevent.publish( 'teampay.showteamleadsplash' ); };

  onMgrSplashClick = function() {      $.gevent.publish( 'teampay.showmanagersplash' ); };

  onTeamLeadTotalsClick = function() { $.gevent.publish( 'teampay.showteamleadtotals' ); };

  onAdminClick = function() {          $.gevent.publish( 'teampay.showadmin' ); };

  onTechHPWClick = function() {	       $.gevent.publish( 'teampay.showtechhpw' ); };

  onMgrHPWClick = function() {         $.gevent.publish( 'teampay.showmanagerhpw' ); };

  onPayCalcClick = function() {	       $.gevent.publish( 'teampay.showpaycalc' ); };

  onTechROsClick = function() {	       $.gevent.publish( 'teampay.showtechros' ); };

  onTeamLeadROsClick = function() {    $.gevent.publish( 'teampay.showteamleadros' ); };

  onManagerROsClick = function() {     $.gevent.publish( 'teampay.showmanagerros' ); };

  //------------------------------ End Event Handlers -----------------------------

  // Begin public method /initModule/
  // Purpose   : Initializes module
  // Arguments :
  //  *  the jquery element used by this feature
  // Returns   : true
  // Throws    : nonaccidental
  //
  initModule = function ( $container ) {
    console.log( 'car.teampay initModule' );
    stateMap.$container = $container;
    setJqueryMap();
    $.gevent.subscribe( $container, 'shell.initmodules', onInitModules );
    $.gevent.subscribe( $container, 'shell.apploadnavbar', onLoadNavBar );
    //    car.teampay.initModule( $('.sidebar-widgets') );
    return true;
  };
  // End public method /initModule/

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
  };
}());
