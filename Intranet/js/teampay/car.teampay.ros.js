/*
* car.mod.teampay.ros.js
* Template for browser feature modules
*/
/*jslint         browser: true,  continue : true,
  devel  : true, indent : 2,     maxerr   : 50,
  newcap : true, nomen  : true,  plusplus : true,
  regexp : true, sloppy : true,  vars     : false,
  white  : true
*/

/*global $, car.mod.teampay.ros */

car.teampay.ros = (function () {

  //--------------------- BEGIN MODULE SCOPE VARIABLES ------------------------
  var
    configMap = {
      
      // Flag Time Adjustments
      flag_time_adjustments_html : String ()
        + '<h2 class="page-header">Your ROs</h2>'
        + '<h4><small>for the pay-period '
        + '  <span id="fromDate">FROMDATE</span> - '
        + '  <span id="thruDate">THRUDATE</span>'
        + '</small></h4>'
        + '<h3 class="section-start">Flag Time Adjustments</h3>'
        + '<table class="splash table table-hover table-striped'
        + '  table-condensed">'
        + '  <thead>'
        + '    <tr id="headings"> '
        + '      <th><span class="arrow"></span>Adjustment Date</th>'
        + '      <th><span class="arrow"></span>RO</th>'
        + '      <th><span class="arrow"></span>Type</th>'
        + '      <th><span class="arrow"></span>Flag Hours</th>'
        + '    </tr>'
        + '  </thead>'
        + '  <tbody>',

      //Booked ROs tables by date
      ros_booked_html : String ()
        + '<h3 class="section-start">Booked ROs</h3>'
        + '<table class="splash table table-hover table-striped'
        + '  table-condensed">'
        + '  <thead>'
        + '    <tr id="headings"> '
        + '      <th><span class="arrow"></span>Date Booked</th>'
        + '      <th><span class="arrow"></span>RO</th>'
        + '      <th><span class="arrow"></span>Line</th>'
        + '      <th><span class="arrow"></span>Flag Hours</th>'
        + '    </tr>'
        + '  </thead>'
        + '  <tbody>',

      settable_map : {}
    },
    stateMap = { $container : null },
    jqueryMap = {},
 
    setJqueryMap, configModule, initModule, 
    onShowFlagTimeAdjustments,  onFulfilledFlagTimeAdjustments,
    onShowBookedROs,            onFulfilledBookedROs,
    onShowTechROs,              onFulfilledTechROs,
    onShowTeamLeadROs,          onFulfilledTeamLeadROs,
    onShowManagerROs,           onFulfilledManagerROs
  ;
    
  //--------------------- END MODULE SCOPE VARIABLES --------------------------
 

  //------------------------ BEGIN UTILITY METHODS ----------------------------
  //------------------------- END UTILITY METHODS -----------------------------
 

  //-------------------------- BEGIN DOM METHODS ------------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;
    jqueryMap = { $container : $container };
  };

  // End DOM method /setJqueryMap/
  //-------------------------- END DOM METHODS --------------------------------

  //----------------------- BEGIN EVENT HANDLERS ------------------------------

  onShowTechROs = function ( e, techID ) {
    $.gevent.publish( 'teampay.requestTechROs', [ techID ] );
  };

  onShowTeamLeadROs = function ( e, teamLeadID ) {
    $.gevent.publish( 'teampay.requestTeamLeadROs', [ teamLeadID ] );
  };

  onShowManagerROs = function ( e, managerID ) {
    $.gevent.publish( 'teampay.requestManagerROs', [ managerID ] );
  };

  onFulfilledTechROs = function( e, adjustments, booked ) {
    var adjustmentscontent = '',
      bookedcontent = '';

    $.each(adjustments, function(i, item) {
      adjustmentscontent += '<tr>'
      + '  <td>' + item.adjustmentDate + '</td>'
      + '  <td>' + item.ro + '</td>'
      + '  <td>' + item.type + '</td>'
      + '  <td>' + item.flagHours + '</td>'
      + '</tr>';
    });
    adjustmentscontent += '</tbody>'
    + '</table>';

    $.each(booked, function(i, item) {
      bookedcontent += '<tr>'
      + '  <td>' + item.flagDate + '</td>'
      + '  <td>' + item.ro + '</td>'
      + '  <td>' + item.line + '</td>'
      + '  <td>' + item.flagHours + '</td>'
      + '</tr>';
    });
    bookedcontent += '</tbody>'
    + '</table>';

    jqueryMap.$container.empty().append( configMap.flag_time_adjustments_html + adjustmentscontent + 
      configMap.ros_booked_html + bookedcontent);
  };
 
  onFulfilledTeamLeadROs = function( e, adjustments, booked ) {
    var adjustmentscontent = '',
      bookedcontent = '';

    $.each(adjustments, function(i, item) {
      adjustmentscontent += '<tr>'
      + '  <td>' + item.adjustmentDate + '</td>'
      + '  <td>' + item.ro + '</td>'
      + '  <td>' + item.type + '</td>'
      + '  <td>' + item.flagHours + '</td>'
      + '</tr>';
    });
    adjustmentscontent += '</tbody>'
    + '</table>';

    $.each(booked, function(i, item) {
      bookedcontent += '<tr>'
      + '  <td>' + item.flagDate + '</td>'
      + '  <td>' + item.ro + '</td>'
      + '  <td>' + item.line + '</td>'
      + '  <td>' + item.flagHours + '</td>'
      + '</tr>';
    });
    bookedcontent += '</tbody>'
    + '</table>';

    jqueryMap.$container.empty().append( configMap.flag_time_adjustments_html + adjustmentscontent + 
      configMap.ros_booked_html + bookedcontent);
  };
 
  onFulfilledManagerROs = function( e, adjustments, booked ) {
    var adjustmentscontent = '',
      bookedcontent = '';

    $.each(adjustments, function(i, item) {
      adjustmentscontent += '<tr>'
      + '  <td>' + item.adjustmentDate + '</td>'
      + '  <td>' + item.ro + '</td>'
      + '  <td>' + item.type + '</td>'
      + '  <td>' + item.flagHours + '</td>'
      + '</tr>';
    });
    adjustmentscontent += '</tbody>'
    + '</table>';

    $.each(booked, function(i, item) {
      bookedcontent += '<tr>'
      + '  <td>' + item.bookedDate + '</td>'
      + '  <td>' + item.ro + '</td>'
      + '  <td>' + '' + '</td>'
      + '  <td>' + item.flagHours + '</td>'
      + '</tr>';
    });
    bookedcontent += '</tbody>'
    + '</table>';

    jqueryMap.$container.empty().append( configMap.flag_time_adjustments_html + adjustmentscontent + 
      configMap.ros_booked_html + bookedcontent);
  };
 
  //------------------------ END EVENT HANDLERS -------------------------------
 

  //----------------------- BEGIN PUBLIC METHODS ------------------------------
  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys
  // Arguments  : A map of settable keys and values
  //  * color_name - color to use
  // Settings   :
  //  * configMap.settable_map declares allowed keys
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    spa.util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/
 

  // Begin public method /initModule/
  // Purpose   : Initializes module
  // Arguments :
  //  *  the jquery element used by this feature
  // Returns   : true
  // Throws    : nonaccidental
  //
  initModule = function ( $container ) {
    console.log('ros initModule');
    stateMap.$container = $container;
    setJqueryMap();
    $.gevent.subscribe( $container, 'teampay.showtechros',          onShowTechROs );
    $.gevent.subscribe( $container, 'teampay.showteamleadros',      onShowTeamLeadROs );
    $.gevent.subscribe( $container, 'teampay.showmanagerros',       onShowManagerROs );
    $.gevent.subscribe( $container, 'teampay.fulfilledTechROs',     onFulfilledTechROs );
    $.gevent.subscribe( $container, 'teampay.fulfilledTeamLeadROs', onFulfilledTeamLeadROs );
    $.gevent.subscribe( $container, 'teampay.fulfilledManagerROs',  onFulfilledManagerROs );
    return true;
  };
  // End public method /initModule/
 

  // return public methods
  return {
    configModule          : configModule,
    initModule            : initModule
  };
  //----------------------- END PUBLIC METHODS --------------------------------
}());
 
