// Module /car.teampay.data/
// Provides database access 
//
car.teampay.data = (function() {
  'use strict';

  //------------------------- Begin Module scope variables -------------------------
  var
    // Set constants
    configMap = {
      // Error message for bad data returned
      data_error_html : String ()
        + '<div class="alert alert-warning">'
        + '  <h4>Error</h4>'
        + '  <p>There was an error returning some data. See Jon to '
        + '    bitch him out.</p>'
        + '</div>'

        + '<div class="clear">'
        + '</div>',

      settable_map               : { baseURL: true },
      baseURL                    : './php/data.php',
      techSummaryURL             : '/techsummary',
      techDetailsURL             : '/techdetails',
      techOtherDetailsURL        : '/techotherdetails',
      techOtherTotalsURL         : '/techothertotals',
      techTotalsURL              : '/techtotals',
      teamLeadSummaryURL         : '/techsummary',
      teamLeadDetailsURL         : '/techdetails',
      teamLeadTotalsURL          : '/techtotals',
      managerSummaryURL          : '/managersummary',
      managerShopTotalsURL       : '/shoptotals',
      managerTeamTotalsURL       : '/teamtotals',
      managerEmployeeTotalsURL   : '/employeetotals',
      teamLeadTotalsSummaryURL   : '/managersummary',
      teamLeadShopTotalsURL      : '/shoptotals',
      teamLeadTeamTotalsURL      : '/teamtotals',
      teamLeadEmployeeTotalsURL  : '/employeetotals',
      openROsURL                 : '/openros',
      techDailyFlaggedROsURL     : '/dailyflaggedros',
      teamLeadDailyFlaggedROsURL : '/dailyflaggedros',
      managerDailyFlaggedROsURL  : '/dailyflaggedros',
      adjustmentsURL             : '/adjustments',
      closedROsURL               : '/closedros',
      techPayCalcURL             : '/techpaycalc'
    },
    // Declare all other module scope variables
    stateMap = { 
      $container : null
    },

    configModule,         
    initModule,
    getBaseURL,
    ajaxGet,
    ajaxPost,
    ajaxDelete,
    isJSON,
    onRequestTechData,            processTechData,             errorTechData,
    onRequestTeamLeadData,        processTeamLeadData,         errorTeamLeadData,
    onRequestManagerData,         processManagerData,          errorManagerData,
    onRequestTeamLeadTotalsData,  processTeamLeadTotalsData,   errorTeamLeadTotalsData,
    onRequestTechROs,             processTechROs,              errorTechROs,
    onRequestTeamLeadROs,         processTeamLeadROs,          errorTeamLeadROs,
    onRequestManagerROs,          processManagerROs,           errorManagerROs,
    onRequestTechPayCalc,         processTechPayCalc
  ;
  //--------------------------- End Module Scope Variables ------------------------

  //----------------------------- Begin Utility Methods ---------------------------
  ajaxGet = function(url, callback) {
    return $.ajax({ 
      type     : "GET",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      success  : callback });
  }

  ajaxPost = function(url, postData, callback) {
    return $.ajax({ 
      type     : "POST",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      data     : postData,
      success  : callback });
  }

  ajaxDelete = function(url, deleteData, callback) {
    return $.ajax({ 
      type     : "DELETE",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      data     : deleteData,
      success  : callback });
  }

  //------------------------------ End Utility Methods ----------------------------

  //------------------------------- Begin DOM Methods -----------------------------
  //-------------------------------- End DOM Methods ------------------------------

  //----------------------------- Begin Event Handlers ----------------------------
  
  onRequestTechData = function( e, techID ) {
    var summary, details, totals, otherDetails, otherTotals, suffix;

    suffix = techID ? '/' + techID : '';

    summary      = ajaxGet( configMap.baseURL + configMap.techSummaryURL      + suffix );
    details      = ajaxGet( configMap.baseURL + configMap.techDetailsURL      + suffix );
    totals       = ajaxGet( configMap.baseURL + configMap.techTotalsURL       + suffix );
    otherDetails = ajaxGet( configMap.baseURL + configMap.techOtherDetailsURL + suffix );
    otherTotals  = ajaxGet( configMap.baseURL + configMap.techOtherTotalsURL  + suffix );

    $.when( summary, details, totals, otherDetails, otherTotals ).then( processTechData, errorTechData );
  };

  onRequestTeamLeadData = function( e, teamLeadID ) {
    var summary, details, totals, otherDetails, otherTotals, suffix;

    suffix = teamLeadID ? '/' + teamLeadID : '';
    console.log('onRequestTeamLeadData');
    summary      = ajaxGet( configMap.baseURL + configMap.techSummaryURL      + suffix );
    details      = ajaxGet( configMap.baseURL + configMap.techDetailsURL      + suffix );
    totals       = ajaxGet( configMap.baseURL + configMap.techTotalsURL       + suffix );
    otherDetails = ajaxGet( configMap.baseURL + configMap.techOtherDetailsURL + suffix );
    otherTotals  = ajaxGet( configMap.baseURL + configMap.techOtherTotalsURL  + suffix );

    $.when( summary, details, totals, otherDetails, otherTotals ).then( processTeamLeadData, errorTeamLeadData );
  };

  onRequestManagerData = function( e, managerID ) {
    var summary, shopTotals, teamTotals, employeeTotals, suffix;

    suffix = managerID ? '/' + managerID : '';

    summary         = ajaxGet( configMap.baseURL + configMap.managerSummaryURL         + suffix );
    shopTotals      = ajaxGet( configMap.baseURL + configMap.managerShopTotalsURL      + suffix );
    teamTotals      = ajaxGet( configMap.baseURL + configMap.managerTeamTotalsURL      + suffix );
    employeeTotals  = ajaxGet( configMap.baseURL + configMap.managerEmployeeTotalsURL  + suffix );

    $.when( summary, shopTotals, teamTotals, employeeTotals ).then( processManagerData, errorManagerData );
  };

  processTechData = function ( summaryjqXHR, detailsjqXHR, totalsjqXHR, otherDetailsjqXHR, otherTotalsjqXHR ) {
    var summary, details, totals, otherDetails, otherTotals;

    summary      =  summaryjqXHR[0];
    details      =  detailsjqXHR[0];
    totals       =  totalsjqXHR[0];
    otherDetails =  otherDetailsjqXHR[0];
    otherTotals  =  otherTotalsjqXHR[0];

    $.gevent.publish( 'teampay.fulfilledTechData', [ summary[0], details, totals[0], otherDetails, otherTotals[0] ] );
  }

  processTeamLeadData = function ( summaryjqXHR, detailsjqXHR, totalsjqXHR, otherDetailsjqXHR, otherTotalsjqXHR ) {
    var summary, details, totals, otherDetails, otherTotals;
    console.log('processTeamLeadData');
    summary      =  summaryjqXHR[0];
    details      =  detailsjqXHR[0];
    totals       =  totalsjqXHR[0];
    otherDetails =  otherDetailsjqXHR[0];
    otherTotals  =  otherTotalsjqXHR[0];

    $.gevent.publish( 'teampay.fulfilledTeamLeadData', [ summary[0], details, totals[0], otherDetails, otherTotals[0] ] );
  }

  processManagerData = function( summaryjqXHR, shoptotalsjqXHR, teamtotalsjqXHR, employeetotalsjqXHR ) {
    var summary, shopTotals, teamTotals, employeeTotals, allTeamsArray, teamArray, teamTotal,
      lastTeam, managerSplashModel;

    // Model
    //
    // managerSplashModel : {
    //   summary : {
    //     fromDate : xx/xx/xx,
    //     thruDate : xx/xx/xx
    //   },
    //
    //   shopSummary : {
    //     teamTotals : [{ }],
    //     shopTotal  : {}
    //   },
    //
    //   teamsSummary : [{
    //     employeeTotals : [{}],
    //     teamTotal      : {}
    //   }]
    // }
    
    summary         = summaryjqXHR[0];     
    shopTotals      = shoptotalsjqXHR[0];   
    teamTotals      = teamtotalsjqXHR[0];   
    employeeTotals  = employeetotalsjqXHR[0];

    allTeamsArray = [];
    teamArray = [];
    lastTeam = '**1st**';
    $.each(employeeTotals, function(i, item) {
      if ( item.teamName != lastTeam ) {            // New team
        if ( lastTeam != '**1st**' ) {              // Add Totals to last team array
          $.each( teamTotals, function(j, team) {   // Find the team total for the team we're building
            if ( team.teamName == lastTeam ) {
              teamTotal = team;
            };
          });
          allTeamsArray.push( { employeeTotals : teamArray, teamTotal : teamTotal } );
          teamArray = [];
        }
        lastTeam = item.teamName;
      }
      teamArray.push( item );
    });
    $.each( teamTotals, function(j, team) {   // Find the team total for the team we're building
      if ( team.teamName == lastTeam ) {
        teamTotal = team;
      };
    });
    allTeamsArray.push( { employeeTotals : teamArray, teamTotal : teamTotal } );
    
    managerSplashModel = {
      summary : summary[0],
      shopSummary : {
        teamTotals : teamTotals,
        shopTotal  : shopTotals[0]
      },
      teamsSummary : allTeamsArray
    };

    $.gevent.publish( 'teampay.fulfilledManagerData', [ managerSplashModel ] );
  };

  errorTechData = function() {
    $.gevent.publish( 'teampay.errorTechData', [ configMap.data_error_html ] );
  };

  errorManagerData = function() {
    $.gevent.publish( 'teampay.errorManagerData', [ configMap.data_error_html ] );
  };

  onRequestTechROs = function( e, techID ) {
    var suffix, adjustments, closed;

    if (techID) {
      suffix = '/' + techID;
    } else {
      suffix = '';
    };

    adjustments = ajaxGet( configMap.baseURL + configMap.adjustmentsURL + suffix );
    closed      = ajaxGet( configMap.baseURL + configMap.techDailyFlaggedROsURL + suffix );
    $.when( adjustments, closed ).then( processTechROs, errorTechROs );
  };

  onRequestTeamLeadROs = function( e, teamLeadID ) {
    var suffix, adjustments, closed;

    if (teamLeadID) {
      suffix = '/' + teamLeadID;
    } else {
      suffix = '';
    };

    adjustments = ajaxGet( configMap.baseURL + configMap.adjustmentsURL + suffix );
    closed      = ajaxGet( configMap.baseURL + configMap.teamLeadDailyFlaggedROsURL + suffix );
    $.when( adjustments, closed ).then( processTeamLeadROs, errorTeamLeadROs );
  };

  onRequestManagerROs = function( e, managerID ) {
    var suffix, adjustments, closed;

    if (managerID) {
      suffix = '/' + managerID;
    } else {
      suffix = '';
    };

    adjustments = ajaxGet( configMap.baseURL + configMap.adjustmentsURL + suffix );
    closed      = ajaxGet( configMap.baseURL + configMap.managerDailyFlaggedROsURL + suffix );
    $.when( adjustments, closed ).done( processManagerROs, errorManagerROs );
  };

  processTechROs = function ( adjustmentsjqXHR, closedjqXHR ) {
    var adjustments, closed;
    adjustments = adjustmentsjqXHR[0];
    closed      = closedjqXHR[0];

    $.gevent.publish( 'teampay.fulfilledTechROs', [ adjustments, closed ] );
  };

  processTeamLeadROs = function ( adjustmentsjqXHR, closedjqXHR ) {
    var adjustments, closed;
    adjustments = adjustmentsjqXHR[0];
    closed      = closedjqXHR[0];

    $.gevent.publish( 'teampay.fulfilledTeamLeadROs', [ adjustments, closed ] );
  };

  processManagerROs = function ( adjustmentsjqXHR, closedjqXHR ) {
    var adjustments, closed;
    adjustments = adjustmentsjqXHR[0];
    closed      = closedjqXHR[0];

    $.gevent.publish( 'teampay.fulfilledManagerROs', [ adjustments, closed ] );
  };

  onRequestTechPayCalc = function( ) {
    var techpaycalc;

    techpaycalc = ajaxGet( configMap.baseURL + configMap.techPayCalcURL );
    techpaycalc.done( processTechPayCalc );
  };

  processTechPayCalc = function ( techPayCalcData ) {
    
    $.gevent.publish( 'hpw.fulfilledTechPayCalc', [ techPayCalcData[0] ] );
  };

  onRequestTeamLeadTotalsData = function( e, teamLeadID ) {
    var summary, shopTotals, teamTotals, employeeTotals, suffix;

    suffix = teamLeadID ? '/' + teamLeadID : '';

    summary         = ajaxGet( configMap.baseURL + configMap.teamLeadTotalsSummaryURL   + suffix );
    shopTotals      = ajaxGet( configMap.baseURL + configMap.teamLeadShopTotalsURL      + suffix );
    teamTotals      = ajaxGet( configMap.baseURL + configMap.teamLeadTeamTotalsURL      + suffix );
    employeeTotals  = ajaxGet( configMap.baseURL + configMap.teamLeadEmployeeTotalsURL  + suffix );

    $.when( summary, shopTotals, teamTotals, employeeTotals ).then( processTeamLeadTotalsData, errorTeamLeadTotalsData );
  };

  processTeamLeadTotalsData = function( summaryjqXHR, shoptotalsjqXHR, teamtotalsjqXHR, employeetotalsjqXHR ) {
    var summary, shopTotals, teamTotals, employeeTotals, allTeamsArray, teamArray, teamTotal,
      lastTeam, teamLeadSplashModel;

    // Model
    //
    // teadLeaderSplashModel : {
    //   summary : {
    //     fromDate : xx/xx/xx,
    //     thruDate : xx/xx/xx
    //   },
    //
    //   shopSummary : {
    //     teamTotals : [{ }],
    //     shopTotal  : {}
    //   },
    //
    //   teamsSummary : [{
    //     employeeTotals : [{}],
    //     teamTotal      : {}
    //   }]
    // }
    
    summary         = summaryjqXHR[0];     
    shopTotals      = shoptotalsjqXHR[0];   
    teamTotals      = teamtotalsjqXHR[0];   
    employeeTotals  = employeetotalsjqXHR[0];

    allTeamsArray = [];
    teamArray = [];
    lastTeam = '**1st**';
    $.each(employeeTotals, function(i, item) {
      if ( item.teamName != lastTeam ) {            // New team
        if ( lastTeam != '**1st**' ) {              // Add Totals to last team array
          $.each( teamTotals, function(j, team) {   // Find the team total for the team we're building
            if ( team.teamName == lastTeam ) {
              teamTotal = team;
            };
          });
          allTeamsArray.push( { employeeTotals : teamArray, teamTotal : teamTotal } );
          teamArray = [];
        }
        lastTeam = item.teamName;
      }
      teamArray.push( item );
    });
    $.each( teamTotals, function(j, team) {   // Find the team total for the team we're building
      if ( team.teamName == lastTeam ) {
        teamTotal = team;
      };
    });
    allTeamsArray.push( { employeeTotals : teamArray, teamTotal : teamTotal } );
    
    teamLeadSplashModel = {
      summary : summary[0],
      shopSummary : {
        teamTotals : teamTotals,
        shopTotal  : shopTotals[0]
      },
      teamsSummary : allTeamsArray
    };

    $.gevent.publish( 'teampay.fulfilledTeamLeadTotalsData', [ teamLeadSplashModel ] );
  };

  errorTeamLeadTotalsData = function() {
    $.gevent.publish( 'teampay.errorTeamLeadTotalsData', [ configMap.data_error_html ] );
  };

  //------------------------------ End Event Handlers -----------------------------

  //----------------------------- Begin Public Methods ----------------------------
  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys {key : value}
  // Arguments  : A map of settable keys and values
  //   * color_name - color to use
  // Settings   : 
  //   * configMap.settable_map declares what keys are allowed to be set
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/

  // Begin public method /initModule/
  // Purpose    : Initializes module
  // Arguments  : 
  //   * $container the jquery element used by this feature
  // Returns    : true
  // Throws     : none
  //
  initModule = function ( $container ) {
    console.log('data initModule');
    $.gevent.subscribe( $container, 'splash.requestTechData',           onRequestTechData );
    $.gevent.subscribe( $container, 'splash.requestTeamLeadData',       onRequestTeamLeadData );
    $.gevent.subscribe( $container, 'splash.requestManagerData',        onRequestManagerData );
    $.gevent.subscribe( $container, 'teampay.requestTechROs',           onRequestTechROs );
    $.gevent.subscribe( $container, 'teampay.requestTeamLeadROs',       onRequestTeamLeadROs );
    $.gevent.subscribe( $container, 'teampay.requestManagerROs',        onRequestManagerROs );
    $.gevent.subscribe( $container, 'hpw.requestTechPayCalc',           onRequestTechPayCalc );
    $.gevent.subscribe( $container, 'splash.requestTeamLeadTotalsData', onRequestTeamLeadTotalsData );
    return true;
  };

  getBaseURL = function() {
    return configMap.baseURL;
  }
  // End public method /initModule/

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule,
    getBaseURL   : getBaseURL
  };
  //------------------------------ End Public Methods -----------------------------
}());
