/*
* car.mod.teampay.hpw.js
* Template for browser feature modules
*/
/*jslint         browser: true,  continue : true,
  devel  : true, indent : 2,     maxerr   : 50,
  newcap : true, nomen  : true,  plusplus : true,
  regexp : true, sloppy : true,  vars     : false,
  white  : true
*/

/*global $, car.mod.teampay.hpw */

car.teampay.hpw = (function () {

  //--------------------- BEGIN MODULE SCOPE VARIABLES ------------------------
  var
    configMap = {
      // When tech is logged in
      hpw_tech_html : String ()
        + '<h2 class="page-header">How Team Pay Works</h2>'
        + '<h3 class="section-start">Terminology</h3>'
        + '<dl>'
        + '  <dt>Flag Hours</dt>'
        + '    <dd>Total number of labor hours billed</dd>'
        + '  <dt>Clock Hours</dt>'
        + '    <dd>Total number of hours clocked in</dd>'
        + '  <dt>Pay Hours</dt>'
        + '    <dd>Clock Hours times Team Proficiency</dd>'
        + '  <dt>Team Proficiency</dt>'
        + '    <dd>Total Team Flag Hours divided by Total Team clock Hours</dd>'
        + '  <dt>Regular Pay</dt>'
        + '    <dd>Your Pay Hours times your Current Pay Rate</dd>'
        + '  <dt>ELR (Effective Labor Rate)</dt>'
        + '    <dd>Labor Sales divided by Flag Hours</dd>'
        + '  <dt>Budget</dt>'
        + '    <dd>Total dollars per pay hour the Manager/Team Lead have to divide'
        + '      amongst the team members</dd>'
        + '</dl>'

        + '  <h3 class="section-start">Team Budgeting</h3>'
        + '  <p>The budget is the total dollars per pay hour the Team '
        + '    Leader and Service Manager have to divide amongst the '
        + '    team members.</p> '
        + '  <p>The Team Leader and Service Manager will assign a percentage of the'
        + '     budget to each team member. The percentage will be determined'
        + "    by the tech's skill set, tech's capacity, "
        + '    and overall proficiency.</p>'

        + '  <div class="panel panel-default">'
        + '    <div class="panel-heading">'
        + '      <h4>Example Team Breakdown</h4>'
        + '    </div>'
        + '    <div class="panel-body table-responsive">'
        + '      <table class="calc table">'
        + '        <tr class="captions">'
        + '          <td>Team Member</td>'
        + '          <td><i>|</i></td>'
        + '          <td>Budget</td>'
        + '          <td><i>x</i></td>'
        + '          <td>Team member % of budget</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Current pay rate</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>1 - 31%</td>'
        + '          <td><small>|</small></td>'
        + '          <td><sup>$</sup>70.86</td>'
        + '          <td><small>x</small></td>'
        + '          <td>31<sup>%</sup></td>'
        + '          <td><small>=</small></td>'
        + '          <td><sup>$</sup>21.97</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>2 - 28%</td>'
        + '          <td><small>|</small></td>'
        + '          <td><sup>$</sup>70.86</td>'
        + '          <td><small>x</small></td>'
        + '          <td>28<sup>%</sup></td>'
        + '          <td><small>=</small></td>'
        + '          <td><sup>$</sup>19.84</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>3 - 23%</td>'
        + '          <td><small>|</small></td>'
        + '          <td><sup>$</sup>70.86</td>'
        + '          <td><small>x</small></td>'
        + '          <td>23<sup>%</sup></td>'
        + '          <td><small>=</small></td>'
        + '          <td><sup>$</sup>16.30</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>4 - 18%</td>'
        + '          <td><small>|</small></td>'
        + '          <td><sup>$</sup>70.86</td>'
        + '          <td><small>x</small></td>'
        + '          <td>18<sup>%</sup></td>'
        + '          <td><small>=</small></td>'
        + '          <td><sup>$</sup>12.75</td>'
        + '        </tr>'
        + '      </table>'
        + '    </div><!--/.panel-body-->'
        + '  </div><!--/.panel.panel-default-->'

        + '  <div class="panel panel-default">'
        + '    <div class="panel-heading">'
        + '      <h4>Example Pay - Team Member 1</h4>'
        + '    </div>'
        + '    <div class="panel-body table-responsive">'
        + '      <table class="calc table">'
        + '        <tr class="captions">'
        + '          <td>Current Pay Rate</td>'
        + '          <td><i>x</i></td>'
        + '          <td>Pay Hrs for Pay Period</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Regular Pay</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td><sup>$</sup>21.97</td>'
        + '          <td><small>x</small></td>'
        + '          <td>100</td>'
        + '          <td><small>=</small></td>'
        + '          <td><sup>$</sup>2,197.00</td>'
        + '        </tr>'
        + '      </table>'
        + '    </div><!--/.panel-body-->'
        + '  </div><!--/.panel.panel-default-->'
        + '  <div class="paycalcwrap"></div>',

      // When team lead OR manager is logged in
      hpw_teamlead_mgr_html : String ()
        + '<h2 class="page-header">How Team Pay Works</h2>'
        + '<h3 class="section-start">Terminology</h3>'
        + '<dl>'
        + '  <dt>Flag Hours</dt>'
        + '    <dd>Total number of labor hours billed</dd>'
        + '  <dt>Clock Hours</dt>'
        + '    <dd>Total number of hours clocked in</dd>'
        + '  <dt>Pay Hours</dt>'
        + '    <dd>Clock Hours times Team Proficiency</dd>'
        + '  <dt>Team Proficiency</dt>'
        + '    <dd>Total Team Flag Hours divided by Total Team clock Hours</dd>'
        + '  <dt>Regular Pay</dt>'
        + '    <dd>Your Pay Hours times your Current Pay Rate</dd>'
        + '  <dt>ELR (Effective Labor Rate)</dt>'
        + '    <dd>Labor Sales divided by Flag Hours</dd>'
        + '  <dt>Budget</dt>'
        + '    <dd>Total dollars per pay hour the Manager/Team Lead have to divide'
        + '      amongst the team members</dd>'
        + '</dl>'
        + '<h3 class="section-start">Calculating Budget</h3>'
        + '<p>Pay is based on the shops 6-month rolling '
        + '<em>Effective Labor Rate</em> or ELR.  The ELR is calculated by '
        + 'taking the total Labor Sales in the last 6 months and dividing it '
        + 'by the total number of Flag Hours in the same time frame. '
        + 'In the team pay system, the team is paid 20% of the ELR for each '
        + 'hour the team produces. </p>'
        + '<p>The Service Manager and Team Leader decide how this will be split'
        + 'up amongst the team members. The percentage of split you receive '
        + 'is what determines your <em>Current Pay Rate</em>.</p>'
        
        + '<div class="panel panel-default">'
        + '    <div class="panel-heading">'
        + '      <h4>Example Team Budget</h4>'
        + '    </div>'
        + '    <div class="panel-body table-responsive">'
        + '      <table class="calc table">'
        + '        <tr class="captions">'
        + '          <td>ELR</td>'
        + '          <td><i>x</i></td>'
        + '          <td>Tech %</td>'
        + '          <td><i>x</i></td>'
        + '          <td>No. of Techs on Team</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Budget</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td><sup>$</sup>88.58</td>'
        + '          <td><small>x</small></td>'
        + '          <td>20<sup>%</sup></td>'
        + '          <td><small>x</small></td>'
        + '          <td>4</td>'
        + '          <td><small>=</small></td>'
        + '          <td><sup>$</sup>70.86</td>'
        + '        </tr>'
        + '      </table>'
        + '    </div><!--/.panel-body-->'
        + '  </div><!--/.panel.panel-default-->'

        + '  <h3 class="section-start">Team Budgeting</h3>'
        + '  <p>The budget is the total dollars per pay hour the Team '
        + '    Leader and Service Manager have to divide amongst the '
        + '    team members.</p> '
        + '  <p>The Team Leader and Service Manager will assign a percentage of the'
        + '     budget to each team member. The percentage will be determined'
        + "    by the tech's skill set, tech's capacity, "
        + '    and overall proficiency.</p>'

        + '  <div class="panel panel-default">'
        + '    <div class="panel-heading">'
        + '      <h4>Example Team Breakdown</h4>'
        + '    </div>'
        + '    <div class="panel-body table-responsive">'
        + '      <table class="calc table">'
        + '        <tr class="captions">'
        + '          <td>Team Member</td>'
        + '          <td><i>|</i></td>'
        + '          <td>Budget</td>'
        + '          <td><i>x</i></td>'
        + '          <td>Team member % of budget</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Current pay rate</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>1 - 31%</td>'
        + '          <td><small>|</small></td>'
        + '          <td><sup>$</sup>70.86</td>'
        + '          <td><small>x</small></td>'
        + '          <td>31<sup>%</sup></td>'
        + '          <td><small>=</small></td>'
        + '          <td><sup>$</sup>21.97</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>2 - 28%</td>'
        + '          <td><small>|</small></td>'
        + '          <td><sup>$</sup>70.86</td>'
        + '          <td><small>x</small></td>'
        + '          <td>28<sup>%</sup></td>'
        + '          <td><small>=</small></td>'
        + '          <td><sup>$</sup>19.84</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>3 - 23%</td>'
        + '          <td><small>|</small></td>'
        + '          <td><sup>$</sup>70.86</td>'
        + '          <td><small>x</small></td>'
        + '          <td>23<sup>%</sup></td>'
        + '          <td><small>=</small></td>'
        + '          <td><sup>$</sup>16.30</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td>4 - 18%</td>'
        + '          <td><small>|</small></td>'
        + '          <td><sup>$</sup>70.86</td>'
        + '          <td><small>x</small></td>'
        + '          <td>18<sup>%</sup></td>'
        + '          <td><small>=</small></td>'
        + '          <td><sup>$</sup>12.75</td>'
        + '        </tr>'
        + '      </table>'
        + '    </div><!--/.panel-body-->'
        + '  </div><!--/.panel.panel-default-->'

        + '  <div class="panel panel-default">'
        + '    <div class="panel-heading">'
        + '      <h4>Example Pay - Team Member 1</h4>'
        + '    </div>'
        + '    <div class="panel-body table-responsive">'
        + '      <table class="calc table">'
        + '        <tr class="captions">'
        + '          <td>Current Pay Rate</td>'
        + '          <td><i>x</i></td>'
        + '          <td>Pay Hrs for Pay Period</td>'
        + '          <td><i>=</i></td>'
        + '          <td>Regular Pay</td>'
        + '        </tr>'
        + '        <tr>'
        + '          <td><sup>$</sup>21.97</td>'
        + '          <td><small>x</small></td>'
        + '          <td>100</td>'
        + '          <td><small>=</small></td>'
        + '          <td><sup>$</sup>2,197.00</td>'
        + '        </tr>'
        + '      </table>'
        + '    </div><!--/.panel-body-->'
        + '  </div><!--/.panel.panel-default-->'
        + '  <div class="paycalcwrap"></div>',

       pay_calc_html : String ()
        + '<h2 class="page-header">Pay Breakdown</h2>'
        + '<h4>"What if" Pay Calculator</h4>'
        + '<p>Use this calculator to evaluate how your Regular Pay can change'
        + '  based on your teams Flag Hours and Clock Hours by changing those'
        + '  values.  You can also see how Team Proficiency changes along'
        + '  with your pay in the team environment.</p>'
        + '<p><strong>Directions:</strong> Change the values in the Team Flag'
        + '  Hrs and Team Clock Hrs input boxes, then click calculate.</p>'
        + '<div class="panel panel-default">'
        + '  <!-- <div class="panel-heading">Panel heading without title</div> -->'
        + '  <div class="panel-body table-responsive">'
        + '    <table class="calc table">'
        + '      <tr class="captions">'
        + '        <td>Team Flag Hrs</td>'
        + '        <td><i>/</i></td>'
        + '        <td>Team Clock Hrs</td>'
        + '        <td><i>=</i></td>'
        + '        <td>Team Proficiency</td>'
        + '      </tr>'
        + '      <tr>'
        + '        <td>'
        + '          <input type="text" class="form-control" name="TeamFlagHours" '
        + '            placeholder="000" value="TEAMFLAGHOURS">'
        + '          </input>'
        + '        </td>'
        + '        <td><small>/</small></td>'
        + '        <td>'
        + '          <input type="text" class="form-control" name="TeamClockHours" '
        + '            placeholder="000" value="TEAMCLOCKHOURS">'
        + '          </input>'
        + '        </td>'
        + '        <td><small>=</small></td>'
        + '        <td>TEAMPROFICIENCY</td>'
        + '      </tr>'
        + '      </table>'
        + '      <table class="calc table">'
        + '      <tr class="captions">  '
        + '        <td>Team Proficiency</td>'
        + '        <td><i>x</i></td>'
        + '        <td>Your Clock Hrs</td>'
        + '        <td><i>=</i></td>'
        + '        <td>Your Pay Hrs</td>'
        + '        <td><i>x</i></td>'
        + '        <td>Your Pay Rate</td>'
        + '        <td><i>=</i></td>'
        + '      </tr>'
        + '      <tr>  '
        + '        <td>TEAMPROFICIENCY</td>'
        + '        <td><small>x</small></td>'
        + '        <td>'
        + '          <input type="text" class="form-control" name="YourClockHours" '
        + '            placeholder="000" value="YOURCLOCKHOURS">'
        + '          </input>'
        + '        </td>'
        + '        <td><small>=</small></td>'
        + '        <td>YOURPAYHOURS</td>'
        + '        <td><small>x</small></td>'
        + '        <td>YOURPAYRATE</td>'
        + '        <td><small>=</small></td>'
        + '      </tr>'
        + '      </table>'
        + '      <table class="calc table">'
        + '      <tr class="captions">'
        + '        <td>Your Regular Pay</td>'
        + '      </tr>'
        + '      <tr>'
        + '        <td>YOURREGULARPAY</td>'
        + '      </tr>'
        + '    </table>'
        + '  </div><!--/.panel-body-->'
        + '  <div class="panel-footer">'
        + '    <button id="calcbutton" class="btn btn-lg btn-side">Calculate Pay</button>'
        + '  </div><!--/.panel-footer-->'
        + '</div><!--/.panel.panel-default-->',

      settable_map : {}
    },
    stateMap = { $container : null },
    calcMap = {
      teamFlagHours   : 300,
      teamClockHours  : 300,
      teamProficiency : 1,
      yourClockHours  : 80,
      yourPayHours    : 80,
      yourPayRate     : 12,
      yourRegularPay  : 960
    },
    jqueryMap = {},
 

    setJqueryMap, configModule, initModule, 
    onShowTechHPW, onShowManagerHPW, onShowPayCalc, onDriverChange, runCalculator;

  //--------------------- END MODULE SCOPE VARIABLES --------------------------
 

  //------------------------ BEGIN UTILITY METHODS ----------------------------
  // We need to create a utility module for stuff like this
  function replaceAll(str, mapObj){
    var re = new RegExp( Object.keys(mapObj).join("|"), "g");

    return str.replace(re, function(matched){
      return mapObj[matched];
    });
  }
  //------------------------- END UTILITY METHODS -----------------------------
 

  //-------------------------- BEGIN DOM METHODS ------------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;
    jqueryMap = { $container : $container };
  };

  // End DOM method /setJqueryMap/
  //-------------------------- END DOM METHODS --------------------------------
 

  //----------------------- BEGIN EVENT HANDLERS ------------------------------

  onShowTechHPW = function() {
    jqueryMap.$container.empty().append( configMap.hpw_tech_html ); 
    jqueryMap.$payCalcWrap = jqueryMap.$container.find( '.paycalcwrap' );
    $.gevent.publish( 'hpw.requestTechPayCalc', [] );
  };

  onShowMgrHPW = function() {
    jqueryMap.$container.empty().append( configMap.hpw_teamlead_mgr_html ); 
    jqueryMap.$payCalcWrap = jqueryMap.$container.find( '.paycalcwrap' );
    $.gevent.publish( 'hpw.requestTechPayCalc', [] );
  };

  // Load initial values in the markup
  // Show the markup
  // onCalc retrieve the values from the markup
  // calculate new values
  // load new values in the markup
  // show the markup
  onShowPayCalc = function( e, techPayParams ) {
    var updateMarkup, calcPay, onCalcButtonClick;
    
    updateMarkup = function() {
      var templateMap;

      if ( techPayParams ) {
        calcMap.teamFlagHours  = techPayParams.teamFlagHours;
        calcMap.teamClockHours = techPayParams.teamClockHours;
        calcMap.yourPayRate    = techPayParams.payRate;
        calcMap.yourClockHours = techPayParams.clockHours;
      }

      templateMap = {
        TEAMFLAGHOURS   : numeral(calcMap.teamFlagHours).format('0.00'),
        TEAMCLOCKHOURS  : numeral(calcMap.teamClockHours).format('0.00'),
        TEAMPROFICIENCY : numeral(calcMap.teamProficiency).format('0%'),
        YOURCLOCKHOURS  : numeral(calcMap.yourClockHours).format('0.00'),
        YOURPAYHOURS    : numeral(calcMap.yourPayHours).format('0.00'),
        YOURPAYRATE     : numeral(calcMap.yourPayRate).format('$0,0.00'),
        YOURREGULARPAY  : numeral(calcMap.yourRegularPay).format('$0,0.00')
      },
      markup = replaceAll( configMap.pay_calc_html, templateMap );
      jqueryMap.$payCalcWrap.empty().append( markup );
      jqueryMap.$teamFlagHours  = jqueryMap.$container.find('input[name="TeamFlagHours"]');
      jqueryMap.$teamClockHours = jqueryMap.$container.find('input[name="TeamClockHours"]');
      jqueryMap.$yourClockHours = jqueryMap.$container.find('input[name="YourClockHours"]');
      jqueryMap.$calcButton     = jqueryMap.$container.find('#calcbutton');
      jqueryMap.$calcButton.on('click', onCalcButtonClick);
    }


    calcPay = function() {
      var teamFlagHours, teamClockHours, teamProficiency, yourClockHours,
        yourPayHours, yourPayRate, yourRegularPay;

      teamFlagHours   = jqueryMap.$teamFlagHours.val();
      teamClockHours  = jqueryMap.$teamClockHours.val();
      yourClockHours  = jqueryMap.$yourClockHours.val();
      teamProficiency = teamFlagHours / teamClockHours;
      yourPayHours    = yourClockHours * teamProficiency;
      yourPayRate     = numeral().unformat(calcMap.yourPayRate.toString());
      yourRegularPay  = yourPayHours * yourPayRate;
    
      calcMap.teamFlagHours   = teamFlagHours;
      calcMap.teamClockHours  = teamClockHours;
      calcMap.teamProficiency = teamProficiency;
      calcMap.yourClockHours  = yourClockHours;
      calcMap.yourPayHours    = yourPayHours;
      calcMap.yourPayRate     = yourPayRate;
      calcMap.yourRegularPay  = yourRegularPay;
    }

    onCalcButtonClick = function() {
      calcPay();
      updateMarkup();
    }

    updateMarkup();
    calcPay();
    updateMarkup();

  };


  //------------------------ END EVENT HANDLERS -------------------------------
 

  //----------------------- BEGIN PUBLIC METHODS ------------------------------

  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys
  // Arguments  : A map of settable keys and values
  //  * color_name - color to use
  // Settings   :
  //  * configMap.settable_map declares allowed keys
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    spa.util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/
 

  // Begin public method /initModule/
  // Purpose   : Initializes module
  // Arguments :
  //  *  the jquery element used by this feature
  // Returns   : true
  // Throws    : nonaccidental
  //
  initModule = function ( $container ) {
    console.log('hpw initModule');
    stateMap.$container = $container;
    setJqueryMap();
    $.gevent.subscribe( $container, 'teampay.showtechhpw',      onShowTechHPW );
    $.gevent.subscribe( $container, 'teampay.showmanagerhpw',   onShowMgrHPW );
    $.gevent.subscribe( $container, 'teampay.showpaycalc',      onShowPayCalc );
    $.gevent.subscribe( $container, 'hpw.fulfilledTechPayCalc', onShowPayCalc );
    return true;
  };
  // End public method /initModule/
 

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
 };
  //----------------------- END PUBLIC METHODS --------------------------------
}());
 
