/*
 * car.netpromoter.js
 * Namespace module for NetPromoter
 *
 * Augments left nav bar to access functionality
*/

/*jslint           browser : true,   continue : true,
  devel  : true,    indent : 2,       maxerr  : 50,
  newcap : true,     nomen : true,   plusplus : true,
  regexp : true,    sloppy : true,       vars : false,
  white  : true
*/
/*global $, spa */

car.netpromoter = (function () {
  'use strict';

  var
    configMap = {
      // Name of 'App' to the right of the Intranet logo
      app_name : String()
        + '<a class="navbar-brand" href="#">'
        + '  <span>Something clever that we can change later</span>'
        + '</a>',

      // Main links
      mainlink_np_manager_summary_html : String()  
        + '<li class="mainlink">'
        + '  <a class="managersplash" href="#">'
        + '    <span class="glyphicon glyphicon-signal"></span>Net Promoter'
        + '    <span class="arrow-right"></span>'
        + '  </a>'
        + '</li>',

      mainlink_np_writer_summary_html : String()  
        + '<li class="mainlink">'
        + '  <a class="writersplash" href="#">'
        + '    <span class="glyphicon glyphicon-signal"></span>Net Promoter'
        + '    <span class="arrow-right"></span>'
        + '  </a>'
        + '</li>',

      // Sub links
      sublink_np_manager_surveys_html : String()
        + '    <li><span class="sublink managersurveys">'
        + '      <a href="#">Surveys</a></span>'
        + '    </li>',

      sublink_np_writer_surveys_html : String()
        + '    <li><span class="sublink writersurveys">'
        + '      <a href="#">Surveys</a></span>'
        + '    </li>',

      sublink_np_about_html : String()
        + '    <li><span class="sublink about">'
        + '      <a href="#">About</a></span>'
        + '    </li>',

      settable_map : {}
    },
    stateMap = { $container : null },
    jqueryMap = {},
 
    setJqueryMap,          configModule,         initModule,
    onLoadNavBar,          onInitModules,
    onManagerSplashClick,  onWriterSplashClick,  
    onManagerSurveysClick, onWriterSurveysClick, onAboutClick
  ;

  //--------------------- END MODULE SCOPE VARIABLES --------------------------
  //-------------------------- BEGIN DOM METHODS ------------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;
    jqueryMap = { $container : $container };
  };

  onLoadNavBar = function( e, appNav ) {
    var navItems = '';

    jqueryMap.$navBar = jqueryMap.$container.find('.app-np');
    $.each(appNav, function(i, item) {
      if ( item.appCode == 'np' && item.navType == 'leftnavbar' ) {
        navItems += configMap[item.configMapMarkupKey];
      };
    });
    jqueryMap.$navBar.append( navItems );
    jqueryMap.$managerSplash  = $( '.managersplash' );
    jqueryMap.$writerSplash   = $( '.writersplash' );
    jqueryMap.$managerSurveys = $( '.managersurveys' );
    jqueryMap.$writerSurveys  = $( '.writersurveys' );
    jqueryMap.$about          = $( '.about' );

    jqueryMap.$managerSplash.on(  'click', onManagerSplashClick );
    jqueryMap.$writerSplash.on(   'click', onWriterSplashClick );
    jqueryMap.$managerSurveys.on( 'click', onManagerSurveysClick );
    jqueryMap.$writerSurveys.on(  'click', onWriterSurveysClick );
    jqueryMap.$about.on(          'click', onAboutClick );
  };

  onInitModules = function() {
    car.netpromoter.data.initModule ( jqueryMap.$container );
    car.netpromoter.splash.initModule(  $( '.page-content-main' ) );
    car.netpromoter.about.initModule(   $( '.page-content-main' ) );
    $.gevent.publish( 'security.requestUserData', [] );
  }

  // End DOM method /setJqueryMap/
  //-------------------------------- END DOM METHODS ------------------------------

  //----------------------------- Begin Event Handlers ----------------------------
  
  onManagerSplashClick  = function() { $.gevent.publish( 'netpromoter.showmanagersplash' ); };

  onWriterSplashClick   = function() { $.gevent.publish( 'netpromoter.showwritersplash' ); };

  onManagerSurveysClick = function() { $.gevent.publish( 'netpromoter.showsurveys' ); };

  onWriterSurveysClick  = function() { $.gevent.publish( 'netpromoter.showwritersurveys' ); };

  onAboutClick          = function() { $.gevent.publish( 'netpromoter.showabout' ); };

  //------------------------------ End Event Handlers -----------------------------

  // Begin public method /initModule/
  // Purpose   : Initializes module
  // Arguments :
  //  *  the jquery element used by this feature
  // Returns   : true
  // Throws    : nonaccidental
  //
  initModule = function ( $container ) {
    console.log( 'car.netpromoter initModule' );
    stateMap.$container = $container;
    setJqueryMap();
    $.gevent.subscribe( $container, 'shell.initmodules', onInitModules );
    $.gevent.subscribe( $container, 'shell.apploadnavbar', onLoadNavBar );
    //    car.teampay.initModule( $('.sidebar-widgets') );
    return true;
  };
  // End public method /initModule/

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
  };
}());
