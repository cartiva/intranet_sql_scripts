// Module /netpromoter.data/
// Provides database access 
//
car.netpromoter.data = (function() {
  'use strict';

  //------------------------- Begin Module scope variables -------------------------
  var
    // Set constants
    configMap = {
      // Error message for bad data returned
      data_error_html : String ()
        + '<div class="alert alert-warning">'
        + '  <h4>Error</h4>'
        + '  <p>There was an error returning some data. See Jon to '
        + '    bitch him out.</p>'
        + '</div>',
        
      settable_map  : { baseURL: true },
      baseURL       : './php/data.php',
      summaryURL    : '/npsummaryscores',
      writerListURL : '/npwriterlist',
      scoresURL     : '/npscores',
      surveysURL    : '/npsurveys',
    },
    // Declare all other module scope variables
    stateMap = { 
      $container : null
    },

    configModule,         
    initModule,
    getBaseURL,
    ajaxGet,
    ajaxPost,
    ajaxDelete,
    isJSON,
    onRequestManagerSummaryData, processManagerSummaryData,
    onRequestSurveysModel,       processSurveysModel,
    onRequestWriterModel,        processWriterModel
  ;
  //--------------------------- End Module Scope Variables ------------------------

  //----------------------------- Begin Utility Methods ---------------------------
  ajaxGet = function(url, callback) {
    return $.ajax({ 
      type     : "GET",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      success  : callback });
  }

  ajaxPost = function(url, postData, callback) {
    return $.ajax({ 
      type     : "POST",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      data     : postData,
      success  : callback });
  }

  ajaxDelete = function(url, deleteData, callback) {
    return $.ajax({ 
      type     : "DELETE",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      data     : deleteData,
      success  : callback });
  }

  //------------------------------ End Utility Methods ----------------------------

  //------------------------------- Begin DOM Methods -----------------------------
  //-------------------------------- End DOM Methods ------------------------------

  //----------------------------- Begin Event Handlers ----------------------------
  
  onRequestManagerSummaryData = function( e, userName ) {
    var summaryScores, writerList;

    summaryScores = ajaxGet( configMap.baseURL + configMap.summaryURL + '/' + userName );
    writerList = ajaxGet( configMap.baseURL + configMap.writerListURL + '/' + userName );
    $.when( summaryScores, writerList ).done( processManagerSummaryData );
  };

  processManagerSummaryData = function ( summaryScoresjqXHR, writerListjqXHR ) {
    var summaryScores, writerList;

    summaryScores = ( summaryScoresjqXHR[1] == 'success' ) ? summaryScoresjqXHR[0] : [{}];
    writerList    = ( writerListjqXHR[1]    == 'success' ) ? writerListjqXHR[0]    : [{}];

    $.gevent.publish( 'netpromoter.fulfilledManagerSummaryData', [ summaryScores[0], writerList ] );
  }

  onRequestSurveysModel = function( e, user ) {
    var suffix, surveys;

    if (user) {
      suffix = '/' + user;
    } else {
      suffix = '';
    };

    surveys = ajaxGet( configMap.baseURL + configMap.surveysURL + suffix );
    surveys.then( processSurveysModel );
  };

  processSurveysModel = function ( surveysData ) {
    var surveysModel;

    surveysModel = {
      surveys : surveysData
    };

    $.gevent.publish( 'netpromoter.fulfilledSurveysModel', [ surveysModel ] );
  };

  onRequestWriterModel = function( e, user ) {
    var suffix, summary, survey;

    if (user) {
      suffix = '/' + user;
    } else {
      suffix = '';
    };

    summary = ajaxGet( configMap.baseURL + configMap.summaryURL + suffix );
    survey  = ajaxGet( configMap.baseURL + configMap.surveysURL + suffix );
    $.when( summary, survey ).done( processWriterModel );

  };

  processWriterModel = function( summaryjqXHR, surveysjqXHR ) {
    var summary, surveys, writerModel;

    summary = summaryjqXHR[0][0];
    surveys = surveysjqXHR[0];
    
    writerModel = { 
      summary : summary,
      surveys : surveys
    };

    $.gevent.publish( 'netpromoter.fulfilledWriterModel', [ writerModel ] );
  };


  //------------------------------ End Event Handlers -----------------------------

  //----------------------------- Begin Public Methods ----------------------------
  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys {key : value}
  // Arguments  : A map of settable keys and values
  //   * color_name - color to use
  // Settings   : 
  //   * configMap.settable_map declares what keys are allowed to be set
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/

  // Begin public method /initModule/
  // Purpose    : Initializes module
  // Arguments  : 
  //   * $container the jquery element used by this feature
  // Returns    : true
  // Throws     : none
  //
  initModule = function ( $container ) {
    console.log('data initModule');
    $.gevent.subscribe( $container, 'netpromoter.requestManagerSummaryData', onRequestManagerSummaryData );
    $.gevent.subscribe( $container, 'netpromoter.requestSurveysModel',       onRequestSurveysModel );
    $.gevent.subscribe( $container, 'netpromoter.requestWriterModel',        onRequestWriterModel );
    return true;
  };

  getBaseURL = function() {
    return configMap.baseURL;
  }
  // End public method /initModule/

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
  };
  //------------------------------ End Public Methods -----------------------------
}());
