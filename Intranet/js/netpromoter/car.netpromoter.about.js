/*
* car.netpromoter.about.js
*/
/*jslint         browser: true,  continue : true,
  devel  : true, indent : 2,     maxerr   : 50,
  newcap : true, nomen  : true,  plusplus : true,
  regexp : true, sloppy : true,  vars     : false,
  white  : true
*/

/*global $, car.mod.teampay.ros */

car.netpromoter.about = (function () {

  //--------------------- BEGIN MODULE SCOPE VARIABLES ------------------------
  var
    configMap = {
      about_html : String ()
        + '<h4>About Net Promoter</h4>'
        + '<p><small>This information was taken from the Net Promoter'
        + '  Website by Bain & Company</small></p>'
        + '<p>In 2003, Fred Reichheld, a partner at Bain & Company, created'
        + '  a new way of measuring how well an organization treats the people'
        + '  whose lives it affects—how well it generates relationships worthy'
        + '  of loyalty.  He called that metric the Net Promoter Score'
        + '  <sup>SM</sup>, or NPS&reg;.</p><p>Net Promoter System<sup>SM</sup>'
        + '  is based on the fundamental perspective that every '
        + '  company' + "'" + 's customers can be divided into three categories.'
        + '  <strong>Promoters</strong> are loyal enthusiasts who keep buying '
        + '  from a company and urge their friends to do the same. '
        + '  <strong>Passives</strong> are satisfied but unenthusiastic'
        + '  customers who can be easily wooed by the competition. And'
        + '  <strong>detractors</strong> are unhappy customers trapped'
        + '  in a bad relationship.'
        + '</p>'
        + '<h5>Measuring your Net Promoter Score</h5>'
        + '<p>The best way to gauge the efficiency of a company' + "'" + 's'
        + '  growth engine is to take the percentage of customers who are'
        + '  promoters and subtract the percentage who are detractors. This'
        + '  equation is how we calculate a Net Promoter Score for a company:'
        + '</p>'
        + '<img src="./images/nps-measure-of-success.jpg" '
        + '  alt="Net Promoter: Measure of Success" '
        + '  style="border: 1px solid #eee;padding:30px;background:#FFF;" />'
        + '<p>While easy to grasp, NPS metric represents a radical change'
        + '  in the way companies manage customer relationships and organize'
        + '  for growth. Rather than relying on notoriously ineffective customer'
        + '  satisfaction surveys, companies can use NPS to measure customer'
        + '  relationships as rigorously as they now measure profits.'
        + '  What' + "'" + 's more, NPS finally enables CEOs to hold employees'
        + '  accountable for treating customers right. It clarifies the link'
        + '  between the quality of a company' + "'" + 's customer relationships'
        + '  and its growth prospects.'
        + '  </p>'
        + '<p>Want to read more?  Check out the '
        + '  <a href="http://www.netpromotersystem.com/about/'
        + '    measuring-your-net-promoter-score.aspx" target="_blank">'
        + '    Net Promoter System website'
        + '  </a> for plenty on the subject.'
        + '</p>',

      settable_map : {}
    },
    stateMap = { $container : null },
    jqueryMap = {},
 
    setJqueryMap,     configModule,    initModule,
    onShowAbout
  ;
    
  //--------------------- END MODULE SCOPE VARIABLES --------------------------
 

  //------------------------ BEGIN UTILITY METHODS ----------------------------
  //------------------------- END UTILITY METHODS -----------------------------
 

  //-------------------------- BEGIN DOM METHODS ------------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;
 
    jqueryMap = { $container : $container };
  };

  // End DOM method /setJqueryMap/
  //-------------------------- END DOM METHODS --------------------------------

  //----------------------- BEGIN EVENT HANDLERS ------------------------------

  onShowAbout = function ( e, techID ) {
    jqueryMap.$container.empty().append( configMap.about_html );
  };

  //------------------------ END EVENT HANDLERS -------------------------------
 

  //----------------------- BEGIN PUBLIC METHODS ------------------------------
  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys
  // Arguments  : A map of settable keys and values
  //  * color_name - color to use
  // Settings   :
  //  * configMap.settable_map declares allowed keys
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    car.util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/
 

  // Begin public method /initModule/
  // Purpose   : Initializes module
  // Arguments :
  //  *  the jquery element used by this feature
  // Returns   : true
  // Throws    : nonaccidental
  //
  initModule = function ( $container ) {
    console.log('netpromoter.about initModule');
    stateMap.$container = $container;
    setJqueryMap();
    $.gevent.subscribe( $container, 'netpromoter.showabout', onShowAbout );
    return true;
  };
  // End public method /initModule/
 

  // return public methods
  return {
    configModule          : configModule,
    initModule            : initModule
  };
  //----------------------- END PUBLIC METHODS --------------------------------
}());
 
