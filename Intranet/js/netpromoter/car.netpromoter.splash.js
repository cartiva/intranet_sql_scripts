/*
* car.netpromoter.splash.js
*
* This module handles the display of the splash screens.
*   Manager Splash
*   Writer Splash
*
* Data needs for module:
*
*   Manager Splash:
*
*   Writer Splash:
*
*     
*/
/*jslint         browser: true,  continue : true,
  devel  : true, indent : 2,     maxerr   : 50,
  newcap : true, nomen  : true,  plusplus : true,
  regexp : true, sloppy : true,  vars     : false,
  white  : true
*/

/*global $, car.netpromoter.splash */

car.netpromoter.splash = (function () {

  //--------------------- BEGIN MODULE SCOPE VARIABLES ------------------------
  var
    configMap = {

      manager_summary_html : String()
        + '<h2 class="page-header">Net Promoter Summary (last 90 days)</h2>'
        + '<p>These are combined Service Writers Net Promoter stats from '
        + '  the last 90 days. Clicking the table will take you to a list of '
        + '  all Service Writer surveys.</p>'
        + '<aside>'
        + '  <a href="#" class="aboutnp">About Net Promoter</a>'
        + '</aside>'
        //+ '<a href="#" class="aboutnp"><aside>About Net Promoter</aside></a>'
			  + '<table id="weekly" class="splash npscoresummary table table-hover '
        + '  table-striped table-condensed">'
			  + '	<thead>'
				+ '		<tr>'
				+ '			<th data-col="sent"><span></span>Sent</th>'
				+ '			<th data-col="returned"><span></span>Returned</th>'
				+ '			<th data-col="responserate"><span></span>Response Rate</th>'
				+ '			<th data-col="netpromoterscore"><span></span>NP Score (last 90 days)</th>'
				+ '		</tr>'
				+ '	</thead>'
				+ '	<tbody>',
      
      manager_writerslist_html : String() 
        + '<h3 class="section-start">Net Promoter Writers List</h3>'
        + '<p>These are Service Writers that have Net Promoter Scores.</p>'
        + '<table id="weekly" class="splash npscores table table-hover '
        + '  table-striped table-condensed">'
				+ '	<thead>'
				+ '		<tr id="headings">'
			  + '			<th data-col="areawriter"><span class="arrow"></span>Writer</th>'
				+ '			<th data-col="sent"><span class="arrow"></span>Sent</th>'
				+ '			<th data-col="returned"><span class="arrow"></span>Returned</th>'
				+ '			<th data-col="responserate"><span class="arrow"></span>Response Rate</th>'
				+ '			<th data-col="netpromoterscore"><span class="arrow"></span>NP Score (last 90 days)</th>'
				+ '		</tr>'
				+ '	</thead>'
				+ '	<tbody class="writers">',

      writer_summary_html : String() 
        + '<h2 class="page-header">Net Promoter Summary (last 90 days)</h2>'
        + '<p>This is the Net Promoter summary from the last 90 days for an individual Service Writer.</p>'
			  + '<table id="weekly" class="splash npscoresummary table table-hover '
        + '  table-striped table-condensed">'
			  + '	<thead>'
				+ '		<tr>'
				+ '			<th data-col="sent"><span></span>Sent</th>'
				+ '			<th data-col="returned"><span></span>Returned</th>'
				+ '			<th data-col="responserate"><span></span>Response Rate</th>'
				+ '			<th data-col="netpromoterscore"><span></span>NP Score (last 90 days)</th>'
				+ '		</tr>'
				+ '	</thead>'
				+ '	<tbody>',

      writer_surveys_html : String()
        + '<h2 class="page-header">Net Promoter Surveys (last 90 days)</h2>'
        + '<p>These are Net Promoter surveys from the last 90 days for an individual Service Writer.</p>'
        + '<table id="weekly" class="splash npsurveys table table-hover '
        + '  table-striped table-condensed">'
			  + '	<thead>'
				+ '		<tr id="headings">'
				+ '			<th data-col="writer"><span class="arrow"></span>Writer</th>'
				+ '			<th data-col="date" class="col-date"><span class="arrow"></span>Date</th>'
				+ '			<th data-col="ro"><span class="arrow"></span>RO</th>'
				+ '			<th data-col="customer"><span class="arrow"></span>Customer</th>'
				+ '			<th data-col="likelihood"><span class="arrow"></span>Referral Likelihood</th>'
				+ '			<th data-col="experience"><span class="arrow"></span>Experience</th>'
				+ '		</tr>'
				+ '	</thead>'
				+ '	<tbody>',

      settable_map : {}
    },

    stateMap = { 
      $container : null,
      user : null
    },

    jqueryMap = {},
 
    setJqueryMap,           configModule,        initModule,
    onFulfilledUserData,
    onShowManagerSplash,    onShowWriterSplash,  onShowSurveys,
    onFulfilledManagerSummaryData,
    onFulfilledWriterSummaryData, 
    onAboutNPLinkClick,
    onWriterClick,
    onFulfilledWriterModel,
    onFulfilledSurveysModel
  ;

  //--------------------- END MODULE SCOPE VARIABLES --------------------------
 

  //------------------------ BEGIN UTILITY METHODS ----------------------------
  function replaceAll(str, mapObj) {
    var re = new RegExp( Object.keys(mapObj).join("|"), "g");

    return str.replace(re, function(matched){
      return mapObj[matched];
    });
  }
  //------------------------- END UTILITY METHODS -----------------------------
 

  //-------------------------- BEGIN DOM METHODS ------------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;
    jqueryMap = { $container : $container };
  };

  // End DOM method /setJqueryMap/
  //-------------------------- END DOM METHODS --------------------------------
 

  //----------------------- BEGIN EVENT HANDLERS ------------------------------
  onFulfilledUserData = function( e, user ) {
    stateMap.user = user;
  };

  onShowManagerSplash = function( e, techID ) {
    $.gevent.publish( 'netpromoter.requestManagerSummaryData', [ stateMap.user.userName ]);
  };

  onFulfilledManagerSummaryData = function(e, summaryData, writerListData) {
    var summarybody = '', writerbody = '';

		summarybody += '		<tr>'
		  + '			<td class="col-sum col-sent">' + summaryData.Sent + '</td>'
		  + '			<td class="col-sum col-returned">' + summaryData.Returned + '</td>'
		  + '			<td class="col-sum col-responserate">' + summaryData.ResponseRate + '</td>'
		  + '			<td class="col-sum col-netpromoterscore">' + summaryData.NetPromoterScore + '</td>'
		  + '		</tr>'
		  + '	</tbody>'
		  + '</table>';

    $.each(writerListData, function(i, item) {
		  writerbody += '		<tr>'
			  + '			<td class="col-writer"><a href="#" data-user="' + item.eeUserName + '"">' + item.FirstName + ' ' + item.LastName + '</a></td>'
			  + '			<td class="col-sent">' + item.Sent + '</td>'
			  + '			<td class="col-returned">' + item.Returned + '</td>'
			  + '			<td class="col-responserate">' + item.ResponseRate + '</td>'
			  + '			<td class="col-netpromoterscore">' + item.NetPromoterScore + '</td>'
			  + '		</tr>';
			});
  	writerbody += '	</tbody>';
		writerbody += '</table>';

    jqueryMap.$container.html( configMap.manager_summary_html + summarybody + configMap.manager_writerslist_html + writerbody );

    jqueryMap.$aboutNPLink = jqueryMap.$container.find( '.aboutnp' );
    jqueryMap.$aboutNPLink.on( 'click', onAboutNPLinkClick );

    jqueryMap.$writersList = jqueryMap.$container.find( '.writers' );
    jqueryMap.$writersList.on( 'click', 'a', onWriterClick );
  };

  onAboutNPLinkClick = function() {
  	$.gevent.publish( 'netpromoter.showabout' );
  };

  onShowWriterSplash = function( e, user ) {
    $.gevent.publish( 'netpromoter.requestManagerSummaryData', [ user ]);
  };

  onFulfilledWriterSummaryData = function(e, summaryData) {
    var map, header, dailybody = '', totalbody = '', footer = '';

    jqueryMap.$container.html( header + configMap.tech_daily_html + dailybody + totalbody + footer );
  };

  onWriterClick = function( e ) {
    $.gevent.publish( 'netpromoter.requestWriterModel', [ $( e.target ).data( 'user') ]);
  };

  onFulfilledWriterModel = function( e, writerModel ) {
  	var summaryBody = '', surveysBody = '';

		summaryBody += '		<tr>'
		  + '	<td class="col-sent">' + writerModel.summary.Sent + '</td>'
		  + '	<td class="col-returned">' + writerModel.summary.Returned + '</td>'
		  + '	<td class="col-responserate">' + writerModel.summary.ResponseRate + '</td>'
		  + '	<td class="col-netpromoterscore">' + writerModel.summary.NetPromoterScore + '</td>'
		  + '</tr>'
		  + '</tbody>'
		  + '</table>';

		$.each(writerModel.surveys, function(i, item) {
	    surveysBody += '		<tr>'
			  + '			<td class="col-writer">' + item.WriterName + '</td>'
	      + '			<td class="col-data">' + item.theDate + '</td>'
	      + '			<td class="col-ro">' + item.Ro + '</td>'
	      + '			<td class="col-customer">' + item.CustomerName + '</td>'
	      + '			<td class="col-likelihood">' + item.ReferralLikelihood + '</td>'
	      + '			<td class="col-experience">' + item.CustomerExperience + '</td>'
	      + '		</tr>';
	  });
	  surveysBody += '	</tbody>'
	    + '</table>';

  	jqueryMap.$container.html( configMap.writer_summary_html + summaryBody + configMap.writer_surveys_html + surveysBody );
  };

  onShowSurveys = function( e, user ) {
    $.gevent.publish( 'netpromoter.requestSurveysModel', [ stateMap.user.userName ] );
  };

  onFulfilledSurveysModel = function( e, surveysModel ) {
  	var surveysBody = '';

		$.each(surveysModel.surveys, function(i, item) {
	    surveysBody += '		<tr>'
			  + '			<td class="col-writer">' + item.WriterName + '</td>'
	      + '			<td class="col-data">' + item.theDate + '</td>'
	      + '			<td class="col-ro">' + item.Ro + '</td>'
	      + '			<td class="col-customer">' + item.CustomerName + '</td>'
	      + '			<td class="col-likelihood">' + item.ReferralLikelihood + '</td>'
	      + '			<td class="col-experience">' + item.CustomerExperience + '</td>'
	      + '		</tr>';
	  });
	  surveysBody += '	</tbody>'
	    + '</table>';

  	jqueryMap.$container.html( configMap.writer_surveys_html + surveysBody );
  }

  //------------------------ END EVENT HANDLERS -------------------------------
 

  //----------------------- BEGIN PUBLIC METHODS ------------------------------

 
  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys
  // Arguments  : A map of settable keys and values
  //  * color_name - color to use
  // Settings   :
  //  * configMap.settable_map declares allowed keys
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    spa.util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/
 

  // Begin public method /initModule/
  // Purpose   : Initializes module
  // Arguments :
  //  *  the jquery element used by this feature
  // Returns   : true
  // Throws    : nonaccidental
  //
  initModule = function ( $container ) {
    stateMap.$container = $container;
    setJqueryMap();

    $.gevent.subscribe( $container, 'security.fulfilledUserData',              onFulfilledUserData );
    $.gevent.subscribe( $container, 'netpromoter.showmanagersplash',           onShowManagerSplash );
    $.gevent.subscribe( $container, 'netpromoter.showwritersplash',            onShowWriterSplash );
    $.gevent.subscribe( $container, 'netpromoter.showsurveys',                 onShowSurveys );
    $.gevent.subscribe( $container, 'netpromoter.fulfilledManagerSummaryData', onFulfilledManagerSummaryData );
    $.gevent.subscribe( $container, 'netpromoter.fulfilledWriterSummaryData',  onFulfilledWriterSummaryData );
    $.gevent.subscribe( $container, 'netpromoter.fulfilledWriterModel',        onFulfilledWriterModel );
    $.gevent.subscribe( $container, 'netpromoter.fulfilledSurveysModel',       onFulfilledSurveysModel );
//    $.gevent.publish( 'checksession');
    return true;
  };
  // End public method /initModule/
 

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
  };
  
  //----------------------- END PUBLIC METHODS --------------------------------
}());
 
