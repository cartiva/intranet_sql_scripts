// Module /security/
// Provides login and session management
//

// Publishes:
//   security.loggedin
//   security.loggedout
//   security.loadappnav
//   security.fulfilledappnavdata
// Subscribes to:
//   
car.security = (function() {
  'use strict';

  //------------------------- Begin Module scope variables -------------------------
  var
    // Set constants
    configMap = {
      settable_map : { baseURL: true },
      baseURL      : './php/session.php',
      sessionURL   : '/session',
      loginURL     : '/login',
      logoutURL    : '/logout',
      appnavURL    : '/appnav'
    },
    // Declare all other module scope variables
    user = { loggedIn : false },
    stateMap = { 
      $container : null, 
      loggedIn : false
    },
    jqueryMap = {},

    setJqueryMap, updateSession, 
    configModule, initModule,    
    ajaxGet,
    ajaxPost,
    ajaxDelete,
    updateSession,
    processLogin,
    onLogIn,   
    onLogOut,
    onCheckSession,
    onGetAppNav,
    publishAppNav, 
    onRequestUserData
  ;
  //--------------------------- End Module Scope Variables ------------------------

  //----------------------------- Begin Utility Methods ---------------------------
  ajaxGet = function(url, callback) {
    return $.ajax({ 
      type     : "GET",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      success  : callback });
  }

  ajaxPost = function(url, postData, callback) {
    return $.ajax({ 
      type     : "POST",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      data     : postData,
      success  : callback });
  }

  ajaxDelete = function(url, deleteData, callback) {
    return $.ajax({ 
      type     : "DELETE",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      data     : deleteData,
      success  : callback });
  }

  publishAppNav = function( appNavData ) {
    $.gevent.publish( 'security.fulfilledappnavdata', [ appNavData ] );
  };

  //------------------------------ End Utility Methods ----------------------------

  //------------------------------- Begin DOM Methods -----------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;

    jqueryMap = { $container : $container };
  };
  //-------------------------------- End DOM Methods ------------------------------

  //----------------------------- Begin Event Handlers ----------------------------
  
  updateSession = function( sessionData ) {
    if ( sessionData.loggedIn == '1' ) {
      user = sessionData;
      console.log('security publishing...');
      $.gevent.publish( 'security.loggedin', [user] );
    } else {
      user = { loggedIn : false };
      console.log('security publishing...');
      $.gevent.publish( 'security.loggedout', [user] );
    }
  };

  processLogin = function( loginData ) {
    if ( loginData.loggedIn == '0' ) {
      $.gevent.publish( 'security.invalidloginattempt', [ loginData ]);
    } else {
      updateSession( loginData );
    }
  }

  onCheckSession = function() {
    ajaxGet( configMap.baseURL + configMap.sessionURL, updateSession );
  };

  onLogIn = function( e, logInFormData ) {
    ajaxPost( configMap.baseURL + configMap.loginURL, logInFormData, processLogin );
  };

  onLogOut = function() {
    ajaxPost( configMap.baseURL + configMap.logoutURL, null, updateSession );
  };

  onGetAppNav = function() {
    ajaxGet( configMap.baseURL + configMap.appnavURL + '/ALL', publishAppNav );
  };

  onRequestUserData = function() {
    $.gevent.publish( 'security.fulfilledUserData', [user] );
  }

  //------------------------------ End Event Handlers -----------------------------

  //----------------------------- Begin Public Methods ----------------------------
  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys {key : value}
  // Arguments  : A map of settable keys and values
  //   * color_name - color to use
  // Settings   : 
  //   * configMap.settable_map declares what keys are allowed to be set
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/

  // Begin public method /initModule/
  // Purpose    : Initializes module
  // Arguments  : 
  //   * $container the jquery element used by this feature
  // Returns    : true
  // Throws     : none
  //
  initModule = function ( $container ) {
    //$container.append( configMap.template_html );
    console.log('security initModule');
    stateMap.$container = $container;
    setJqueryMap();

    $.gevent.subscribe( $container, 'shell.login',  onLogIn );
    $.gevent.subscribe( $container, 'shell.logout',  onLogOut );
    $.gevent.subscribe( $container, 'car.checksession',  onCheckSession );
    $.gevent.subscribe( $container, 'shell.getappnavdata', onGetAppNav );
    $.gevent.subscribe( $container, 'security.requestUserData', onRequestUserData );
    return true;
  };
  // End public method /initModule/

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
  };
  //------------------------------ End Public Methods -----------------------------
}());

