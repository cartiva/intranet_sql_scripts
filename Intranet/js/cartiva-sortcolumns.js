(function($) {
  $.sortIt = function(element, options) {
    var defaults = {
      ascendingClass: 'sortup',
      descendingClass: 'sortdown',
      ignoreRowClass: 'nosort',
      ingnoreColClass: 'nosort',
      //cursor: 'pointer',
      onSort: function(){}
    }

    var plugin = this;

    plugin.settings = {};

    var $element = $(element),
        element = element,
        rows = [],
        cols = [];

    plugin.init = function() {
      plugin.settings = $.extend({}, defaults, options);

      $element.find('tr:first th').each(function(i) {
        if ($(this).hasClass(plugin.settings.ingnoreColClass)) {
          return;
        }

        cols.push(this);

        $(this).on('click', {colIndex:i, col:this}, colClick).css('cursor', plugin.settings.cursor);

        $element.find('tr:not(:first, .' + plugin.settings.ignoreRowClass + ')').each(function(i) {
          var $this = $(this);
          var cellVals = [];
          $this.find('td').each(function() {
            var cellVal = $(this).text().replace(/[^A-Za-z0-9\s]/g, '').toUpperCase();
            cellVals.push(cellVal);
          });
          rows.push({values:cellVals, row:this});
        });
      });
    };

    plugin.sort = function(col, dir) {
      var dir = dir || 'asc';
      rows.sort(function(a, b) {
        var aVal = a.values[col];
            bVal = b.values[col];

        if (aVal == bVal) {
          return 0;
        }

        if (isNaN(aVal) || isNaN(bVal)) {
          if (dir == 'asc') {
            if (aVal > bVal) {
              return 1;
            } else {
              return -1;
            }
          } else {
            if (bVal > aVal) {
              return 1;
            } else {
              return -1;
            }
          }
        } else {
          if (dir == 'asc') {
            return aVal - bVal;
          } else {
            return bVal - aVal;
          }
        }
      });

      var sortedRows = $.map(rows, function(o) {return o.row});

      //$(sortedRows).insertAfter($element.find('tbody'));
      $element.find('tbody').children().remove();
      $element.find('tbody').html(sortedRows);

    }

    var colClick = function(event) {
      var $theCol = $(event.data.col);

      if ($theCol.hasClass(plugin.settings.ascendingClass)) {
        $(cols).removeClass(plugin.settings.ascendingClass).removeClass(plugin.settings.descendingClass);
        $theCol.addClass(plugin.settings.descendingClass);
        plugin.sort(event.data.colIndex, 'desc');
        plugin.settings.onSort.call(this, 'desc');
      } else {
        $(cols).removeClass(plugin.settings.ascendingClass).removeClass(plugin.settings.descendingClass);
        $theCol.addClass(plugin.settings.ascendingClass);
        plugin.sort(event.data.colIndex, 'asc');
        plugin.settings.onSort.call(this, 'asc');
      }
    }

    plugin.init();
  }

  $.fn.sortIt = function(options) {
    var args = arguments;

    return this.each(function() {
      if (undefined == $(this).data('sortIt')) {
        var plugin = new $.sortIt(this, options);
        $(this).data('sortIt', plugin)
      } else {
        var $data = $(this).data('sortIt');
        if (typeof options == 'string' && $data[options]) {
          if (typeof $data[options] == 'function') {
            $data[options].apply(this, Array.prototype.splice.call(args, 1));
          }
        }
      }
    });
  }

})(jQuery);