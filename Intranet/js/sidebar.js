function getLeftNavBar(username) {
	return $.getJSON(baseURL + '/leftnavbar/' + username);
}

function getWriterCheckOutStatus(username) {
 	return $.getJSON(baseURL + '/checkoutstatus/' + username);
}
	
function getSideBar(snippet, CheckedOut) {

	var content;
 	content = '';
  $.each(snippet, function(i, item) {
	  content += item.snippet;
	});

 	$('#sidebar-widgets').empty().append('<nav class="menu-item">' +
	  '<a id="header-dashboard" class="mainlink active" href="../main/dashboard.php"><span class="glyphicon glyphicon-home"></span>Your Dashboard<span class="arrow-right"></span></a>' +
	  '<ul class="submenu" id="dashboard">' +
  	'<!-- ADMIN LIST GOES HERE -->' +
  	'<!-- <li><span class="sublink">Settings</span></li> -->' +
  	'<li><a href="#logout"><span class="sublink">All done? Logout</span></a></li>' +
	  '</ul>' +
		'</nav><!-- .menu-item -->').append(content);

	if (myData.globalUser.memberGroup == 'ServiceWriter') {
		if (CheckedOut) {
			$('#dashboard').append('<li><span class="nolink">Thank you for checking out.</span></li>');
    } else {
	    $('#dashboard').append('<li><a href="#sco/checkout"><span class="sublink">Checkout</span></a></li>')	
	  }
	}
	  
	$('#sidebar-widgets').on('click', 'a', function(event) {
		$('#sidebar-widgets *').removeClass('active');
		if ($(this).hasClass('mainlink')) {
			$(this).addClass('active');
		}	else {
			$(this).parent().parent().parent().prev().addClass('active');
		}
	});
	
	$('#sidebar-widgets').on('click', 'span', function(event) {
		if ($(this).data('action') == 'viewagedros') {
		agedROs.insertList('#servicestats-splash');
		}
		if ($(this).data('action') == 'viewwarrantycalls') {
			warrantyROs.insertList('#servicestats-splash');
		}
		if ($(this).data('action') == 'viewnpscore') {
			npScores.insertList('#servicestats-splash');
		}
		if ($(this).data('action') == 'viewnpsurveys') {
			npSurveys.insertList('#servicestats-splash');
		}
	});
}

$(document).ready(function() {
	$.when(getLeftNavBar(myData.globalUser.userName), getWriterCheckOutStatus(myData.globalUser.userName)).done(function(snippets, checkoutdata) {
		getSideBar(snippets[0], checkoutdata[0][0].hasCheckedOutToday === 'true');
		$.gevent.publish('leftnavbarloaded');
	});
});
