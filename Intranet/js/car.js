/*
 * car.js
 * Root namespace module for all Cartiva apps
*/

/*jslint           browser : true,   continue : true,
  devel  : true,    indent : 2,       maxerr  : 50,
  newcap : true,     nomen : true,   plusplus : true,
  regexp : true,    sloppy : true,       vars : false,
  white  : true
*/
/*global $, car */

var car = (function () {
  'use strict';

  var initModule = function ( $container ) {
    console.log( 'car initModule' );
    $.ajaxSetup( { cache: false } );
    car.shell.initModule( $container );
    car.security.initModule( $container );
    car.teampay.initModule( $container.find( '.page-content' ) );
    //car.netpromoter.initModule( $container.find( '.page-content' ) );
    //car.servicecheckout.initModule( $container.find( '.page-content' ) );
    car.dashboard.initModule( $container.find( '.page-content' ));
    console.log( 'car publishing "car.checksession" ');
    $.gevent.publish( 'car.checksession' );
  };

  return { initModule: initModule };
}());
