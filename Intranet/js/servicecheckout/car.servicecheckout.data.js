// Module /servicecheckout.data/
// Provides database access 
//
car.servicecheckout.data = (function() {
  'use strict';

  //------------------------- Begin Module scope variables -------------------------
  var
    // Set constants
    configMap = {
      // Error message for bad data returned
      data_error_html : String ()
        + '<div class="alert alert-warning">'
        + '  <h4>Error</h4>'
        + '  <p>There was an error returning some data. See Jon to '
        + '    bitch him out.</p>'
        + '</div>',
        
      settable_map  : { baseURL: true },
      baseURL       : './php/data.php'
      // summaryURL    : '/npsummaryscores',
      // writerListURL : '/npwriterlist',
      // scoresURL     : '/npscores',
      // surveysURL    : '/npsurveys',
    },
    // Declare all other module scope variables
    stateMap = { 
      $container : null
    },

    configModule,         
    initModule,
    getBaseURL,
    ajaxGet,
    ajaxPost,
    ajaxDelete,
    isJSON
  ;
  //--------------------------- End Module Scope Variables ------------------------

  //----------------------------- Begin Utility Methods ---------------------------
  ajaxGet = function(url, callback) {
    return $.ajax({ 
      type     : "GET",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      success  : callback });
  }

  ajaxPost = function(url, postData, callback) {
    return $.ajax({ 
      type     : "POST",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      data     : postData,
      success  : callback });
  }

  ajaxDelete = function(url, deleteData, callback) {
    return $.ajax({ 
      type     : "DELETE",
      dataType : "JSON",
      cache    : false,
      url      : url, 
      data     : deleteData,
      success  : callback });
  }

  //------------------------------ End Utility Methods ----------------------------

  //------------------------------- Begin DOM Methods -----------------------------
  //-------------------------------- End DOM Methods ------------------------------

  //----------------------------- Begin Event Handlers ----------------------------
  

  //------------------------------ End Event Handlers -----------------------------

  //----------------------------- Begin Public Methods ----------------------------
  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys {key : value}
  // Arguments  : A map of settable keys and values
  //   * color_name - color to use
  // Settings   : 
  //   * configMap.settable_map declares what keys are allowed to be set
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/

  // Begin public method /initModule/
  // Purpose    : Initializes module
  // Arguments  : 
  //   * $container the jquery element used by this feature
  // Returns    : true
  // Throws     : none
  //
  initModule = function ( $container ) {
    console.log('data initModule');
    return true;
  };

  getBaseURL = function() {
    return configMap.baseURL;
  }
  // End public method /initModule/

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
  };
  //------------------------------ End Public Methods -----------------------------
}());
