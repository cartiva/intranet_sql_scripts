/*
car.dashboard.js
* Template for browser feature modules
*/
/*jslint         browser: true,  continue : true,
  devel  : true, indent : 2,     maxerr   : 50,
  newcap : true, nomen  : true,  plusplus : true,
  regexp : true, sloppy : true,  vars     : false,
  white  : true
*/

/*global $, car.dashboard */

car.dashboard = (function () {

  //--------------------- BEGIN MODULE SCOPE VARIABLES ------------------------
  var
    configMap = {
      // Generic 'Soon' dashboard text for ALL users
      dashboard_html : String ()
        + '<h2 class="page-header">Dashboard</h2>'
        + '<section class="panel panel-warning">'
        + '  <div class="panel-heading">'
        + '    <h5 class="panel-title">Dashboard Updates (the new stuff)</h5>'
        + '  </div><!--/.panel-heading-->'
        + '  <div class="panel-body">'
        + '    <ul>'
        + '      <li><strong>Warranty Admins</strong> - '
        + '        PDQ ROs now available for viewing'
        + '      </li>'
        + '    </ul>'
        + "    <p>As you may have noticed, we've been doing lots of "
        + "      updating.  Every time we do, we'll list the newest "
        + '      changes here.  We want to keep you in the loop.  Keep you '
        + '      happy.  As always, if you see anything weird, let us know '
        + '      @ ext. 5979.'
        + '    </p>'
        + '  <img src="./images/dashboard-soon6.png" '
        + '    alt="Dashboard items coming soon. Hold your pants on." '
        + '    style="padding:10px; border:1px solid #CCC;background:#FFF;">'
        + '  </div><!--/.panel-body-->'
        + '</section><!--/.panel.panel-warning-->',
      settable_map : {}
    },
    stateMap = { $container : null },
    jqueryMap = {},
 

    setJqueryMap, configModule, initModule, onLoadDashboard, onDashboardLinkClick;

  //--------------------- END MODULE SCOPE VARIABLES --------------------------
 

  //------------------------ BEGIN UTILITY METHODS ----------------------------
  //------------------------- END UTILITY METHODS -----------------------------
 

  //-------------------------- BEGIN DOM METHODS ------------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;
    jqueryMap = { $container : $container };
  };

  // End DOM method /setJqueryMap/
  //-------------------------- END DOM METHODS --------------------------------
 

  //----------------------- BEGIN EVENT HANDLERS ------------------------------

  onLoadDashboard = function() {
    jqueryMap.$pageContent = jqueryMap.$container.find( ' .page-content-main ' );
    jqueryMap.$pageContent.html( configMap.dashboard_html );
    jqueryMap.$dashboardLink = jqueryMap.$container.find( '#header-dashboard' );
    jqueryMap.$dashboardLink.on( 'click', onDashboardLinkClick );
  };

  onDashboardLinkClick = function() {
    jqueryMap.$pageContent.html( configMap.dashboard_html );
  };
  //------------------------ END EVENT HANDLERS -------------------------------
 

  //----------------------- BEGIN PUBLIC METHODS ------------------------------
  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys
  // Arguments  : A map of settable keys and values
  //  * color_name - color to use
  // Settings   :
  //  * configMap.settable_map declares allowed keys
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    spa.util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/
 

  // Begin public method /initModule/
  // Purpose   : Initializes module
  // Arguments :
  //  *  the jquery element used by this feature
  // Returns   : true
  // Throws    : nonaccidental
  //
  initModule = function ( $container ) {
    console.log( 'car.dashboard initModule' );
    stateMap.$container = $container;
    setJqueryMap();
    $.gevent.subscribe( $container, 'shell.loaddashboard', onLoadDashboard );
    return true;
  };
  // End public method /initModule/
 

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
  };
  //----------------------- END PUBLIC METHODS --------------------------------
}());
 
