/*
* car.nav.js
* Template for browser feature modules
*/
/*jslint         browser: true,  continue : true,
  devel  : true, indent : 2,     maxerr   : 50,
  newcap : true, nomen  : true,  plusplus : true,
  regexp : true, sloppy : true,  vars     : false,
  white  : true
*/

/*global $, car.nav */

car.nav = (function () {

  //--------------------- BEGIN MODULE SCOPE VARIABLES ------------------------
  var
    configMap = {
    	mainnav_html : String ()
			  + '<!-- Main nav bar -->'
        + '<div class="navbar navbar-default navbar-static-top" role="navigation">'
        + '  <div class="container">'
        + '    <div>'
        + '      <ul class="nav navbar-nav navbar-right">'
        + '        <li>'
        + '          <a class="listlink" href="#">'
        + '            <span class="glyphicon sectionicon glyphicon-user"></span> '
        + '            <strong>Employees</strong>'
        + '          </a>'
        + '        </li>'
        + '        <li class="active">'
        + '          <a class="listlink" href="#">'
        + '            <span class="glyphicon sectionicon glyphicon-calendar"></span> '
        + '            <strong>Calendar</strong>'
        + '          </a>'
        + '        </li>'
        + '        <li class="dropdown">'
        + '          <a href="#" class="dropdown-toggle" data-toggle="dropdown">'
        + '            <span class="glyphicon sectionicon glyphicon-inbox"></span> '
        + '            <strong>HR Resources</strong> '
        + '            <b class="caret"></b>'
        + '          </a>'
        + '          <ul class="dropdown-menu">'
        + '            <li>'
        + '              <a class="addvendorlink" href="#">Add a Vendor</a>'
        + '            </li>'
        + '            <li>'
        + '              <a class="vendorlink" href="#">View Vendor List</a>'
        + '            </li>'
        + '          </ul>'
        + '        </li>'
        + '        <li>'
        + '          <a class="restorejoblink" href="#">'
        + '            <span class="glyphicon sectionicon '
        + '              glyphicon-phone"></span> '
        + '            <strong>IT Info</strong></a>'
        + '          </li>'
        + '      </ul>'
        + '    </div>'
        + '  </div><!--/.container-->'
        + '</div><!--/.navbar-static-top-->',

      settable_map : {}
    },
    stateMap = { $container : null },
    jqueryMap = {},
 

    setJqueryMap, configModule, initModule;

  //--------------------- END MODULE SCOPE VARIABLES --------------------------
 

  //------------------------ BEGIN UTILITY METHODS ----------------------------
  //------------------------- END UTILITY METHODS -----------------------------
 

  //-------------------------- BEGIN DOM METHODS ------------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;
    jqueryMap = { $container : $container };
  };

  // End DOM method /setJqueryMap/
  //-------------------------- END DOM METHODS --------------------------------
 

  //----------------------- BEGIN EVENT HANDLERS ------------------------------
  //------------------------ END EVENT HANDLERS -------------------------------
 

  //----------------------- BEGIN PUBLIC METHODS ------------------------------
  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys
  // Arguments  : A map of settable keys and values
  //  * color_name - color to use
  // Settings   :
  //  * configMap.settable_map declares allowed keys
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    spa.util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/
 

  // Begin public method /initModule/
  // Purpose   : Initializes module
  // Arguments :
  //  *  the jquery element used by this feature
  // Returns   : true
  // Throws    : nonaccidental
  //
  initModule = function ( $container ) {
  	console.log( 'nav initModule' );
    stateMap.$container = $container;
    setJqueryMap();
  //  stateMap.$container.append( configMap.mainnav_html );
    return true;
  };
  // End public method /initModule/
 

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
  };
  //----------------------- END PUBLIC METHODS --------------------------------
}());
 
