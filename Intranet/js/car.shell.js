/*
* module_template.js
* Template for browser feature modules
*/

// Publishes:
//   shell.login
//   shell.getappnavdata
//   shell.apploadnavbar
// Subscribes to:
//   security.loggedin
//   security.loggedout
//   security.fulfilledappnavdata

/*jslint         browser: true,  continue : true,
  devel  : true, indent : 2,     maxerr   : 50,
  newcap : true, nomen  : true,  plusplus : true,
  regexp : true, sloppy : true,  vars     : false,
  white  : true
*/

/*global $, spa */

car.shell = (function () {

  //--------------------- BEGIN MODULE SCOPE VARIABLES ------------------------
  var
    configMap = {
      page_title : 'Whatever',
    	// Wrap
      wrap_html : String ()
        + '<div id="wrap">'

        + '<!-- Begin page content -->'
        + '<div class="container">'
        + '  <div class="row page-content">'
        + '  </div><!--/.row.page-content-->'
        + '  <div id="push"></div>'
        + '</div><!--/.container-->'
        + '</div><!-- #wrap -->'
        + '<div id="footer">'
        + '  <div class="container">'
        + '    <p class="muted credit">'
        + '      Application courtesy of Cartiva Development and '
        + '      <a href="http://www.guinness.com/en-us/">Guiness Beer</a>.'
        + '    </p>'
        + '  </div>'
        + '</div><!--/#footer-->'

        + '<!-- Back to top -->'
        + '<a id="back-to-top" href="#top" style="display: none;">'
        + '  <span class="glyphicon glyphicon-chevron-up"></span>'
        + '</a><!--/#back-to-top-->'
        + '<!-- Google Analytics -->'
        + '<script>'
        + '  (function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]'
        + '    ||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new '
        + '    Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];'
        + '    a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})'
        + '  (window,document,"script",'
        + '  "//www.google-analytics.com/analytics.js","ga");'
        + '  ga("create", "UA-40742029-1", "cartiva.com");'
        + '  ga("send", "pageview");'
        + '</script>',

      // Navbar inverse
      navbar_inverse_html : String ()
        + '<!-- Top nav bar -->'
        + '<nav class="navbar navbar-inverse" role="navigation">'
        + '  <div class="container">'
        + '    <div class="navbar-header">'
        + '      <a class="homelink" href="#"><span>Rydell Intranet</span></a>'
        + '    </div><!-- .navbar-header -->'
        + '    <ul id="loginmenu" class="nav navbar-nav navbar-right">'
        + '      <!--<li class="pipe-right dashboard"><a href="#">'
        + '        Choose Dashboard view:</a></li>'
        + '      <li><a href="#">RY1 Service</a></li>'
        + '      <li class="pipe-right"><a href="#">RY1 Body Shop</a></li>'
        + '      <li><a href="#" class="active">RY2 PDQ</a></li>-->'
        + '      <li class="username">'
        + '        <span class="greeting"> '
        + '        </span>'
        + '      </li>'
        + '    </ul><!--/#loginmenu.nav.navbar-right-->'
        + '  </div><!--/.container-->'
        + '</nav><!--/.navbar-inverse-->',

      // Navbar static
      navbar_static_top_html : String ()
        + '<!-- Main nav bar -->'
        + '<div class="navbar navbar-default navbar-static-top" '
        + '  role="navigation">'
        + '  <div class="container">'
        + '    <div>'
        + '      <ul class="nav navbar-nav navbar-right">'

        + '      </ul>'
        + '    </div>'
        + '  </div><!--/.container-->'
        + '</div><!--/.navbar-static-top-->',

      // .col / Page-Content-Main 12 Columns
      wrap_pagecontent_pagecontentmain_col12_html : String ()
        + '<div class="col-md-12 page-content-main">'

        + '</div><!--/.col-md-12.page-content-main-->',

      // Main login screen  
      wrap_pagecontent_pagecontentmain_mainlogin_html : String ()
        + '<h1 class="page-header">Time to Login!!!!!1</h1>'
        + '<form name="login" class="form-horizontal" role="form">'
        + '  <div class="form-group">'
        + '    <label for="username" class="col-sm-2 control-label">Username</label>'
        + '    <div class="col-sm-10">'
        + '      <input type="username" class="form-control" name="username" placeholder="Username" autofocus>'
        + '    </div>'
        + '  </div>'
        + '  <div class="form-group">'
        + '    <label for="password" class="col-sm-2 control-label">Password</label>'
        + '    <div class="col-sm-10">'
        + '      <input type="password" class="form-control text" name="password" placeholder="Password">'
        + '    </div>'
        + '  </div>'
        + '  <!-- <div class="form-group">'
        + '    <div class="col-sm-offset-2 col-sm-10">'
        + '      <div class="checkbox">'
        + '        <label>'
        + '          <input type="checkbox"> Remember me'
        + '        </label>'
        + '      </div>'
        + '    </div>'
        + '  </div> -->'
        + '  <div class="form-group">'
        + '    <div class="col-sm-offset-2 col-sm-10">'
        + '      <!-- <a class="btn" href="#">Login</a> -->'
        + '      <button default class="loginbtn btn btn-default">Sign in</button>'
        + '    </div>'
        + '  </div>'
        + '  <!-- Error message -->'
        + '  <div class="loginerror"></div>'
        + '</form>',


      login_error_message_html : String() 
        + '  <div class="alert alert-danger">'
        + '    <h4>'
        + '      <strong>Oops, wrong username or password.</strong>  Please try again.</h4>'
        + '  </div>',

      // .col / Page-Content-Main sized for Sidebar
      wrap_pagecontent_pagecontentmain_col9_html : String ()
        + '<div class="col-md-9 page-content-main">'
       
        + '</div><!--/.col-md-9.page-content-main-->',

      // Main content area (generic)
      wrap_pagecontent_pagecontentmain_test_html : String ()
        + '<h1 class="page-header">PAGETITLE</h1>'
        + '<p class="lead">Main content area (8 grid units wide)</p>'
        + '<p>Used to be stats container, will have a new name.</p>',

      // .col / Page-Content-Sidebar
      wrap_pagecontent_pagecontentsidebar_col3_html : String ()
        + '<div class="col-md-3 page-content-sidebar" role="navigation">'
        + '  <div class="sidebar-widgets">'
        + '    <!-- Dasnboard Info -->'
        + '    <nav class="menu-item">'
        + '      <a id="header-dashboard" class="mainlink active" '
        + '        href="#">'
        + '        <span class="glyphicon glyphicon-home"></span>Your Dashboard'
        + '        <span class="arrow-right"></span>'
        + '      </a>'
        + '      <ul class="submenu" id="dashboard">'
        + '        <li>'
        + '          <a href="#">'
        + '            <span class="sublink logout">All done? Logout</span>'
        + '          </a>'
        + '        </li>'
        + '      </ul>'
        + '    </nav><!--/.menu-item-->'

        + '  </div><!--/.sidebar-widgets-->'
        + '</div><!--/.col-md-3.page-content-sidebar-->',

      // Wrapper for app-specific left-navbar items
      navwrapper_html : String()
        + '<nav class="menu-item">'       
        + '  <ul class="submenu">'
 
        + '  </ul>'
        + '</nav>',   

      settable_map : {}
    },
    stateMap = { $container : null },
    jqueryMap = {},
 

    setJqueryMap, configModule, initModule,
    onInvalidLogin,
    onLoggedIn,   onLoggedOut, onAppNav,
    onLogOutLinkClick

  //--------------------- END MODULE SCOPE VARIABLES --------------------------
 

  //------------------------ BEGIN UTILITY METHODS ----------------------------
  function replaceAll(str, mapObj){
    var re = new RegExp( Object.keys(mapObj).join("|"), "g");

    return str.replace(re, function(matched){
      return mapObj[matched];
    });
  }
  //------------------------- END UTILITY METHODS -----------------------------
 

  //-------------------------- BEGIN DOM METHODS ------------------------------
  // Begin DOM method /setJqueryMap/
  setJqueryMap = function () {
    var $container = stateMap.$container;
    jqueryMap = { 
      $container : $container 
    };
  };

  // End DOM method /setJqueryMap/
  //-------------------------- END DOM METHODS --------------------------------
 

  //----------------------- BEGIN EVENT HANDLERS ------------------------------

  onInvalidLogin = function( e, loginData ) {
    jqueryMap.$loginError.empty().append( configMap.login_error_message_html );
  }

  onLoggedIn = function( e, user ) {
    jqueryMap.$page_content.empty().append( configMap.wrap_pagecontent_pagecontentsidebar_col3_html );
    jqueryMap.$page_content.append( configMap.wrap_pagecontent_pagecontentmain_col9_html );
    $.gevent.publish( 'shell.initmodules' );
    jqueryMap.$greeting.text('Hi ' + user.firstName );
    jqueryMap.$logOutLink = jqueryMap.$page_content.find('.logout');
    jqueryMap.$logOutLink.on( 'click', onLogOutLinkClick );
    jqueryMap.$leftNav = jqueryMap.$page_content.find('.menu-item');
    // what apps is user authed for -- build hangers
    $.gevent.publish( 'shell.getappnavdata', [] );
//    $.gevent.publish( 'leftnavbarloaded', [ $('.sidebar-widgets') ]);
  };

  onLoggedOut = function() {
    jqueryMap.$page_content.empty().append( configMap.wrap_pagecontent_pagecontentmain_col12_html );
    jqueryMap.$page_content_main = jqueryMap.$container.find( '.page-content-main' );
    jqueryMap.$page_content_main.append( configMap.wrap_pagecontent_pagecontentmain_mainlogin_html );
    jqueryMap.$loginMessage = jqueryMap.$page_content_main.find( '#loginmessage' );
    jqueryMap.$loginButton = jqueryMap.$container.find( 'button.loginbtn' );
    jqueryMap.$loginForm = jqueryMap.$container.find( 'form[name="login"]');
    jqueryMap.$loginError = jqueryMap.$container.find( '.loginerror' );
    jqueryMap.$greeting.text('You are logged out');
    jqueryMap.$loginButton.on( 'click', function( e ) {
      e.preventDefault();
      $.gevent.publish( 'shell.login', [jqueryMap.$loginForm.serialize()] );
    });
  };

  onAppNav = function( e, appNav ) {
    lastAppCode = '';
    var appNavHangers = '';
    $.each( appNav, function( i, item ) {
      if ( item.appCode != lastAppCode ) {
        appNavHangers += '<ul class="app-' + item.appCode + '"></ul>';
        lastAppCode = item.appCode;
      };
    });
    jqueryMap.$leftNav.append( appNavHangers );
    $.gevent.publish( 'shell.apploadnavbar', [ appNav ] );
    $.gevent.publish( 'shell.loaddashboard', [] );
  }

  onLogOutLinkClick = function() {
    $.gevent.publish( 'shell.logout', [] );
  }
  //------------------------ END EVENT HANDLERS -------------------------------
 

  //----------------------- BEGIN PUBLIC METHODS ------------------------------
  // Begin public method /configModule/
  // Purpose    : Adjust configuration of allowed keys
  // Arguments  : A map of settable keys and values
  //  * color_name - color to use
  // Settings   :
  //  * configMap.settable_map declares allowed keys
  // Returns    : true
  // Throws     : none
  //
  configModule = function ( input_map ) {
    spa.util.setConfigMap({
      input_map    : input_map,
      settable_map : configMap.settable_map,
      config_map   : configMap
    });
    return true;
  };
  // End public method /configModule/
 

  // Begin public method /initModule/
  // Purpose   : Initializes module
  // Arguments :
  //  *  the jquery element used by this feature
  // Returns   : true
  // Throws    : nonaccidental
  //
  initModule = function ( $container ) {
  	console.log( 'shell initModule' );
    stateMap.$container = $container;
    setJqueryMap();

    // map = {
    //   PAGETITLE : configMap.page_title
    // };
    // header = replaceAll( configMap.tech_summary_html, map );

    stateMap.$container.append( configMap.wrap_html );
    jqueryMap.$wrap = $container.find( '#wrap' );
    jqueryMap.$wrap.prepend( configMap.navbar_static_top_html );
    jqueryMap.$wrap.prepend( configMap.navbar_inverse_html );
    jqueryMap.$greeting = jqueryMap.$wrap.find( '.greeting' );
    jqueryMap.$page_content = $container.find( '.page-content' );
    $.gevent.subscribe( $container, 'security.invalidloginattempt', onInvalidLogin );
    $.gevent.subscribe( $container, 'security.loggedin', onLoggedIn );
    $.gevent.subscribe( $container, 'security.loggedout', onLoggedOut );
    $.gevent.subscribe( $container, 'security.fulfilledappnavdata', onAppNav );
    return true;
  };
  // End public method /initModule/
 

  // return public methods
  return {
    configModule : configModule,
    initModule   : initModule
  };
  //----------------------- END PUBLIC METHODS --------------------------------
}());
 
