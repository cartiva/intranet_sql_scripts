function getCookie(name) {
  var re = new RegExp(name + "=([^;]+)");
  var value = re.exec(document.cookie);
  return (value != null) ? unescape(value[1]) : null;
}

function getDate(offset) {
  offset = offset || 0;

  var theDate = new Date();
  theDate.setDate(theDate.getDate() + offset);

  var twoDigitMonth = theDate.getMonth() + 1 + '';
  if (twoDigitMonth.length == 1) {
    twoDigitMonth = '0' + twoDigitMonth;
  }

  var twoDigitDay = theDate.getDate() + '';
  if (twoDigitDay.length == 1) {
 	twoDigitDay = '0' + twoDigitDay;
  }

  return twoDigitMonth + '/' + twoDigitDay + '/' + theDate.getFullYear();
}

function initModal() {
  if (!($('div').hasClass('mask'))) {
    $(document.body).append('<div id="mask"></div>');
  }
  $(window).resize(function () {
    var box = $('#boxes .window'),
      maskHeight = $(document).height(),
      maskWidth = $(window).width(),
      winH = $(window).height(),
      winW = $(window).width();
    //Set height and width to mask to fill up the whole screen
    $('#mask').css({'width': maskWidth, 'height': maskHeight});
    //Get the window height and width
    //Set the popup window to center
    box.css('top', winH / 2 - box.height() / 2);
    box.css('left', winW / 2 - box.width() / 2);
  });
}

function openModal(target) {
  var maskHeight = $(document).height(),
    maskWidth = $(window).width(),
    winH = $(window).height(),
    winW = $(window).width();
  //Set height and width to mask to fill up the whole screen
  $('#mask').css({'width': maskWidth, 'height': maskHeight});
  //transition effect   
  $('#mask').fadeIn(500);
  //$('#mask').fadeTo("fast",0.9);  
  //Get the window height and width
  //Set the popup window to center
  $('#modal').css('top', winH / 2 - $('#modal').height() / 2);
  $('#modal').css('left', winW / 2 - $('#modal').width() / 2);
  //transition effect
  $('#modal').fadeIn(500);
}

function closeModal(target) {
  $('#mask').hide();
  $('#modal').hide();
}

function sortBy(field, direction, metaData) {
  return function (a, b) {
    var result,
        A = a[field],
        B = b[field];
    if (metaData[field].type == 'date') {
      A = new Date(a[field]);
      B = new Date(b[field]);
    };
    if (A < B) {
      result = -1;
    } else if (A > B) {
      result = +1;
    } else {
      result = 0;
    };
    if (direction == 'descending') {
      result = result * -1;
    }
    return result;
    //return ((a.field < b.field) ? -1 : (a.field > b.field) ? +1 : 0);
  }
}