var modal = (function(){
	var 
	method = {},
	$overlay,
	$modal,
	$content,
	$close,
	$cancelbutton,
	$submitbutton;

	// Center the modal in the viewport
	method.center = function () {
		var top, left;

		top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
		left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

		$modal.css({
			top:top + $(window).scrollTop(), 
			left:left + $(window).scrollLeft()
		});
	};

	// Open the modal
	method.open = function (settings) {
		$content.empty().append(settings.content);
		$cancelbutton = $(settings.cancelselector);
		$cancelbutton.on('click', function(e){
			e.preventDefault();
			method.close();

		});
		$submitbutton = $(settings.submitselector);
		$submitbutton.on('click', function() { 
			window.location.hash = settings.submitaction 
		});

		$modal.css({
			width: settings.width || 'auto', 
			height: settings.height || 'auto'
		});

		method.center();
		$(window).bind('resize.modal', method.center);
		$modal.show();
		$overlay.show();

	//Print button stuff
	$('#printBtn').on('click', function(){
		var $thediv = $('#lemodal');
		var printWindow = window.open('','printWindow');
		printWindow.document.open();
		printWindow.document.write('<head><link rel="stylesheet" href="../css/stylesheet_sub.css" type="text/css" /><link rel="stylesheet" href="../css/print.css" type="text/css" /></head>');
		printWindow.document.write($thediv.html());
		printWindow.document.close();
		printWindow.print();
		printWindow.close(); 
	})
};

	// Close the modal
	method.close = function () {
		$modal.hide();
		$overlay.hide();
		$content.empty();
		$(window).unbind('resize.modal');
	};

	// Generate the HTML and add it to the document
	$overlay = $('<div id="leoverlay"></div>');
	$modal = $('<div id="lemodal"></div>');
	$content = $('<div id="lecontent"></div>');
	$close = $('<a id="leclose" href="#">close</a>');

	$modal.hide();
	$overlay.hide();
	$modal.append($content, $close);

	$(document).ready(function(){
		$('body').append($overlay, $modal);						
	});

	$close.on('click', function(e){
		e.preventDefault();
		method.close();
	});

	return method;
}());

