<?php

// Database dependencies
// Uses SCOTEST/SCO.ADD database
// -- Stored Procs
// --   phpLogin
// --   phpLogout
// --   phpGetSession
// --   phpSetSession

define('CONNECTION_STRING', "DataDirectory=\\\\67.135.158.12:6363\\Advantage\\scotest\\sco.add;TrimTrailingSpaces=True");

// This function provides a database connection
function db_connect() {
  $connection_string = CONNECTION_STRING;

  $connection = ads_connect($connection_string, "AdsSys", "cartiva");
  if (!$connection) {
    throw new Exception('Could not connect to database server');
  } else {
    return $connection;
  }
}

// Validates login by checking username and password in the People table
// If it is a valid login it sets the browser's sessionid cookie to coincide with the Sessions table
function login() {
//  header('Content-type: application/json; charset=utf-8');
  $sql = "execute procedure phpIntranetLogin(:UserName, :Password)";
  $result = ads_prepare(db_connect(), $sql);
  $params = array();
  $params['UserName'] = $_POST['username'];
  $params['Password'] = $_POST['password'];
  $success = ads_execute($result, $params);
  $row = ads_fetch_array($result);
  if ($row['sessionID'] != null) {
    header("Set-Cookie: sessionid=" . $row['sessionID'] . "; path=/; expires=".gmdate('D, d M Y H:i:s \G\M\T',time()+2*60*60) . '"', false);
  }
  return json_encode($row);
}

function logout() {
  $sql = "execute procedure phpLogout(:sessionid)";
  $result = ads_prepare(db_connect(), $sql);
  $params = array();
  $params['sessionid'] = $_COOKIE['sessionid'];
  $success = ads_execute($result, $params);
  $row = ads_fetch_array($result);
  header("Set-Cookie: sessionid='deleted'; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT");
  return json_encode($row);
}


function getSession() {
  header('Content-type: application/json; charset=utf-8');
  if (isset($_COOKIE['sessionid'])) {
    $sql = "execute procedure phpGetSession(:sessionid)";
    $result = ads_prepare(db_connect(), $sql);
    $params = array();
    $params['sessionid'] = $_COOKIE['sessionid'];
    $success = ads_execute($result, $params);
    $row = ads_fetch_array($result);
    if ( $row ) {
      return json_encode($row);
    }
      return json_encode('Stored Proc Failure');
  } else {
    return json_encode('none');
  }
}

function getAuthorizedApps() {
  header('Content-type: application/json; charset=utf-8');
  if (isset($_COOKIE['sessionid'])) {
    $sql = "execute procedure getApplicationAuthorization(:sessionid)";
    $result = ads_prepare(db_connect(), $sql);
    $params = array();
    $params[':sessionid'] = $_COOKIE['sessionid'];
    $success = ads_execute($result, $params);
    $data = array();
    while($row = ads_fetch_array($result)) {
      $data[] = $row;
    };
    return json_encode($data);
  } else {
    return json_encode('none');
  }
}

function getAppNavigation( $appCode ) {
  header('Content-type: application/json; charset=utf-8');
  if (isset($_COOKIE['sessionid'])) {
    $sql = "execute procedure getApplicationNavigation(:sessionid, :appcode)";
    $result = ads_prepare(db_connect(), $sql);
    $params = array();
    $params[':sessionid'] = $_COOKIE['sessionid'];
    $params[':appcode'] = $appCode;
    $success = ads_execute($result, $params);
    $data = array();
    while($row = ads_fetch_array($result)) {
      $data[] = $row;
    };
    return json_encode($data);
  } else {
    return json_encode('none');
  }
}

function setSession() {
  $sql = "execute procedure phpSetSession(:sessionid)";
  $result = ads_prepare(db_connect(), $sql);
  $params = array();
  $params[':sessionid'] = $_COOKIE['sessionid'];
  $params[':state'] = $_POST['state'];
  $success = ads_execute($result, $params);
  $row = ads_fetch_array($result);
  return json_encode($row);
}


// check for path elements
if (!empty($_SERVER['PATH_INFO'])) {
  $path = $_SERVER['PATH_INFO'];
  $path_params = explode("/", $path);
}

// check the http method and perform an appropriate query
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

  //log_db($_POST);

  if (!empty($path_params)) {

    switch($path_params[1]) {

      case 'login':
        echo login();
      break;

      case 'session':
        echo setSession();
      break;

      case 'logout':
        echo logout();
      break;

    }
  }

} else if ($_SERVER['REQUEST_METHOD'] == 'GET') {

  if (!empty($path_params)) {

    switch($path_params[1]) {

      case 'session':
        echo getSession();
      break;

      case 'authorizedapps':
        echo getAuthorizedApps();
      break;

      case 'appnav':
        echo getAppNavigation($path_params[2]);
      break;

      default: 
        header('HTTP/1.1 400 Bad Request');
    }
  }
  
} 
?>