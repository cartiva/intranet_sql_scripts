<?php

define('ADS_SCO_CONNECTION_STRING', "DataDirectory=\\\\67.135.158.12:6363\\Advantage\\scotest\\sco.add;TrimTrailingSpaces=True");
define('ADS_NP_CONNECTION_STRING', "DataDirectory=\\\\67.135.158.12:6363\\Advantage\\NetPromoter\\NetPromoter.add;TrimTrailingSpaces=True");
define('ADS_USERNAME', 'AdsSys');
define('ADS_PASSWORD', 'cartiva');

//this function provides an ADS database connection
function ads_db_connect($db) {
  switch ($db) {
    case 'SCO':
      return ads_connect(ADS_SCO_CONNECTION_STRING, ADS_USERNAME, ADS_PASSWORD);
      break;

    case 'NP':
      return ads_connect(ADS_NP_CONNECTION_STRING, ADS_USERNAME, ADS_PASSWORD);
      break;
  }
}

function get_resource($resource, $path) {

  $data = array();
  switch ($resource) {
    case 'connection':
      return json_encode(ADS_DDS_CONNECTION_STRING);
      break;

    case 'techsummary':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getTPTechSummary(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'techdetails':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getTPTechDetail(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'techtotals':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getTPTechTotal(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'techotherdetails':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getTechOtherHoursDetail(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'techothertotals':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getTechOtherHoursTotal(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'managersummary':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getManagerSummary(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'shoptotals':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getShopTotals(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'teamtotals':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getTeamTotals(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'employeetotals':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getEmployeeTotals(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'openros':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getTPTechROsOpen(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'adjustments':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getTechROsAdjustmentsByDay(:sessionid, :techid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $params['techid'] = '';
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'dailyflaggedros':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getTechROsByDay(:sessionid, :techid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $params['techid'] = '';
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'closedros':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getTPTechROsClosed(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    case 'techpaycalc':
      $conn = ads_db_connect('SCO');
      $sql = "execute procedure getTechPayCalc(:sessionid)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['sessionid'] = $_COOKIE['sessionid'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    // npsummaryscores/{username}
    case 'npsummaryscores':
      $conn = ads_db_connect('NP');
      $sql = "execute procedure getNPSummary(:username)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['username'] = $path[2];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    // npwriterlist/{username}
    case 'npwriterlist':
      $conn = ads_db_connect('NP');
      $sql = "execute procedure getNPWriterList(:username)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['username'] = $path[2];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    // npscores/{username}
    case 'npscores':
      $conn = ads_db_connect('NP');
      $sql = "execute procedure getNetPromoterScores(:username)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['username'] = $path[2];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;

    // npsurveys/{username}
    case 'npsurveys':
      $conn = ads_db_connect('NP');
      $sql = "execute procedure getNetPromoterSurveys(:username)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['username'] = $path[2];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
      break;
  }
};

function post_resource($resource, $path) {
  $data = array();
  switch ($resource) {

    case 'partsjob':
      $sql = "execute procedure RestorePartsOrdersByRONumber(:RO)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['RO'] = $path[2];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
    break;

    case 'vendors':
      $sql = "execute procedure addVendor(:vendorName, :phoneNumber)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['vendorName'] = $_POST['vendorName'];
      $params['phoneNumber'] = $_POST['phoneNumber'];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
    break;

  }
};

function put_resource($resource, $path) {
  $data = array();
  switch ($resource) {
  }
};

function delete_resource($resource, $path) {
  $data = array();
  switch ($resource) {

    case 'partsjob':
      $sql = "execute procedure ExcludePartsOrdersByRONumber(:RO)";
      $result = ads_prepare($conn, $sql);
      $params = array();
      $params['RO'] = $path[2];
      $success = ads_execute($result, $params);
      $data = array();
      while($row = ads_fetch_array($result)) {
        $data[] = $row;
      };
      return json_encode($data);
    break;
  }
};

function main() {
  // Put URL path parameters into an array 
  if (!empty($_SERVER['PATH_INFO'])) {
    $path = $_SERVER['PATH_INFO'];
    $path_params = explode("/", $path);
  };

  if (!empty($path_params)) {

    $validResources = array('techsummary', 'techdetails', 'techtotals', 'techotherdetails', 'techothertotals', 'dailyflaggedros',
      'managersummary', 'shoptotals', 'teamtotals', 'employeetotals', 'openros', 'adjustments', 'closedros', 'techpaycalc',
      'npsummaryscores', 'npwriterlist', 'npscores', 'npsurveys'
      );
    $resource = $path_params[1];

    if (in_array($resource, $validResources)) {
      switch ($_SERVER['REQUEST_METHOD']) {
        case 'GET':
          $data = get_resource($resource, $path_params);
          echo $data;
          break;

        case 'POST':
          $data = post_resource($resource, $path_params);
          echo $data;
          break;

        case 'PUT':
          $data = put_resource($resource, $path_params);
          echo $data;
          break;

        case 'DELETE':
          $data = delete_resource($resource, $path_params);
          echo $data;
          break;
      }
    } else {
      header('HTTP/1.1 400 Bad Request');
    };
  };
};

main();
?>
