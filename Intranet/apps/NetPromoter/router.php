<?php
require_once('db_funcs.php');
set_time_limit(60000);

// check for path elements
if (!empty($_SERVER['PATH_INFO'])) {
	$path = $_SERVER['PATH_INFO'];
	$path_params = explode("/", $path);
}

// check the http method and perform an appropriate query
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	//log_db($_POST);

} else if ($_SERVER['REQUEST_METHOD'] == 'GET') {

	if (!empty($path_params)) {

		switch($path_params[1]) {

			/*********************
			// NPSCORE STUFF
			**********************/

			// npsummaryscores/{username}
			case 'npsummaryscores':
			if (!is_email($path_params[2])) {
				show_error(400);
			} else {
				$npsummaryscores = get_npsummaryscores($path_params[2]);
				echo json_encode($npsummaryscores);
			}
			break;

			// npwriterlist/{username}
			case 'npwriterlist':
			if (!is_email($path_params[2])) {
				show_error(400);
			} else {
				$npwriterlist = get_npwriterlist($path_params[2]);
				echo json_encode($npwriterlist);
			}
			break;

			// npscores/{username}
			case 'npscores':
			if (!is_email($path_params[2])) {
				show_error(400);
			} else {
				$npscores = get_npscores($path_params[2]);
				echo json_encode($npscores);
			}
			break;

			// npsurveys/{username}
			case 'npsurveys':
			if (!is_email($path_params[2])) {
				show_error(400);
			} else {
				$npsurveys = get_npsurveys($path_params[2]);
				echo json_encode($npsurveys);
			}
			break;

			// connection
			case 'connection':
			$connection = get_connection();
			echo json_encode($connection);
			break;

			default: 
				show_error(400);
		}
	}
	
} else if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
	

} else if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {

}



// This function returns an error based on an http status code and optional 
// message
function show_error($status_code, $message = "") {
	$header = "";

	switch ($status_code) {
		case 400:
			$header = "HTTP/1.1 400 Bad Request";
		break;

		case 404:
			$header = "HTTP/1.1 404 Not Found";
		break;

		case 500:
			$header = "HTTP/1.1 500 Internal Server Error";
		break;
	}

	header($header);

}

// check if a string is an email address
function is_email($string) {
	if (stripos($string, "@") === false) {
		return false;
	} else {
		return true;
	}
}


?>