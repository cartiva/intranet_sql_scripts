function getNPSummaryScores(user) {
  return $.getJSON(npbaseURL + '/npsummaryscores/' + user);
}

function getNPWriterList(user) {
  return $.getJSON(npbaseURL + '/npwriterlist/' + user);
}

function getNPScoresData(user) {
  return $.getJSON(npbaseURL + '/npscores/' + user);
}

function getNPSurveysData(user) {
  return $.getJSON(npbaseURL + '/npsurveys/' + user);
}
