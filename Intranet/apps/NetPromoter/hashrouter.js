// Net Promoter Routes
// #np/managersummary -- Manager Summary
// #np/managersurveys -- Manager Surveys
// #np/writersummary -- Writer Summary
// #np/writer/ -- Manager drill down on writer
// #np/aboutnp -- About NP

$(window).bind('hashchange', function(e)  {

  // Manager Summary with Writer List
  if (location.hash === '#np/managersummary') {
    $('.stats-container').empty().append('<h4>Loading Net Promoter Summary...</h4>');

    $.when(netPromoter.summaryMarkup(myData.globalUser.userName), 
           netPromoter.writerListMarkup(myData.globalUser.userName))
      .then(function(summaryhtml, writerlisthtml) {
      $('.stats-container').empty().append('<h4>Net Promoter Summary (last 90 days)</h4>');
      $('.stats-container').append('<p>These are combined Service Writers Net Promoter stats from the last 90 days. Clicking the table will take you to a list of all Service Writer surveys.</p>');
      $('.stats-container').append('<a href="#np/aboutnp"><aside>About Net Promoter</aside></a>')
      $('.stats-container').append(summaryhtml);
      $('.stats-container table').addClass('manager-summary');
      $('.stats-container').append('<h4>Net Promoter Writers List</h4>');
      $('.stats-container').append('<p>These are Service Writers that have Net Promoter Scores.</p>');
      $('.stats-container').append(writerlisthtml);
      $('.npscoresummary').on('click', 'tr', function() { window.location.hash = '#np/managersurveys'});
    });

    window.location.hash = '';
  }

  // Manager all surveys
  if (location.hash === '#np/managersurveys') {
    $('.stats-container').empty().append('<h4>Loading Net Promoter Surveys...</h4>');

    netPromoter.surveysMarkup(myData.globalUser.userName)
      .done(function(html) {
      $('.stats-container').empty().append('<h4>Net Promoter Surveys (last 90 days)</h4>');
      $('.stats-container').append('<p>These are Net Promoter surveys for all the Service Writers from the last 90 days.  You can sort by clicking the column headers.</p>');
      $('.stats-container').append(html);
      $('.npsurveys').sortIt();
    });

    window.location.hash = '';
  }

  // Manager drilling down on writer
  if (location.hash.indexOf("#np/writer/") != -1) {
    $('.stats-container').empty().append('<h4>Loading Net Promoter Summary...</h4>');
  
    $.when(netPromoter.summaryMarkup(location.hash.split("/").pop()), 
      netPromoter.surveysMarkup(location.hash.split("/").pop()))
      .then(function(summaryhtml, surveyshtml) {
        $('.stats-container').empty().append('<h4>Net Promoter Summary (last 90 days)</h4>');
        $('.stats-container').append('<p>This is the Net Promoter summary from the last 90 days for an individual Service Writer.</p>');
        $('.stats-container').append(summaryhtml);
        $('.stats-container').append('<h4>Net Promoter Surveys (last 90 days)</h4>');
        $('.stats-container').append('<p>These are Net Promoter surveys from the last 90 days for an individual Service Writer.</p>');
        $('.stats-container').append(surveyshtml);
        $('.npsurveys').sortIt();
    });

    window.location.hash = '';
  }

  // Writer summary
  if (location.hash === "#np/writersummary") {
    $('.stats-container').empty().append('<h4>Loading Net Promoter Summary...</h4>');

    $.when(netPromoter.summaryMarkup(myData.globalUser.userName), 
           netPromoter.surveysMarkup(myData.globalUser.userName))
      .then(function(summaryhtml, surveyshtml) {
        $('.stats-container').empty().append('<h4>Net Promoter Summary (last 90 days)</h4>');
        $('.stats-container').append('<p>This is your Net Promoter summary from the last 90 days.</p>');
        $('.stats-container').append('<a href="#np/aboutnp"><aside>About Net Promoter</aside></a>')
        $('.stats-container').append(summaryhtml);
        $('.stats-container').append('<h4>Net Promoter Surveys (last 90 days)</h4>');
        $('.stats-container').append('<p>These are your Net Promoter surveys from the last 90 days.</p>');
        $('.stats-container').append(surveyshtml);
        $('.npsurveys').sortIt();
    });

    window.location.hash = '';
  }

  // About NP
  if (location.hash === "#np/aboutnp") {
    //$('.stats-container').empty().append('<h4>Loading page...</h4>');
    
      $('.stats-container').empty().append('<h4>About Net Promoter</h4>');
      $('.stats-container').append('<p><small>This information was taken from the Net Promoter Website by Bain & Company</small></p>')
      $('.stats-container').append('<p>In 2003, Fred Reichheld, a partner at Bain & Company, created a new way of measuring how well an organization treats the people whose lives it affects—how well it generates relationships worthy of loyalty.  He called that metric the Net Promoter Score<sup>SM</sup>, or NPS&reg;.</p><p>Net Promoter System<sup>SM</sup> is based on the fundamental perspective that every company' + "'" + 's customers can be divided into three categories. <strong>Promoters</strong> are loyal enthusiasts who keep buying from a company and urge their friends to do the same. <strong>Passives</strong> are satisfied but unenthusiastic customers who can be easily wooed by the competition. And <strong>detractors</strong> are unhappy customers trapped in a bad relationship.</p>');
      $('.stats-container').append('<h5>Measuring your Net Promoter Score</h5>')
      $('.stats-container').append('<p>The best way to gauge the efficiency of a company' + "'" + 's growth engine is to take the percentage of customers who are promoters and subtract the percentage who are detractors. This equation is how we calculate a Net Promoter Score for a company:</p>')
      $('.stats-container').append('<img src="../images/nps-measure-of-success.jpg" alt="Net Promoter: Measure of Success" style="border: 1px solid #eee;padding:30px;background:#FFF;" />');
      $('.stats-container').append('<p>While easy to grasp, NPS metric represents a radical change in the way companies manage customer relationships and organize for growth. Rather than relying on notoriously ineffective customer satisfaction surveys, companies can use NPS to measure customer relationships as rigorously as they now measure profits. What' + "'" + 's more, NPS finally enables CEOs to hold employees accountable for treating customers right. It clarifies the link between the quality of a company' + "'" + 's customer relationships and its growth prospects.</p>')
      $('.stats-container').append('<p>Want to read more?  Check out the <a href="http://www.netpromotersystem.com/about/measuring-your-net-promoter-score.aspx" target="_blank">Net Promoter System website</a> for plenty on the subject.</p>')

    window.location.hash = '';
  }

});
