<?php
define('CONNECTION_STRING', "DataDirectory=\\\\67.135.158.12:6363\\Advantage".
		"\\NetPromoter\\NetPromoter.add;TrimTrailingSpaces=True");

//this function provides a database connection
function db_connect() {
	$connection_string = CONNECTION_STRING;

	$connection = ads_connect($connection_string, "AdsSys", "cartiva");
	if (!$connection) {
		throw new Exception('Could not connect to database server');
	} else {
		return $connection;
	}
}

// get the connection string
function get_connection() {
	return CONNECTION_STRING;
}

// get department npsummaryscores
function get_npsummaryscores($username) {
	$params = array();
	$params[0] = $username;
    $data = call_stored_proc("GetNPSummary", $params, TRUE);
    return $data;
}

// get department writer list
function get_npwriterlist($username) {
	$params = array();
	$params[0] = $username;
    $data = call_stored_proc("GetNPWriterList", $params, TRUE, 'ScoreAsInteger DESC');
    return $data;
}

// get department npscores
function get_npscores($username) {
	$params = array();
	$params[0] = $username;
    $data = call_stored_proc("GetNetPromoterScores", $params, TRUE, 'DeptWriter, ScoreAsInteger DESC');
    return $data;
}

// get NPSurveys
function get_npsurveys($username) {
	$params = array();
	$params[0] = $username;
    $data = call_stored_proc("GetNetPromoterSurveys", $params);
    return $data;
}



// call a stored proc, log it, and return the result
function call_stored_proc($proc_name, $params = array(), $return_data = TRUE,
		$orderby = "") {

	$conn = db_connect();

	if (!empty($params)) {
		$params =  "'" .implode("','", $params) . "'";
	} else {
		$params = "";
	}

	if (!empty($orderby)) {
		$query = "SELECT * FROM (EXECUTE PROCEDURE $proc_name($params)) 
			a ORDER BY $orderby";
	} else {
		$query = "EXECUTE PROCEDURE $proc_name($params)";
	}

	$result = ads_do($conn, $query);
	if (ads_error() != '') {
  	  header("HTTP/1.1 500 Internal Server Error");
	}
	// build the result data
	$data = array();

	if (!empty($result) && $return_data) {
		while($row = ads_fetch_array($result)) {
			$data[] = $row;
		}
	}

	ads_close($conn);

	/* //log the information about the call
	$info = array();
	$info['queryUsed'] = $query;
	$info['calledBy'] = "webservice db_funcs.php";
	$info['storedProc'] = $proc_name;

	if ($return_data) {
		$info['result'] = json_encode($data);
	} else {
		$info['result'] = ("insert");
	} */

	// log_stored_proc($info);

	// return the data
	return $data;
}