function newNetPromoter() {
	'use strict'
	return {

		summaryMarkup: function(userName) {

			function createSummaryMarkup(data) {
  	    var content = '';
			  content += '<table id="weekly" class="splash npscoresummary">';
			  content += '	<thead>';
				content += '		<tr>';
				content += '			<th data-col="sent"><span></span>Sent</th>';
				content += '			<th data-col="returned"><span></span>Returned</th>';
				content += '			<th data-col="responserate"><span></span>Response Rate</th>';
				content += '			<th data-col="netpromoterscore"><span></span>NP Score (last 90 days)</th>';
				content += '		</tr>';
				content += '	</thead>';
				content += '	<tbody>';
				content += '		<tr>';
				content += '			<td class="col-sum col-sent">' + data.Sent + '</td>';
				content += '			<td class="col-sum col-returned">' + data.Returned + '</td>';
				content += '			<td class="col-sum col-responserate">' + data.ResponseRate + '</td>';
				content += '			<td class="col-sum col-netpromoterscore">' + data.NetPromoterScore + '</td>';
				content += '		</tr>';
				content += '	</tbody>';
		  	content += '</table>';
		  	return content;
			}

		  return getNPSummaryScores(userName).then(function(data) { 
		  	return createSummaryMarkup(data[0]); 
		  });

		},

		writerListMarkup: function(userName) {

			function createWriterListMarkup(data) {
			  var content = '';
				content += '<table id="weekly" class="splash npscores">';
				content += '	<thead>';
				content += '		<tr id="headings">';
				content += '			<th data-col="areawriter"><span class="arrow"></span>Writer</th>';
				content += '			<th data-col="sent"><span class="arrow"></span>Sent</th>';
				content += '			<th data-col="returned"><span class="arrow"></span>Returned</th>';
				content += '			<th data-col="responserate"><span class="arrow"></span>Response Rate</th>';
				content += '			<th data-col="netpromoterscore"><span class="arrow"></span>NP Score (last 90 days)</th>';
				content += '		</tr>';
				content += '	</thead>';
				content += '	<tbody>';
			  $.each(data, function(i, item) {
				  content += '		<tr>';
			  	content += '			<td class="col-writer"><a href="#np/writer/' + item.eeUserName + '"">' + item.FirstName + ' ' + item.LastName + '</a></td>';
				  content += '			<td class="col-sent">' + item.Sent + '</td>';
				  content += '			<td class="col-returned">' + item.Returned + '</td>';
				  content += '			<td class="col-responserate">' + item.ResponseRate + '</td>';
				  content += '			<td class="col-netpromoterscore">' + item.NetPromoterScore + '</td>';
				  content += '		</tr>';
			  });
				content += '	</tbody>';
			  content += '</table>';
			  return content;
			};

		  return getNPWriterList(userName).then(function(data) { 
		  	return createWriterListMarkup(data); 
		  });

		},

		surveysMarkup: function(userName) {

			function createSurveysMarkup(data) {
			  var content = '';
			  content += '<table id="weekly" class="splash npsurveys">';
			  content += '	<thead>';
				content += '		<tr id="headings">';
				content += '			<th data-col="writer"><span class="arrow"></span>Writer</th>';
				content += '			<th data-col="date" class="col-date"><span class="arrow"></span>Date</th>';
				content += '			<th data-col="ro"><span class="arrow"></span>RO</th>';
				content += '			<th data-col="customer"><span class="arrow"></span>Customer</th>';
				content += '			<th data-col="likelihood"><span class="arrow"></span>Referral Likelihood</th>';
				content += '			<th data-col="experience"><span class="arrow"></span>Experience</th>';
				content += '		</tr>';	
				content += '	</thead>';
				content += '	<tbody>';
				$.each(data, function(i, item) {
			    content += '		<tr>';
					content += '			<td class="col-writer">' + item.WriterName + '</td>';
			    content += '			<td class="col-data">' + item.theDate + '</td>';
			    content += '			<td class="col-ro">' + item.Ro + '</td>';
			    content += '			<td class="col-customer">' + item.CustomerName + '</td>';
			    content += '			<td class="col-likelihood">' + item.ReferralLikelihood + '</td>';
			    content += '			<td class="col-experience">' + item.CustomerExperience + '</td>';
			    content += '		</tr>';
			  });
			  content += '	</tbody>';
			  content += '</table>';
			  return content;
			};

		  return getNPSurveysData(userName).then(function(data) {
	      return createSurveysMarkup(data);
     	});
		}
	}
}

var netPromoter = newNetPromoter();
