  <section class="stats-container">
  	<!-- ------------- Tech Splash Page ------------- -->
	<h4>Pay-period hours/rates for Billy Jenkins</h4>
	<h5><span id="fromDate">November 3 </span> - <span id="thruDate">November 16, 2013 </span></h5>
	<aside>
		<h5>Current Pay Rate: <span>$12.51 </span></h5>
	</aside>

	<table class="weekly"> 
		<thead> 
			<tr> 
				<th class="column">Day</th> 
				<th>Flag Hours</th> 
				<th class="Today">Clock Hours</th>
				<th>Team Prof.</th> 
				<th>Pay Hours</th> 
				<th>Regular Hours</th>
			</tr> 
		</thead> 

		<tbody> 
			<tr> 
				<td class="column">Monday, November 3</td> 
				<td>9hrs</td>
				<td>8hrs</td>
				<td>82%</td>
				<td>10hrs</td>
				<td>12hrs</td>
			</tr>
		</tbody> 
	</table>

  </section><!-- /stats container -->
