<?php
define('CONNECTION_STRING', "DataDirectory=\\\\67.135.158.12:6363\\Advantage".
		"\\dpsvseries\\dpsvseries.add;TrimTrailingSpaces=True");

//this function provides a database connection
function db_connect() {
	$connection_string = CONNECTION_STRING;

	$connection = ads_connect($connection_string, "AdsSys", "cartiva");
	if (!$connection) {
		throw new Exception('Could not connect to database server');
	} else {
		return $connection;
	}
}

function get_resource($conn, $resource, $format='html') {

	function createHTMLtable($tabledata = array(), $id) {
        $table = '<table id="'. $id .'"  class="table table-hover table-bordered table-striped"><thead><tr>';
        if (count($tabledata) > 0) {
	        foreach($tabledata[0] as $key => $value) {
	       		$table .= "<th>".$key."</th>";
	        }
        }
        $table .= '</tr></thead><tbody>';
        foreach ($tabledata as $row) {
        	$table .= '<tr>';
        	foreach ($row as $key => $value) {
        		$table .= "<td>".$value."</td>";
        	}
        	$table .= '</tr>';
        }
        $table .= '</tbody></table>';
        return $table;
	};

	$data = array();
	switch ($resource) {
		case 'customerpayjobs':
			$sql = 'select Misc as Scheduled, Customer, Vehicle, Keys, Job from (execute procedure dwbGetcustomerpayjobs()) a order by Appointment';
			break;
		case 'reconbufferjobs':
			$sql = 'select Misc as Priority, Customer, Vehicle, Keys, Job from (execute procedure dwbGetreconbufferjobs()) a order by Misc DESC';
			break;
		case 'rawmaterialsjobs':
			$sql = 'select Misc as Priority, Customer, Vehicle, Keys, Job from (execute procedure dwbGetrawmaterialsjobs()) a order by Misc DESC';
			break;
		case 'otherjobs':
			$sql = 'select Misc as Priority, Customer, Vehicle, Keys, Job from (execute procedure dwbGetotherjobs()) a order by Customer';
			break;
		case 'whiteboard':
      $sql = 'execute procedure dwbUpdateWhiteBoardLocations()';
      $result = ads_do($conn, $sql);
			$sql = 'select Priority, Customer, Vehicle, Job, Keys, Location from (execute procedure dwbGetwhiteboard()) a order by seq';
			break;
		case 'wip':
			$sql = 'select Customer, Vehicle, Job from (execute procedure dwbGetwip()) a';
			break;
		case 'complete':
			$sql = 'select Customer, Vehicle, Job from (execute procedure dwbGetcomplete()) a';
			break;
    case 'servertime':
      $sql = "select curtime() as [ServerTime] from system.iota";
      break;
		default:
			break;
	}
	
 	$result = ads_do($conn, $sql);

 	while($row = ads_fetch_array($result)) {
		$data[] = $row;
	};

 	if (strtoupper($format) == 'JSON') {
		return json_encode($data);
	} else {
    return createHTMLtable($data, $resource);
  }
};

function post_resource($conn, $resource, $postdata) {
	if ($resource == 'whiteboardsequence') {
    for ($i=0; $i < count($postdata['data']); $i++) { 
			$sql = "execute procedure dwbUpdateWhiteBoardSequence('".$postdata['data'][$i]['customer']."', '".$postdata['data'][$i]['vehicle']."', ".$postdata['data'][$i]['sequence'].")";
     	ads_do($conn, $sql);
    }
	} else {
		$sql = "execute procedure dwbMoveJob('".$postdata['customer']."', '".$postdata['vehicle']."', '".$postdata['movefrom']."', '".$postdata['complete']."')";
  	ads_do($conn, $sql);
	}
}

function main() {
	if (!empty($_SERVER['PATH_INFO'])) {
		$path = $_SERVER['PATH_INFO'];
		$path_params = explode("/", $path);
	};

	if (!empty($path_params)) {

		$validResources = array('customerpayjobs', 'reconbufferjobs', 'rawmaterialsjobs', 'whiteboard', 'otherjobs', 'wip', 'complete', 'whiteboardsequence', 'servertime');
		$resource = $path_params[1];
		if (count($path_params) > 2) {
			$format = $path_params[2];
		};

		if (in_array($resource, $validResources)) {
			$adsconn = db_connect();
			if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	  	  $data = get_resource($adsconn, $resource, $format);
		  	echo $data;
      } else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        post_resource($adsconn, $resource, $_POST);
		  }
 	  	ads_close($adsconn);
		} else {
			header('HTTP/1.1 400 Bad Request');
		};
	};
};

main();
?>