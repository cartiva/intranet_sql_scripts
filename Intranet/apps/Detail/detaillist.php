    <div id="wrap">
      <!-- Begin page content -->
      <div class="container-fluid">
        <div class="row-fluid">
          <div class="span12">
            <div class="page-header">
              <h1>Detail Command Center</h1>
            </div>
          </div><!--.span12-->
        </div><!--.row-fluid-->
          
        <div class="row-fluid">
          <div class="well span6"><!--Sidebar content-->
            <h2>Library</h2>
            <p>Move cars from <em class="text-warning">Customer Pay</em>, <em class="text-warning">Recon Buffer</em>, <em class="text-warning">Raw Materials</em> or <em class="text-warning">Other</em> to the <em class="text-warning">Whiteboard</em>.</p>
            <!--Customer pay jobs-->
            <h3>Customer Pay Jobs</h3>
            <div id="customerpayjobsarea"></div>

            <!--Recon Buffer Jobs-->
            <h3>Recon Buffer Jobs</h3>
            <div id="reconbufferjobsarea"></div>

            <!--Raw Materials Jobs-->
            <h3>Raw Materials Jobs</h3>
            <div id="rawmaterialsjobsarea"></div>

            <!--Other Jobs-->
            <h3>Other Jobs</h3><button class='otherjobs'>Get</button>
            <div id="otherjobsarea"></div>
          </div><!--.span6-->

          <div class="span6"><!--Body content-->
            <!--Whiteboard-->
            <h2>Whiteboard <a href="whiteboard.php" target="_blank"><span>Want to see the standalone Whiteboard?</span></a></h2>
            <p>You can also rearrange the cars inside the <em class="text-warning">Whiteboard</em> by drag-and-dropping.</p>
            <div id="whiteboardarea"></div>

            <!--WIP-->
            <h2>WIP</h2>
            <div id="wiparea"></div>

            <!--Complete-->
            <h2>Complete</h2>
            <div id="completearea"></div>

          </div><!--.span6-->
        </div><!--.row-fluid-->
      </div><!--.container-fluid-->

      <!-- <div id="push"></div> -->
    </div>

    <!-- Le Javascripts -->
    <script>
      function postResource(event, customerColumn, vehicleColumn, forward) {
        var target = $(event.target);
        var customer = target.closest('tr').find('td:eq(' + customerColumn + ')').text();
        var vehicle = target.closest('tr').find('td:eq(' + vehicleColumn + ')').text();
        var resource = target.closest('table').attr('id');
        $.post('./detail/router.php/' + resource, { customer: customer, vehicle: vehicle, movefrom: resource, complete: forward}).done(function() {
          loadResource(resource);
          myData.lastClick.customer = customer;
          myData.lastClick.vehicle = vehicle;
          switch (resource) {
            case 'customerpayjobs':
            case 'reconbufferjobs':
            case 'rawmaterialsjobs':
            case 'otherjobs':
              loadResource('whiteboard');
              persistWhiteBoardSequence();
              break;
            case 'whiteboard':
              if (forward == 'true') {
                loadResource('wip');
              }
              else {
                loadResource('customerpayjobs');
                loadResource('reconbufferjobs');
                loadResource('rawmaterialsjobs');
                loadResource('otherjobs');
              };
              break;
            case 'wip':
              if (forward == 'true') {
                loadResource('complete');
              } else {
                loadResource('whiteboard');
              };
              break;
            case 'complete':
              loadResource('wip');
              break;
            default:
              break;
          }
        });
      }

      function loadResource(resource) {
        return $.get('./detail/router.php/' + resource + '/html').done(function(data) { 
          $('#' + resource + 'area').empty().append(data);
          switch (resource) {
          case 'customerpayjobs':
          case 'reconbufferjobs':
          case 'rawmaterialsjobs':
          case 'otherjobs':
            $('#' + resource + ' tbody tr').append('<td><button class="whiteboard btn">Whiteboard</button></td>');
            $('#' + resource).on('click', '.whiteboard', function(e) {
              $(this).removeClass('whiteboard');
              postResource(e, 1, 2, 'true');
            });
            break;
          case 'whiteboard':
            $('#whiteboard tbody').sortable({ items: "> tr", axis: "y", update: function() {
            persistWhiteBoardSequence()
            }});
            $('#whiteboard tbody tr').append('<td><div class="btn-group"><button class="undo btn">Library</button><button class="wip btn">WIP</button></div></td>');
            $('#whiteboard').on('click', '.undo', function(e) {
              $(this).removeClass('whiteboard');
              postResource(e, 1, 2, 'false');
            });
            $('#whiteboard').on('click', '.wip', function(e) {
              $(this).removeClass('whiteboard');
              postResource(e, 1, 2, 'true');
            });
            break;
          case 'wip':
            $('#wip tbody tr').append('<td><div class="btn-group"><button class="unwip btn">Whiteboard</button><button class="complete btn">Complete</button></div></td>');
            $('#wip').on('click', '.unwip', function(e) {
              $(this).removeClass('whiteboard');
              postResource(e, 0, 1, 'false');
            });
            $('#wip').on('click', '.complete', function(e) {
              $(this).removeClass('whiteboard');
              postResource(e, 0, 1, 'true');
            });
            break;
          case 'complete':
            $('#complete tbody tr').append('<td><button class="undo btn">WIP</button></td>');
            $('#complete').on('click', '.undo', function(e) {
              $(this).removeClass('whiteboard');
              postResource(e, 0, 1, 'false');
            });
            break;
          default:
            break;
          }
          if ($('#' + resource + ' tr td').text().length > 0) {
            $('#' + resource + ' thead tr').append('<th>Action</th>');
          };
          if (myData.lastClick.customer.length > 0) {
            $('#' + resource + " tr td:contains('" + myData.lastClick.customer + "')").siblings().addBack().effect('highlight', 2000, function() {
              myData.lastClick.customer = {};
            });
          };
        });
      }

      function persistWhiteBoardSequence() {
        var arr = [],
            customer,
            vehicle,
            obj;
        $('#whiteboard tbody tr').each(function(index, value) {
          customer = $(this).find('td:eq(1)').text();
          vehicle = $(this).find('td:eq(2)').text();
          obj = { customer: customer, vehicle: vehicle, sequence: index };
          arr.push(obj);
        });
        $.post('./router.php/whiteboardsequence', {data: arr});
      }

      function loadResources() {
        loadResource('customerpayjobs');
        loadResource('reconbufferjobs');
        loadResource('rawmaterialsjobs');
        loadResource('whiteboard');
        loadResource('wip');
        loadResource('complete');
      }

      $.ajaxSetup({ cache: false });
      $(document).ajaxSend(function(event, jqxhr, settings) {
        switch (settings.url.split('?')[0]) {
          // case './router.php/customerpayjobs/html':
          //   $( ".log" ).append( settings.type + " Triggered customerpayjobs.</br>" );
          //   break;
          // case './router.php/reconbufferjobs/html':
          //   $( ".log" ).append( settings.type + " Triggered reconbufferjobs.</br>" );
          //   break;
          // case './router.php/rawmaterialsjobs/html':
          //   $( ".log" ).append( settings.type + " Triggered rawmaterilsjobs.</br>" );
          //   break;
          // case './router.php/whiteboard/html':
          //   $( ".log" ).append( settings.type + " Triggered whiteboard.</br>" );
          //   break;
          // case './router.php/wip/html':
          //   $( ".log" ).append( settings.type + " Triggered wip.</br>" );
          //   break;
          // case './router.php/complete/html':
          //   $( ".log" ).append( settings.type + " Triggered complete.</br>" );
          //   break;
          default:
            break;
        }
      });
      myData = {};
      myData.lastClick = {};
      myData.lastClick.customer = {};
      myData.lastClick.vehicle = {};
      loadResources();
      $('.otherjobs').on('click', function() {
        loadResource('otherjobs');
      })
      setInterval('loadResources();', 60000); 
    </script>

    