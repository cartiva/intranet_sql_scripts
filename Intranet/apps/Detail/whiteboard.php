<?php
$page_title = "Detail Whiteboard";
$author = "Cartiva Development";
?>


<!-- HTML STARTS HERE -->
<html>
<head>
  <title><?php echo $page_title;?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=10; IE=9; IE=8; IE=7; IE=Edge" />
  <meta name="description" content="This is a <?php echo $author;?> Application, for the Detail shop to view jobs in a display-only mode.">
  <meta name="author" content="<?php echo $author;?>">
  <meta http-equiv="refresh" content="300;URL='http://go.cartiva.com/goplay/detail/whiteboard.php'" />  
  <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-responsive.css">
  <link rel="stylesheet" type="text/css" href="../css/detail.css">
  <link rel="stylesheet" type="text/css" href="../css/detail_whiteboard.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script src="../js/jquery.ui.touch-punch.min.js"></script>
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
  <![endif]-->
</head>
<body>
  <div id="wrap">
      <!-- Begin page content -->
      <div class="container-fluid">
        <div class="row-fluid">
          <div class="span12">
            <div class="page-header">
              <h1 id="clockDiv"></h1>
            </div>
            <div id="whiteboardarea"></div>
          </div><!--.span12-->
        </div><!--.row-fluid--> 

    <div id="push"></div>
    </div>
    
    <div id="footer">
      <div class="container-fluid">
        <p class="muted credit">Application courtesy of <?php echo $author;?>.</p>
      </div>
    </div>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  	<script>
      function loadWhiteBoard() {
        $.get('./router.php/whiteboard/html').done(function(data) { 
          $('#whiteboardarea').empty().append(data);
          if ($('table tr td').text().length == 0) {
            $('#whiteboardarea').empty().append('<h4>Whiteboard is empty.</h4>');
          }
        });
        $.getJSON('./router.php/servertime/json').done(function(data) {
          document.getElementById('clockDiv').innerHTML = 'Detail Whiteboard as of ' + data[0].ServerTime;
        });
      }
      loadWhiteBoard();
      setInterval('loadWhiteBoard();', 5000); 
    </script>
    
    <!-- Le GA code
    ================================================== -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-40742029-1', 'cartiva.com');
      ga('send', 'pageview');
    </script>
</body>
</html>
<!-- END OF HTML -->