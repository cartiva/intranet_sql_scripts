// Employee Routes
// #emps/employeelist

$(window).bind('hashchange', function(e) {

  // Employees Routes
  if (location.hash === "#emps/employeelist") {
    $('.stats-container').empty().append('<h4>Loading Employees...</h4>');
    employees.insertList('.stats-container');
    window.location.hash = '';
  }
});
