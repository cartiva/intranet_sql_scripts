var baseURL = './webservice/index.php';


function getWriters(user) {
  return $.getJSON(baseURL + '/writers/' + user);
}

function getUsersMainActions(user, displayname) {
  return $.getJSON(baseURL + '/mainactions/' + user).then(function(snippets) {
    return { userName:user, displayName:displayname, snippets:snippets};
  });
}

function getWritersWithMainActions(user) {
  var promises = [];
  var objects = [];
  return getWriters(user).then(function(writers) {
    for (var i = 0; i < writers.length; i++) {
      promises.push(getUsersMainActions(writers[i].writerEEUserName, writers[i].writerName).done(function(data) {
        objects.push(data);
      }));
    }
    return $.when.apply($, promises).then(function(snippets) {
      return objects;
    });
  });
}


