function newEmployees() {
	'use strict'
	return {

		insertList: function(target) {

			function createEmployeesMarkup(data) {
			  var content = '';
				content += '<h4>Your Employees</h4>';
				content += '<p>These are all employees for which you can view information.  "Apps" that are available to view will show up in the employee' + "'s row.</p>";
				content += '<table class="employees">';
				content += '  <thead>';
				content += '	  <tr>';
				content += '			<th class="col-writer">Name</th>';
				content += '			<th>Apps</th>';
				content += '		</tr>';
				content += '  </thead>';
				content += '  <tbody>';
  		  $.each(data, function(i, item) {
					content += '		<tr>';
					content += '			<td class="col-writer">' + item.displayName + '</td>';
					content += '			<td class="move">';
					$.each(item.snippets, function(j, snip) {
						content += snip.snippet;
					})
					content += '			</td>';
					content += '		</tr>';
				});
				content += '  </tbody>';
				content += '</table>';
			  return content;
			}
			
		  $.when(getWritersWithMainActions(myData.globalUser.userName))
		    .then(function(data) {
		    	$(target).empty().append(createEmployeesMarkup(data));
          //$('.employees').sortIt();
		    });
		  
		}
	}
}

var employees = newEmployees();
