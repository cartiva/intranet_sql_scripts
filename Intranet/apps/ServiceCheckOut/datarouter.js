function getUserInfo(username) {
  return $.getJSON(baseURL + '/userinfo/' + username);
}
function getLeftNavBar(username) {
	return $.getJSON(baseURL + '/leftnavbar/' + username);
}
function getSubActions(username, app) {
  return $.getJSON(baseURL + '/subactions/' + username + '/' + app);
}
function getWriterCheckOutStatus(username) {
 	return $.getJSON(baseURL + '/checkoutstatus/' + username);
}
function getDatesData(theDate) {
  return $.getJSON(baseURL + '/dates/' + theDate);
}
function getManagerStatsData(user, which) {
  return $.getJSON(baseURL + '/mgrlanding/' + user + '/' + which);
}
function getWritersForManager(user) {
  return $.getJSON(baseURL + '/writers/' + user);
}
function getWriterStatsData(user) {
  return $.getJSON(baseURL + '/landing/' + user);
}
function getWriterSalesData(user) {
  return $.getJSON(baseURL + '/salesmtd/' + user);
}
// Get data for an individual RO
function getRO(ro) {
  return $.getJSON(baseURL + '/newros/' + ro);
}
// Get all statuses for an indivdual RO
function getAgedROStatuses(ro) {
  return $.getJSON(baseURL + '/agedros/statuses/' + ro);
}
function getWriterAgedROsData(writer) {
  return $.getJSON(baseURL + '/agedros/' + writer);
}
// Get all statuses for a Warranty Admin RO
function getWarrantyAdminROStatuses(ro) {
  return $.getJSON(baseURL + '/warrantyadminros/statuses/' + ro);
}
function getWarrantyAdminROsData(writer) {
  return $.getJSON(baseURL + '/warrantyadminros/' + writer);
}
// Get all statuses for a PDQ Warranty Admin RO
function getPdqWarrantyAdminROStatuses(ro) {
  return $.getJSON(baseURL + '/pdqwarrantyadminros/statuses/' + ro);
}
function getPdqWarrantyAdminROsData(writer) {
  return $.getJSON(baseURL + '/pdqwarrantyadminros/' + writer);
}
// Get all statuses for an Current RO
function getCurrentROStatuses(ro) {
  return $.getJSON(baseURL + '/currentros/statuses/' + ro);
}
function getWriterCurrentROsData(writer) {
  return $.getJSON(baseURL + '/currentros/' + writer);
}
function getCheckOutData(user) {
  return $.getJSON(baseURL + '/checkouts/' + user);
}
// Get data for an individual RO
function getRY2RO(ro) {
  return $.getJSON(baseURL + '/newros/' + ro);
}
// Get all statuses for an indivdual RO
function getServiceROStatuses(ro) {
  return $.getJSON(baseURL + '/ry2rostatuses/' + ro);
}
function getRY2ServiceCallsData(writer) {
  return $.getJSON(baseURL + '/ry2servicecalls/' + writer);
}
// Get all statuses for an indivdual RO
function getWarrantyROStatuses(ro) {
  return $.getJSON(baseURL + '/warrantycalls/statuses/' + ro);
}
function getWriterWarrantyCallsData(user) {
  return $.getJSON(baseURL + '/warrantycalls/' + user);
}

	

