// Service Check Out App Markup
//
// dateHeadingMarkup()
// manangerStatsMarkup(userName, which {this:last})
// writerListMarkup(userName) 
// writerSalesMarkup(userName)
// writerStatsMarkup(userName)
// agedROsListMarkup(userName)
// agedROMarkup(ro)
// currentROsListMarkup(userName)
// currentROMarkup(ro)
// warrantyAdminROsListMarkup(userName)
// warrantyAdminROMarkup(ro)
// warrantyROsListMarkup(userName)
// warrantyROMarkup(userName)
// checkOutMarkup(userName)

function newServiceCheckOut() {
	'use strict';
	return {

	  dateHeadingMarkup: function() {

	  	function createDateHeadingMarkup(dates) {
			  return '<h5><span id="fromDate">' + dates.fromDate + '</span> - <span id="thruDate">' + dates.thruDate + '</span></h5>';

	  	}

	  	return getDatesData(getDate()).then(function(data) {
	  		return createDateHeadingMarkup(data[0]);
	  	});
	  },

	  managerStatsMarkup: function(userName, which) {

  	  function createManagerStatsMarkup(dates, data) {
			  var content = '',
			    now = new Date(),
			    classes = ['', '', '', '', '', '', ''];
			  classes[now.getDay()] = ' class="Today"';
			  content += '<table class="splash" id="weekly">';
			  content += '<thead>';
			  content += '  <tr>';
			  content += '    <th></th>';
			  content += '    <th' + classes[0] + '>Sun<span class="datespan">' + dates.Sunday + '</span></th>';
			  content += '    <th' + classes[1] + '>Mon<span class="datespan">' + dates.Monday + '</span></th>';
			  content += '		<th' + classes[2] + '>Tue<span class="datespan">' + dates.Tuesday + '</span></th>';
			  content += '		<th' + classes[3] + '>Wed<span class="datespan">' + dates.Wednesday + '</span></th>';
			  content += '		<th' + classes[4] + '>Thu<span class="datespan">' + dates.Thursday + '</span></th>';
			  content += '		<th' + classes[5] + '>Fri<span class="datespan">' + dates.Friday + '</span></th>';
			  content += '		<th' + classes[6] + '>Sat<span class="datespan">' + dates.Saturday + '</span></th>';
			  content += '		<th class="total">Week-to-date</th>';
			  content += '	</tr>';
			  content += '</thead>';
			  content += '<tbody>';
			  $.each(data, function(i, item) {
			    content += '<tr><td class="column">' + item.Metric + '<span class="def-icon"></span><span class="def">' + item.def + '</span></td>';
			    content += '<td' + classes[0] + '>' + item.Sun + '</td>';
			    content += '<td' + classes[1] + '>' + item.Mon + '</td>';
			    content += '<td' + classes[2] + '>' + item.Tue + '</td>';
			    content += '<td' + classes[3] + '>' + item.Wed + '</td>';
			    content += '<td' + classes[4] + '>' + item.Thu + '</td>';
			    content += '<td' + classes[5] + '>' + item.Fri + '</td>';
			    content += '<td' + classes[6] + '>' + item.Sat + '</td>';
			    content += '<td class="total">' + item.Total + '</td></tr>';
			  });
			  content += '</tbody>';
			  content += '</table>';
			  return content;
			}

		  return $.when(getDatesData(getDate()), getManagerStatsData(userName, which)).then(function(dates, data) { 
		  	return createManagerStatsMarkup(dates[0][0], data[0]); 
		  });
		},

		writerListMarkup: function(userName) {

  	  function createWriterListMarkup(data) {
			  var content = '';
				content += '<h4>Service Writers</h4>';
				content += '<p>These are the Service Writers that report to you.  To view their individual stats, click their name. You will then also have access to their individual RO lists.</p>';
  				content += '<table id="weekly" class="splash servicewriters-list">';
	  			content += '  <tbody>';
			  $.each(data, function(i, item) {
			    content += '    <tr>';
			    content += '      <td class="col-writer"><a href="#sco/writerstats/' + item.writerEEUserName + '">' + item.writerName + '</a></td>';
  				// content += '			<td class="move">';
  				// content += '				<div class="btn-group" id="servicestats">';
  				// content += '					<a class="button" href="#viewagedros">View Aged ROs</a>';
  				// content += '					<a class="button" href="#viewagedros">View Aged ROs</a>';
  				// content += '				</div>';
			   //  content += '      <td>';
			    content += '    <tr>';
			  });
			  content += '  </tbody>'
			  content += '  </table>';
			  return content;
			}

			return getWritersForManager(userName).then(function(writerdata) {
				return createWriterListMarkup(writerdata);
			});
  	},

  	displayName: function(userName) {

  		return getUserInfo(userName).then(function(data) {
  			return data[0].FirstName;
  		});

  	},

  	writerSalesMarkup: function(userName) {

  		function createWriterSalesMarkup(salesData) {
  			return '<aside><h5>Month-to-date labor sales: <span>' + salesData.LaborSalesMonthToDate + '</span></h5></aside>';
  		}

			return getWriterSalesData(userName).then(function(data) {
				return createWriterSalesMarkup(data[0]);
			});
  	},

  	writerStatsMarkup: function(userName) {

  	  function createWriterStatsMarkup(dates, data, snippets) {
			  var content = '',
			    writerName = data[0].DisplayFirstName,
			    now = new Date(),
			    classes = ['', '', '', '', '', '', ''];
 			  classes[now.getDay()] = ' class="Today"';
 			  content += '<div class="btn-group-lg">';
 			  $.each(snippets, function(j, snip) {
 			  	content += snip.snippet;
 			  });
 			  content += '</div>';
			  content += '<table class="splash" id="weekly">';
			  content += '	<thead>';
			  content += '		<tr>';
			  content += '			<th></th>';
			  content += '			<th' + classes[0] + '>Sun<span class="datespan">' + dates.Sunday + '</span></th>';
			  content += '			<th' + classes[1] + '>Mon<span class="datespan">' + dates.Monday + '</span></th>';
			  content += '			<th' + classes[2] + '>Tue<span class="datespan">' + dates.Tuesday + '</span></th>';
			  content += '			<th' + classes[3] + '>Wed<span class="datespan">' + dates.Wednesday + '</span></th>';
			  content += '			<th' + classes[4] + '>Thu<span class="datespan">' + dates.Thursday + '</span></th>';
			  content += '			<th' + classes[5] + '>Fri<span class="datespan">' + dates.Friday + '</span></th>';
			  content += '			<th' + classes[6] + '>Sat<span class="datespan">' + dates.Saturday + '</span></th>';
			  content += '			<th class="total">Week-to-date</th>';
			  content += '		</tr>';
			  content += '  </thead>';
			  content += '	<tbody>';
			  $.each(data, function(i, item) {
			    content += '<tr><td class="column">' + item.Metric + '<span class="def-icon"></span><span class="def">' + item.def + '</span></td>';
			    content += '<td' + classes[0] + '>' + item.Sun + '</td>';
			    content += '<td' + classes[1] + '>' + item.Mon + '</td>';
			    content += '<td' + classes[2] + '>' + item.Tue + '</td>';
			    content += '<td' + classes[3] + '>' + item.Wed + '</td>';
			    content += '<td' + classes[4] + '>' + item.Thu + '</td>';
			    content += '<td' + classes[5] + '>' + item.Fri + '</td>';
			    content += '<td' + classes[6] + '>' + item.Sat + '</td>';
			    content += '<td class="total">' + item.Total + '</td></tr>';
			  });
			  content += '</table>';
			  return content;
			}

		  return $.when(getDatesData(getDate()), getWriterStatsData(userName), getSubActions(userName, 'sco')).then(function(dates, data, snips) { 
		  	return createWriterStatsMarkup(dates[0][0], data[0], snips[0]); 
		  });
  	},

  	agedROsListMarkup: function(userName) {

  		function createAgedROsListMarkup(ros) {
			  var content = '';
			  if (ros.length == 0) {
				  content += '<div id="no-ro-data">';
				  content += '<img src="../images/success-ros.jpg" alt="Success at Aged ROs!" />';
				  content += '</div>';
				} else {
				  content += '<h4>Aged ROs</h4>';		  
				  content += '<table id="table" class="agedros">';
				  content += '<thead>';
				  content += '<tr id="headings">';
				  content += '<th data-col="ro"><span class="arrow"></span>RO</th>';
				  content += '<th data-col="firstName"><span class="arrow"></span>Writer</th>';
				  content += '<th data-col="roStatus"><span class="arrow"></span>Status</th>';
				  content += '<th data-col="openDate" class="col-date"><span class="arrow"></span>Open Date</th>';
				  content += '<th data-col="customer"><span class="arrow"></span>Customer</th>';
				  content += '<th data-col="vehicle"><span class="arrow"></span>Vehicle</th>';
				  content += '<th data-col="statusUpdate"><span class="arrow"></span>Update</th>';
				  content += '</tr>';
				  content += '</thead>';
				  content += '<tbody>';
				  $.each(ros, function(i, item) {
				    content += '<tr><td>' + item.ro + '</td>';
				    content += '<td class="col-customer">' + item.firstName + '</td>';
				    content += '<td class="col-status">' + item.roStatus + '</td>';
				    content += '<td class="col-date">' + item.openDate + '</td>';
				    content += '<td class="col-customer">' + item.customer + '</td>';
				    content += '<td class="col-vechile">' + item.vehicle + '</td>';
				    content += '<td class="col-update">' + item.statusUpdate + '<span class="spacer"><a href="#sco/editagedro/' + item.ro + '/' + myData.globalUser.userName + '" class="agededitbutton modal">Edit</a></span></td></tr>';
//				    content += '<td class="col-update">' + item.statusUpdate + '<span class="spacer"><a data-ro="' + item.ro + '" data-userid="' + myData.globalUser.userName + '" class="agededitbutton modal">Edit</a></span></td></tr>';
				  });
				  content += '</tbody>';
				  content += '</table>';
				  content += '<div id="modal" class="window"></div>';
				  content += '<div id="mask"></div>';
				}
			  return content;
			}

		  return $.when(getWriterAgedROsData(userName))
		    .then(function(data) {
		      return createAgedROsListMarkup(data);
		  });
  	},

  	agedROMarkup: function(ro) {

  		function createAgedROMarkup(ronum, ro, statuses) {
			  var content = '';
			  content += '<span type="button" class="x-close close-can cancelbutton"></span>';
			  content += '<h3>Update the status of this Aged RO</h3>';
			  content += '<h4>RO Detail</h4>';
			  content += '<div id="rodetail">';
			  content += '<table>';
			  content += '	<tr>';
			  content += '		<th>Name</th>';
			  content += '		<th>Contact</th>';
			  content += '		<th>Vehicle</th>';
			  content += '		<th>RO Info</th>';
			  //content += '		<th>Status</th>';
			  //content += '		<th>Open/Closed</th>';
			  content += '	</tr>';
			  content += '	<tr>';
			  content += '		<td class="customer">' + ro[0].Customer + '</td>';
			  content += '		<td class="contact shortdetails">';
			  content += '			<div>';
			  content += '				<p>Home: ' + ro[0].HomePhone + '</p>';
			  content += '				<p>Work: ' + ro[0].WorkPhone + '</p>';
			  content += '				<p>Cell: ' + ro[0].CellPhone + '</p>';
			  content += '				<p>Email: ' + ro[0].CustomerEMail + '</p>';
			  content += '			</div>';
			  content += '		</td>';
			  content += '		<td class="vehicle">' + ro[0].Vehicle + '</td>';
			  content += '		<td class="ro shortdetails">';
			  content += '			<div>';
			  content += '				<p>RO: <span id="agedro">' + ro[0].RO + '</span></p>';
			  content += '				<p>Status: ' + ro[0].ROStatus + '</p>';
			  content += '				<p>O/C: ' + ro[0].OpenDate + ' / ' + ro[0].CloseDate + '</p>';
			  content += '				<p>Writer: ' + ro[0].WriterFirstName + '</p>';			  
			  content += '			</div>';
			  content += '			</dl>';
			  content += '		</td>';
			  content += '	</tr>';
			  content += '</table>';
			  content += '<table class="roshit">';
			  content += '	<tr>';
			  content += '		<th>Line</th>';
			  content += '		<th class="paytype">Pay Type</th>';
			  content += '		<th>Status</th>';
			  content += '		<th>Descr.</th>';
			  content += '	</tr>';
			  $.each(ro[1], function(i, line) {
				  content += '	<tr>';
				  content += '		<td>' + line.Line + '</td>';
				  content += '		<td class="paytype">' + line.PayType + '</td>';
				  content += '		<td>' + line.LineStatus + '</td>';
				  content += '		<td class="longdetails">';
				  content += '			<div>';
				  content += '				<p>Op Code: ' + line.OpCode + ': ' + line.OpCodeDesc + '</p>';
				  content += '				<p>Complaint: ' + line.Complaint + '</p>';
				  content += '				<p>Cause: ' + line.Cause + '</p>';
				  content += '				<p>Cor Code: ' + line.CorCode + ': ' + line.CorCodeDesc + '</p>';
				  content += '				<p>Correction: ' + line.Correction + '</p>';
				  content += '			</div>';
				  content += '		</td>';
				  content += '	</tr>';
			  });
				content += '	<tr>';
				content += '		<td colspan="4">Please Note: ' + ro[0].PleaseNote + '</td>';
				content += '	</tr>';

			  content += '</table>';
			  content += '</div>';
			  content += '<h5>Update Status</h5>';
			  content += '	<select id="select">';
			  content += '		<option value="">-- Select a status --</option>';
			  content += '		<option value="Warranty admin has RO.">Warranty admin has RO.</option>';
			  content += '		<option value="Special order parts.">Special order parts.</option>';
			  content += '		<option value="Parts hold.">Parts hold.</option>';
			  content += '		<option value="Authorization hold.">Authorization hold.</option>';
			  content += '	</select>';
			  content += '<textarea rows="7" id="aged_status_update" name="status_update" type="" required class="entry" placeholder="Enter the new status here."></textarea>';
			  content += '<br />';
			  content += '<button class="submit" id="submitagedro">Submit</button>';
			  content += '<button type="button" class="close-can cancelbutton">Cancel</button>';
			  content += '<button id="printBtn" type="button" class="print">Print</button>';
			  content += '<div id="ro-list">';
			  content += '<h4>Status Updates</h4><table>';
			  $.each(statuses, function(i, item) {
			    content += '<tr><td class="label">' + item.eeUserName;
			    content += '<span>' + item.updateTS + '</span></td>';
			    content += '<td class="result">' + item.statusUpdate + '</td></tr>';
			  });
			  content += '</table>';
			  return content;
  		}

			return $.when(getRO(ro), getAgedROStatuses(ro))
    		.then(function(a1, a2) {
          return createAgedROMarkup(ro, a1[0], a2[0]);
    		});
		},

	currentROsListMarkup: function(userName) {

  		function createCurrentROsListMarkup(ros) {
			  var content = '';
			  if (ros.length == 0) {
				  content += '<div id="no-ro-data">';
				  content += '<img src="../images/success-current.jpg" alt="Success at Current ROs!" />';
				  content += '</div>';
				} else {
				  content += '<h4>Current ROs</h4>';		  
				  content += '<table id="table" class="currentros">';
				  content += '<thead>';
				  content += '<tr id="headings">';
				  content += '<th data-col="ro"><span class="arrow"></span>RO</th>';
				  content += '<th data-col="firstName"><span class="arrow"></span>Writer</th>';
				  content += '<th data-col="roStatus"><span class="arrow"></span>Status</th>';
				  content += '<th data-col="openDate" class="col-date"><span class="arrow"></span>Open Date</th>';
				  content += '<th data-col="customer"><span class="arrow"></span>Customer</th>';
				  content += '<th data-col="vehicle"><span class="arrow"></span>Vehicle</th>';
				  content += '<th data-col="statusUpdate"><span class="arrow"></span>Update</th>';
				  content += '</tr>';
				  content += '</thead>';
				  content += '<tbody>';
				  $.each(ros, function(i, item) {
				    content += '<tr><td>' + item.ro + '</td>';
				    content += '<td class="col-customer">' + item.firstName + '</td>';
				    content += '<td class="col-status">' + item.roStatus + '</td>';
				    content += '<td class="col-date">' + item.openDate + '</td>';
				    content += '<td class="col-customer">' + item.customer + '</td>';
				    content += '<td class="col-vechile">' + item.vehicle + '</td>';
				    content += '<td class="col-update">' + item.statusUpdate + '<span class="spacer"><a href="#sco/editcurrentro/' + item.ro + '/' + myData.globalUser.userName + '" class="currenteditbutton modal">Edit</a></span></td></tr>';
//				    content += '<td class="col-update">' + item.statusUpdate + '<span class="spacer"><a data-ro="' + item.ro + '" data-userid="' + myData.globalUser.userName + '" class="currenteditbutton modal">Edit</a></span></td></tr>';
				  });
				  content += '</tbody>';
				  content += '</table>';
				  content += '<div id="modal" class="window"></div>';
				  content += '<div id="mask"></div>';
				}
			  return content;
			}

		  return $.when(getWriterCurrentROsData(userName))
		    .then(function(data) {
		      return createCurrentROsListMarkup(data);
		  });
  	},

  	currentROMarkup: function(ro) {

  		function createCurrentROMarkup(ronum, ro, statuses) {
			  var content = '';
			  content += '<span type="button" class="x-close close-can cancelbutton"></span>';
			  content += '<h3>Update the status of this Current RO</h3>';
			  content += '<h4>RO Detail</h4>';
			  content += '<div id="rodetail">';
			  content += '<table>';
			  content += '	<tr>';
			  content += '		<th>Name</th>';
			  content += '		<th>Contact</th>';
			  content += '		<th>Vehicle</th>';
			  content += '		<th>RO Info</th>';
			  //content += '		<th>Status</th>';
			  //content += '		<th>Open/Closed</th>';
			  content += '	</tr>';
			  content += '	<tr>';
			  content += '		<td class="customer">' + ro[0].Customer + '</td>';
			  content += '		<td class="contact shortdetails">';
			  content += '			<div>';
			  content += '				<p>Home: ' + ro[0].HomePhone + '</p>';
			  content += '				<p>Work: ' + ro[0].WorkPhone + '</p>';
			  content += '				<p>Cell: ' + ro[0].CellPhone + '</p>';
			  content += '				<p>Email: ' + ro[0].CustomerEMail + '</p>';
			  content += '			</div>';
			  content += '		</td>';
			  content += '		<td class="vehicle">' + ro[0].Vehicle + '</td>';
			  content += '		<td class="ro shortdetails">';
			  content += '			<div>';
			  content += '				<p>RO: <span id="currentro">' + ro[0].RO + '</span></p>';
			  content += '				<p>Status: ' + ro[0].ROStatus + '</p>';
			  content += '				<p>O/C: ' + ro[0].OpenDate + ' / ' + ro[0].CloseDate + '</p>';
			  content += '				<p>Writer: ' + ro[0].WriterFirstName + '</p>';			  
			  content += '			</div>';
			  content += '			</dl>';
			  content += '		</td>';
			  content += '	</tr>';
			  content += '</table>';
			  content += '<table class="roshit">';
			  content += '	<tr>';
			  content += '		<th>Line</th>';
			  content += '		<th class="paytype">Pay Type</th>';
			  content += '		<th>Status</th>';
			  content += '		<th>Descr.</th>';
			  content += '	</tr>';
			  $.each(ro[1], function(i, line) {
				  content += '	<tr>';
				  content += '		<td>' + line.Line + '</td>';
				  content += '		<td class="paytype">' + line.PayType + '</td>';
				  content += '		<td>' + line.LineStatus + '</td>';
				  content += '		<td class="longdetails">';
				  content += '			<div>';
				  content += '				<p>Op Code: ' + line.OpCode + ': ' + line.OpCodeDesc + '</p>';
				  content += '				<p>Complaint: ' + line.Complaint + '</p>';
				  content += '				<p>Cause: ' + line.Cause + '</p>';
				  content += '				<p>Cor Code: ' + line.CorCode + ': ' + line.CorCodeDesc + '</p>';
				  content += '				<p>Correction: ' + line.Correction + '</p>';
				  content += '			</div>';
				  content += '		</td>';
				  content += '	</tr>';
			  });
				content += '	<tr>';
				content += '		<td colspan="4">Please Note: ' + ro[0].PleaseNote + '</td>';
				content += '	</tr>';

			  content += '</table>';
			  content += '</div>';
			  content += '<h5>Update Status</h5>';
			  content += '	<select id="select">';
			  content += '		<option value="">-- Select a status --</option>';
			  content += '		<option value="Warranty admin has RO.">Warranty admin has RO.</option>';
			  content += '		<option value="Special order parts.">Special order parts.</option>';
			  content += '		<option value="Parts hold.">Parts hold.</option>';
			  content += '		<option value="Authorization hold.">Authorization hold.</option>';
			  content += '	</select>';
			  content += '<textarea rows="7" id="current_status_update" name="status_update" type="" required class="entry" placeholder="Enter the new status here."></textarea>';
			  content += '<br />';
			  content += '<button class="submit" id="submitcurrentro">Submit</button>';
			  content += '<button type="button" class="close-can cancelbutton">Cancel</button>';
			  content += '<button id="printBtn" type="button" class="print">Print</button>';
			  content += '<div id="ro-list">';
			  content += '<h4>Status Updates</h4><table>';
			  $.each(statuses, function(i, item) {
			    content += '<tr><td class="label">' + item.eeUserName;
			    content += '<span>' + item.updateTS + '</span></td>';
			    content += '<td class="result">' + item.statusUpdate + '</td></tr>';
			  });
			  content += '</table>';
			  return content;
  		}

			return $.when(getRO(ro), getCurrentROStatuses(ro))
    		.then(function(a1, a2) {
          return createCurrentROMarkup(ro, a1[0], a2[0]);
    		});
		},



		warrantyAdminROsListMarkup: function(userName) {

  		function createWarrantyAdminROsListMarkup(ros) {
			  var content = '';
			  if (ros.length == 0) {
				  content += '<div id="no-ro-data">';
				  content += '<img src="../images/success-warranrtadmin.jpg" alt="Success at Warranty ROs!" />';
				  content += '</div>';
				} else {
				  content += '<h4>Warranty ROs in Cashier Status</h4>';		  
				  content += '<table id="table" class="warrantyadminros">';
				  content += '<thead>';
				  content += '<tr id="headings">';
				  content += '<th data-col="ro"><span class="arrow"></span>RO</th>';
				  content += '<th data-col="firstName"><span class="arrow"></span>Writer</th>';
				  content += '<th data-col="roStatus"><span class="arrow"></span>Status</th>';
				  content += '<th data-col="openDate" class="col-date"><span class="arrow"></span>Open Date</th>';
				  content += '<th data-col="customer"><span class="arrow"></span>Customer</th>';
				  content += '<th data-col="vehicle"><span class="arrow"></span>Vehicle</th>';
				  content += '<th data-col="statusUpdate"><span class="arrow"></span>Update</th>';
				  content += '</tr>';
				  content += '</thead>';
				  content += '<tbody>';
				  $.each(ros, function(i, item) {
				    content += '<tr><td>' + item.ro + '</td>';
				    content += '<td class="col-customer">' + item.firstName + '</td>';
				    content += '<td class="col-status">' + item.roStatus + '</td>';
				    content += '<td class="col-date">' + item.openDate + '</td>';
				    content += '<td class="col-customer">' + item.customer + '</td>';
				    content += '<td class="col-vechile">' + item.vehicle + '</td>';
				    content += '<td class="col-update">' + item.statusUpdate + '<span class="spacer"><a href="#sco/editwarrantyadminro/' + item.ro + '/' + myData.globalUser.userName + '" class="warrantyadmineditbutton modal">Edit</a></span></td></tr>';
				  });
				  content += '</tbody>';
				  content += '</table>';
				  content += '<div id="modal" class="window"></div>';
				  content += '<div id="mask"></div>';
				}
			  return content;
			}

		  return $.when(getWarrantyAdminROsData(userName))
		    .then(function(data) {
		      return createWarrantyAdminROsListMarkup(data);
		  });
  	},

  	warrantyAdminROMarkup: function(ro) {

  		function createWarrantyAdminROMarkup(ronum, ro, statuses) {
			  var content = '';
			  content += '<span type="button" class="x-close close-can cancelbutton"></span>';
			  content += '<h3>Update the status of this Warranty Admin RO</h3>';
			  content += '<h4>RO Detail</h4>';
			  content += '<div id="rodetail">';
			  content += '<table>';
			  content += '	<tr>';
			  content += '		<th>Name</th>';
			  content += '		<th>Contact</th>';
			  content += '		<th>Vehicle</th>';
			  content += '		<th>RO Info</th>';
			  //content += '		<th>Status</th>';
			  //content += '		<th>Open/Closed</th>';
			  content += '	</tr>';
			  content += '	<tr>';
			  content += '		<td class="customer">' + ro[0].Customer + '</td>';
			  content += '		<td class="contact shortdetails">';
			  content += '			<div>';
			  content += '				<p>Home: ' + ro[0].HomePhone + '</p>';
			  content += '				<p>Work: ' + ro[0].WorkPhone + '</p>';
			  content += '				<p>Cell: ' + ro[0].CellPhone + '</p>';
			  content += '				<p>Email: ' + ro[0].CustomerEMail + '</p>';
			  content += '			</div>';
			  content += '		</td>';
			  content += '		<td class="vehicle">' + ro[0].Vehicle + '</td>';
			  content += '		<td class="ro shortdetails">';
			  content += '			<div>';
			  content += '				<p>RO: <span id="warrantyadminro">' + ro[0].RO + '</span></p>';
			  content += '				<p>Status: ' + ro[0].ROStatus + '</p>';
			  content += '				<p>O/C: ' + ro[0].OpenDate + ' / ' + ro[0].CloseDate + '</p>';
			  content += '				<p>Writer: ' + ro[0].WriterFirstName + '</p>';			  
			  content += '			</div>';
			  content += '			</dl>';
			  content += '		</td>';
			  content += '	</tr>';
			  content += '</table>';
			  content += '<table class="roshit">';
			  content += '	<tr>';
			  content += '		<th>Line</th>';
			  content += '		<th class="paytype">Pay Type</th>';
			  content += '		<th>Status</th>';
			  content += '		<th>Descr.</th>';
			  content += '	</tr>';
			  $.each(ro[1], function(i, line) {
				  content += '	<tr>';
				  content += '		<td>' + line.Line + '</td>';
				  content += '		<td class="paytype">' + line.PayType + '</td>';
				  content += '		<td>' + line.LineStatus + '</td>';
				  content += '		<td class="longdetails">';
				  content += '			<div>';
				  content += '				<p>Op Code: ' + line.OpCode + ': ' + line.OpCodeDesc + '</p>';
				  content += '				<p>Complaint: ' + line.Complaint + '</p>';
				  content += '				<p>Cause: ' + line.Cause + '</p>';
				  content += '				<p>Cor Code: ' + line.CorCode + ': ' + line.CorCodeDesc + '</p>';
				  content += '				<p>Correction: ' + line.Correction + '</p>';
				  content += '			</div>';
				  content += '		</td>';
				  content += '	</tr>';
			  });
				content += '	<tr>';
				content += '		<td colspan="4">Please Note: ' + ro[0].PleaseNote + '</td>';
				content += '	</tr>';

			  content += '</table>';
			  content += '</div>';
			  content += '<h5>Update Status</h5>';
			  content += '	<select id="select">';
			  content += '		<option value="">-- Select a status --</option>';
			  content += '		<option value="I have the RO.">I have the RO.</option>';
			  content += '		<option value="I DO NOT have the RO!!!!!!!1">I DO NOT have the RO!!!!!!!1</option>';
			  content += '		<option value="Submitted.">Submitted.</option>';
			  content += '	</select>';
			  content += '<textarea rows="7" id="warrantyadmin_status_update" name="status_update" type="" required class="entry" placeholder="Enter the new status here."></textarea>';
			  content += '<br />';
			  content += '<button class="submit" id="submitwarrantyadminro">Submit</button>';
			  content += '<button type="button" class="close-can cancelbutton">Cancel</button>';
			  content += '<button id="printBtn" type="button" class="print">Print</button>';
			  content += '<div id="ro-list">';
			  content += '<h4>Status Updates</h4><table>';
			  $.each(statuses, function(i, item) {
			    content += '<tr><td class="label">' + item.eeUserName;
			    content += '<span>' + item.updateTS + '</span></td>';
			    content += '<td class="result">' + item.statusUpdate + '</td></tr>';
			  });
			  content += '</table>';
			  return content;
  		}

			return $.when(getRO(ro), getWarrantyAdminROStatuses(ro))
    		.then(function(a1, a2) {
          return createWarrantyAdminROMarkup(ro, a1[0], a2[0]);
    		});
		},




	//PDQ Warranty RO's Start
	PdqWarrantyAdminROsListMarkup: function(userName) {

  		function createPdqWarrantyAdminROsListMarkup(ros) {
			  var content = '';
			  if (ros.length == 0) {
				  content += '<div id="no-ro-data">';
				  content += '<img src="../images/success-pdqwarranrtadmin.jpg" alt="Success at PDQ Warranty ROs!" />';
				  content += '</div>';
				} else {
				  content += '<h4>PDQ Warranty ROs in Cashier Status</h4>';		  
				  content += '<table id="table" class="pdqwarrantyadminros">';
				  content += '<thead>';
				  content += '<tr id="headings">';
				  content += '<th data-col="ro"><span class="arrow"></span>RO</th>';
				  content += '<th data-col="firstName"><span class="arrow"></span>Writer</th>';
				  content += '<th data-col="roStatus"><span class="arrow"></span>Status</th>';
				  content += '<th data-col="openDate" class="col-date"><span class="arrow"></span>Open Date</th>';
				  content += '<th data-col="customer"><span class="arrow"></span>Customer</th>';
				  content += '<th data-col="vehicle"><span class="arrow"></span>Vehicle</th>';
				  content += '<th data-col="statusUpdate"><span class="arrow"></span>Update</th>';
				  content += '</tr>';
				  content += '</thead>';
				  content += '<tbody>';
				  $.each(ros, function(i, item) {
				    content += '<tr><td>' + item.ro + '</td>';
				    content += '<td class="col-customer">' + item.firstName + '</td>';
				    content += '<td class="col-status">' + item.roStatus + '</td>';
				    content += '<td class="col-date">' + item.openDate + '</td>';
				    content += '<td class="col-customer">' + item.customer + '</td>';
				    content += '<td class="col-vechile">' + item.vehicle + '</td>';
				    content += '<td class="col-update">' + item.statusUpdate + '<span class="spacer"><a href="#sco/editpdqwarrantyadminro/' + item.ro + '/' + myData.globalUser.userName + '" class="pdqwarrantyadmineditbutton modal">Edit</a></span></td></tr>';
				  });
				  content += '</tbody>';
				  content += '</table>';
				  content += '<div id="modal" class="window"></div>';
				  content += '<div id="mask"></div>';
				}
			  return content;
			}

		  return $.when(getPdqWarrantyAdminROsData(userName))
		    .then(function(data) {
		      return createPdqWarrantyAdminROsListMarkup(data);
		  });
  	},

  	PdqWarrantyAdminROMarkup: function(ro) {

  		function createPdqWarrantyAdminROMarkup(ronum, ro, statuses) {
			  var content = '';
			  content += '<span type="button" class="x-close close-can cancelbutton"></span>';
			  content += '<h3>Update the status of this PDQ Warranty Admin RO</h3>';
			  content += '<h4>RO Detail</h4>';
			  content += '<div id="rodetail">';
			  content += '<table>';
			  content += '	<tr>';
			  content += '		<th>Name</th>';
			  content += '		<th>Contact</th>';
			  content += '		<th>Vehicle</th>';
			  content += '		<th>RO Info</th>';
			  //content += '		<th>Status</th>';
			  //content += '		<th>Open/Closed</th>';
			  content += '	</tr>';
			  content += '	<tr>';
			  content += '		<td class="customer">' + ro[0].Customer + '</td>';
			  content += '		<td class="contact shortdetails">';
			  content += '			<div>';
			  content += '				<p>Home: ' + ro[0].HomePhone + '</p>';
			  content += '				<p>Work: ' + ro[0].WorkPhone + '</p>';
			  content += '				<p>Cell: ' + ro[0].CellPhone + '</p>';
			  content += '				<p>Email: ' + ro[0].CustomerEMail + '</p>';
			  content += '			</div>';
			  content += '		</td>';
			  content += '		<td class="vehicle">' + ro[0].Vehicle + '</td>';
			  content += '		<td class="ro shortdetails">';
			  content += '			<div>';
			  content += '				<p>RO: <span id="pdqwarrantyadminro">' + ro[0].RO + '</span></p>';
			  content += '				<p>Status: ' + ro[0].ROStatus + '</p>';
			  content += '				<p>O/C: ' + ro[0].OpenDate + ' / ' + ro[0].CloseDate + '</p>';
			  content += '				<p>Writer: ' + ro[0].WriterFirstName + '</p>';			  
			  content += '			</div>';
			  content += '			</dl>';
			  content += '		</td>';
			  content += '	</tr>';
			  content += '</table>';
			  content += '<table class="roshit">';
			  content += '	<tr>';
			  content += '		<th>Line</th>';
			  content += '		<th class="paytype">Pay Type</th>';
			  content += '		<th>Status</th>';
			  content += '		<th>Descr.</th>';
			  content += '	</tr>';
			  $.each(ro[1], function(i, line) {
				  content += '	<tr>';
				  content += '		<td>' + line.Line + '</td>';
				  content += '		<td class="paytype">' + line.PayType + '</td>';
				  content += '		<td>' + line.LineStatus + '</td>';
				  content += '		<td class="longdetails">';
				  content += '			<div>';
				  content += '				<p>Op Code: ' + line.OpCode + ': ' + line.OpCodeDesc + '</p>';
				  content += '				<p>Complaint: ' + line.Complaint + '</p>';
				  content += '				<p>Cause: ' + line.Cause + '</p>';
				  content += '				<p>Cor Code: ' + line.CorCode + ': ' + line.CorCodeDesc + '</p>';
				  content += '				<p>Correction: ' + line.Correction + '</p>';
				  content += '			</div>';
				  content += '		</td>';
				  content += '	</tr>';
			  });
				content += '	<tr>';
				content += '		<td colspan="4">Please Note: ' + ro[0].PleaseNote + '</td>';
				content += '	</tr>';

			  content += '</table>';
			  content += '</div>';
			  content += '<h5>Update Status</h5>';
			  content += '	<select id="select">';
			  content += '		<option value="">-- Select a status --</option>';
			  content += '		<option value="I have the RO.">I have the RO.</option>';
			  content += '		<option value="I DO NOT have the RO!!!!!!!1">I DO NOT have the RO!!!!!!!1</option>';
			  content += '		<option value="Submitted.">Submitted.</option>';
			  content += '	</select>';
			  content += '<textarea rows="7" id="pdqwarrantyadmin_status_update" name="status_update" type="" required class="entry" placeholder="Enter the new status here."></textarea>';
			  content += '<br />';
			  content += '<button class="submit" id="submitpdqwarrantyadminro">Submit</button>';
			  content += '<button type="button" class="close-can cancelbutton">Cancel</button>';
			  content += '<button id="printBtn" type="button" class="print">Print</button>';
			  content += '<div id="ro-list">';
			  content += '<h4>Status Updates</h4><table>';
			  $.each(statuses, function(i, item) {
			    content += '<tr><td class="label">' + item.eeUserName;
			    content += '<span>' + item.updateTS + '</span></td>';
			    content += '<td class="result">' + item.statusUpdate + '</td></tr>';
			  });
			  content += '</table>';
			  return content;
  		}

			return $.when(getRO(ro), getPdqWarrantyAdminROStatuses(ro))
    		.then(function(a1, a2) {
          return createPdqWarrantyAdminROMarkup(ro, a1[0], a2[0]);
    		});
		},







		warrantyROsListMarkup: function(userName) {

			function createWarrantyROsListMarkup(ros) {
			  var content = '';
			  if (ros.length == 0) {
				  content += '<div id="no-warranty-data">';
				  content += '<img src="../images/success-warranty.jpg" alt="Success at Warranty Calls!" />';
				  content += '</div>';
			  } else {
				  content += '<h4>Warranty Calls</h4>';		  
				  content += '<table id="table" class="warrantyros">';
				  content += '<thead>';
				  content += '<tr id="headings">';
				  content += '<th data-col="ro"><span class="arrow"></span>RO</th>';
				  content += '<th data-col="FirstName"><span class="arrow"></span>Writer</th>';
				  content += '<th data-col="CloseDate" class="col-date"><span class="arrow"></span>Close Date</th>';
				  content += '<th data-col="Customer"><span class="arrow"></span>Customer</th>';
				  content += '<th data-col="HomePhone"><span class="arrow"></span>Home Phone</th>';
				  content += '<th data-col="WorkPhone"><span class="arrow"></span>Work Phone</th>';
				  content += '<th data-col="CellPhone"><span class="arrow"></span>Cell Phone</th>';
				  content += '<th data-col="Vehicle"><span class="arrow"></span>Vehicle</th>';
				  content += '<th data-col="LastCall"><span class="arrow"></span>Last Call</th>';
				  content += '</tr>';
				  content += '</thead>';
				  content += '<tbody>';
				  $.each(ros, function(i, item) {
				    content += '<tr';
				    if (item.aged === '1') {
				      content += ' class="aged24"';
				    }
				    content += '><td>' + item.ro + '</td>';
				    content += '<td class="col-writer">' + item.FirstName + '</td>';
				    content += '<td class="col-date">' + item.CloseDate + '</td>';
				    content += '<td class="col-customer">' + item.Customer + '</td>';
				    content += '<td class="col-phone">' + item.HomePhone + '</td>';
				    content += '<td class="col-phone">' + item.WorkPhone + '</td>';
				    content += '<td class="col-phone">' + item.CellPhone + '</td>';
				    content += '<td class="col-vehicle">' + item.Vehicle + '</td>';
				    content += '<td class="col-update">' + item.LastCall + '<span class="spacer"><a href="#sco/editwarrantyro/' + item.ro + '/' + myData.globalUser.userName + '" class="warrantyeditbutton modal">Edit</a></span></td></tr>';
//				    content += '<td class="col-update">' + item.LastCall + '<span class="spacer"><a data-ro="' + item.ro + '" data-userid="' + myData.globalUser.userName + '" class="warrantyeditbutton modal">Edit</a></span></td></tr>';
				  });
				  content += '</tbody>';
				  content += '</table>';
			  };
			  return content;
			};
			
		  return $.when(getWriterWarrantyCallsData(userName))
		    .then(function(data) {
		      return createWarrantyROsListMarkup(data);
		  });
		},

  	warrantyROMarkup: function(ro) {

  		function createWarrantyROMarkup(ronum, ro, statuses) {
			  var content = '';
			  content += '<span type="button" class="x-close close-can cancelbutton"></span>';
			  content += '<h3>Update the status of this Warranty Call</h3>';
			  content += '<h4>RO Detail</h4>';
			  content += '<div id="rodetail">';
			  content += '<table>';
			  content += '	<tr>';
			  content += '		<th>Name</th>';
			  content += '		<th>Contact</th>';
			  content += '		<th>Vehicle</th>';
			  content += '		<th>RO Info</th>';
			  //content += '		<th>Status</th>';
			  //content += '		<th>Open/Closed</th>';
			  content += '	</tr>';
			  content += '	<tr>';
			  content += '		<td class="customer">' + ro[0].Customer + '</td>';
			  content += '		<td class="contact shortdetails">';
			  content += '			<div>';
			  content += '				<p>Home: ' + ro[0].HomePhone + '</p>';
			  content += '				<p>Work: ' + ro[0].WorkPhone + '</p>';
			  content += '				<p>Cell: ' + ro[0].CellPhone + '</p>';
			  content += '				<p>Email: ' + ro[0].CustomerEMail + '</p>';
			  content += '			</div>';
			  content += '		</td>';
			  content += '		<td class="vehicle">' + ro[0].Vehicle + '</td>';
			  content += '		<td class="ro shortdetails">';
			  content += '			<div>';
			  content += '				<p>RO: <span id="warrantyro">' + ro[0].RO + '</span></p>';
			  content += '				<p>Status: ' + ro[0].ROStatus + '</p>';
			  content += '				<p>O/C: ' + ro[0].OpenDate + ' / ' + ro[0].CloseDate + '</p>';
			  content += '				<p>Writer: ' + ro[0].WriterFirstName + '</p>';			  
			  content += '			</div>';
			  content += '		</td>';
			  content += '	</tr>';
			  content += '</table>';
			  content += '<table class="roshit">';
			  content += '	<tr>';
			  content += '		<th>Line</th>';
			  content += '		<th class="paytype">Pay Type</th>';
			  content += '		<th>Status</th>';
			  content += '		<th>Descr.</th>';
			  content += '	</tr>';
			  $.each(ro[1], function(i, line) {
				  content += '	<tr>';
				  content += '		<td>' + line.Line + '</td>';
				  content += '		<td class="paytype">' + line.PayType + '</td>';
				  content += '		<td>' + line.LineStatus + '</td>';
				  content += '		<td class="longdetails">';
				  content += '			<div>';
				  content += '				<p>Op Code: ' + line.OpCode + ': ' + line.OpCodeDesc + '</p>';
				  content += '				<p>Complaint: ' + line.Complaint + '</p>';
				  content += '				<p>Cause: ' + line.Cause + '</p>';
				  content += '				<p>Cor Code: ' + line.CorCode + ': ' + line.CorCodeDesc + '</p>';
				  content += '				<p>Correction: ' + line.Correction + '</p>';
				  content += '			</div>';
				  content += '		</td>';
				  content += '	</tr>';
			  });
				  content += '	<tr>';
				  content += '		<td colspan="4">Please Note: ' + ro[0].PleaseNote + '</td>';
				  content += '	</tr>';

			  content += '</table>';
			  content += '</div>';
			  content += '<h4>Update Status</h4>';
			  content += '	<select id="select">';
			  content += '		<option value="">-- Select a status --</option>';
			  content += '		<option value="Complete.">Complete.</option>';
			  content += '		<option value="Left message.">Left message.</option>';
			  content += '		<option value="Unable to reach customer.">Unable to reach customer. </option>';
			  content += '	</select>';			  
			  content += '<textarea rows="7" id="warranty_status_update" name="status_update" type="text" class="entry" ';
			  content += 'placeholder="Enter the new status here." title="Enter the new status or hit cancel." x-moz-errormessage="Enter the new status or hit cancel."></textarea>';
			  content += '<div class="complete">';
			  content += '  <input id="warrantyfinished" type="checkbox" name="finished" value="Finshed">';
			  content += '  <label for="Finished-RONumber">Is this Warranty Follow-Up Call complete?</label>';
			  content += '</div>';
			  content += '<br />';
			  content += '<button class="submit" id="submitwarrantyro">Submit</button>';
			  content += '<button type="button" class="close-can cancelbutton">Cancel </button>';
			  content += '<button id="printBtn" type="button" class="print">Print</button>';
			  content += '<div id="ro-list">';
			  content += '<h4>Status Updates</h4><table>';
			  $.each(statuses, function(i, item) {
			    content += '<tr><td class="label">' + item.eeUserName;
			    content += '<span>' + item.callTS + '</span></td>';
			    content += '<td class="result">' + item.description + '</td></tr>';
			  });
			  content += '</table>';
			  content += '</div>';
			  return content;
  		}

			return $.when(getRO(ro), getWarrantyROStatuses(ro))
    		.then(function(a1, a2) {
          return createWarrantyROMarkup(ro, a1[0], a2[0]);
    		});
		},

		serviceROsListMarkup: function(userName) {

			function createServiceROsListMarkup(ros) {
			  var content = '';
			  if (ros.length == 0) {
				  content += '<div id="no-service-data">';
				  content += '<img src="images/success-service.jpg" alt="Success at Service Calls!" />';
				  content += '</div>';
			  } else {
				  content += '<h4>Nissan Service Calls</h4>';		  
				  content += '<table id="table" class="serviceros">';
				  content += '<thead>';
				  content += '<tr id="headings">';
				  content += '<th data-col="Writer"><span class="arrow"></span>Writer</th>';
				  content += '<th data-col="ro"><span class="arrow"></span>RO</th>';
				  content += '<th data-col="CloseDate"><span class="arrow"></span>Close Date</th>';
				  content += '<th data-col="Customer"><span class="arrow"></span>Customer</th>';
				  content += '<th data-col="Vehicle"><span class="arrow"></span>Vehicle</th>';
				  content += '<th data-col="LastCall"><span class="arrow"></span>Last Call</th>';
				  content += '</tr>';
				  content += '</thead>';
				  content += '<tbody>';
				  $.each(ros, function(i, item) {
				    content += '<tr';
				    if (item.aged === '1') {
				      content += ' class="aged24"';
				    }
				    content += '><td class="col-customer">' + item.Writer + '</td>';
				    content += '<td>' + item.ro + '</td>';
				    content += '<td class="col-date">' + item.CloseDate + '</td>';
				    content += '<td class="col-customer">' + item.Customer + '</td>';
				    content += '<td class="col-vehicle">' + item.Vehicle + '</td>';
				    content += '<td class="col-update">' + item.LastCall + '<span class="spacer"><a href="#sco/editservicero/' + item.ro + '/' + myData.globalUser.userName + '" class="serviceeditbutton modal">Edit</a></span></td></tr>';
//				    content += '<td class="col-update">' + item.LastCall + '<span class="spacer"><a data-ro="' + item.ro + '" data-userid="' + myData.globalUser.userName + '" class="serviceeditbutton modal">Edit</a></span></td></tr>';
				  });
				  content += '</tbody>';
				  content += '</table>';
				};
			  return content;
			};
			
		  return $.when(getRY2ServiceCallsData(userName))
		    .then(function(data) {
		      return createServiceROsListMarkup(data);
		  });
		},

  	serviceROMarkup: function(ro) {

  		function createServiceROMarkup(ronum, ro, statuses) {
			  var content = '';
			  content += '<span type="button" class="x-close close-can cancelbutton"></span>';
			  content += '<h3>Update the status of this Honda/Nissan follow-up call</h3>';
			  content += '<h4>RO Detail</h4>';
			  content += '<div id="rodetail">';
			  content += '<table>';
			  content += '	<tr>';
			  content += '		<th>Name</th>';
			  content += '		<th>Contact</th>';
			  content += '		<th>Vehicle</th>';
			  content += '		<th>RO Info</th>';
			  //content += '		<th>Status</th>';
			  //content += '		<th>Open/Closed</th>';
			  content += '	</tr>';
			  content += '	<tr>';
			  content += '		<td class="customer">' + ro[0].Customer + '</td>';
			  content += '		<td class="contact shortdetails">';
			  content += '			<dl>';
			  content += '				<dt>Home: </dt>';
			  content += '				<dd>' + ro[0].HomePhone + '</dd>';
			  content += '				<dt>Work: </dt>';
			  content += '				<dd>' + ro[0].WorkPhone + '</dd>';
			  content += '				<dt>Cell: </dt>';
			  content += '				<dd>' + ro[0].CellPhone + '</dd>';
			  content += '				<dt>Email: </dt>';
			  content += '				<dd>' + ro[0].CustomerEMail + '</dd>';
			  content += '			</dl>';
			  content += '		</td>';
			  content += '		<td class="vehicle">' + ro[0].Vehicle + '</td>';
			  content += '		<td class="ro shortdetails">';
			  content += '			<dl>';
			  content += '				<dt>RO: </dt>';
			  content += '				<dd id="servicero">' + ro[0].RO + '</dd>';
			  content += '				<dt>Status: </dt>';
			  content += '				<dd>' + ro[0].ROStatus + '</dd>';
			  content += '				<dt>O/C: </dt>';
			  content += '				<dd>' + ro[0].OpenDate + '/' + ro[0].CloseDate + '</dd>';
			  content += '				<dt>Writer: </dt>';
			  content += '				<dd>' + ro[0].WriterFirstName + '</dd>';			  
			  content += '			</dl>';
			  content += '		</td>';
			  content += '	</tr>';
			  content += '</table>';
			  content += '<table class="roshit">';
			  content += '	<tr>';
			  content += '		<th>Line</th>';
			  content += '		<th class="paytype">Pay Type</th>';
			  content += '		<th>Status</th>';
			  content += '		<th>Descr.</th>';
			  content += '	</tr>';
			  $.each(ro[1], function(i, line) {
				  content += '	<tr>';
				  content += '		<td>' + line.Line + '</td>';
				  content += '		<td class="paytype">' + line.PayType + '</td>';
				  content += '		<td>' + line.LineStatus + '</td>';
				  content += '		<td class="longdetails">';
				  content += '			<dl>';
				  content += '				<dt>Op Code: </dt>';
				  content += '				<dd>' + line.OpCode + ': ' + line.OpCodeDesc + '</dd>';
				  content += '				<dt>Complaint: </dt>';
			    content += '				<dd>' + line.Complaint + '</dd>';
				  content += '				<dt>Cause: </dt>';
				  content += '				<dd>' + line.Cause + '</dd>';
				  content += '				<dt>Cor Code: </dt>';
				  content += '				<dd>' + line.CorCode + ': ' + line.CorCodeDesc + '</dd>';
				  content += '				<dt>Correction: </dt>';
				  content += '        		<dd>' + line.Correction + '</dd>';
				  content += '			</dl>';
				  content += '		</td>';
				  content += '	</tr>';
			  });
				  content += '	<tr>';
				  content += '		<td colspan="4">Please Note: ' + ro[0].PleaseNote + '</td>';
				  content += '	</tr>';
			  content += '</table>';
			  content += '</div>';
			  content += '<h5>Update Status</h5>';
			  content += '<textarea rows="7" id="service_status_update" name="status_update" type="text" class="entry"';
			  content += 'placeholder="Enter the new status here." title="Enter the new status or hit cancel." x-moz-errormessage="Enter the new status or hit cancel."></textarea>';
			  content += '<div class="complete">';
			  content += '  <input id="servicefinished" type="checkbox" name="finished" value="Finshed">';
			  content += '  <label for="Finished-RONumber">Is this Honda/Nissan follow-up call complete?</label>';
			  content += '</div>';
			  content += '<br />';
			  content += '<button class="submit" id="submitservicero">Submit</button>';
			  content += '<button type="button" class="close-can cancelbutton">Cancel </button>';
			  content += '<div id="ro-list">';
			  content += '<h4>Status Updates</h4><table>';
			  $.each(statuses, function(i, item) {
			    content += '<tr><td class="label">' + item.eeUserName;
			    content += '<span>' + item.callTS + '</span></td>';
			    content += '<td class="result">' + item.description + '</td></tr>';
			  });
			  content += '</table>';
			  return content;
  		}

			return $.when(getRO(ro), getServiceROStatuses(ro))
    		.then(function(a1, a2) {
          return createServiceROMarkup(ro, a1[0], a2[0]);
    		});
		},

		checkOutMarkup: function(userName) {

			function createCheckOutMarkup(data, userName) {
			  var content = '';
			  content += '<span type="button" class="x-close close-can cancelbutton"></span>';
			  content += '<h3>Daily Checkout</h3>';
			  content += '<table class="readonly">';
			  content += '  <tr>';
			  content += '    <td class="item">Cashiering percentage</td>';
			  content += '    <td id="CashieringPercentage" class="number">' + data.Cashiering + '</td>';
			  content += '  </tr>';
			  content += '  <tr>';
			  content += '    <td class="item">Open ROs aged more than 7 days</td>';
			  content += '    <td id="OpenROs" class="number">' + data.AgedROs + '</td>';
			  content += '  </tr>';
			  content += '  <tr>';
			  content += '    <td class="item">Open follow-up calls</td>';
			  content += '    <td id="OpenFollowUp" class="number">' + data.WarrantyCalls + '</td>';
			  content += '  </tr>';
			  content += '    <tr>';
			  content += '      <td class="item">Average hours per RO (week-to-date)</td>';
			  content += '      <td id="AverageHours" class="number">' + data.AvgHoursPerRO + '</td>';
			  content += '    </tr>';
			  content += '  <tr>';
			  content += '    <td class="item">Labor sales (week-to-date)</td>';
			  content += '    <td id="LaborSales" class="number">' + data.LaborSales + '</td>';
			  content += '  </tr>';
			  content += '  <tr>';
			  content += '    <td class="item">ROs opened (week-to-date)</td>';
			  content += '    <td id="ROsOpened" class="number">' + data.ROsOpened + '</td>';
			  content += '  </tr>';
			  content += '  <tr>';
			  content += '    <td class="item">ROs closed (week-to-date)</td>';
			  content += '    <td id="ROsClosed" class="number">' + data.ROsClosed + '</td>';
			  content += '  </tr>';
			  content += '  <tr>';
			  content += '    <td class="item">Inspections requested</td>';
			  content += '    <td id="InspectionsRequested" class="number">' + data.InspectionsRequested + '</td>';
			  content += '  </tr>';
			  content += '</table>';
			  content += '<h5>Enter these fields</h5>';
			  content += '<table>';
			  content += '  <tr>';
			  content += '    <td class="item"><label for="csi">CSI (quarter-to-date)<span class="instr">CSI is a percentage.</span></label></td>';
			  content += '    <td class="number"><input id="csi" class="entry invalid-field" name="CSI" autofocus required /></td>';
			  content += '  </tr>';
			  content += '</table>';
			  content += '<button data-name="' + userName + '" class="submit" id="submitit">Submit</button>';
			  content += '<button type="button" class="close-can cancelbutton">Cancel </button>';
			  return content;
			}

			return $.when(getCheckOutData(userName))
    		.then(function(data) {
          return createCheckOutMarkup(data[0], userName);
   		});

		}
 	};
}

var serviceCheckOut = newServiceCheckOut();

