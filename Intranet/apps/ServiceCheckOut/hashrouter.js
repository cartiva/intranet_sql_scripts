// Service Checkout Routes
// #sco/managerstats -- Manager stats page
// #sco/writerstats/email@here -- Manager drilling down on writer
// #sco/writerstats -- Writer stats page

function handleStatsClicks() {
  $('.def').hide();
  $(document).on('click', function(event) {
    if ($('span.def').hasClass('defvisible')) {
      event.preventDefault();
      $('.def').hide();
      $('span').removeClass('defvisible');
    }
  });
  $('.weekly').on('click', '.def-icon', function(event) {
    event.stopPropagation();
    if ($(event.target).next('span').hasClass('defvisible')) {
      $('.def').hide();
      $(event.target).next('span').removeClass('defvisible');
    } else {
      $('.defvisible').hide();
      $('span').removeClass('defvisible');
      $(event.target).next('span').addClass('defvisible');
      $(event.target).next('span').show();
    }
  });
}
      
$(window).bind('hashchange', function(e) {

  // Service Checkout
  if (location.hash === "#sco/managerstats")                      { routeManagerStats(myData.globalUser.userName) } else
  if (location.hash === "#sco/writerstats")                       { routeWriterStats(myData.globalUser.userName) } else
  if (location.hash.indexOf('#sco/writerstats/') != -1)           { routeWriterStats(location.hash.split("/").pop()) } else
  if (location.hash === "#sco/agedros")                           { routeAgedROsList(myData.globalUser.userName) } else 
  if (location.hash.indexOf("#sco/agedros/") != -1)               { routeAgedROsList(location.hash.split("/").pop()) } else
  if (location.hash.indexOf("#sco/editagedro/") != -1)            { routeEditAgedRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else
  if (location.hash.indexOf("#sco/submitagedro/") != -1)          { routeSubmitAgedRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else
  if (location.hash === "#sco/currentros")                        { routeCurrentROsList(myData.globalUser.userName) } else 
  if (location.hash.indexOf("#sco/currentros/") != -1)            { routeCurrentROsList(location.hash.split("/").pop()) } else
  if (location.hash.indexOf("#sco/editcurrentro/") != -1)         { routeEditCurrentRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else
  if (location.hash.indexOf("#sco/submitcurrentro/") != -1)       { routeSubmitCurrentRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else
  if (location.hash === "#sco/warrantycalls")                     { routeWarrantyROsList(myData.globalUser.userName) } else
  if (location.hash.indexOf("#sco/warrantycalls/") != -1)         { routeWarrantyROsList(location.hash.split("/").pop()) } else
  if (location.hash.indexOf("#sco/editwarrantyro/") != -1)        { routeEditWarrantyRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else
  if (location.hash.indexOf("#sco/submitwarrantyro/") != -1)      { routeSubmitWarrantyRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else
  if (location.hash === "#sco/ry2servicecalls")                   { routeServiceROsList(myData.globalUser.userName) } else
  if (location.hash.indexOf("#sco/ry2servicecalls/") != -1)       { routeServiceROsList(location.hash.split("/").pop()) } else
  if (location.hash.indexOf("#sco/editservicero/") != -1)         { routeEditServiceRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else
  if (location.hash.indexOf("#sco/submitservicero/") != -1)       { routeSubmitServiceRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else
  
  if (location.hash === "#sco/warrantyadminros")                     { routeWarrantyAdminROsList(myData.globalUser.userName) } else
  if (location.hash.indexOf("#sco/warrantyadminros/") != -1)         { routeWarrantyAdminROsList(location.hash.split("/").pop()) } else
  if (location.hash.indexOf("#sco/editwarrantyadminro/") != -1)   { routeEditWarrantyAdminRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else
  if (location.hash.indexOf("#sco/submitwarrantyadminro/") != -1) { routeSubmitWarrantyAdminRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else

  if (location.hash === "#sco/pdqwarrantyadminros")                     { routePdqWarrantyAdminROsList(myData.globalUser.userName) } else
  if (location.hash.indexOf("#sco/pdqwarrantyadminros/") != -1)         { routePdqWarrantyAdminROsList(location.hash.split("/").pop()) } else
  if (location.hash.indexOf("#sco/editpdqwarrantyadminro/") != -1)   { routeEditPdqWarrantyAdminRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else
  if (location.hash.indexOf("#sco/submitpdqwarrantyadminro/") != -1) { routeSubmitPdqWarrantyAdminRO(location.hash.split("/")[2], location.hash.split("/")[3]) } else

  
  if (location.hash === "#sco/checkout")                          { routeCheckOut(myData.globalUser.userName) } else
  if (location.hash === "#sco/submitcheckout")                    { routeSubmitCheckOut(myData.globalUser.userName) }

  //Stats
  function routeManagerStats(userName) {
    $.when(serviceCheckOut.dateHeadingMarkup(),
           serviceCheckOut.managerStatsMarkup(userName, 'this'),
           serviceCheckOut.writerListMarkup(userName))
      .then(function(dateheadinghtml, managerstatshtml, writerlisthtml) {
      $('.stats-container').empty().append('<h4>Service stats for this week</h4>');
      $('.stats-container').append(dateheadinghtml);
      $('.stats-container').append('<p>This table represents ALL the Service Writers combined stats.</p>');        
      $('.stats-container').append(managerstatshtml);
      $('.stats-container').append(writerlisthtml);
      handleStatsClicks();         
    });
    window.location.hash = '';
  }

  function routeWriterStats(userName) {
    $.when(serviceCheckOut.displayName(userName),
           serviceCheckOut.dateHeadingMarkup(),
           serviceCheckOut.writerSalesMarkup(userName),
           serviceCheckOut.writerStatsMarkup(userName))
      .then(function(displayName, dateheadinghtml, writersaleshtml, writerstatshtml) {
      $('.stats-container').empty().append('<h4>Service Stats Summary for ' + displayName + '</h4>');
      $('.stats-container').append(dateheadinghtml);
      $('.stats-container').append(writersaleshtml);
      //$('.stats-container').append('<p>This table represents stats for an individual writer.</p>');        
      $('.stats-container').append(writerstatshtml);
      handleStatsClicks();         
    });
    window.location.hash = '';
  }

  // Current ROs List
  function routeCurrentROsList(userName) {
    $('.stats-container').empty().append("<h4>Loading Current RO's...</h4>");
    $.when(serviceCheckOut.currentROsListMarkup(userName))
      .then(function(currentroslisthtml) {
        $('.stats-container').empty().append(currentroslisthtml);
        $('.currentros').sortIt();
      }); // then

    window.location.hash = '';
  } // if

  // Edit Current RO
  function routeEditCurrentRO(RO, userName) {
    $.when(serviceCheckOut.currentROMarkup(RO, userName))
      .then(function(data) {
        modal.open({content: data, 
          submitselector: '.submit', 
          submitaction: '#sco/submitcurrentro/' + RO + '/' + userName,
          cancelselector: '.cancelbutton'});
          //appends dropdown text into textarea
          $('#select').on('change', function() {
            $('#current_status_update').val($('#select').val())
          }); // change
      }) // then
      .fail(function() {
        alert('Could not open Edit Current RO window');
      }); // fail
  }

  // Submit a Current RO Edit
  function routeSubmitCurrentRO(RO, userName) {
      
      function validateCurrentRO(event) {
        var validated = $.Deferred();
        if ($('#current_status_update').val() === '') {
          alert('Enter a status or cancel.');
          validated.fail();
        } else {
          validated.resolve();
        }
        return validated;
      }

      validateCurrentRO(event).done(function() {
        $.post(baseURL + '/currentro/' + $('#currentro').text(), 
          {'ee_username': userName,
           'status_update': $('#current_status_update').val()})
            .fail(function() {
              alert('Error submitting current RO');
            })
            .done(function() {
              modal.close();
              window.location.hash = '#sco/currentros/' + userName;
            });
        });
  }

  // Aged ROs List
  function routeAgedROsList(userName) {
    $('.stats-container').empty().append("<h4>Loading Aged RO's...</h4>");
    $.when(serviceCheckOut.agedROsListMarkup(userName))
      .then(function(agedroslisthtml) {
        $('.stats-container').empty().append(agedroslisthtml);
        $('.agedros').sortIt();
      }); // then

    window.location.hash = '';
  } // if

  // Edit Aged RO
  function routeEditAgedRO(RO, userName) {
    $.when(serviceCheckOut.agedROMarkup(RO, userName))
      .then(function(data) {
        modal.open({content: data, 
          submitselector: '.submit', 
          submitaction: '#sco/submitagedro/' + RO + '/' + userName,
          cancelselector: '.cancelbutton'});
          //appends dropdown text into textarea
          $('#select').on('change', function() {
            $('#aged_status_update').val($('#select').val())
          }); // change
      }) // then
      .fail(function() {
        alert('Could not open Edit Aged RO window');
      }); // fail
  }

  // Submit an Aged RO Edit
  function routeSubmitAgedRO(RO, userName) {
      
      function validateAgedRO(event) {
        var validated = $.Deferred();
        if ($('#aged_status_update').val() === '') {
          alert('Enter a status or cancel.');
          validated.fail();
        } else {
          validated.resolve();
        }
        return validated;
      }

      validateAgedRO(event).done(function() {
        $.post(baseURL + '/agedro/' + $('#agedro').text(), 
          {'ee_username': userName,
           'status_update': $('#aged_status_update').val()})
            .fail(function() {
              alert('Error submitting aged RO');
            })
            .done(function() {
              modal.close();
              window.location.hash = '#sco/agedros/' + userName;
            });
        });
  }

  // Warranty Calls 
  function routeWarrantyROsList(userName) {
    $('.stats-container').empty().append('<h4>Loading Warranty Calls...</h4>');
    $.when(serviceCheckOut.warrantyROsListMarkup(userName))
      .then(function(warrantyroslisthtml) {
        $('.stats-container').empty().append(warrantyroslisthtml);
        $('.warrantyros').sortIt();
      }); // then

    window.location.hash = '';
  }

  // Edit Waranty RO
  function routeEditWarrantyRO(RO, userName) {
    $.when(serviceCheckOut.warrantyROMarkup(RO, userName))
      .then(function(data) {
        modal.open({content: data, 
          submitselector: '.submit', 
          submitaction: '#sco/submitwarrantyro/' + RO + '/' + userName,
          cancelselector: '.cancelbutton'});
          //appends dropdown text into textarea
          $('#select').on('change', function() {
            $('#warranty_status_update').val($('#select').val())
          }); // change
      }) // then
      .fail(function() {
        alert('Could not open Edit Warranty RO window');
      }); // fail
  }

  function routeSubmitWarrantyRO(RO, userName) {
      
    function validateWarrantyRO(event) {
      var validated = $.Deferred();
      if ($('#warranty_status_update').val() === '') {
        alert('Enter a status or cancel.');
        validated.fail();
      } else {
        validated.resolve();
      }
      return validated;
    }

    validateWarrantyRO(event).done(function() {
      var completed;
      if ($('#warrantyfinished').is(':checked')) {
        completed = 'true';
      } else {
        completed = 'false';
      }
      $.post(baseURL + '/warrantycall/' + $('#warrantyro').text(), 
             {'ee_username': userName,
              'description': $('#warranty_status_update').val(),
              'completed': completed})
        .fail(function() {
          alert('Error submitting warranty RO');
        })
        .done(function() {
          modal.close();
          window.location.hash = '#sco/warrantycalls/' + userName;
        });
    });
  }

  // RY2 Service Calls  
  function routeServiceROsList(userName) {
    $('.stats-container').empty().append('<h4>Loading Service Calls...</h4>');
    $.when(serviceCheckOut.serviceROsListMarkup(userName))
      .then(function(serviceroslisthtml) {
        $('.stats-container').empty().append(serviceroslisthtml);
        $('.serviceros').sortIt();
      }); // then

    window.location.hash = '';
  }

// Edit Service RO
  function routeEditServiceRO(RO, userName) {
    $.when(serviceCheckOut.serviceROMarkup(RO, userName))
      .then(function(data) {
        modal.open({content: data, 
          submitselector: '.submit', 
          submitaction: '#sco/submitservicero/' + RO + '/' + userName,
          cancelselector: '.cancelbutton'});
      }) // then
      .fail(function() {
        alert('Could not open Edit Service RO window');
      }); // fail
  }

  function routeSubmitServiceRO(RO, userName) {
      
    function validateServiceRO(event) {
      var validated = $.Deferred();
      if ($('#service_status_update').val() === '') {
        alert('Enter a status or cancel.');
        validated.fail();
      } else {
        validated.resolve();
      }
      return validated;
    }

    validateServiceRO(event).done(function() {
      var completed;
      if ($('#servicefinished').is(':checked')) {
        completed = 'true';
      } else {
        completed = 'false';
      }
      $.post(baseURL + '/ry2rostatuses/' + $('#servicero').text(), 
             {'ee_username': userName,
              'status_update': $('#service_status_update').val(),
              'completed': completed})
        .fail(function() {
          alert('Error submitting service RO');
        })
        .done(function() {
          modal.close();
          window.location.hash = "#sco/ry2servicecalls/" + userName;
        });
    });
  }






  // Warranty Admin ROs List
  function routeWarrantyAdminROsList(userName) {
    $('.stats-container').empty().append("<h4>Loading Warranty Admin RO's...</h4>");
    $.when(serviceCheckOut.warrantyAdminROsListMarkup(userName))
      .then(function(warrantyadminroslisthtml) {
        $('.stats-container').empty().append(warrantyadminroslisthtml);
        $('.warrantyadminros').sortIt();
      }); // then

    window.location.hash = '';
  } // if

  // Edit Warranty Admin RO
  function routeEditWarrantyAdminRO(RO, userName) {
    $.when(serviceCheckOut.warrantyAdminROMarkup(RO, userName))
      .then(function(data) {
        modal.open({content: data, 
          submitselector: '.submit', 
          submitaction: '#sco/submitwarrantyadminro/' + RO + '/' + userName,
          cancelselector: '.cancelbutton'});
          //appends dropdown text into textarea
          $('#select').on('change', function() {
            $('#warrantyadmin_status_update').val($('#select').val())
          }); // change
      }) // then
      .fail(function() {
        alert('Could not open Edit Warranty Admin RO window');
      }); // fail
  }

  // Submit a Warranty Admin RO Edit
  function routeSubmitWarrantyAdminRO(RO, userName) {
      
      function validateWarrantyAdminRO(event) {
        var validated = $.Deferred();
        if ($('#warrantyadmin_status_update').val() === '') {
          alert('Enter a status or cancel.');
          validated.fail();
        } else {
          validated.resolve();
        }
        return validated;
      }

      validateWarrantyAdminRO(event).done(function() {
        $.post(baseURL + '/warrantyadminro/' + $('#warrantyadminro').text(), 
          {'ee_username': userName,
           'status_update': $('#warrantyadmin_status_update').val()})
            .fail(function() {
              alert('Error submitting warranty admin RO');
            })
            .done(function() {
              modal.close();
              window.location.hash = '#sco/warrantyadminros/' + userName;
            });
        });
  }



// PDQ Warranty Admin ROs List
  function routePdqWarrantyAdminROsList(userName) {
    $('.stats-container').empty().append("<h4>Loading PDQ Warranty Admin RO's...</h4>");
    $.when(serviceCheckOut.PdqWarrantyAdminROsListMarkup(userName))
      .then(function(pdqwarrantyadminroslisthtml) {
        $('.stats-container').empty().append(pdqwarrantyadminroslisthtml);
        $('.pdqwarrantyadminros').sortIt();
      }); // then

    window.location.hash = '';
  } // if

  // Edit PDQ Warranty Admin RO
  function routeEditPdqWarrantyAdminRO(RO, userName) {
    $.when(serviceCheckOut.PdqWarrantyAdminROMarkup(RO, userName))
      .then(function(data) {
        modal.open({content: data, 
          submitselector: '.submit', 
          submitaction: '#sco/submitpdqwarrantyadminro/' + RO + '/' + userName,
          cancelselector: '.cancelbutton'});
          //appends dropdown text into textarea
          $('#select').on('change', function() {
            $('#pdqwarrantyadmin_status_update').val($('#select').val())
          }); // change
      }) // then
      .fail(function() {
        alert('Could not open Edit PDQ Warranty Admin RO window');
      }); // fail
  }

  // Submit a PDQ Warranty Admin RO Edit
  function routeSubmitPdqWarrantyAdminRO(RO, userName) {
      
      function validatePdqWarrantyAdminRO(event) {
        var validated = $.Deferred();
        if ($('#pdqwarrantyadmin_status_update').val() === '') {
          alert('Enter a status or cancel.');
          validated.fail();
        } else {
          validated.resolve();
        }
        return validated;
      }

      validatePdqWarrantyAdminRO(event).done(function() {
        $.post(baseURL + '/pdqwarrantyadminro/' + $('#pdqwarrantyadminro').text(), 
          {'ee_username': userName,
           'status_update': $('#pdqwarrantyadmin_status_update').val()})
            .fail(function() {
              alert('Error submitting PDQ warranty admin RO');
            })
            .done(function() {
              modal.close();
              window.location.hash = '#sco/pdqwarrantyadminros/' + userName;
            });
        });
  }





  //Checkout
  function routeCheckOut(userName) {
    $.when(serviceCheckOut.checkOutMarkup(userName))
     .then(function(checkouthtml) {
        modal.open({content: checkouthtml, 
                    submitselector: '.submit', 
                    submitaction: "#sco/submitcheckout",
                    cancelselector: '.cancelbutton'});
    });
  }

  function routeSubmitCheckOut(userName) {

    function validateCheckOut(event) {
      var validated = $.Deferred();
      var errorMessage = '',
        csi = $('#csi').val();
        //walkarounds = $('#WalkaroundsToday').val();
      // check the input values and build up an error message
      if (isNaN(csi) || csi < 0 || !csi) {
        errorMessage += 'CSI must be a positive number';
      }
      /*if (isNaN(walkarounds) || walkarounds < 0 || !walkarounds) {
        errorMessage += '\nWalkarounds must be a postive number';
      }*/
      if (errorMessage) {
        alert(errorMessage);
        validated.fail();
      } else {
        validated.resolve();
      }
      return validated;
    }

    validateCheckOut().done(function() {
      $.post(baseURL + '/checkouts/' + userName, 
             {'ee_username': userName,
              'CSI': $('#csi').val()})
        .fail(function() {
          alert('Error submitting writer checkout');
        })
        .done(function() {
          modal.close();
          window.location = "../main/dashboard.php";
        });
    });
  }
});
