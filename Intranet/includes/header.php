<?php
//$logged_in = false;
if (isset($_SESSION['LoggedIn'])) {
  $logged_in = true;
  $first_name = $_SESSION['FirstName'];
} else {
  $logged_in = false;
}

?>
<!doctype html>
<!--
  index.html
  Body Parts Tracking
-->
<html>
<head>
  <title>Rydell Intranet</title>
  <meta charset="utf-8">
  <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
  <meta http-equiv="X-UA-Compatible" content="IE=10; IE=9; IE=8; IE=7; IE=Edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Rydell Company Intranet">
  <meta name="keywords" content="Rydell, more, keywords, here">
  <meta name="publisher" content="Rydell Corporation" />
  <meta name="author" content="Cartiva Development Team">
  <meta name="author-email" content="mailto:libby@cartiva.com" />
  <meta name="copyright" content="Copyright (c) 2013 Cartiva Development" />

  <!-- Favicons -->
  <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon-16.png" />
  <link rel="icon" type="image/png" sizes="24x24" href="../images/favicon-24.png" />
  <link rel="icon" type="image/png" sizes="32x32" href="../images/favicon-32.png" />
  <link rel="icon" type="image/png" sizes="48x48" href="../images/favicon-48.png" />
  <link rel="icon" type="image/png" sizes="64x64" href="../images/favicon-64.png" />
  <link rel="icon" type="image/png" sizes="256x256" href="../images/favicon-256.png" />
  <link rel="icon" type="image/png" sizes="512x512" href="../images/favicon-512.png" />
  <link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico" />

  <!-- Third-party stylesheets -->
  <link rel="stylesheet" type="text/css" href="http://go.cartiva.com/libraries/bootstrap/css/glyphicons.css">

  <!-- Cartiva stylesheets -->
  <link rel="stylesheet" type="text/css" href="http://go.cartiva.com/libraries/fonts/type.css">
  <link rel="stylesheet" href="../css/reset.css?v1.7" type="text/css" />
  <link rel="stylesheet" href="../css/stylesheet.css?v1.7" type="text/css" />
  <link rel="stylesheet" href="../css/stylesheet_sub.css?v1.7" type="text/css" />
  <link rel="stylesheet" href="../css/app-np.css?v1.7" type="text/css" />
  <link rel="stylesheet" href="../css/app-servicestats.css?v1.7" type="text/css" />
  <link rel="stylesheet" href="../css/print.css?v1.7" type="text/css" media="print" />

  <!-- Third-party JS -->
  <script src="../js/jq/jquery-1.9.1.js"></script>
  <script src="../js/jq/jquery.uriAnchor-1.1.3.js"></script>
  <script src="../js/jq/jquery.event.gevent-0.1.10.js"></script>
  <script src="../js/jq/jquery.event.ue-0.3.2.js"></script>
  <!--<script src="../js/jq/jquery.numeral.js"></script>-->
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/1.4.5/numeral.min.js"></script>
  <!-- <script src="../js/jq/jquery.moment.js"></script> -->
  <script src="http://go.cartiva.com/libraries/javascript/moment.min.js"></script>
  <script src="../js/car.js"></script>
  <script src="../js/car.security.js"></script>
  <script src="../js/car.teampay.js"></script>
  <script src="../js/car.teampay.data.js"></script>
  <script src="../js/car.teampay.splash.js"></script>
  <script src="../js/car.teampay.admin.js"></script>
  <script src="../js/car.teampay.hpw.js"></script>
  <script src="../js/car.teampay.ros.js"></script>

  <!-- Cartiva JS -->
  <script src="http://go.cartiva.com/libraries/javascript/jump-to-top.js"></script>
  <script src="http://go.cartiva.com/libraries/javascript/cartiva-sortcolumns.js?"></script>

  <!-- Old scripts -->
  <script src="../js/cartivautils.js?v1.7"></script>
  <script src="../js/base.js?v1.7"></script>
  <script src="../js/sidebar.js?v1.7"></script>
  <script src="../js/cartivamodal.js?v1.7"></script>

  <script src="../apps/netpromoter/hashrouter.js?v1.7"></script>
  <script src="../apps/netpromoter/datarouter.js?v1.7"></script>
  <script src="../apps/netpromoter/markup.js?v1.7"></script>
  
  <script src="../apps/servicecheckout/hashrouter.js?v1.7"></script>
  <script src="../apps/servicecheckout/datarouter.js?v1.7"></script>
  <script src="../apps/servicecheckout/markup.js?v1.7"></script>
  
  <script src="../apps/employees/hashrouter.js?v1.7"></script>
  <script src="../apps/employees/datarouter.js?v1.7"></script>
  <script src="../apps/employees/employees.js?v1.7"></script>
    
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
   <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js?v1.7"></script>
   <![endif]-->

 </head>

 <body>
  <div id="body-wrap">
    <header>
      <hgroup id="top-bar">
        <!-- This will contain the login area, both before and after login -->
        <div id="login-form">
          <?php if ($logged_in) {?>
          <div class="dropdown">
            <div class="dropclick">Hi, <?php echo $first_name; ?></div><span></span>
            <ul class="dditems">
              <li class="arrow"></li>
              <li><a href="../main/dashboard.php"><span>Your dashboard</span></a></li>
              <li class="section-start"><a href="../main/logout.php"><span>Log Out</span></a></li>
            </ul>
          </div>
          <?php } else {?>
          <div class="dropdown">
            <div class="dropclick">Login!</div>
            <ul class="dditems">
              <li class="arrow"></li>
              <li><?php require('./login_form.php');?></li>
            </ul>
          </div>
          <?php }?>
        </div>
        <nav id="sitemap">
          <ul>
            <li><a href="../main/coming-soon.php"><span>Help</span></a></li>
            <li><a href="../main/contact.php"><span>Contact Us</span></a></li>
            <li><a href="http://www.facebook.com/Rydellcars" target="_blank"><span>Facebook Page</span></a></li>
            <li><a href="http://www.rydellcars.com/" target="_blank"><span>Rydell Website</span></a></li>
            <?php if ($logged_in) {?>
            <li><a href="../main/dashboard.php"><span>Dashboard</span></a></li>
            <?php }?>
          </ul>
        </nav>
      </hgroup>
      <hgroup>
        <h1>
          <?php if ($logged_in) {?>
            <a href="./dashboard.php">
            <?php } else {?>
            <a href="./homepage.php">
          <?php }?>
            <span>Rydell Intranet</span>
          </a>
        </h1>
        <span class="tagline">Your go-to place for all things Rydell</span>
      </hgroup>
      
      <nav id="main">
        <ul>
          <li><a href="coming-soon.php"><span>Resources</span></a></li>
          <li><a href="coming-soon.php"><span>Jobs</span></a></li>
          <li><a href="coming-soon.php"><span>Calendar</span></a></li>
          <li><a href="coming-soon.php"><span>IT Info</span></a></li>
          <li><a href="coming-soon.php"><span>Employees</span></a></li>
        </ul>
      </nav>
      <!--Test data identifier-->
    </header>  
      <span id="testdata">This is test data. It's ok to mess with it...</span>