
</div><!-- #wrap -->
<footer>
  <aside>
   <div class="search" id="search">
    <form method="post" action="/index.php"  >
      <div class='hiddenFields'>
        <input type="hidden" name="ACT" value="6" />
        <input type="hidden" name="XID" value="00a892aa75f19ed15c5daa00db13c01bf97e6f16" />
        <input type="hidden" name="RES" value="" />
        <input type="hidden" name="meta" value="G/N8Tbk+v4LdchLOjtAeYl2Ii9u3QNWAMF04CDorkP7Wzi8tRRXMnRXQxeT+1TZB3YLAd0vrk+oEVK30QZyG0ofCuUvnte8LXTga7HOc0JPQTtEnTLP89vcMcRdDzrIIiGJtTJ2Ci4T/ObSX3ZutJgJ2OJ5jbPXnD6+BmiG2R0/syXhmzA0gAibtkOKnLPJafYk8yMY5x0Nbt3sZD9iT//iZqZzWgRAuVv4JMCkyRGtG8nqRTAQF3mb3Lli8WTK+Gq7gy4pYQhTgz0TACuHKqAhMjhAIUYM9uguq7q1H/3DD15xhv7eCnl3m1AriduBghZ1tNbJBsIkQJJui9yWmyA==" />
        <input type="hidden" name="site_id" value="1" />
      </div>


      <!-- This will contain the search bar for the footer -->
      <h4><span>Can't find what you're looking for?</span></h4>
      <input type="text" name="keywords" class="keywords" placeholder="Search the Rydell Intranet" size="10" maxlength="100" />
      <!--<a href="search">Advanced Search</a> -->
      <input type="submit" value="" class="submit" />
      <!-- END Search form -->
    </form>
  </div>
</aside>
<div id="epic">
  <section>
    <h5>Main Sections</h5>
    <ul>
      <!--<li><a href="/index.php" class="selected"><span>Home</span></a>-->
      <li><a href="coming-soon.php"><span>Resources</span></a></li>
      <li><a href="coming-soon.php"><span>Jobs</span></a></li>
      <li><a href="coming-soon.php"><span>Calendar</span></a></li>
      <li><a href="coming-soon.php"><span>IT Info</span></a></li>
      <li><a href="coming-soon.php"><span>Employees</span></a></li>
      <!-- <li><a href="coming-soon.php"><span>Departments</span></a></li> -->
    </ul>
  </section>
  <section>
    <h5>Sitemap</h5>
    <ul>
      <li><a href="coming-soon.php"><span>Help</span></a></li>
      <li><a href="contact.php"><span>Contact Us</span></a></li>
      <li><a href="http://www.facebook.com/Rydellcars" target="_blank"><span>Facebook Page</span></a></li>
      <li><a href="http://www.rydellcars.com/" target="_blank"><span>Rydell Website</span></a></li>
    </ul>
  </section>
  <section>
    <h5>Outside Links</h5>
    <ul>
      <li><a href="http://mail.rydellchev.com:8383/" target="_blank"><span>Rydell Email</span></a></li>
      <li><a href="http://mail.gfhonda.com:8383/" target="_blank"><span>Honda Email</span></a></li>
      <li><a href="http://www.gmnacsi.com/gmcsi/sec_login.asp" target="_blank"><span>Dealer Pulse</span></a></li>
      <li><a href="http://www.gmglobalconnect.com/" target="_blank"><span>GM Global Connect</span></a></li>
      <li><a href="http://www.rydellcommunity.ning.com/" target="_blank"><span>Rydell Community</span></a></li>
    </ul>
  </section>
  <section>
    <h5>More Outside Links</h5>
    <ul>
      <li><a href="https://www.dealertrack.com/public/login.fcc?TYPE=33554432&REALMOID=06-fd77313c-8b24-11d4-aa8c-000629858070&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=ZmQrWp0ZwhTU3n4KbxTUP1hkSt1K7W8YrKMLsXQtdaPSTqkxEWfAVJT7jlXK4K37&TARGET=-SM-%2f" target="_blank"><span>Dealertrack</span></a></li>
      <li><a href="https://www.autopartners.net/gmentsso/UI/Login?goto=https%3A%2F%2Fwww.autopartners.net%3A443%2Fapps%2Fsporef%2Fsi.html" target="_blank"><span>GM Autoparts</span></a></li>
      <li><a href="https://www.gmtraining.com/HomePage/LoginPage.asp" target="_blank"><span>GM Training</span></a></li>
      <li><a href="http://new.fosterstraining.com/" target="_blank"><span>Fosters Training</span></a></li>
    </ul>
  </section>
</div>
<!-- Login dropdown -->
<script>

// This fires up the teampay base module

$(function() {
  $('.dropclick').on('click', function(e) {
    e.stopPropagation();
    $('.dditems').slideToggle('fast');
  });

  $(document).on('click', function(e) {
    if( $(e.target).hasClass('dropdown') || $(e.target).hasClass('other-stuff') || $(e.target).attr('class') === 'dditems'){
        
    }
    else{$('.dditems').hide();}
  });
});
</script>
<!-- END Login dropdown --></footer>
</body>
</html>
