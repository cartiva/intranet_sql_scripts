<?php
if (isset($_SESSION['LoggedIn'])) {
  $logged_in = true;
  $first_name = $_SESSION['FirstName'];
} else {
  $logged_in = false;
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Rydell Intranet</title>
  <meta http-equiv="X-UA-Compatible" content="IE=10; IE=9; IE=8; IE=7; IE=Edge" />
  <meta charset="utf-8">
  <meta name="description" content="Rydell company intranet">
  <meta name="keywords" content="Rydell, more, keywords, here">
  <meta name="google-site-verification" content="tbd" />
  <meta http-equiv="pragma" content="no-cache" />
  <meta http-equiv="expires" content="-1" />
  <meta name="publisher" content="Rydell Corporation" />
  <meta name="author" content="Libby Symons">
  <meta name="author-email" content="mailto:libby@cartiva.com" />
  <meta name="copyright" content="Copyright (c) 2013 Libby Symons" />
  <link rel="shortcut icon" href="images/favicon.ico" />
  <!-- Libraries -->
  <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js'></script>
  <script src="./scripts/cartivautils.js"></script>

  <!-- END Libraries -->
  <!-- <link rel="stylesheet" href="css/dummy.css" type="text/css" /> -->
  <link rel="stylesheet" href="css/reset.css" type="text/css" />
  <link rel="stylesheet" href="css/type.css" type="text/css" />
  <link rel="stylesheet" href="css/stylesheet.css" type="text/css" />
  <link rel="stylesheet" href="css/stylesheet_fullscreen.css" type="text/css" />

  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
  <!-- <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-responsive.css"> -->
  <link rel="stylesheet" type="text/css" href="detail/detaillist.css">

  <!--<link rel="stylesheet" href="css/print.css" type="text/css" media="print" /> -->
  <!--[if lt IE 9]>
   <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
   <![endif]-->

  <style type="text/css">
    
  </style>

</head>

 <body>
  <div id="body-wrap">
    <header>
      <hgroup id="top-bar">
        <!-- This will contain the login area, both before and after login -->
        <div id="login-form">
          <?php if ($logged_in) {?>
          <div class="dropdown">
            <div class="dropclick">Hi, <?php echo $first_name; ?></div><span></span>
            <ul class="dditems">
              <li class="arrow"></li>
              <li><a href="./dashboard.php"><span class="dash">Your dashboard</span></a></li>
                
                <!-- Tasklist per memberGroup -->
                <li><a href="./detail/detaillist.php"><span>Command Center</span></a></li>
                <li><a href="./detail/whiteboard.php"><span>Whiteboard</span></a></li>
                <!-- END Tasklist per memberGroup -->

              <li class="section-start"><a href="./logout.php"><span>Log Out</span></a></li>
            </ul>
          </div>
          <?php } else {?>
          <div class="dropdown">
            <div class="dropclick">Login!</div>
            <ul class="dditems">
              <li class="arrow"></li>
              <li><?php require('./login_form.php');?></li>
            </ul>
          </div>
          <?php }?>
        </div>
        <!-- <nav id="sitemap">
          <ul id="fullscreen">
            <?php if ($logged_in) {?>
            <li><a href="./dashboard.php"><span>Dashboard</span></a></li>
            <?php }?>
          </ul>
        </nav> -->
      </hgroup>
      <hgroup>
        <h1>
          <?php if ($logged_in) {?>
            <a href="./dashboard.php">
            <?php } else {?>
            <a href="./homepage.php">
          <?php }?>
            <span>Rydell Intranet</span>
          </a>
        </h1>
        <span class="tagline">Your go-to place for all things Rydell</span>
      </hgroup>

      <!--Test data identifier-->
      <span id="testdata">This is test data. It's ok to mess with it...</span>
    </header>  