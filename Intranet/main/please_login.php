<?php
session_start();
require('../includes/header.php'); // include the header
?>

<!-- Content section for the main page only -->
<div id="content-main">
  <div class="simple">
    <section class="gray-box">
	    <h4>Please login to view this page</h4>

	    <?php require('../main/login_form.php');?>
    </section>
  </div>

  <div id="clear"></div>
  <div id="push"></div><!-- pushes content down to sicky footer -->
</div>

<?php
require('../includes/footer.php');
?>