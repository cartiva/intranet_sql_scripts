<?php
session_start();
require('../webservice/utils.php');
require_login();
?>


<!-- Content section for the sub-pages -->
<div id="content-sub">
  <div class="sidebar">
    <div id="sidebar-widgets">
    </div>
  </div><!-- .sidebar -->

<div class="stuff">
  <div id="notices">
  </div><!-- #notices -->

  <section class="stats-container">
  	<!-- ------------- Tech Splash Page ------------- -->
	<h4>Your ROs</h4>
	<h6>for the pay-period 
		<span id="fromDate">November 3 </span> - 
		<span id="thruDate">November 16, 2013 </span>
	</h6>

	<h5 class="section-start">Open ROs</h5>
	<table class="open"> 
		<thead> 
			<tr id="headings"> 
				<th><span class="arrow"></span>Flag Date</th> 
				<th class="column"><span class="arrow"></span>RO</th> 
				<th><span class="arrow"></span>Line</th> 
				<th class="Today"><span class="arrow"></span>Flag Hours</th>
			</tr> 
		</thead> 
		<tbody> 
			<tr> 
				<td>11/12/13</td> 
				<td>16135631</td> 
				<td>2</td>
				<td>2.3</td>
			</tr>
			<tr class="total"> 
				<td class="column">Totals</td> 
				<td></td>
				<td></td>
				<td>100</td>			
			</tr>
		</tbody> 
	</table>

	<h5 class="section-start">Closed ROs</h5>
	<h6>Mon Nov. 3, 2013</h6>
	<table class="closed"> 
		<thead> 
			<tr id="headings"> 
				<th><span class="arrow"></span>Date Closed</th>
				<th><span class="arrow"></span>RO</th> 
				<th><span class="arrow"></span>Line</th> 
				<th><span class="arrow"></span>Flag Hours</th>
			</tr> 
		</thead> 

		<tbody> 
			<tr> 
				<td>Mon Nov. 3, 2013</td>
				<td>16135631</td> 
				<td>2</td>
				<td>2.3</td>
			</tr>
			<tr class="total"> 
				<td class="column">Totals</td> 
				<td></td>
				<td></td>
				<td>100</td>			
			</tr>
		</tbody> 
	</table>
	<h6>Tues Nov. 3, 2013</h6>
	<table class="closed"> 
		<thead> 
			<tr id="headings"> 
				<th><span class="arrow"></span>Date Closed</th>
				<th><span class="arrow"></span>RO</th> 
				<th><span class="arrow"></span>Line</th> 
				<th><span class="arrow"></span>Flag Hours</th>
			</tr> 
		</thead> 

		<tbody> 
			<tr> 
				<td>Mon Nov. 3, 2013</td>
				<td>16135631</td> 
				<td>2</td>
				<td>2.3</td>
			</tr>
			<tr class="total"> 
				<td class="column">Totals</td> 
				<td></td>
				<td></td>
				<td>100</td>			
			</tr>
		</tbody> 
	</table>
	<h6>Tue Nov. 4, 2013</h6>
	<table class="closed"> 
		<thead> 
			<tr id="headings"> 
				<th><span class="arrow"></span>Date Closed</th>
				<th><span class="arrow"></span>RO</th> 
				<th><span class="arrow"></span>Line</th> 
				<th><span class="arrow"></span>Flag Hours</th>
			</tr> 
		</thead> 

		<tbody> 
			<tr> 
				<td>Mon Nov. 3, 2013</td>
				<td>16135631</td> 
				<td>2</td>
				<td>2.3</td>
			</tr>
			<tr class="total"> 
				<td class="column">Totals</td> 
				<td></td>
				<td></td>
				<td>100</td>			
			</tr>
		</tbody> 
	</table>
  </section><!-- /stats container -->
  
</div><!-- .stuff .important-content -->
<div class="clear"></div>
<div id="push"></div><!-- pushes content down to sicky footer -->
</div><!-- #content-sub -->
<?php
require('../includes/footer.php');
?>
