<!-- Teampay leftnav for managers -->
<nav class="menu-item">
  <a class="mainlink" href="#teampay/splash"><span class="glyphicon glyphicon-tasks"></span>Team Pay<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#teampay/admin">Admin Settings</a></span></li>    
    <li><span class="sublink"><a href="#teampay/rolist">All Pay-period ROs</a></span></li>
    <li><span class="sublink"><a href="#teampay/hpw">How the Pay Scale Works</a></span></li>
  </ul><!--/.submenu -->
</nav><!--/.menu-item -->

<!-- Teampay leftnav for team leads -->
<nav class="menu-item">
  <a class="mainlink" href="#teampay/splash"><span class="glyphicon glyphicon-tasks"></span>Team Pay<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#teampay/admin">Admin Settings</a></span></li>    
    <li><span class="sublink"><a href="#teampay/totals">Store / Team Totals</a></span></li>
    <li><span class="sublink"><a href="#teampay/rolist">Pay-period ROs</a></span></li>
    <li><span class="sublink"><a href="#teampay/hpw">How the Pay Scale Works</a></span></li>
  </ul><!--/.submenu -->
</nav><!--/.menu-item -->

<!-- Teampay leftnav for techs -->
<nav class="menu-item">
  <a class="mainlink" href="#teampay/splash"><span class="glyphicon glyphicon-tasks"></span>Team Pay<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#teampay/admin">Admin Settings</a></span></li>    
    <li><span class="sublink"><a href="#teampay/rolist">All Pay-period ROs</a></span></li>
    <li><span class="sublink"><a href="#teampay/hpw">How the Pay Scale Works</a></span></li>
  </ul><!--/.submenu -->
</nav><!--/.menu-item -->





<!-- OLD code -->
<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/managerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/currentros">Current ROs</a></span></li>    
    <li><span class="sublink"><a href="#sco/agedros">Aged ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/warrantyadminros">Warranty ROs in Cashier Status</a></span></li>
    <li><span class="sublink"><a href="#sco/warrantycalls">Warranty Calls</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->