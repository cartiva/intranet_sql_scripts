<?php
session_start();
require('../includes/header.php'); // include the header
?>

<!-- Content section for the main page only -->
<div id="content-main">
  <!-- Message block for main page -->
  <div class="main-login">
    <section class="blue-box">
    	<?php if ($logged_in) {?>
        <h3>Hey, <?php echo $first_name ?></h3>
        <p>Did you know?  All your important stuff is on your dashboard.</p>
        <a class="button" href="./dashboard.php"><span></span>Go to your dashboard</a>
        <p class="all-done">All done?  <a href="./logout.php"><span>Log out.</span></a>

      <?php } else {?>

        <p class="title">Login to see your dashboard!</p>

        <?php require('./login_form.php');

      }?>
    </section>

    <aside class="beta-message">
      <h5>You must be testing out the new</h5>
      <h3>Rydell Internal Site</h3>
      <p>Things may not always work perfectly.  If you encounter anything odd, contact the <a href="mailto:libby@cartiva.com">web team</a> and we will keep track of the bugs.  Thanks for being testers!</p>
    </aside>
  </div>
  <div class="clear"></div>
  <div id="push"></div><!-- pushes content down to sicky footer -->
</div>

<?php
require('../includes/footer.php');
?>