<?php
session_start();
require('../includes/header.php'); // include the header
?>

<!-- Content section for the main page only -->
<div id="content-main">
  <div class="simple">
    <div class="message">
	    <h4>Get in touch w/<span>us!</span></h4>
	    <h5>We're open to new ideas, suggestions and feedback of any kind!</h5>
	    <p>The new Rydell Intranet site is a work in progress.  So are any job-specific apps you may be testing.  We're open to any feedback you may have on any part of the new web experience.  Please send us an <a href="mailto:libby@cartiva.com">email</a> or stop by the Cartiva office located near the Parts entrance.  </p>
    </div>
    <div class="contact">
      <h2>Email us</h2>

      <?php
      	require('../webservice/utils.php');
      	if (isset($_POST['submit'])) { // check to see if the form was submitted
			$errors = array();
      		// make sure the fields are filled out properly
			if (empty($_POST['from']) || (filter_var($_POST['from'], FILTER_VALIDATE_EMAIL)) == false) {
				$errors[] = "Please enter a valid email address";
			} else {
				$email = $_POST['from'];
			}

			if (empty($_POST['subject'])) {
				$errors[] = "Please enter a subject";
			} else {
				$subject = $_POST['subject'];
			}

			if (empty($_POST['message'])) {
				$errors[] = "Please enter a message";
			} else {
				$message = $_POST['message'];
			}

			// check for errors or send the email
			if (!empty($errors)) { // if there are errors
					show_contact_form($errors); // show the form with errors
			} else {
				send_message($email, $subject, $message);
				echo "<p class='form_success'>Thanks. We'll be in touch with you soon.</p>";
			} 
      	} else { // if the form hasn't been submitted
      		// show the form
      		show_contact_form();
      	}

      ?>

    </div>
  </div>

  <div id="clear"></div>
  <div id="push"></div><!-- pushes content down to sicky footer -->
</div>

<?php
require('../includes/footer.php');
?>
<!--END OF PAGE -->

<?php
function show_contact_form($errors = array()) {
	if (!empty($errors)) {
		show_form_errors($errors);
	}?>

	<form method="POST" action="../main/contact.php">
	    <span>
	    <label class="label" for="from">Your Email:</label>
	    <input class="text" type="text" id="from" name="from"
	    <?php if (!empty($errors)) {
	    	echo "value='" . $_POST['from'] . "'";
	    }?> 
	    	placeholder="Email address" autofocus required />
	    </span>

	    <span>
	    <label class="label" for="subject">Subject:</label>
	    <input class="text" type="text" id="subject" name="subject"
	    <?php if (!empty($errors)) {
	    	echo "value='" . $_POST['subject'] . "'";
	    }?>
	    	placeholder="Subject" required />
	    </span>

	    <span>
	    <label class="label" for="message">Message:</label>
	    <textarea id="message" name="message" rows="18" cols="40" required><?php
	     if (!empty($errors)) {
	    	echo $_POST['message'];
	    }?></textarea>
	    </span>
	    <input name="submit" type="submit" class="submit"  value="Send" />
	</form>

	<?php
}
# This function sends the email contact us page
function send_message($email, $subject, $message) {
  	require_once('Mail.php');
	$from = $email;
	$to = "libby@cartiva.com";
	$subject = $subject;
	$body = $message;
	$host = "mail.cartiva.com";
	$username = "admin@cartiva.com";
	$password = "dilbert";

	$headers = array(
		'From' => $from,
		'To' => $to,
		'Subject' => $subject
		);

	$smtp = Mail::factory(
		'smtp', array('host' => $host,
					  'auth' => true,
					  'username' => $username,
					  'password' => $password)
		);
	$mail = $smtp->send($to, $headers, $body);

	if (PEAR::isError($mail)) {
		throw new Exception($mail->getMessage());
	} else {
		return;
	}
}
?>