<?php
session_start();
require('../includes/header.php'); // include the header
?>

<!-- Content section for the main page only -->
<div id="content-main">
  <div class="simple">
    <section class="gray-box">
	    <h4><em class="error">Incorrect username or password</em></h4>
	    <h5 class="secondary">Please try logging in again</h5>
	    <?php require('./login_form.php');?>
    </section>
  </div>

  <div id="clear"></div>
  <div id="push"></div><!-- pushes content down to sicky footer -->
</div>

<?php
require('../includes/footer.php');
?>