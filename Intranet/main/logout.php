<?php
require('../webservice/db_funcs.php');
session_start();
if (isset($_SESSION['FullName'])) {
	$old_user = $_SESSION['FullName'];
	$username = $_SESSION['Username'];
}

// unset session variables and store result of session_destroy to see if they
// *were* logged in
$_SESSION = array();


$destroyed = session_destroy(); //returns true on success false on failure


if (!empty($old_user)) {
	if ($destroyed) {

    $params = array();
    $params[0] = $username;

    $login_result = call_stored_proc("phpIntranetLogout", $params);

		//if they were logged in and are now logged out
		header('Location: ./homepage.php');
	} else {
		// they were logged in and could not be logged out
		die('Could not log you out.');
	}
} else {
	// if they weren't logged in but came to this page somehow
	die('You were not logged in, and so have not been logged out.');
}

?>