<?php
session_start();
require('../webservice/utils.php');
require_login();
?>


<!-- Content section for the sub-pages -->
<div id="content-sub">
  <div class="sidebar">
    <div id="sidebar-widgets">
    </div>
  </div><!-- .sidebar -->

<div class="stuff">
  <div id="notices">
  </div><!-- #notices -->

  <section class="stats-container">
  <!-- ------------- Tech Splash Page ------------- -->
	<h4>Main Shop Totals</h4>
	<h6>for the pay-period
		<span id="fromDate">November 3 </span> - 
		<span id="thruDate">November 16, 2013 </span>
	</h6>

	<h5 class="section-start">Shop Total</h5>
	<table class="weekly"> 
		<thead> 
			<tr id="headings"> 
				<th class="column">Teams</th> 
				<th>Flag Hours</th> 
				<th>Clock Hours</th>
				<th>Team Prof.</th> 
				<th>Pay Hours</th> 
				<th>Regular Pay</th>
			</tr> 
		</thead> 

		<tbody> 
			<tr> 
				<td class="column">Team [Number/Name]</td> 
				<td>9hrs</td>
				<td>8hrs</td>
				<td>82%</td>
				<td>10hrs</td>
				<td>12hrs</td>
			</tr>
			
			<tr class="total"> 
				<td class="column">Totals</td> 
				<td>201.2hrs</td>
				<td>123hrs</td>
				<td>92%</td>
				<td>123hrs</td>
				<td>89hrs</td>
			</tr>
		</tbody> 
	</table>

	<h5 class="section-start">Team Totals</h5>
	<h6>[Team Number/Name]</h6>
	<table class="weekly"> 
		<thead> 
			<tr id="headings"> 
				<th class="column">Employee</th> 
				<th>Flag Hours</th> 
				<th>Clock Hours</th>
				<th>Team Prof.</th> 
				<th>Pay Hours</th> 
				<th>Regular Pay</th>
			</tr> 
		</thead> 

		<tbody> 
			<tr> 
				<td class="column">[Employee Name]</td> 
				<td>9hrs</td>
				<td>8hrs</td>
				<td>82%</td>
				<td>10hrs</td>
				<td>12hrs</td>
			</tr>
			
			<tr class="total"> 
				<td class="column">[Team Number] Totals</td> 
				<td>201.2hrs</td>
				<td>123hrs</td>
				<td>92%</td>
				<td>123hrs</td>
				<td>89hrs</td>
			</tr>
		</tbody> 
	</table>
  
</div><!-- .stuff .important-content -->
<div class="clear"></div>
<div id="push"></div><!-- pushes content down to sicky footer -->
</div><!-- #content-sub -->
<?php
require('../includes/footer.php');
?>
