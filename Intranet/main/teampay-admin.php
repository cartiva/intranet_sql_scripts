<?php
session_start();
require('../webservice/utils.php');
require_login();
?>


<!-- Content section for the sub-pages -->
<div id="content-sub">
  <div class="sidebar">
    <div id="sidebar-widgets">
    </div>
  </div><!-- .sidebar -->

<div class="stuff">
  <div id="notices">
  </div><!-- #notices -->

  <section class="stats-container admin">
  	<!-- ------------- Tech Splash Page ------------- -->
	<h4>Admin Settings</h4>
  
  <div id="unassigned-techs">
    <h5>Unassigned Techs</h5>	
    <div class="tech-line">
      <h4>Ben Dover</h4>
      <button type="button" class="btn edit">Edit/Assign</button>
    </div>
    <div class="tech-line">
      <h4>Kenny G</h4>
      <button type="button" class="btn edit">Edit/Assign</button>
    </div>
    <div class="tech-line">
      <h4>Buck Naked</h4>
      <button type="button" class="btn edit">Edit/Assign</button>
    </div>
  </div>

  <div id="teams">
    <h5>Teams</h5>
    <div class="box">
      <div class="box-header">
        <h5>Team 1 - Dick Less</h5>
      </div><!--/.box-header-->
      <div class="box-content">
        <div class="tech-line">
          <h4>Ben Dover 
            <span class="iflead">- Lead</span>
          </h4>
          <span class="mem-percent">34%</span>
          <button type="button" class="btn edit edit-sm">
            <span>Edit</span>
          </button>
        </div><!--/.tech-line-->
        
        <div class="grosspool-line">
          <form class="form-inline">
            <label class="gross-pool" for="gPoolTeam1">Gross Pool</label>
            <input type="text" class="form-control" id="gPoolTeam1" 
              placeholder="00.0%" content="12.5%">
            <button type="button" class="btn edit edit-sm">
              <span>Edit</span>
            </button>
          </form>
        </div><!--/.grosspool-line-->
      </div><!--/.box-content-->
      <div class="box-footer">
        <button type="button" class="btn submit">Submit</button>
      </div><!--/.box-footer-->
    </div><!--/.box-->
    <div class="box">
      <div class="box-header">
        <h5>Team 1 - Dick Less</h5>
      </div><!--/.box-header-->
      <div class="box-content">
        <div class="tech-line">
          <h4>Ben Dover 
            <span class="iflead">- Lead</span>
          </h4>
          <span class="mem-percent">34%</span>
          <button type="button" class="btn edit edit-sm">
            <span>Edit</span>
          </button>
        </div><!--/.tech-line-->
        
        <div class="grosspool-line">
          <form class="form-inline">
            <label class="gross-pool" for="gPoolTeam1">Gross Pool</label>
            <input type="text" class="form-control" id="gPoolTeam1" 
              placeholder="00.0%" content="12.5%">
            <button type="button" class="btn edit edit-sm">
              <span>Edit</span>
            </button>
          </form>
        </div><!--/.grosspool-line-->
      </div><!--/.box-content-->
      <div class="box-footer">
        <button type="button" class="btn submit">Submit</button>
      </div><!--/.box-footer-->
    </div><!--/.box-->
  </div><!--/#teams-->

  <div id="single-team">
    <h5>Team Members &amp; Settings</h5>
    <div class="box">
      <div class="box-header">
        <h5>Team 4 - Ron Burgundy</h5>
      </div><!--/.box-header-->
      <div class="box-content">
        <div class="tech-line">
          <h4>Mike Unt 
            <span class="iflead">- Lead</span>
          </h4>
          <span class="mem-percent">14%</span>
          <button type="button" class="btn edit edit-sm">
            <span>Edit</span>
          </button>
        </div><!--/.tech-line-->
        
        <div class="grosspool-line">
          <form class="form-inline">
            <label class="gross-pool" for="gPoolTeam1">Gross Pool</label>
            <input type="text" class="form-control" id="gPoolTeam1" 
              placeholder="00.0%" content="12.5%">
            <button type="button" class="btn edit edit-sm">
              <span>Edit</span>
            </button>
          </form>
        </div><!--/.grosspool-line-->
      </div><!--/.box-content-->
      <div class="box-footer">
        <button type="button" class="btn submit">Submit</button>
      </div><!--/.box-footer-->
    </div><!--/.box-->
  </div><!--/#single-team-->

  </section><!-- /stats container -->
  
</div><!-- .stuff .important-content -->
<div class="clear"></div>
<div id="push"></div><!-- pushes content down to sicky footer -->
</div><!-- #content-sub -->
<?php
require('../includes/footer.php');
?>
