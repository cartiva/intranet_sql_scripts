<?php
session_start();
require('../includes/header.php'); // include the header
?>

<!-- Content section for the main page only -->
<div id="content-main">
  <div class="simple">
    <h4><span>Oops</span> You're a sneaky devil!</h4>
    <h5>This part of the site isn't finished yet.</h5>
    <p>We're working hard to complete the whole Rydell Intranet site in a timely manner. If you have a suggestion, please <a href="mailto:libby@cartiva.com">email</a> us.  Thanks for your patience!</p>
  </div>

  <div id="clear"></div>
  <div id="push"></div><!-- pushes content down to sicky footer -->
</div>

<?php
require('../includes/footer.php');
?>