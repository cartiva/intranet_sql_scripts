<?php
session_start();
require('../webservice/utils.php');
require_login();
?>


<!-- Content section for the sub-pages -->
<div id="content-sub">
  <div class="sidebar">
    <div id="sidebar-widgets">
    </div>
  </div><!-- .sidebar -->

<div class="stuff">
  <div id="notices">
  </div><!-- #notices -->

  <section class="stats-container">
  	<h4>Dashboard</h4>
    <section class="dsh-float">
      <div class="box box-update" id="sum-desc">
        <h5>Dashboard Updates (the new stuff)</h5>
        <ul>
          <li><strong>Warranty Admins</strong> - PDQ ROs now available for viewing</li>
        </ul>
        <p>As you may have noticed, we've been doing lots of updating.  Every time we do, we'll list the newest changes here.  We want to keep you in the loop.  Keep you happy.  As always, if you see anything weird, let us know @ ext. 5979.</p>
      </div>
      <img src="../images/dashboard-soon6.png" alt="Dashboard items coming soon. Hold your pants on." style="padding:10px; border:1px solid #CCC;background:#FFF;">
      
    </section>

  </section><!-- /stats container -->
  
</div><!-- .stuff .important-content -->
<div class="clear"></div>
<div id="push"></div><!-- pushes content down to sicky footer -->
</div><!-- #content-sub -->
<script>
  $(function () { car.initModule( $('.stats-container') ); });
</script>
<?php
require('../includes/footer.php');
?>