<?php
session_start();
require('../webservice/utils.php');
require_login();
?>

	<!-- Content section for the fullscreen pages -->
	  <div id="content-fullscreen">

	  	<?php include('../detail/detaillist.php'); ?>

		<div class="clear"></div>
		<div id="push"></div><!-- pushes content down to sicky footer -->
	  </div><!-- #content-fullscreen -->

	</div><!-- #body-wrap -->

</body>
</html>
<!-- Login dropdown -->
	<script>
		$(function() {
		  $('.dropclick').on('click', function(e) {
		    e.stopPropagation();
		    $('.dditems').slideToggle('fast');
		  });

		  $(document).on('click', function(e) {
		    if( $(e.target).hasClass('dropdown') || $(e.target).hasClass('other-stuff') || $(e.target).attr('class') === 'dditems'){
		        
		    }
		    else{$('.dditems').hide();}
		  });
		});
	</script>
<!-- END Login dropdown -->