<?php
session_start();
require('../includes/header.php'); // include the header
?>

<!-- Content section for the main page only -->
<div id="content-main">
  <div class="simple">
    <h4><span>Sorry</span> That page doesn't exist</h4>
    <h5>Visit the <a href="homepage.php">home</a>  page instead.</h5>
    
  </div>

  <div id="clear"></div>
  <div id="push"></div><!-- pushes content down to sicky footer -->
</div>

<?php
require('../includes/footer.php');
?>