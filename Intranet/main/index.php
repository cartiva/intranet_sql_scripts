<?php
/*
 * This is the main index file which will route all of the page requests.
 *
 */

if (!empty($_SERVER['PATH_INFO'])) { // check for path elements
	$path = $_SERVER['PATH_INFO'];
	$path = rtrim($path, "/");
	$path_params = explode("/", $path);
	if (count($path_params) == 1) {
		array_pop($path_params);
		require('./homepage.php');
	}
} else {
	require('./homepage.php');
}

// check the http method
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

	if (!empty($path_params)) {
		switch($path_params[1]) {
			// test
			case 'dashboard':
				require('./dashboard.php');
			break;

			default:
				die404();
		}
	}
} else {
	//something
}

function die404() {
	header("HTTP/1.1 404 Not Found");
	echo "<h1>file not found</h1>";
}

?>