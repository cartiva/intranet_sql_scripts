<?php
session_start();
require('../webservice/utils.php');
require_login();
?>


<!-- Content section for the sub-pages -->
<div id="content-sub">
  <div class="sidebar">
    <div id="sidebar-widgets">
    </div>
  </div><!-- .sidebar -->

<div class="stuff">
  <div id="notices">
  </div><!-- #notices -->

  <section class="stats-container">
  	<!-- ------------- Tech Splash Page ------------- -->
  	    <h4>"What if" Pay Calculator</h4>
        <p>Use this calculator to evaluate how your Regular Pay can change
          based on your teams Flag Hours and Clock Hours by changing those
          values.  You can also see how Team Proficiency changes along
          with your pay in the team environment.</p>
        <p><strong>Directions:</strong> Change the values in the Team Flag
          Hrs and Team Clock Hrs input boxes, then click calculate.</p>
        <section class="calc calc-wide">
          <div class="row captions">
            <span class="calc-3">Team Flag Hrs</span> <b>/</b>
            <span class="calc-3">Team Clock Hrs</span> <b>=</b>
            <span class="calc-3">Team Proficiency</span> 
          </div>
          <div class="row">
            <span class="calc-3">
              <input type="text" class="form-control" name="TeamFlagHours" 
                placeholder="Team Flag Hours" value="TEAMFLAGHOURS">
              </input>
            </span> / 
            <span class="calc-3">
              <input type="text" class="form-control" name="TeamClockHours" 
                placeholder="Team Clock Hours" value="TEAMCLOCKHOURS">
              </input>
            </span> = 
            <span class="calc-3">TEAMPROFICIENCY</span> 
          </div>
          <hr>
          <div class="row captions">
            <span class="calc-4">Team Proficiency</span> <b>x</b> 
            <span class="calc-4">Your Clock Hrs</span> <b>=</b> 
            <span class="calc-4">Your Pay Hrs</span> <b>x</b> 
            <span class="calc-4">Your Pay Rate</span> <b>=</b>
          </div>
          <div class="row">
            <span class="calc-4"><em>TEAMPROFICIENCY</em></span> x 
            <span class="calc-4">
              <input type="text" class="form-control" name="YourClockHours" 
                placeholder="Your Clock Hours" value="YOURCLOCKHOURS">
              </input>
            </span> = 
            <span class="calc-4"><em>YOURPAYHOURS</em></span> x 
            <span class="calc-4">YOURPAYRATE</span> =
          </div>
          <div class="reg-pay">
            <div class="row captions">
              <span class="calc-1 total">Your Regular Pay</span>
            </div>
            <div class="row">
              <span class="calc-1 total"><em>YOURREGULARPAY</em></span>
            </div>
          </div><!--/.reg-pay-->          
          <button id="calcbutton" class="btn btn-lg btn-side">Calculate Pay</button>
        </section><!--/.calc-->

	  </section><!-- /stats container -->
  
</div><!-- .stuff .important-content -->
<div class="clear"></div>
<div id="push"></div><!-- pushes content down to sicky footer -->
</div><!-- #content-sub -->
<?php
require('../includes/footer.php');
?>
