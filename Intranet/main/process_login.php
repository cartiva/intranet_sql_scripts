<?php
require('../webservice/db_funcs.php');
session_start();

if (!function_exists('getallheaders')) 
{ 
    function getallheaders() 
    { 
           $headers = ''; 
       foreach ($_SERVER as $name => $value) 
       { 
           if (substr($name, 0, 5) == 'HTTP_') 
           { 
               $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
           } 
       } 
       return $headers; 
    } 
}


$params = array();
$params[0] = $_POST['username'];
$params[1] = $_POST['password'];

$login_result = call_stored_proc("ValidateLogin", $params);
//var_dump($login_result);

if (!empty($login_result) && $login_result[0]['MemberGroup'] !== "Denied") {

	$first_name = $login_result[0]['FirstName'];
	$last_name = $login_result[0]['LastName'];
	$member_group = $login_result[0]['MemberGroup'];
	$store_code = $login_result[0]['StoreCode'];

	$_SESSION['LoggedIn'] = TRUE;
	$_SESSION['Username'] = $_POST['username'];
	$_SESSION['FirstName'] = $first_name;
	$_SESSION['LastName'] = $last_name;
	$_SESSION['FullName'] = $first_name . ' ' . $last_name;
	$_SESSION['MemberGroup'] = $member_group;
	$_SESSION['StoreCode'] = $store_code;

  // Bridge to new sesssion management
  // Grap the PHPSESSID that PHP is setting for a session
	$headers_so_far = getallheaders();
  preg_match('/PHPSESSID=(.*)/is', $headers_so_far["Cookie"], $m);
  $phpsessionid = $m[1];

  $myparams = array();
  $myparams['username'] = $_POST['username'];
  $myparams['password'] = $_POST['password'];
  $myparams['sessionid'] = $phpsessionid;
  $sql = call_stored_proc("phpIntranetLogin", $myparams);

  // Call new session management URI and pass it the phpsessionID
	// $domain = $_SERVER['HTTP_HOST'];
 //  $myparams = array();
 //  $myparams['username'] = $_POST['username'];
 //  $myparams['password'] = $_POST['password'];
 //  $myparams['sessionid'] = $phpsessionid;
	// $ch = curl_init();
 //  curl_setopt($ch, CURLOPT_URL, "http://".$domain."/intranet/php/session.php/login");
 //  curl_setopt($ch, CURLOPT_POST, 1);
 //  curl_setopt($ch, CURLOPT_POSTFIELDS, $myparams);
 //  $response = curl_exec($ch);
 //  curl_close($ch);

	header('location: ./dashboard.php');

} else {
  	header('location: ./failed_login.php');
}

?>