<?php
session_start();
require('../webservice/utils.php');
require_login();
?>


<!-- Content section for the sub-pages -->
<div id="content-sub">
  <div class="sidebar">
    <div id="sidebar-widgets">
    </div>
  </div><!-- .sidebar -->

<div class="stuff">
  <div id="notices">
  </div><!-- #notices -->

  <section class="stats-container">
  	<!-- ------------- Tech Splash Page ------------- -->
  	<h4>How Flat-Rate Team Pay Works</h4>
    <h5>Terminology</h5>
  	<dl>
      <dt>Flag Hours</dt>
        <dd>Total number of labor hours billed</dd>
      <dt>Clock Hours</dt>
        <dd>Total number of hours clocked in</dd>
      <dt>Pay Hours</dt>
        <dd>Clock Hours times Team Proficiency</dd>
      <dt>Team Proficiency</dt>
        <dd>Total Team Flag Hours divided by Total Team clock Hours</dd>
      <dt>Regular Pay</dt>
        <dd>Your Pay Hours times your Current Pay Rate</dd>
      <dt>ELR (Effective Labor Rate)</dt>
        <dd>Some shit goes here that I don't know yet</dd>
      <dt>Budget</dt>
        <dd>Total dollars per pay hour the Manager/Team Lead have to divide
          amongst the team members</dd>
    </dl>
    <h5>Calculating Budget</h5>
    <p>Pay is based on the shops 6-month rolling <em>Effective Labor Rate</em> 
    or ELR.  The ELR is calculated by taking the total Labor Sales in 
    the last 6 months and dividing it by the total number of Flag 
    Hours in the same time frame. In the team pay system, the team 
    is paid 20% of the ELR for each hour the team produces. </p>
    
    <p>The Service Manager and team leader decide how this will be split 
    up amongst the team members. The percentage of split you receive 
    is what determines your <em>Current Pay Rate</em>.</p>
    <section class="calc">
      <h5>Example Team Budget</h5>
      <div class="captions">
        <span class="calc-4">ELR</span> <b>x</b>
        <span class="calc-4">Tech %</span> <b>x</b>
        <span class="calc-4">Number of techs on team</span> <b>=</b>
        <span class="calc-4">Budget</span>
      </div>
      <div>
        <span class="calc-4"><sup>$</sup>88.58</span> x 
        <span class="calc-4">20<sup>%</sup></span> x 
        <span class="calc-4">4</span> = 
        <span class="calc-4"><sup>$</sup>70.86</span>
      </div>
    </section><!--/.calc-->
    <h5>Team Budgeting</h5>
    <p>The budget is the total dollars per pay hour the Team 
      Leader and Service Manager have to divide amongst the team members.</p>	
    <p>The Team Leader and Service Manager will assign a percentage of the 
       budget to each team member. The percentage will be determined
      by the tech's skill set, tech's capacity, and overall proficiency.</p>
    <section class="calc calc-wide">
      <h5>Example Team Breakdown</h5>
      <div class="captions">
        <span class="calc-4">Team Member</span>
        <span class="calc-4">Budget</span> <b>x</b>
        <span class="calc-4">Team member % of budget</span> <b>=</b>
        <span class="calc-4">Current pay rate</span>
      </div>
      <div>
        <span class="calc-4">1 - 31%</span> 
        <span class="calc-4"><sup>$</sup>70.86</span> x 
        <span class="calc-4">31<sup>%</sup></span> = 
        <span class="calc-4"><sup>$</sup>21.97</span>
      </div>
      <div>
        <span class="calc-4">2 - 28%</span> 
        <span class="calc-4"><sup>$</sup>70.86</span> x 
        <span class="calc-4">28<sup>%</sup></span> = 
        <span class="calc-4"><sup>$</sup>19.84</span>
      </div>
      <div>
        <span class="calc-4">3 - 23%</span> 
        <span class="calc-4"><sup>$</sup>70.86</span> x 
        <span class="calc-4">23<sup>%</sup></span> = 
        <span class="calc-4"><sup>$</sup>16.30</span>
      </div>
      <div>
        <span class="calc-4">4 - 18%</span> 
        <span class="calc-4"><sup>$</sup>70.86</span> x 
        <span class="calc-4">18<sup>%</sup></span> = 
        <span class="calc-4"><sup>$</sup>12.75</span>
      </div>
    </section><!--/.calc-->
    <section class="calc calc-wide">
      <h5>Example Pay - Team Member 1</h5>
      <div class="captions">
        <span class="calc-3">Current Pay Rate</span> <b>x</b>
        <span class="calc-3">Pay Hours for Pay Period</span> <b>=</b>
        <span class="calc-3">Regular Pay</span>
      </div>
      <div>
        <span class="calc-3"><sup>$</sup>21.97</span> x 
        <span class="calc-3">100</span> = 
        <span class="calc-3"><sup>$</sup>2,197.00</span>
      </div>
    </section><!--/.calc-->
	
  </section><!-- /stats container -->
  
</div><!-- .stuff .important-content -->
<div class="clear"></div>
<div id="push"></div><!-- pushes content down to sicky footer -->
</div><!-- #content-sub -->
<?php
require('../includes/footer.php');
?>
