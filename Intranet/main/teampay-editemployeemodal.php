
<?php
session_start();
require('../webservice/utils.php');
require_login();
?>


<!-- Content section for the sub-pages -->
<div id="content-sub">
  <div class="sidebar">
    <div id="sidebar-widgets">
    </div>
  </div><!-- .sidebar -->

<div class="stuff">
  <div id="notices">
  </div><!-- #notices -->

  <section class="stats-container">
  	<!-- ------------- Tech Splash Page ------------- -->
	<h4>"What if" Pay Calculator</h4>
	<p>--We'll put a calculation with an image or markup here--</p>
	
	
  </section><!-- /stats container -->
  
</div><!-- .stuff .important-content -->
<div class="clear"></div>
<div id="push"></div><!-- pushes content down to sicky footer -->
</div><!-- #content-sub -->

</div><!-- #wrap -->
<footer>
  <aside>
   <div class="search" id="search">
    <form method="post" action="/index.php"  >
      <div class='hiddenFields'>
        <input type="hidden" name="ACT" value="6" />
        <input type="hidden" name="XID" value="00a892aa75f19ed15c5daa00db13c01bf97e6f16" />
        <input type="hidden" name="RES" value="" />
        <input type="hidden" name="meta" value="G/N8Tbk+v4LdchLOjtAeYl2Ii9u3QNWAMF04CDorkP7Wzi8tRRXMnRXQxeT+1TZB3YLAd0vrk+oEVK30QZyG0ofCuUvnte8LXTga7HOc0JPQTtEnTLP89vcMcRdDzrIIiGJtTJ2Ci4T/ObSX3ZutJgJ2OJ5jbPXnD6+BmiG2R0/syXhmzA0gAibtkOKnLPJafYk8yMY5x0Nbt3sZD9iT//iZqZzWgRAuVv4JMCkyRGtG8nqRTAQF3mb3Lli8WTK+Gq7gy4pYQhTgz0TACuHKqAhMjhAIUYM9uguq7q1H/3DD15xhv7eCnl3m1AriduBghZ1tNbJBsIkQJJui9yWmyA==" />
        <input type="hidden" name="site_id" value="1" />
      </div>


      <!-- This will contain the search bar for the footer -->
      <h4><span>Can't find what you're looking for?</span></h4>
      <input type="text" name="keywords" class="keywords" placeholder="Search the Rydell Intranet" size="10" maxlength="100" />
      <!--<a href="search">Advanced Search</a> -->
      <input type="submit" value="" class="submit" />
      <!-- END Search form -->
    </form>
  </div>
</aside>
<div id="epic">
  <section>
    <h5>Main Sections</h5>
    <ul>
      <!--<li><a href="/index.php" class="selected"><span>Home</span></a>-->
      <li><a href="coming-soon.php"><span>Resources</span></a></li>
      <li><a href="coming-soon.php"><span>Jobs</span></a></li>
      <li><a href="coming-soon.php"><span>Calendar</span></a></li>
      <li><a href="coming-soon.php"><span>IT Info</span></a></li>
      <li><a href="coming-soon.php"><span>Employees</span></a></li>
      <!-- <li><a href="coming-soon.php"><span>Departments</span></a></li> -->
    </ul>
  </section>
  <section>
    <h5>Sitemap</h5>
    <ul>
      <li><a href="coming-soon.php"><span>Help</span></a></li>
      <li><a href="contact.php"><span>Contact Us</span></a></li>
      <li><a href="http://www.facebook.com/Rydellcars" target="_blank"><span>Facebook Page</span></a></li>
      <li><a href="http://www.rydellcars.com/" target="_blank"><span>Rydell Website</span></a></li>
    </ul>
  </section>
  <section>
    <h5>Outside Links</h5>
    <ul>
      <li><a href="http://mail.rydellchev.com:8383/" target="_blank"><span>Rydell Email</span></a></li>
      <li><a href="http://mail.gfhonda.com:8383/" target="_blank"><span>Honda Email</span></a></li>
      <li><a href="http://www.gmnacsi.com/gmcsi/sec_login.asp" target="_blank"><span>Dealer Pulse</span></a></li>
      <li><a href="http://www.gmglobalconnect.com/" target="_blank"><span>GM Global Connect</span></a></li>
      <li><a href="http://www.rydellcommunity.ning.com/" target="_blank"><span>Rydell Community</span></a></li>
    </ul>
  </section>
  <section>
    <h5>More Outside Links</h5>
    <ul>
      <li><a href="https://www.dealertrack.com/public/login.fcc?TYPE=33554432&REALMOID=06-fd77313c-8b24-11d4-aa8c-000629858070&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=ZmQrWp0ZwhTU3n4KbxTUP1hkSt1K7W8YrKMLsXQtdaPSTqkxEWfAVJT7jlXK4K37&TARGET=-SM-%2f" target="_blank"><span>Dealertrack</span></a></li>
      <li><a href="https://www.autopartners.net/gmentsso/UI/Login?goto=https%3A%2F%2Fwww.autopartners.net%3A443%2Fapps%2Fsporef%2Fsi.html" target="_blank"><span>GM Autoparts</span></a></li>
      <li><a href="https://www.gmtraining.com/HomePage/LoginPage.asp" target="_blank"><span>GM Training</span></a></li>
      <li><a href="http://new.fosterstraining.com/" target="_blank"><span>Fosters Training</span></a></li>
    </ul>
  </section>
</div>
<!-- Login dropdown -->
<script>
$(function() {
  $('.dropclick').on('click', function(e) {
    e.stopPropagation();
    $('.dditems').slideToggle('fast');
  });

  $(document).on('click', function(e) {
    if( $(e.target).hasClass('dropdown') || $(e.target).hasClass('other-stuff') || $(e.target).attr('class') === 'dditems'){
        
    }
    else{$('.dditems').hide();}
  });
});
</script>
<!-- END Login dropdown --></footer>

<div id="leoverlay" style></div>
<div id="lemodal" style="width:auto;height:auto;top:0;left:0;">
<div id="lecontent">
  <span type="button" class="x-close close-can cancelbutton">
  </span>
  <h3>
    Update the status of this Current RO
  </h3>f
  <h4>
    RO Detail
  </h4>
  <div id="rodetail">
    <table>
      
      <tbody>
        <tr>
          
          <th>
            Name
          </th>
          
          <th>
            Contact
          </th>
          
          <th>
            Vehicle
          </th>
          
          <th>
            RO Info
          </th>
          
        </tr>
        
        <tr>
          
          <td class="customer">
            MOEN, VICKI NADINE
          </td>
          
          <td class="contact shortdetails">
			
            <div>
              
              <p>
                Home: 7017406233
              </p>
              
              <p>
                Work: No Phone
              </p>
              
              <p>
                Cell: 7017406233
              </p>
              
              <p>
                Email: dnh@aul.com
              </p>
              
            </div>
            
          </td>
          
          <td class="vehicle">
            BUICK RAINIER CXL AWD
          </td>
          
          <td class="ro shortdetails">
            
            <div>
              
              <p>
                RO: 
                <span id="currentro">
                  16135627
                </span>
              </p>
              
              <p>
                Status: Closed
              </p>
              
              <p>
                O/C: Wed 11-06 / Mon 11-11
              </p>
              
              <p>
                Writer: Rodney Troftgruben
              </p>
              
            </div>
            
          </td>
          
        </tr>
      </tbody>
    </table>
    <table class="roshit">
      
      <tbody>
        <tr>
          
          <th>
            Line
          </th>
          
          <th class="paytype">
            Pay Type
          </th>
          
          <th>
            Status
          </th>
          
          <th>
            Descr.
          </th>
          
        </tr>
        
        <tr>
          
          <td>
            1
          </td>
          
          <td class="paytype">
            C
          </td>
          
          <td>
            Closed
          </td>
          
          <td class="longdetails">
            
            <div>
              
              <p>
                Op Code: MAINT: CUSTOMER STATES
              </p>
              
              <p>
                Complaint: DRIVERS SIDE REAR TIRE HAS A LEAK(BEAD ON RIM IS BROKEN-MAY NEED TO ORDER WHEEL)
              </p>
              
              <p>
                Cause: N/A
              </p>
              
              <p>
                Cor Code: 52: SEE NOTE BELOW
              </p>
              
              <p>
                Correction: FOUND BEAD AROUND BUSTED RIM LEAKING NEED TO ORDER WHEEL. DIA-0.5
              </p>
              
            </div>
            
          </td>
          
        </tr>
        
        <tr>
          
          <td>
            2
          </td>
          
          <td class="paytype">
            C
          </td>
          
          <td>
            Closed
          </td>
          
          <td class="longdetails">
            
            <div>
              
              <p>
                Op Code: MAINT: CUSTOMER STATES
              </p>
              
              <p>
                Complaint: PUT SPARE ON DRIVERS REAR WHILE WE ORDER A WHEEL IF WE NEED TO
              </p>
              
              <p>
                Cause: N/A
              </p>
              
              <p>
                Cor Code: DIA: DIAGNOSTIC TIME
              </p>
              
              <p>
                Correction: SPARE WOULD NOT RELEASE AS DESIGNED. HAD TO RAISE VEHICLE AND MANUALLY REMOVE SPARE. FOUND SPARE CAB LE TO BE FRAYED, AND MECHANISM TO BE CRACKED. NEED TO REPLACE SPARE RETRACTING/HOLDING PARTS. DIA-1.0
              </p>
              
            </div>
            
          </td>
          
        </tr>
        
        <tr>
          
          <td>
            3
          </td>
          
          <td class="paytype">
            I
          </td>
          
          <td>
            Closed
          </td>
          
          <td class="longdetails">
            
            <div>
              
              <p>
                Op Code: MAINT: CUSTOMER STATES
              </p>
              
              <p>
                Complaint:  DRIVERS FRONT CORNER MARKER LAMP BULB OUT
              </p>
              
              <p>
                Cause: N/A
              </p>
              
              <p>
                Cor Code: BULBN: NICE CARE - REPLACE BULB
              </p>
              
              <p>
                Correction:  BULB-0.4
              </p>
              
            </div>
            
          </td>
          
        </tr>
        
        <tr>
          
          <td>
            4
          </td>
          
          <td class="paytype">
            C
          </td>
          
          <td>
            Closed
          </td>
          
          <td class="longdetails">
            
            <div>
              
              <p>
                Op Code: 23I: MULTIPOINT INSPECTION
              </p>
              
              <p>
                Complaint: N/A
              </p>
              
              <p>
                Cause: N/A
              </p>
              
              <p>
                Cor Code: 23I: MULTIPOINT INSPECTION
              </p>
              
              <p>
                Correction:  23I-0.5
              </p>
              
            </div>
            
          </td>
          
        </tr>
        
        <tr>
          
          <td>
            5
          </td>
          
          <td class="paytype">
            C
          </td>
          
          <td>
            Closed
          </td>
          
          <td class="longdetails">
            
            <div>
              
              <p>
                Op Code: MAINT: CUSTOMER STATES
              </p>
              
              <p>
                Complaint:  INSTALL SOP WHEEL.
              </p>
              
              <p>
                Cause: N/A
              </p>
              
              <p>
                Cor Code: 48M: MOUNT AND BALANCE
              </p>
              
              <p>
                Correction: INSTALLED NEW RIM FOR LR TIRE 48M 0.4
              </p>
              
            </div>
            
          </td>
          
        </tr>
        
        <tr>
          
          <td>
            6
          </td>
          
          <td class="paytype">
            C
          </td>
          
          <td>
            Closed
          </td>
          
          <td class="longdetails">
            
            <div>
              
              <p>
                Op Code: MAINT: CUSTOMER STATES
              </p>
              
              <p>
                Complaint:  INSTALL SOP TIRE HOIST
              </p>
              
              <p>
                Cause: N/A
              </p>
              
              <p>
                Cor Code: 8060860: SPARE WHEEL HOIST REPLACEMENT
              </p>
              
              <p>
                Correction: INSTALLED NEW REAR TIRE HOIST AND INSTALLED REAR T IRE AS WELL. 8060860 1.5
              </p>
              
            </div>
            
          </td>
          
        </tr>
        
        <tr>
          
          <td colspan="4">
            Please Note: N/A
          </td>
          
        </tr>
      </tbody>
    </table>
  </div>
  <h5>
    Update Status
  </h5>
  
  <select id="select">
    
    <option value="">
      -- Select a status --
    </option>
    
    <option value="Warranty admin has RO.">
      Warranty admin has RO.
    </option>
    
    <option value="Special order parts.">
      Special order parts.
    </option>
    
    <option value="Parts hold.">
      Parts hold.
    </option>
    
    <option value="Authorization hold.">
      Authorization hold.
    </option>
	
  </select>
  <textarea rows="7" id="current_status_update" name="status_update" type="" required="" class="entry" placeholder="Enter the new status here.">
  </textarea>
  <br>
  <button class="submit" id="submitcurrentro">
    Submit
  </button>
  <button type="button" class="close-can cancelbutton">
    Cancel
  </button>
  <button id="printBtn" type="button" class="print">
    Print
  </button>
  <div id="ro-list">
    <h4>
      Status Updates
    </h4>
    <table>
    </table>
  </div>
</div>
</div>




</body>
</html>


