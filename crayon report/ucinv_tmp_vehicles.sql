/*
SELECT c.value, a.stocknumber, CAST(a.fromts AS sql_date) AS from_date, 
  CAST(a.thruts AS sql_date) AS thru_date, b.vin, b.bodystyle, 
  b.TRIM, b.interiorcolor, b.exteriorcolor, b.engine, b.transmission, b.make, 
  b.model, b.yearmodel AS model_year, 
  c.value
FROM dps.VehicleInventoryItems a
INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID 
LEFT JOIN (
  SELECT a.stocknumber, a.fromts, a.thruts, b.vin, c.value, c.value, c.VehicleItemMileagets
  FROM dps.VehicleInventoryItems a
  INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID
  LEFT JOIN dps.VehicleItemMileages c on b.VehicleItemID = c.VehicleItemID 
  --  AND c.VehicleItemMileagets BETWEEN a.fromts AND a.thruts
  WHERE c.VehicleItemMileagets = (
    SELECT MAX(VehicleItemMileagets)
    FROM dps.VehicleItemMileages
    WHERE VehicleItemID = c.VehicleItemID 
      AND cast(VehicleItemMileagets AS sql_date) BETWEEN cast(a.fromts AS sql_date) AND coalesce(cast(a.thruts AS sql_date), CAST('12/31/9999' AS sql_date)))) c on b.vin = c.vin --a.stocknumber = c.stocknumber
WHERE CAST(a.fromts AS sql_date) > '10/01/2015'


NOT BETWEEN but MAX ts less than thru date
fucking mileage recorded AS part of an evaluation will have a ts prior to the
fromts of the VehicleInventoryItem
bad fucking model

DROP TABLE #wtf;
SELECT a.stocknumber, CAST(a.fromts AS sql_date) AS from_date, 
  CAST(a.thruts AS sql_date) AS thru_date, b.vin, left(b.bodystyle, 30),
  left(b.TRIM , 40), left(b.interiorcolor, 30), left(b.exteriorcolor, 30),
  left(b.engine, 30), left(b.transmission, 10), left(b.make, 20), 
  left(b.model, 30), b.yearmodel AS model_year, 
  coalesce(c.value, 0) AS miles, g.shape, g.size, g.shapeandsize
--SELECT *  
INTO #wtf  
FROM dps.VehicleInventoryItems a
INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID 
LEFT JOIN (
  SELECT a.stocknumber, a.fromts, a.thruts, b.vin, c.value, c.value, c.VehicleItemMileagets
  FROM dps.VehicleInventoryItems a
  INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID
  LEFT JOIN dps.VehicleItemMileages c on b.VehicleItemID = c.VehicleItemID 
  --  AND c.VehicleItemMileagets BETWEEN a.fromts AND a.thruts
  WHERE c.VehicleItemMileagets = (
    SELECT MAX(VehicleItemMileagets)
    FROM dps.VehicleItemMileages
    WHERE VehicleItemID = c.VehicleItemID 
      AND cast(VehicleItemMileagets AS sql_date) BETWEEN CAST(a.fromts AS sql_date) AND  coalesce(cast(a.thruts AS sql_date), CAST('12/31/9999' AS sql_date)))) c on a.stocknumber = c.stocknumber      
LEFT JOIN dps.MakeModelClassifications d on b.make = d.make
  AND b.model = d.model  
LEFT JOIN dps.typDescriptions e on d.vehicleSegment = e.typ
LEFT JOIN dps.typDescriptions f on d.vehicleType = f.typ 
LEFT JOIN ucinv_shapes_and_sizes g on upper(f.description) = upper(g.shape) collate ads_default_ci 
  AND upper(e.description) = upper(g.size) collate ads_default_ci    

SELECT MAX(length(bodystyle)) AS bodystyle, MAX(length(trim)) AS trim,MAX(length(interiorcolor)) AS interiorcolor,
MAX(length(exteriorcolor)) as exteriorcolor,MAX(length(engine)) AS engine,MAX(length(transmission)) AS transmission,
MAX(length(make)) AS make,MAX(length(model)) AS model,MAX(length(shape)) AS shape,MAX(length(size)) AS size,MAX(length(shapeandsize)) AS shapeandsize
FROM #wtf
*/
DROP TABLE ucinv_tmp_vehicles; 
CREATE TABLE ucinv_tmp_vehicles (
  stocknumber cichar(20) constraint NOT NULL,
  from_date date constraint NOT NULL,
  thru_date date constraint NOT NULL,
  vin cichar(24) constraint NOT NULL,
  body_style cichar(30),
  trim_level cichar(40),
  interior_color cichar(30),
  exterior_color cichar(30),
  engine cichar(30),
  transmission cichar(10),
  make cichar(20) constraint NOT NULL,
  model cichar(30) constraint NOT NULL,
  model_year cichar(4) constraint NOT NULL,
  miles integer default '0' constraint NOT NULL,
  shape cichar(10),
  size cichar(10),
  shape_and_size cichar(20),
  cab cichar(12),
  drive cichar(3),
  constraint pk primary key (stocknumber)) IN database;
  
INSERT INTO ucinv_tmp_vehicles
SELECT a.stocknumber, CAST(a.fromts AS sql_date) AS from_date, 
  cast(coalesce(a.thruts, cast('12/31/9999 01:00:00' as sql_timestamp)) AS sql_date) AS thru_date, b.vin, left(b.bodystyle, 30),
  left(b.TRIM , 40), left(b.interiorcolor, 30), left(b.exteriorcolor, 30),
  left(b.engine, 30), left(b.transmission, 10), left(b.make, 20), 
  left(b.model, 30), b.yearmodel AS model_year, 
  coalesce(c.value, 0) AS miles, left(g.shape, 10), left(g.size, 10),
  left(g.shapeandsize, 20),
  CASE
    WHEN BodyStyle LIKE  '%King%' 
      OR BodyStyle LIKE '%Access%' 
      OR BodyStyle LIKE '%Ext Cab%' 
      OR BodyStyle LIKE '%Supercab%' 
      OR BodyStyle LIKE '%Club%' 
      OR BodyStyle LIKE '%xtra%' 
      OR BodyStyle LIKE '%Double%' THEN 'Extended Cab'
    WHEN BodyStyle LIKE '%Reg%' THEN 'Regular Cab'
    WHEN BodyStyle LIKE '%crew%'
      OR BodyStyle LIKE '%quad%'
      OR BodyStyle LIKE 'mega%' THEN 'Crew Cab'
  END AS Cab,
  CASE 
    WHEN BodyStyle LIKE '%AWD%' THEN 'AWD'
    WHEN BodyStyle LIKE '%FWD%' THEN 'FWD'
    WHEN BodyStyle LIKE '%4WD%' OR BodyStyle LIKE '%4X4%' THEN '4WD'
  END AS Drive    
FROM dps.VehicleInventoryItems a
INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID 
LEFT JOIN (
  SELECT a.stocknumber, a.fromts, a.thruts, b.vin, c.value, c.value, c.VehicleItemMileagets
  FROM dps.VehicleInventoryItems a
  INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID
  LEFT JOIN dps.VehicleItemMileages c on b.VehicleItemID = c.VehicleItemID 
  --  AND c.VehicleItemMileagets BETWEEN a.fromts AND a.thruts
  WHERE c.VehicleItemMileagets = (
    SELECT MAX(VehicleItemMileagets)
    FROM dps.VehicleItemMileages
    WHERE VehicleItemID = c.VehicleItemID 
      AND cast(VehicleItemMileagets AS sql_date) BETWEEN CAST(a.fromts AS sql_date) AND  coalesce(cast(a.thruts AS sql_date), CAST('12/31/9999' AS sql_date)))) c on a.stocknumber = c.stocknumber      
LEFT JOIN dps.MakeModelClassifications d on b.make = d.make
  AND b.model = d.model  
LEFT JOIN dps.typDescriptions e on d.vehicleSegment = e.typ
LEFT JOIN dps.typDescriptions f on d.vehicleType = f.typ 
LEFT JOIN ucinv_shapes_and_sizes g on upper(f.description) = upper(g.shape) collate ads_default_ci 
  AND upper(e.description) = upper(g.size) collate ads_default_ci
WHERE a.stocknumber IS NOT NULL; -- 10/12/15 unresolved intramarket wholesales


  

