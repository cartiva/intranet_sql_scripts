SELECT a.vehicleshape, a.vehiclesize, b.priceband, COUNT(*)
FROM ucinv_available a
LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
  AND b.thedate = curdate() - 1
WHERE a.storecode = 'ry1'
  AND a.thruts > now()
GROUP BY a.vehicleshape, a.vehiclesize, b.priceband
ORDER BY COUNT(*) DESC   

SELECT *
FROM ucinv_sales_previous_30
WHERE thedate = curdate()

SELECT g.*, h.units AS prev_30, i.units AS prev_60

-- previous 30 retail IS way high
SELECT g.vehicleshape, g.vehiclesize, g.priceband, g.avail,
  coalesce(sum(CASE WHEN h.saletype = 'retail' THEN h.units END), 0) AS prev_30_retail,
  coalesce(sum(CASE WHEN h.saletype = 'wholesale' THEN h.units END), 0) AS pre_30_ws,
  coalesce(sum(CASE WHEN i.saletype = 'retail' THEN i.units END), 0) AS prev_60_retail,
  coalesce(sum(CASE WHEN i.saletype = 'wholesale' THEN i.units END), 0) AS pre_60_ws,  
  coalesce(sum(CASE WHEN j.saletype = 'retail' THEN j.units END), 0) AS prev_90_retail,
  coalesce(sum(CASE WHEN j.saletype = 'wholesale' THEN j.units END), 0) AS pre_90_ws  
-- SELECT *
FROM (
  SELECT a.vehicleshape, a.vehiclesize, b.priceband, COUNT(*) avail
  FROM ucinv_available a
  LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
    AND b.thedate = curdate() - 1
  WHERE a.storecode = 'ry1'
    AND a.thruts > now()
  GROUP BY a.vehicleshape, a.vehiclesize, b.priceband) g
LEFT JOIN (
  SELECT *
  FROM ucinv_sales_previous_30
  WHERE thedate = curdate()
    AND storecode = 'ry1') h on g.vehicleshape = h.vehicleshape and g.vehiclesize = h.vehiclesize AND g.priceband = h.priceband  
LEFT JOIN (
  SELECT *
  FROM ucinv_sales_previous_60
  WHERE thedate = curdate()
    AND storecode = 'ry1') i on g.vehicleshape = i.vehicleshape and g.vehiclesize = i.vehiclesize AND g.priceband = i.priceband  
LEFT JOIN (
  SELECT *
  FROM ucinv_sales_previous_90
  WHERE thedate = curdate()
    AND storecode = 'ry1') j on g.vehicleshape = j.vehicleshape and g.vehiclesize = j.vehiclesize AND g.priceband = j.priceband     
GROUP BY g.vehicleshape, g.vehiclesize, g.priceband, g.avail    
ORDER BY avail DESC 


SELECT *
FROM ucinv_sales_previous_30
WHERE vehicleshape = 'suv'
  AND vehiclesize = 'midsize'
  and priceband = '40k+'
  AND thedate = '08/06/2015'