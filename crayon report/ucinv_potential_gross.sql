SELECT a.stocknumber, a.from_date, a.best_price, a.status,
  b.gtjrnl, gtdate, gtdoc#, gttamt
-- SELECT COUNT(*) 
FROM ucinv_tmp_avail_4 a
LEFT JOIN dds.stgArkonaGLPTRNS b on a.stocknumber = b.gtctl#
  AND b.gtacct IN ('124000','124100')
WHERE a.thedate = curdate() - 1
  AND a.sale_date > curdate()
  AND a.storecode = 'RY1'

  
SELECT a.stocknumber, a.from_date, a.best_price, a.status,
  b.gtjrnl, sum(gttamt)
-- SELECT COUNT(*) 
FROM ucinv_tmp_avail_4 a
LEFT JOIN dds.stgArkonaGLPTRNS b on a.stocknumber = b.gtctl#
  AND b.gtacct IN ('124000','124100')
WHERE a.thedate = curdate() - 1
  AND a.sale_date > curdate()
  AND a.storecode = 'RY1'
GROUP BY a.stocknumber, a.from_date, a.best_price, a.status, b.gtjrnl
ORDER BY stocknumber   


SELECT a.gtdate, a.gtctl#, a.gtacct, left(al2,12), left(dl4,12),
  -1 * cast(round(coalesce(gttamt, 0), 0) as sql_integer) as gttamt
FROM dds.stgarkonaglptrns a
INNER JOIN dds.zcb d on a.gtacct = d.glaccount
  AND d.name = 'gmfs gross'
  AND d.dl5 = 'used'
WHERE gtdate between curdate() - 45 and curdate();
			
SELECT *
FROM dds.zcb
WHERE name = 'gmfs gross'
  AND dl5 = 'used'		
  
select * FROM dds.gmfs_accounts WHERE page = 16 AND department = 'UC' AND account_type = '4'


  SELECT a.gtdate, a.gtctl#, a.gtacct, left(al2,12), left(dl4,12),
    -1 * cast(round(coalesce(gttamt, 0), 0) as sql_integer) as gttamt
INTO #wtf1
  FROM dds.stgarkonaglptrns a
  INNER JOIN dds.zcb d on a.gtacct = d.glaccount
    AND d.name = 'gmfs gross'
    AND d.dl5 = 'used'
  WHERE gtdate between curdate() - 45 and curdate()

  
  SELECT a.gtdate, a.gtctl#, a.gtacct, 
    -1 * cast(round(coalesce(gttamt, 0), 0) as sql_integer) as gttamt 
INTO #wtf2	
  FROM dds.stgarkonaglptrns a
  INNER JOIN dds.gmfs_Accounts b on a.gtacct = b.gl_account
    AND b.page = 16
    AND b.department = 'UC' 
    AND b.account_type = '4'
  WHERE gtdate between curdate() - 45 and curdate()
  
SELECT *
FROM #wtf1 a
full OUTER JOIN #wtf2 b on a.gtctl# = b.gtctl# 
WHERE a.gttamt <> b.gttamt
			
SELECT DISTINCT gtacct FROM #wtf2
			
-- 5/20	------------------------------------------------------------------------	
SELECT * 
INTO #gl
FROM dds.stgArkonaGLPTRNS a
INNER JOIN dds.gmfs_Accounts b on a.gtacct = b.gl_account
  AND account_type = '1' 
  AND department = 'UC'	
AND a.gtdate > '12/31/2015'  

select COUNT(*) FROM #gl 

select * FROM #gl 

select gtjrnl, COUNT(*) FROM #gl GROUP BY gtjrnl

select * 
INTO #inv
from ucinv_tmp_Avail_4 
WHERE from_date > '12/31/2015' -- acquired IN 2016
  AND sale_date <> thedate -- only unsold inventory
  
select * FROM #inv ORDER BY thedate
  
select a.thedate, a.stocknumber, a.from_Date, a.sale_date, a.best_price, a.invoice, 
  a.sold_amount, 
  b.*
FROM #inv a
LEFT JOIN #gl b on a.thedate = b.gtdate
  AND a.stocknumber = b.gtctl#
WHERE a.stocknumber = '26531B'
ORDER BY a.thedate  

SELECT gtjrnl, gtdate, gtctl#, gtdoc#, gtref#, SUM(gttamt) AS gttamt
INTO #gl1
FROM #gl 
GROUP BY gtjrnl, gtdate, gtctl#, gtdoc#, gtref#

select * FROM #gl1

select a.thedate, a.stocknumber, a.from_Date, a.sale_date, a.best_price, a.invoice, 
  a.sold_amount, 
  b.*
FROM #inv a
LEFT JOIN #gl1 b on a.thedate = b.gtdate
  AND a.stocknumber = b.gtctl#
WHERE a.stocknumber = '26684AB'
ORDER BY a.thedate 

/*** Anomalies ***/
26684AB: joining on inv date AND gl date does NOT WORK here
		 tool acquisition: 3/7
		 gl acquisition:  3/5

/*** Anomalies ***/

select gtdate, gtjrnl, gtacct, gtctl#, gtdoc#, gtdesc, SUM(gttamt) AS gttamt
FROM #gl a
WHERE EXISTS (
  SELECT 1
  FROM #gl
  WHERE gtctl# = a.gtctl#
    AND gtdesc = 'C/E')
GROUP BY gtdate, gtjrnl, gtacct, gtctl#, gtdoc#, gtdesc	
ORDER BY gtctl#	


select * FROM #gl WHERE gtctl# = '24295P'

-- restart: more
DROP TABLE #inv;
select * 
INTO #inv
from ucinv_tmp_Avail_4 
WHERE from_date > '12/31/2014' -- acquired > 2014
  AND sale_date <> thedate -- only unsold inventory
  
SELECT *
FROM dds.gmfs_Accounts 
WHERE account_type = '1' 
  AND department = 'UC'  

-- base gl on #inv AND account  
DROP TABLE #gl;
SELECT a.* 
INTO #gl
FROM dds.stgArkonaGLPTRNS a
INNER JOIN dds.gmfs_Accounts b on a.gtacct = b.gl_account
  AND account_type = '1' 
  AND department = 'UC'	
INNER JOIN (
  SELECT stocknumber 
  FROM #inv
  GROUP BY stocknumber)  c on a.gtctl# = c.stocknumber;

DROP TABLE #gl1;  
SELECT gtjrnl, gtdate, gtctl#, gtdoc#, gtref#, SUM(gttamt) AS gttamt
INTO #gl1
FROM #gl 
GROUP BY gtjrnl, gtdate, gtctl#, gtdoc#, gtref#;  

select * 
FROM #gl1
ORDER BY gtctl#

SELECT gtjrnl, COUNT(*)
FROM #gl1
GROUP BY gtjrnl

select * 
-- SELECT COUNT(*)
FROM #inv a
WHERE a.thedate <> a.sale_date
  AND from_Date > '01/01/2016'
  
SELECT * FROM #gl1 order BY gtctl#

-- i fucking need to DO this IN postgres --


