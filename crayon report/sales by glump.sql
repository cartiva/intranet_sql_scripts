SELECT *
FROM ucinv_tmp_sales_activity
WHERE sale_type = 'retail'


SELECT MIN(sale_date), MAX(sale_Date)
FROM ucinv_tmp_sales_activity

SELECT shape, size, COUNT(*),
  SUM(CASE WHEN sale_amount < 5000 THEN 1 ELSE 0 END) AS [0 - 5k],
  SUM(CASE WHEN sale_amount between 5000  and 9999 THEN 1 ELSE 0 END) AS [5 - 10k],
  SUM(CASE WHEN sale_amount between 10000  and 14999 THEN 1 ELSE 0 END) AS [10 - 15k],
  SUM(CASE WHEN sale_amount between 15000  and 19999 THEN 1 ELSE 0 END) AS [15 - 20k],
  SUM(CASE WHEN sale_amount between 20000  and 24999 THEN 1 ELSE 0 END) AS [20 - 25k],
  SUM(CASE WHEN sale_amount between 25000  and 29999 THEN 1 ELSE 0 END) AS [25 - 30k],
  SUM(CASE WHEN sale_amount between 30000  and 39999 THEN 1 ELSE 0 END) AS [30 - 40k],
  SUM(CASE WHEN sale_amount > 39999 THEN 1 ELSE 0 END) AS [40k +]
FROM ucinv_tmp_sales_activity
WHERE sale_type = 'retail'
  AND storecode = 'ry1'
GROUP BY shape, size
ORDER BY COUNT(*) DESC


SELECT shape, size, COUNT(*),
  SUM(CASE WHEN sale_amount < 5000 THEN 1 ELSE 0 END) AS [0 - 5k],
  SUM(CASE WHEN sale_amount between 5000  and 9999 THEN 1 ELSE 0 END) AS [5 - 10k],
  SUM(CASE WHEN sale_amount between 10000  and 14999 THEN 1 ELSE 0 END) AS [10 - 15k],
  SUM(CASE WHEN sale_amount between 15000  and 19999 THEN 1 ELSE 0 END) AS [15 - 20k],
  SUM(CASE WHEN sale_amount between 20000  and 24999 THEN 1 ELSE 0 END) AS [20 - 25k],
  SUM(CASE WHEN sale_amount between 25000  and 29999 THEN 1 ELSE 0 END) AS [25 - 30k],
  SUM(CASE WHEN sale_amount between 30000  and 39999 THEN 1 ELSE 0 END) AS [30 - 40k],
  SUM(CASE WHEN sale_amount > 39999 THEN 1 ELSE 0 END) AS [40k +]
FROM (  
  SELECT cast(a.soldts as sql_date), 
    substring(d.vehicletype, position('_' in d.vehicletype) + 1, 12) AS shape, 
    substring(d.vehiclesegment,  position('_' in d.vehiclesegment) + 1, 12) as size,
    a.soldamount AS sale_amount
  -- SELECT *
  FROM dps.vehiclesales a
  INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
  INNER JOIN dps.MakeModelClassifications d on c.make = d.make AND c.model = d.model
  WHERE a.typ = 'VehicleSale_Retail'
    AND a.status = 'VehicleSale_Sold'
    AND LEFT(b.stocknumber, 1) IN ('1','2','3','4','5','6','7','8','9')) r
GROUP BY shape, size    
ORDER BY COUNT(*) DESC     