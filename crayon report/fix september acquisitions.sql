-- ok
SELECT storecode, month(from_date), COUNT(*)
from ucinv_tmp_avail_1
WHERE from_date > '01/01/2015'
GROUP BY storecode, month(from_date)
-- ok
SELECT storecode, month, COUNT(*)
FROM (
SELECT storecode, stocknumber, month(from_Date) AS month 
FROM ucinv_tmp_avail_2
WHERE from_date > '01/01/2015'
GROUP BY storecode, stocknumber, month(from_Date)) X
GROUP BY storecode, month
-- ok
SELECT month(acq_Date), COUNT(*)
FROM ucinv_tmp_avail_3
WHERE acq_date > '01/01/2015'
  AND LEFT(stocknumber, 1) <> 'h'
GROUP BY month(acq_date)
-- ok
SELECT storecode, month, COUNT(*)
FROM ( SELECT storecode, stocknumber, month(from_date) AS month
  FROM ucinv_tmp_avail_4 WHERE from_date > '01/01/2015'
  GROUP BY storecode, stocknumber, month(from_date)) x GROUP BY storecode, month

SELECT DISTINCT sale_type FROM ucinv_tmp_avail_4