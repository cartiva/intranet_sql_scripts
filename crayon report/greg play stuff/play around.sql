DROP TABLE #wtf;
DECLARE @date date;
@date = curdatE() - 1;
SELECT x.*, y.shapeAndSize
INTO #wtf
FROM (
  SELECT e.priceband, e.pricefrom, e.priceThru, e.location, e.shape, e.size, 
    coalesce(SUM(f.units), 0) AS previous30,
    coalesce(sum(round(1.0 * g.units/12, 1)), 0) AS previous365,
    COUNT(h.stocknumber) AS fresh,
    COUNT(i.stocknumber) AS aged,
    COUNT(j.stocknumber) AS pulled
  FROM (
    SELECT *
    FROM (
      SELECT * 
      FROM ucinv_price_bands) a,
    (
      SELECT 'RY1' as location FROM system.iota
      UNION SELECT 'RY2' FROM system.iota
      UNION SELECT 'Market' FROM system.iota) b,
    (
      SELECT distinct shape
      FROM ucinv_shapes_and_sizes) c,  
    (
      SELECT distinct size
      FROM ucinv_shapes_and_sizes) d) e
  LEFT JOIN ucinv_sales_previous_30 f on f.thedate = @date
    AND f.saleType = 'Retail' 
    AND e.shape = f.vehicleShape
    AND e.size = f.vehicleSize 
    AND e.priceband = f.priceband 
    AND 
      CASE e.location  --collate ads_default_ci
        WHEN 'Market' THEN 1 = 1 
        ELSE e.location = f.storecode collate ads_default_ci
      END        
  LEFT JOIN ucinv_sales_previous_365 g on g.thedate = @date
    AND g.saleType = 'Retail' 
    AND e.shape = g.vehicleShape
    AND e.size = g.vehicleSize 
    AND e.priceband = g.priceband 
    AND 
      CASE e.location  --collate ads_default_ci
        WHEN 'Market' THEN 1 = 1 
        ELSE e.location = g.storecode collate ads_default_ci
      END  
  LEFT JOIN (
    SELECT *
    FROM ucinv_available a
    LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
      AND b.theDate = @date
    WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) BETWEEN a.fromts AND a.thruts
      AND timestampdiff(sql_tsi_day, CAST(a.fromts AS sql_date), @date) <= 30) h on e.shape = h.vehicleShape
      AND e.size = h.vehicleSize
      AND e.priceband = h.priceband  
      AND 
        CASE e.location  --collate ads_default_ci
          WHEN 'Market' THEN 1 = 1 
          ELSE e.location = h.storecode collate ads_default_ci
        END    
  LEFT JOIN (
    SELECT *
    FROM ucinv_available a
    LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
      AND b.theDate = @date
    WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) BETWEEN a.fromts AND a.thruts
      AND timestampdiff(sql_tsi_day, CAST(a.fromts AS sql_date), @date) > 30) i on e.shape = i.vehicleShape
      AND e.size = i.vehicleSize
      AND e.priceband = i.priceband  
      AND 
        CASE e.location  --collate ads_default_ci
          WHEN 'Market' THEN 1 = 1 
          ELSE e.location = i.storecode collate ads_default_ci
        END      
  LEFT JOIN (
    SELECT *
    FROM ucinv_pulled a
    LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
      AND thedate = @date
    WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) 
      BETWEEN a.fromts AND a.thruts) j on e.shape = j.vehicleShape
      AND e.size = j.vehicleSize
      AND e.priceband = j.priceband
      AND 
        CASE e.location
          WHEN 'MARKET' THEN 1 = 1
          ELSE e.location = j.storecode collate ads_default_ci
        END     
  GROUP BY e.priceband, e.pricefrom, e.priceThru, e.location, e.shape, e.size) x
LEFT JOIN ucinv_shapes_and_sizes y on x.shape = y.shape
  AND x.size = y.size;
  
  
SELECT *
FROM #wtf

SELECT priceband, SUM(previous30)
FROM #wtf
GROUP BY priceband
ORDER BY SUM(previous30) DESC 

SELECT priceband, shape, SUM(previous30)
FROM #wtf
GROUP BY priceband, shape
ORDER BY SUM(previous30) DESC 

SELECT shape, SUM(previous30)
FROM #wtf
GROUP BY shape
ORDER BY SUM(previous30) DESC 


SELECT *
FROM #wtf a
INNER JOIN (
  SELECT n - 1 AS n
  FROM dds.tally
  WHERE mod(n, 10000) = 0
    AND n < 50000) b on a.pricefrom > b.n
    
SELECT b.n, SUM(previous30)
FROM #wtf a
INNER JOIN (   
  SELECT n AS n, n + 9999 AS m
  FROM dds.tally
  WHERE mod(n, 10000) = 0
    AND n < 50000) b on a.pricefrom >= b.n AND a.priceThru <= b.m   
GROUP BY b.n    