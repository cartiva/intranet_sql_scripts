/*
20 shapes & sizes
19 price bands
380 rows per day
*/

/*  
ok forget the date for now,
just get what IS needed for basic inventory
date does NOT matter until we get to statuses AND pricebands
-- go with vehiclesales sold date (may be earlier than VehicleInventoryItems thrudate) 
*/
DROP TABLE ucinv_tmp_avail_1;
CREATE TABLE ucinv_tmp_avail_1 (
  stocknumber cichar(20) constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  shape_and_size cichar(24) constraint NOT NULL,
  from_date date constraint NOT NULL,
  sale_date date constraint NOT NULL,
  sale_type cichar(9) constraint NOT NULL,
  VehicleInventoryItemID char(38) constraint NOT NULL,
  constraint pk primary key (stocknumber)) IN database;
INSERT INTO ucinv_tmp_avail_1  
SELECT a.stocknumber, 
  CASE a.owninglocationid
    WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
    WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
    WHEN '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'RY3'
    ELSE 'XXX'
  END AS storecode, 
  left(f.shapeandsize, 24) AS shape_and_size,
  CAST(a.fromts AS sql_date) AS from_date,
  coalesce(CAST(g.soldts AS sql_date), CAST('12/31/9999' AS sql_date)) AS sale_date,
  coalesce(substring(g.typ, position('_' IN g.typ) + 1, 9), 'Inventory') AS sale_type,  
  a.VehicleInventoryItemID 
-- SELECT COUNT(*)  
FROM dps.VehicleInventoryItems a
INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID
INNER JOIN dps.MakeModelClassifications c on b.make = c.make
  AND b.model = c.model  
INNER JOIN dps.typDescriptions d on c.vehicleSegment = d.typ
INNER JOIN dps.typDescriptions e on c.vehicleType = e.typ 
INNER JOIN ucinv_shapes_and_sizes f on upper(e.description) = upper(f.shape) collate ads_default_ci 
  AND upper(d.description) = upper(f.size) collate ads_default_ci  
LEFT JOIN dps.vehiclesales g on a.VehicleInventoryItemID = g.VehicleInventoryItemID 
  AND g.status = 'VehicleSale_Sold' 
-- limit to those vehicles sold after 1/1/14  
WHERE coalesce(CAST(a.thruts AS sql_date), CAST('12/31/9999' AS sql_date)) > '01/01/2014'
  AND a.stocknumber IS NOT NULL;-- 10/12/15 unbooked intramarket xfts 


EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_1','ucinv_tmp_avail_1.adi','storecode','storecode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_1','ucinv_tmp_avail_1.adi','shape_and_size','shape_and_size','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_1','ucinv_tmp_avail_1.adi','from_date','from_date','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_1','ucinv_tmp_avail_1.adi','sale_Date','sale_Date','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_1','ucinv_tmp_avail_1.adi','sale_type','sale_type','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_1','ucinv_tmp_avail_1.adi','VehicleInventoryItemID','VehicleInventoryItemID','',2,512,'');

DROP TABLE ucinv_tmp_avail_2;
CREATE TABLE ucinv_tmp_avail_2 (
  thedate date constraint NOT NULL,
  stocknumber cichar(20) constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  shape_and_size cichar(24) constraint NOT NULL,
  from_date date constraint NOT NULL,
  sale_date date constraint NOT NULL,
  sale_type cichar(9) constraint NOT NULL,
  VehicleInventoryItemID char(38) constraint NOT NULL,
  date_priced date constraint NOT NULL,
  best_price integer constraint NOT NULL,
  invoice integer constraint NOT NULL,
  days_since_priced integer constraint NOT NULL,
  price_band cichar(20) constraint NOT NULL,
  days_owned integer constraint NOT NULL,
  constraint pk primary key (thedate, stocknumber)) IN database;
INSERT INTO ucinv_tmp_avail_2
SELECT a.thedate, b.*, -- SELECT COUNT(*)
  coalesce(CAST(c.pricingts AS sql_date), CAST('12/31/9999' AS sql_date)) AS date_priced,
  coalesce(bestprice, -1) AS best_price, coalesce(invoice, -1) AS invoice,
  coalesce(dayssincepriced, 0) AS days_since_priced, 
  coalesce(c.priceband, 'Not Priced') AS price_band,
  (a.thedate - b.from_date) + 1 AS days_owned
FROM dds.day a
LEFT JOIN ucinv_tmp_avail_1 b on a.thedate BETWEEN b.from_date AND b.sale_date
LEFT JOIN ucinv_price_by_day c on a.thedate = c.thedate
  AND b.stocknumber = c.stocknumber
WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1;

EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_2','ucinv_tmp_avail_2.adi','storecode','storecode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_2','ucinv_tmp_avail_2.adi','shape_and_size','shape_and_size','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_2','ucinv_tmp_avail_2.adi','from_date','from_date','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_2','ucinv_tmp_avail_2.adi','sale_Date','sale_Date','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_2','ucinv_tmp_avail_2.adi','sale_type','sale_type','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_2','ucinv_tmp_avail_2.adi','VehicleInventoryItemID','VehicleInventoryItemID','',2,512,'');

/*
this IS what i actually need to use with _avail_1 to sort out the status
dates

how to figure out the status for the day
possible combinations:

available no pull
pull no avail : sale date < pull thru

deriving 2 bits of inf: status (raw,avail,pull) AND sold_from, status on day of 
  this might actually need to be done after statuses
stuff LIKE raw_flr also might need to be done IN a subsequent step  

CASE 1: no pull/avail status :
           IN raw every day of owned
           
leaning towards
  on the day a vehicle IS pulled, it does NOT COUNT AS raw materials
  on the day a vehicle becomes available, it does NOT count    
  on the day a vehicle IS sold, it IS counted AS a sale AND AS a status       
*/

-- all vehicles from dps.viis with a pulled or avail status, most recent pulled, available statuses 
DROP TABLE ucinv_tmp_statuses;
CREATE TABLE ucinv_tmp_statuses (
  VehicleInventoryItemID char(38) constraint NOT NULL,
  status cichar(20) constraint NOT NULL,
  status_from_date date constraint NOT NULL,
  status_thru_date date constraint NOT NULL,
  constraint pk primary key (VehicleInventoryItemID,status)) IN database;
INSERT INTO ucinv_tmp_statuses
SELECT VehicleInventoryItemID, status, from_date, coalesce(thru_date, CAST('12/31/9999' AS sql_date))
FROM (
  SELECT a.VehicleInventoryItemID, 'available' AS status, CAST(a.fromts AS sql_date) AS from_date, CAST(b.thruts AS sql_date) AS thru_date
  FROM (
    SELECT VehicleInventoryItemID, MAX(fromts) AS fromts
    FROM dps.VehicleInventoryItemStatuses a
    WHERE status = 'RMFlagAV_Available'
    GROUP BY VehicleInventoryItemID) a
    INNER JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
      AND a.fromts = b.fromts
      AND b.status = 'RMFlagAV_Available'
UNION 
select a.VehicleInventoryItemID, 'pulled', CAST(a.fromts AS sql_date) AS from_date, CAST(b.thruts AS sql_date) AS thru_date
FROM (
  SELECT VehicleInventoryItemID, MAX(fromts) AS fromts
  FROM dps.VehicleInventoryItemStatuses a
  WHERE status = 'RMFlagPulled_Pulled'
  GROUP BY VehicleInventoryItemID) a
  INNER JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND a.fromts = b.fromts
    AND b.status = 'RMFlagPulled_Pulled') x;
    
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_statuses','ucinv_tmp_statuses.adi','VehicleInventoryItemID','VehicleInventoryItemID','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_statuses','ucinv_tmp_statuses.adi','status','status','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_statuses','ucinv_tmp_statuses.adi','status_from_date','status_from_date','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_statuses','ucinv_tmp_statuses.adi','status_thru_date','status_thru_date','',2,512,'');    

/*
-- CASE 1: no status   
SELECT a.VehicleInventoryItemID, a.sale_date, a.sale_type,
  a.from_date AS raw_from_date, 
  CASE sale_date
    WHEN '12/31/9999' THEN cast('12/31/9999' AS sql_date)
    ELSE sale_date
  END AS raw_thru_date,
  cast('12/31/9999' AS sql_date) AS pull_from_date,
  cast('12/31/9999' AS sql_date) AS pull_thru_date,
  cast('12/31/9999' AS sql_date) AS avail_from_date,
  cast('12/31/9999' AS sql_date) AS avail_thru_date
-- SELECT COUNT(*)  
FROM ucinv_tmp_avail_1 a
LEFT JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE b.VehicleInventoryItemID IS NULL  
  

-- CASE 2: status EXISTS -- pulled but no available
-- IF pull ends before sale date, goes back to raw
shit what i want IS a single row per vehicle with a single period for each status
shit shit shit
-- so, IN this CASE, WHERE there IS no available status, IF pull ends before sale date
--   THEN there IS no pull status, often, mech finds excessive WORK, vehicle
--   becomes a ws item, essentially pull IS cancelled
-- so i am only interested IN recording pulls that result IN an available status
--   OR a sale FROM pulled status
-- SELECT * FROM (
--24262xxa
--SELECT a.sale_type, a.stocknumber, a.storecode, a.from_Date, a.sale_date, 
--  b.status, b.status_From_Date, b.status_thru_date,
-- raw
--  a.from_date AS raw_from_date,
SELECT a.VehicleInventoryItemID, a.sale_date, a.sale_type,  
  a.from_date AS raw_from_date,
  CASE 
    WHEN b.status_thru_date < a.sale_date THEN a.sale_date -- cancelled pulls
    ELSE b.status_from_date - 1 
  END AS raw_thru_date,
  CASE 
    WHEN b.status_thru_date < a.sale_date THEN cast('12/31/9999' AS sql_date) -- cancelled pulls
    ELSE b.status_from_date
  END AS pull_from_date,
  CASE
    WHEN b.status_thru_date < a.sale_date THEN cast('12/31/9999' AS sql_date) -- cancelled pul
    ELSE a.sale_date 
  END AS pull_thru_date, 
  cast('12/31/9999' AS sql_date) AS avail_from_date,
  cast('12/31/9999' AS sql_date) AS avail_thru_date
-- SELECT *  
-- SELECT COUNT(DISTINCT a.stocknumber)
FROM ucinv_tmp_avail_1 a
INNER JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE NOT EXISTS (
  SELECT 1
  FROM ucinv_tmp_statuses
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
    AND status = 'available') 
--AND b.status_thru_date < a.sale_date
--AND b.status_from_date < a.from_date -- pull before FROM: there are none
--AND b.status_From_date = a.from_date -- pull on FROM: there are none 
-- ) x WHERE stocknumber = '24262xxa'
*/
/*
-- there are no vehicles with a status that have NOT been pulled    
SELECT a.*, b.*
-- SELECT COUNT(DISTINCT a.stocknumber)
FROM ucinv_tmp_avail_1 a
INNER JOIN #wtf b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE NOT EXISTS (
  SELECT 1
  FROM #wtf
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
    AND status = 'pulled')   
*/     
/*    
-- CASE 3: status EXISTS both pulled AND availabe
SELECT a.*, b.*
-- SELECT COUNT(DISTINCT a.stocknumber)
FROM ucinv_tmp_avail_1 a
INNER JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE EXISTS (
  SELECT 1
  FROM ucinv_tmp_statuses
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
    AND status = 'available')  
AND EXISTS (
  SELECT 1
  FROM ucinv_tmp_statuses
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
    AND status = 'pulled')      

SELECT VehicleInventoryItemID, sale_date, sale_type, 
  from_date as raw_from_date,
  pull_from_date -1 as raw_thru_date,
  pull_from_date,
  sale_Date as pull_thru_date, 
  cast('12/31/9999' AS sql_date) AS avail_from_date,
  cast('12/31/9999' AS sql_date) AS avail_thru_date  
FROM (   
-- SELECT * FROM (
SELECT a.stocknumber, a.from_date, a.sale_date, a.sale_type, a.VehicleInventoryItemID, 
  b.status_from_date AS pull_from_date,
  b.status_thru_date AS pull_thru_date,
  c.status_from_date AS avail_from_date,
  c.status_thru_date AS avail_thru_date
-- SELECT COUNT(*)  
FROM ucinv_tmp_avail_1 a   
INNER JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'pulled'
INNER JOIN ucinv_tmp_statuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.status = 'available'  
) x WHERE pull_thru_date <> avail_from_date

anomalies
1. pull_thru_date <> avail_from_dates
  pull_thru_date <> avail_from_date AND sale_date = pull_thru_Date
    pulled mult times, did NOT make it to avail on last pull, sold out of pull
      raw: from_date -> pull_from - 1
      pull: pull_from -> sale_date
      avail: 9999
  pull_thru_date <> avail_from_date AND sale_date <> pull_thru_date
    these also pulled mult times, did NOT make it to avail on last pull, sold out of pull
    the diff IS sale_date & pull_thru DO NOT match, fuck it go with sale_date
  so these 2 become 1
  filter: pull_thru_date <> avail_from_date
      raw: from_date -> pull_from - 1
      pull: pull_from -> sale_date
      avail: 9999  
      
SELECT * FROM ( 
-- SELECT COUNT(*) FROM (
SELECT a.stocknumber, a.sale_type, a.VehicleInventoryItemID, 
  a.from_date, 
  b.status_from_date AS pull_from_date,
  b.status_thru_date AS pull_thru_date,
  c.status_from_date AS avail_from_date,
  c.status_thru_date AS avail_thru_date,
  a.sale_date 
-- SELECT COUNT(*)  
FROM ucinv_tmp_avail_1 a   
INNER JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'pulled'
INNER JOIN ucinv_tmp_statuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.status = 'available'  
) x WHERE pull_thru_Date = avail_from_date -- elim anomaly 1
-- AND from_date = pull_from_date
AND pull_from_date = avail_from_date  -- 3.
--AND pull_from_date = avail_from_date -- 4.
--AND sale_type = 'inventory'
-- AND avail_thru_date <> sale_date -- 2.
*/
/*
Anomalies
2.  avail_thru_date <> sale_date
    available, THEN put INTO ws buffer & wsed
      these should be seen AS ws out of avail    
    back ons, put back INTO raw AND sold
  so it is looking LIKE this IS NOT an anomaly to be addressed separately,
    :: simply make avail_Thu always = sale_date    
3. IF the vehicle becomes avail on the same day it IS pulled, what IS the 
      status on that day, thinking, pull = 9999, but avail IS thedate
      that should WORK
      :: pull = 9999
4. from_date = pull_from_date 
     :: raw becomes 9999     
    
      
SELECT VehicleInventoryItemID, sale_date, sale_type,  
  CASE -- anomaly 4
    WHEN from_date = pull_from_date THEN cast('12/31/9999' AS sql_date) 
    ELSE from_date 
  END AS raw_from_date,
  CASE -- anomaly 4
    WHEN from_date = pull_from_date THEN cast('12/31/9999' AS sql_date) 
    ELSE pull_from_date -1
  END AS raw_thru_date,
  CASE -- anomaly 3
    WHEN pull_from_date = avail_from_date THEN cast('12/31/9999' AS sql_date) 
    ELSE pull_from_date
  END AS pull_from_date,
  CASE -- anomaly 3
    WHEN pull_from_date = avail_from_date THEN cast('12/31/9999' AS sql_date) 
    ELSE pull_thru_date -1
  END AS pull_thru_date,
  avail_from_date, 
  sale_date AS avail_thru_date  
FROM ( 
-- SELECT COUNT(*) FROM (
SELECT a.stocknumber, a.sale_type, a.VehicleInventoryItemID, 
  a.from_date, 
  b.status_from_date AS pull_from_date,
  b.status_thru_date AS pull_thru_date,
  c.status_from_date AS avail_from_date,
  c.status_thru_date AS avail_thru_date,
  a.sale_date 
-- SELECT COUNT(*)  
FROM ucinv_tmp_avail_1 a   
INNER JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'pulled'
INNER JOIN ucinv_tmp_statuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.status = 'available'  
) x WHERE pull_thru_Date = avail_from_date -- elim anomaly 1     

-- need to generate a TABLE to JOIN to _avail_2 so that i know the status of
-- each vehicle on each day
*/   
-- 1 row per vehicle
DROP TABLE ucinv_tmp_avail_3;
CREATE TABLE ucinv_tmp_avail_3 (
  stocknumber cichar(20) constraint NOT NULL,
  VehicleInventoryItemID char(38) constraint NOT NULL,
  acq_date date constraint NOT NULL,
  sale_date date constraint NOT NULL,
  sale_type cichar(9) constraint NOT NULL,
  raw_from_date date constraint NOT NULL,
  raw_thru_date date constraint NOT NULL,
  pull_from_date date constraint NOT NULL,
  pull_thru_date date constraint NOT NULL,
  avail_from_date date constraint NOT NULL,
  avail_thru_date date constraint NOT NULL,
  constraint pk primary key (stocknumber)) IN database;
INSERT INTO ucinv_tmp_avail_3
-- CASE 1
SELECT a.stocknumber, a.VehicleInventoryItemID, a.from_date, a.sale_date, a.sale_type,
  a.from_date AS raw_from_date, 
  CASE sale_date
    WHEN '12/31/9999' THEN cast('12/31/9999' AS sql_date)
    ELSE sale_date
  END AS raw_thru_date,
  cast('12/31/9999' AS sql_date) AS pull_from_date,
  cast('12/31/9999' AS sql_date) AS pull_thru_date,
  cast('12/31/9999' AS sql_date) AS avail_from_date,
  cast('12/31/9999' AS sql_date) AS avail_thru_date
-- SELECT COUNT(*)  
FROM ucinv_tmp_avail_1 a
LEFT JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE b.VehicleInventoryItemID IS NULL  
UNION 
-- CASE 2
SELECT a.stocknumber, a.VehicleInventoryItemID, a.from_date, a.sale_date, a.sale_type,  
  a.from_date AS raw_from_date,
  CASE 
    WHEN b.status_thru_date < a.sale_date THEN a.sale_date -- cancelled pulls
    ELSE b.status_from_date - 1 
  END AS raw_thru_date,
  CASE 
    WHEN b.status_thru_date < a.sale_date THEN cast('12/31/9999' AS sql_date) -- cancelled pulls
    ELSE b.status_from_date
  END AS pull_from_date,
  CASE
    WHEN b.status_thru_date < a.sale_date THEN cast('12/31/9999' AS sql_date) -- cancelled pul
    ELSE a.sale_date 
  END AS pull_thru_date, 
  cast('12/31/9999' AS sql_date) AS avail_from_date,
  cast('12/31/9999' AS sql_date) AS avail_thru_date
-- SELECT *  
-- SELECT COUNT(DISTINCT a.stocknumber)
FROM ucinv_tmp_avail_1 a
INNER JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE NOT EXISTS (
  SELECT 1
  FROM ucinv_tmp_statuses
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
    AND status = 'available') 
-- CASE 3 anomaly 1: mult pulls, last pull did NOT become avail, sold FROM pull        
UNION 
SELECT stocknumber, VehicleInventoryItemID, from_date, sale_date, sale_type, 
  from_date as raw_from_date,
  pull_from_date -1 as raw_thru_date,
  pull_from_date,
  sale_Date as pull_thru_date, 
  cast('12/31/9999' AS sql_date) AS avail_from_date,
  cast('12/31/9999' AS sql_date) AS avail_thru_date  
FROM (   
  -- SELECT * FROM (
  SELECT a.stocknumber, a.from_date, a.sale_date, a.sale_type, a.VehicleInventoryItemID, 
    b.status_from_date AS pull_from_date,
    b.status_thru_date AS pull_thru_date,
    c.status_from_date AS avail_from_date,
    c.status_thru_date AS avail_thru_date
  -- SELECT COUNT(*)  
  FROM ucinv_tmp_avail_1 a   
  INNER JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND b.status = 'pulled'
  INNER JOIN ucinv_tmp_statuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
    AND c.status = 'available') x WHERE pull_thru_date <> avail_from_date
UNION
-- CASE 3 everything except anomaly 1
SELECT stocknumber, VehicleInventoryItemID, from_date, sale_date, sale_type,  
  CASE -- anomaly 4
    WHEN from_date = pull_from_date THEN cast('12/31/9999' AS sql_date) 
    ELSE from_date 
  END AS raw_from_date,
  CASE -- anomaly 4
    WHEN from_date = pull_from_date THEN cast('12/31/9999' AS sql_date) 
    ELSE pull_from_date -1
  END AS raw_thru_date,
  CASE -- anomaly 3
    WHEN pull_from_date = avail_from_date THEN cast('12/31/9999' AS sql_date) 
    ELSE pull_from_date
  END AS pull_from_date,
  CASE -- anomaly 3
    WHEN pull_from_date = avail_from_date THEN cast('12/31/9999' AS sql_date) 
    ELSE pull_thru_date -1
  END AS pull_thru_date,
  avail_from_date, 
  sale_date AS avail_thru_date  
FROM ( 
  -- SELECT COUNT(*) FROM (
  SELECT a.stocknumber, a.sale_type, a.VehicleInventoryItemID, 
    a.from_date, 
    b.status_from_date AS pull_from_date,
    b.status_thru_date AS pull_thru_date,
    c.status_from_date AS avail_from_date,
    c.status_thru_date AS avail_thru_date,
    a.sale_date 
  -- SELECT COUNT(*)  
  FROM ucinv_tmp_avail_1 a   
  INNER JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND b.status = 'pulled'
  INNER JOIN ucinv_tmp_statuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
    AND c.status = 'available'  
  ) x WHERE pull_thru_Date = avail_from_date; -- elim anomaly 1     
      
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_3','ucinv_tmp_avail_3.adi','VehicleInventoryItemID','VehicleInventoryItemID','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_3','ucinv_tmp_avail_3.adi','sale_date','sale_date','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_3','ucinv_tmp_avail_3.adi','sale_type','sale_type','',2,512,'');    
     

/*
ucinv_tmp_avail_2 with days_avail, status, sold_from_Status, disposition
10/13/15 ADD sold_amount (coalesced to most recent best price)
*/ 
DROP TABLE ucinv_tmp_avail_4;
CREATE TABLE ucinv_tmp_avail_4 (
  thedate date constraint NOT NULL,
  stocknumber cichar(20) constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  shape_and_size cichar(24) constraint NOT NULL,
  from_date date constraint NOT NULL,
  sale_date date constraint NOT NULL,
  sale_type cichar(9) constraint NOT NULL,
  VehicleInventoryItemID char(38) constraint NOT NULL,
  date_priced date constraint NOT NULL,
  best_price integer constraint NOT NULL,
  invoice integer constraint NOT NULL,
  days_since_priced integer constraint NOT NULL,
  price_band cichar(20) constraint NOT NULL,
  days_owned integer constraint NOT NULL,
  days_avail integer constraint NOT NULL,
  status cichar(12) constraint NOT NULL,
  sold_from_status cichar(12) constraint NOT NULL,
  disposition cichar(18) constraint NOT NULL,
  sold_amount integer constraint NOT NULL,
  constraint pk primary key (thedate, stocknumber)) IN database;
INSERT INTO ucinv_tmp_avail_4
SELECT a.*, 
  CASE 
    WHEN a.thedate >= avail_from_date THEN (a.thedate - avail_from_date) + 1
    ELSE 0
  END AS days_avail,
  CASE
    WHEN thedate between raw_from_date AND raw_thru_date THEN 'raw'
    WHEN thedate BETWEEN pull_from_date AND pull_thru_date THEN 'pull'
    WHEN thedate BETWEEN avail_from_date AND avail_thru_date THEN 
      CASE 
        WHEN thedate - avail_from_date < 31 THEN 'avail_fresh'
        ELSE 'avail_aged'
      END
  END AS status,
  b.sold_from_status, 
  b.disposition,
  coalesce(c.sale_amount, a.best_price)  
FROM ucinv_tmp_avail_2 a
LEFT JOIN (
  SELECT a.*,
    CASE -- sold FROM status
      WHEN sale_type = 'inventory' THEN 'inventory'
      WHEN avail_thru_date = sale_date THEN 
        CASE 
          WHEN avail_thru_date - avail_from_date < 31 then 'avail_fresh'
          ELSE 'avail_aged'
        END 
      WHEN pull_thru_date = sale_date THEN 'pulled'
      WHEN raw_thru_date = sale_date THEN 'raw'    
    END AS sold_from_status,
    CASE -- disposition of avail vehicles
      WHEN sale_type = 'inventory' THEN 'NA'
      WHEN avail_from_date > curdate() THEN 'NA'
      WHEN sale_type = 'Retail' THEN 
        CASE 
          WHEN sale_date - avail_from_date < 31 THEN 'Sold 0-30'
          WHEN sale_date - avail_from_date BETWEEN 31 AND 60 THEN 'Sold 31-60'
          WHEN sale_date - avail_from_date > 60 THEN 'Sold > 60'
        END 
      WHEN sale_Type = 'Wholesale' THEN 'W/S from available'
    END AS Disposition
  FROM ucinv_tmp_avail_3 a) b on a.stocknumber = b.stocknumber
LEFT JOIN ucinv_tmp_gross_2 c on a.stocknumber = c.stocknumber;
    

EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_4','ucinv_tmp_avail_4.adi','storecode','storecode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_4','ucinv_tmp_avail_4.adi','shape_and_size','shape_and_size','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_4','ucinv_tmp_avail_4.adi','price_band','price_band','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_4','ucinv_tmp_avail_4.adi','from_date','from_date','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_4','ucinv_tmp_avail_4.adi','sale_Date','sale_Date','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_4','ucinv_tmp_avail_4.adi','sale_type','sale_type','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_4','ucinv_tmp_avail_4.adi','VehicleInventoryItemID','VehicleInventoryItemID','',2,512,'');  
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_4','ucinv_tmp_avail_4.adi','disposition','disposition','',2,512,''); 

DROP TABLE ucinv_tmp_avail_5;
CREATE TABLE ucinv_tmp_avail_5 (
  thedate date constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  shape_and_size cichar(24) constraint NOT NULL,
  price_band cichar(10) constraint NOT NULL,
  avail_aged integer constraint NOT NULL,
  avail_fresh integer constraint NOT NULL,
  avail_total integer constraint NOT NULL,
  pull integer constraint NOT NULL,
  raw integer constraint NOT NULL,
  constraint pk primary key(thedate,storecode,shape_and_size,price_band)) IN database;
-- DROP TABLE #wtf;
INSERT INTO ucinv_tmp_avail_5
select d.*, coalesce(e.avail_aged, 0) AS avail_aged, coalesce(e.avail_fresh, 0) AS avail_fresh,
  coalesce(e.avail_total, 0) AS avail_total, coalesce(e.pull, 0) AS pull,
  coalesce(e.raw, 0) AS raw
  FROM (
  SELECT a.thedate, 'RY1' as storecode, left(b.shapeandsize, 24) AS shape_and_size, left(c.priceband, 10) AS price_band
  FROM dds.day a, ucinv_shapes_and_sizes b, ucinv_price_bands c
  WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1
  UNION ALL 
  SELECT a.thedate, 'RY2' as storecode, left(b.shapeandsize, 24) AS shape_and_size, left(c.priceband, 10) AS price_band
  FROM dds.day a, ucinv_shapes_and_sizes b, ucinv_price_bands c
  WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1) d
LEFT JOIN (  
  SELECT thedate, storecode, shape_and_size, price_band, 
    SUM(CASE WHEN status = 'avail_aged' THEN 1 ELSE 0 END) AS avail_aged,
    SUM(CASE WHEN status = 'avail_fresh' THEN 1 ELSE 0 END) AS avail_fresh,
    SUM(CASE WHEN status = 'avail_aged' or status = 'avail_fresh' THEN 1 ELSE 0 END) AS avail_total,
    SUM(CASE WHEN status = 'pull' THEN 1 ELSE 0 END) AS pull,
    SUM(CASE WHEN status = 'raw' THEN 1 ELSE 0 END) AS raw
  FROM ucinv_tmp_avail_4
  GROUP BY thedate, storecode, shape_and_size, price_band) e on d.thedate = e.thedate
    AND d.storecode = e.storecode collate ads_default_ci
    AND d.shape_and_size = e.shape_and_size
    AND d.price_band = e.price_band; 
     
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_5','ucinv_tmp_avail_5.adi','storecode','storecode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_5','ucinv_tmp_avail_5.adi','shape_and_size','shape_and_size','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_5','ucinv_tmp_avail_5.adi','price_band','price_band','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_5','ucinv_tmp_avail_5.adi','avail_aged','avail_aged','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_5','ucinv_tmp_avail_5.adi','avail_fresh','avail_fresh','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_5','ucinv_tmp_avail_5.adi','avail_total','avail_total','',2,512,'');

-- ADD sales prev 30 & 365    
-- 6/24 previous 30 being derived FROM ucinv_sales_previous_30 which IS derived
-- FROM ucinv_sales which IS derived FROM factVehicleSale which IS derived
-- FROM arkona, change it to derive FROM dps, use ucinv_tmp_avail_2
DROP TABLE ucinv_tmp_avail_6;
CREATE TABLE ucinv_tmp_avail_6 (
  thedate date constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  shape_and_size cichar(24) constraint NOT NULL,
  price_band cichar(10) constraint NOT NULL,
  avail_aged integer constraint NOT NULL,
  avail_fresh integer constraint NOT NULL,
  avail_total integer constraint NOT NULL,
  pull integer constraint NOT NULL,
  raw integer constraint NOT NULL,
  sales_prev_30_retail integer constraint NOT NULL,
  sales_prev_30_ws integer constraint NOT NULL,
  sales_prev_365_retail numeric(8,1) constraint NOT NULL,
  sales_prev_365_ws numeric(8,1) constraint NOT NULL,
  sales_prev_30_retail_avail_fresh integer default '0' constraint NOT NULL,
  sales_prev_30_ws_avail_fresh integer default '0' constraint NOT NULL,
  sales_prev_30_retail_avail_aged integer default '0' constraint NOT NULL,
  sales_prev_30_ws_avail_aged integer default '0' constraint NOT NULL,
  sales_prev_30_retail_raw integer default '0' constraint NOT NULL,
  sales_prev_30_ws_raw integer default '0' constraint NOT NULL,
  sales_prev_30_retail_pulled integer default '0' constraint NOT NULL,
  sales_prev_30_ws_pulled integer default '0' constraint NOT NULL,  
  avail_fresh_prev_30_avg numeric(8,1) default '0' constraint NOT NULL,
  avail_aged_prev_30_avg numeric(8,1) default '0' constraint NOT NULL,
  avail_total_prev_30_avg numeric(8,1) default '0' constraint NOT NULL,
  raw_pull_prev_30_avg numeric(8,1) default '0' constraint NOT NULL, 
  owned_prev_30_avg numeric(8,1) default '0' constraint NOT NULL,
  turns_avail_fresh numeric(8,1) default '0' constraint NOT NULL,
  turns_raw_pull numeric(8,1) default '0' constraint NOT NULL, 
  turns_owned numeric(8,1) default '0' constraint NOT NULL,    
  constraint pk primary key(thedate,storecode,shape_and_size,price_band)) IN database;    
   
       
INSERT INTO ucinv_tmp_avail_6  
SELECT a.*, 
  coalesce(c.prev_30_retail, 0) AS prev_30_retail,
  coalesce(c.prev_30_ws, 0) AS prev_30_ws, 
  coalesce(d.prev_365_retail, 0) AS prev_365_retail,
  coalesce(e.prev_365_ws, 0) AS prev_365_ws,
  0,0,0,0,0,0,0,0,
  0,0,0,
  0,0,0,0,0
FROM ucinv_tmp_avail_5  a
LEFT JOIN (
  SELECT a.thedate, a.storecode, b.shape_and_size, b.price_band,
    SUM(CASE WHEN sale_type = 'retail' THEN 1 ELSE 0 END) AS prev_30_retail,
    SUM(CASE WHEN sale_type = 'wholesale' THEN 1 ELSE 0 END) AS prev_30_ws
  FROM (
    SELECT thedate, 'RY1' AS storecode
    FROM dds.day
    WHERE thedate BETWEEN '01/31/2014' AND curdate() - 1
    union
    SELECT thedate, 'RY2' AS storecode
    FROM dds.day
    WHERE thedate BETWEEN '01/31/2014' AND curdate() - 1) a
  LEFT JOIN (
    SELECT *
    FROM ucinv_tmp_avail_2
    WHERE thedate = sale_date) b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1  
      AND a.storecode = b.storecode collate ads_default_ci
  GROUP BY a.thedate, a.storecode, b.shape_and_size, b.price_band) c on a.thedate = c.thedate 
      AND a.storecode = c.storecode collate ads_default_ci
      AND a.shape_and_size = c.shape_and_size
      AND a.price_band = c.price_band 
LEFT JOIN (
  SELECT thedate, storecode, 
    TRIM(vehicleshape) + ' - ' + TRIM(vehiclesize) AS shape_and_size, priceband,
    round(1.0 * units/12, 1) AS prev_365_retail
  FROM ucinv_sales_previous_365
  WHERE saletype = 'retail') d on a.thedate = d.thedate 
      AND a.storecode = d.storecode
      AND a.shape_and_size = d.shape_and_size
      AND a.price_band = d.priceband  
LEFT JOIN (
  SELECT thedate, storecode, 
    TRIM(vehicleshape) + ' - ' + TRIM(vehiclesize) AS shape_and_size, priceband,
    round(1.0 * units/12, 1) AS prev_365_ws
  FROM ucinv_sales_previous_365
  WHERE saletype = 'wholesale') e on a.thedate = e.thedate 
      AND a.storecode = e.storecode
      AND a.shape_and_size = e.shape_and_size
      AND a.price_band = e.priceband;
/*
SELECT a.thedate, b.storecode, b.shape_and_size, b.price_band,
  round(SUM(avail_fresh)/30.0, 1) AS avail_fresh_prev_30_avg, 
  round(SUM(avail_aged)/30.0, 1)AS avail_aged_prev_30_avg, 
  round(SUM(avail_total)/30.0, 1) AS avail_total_prev_30_avg  
--INTO #wtf  
FROM dds.day a
LEFT JOIN ucinv_tmp_avail_5 b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1
WHERE a.thedate BETWEEN curdate()-7 AND curdate() - 1
GROUP BY a.thedate, b.storecode, b.shape_and_size, b.price_band;
*/
DROP TABLE ucinv_tmp_wtf;
CREATE TABLE ucinv_tmp_wtf (
  thedate date,
  storecode cichar(3),
  shape_and_size cichar(24), 
  price_band cichar(10),
  avail_fresh_prev_30_avg numeric(15,2),
  avail_aged_prev_30_avg numeric(15,2),
  avail_total_prev_30_avg numeric(15,2)) IN database;
INSERT INTO ucinv_tmp_wtf  
SELECT a.thedate, b.storecode, b.shape_and_size, b.price_band,
  round(SUM(avail_fresh)/30.0, 1) AS avail_fresh_prev_30_avg, 
  round(SUM(avail_aged)/30.0, 1)AS avail_aged_prev_30_avg, 
  round(SUM(avail_total)/30.0, 1) AS avail_total_prev_30_avg  
--INTO #wtf  
FROM dds.day a
LEFT JOIN ucinv_tmp_avail_5 b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1
WHERE a.thedate BETWEEN '01/31/2014' AND curdate() - 1
GROUP BY a.thedate, b.storecode, b.shape_and_size, b.price_band;  

EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_wtf','ucinv_tmp_wtf.adi','thedate','thedate','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_wtf','ucinv_tmp_wtf.adi','storecode','storecode','',2,512,''); 
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_wtf','ucinv_tmp_wtf.adi','shape_and_size','shape_and_size','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_wtf','ucinv_tmp_wtf.adi','price_band','price_band','',2,512,''); 
 
/* move this up to initial TABLE def
ALTER TABLE ucinv_tmp_avail_6
ADD COLUMN avail_fresh_prev_30_avg numeric(8,1) default '0' constraint NOT null
ADD COLUMN avail_aged_prev_30_avg numeric(8,1) default '0' constraint NOT null
ADD COLUMN avail_total_prev_30_avg numeric(8,1) default '0' constraint NOT NULL;

UPDATE ucinv_tmp_avail_6
SET avail_fresh_prev_30_avg = 0,
    avail_aged_prev_30_avg = 0,
    avail_total_prev_30_avg = 0;
*/
-- ucinv_tmp_avail_6 UPDATE 1
UPDATE ucinv_tmp_avail_6
SET avail_fresh_prev_30_avg = x.avail_fresh_prev_30_avg,
    avail_aged_prev_30_avg = x.avail_aged_prev_30_avg,
    avail_total_prev_30_avg = x.avail_total_prev_30_avg
FROM (
  SELECT b.*
  FROM ucinv_tmp_avail_6 a
  inner JOIN ucinv_tmp_wtf b on a.thedate = b.thedate
    AND a.storecode = b.storecode
    AND a.shape_and_size = b.shape_and_size
    AND a.price_band = b.price_band) x
WHERE ucinv_tmp_avail_6.thedate = x.thedate
  AND ucinv_tmp_avail_6.storecode = x.storecode
  AND ucinv_tmp_avail_6.shape_and_size = x.shape_and_size
  AND ucinv_tmp_avail_6.price_band = x.price_band;
  
/*
Greg Sorum <gsorum@cartiva.com>

Fresh Sold Last 30 Days
Aged Sold Last 30 Days
Raw Sold Last 30 Days
*/  

/* move this up to initial TABLE def
ALTER TABLE ucinv_tmp_avail_6
ADD COLUMN sales_prev_30_retail_avail_fresh integer default '0' constraint NOT NULL
ADD COLUMN sales_prev_30_ws_avail_fresh integer default '0' constraint NOT NULL
ADD COLUMN sales_prev_30_retail_avail_aged integer default '0' constraint NOT NULL
ADD COLUMN sales_prev_30_ws_avail_aged integer default '0' constraint NOT NULL
ADD COLUMN sales_prev_30_retail_raw integer default '0' constraint NOT NULL
ADD COLUMN sales_prev_30_ws_raw integer default '0' constraint NOT NULL
ADD COLUMN sales_prev_30_retail_pulled integer default '0' constraint NOT NULL
ADD COLUMN sales_prev_30_ws_pulled integer default '0' constraint NOT NULL;

UPDATE ucinv_tmp_avail_6
set sales_prev_30_retail_avail_fresh = 0,
    sales_prev_30_ws_avail_fresh = 0,
    sales_prev_30_retail_avail_aged = 0,
    sales_prev_30_ws_avail_aged = 0,
    sales_prev_30_retail_raw = 0,
    sales_prev_30_ws_raw = 0,
    sales_prev_30_retail_pulled = 0,
    sales_prev_30_ws_pulled = 0;     
*/    
-- ucinv_tmp_avail_6 UPDATE 2   
UPDATE ucinv_tmp_avail_6
set sales_prev_30_retail_avail_fresh = x.sales_prev_30_retail_avail_fresh,
    sales_prev_30_ws_avail_fresh = x.sales_prev_30_ws_avail_fresh,
    sales_prev_30_retail_avail_aged = x.sales_prev_30_retail_avail_aged,
    sales_prev_30_ws_avail_aged = x.sales_prev_30_ws_avail_aged,
    sales_prev_30_retail_raw = x.sales_prev_30_retail_raw,
    sales_prev_30_ws_raw = x.sales_prev_30_ws_raw,
    sales_prev_30_retail_pulled = x.sales_prev_30_retail_pulled,
    sales_prev_30_ws_pulled = x.sales_prev_30_ws_pulled
FROM (
  SELECT thedate, storecode, shape_and_size, price_band, 
    sum(CASE WHEN sale_type = 'retail' and sold_from_status = 'avail_fresh' THEN units ELSE 0 END) AS sales_prev_30_retail_avail_fresh,
    sum(CASE WHEN sale_type = 'wholesale' and sold_from_status = 'avail_fresh' THEN units ELSE 0 END) AS sales_prev_30_ws_avail_fresh,
    sum(CASE WHEN sale_type = 'retail' and sold_from_status = 'avail_aged' THEN units ELSE 0 END) AS sales_prev_30_retail_avail_aged,
    sum(CASE WHEN sale_type = 'wholesale' and sold_from_status = 'avail_aged' THEN units ELSE 0 END) AS sales_prev_30_ws_avail_aged,  
    sum(CASE WHEN sale_type = 'retail' and sold_from_status = 'raw' THEN units ELSE 0 END) AS sales_prev_30_retail_raw,
    sum(CASE WHEN sale_type = 'wholesale' and sold_from_status = 'raw' THEN units ELSE 0 END) AS sales_prev_30_ws_raw,  
    sum(CASE WHEN sale_type = 'retail' and sold_from_status = 'pulled' THEN units ELSE 0 END) AS sales_prev_30_retail_pulled,
    sum(CASE WHEN sale_type = 'wholesale' and sold_from_status = 'pulled' THEN units ELSE 0 END) AS sales_prev_30_ws_pulled      
  FROM (
    SELECT a.thedate, storecode, b.sale_type, shape_and_size, price_band, sold_from_status, COUNT(*) AS units
    FROM dds.day a
    LEFT JOIN (
      SELECT *
      FROM ucinv_tmp_avail_4
      WHERE sale_Date = thedate) b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1
    WHERE a.thedate BETWEEN '01/31/2014' AND curdate() - 1
    group by a.thedate, storecode, b.sale_type, shape_and_size, price_band, sold_from_status) m 
  GROUP BY thedate, storecode, shape_and_size, price_band) x    
WHERE ucinv_tmp_avail_6.thedate = x.thedate
  AND ucinv_tmp_avail_6.storecode = x.storecode
  AND ucinv_tmp_avail_6.shape_and_size = x.shape_and_size
  AND ucinv_tmp_avail_6.price_band = x.price_band;     
  
/*
7/7/15 
  ADD raw_pull_prev_30_avg, owned_prev_30_avg
  AND turns for avail_fresh, raw_pull, owned
*/  
/*
SELECT a.thedate, b.storecode, b.shape_And_size, b.price_band, 
  round(SUM(pull + raw)/30.0, 1) AS raw_pull_prev_30_avg,
  round(SUM(pull + raw + avail_total)/30.0, 1) AS owned_prev_30_avg
INTO #wtf2  
FROM dds.day a
LEFT JOIN ucinv_tmp_avail_5 b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1
WHERE a.thedate BETWEEN '01/31/2014' AND curdate() - 1
GROUP BY a.thedate, b.storecode, b.shape_and_size, b.price_band;  
*/
DROP TABLE ucinv_tmp_wtf_2;
CREATE TABLE ucinv_tmp_wtf_2 (
  thedate date,
  storecode cichar(3),
  shape_and_size cichar(24), 
  price_band cichar(10),
  raw_pull_prev_30_avg numeric(15,2),
  owned_prev_30_avg numeric(15,2)) IN database;
INSERT INTO ucinv_tmp_wtf_2  
SELECT a.thedate, b.storecode, b.shape_And_size, b.price_band, 
  round(SUM(pull + raw)/30.0, 1) AS raw_pull_prev_30_avg,
  round(SUM(pull + raw + avail_total)/30.0, 1) AS owned_prev_30_avg
-- INTO #wtf2  
FROM dds.day a
LEFT JOIN ucinv_tmp_avail_5 b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1
WHERE a.thedate BETWEEN '01/31/2014' AND curdate() - 1
GROUP BY a.thedate, b.storecode, b.shape_and_size, b.price_band;  

EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_wtf_2','ucinv_tmp_wtf_2.adi','thedate','thedate','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_wtf)2','ucinv_tmp_wtf_2.adi','storecode','storecode','',2,512,''); 
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_wtf_2','ucinv_tmp_wtf_2.adi','shape_and_size','shape_and_size','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_wtf_2','ucinv_tmp_wtf_2.adi','price_band','price_band','',2,512,''); 
 
 
/* move this up to initial TABLE def
ALTER TABLE ucinv_tmp_avail_6
ADD COLUMN raw_pull_prev_30_avg numeric(8,1) default '0' constraint NOT NULL 
ADD COLUMN owned_prev_30_avg numeric(8,1) default '0' constraint NOT NULL
ADD COLUMN turns_avail_fresh numeric(8,1) default '0' constraint NOT NULL
ADD COLUMN turns_raw_pull numeric(8,1) default '0' constraint NOT NULL 
ADD COLUMN turns_owned numeric(8,1) default '0' constraint NOT NULL;


UPDATE ucinv_tmp_avail_6
SET raw_pull_prev_30_avg = 0,
    owned_prev_30_avg = 0,
    turns_avail_fresh = 0,
    turns_raw_pull = 0,
    turns_owned = 0;
*/
-- ucinv_tmp_avail_6 UPDATE 3
UPDATE ucinv_tmp_avail_6
SET raw_pull_prev_30_avg = x.raw_pull_prev_30_avg,
    owned_prev_30_avg = x.owned_prev_30_avg
FROM (   
  SELECT b.*
  FROM ucinv_tmp_avail_6 a
  INNER JOIN #wtf2 b on a.thedate = b.thedate
    AND a.storecode = b.storecode
    AND a.shape_and_size = b.shape_and_size
    AND a.price_band = b.price_band) x
WHERE ucinv_tmp_avail_6.thedate = x.thedate
  AND ucinv_tmp_avail_6.storecode = x.storecode    
  AND ucinv_tmp_avail_6.shape_and_size = x.shape_and_size
  AND ucinv_tmp_avail_6.price_band = x.price_band; 

-- ucinv_tmp_avail_6 UPDATE 4  
UPDATE ucinv_tmp_avail_6
SET turns_avail_fresh =   
      CASE avail_fresh_prev_30_avg
        WHEN 0 THEN 0
        ELSE round(sales_prev_30_retail_avail_fresh/avail_fresh_prev_30_avg, 2)
      END ,
    turns_raw_pull = 
      CASE raw_pull_prev_30_avg
        WHEN 0 THEN 0
        ELSE round(sales_prev_30_retail_raw + sales_prev_30_retail_pulled/raw_pull_prev_30_avg, 2)
      END, 
    turns_owned = 
  CASE owned_prev_30_avg
    WHEN 0 THEN 0
    ELSE round(sales_prev_30_retail/owned_prev_30_avg, 2)
  END;             

DROP TABLE ucinv_tmp_avail_disposition;
CREATE TABLE ucinv_tmp_avail_disposition (
  thedate date constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  avail_fresh integer constraint NOT NULL,
  avail_aged integer constraint NOT NULL,
  avail_total integer constraint NOT NULL,
  sold_0_to_30 integer constraint NOT NULL,
  sold_31_to_60 integer constraint NOT NULL,
  sold_over_60 integer constraint NOT NULL,
  ws_from_avail integer constraint NOT NULL,
  constraint pk primary key (thedate,storecode)) IN database;
INSERT INTO ucinv_tmp_avail_disposition 
SELECT a.*, b.sold_0_to_30, b.sold_31_to_60, b.sold_over_60, b.ws_from_avail
  FROM (
  SELECT thedate, storecode, 
    SUM(CASE WHEN status = 'avail_fresh' THEN 1 ELSE 0 END) AS avail_fresh,
    SUM(CASE WHEN status = 'avail_aged' THEN 1 ELSE 0 END) AS avail_aged,
    COUNT(*) AS avail_total
  FROM ucinv_tmp_Avail_4
  WHERE status IN ('avail_fresh','avail_aged')
  GROUP BY thedate, storecode) a
LEFT JOIN (
  SELECT thedate, storecode,
    SUM(CASE WHEN disposition = 'sold 0-30' THEN 1 ELSE 0 END) AS sold_0_to_30,
    SUM(CASE WHEN disposition = 'sold 31-60' THEN 1 ELSE 0 END) AS sold_31_to_60,
    SUM(CASE WHEN disposition = 'sold > 60' THEN 1 ELSE 0 END) AS sold_over_60,
    SUM(CASE WHEN disposition = 'W/S from available' THEN 1 ELSE 0 END) AS ws_from_avail
  FROM ucinv_tmp_Avail_4
  WHERE status IN ('avail_fresh','avail_aged')
  GROUP BY thedate, storecode) b on a.storecode = b.storecode
    AND a.thedate = b.thedate;
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_disposition','ucinv_tmp_avail_disposition.adi','storecode','storecode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_disposition','ucinv_tmp_avail_disposition.adi','thedate','thedate','',2,512,'');        


DROP TABLE ucinv_tmp_sales_by_status;
CREATE TABLE ucinv_tmp_sales_by_status (
  thedate date constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  sales_total integer constraint NOT NULL,
  avail_fresh integer constraint NOT NULL,
  avail_aged integer constraint NOT NULL,
  pulled integer constraint NOT NULL,
  raw integer constraint NOT NULL,
  avail_fresh_perc numeric(8,2) constraint NOT NULL,
  avail_aged_perc numeric(8,2) constraint NOT NULL,
  pulled_perc numeric(8,2) constraint NOT NULL,
  raw_perc numeric(8,2) constraint NOT NULL,
  constraint pk primary key(thedate, storecode)) IN database;
INSERT INTO ucinv_tmp_sales_by_status
SELECT aa.*, coalesce(bb.sales_total, 0) AS sales_total, 
  coalesce(bb.avail_fresh, 0) AS avail_fresh, 
  coalesce(bb.avail_aged, 0) AS avail_aged,
  coalesce(bb.pulled, 0) AS pulled, coalesce(bb.raw, 0) AS raw,
  CASE 
    WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
    ELSE round(1.0 * coalesce(bb.avail_fresh, 0)/coalesce(bb.sales_total, 0), 2) 
  END AS avail_fresh_perc,
  CASE 
    WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
    ELSE round(1.0 * coalesce(bb.avail_aged, 0)/coalesce(bb.sales_total, 0), 2) 
  END AS avail_aged_perc,
  CASE 
    WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
    ELSE round(1.0 * coalesce(bb.pulled, 0)/coalesce(bb.sales_total, 0), 2) 
  END AS pulled_perc,
  CASE 
    WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
    ELSE round(1.0 * coalesce(bb.raw, 0)/coalesce(bb.sales_total, 0), 2) 
  END AS raw_perc      
FROM (
  SELECT thedate, 'RY1' AS storecode
  FROM dds.day 
  WHERE thedate BETWEEN '01/01/2014' AND curdatE() - 1
  UNION 
  SELECT thedate, 'RY2' AS storecode
  FROM dds.day 
  WHERE thedate BETWEEN '01/01/2014' AND curdatE() - 1) aa
LEFT JOIN (
  SELECT a.*, b.avail_fresh, b.avail_aged, b.pulled, b.raw
  FROM (
    SELECT thedate, storecode, COUNT(*) AS sales_total
    -- SELECT *
    FROM ucinv_tmp_avail_4
    WHERE thedate = sale_date
      AND sale_type = 'retail'
    GROUP BY thedate, storecode) a
  LEFT JOIN (
    SELECT thedate, storecode, 
      SUM(CASE WHEN status = 'avail_fresh' THEN 1 ELSE 0 END) AS avail_fresh,
      SUM(CASE WHEN status = 'avail_aged' THEN 1 ELSE 0 END) AS avail_aged,
      SUM(CASE WHEN status = 'pull' THEN 1 ELSE 0 END) AS pulled,
      SUM(CASE WHEN status = 'raw' THEN 1 ELSE 0 END) AS raw
    -- SELECT *
    FROM ucinv_tmp_avail_4
    WHERE thedate = sale_date
      AND sale_type = 'retail'
    GROUP BY thedate, storecode) b on a.thedate = b.thedate AND a.storecode = b.storecode) bb on aa.thedate = bb.thedate AND aa.storecode = bb.storecode collate ads_Default_ci
    
  
/* 
6/19/15  
unwinds: being disregarded for now
SELECT storecode, yearmonth, COUNT(*) FROM (
SELECT 
    CASE b.owninglocationid
    WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
    WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
    WHEN '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'RY3'
    ELSE 'XXX'
  END AS storecode,
  b.stocknumber, cast(a.soldts AS sql_date) ,
  c.yearmonth
FROM dps.vehicleSales a
INNER JOIN dps.VehicleInventoryItems b  on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN dds.day c on cast(a.soldts AS sql_date)  = c.thedate
  AND c.thedate > '12/31/2013'
WHERE a.status = 'VehicleSale_SaleCanceled'
) x GROUP BY storecode, yearmonth
*/

