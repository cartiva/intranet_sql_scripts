/*
-- which accounts
SELECT storecode, dl4, dl6, dl7, dl8, al2, al3, glaccount, gmcoa, page, line
FROM dds.zcb c 
WHERE c.name = 'gmfs gross'
  AND c.dl5 = 'used'
ORDER BY storecode, al2  
 
SELECT gtctl#, gtacct, SUM(gttamt)
FROM dds.stgArkonaGLPTRNS
WHERE gtacct IN ('124000', '124100')
  AND gtdate BETWEEN '04/01/2015' AND '04/30/2015' 
GROUP BY gtctl#, gtacct  

-- april 2015 
DROP TABLE #april;
SELECT storecode, dl4, dl6, dl7, dl8, al2, al3, glaccount, gmcoa, page, line, SUM(d.gttamt)
--INTO #april
-- SELECT *
FROM dds.zcb c 
LEFT JOIN dds.stgArkonaGLPTRNS d on c.glaccount = d.gtacct
  AND d.gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
WHERE c.name = 'gmfs gross'
AND c.dl5 = 'used'  
GROUP BY storecode, dl4, dl6, dl7, dl8, al2, al3, glaccount, gmcoa, page, line

-- april 2015 
-- the amounts are exactly correct, the counts are off
SELECT storecode, al3, glaccount, gmcoa, page, line, SUM(d.gttamt), COUNT(*)
FROM dds.zcb c 
LEFT JOIN dds.stgArkonaGLPTRNS d on c.glaccount = d.gtacct
  AND d.gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
WHERE c.name = 'gmfs gross'
  AND c.dl5 = 'used'  
  AND c.dl4 = 'sales'
  AND c.al2 = 'sales'
  AND c.storecode = 'ry1'
GROUP BY storecode, al3, glaccount, gmcoa, page, line

-- april 2015 
-- the amounts are exactly correct, the counts are off
SELECT storecode, al3, glaccount, gmcoa, page, line, d.gttamt, d.gtctl#
FROM dds.zcb c 
LEFT JOIN dds.stgArkonaGLPTRNS d on c.glaccount = d.gtacct
  AND d.gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
WHERE c.name = 'gmfs gross'
  AND c.dl5 = 'used'  
  AND c.dl4 = 'sales'
  AND c.al2 = 'sales'
  AND c.storecode = 'ry1'
ORDER BY glaccount, gtctl#  

-- aha, DO the counts after the grouping
-- much closer, 446b (uc other) still 2 high
-- jeri solved this one for me: 24069XXA amt IS positive, need to subtract 1
-- it IS NOT good enuf to NOT COUNT it (this seemed to make sense WHEN she
-- was explaining it but it currently makes my head hurt)
-- offsetting entries, LIKE 24409A can simply be NOT counted
SELECT storecode, al3, glaccount, gmcoa, page, line, SUM(gttamt), 
  SUM(CASE WHEN gttamt < 0 THEN 1 ELSE - 1 END) AS the_count
FROM (
  SELECT storecode, al3, glaccount, gmcoa, page, line, d.gtctl#, SUM(d.gttamt) AS gttamt
  FROM dds.zcb c 
  LEFT JOIN dds.stgArkonaGLPTRNS d on c.glaccount = d.gtacct
    AND d.gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
  WHERE c.name = 'gmfs gross'
    AND c.dl5 = 'used'  
    AND c.dl4 = 'sales'
    AND c.al2 = 'sales'
    AND c.storecode = 'ry1'
  GROUP BY storecode, al3, glaccount, gmcoa, page, line, d.gtctl# 
  HAVING SUM(d.gttamt) <> 0) x
GROUP BY storecode, al3, glaccount, gmcoa, page, line

SELECT storecode, al3, glaccount, gmcoa, page, line, d.gtctl#, SUM(d.gttamt) AS gttamt
FROM dds.zcb c 
LEFT JOIN dds.stgArkonaGLPTRNS d on c.glaccount = d.gtacct
  AND d.gtdate BETWEEN '04/01/2015' AND '04/30/2015'  
WHERE c.name = 'gmfs gross'
  AND c.dl5 = 'used'  
  AND c.dl4 = 'sales'
  AND c.al2 = 'sales'
  AND c.storecode = 'ry1'
  AND c.glaccount = '144601'
GROUP BY storecode, al3, glaccount, gmcoa, page, line, d.gtctl# 
ORDER BY CAST(LEFT(gtctl#, 5) AS sql_integer)


-- BY accout
DROP TABLE #byacct;
SELECT a.storecode, a.stocknumber, thedate, best_price, days_avail, status, 
  b.gtjrnl, b.gtacct, c.al3, SUM(gttamt) AS amt
INTO #byacct  
FROM ucinv_tmp_avail_4 a
LEFT JOIN dds.stgArkonaGLPTRNS b on a.stocknumber = b.gtctl#
INNER JOIN dds.zcb c on b.gtacct = c.glaccount
  AND c.name = 'gmfs gross'
  AND c.dl5 = 'used' 
WHERE thedate = sale_Date
GROUP BY a.storecode, a.stocknumber, thedate, best_price, days_avail, status, 
  b.gtjrnl, b.gtacct, c.al3

SELECT * 
FROM #byacct a 
LEFT JOIN (
  SELECT SUM(amt) AS gross
  FROM #byacct 
  WHERE stocknumber = '25097xx') b on 1 = 1
WHERE stocknumber = '25097XX'

select *
FROM dds.stgArkonaGLPTRNS
WHERE gtctl# = '25097xx'
  AND gtjrnl IN ('SVI', 'SWA', 'SCA')
  
select *
FROM dds.stgArkonaGLPTRNS
WHERE gtctl# = '25097xx'
  AND gtacct IN ('124000','124100')  
  
DROP TABLE #gross;  
SELECT a.storecode, a.thedate, a.stocknumber, best_price, days_avail, status, SUM(gttamt) AS gross
INTO #gross
FROM ucinv_tmp_avail_4 a
LEFT JOIN dds.stgArkonaGLPTRNS b on a.stocknumber = b.gtctl#
INNER JOIN dds.zcb c on b.gtacct = c.glaccount
  AND c.name = 'gmfs gross'
  AND c.dl5 = 'used'
WHERE thedate = sale_Date
GROUP BY a.storecode, a.thedate, a.stocknumber, best_price, days_avail, status

SELECT a.*, b.vehicleprice, b.vehiclecost, b.vehicleprice - b.vehiclecost, c.*
FROM #gross a
LEFT JOIN dds.factvehiclesale b on a.stocknumber = b.stocknumber
LEFT JOIN #byacct c on a.stocknumber = c.stocknumber 
WHERE a.stocknumber = '24561xx'

SELECT a.storecode, a.thedate, a.stocknumber, a.gross, b.vehicleprice, b.vehiclecost, 
  b.vehicleprice - b.vehiclecost AS deal,
  c.gtjrnl, c.gtacct, c.al3, c.expr
FROM #gross a 
LEFT join dds.factvehiclesale b on a.stocknumber = b.stocknumber
LEFT JOIN #byacct c on a.stocknumber = c.stocknumber 


-- april financial statement
select c.thedate, a.stocknumber, d.*
-- SELECT COUNT(*)
FROM dds.factvehiclesale a
INNER JOIN dds.dimcardealinfo b on a.cardealinfokey = b.cardealinfokey
  AND b.vehicletypecode = 'u'
  AND b.saletypecode = 'r'
INNER JOIN dds.day c on a.cappeddatekey = c.datekey
  AND c.yearmonth = 201504
LEFT JOIN #byacct d on a.stocknumber = d.stocknumber 
WHERE gtacct = '144601'
ORDER BY gtacct, left(a.stocknumber, 5) 

SELECT gtacct, SUM(amt), COUNT(*)
FROM (
select c.thedate, a.stocknumber, d.*
-- SELECT COUNT(*)
FROM dds.factvehiclesale a
INNER JOIN dds.dimcardealinfo b on a.cardealinfokey = b.cardealinfokey
  AND b.vehicletypecode = 'u'
  AND b.saletypecode = 'r'
INNER JOIN dds.day c on a.cappeddatekey = c.datekey
  AND c.yearmonth = 201504
LEFT JOIN #byacct d on a.stocknumber = d.stocknumber 
) x GROUP BY gtacct


SELECT *
FROM #gross a
LEFT JOIN dps.vehiclecosts b on a.stocknumber = b.stocknumber
WHERE a.storecode = 'ry1'
SELECT *
FROM #byacct

SELECT * 
FROM #april
WHERE storecode = 'ry1'


SELECT * 
FROM #byacct a 
LEFT JOIN (
  SELECT SUM(amt) AS gross
  FROM #byacct 
  WHERE stocknumber = '25097xx') b on 1 = 1
WHERE stocknumber = '25097XX'

select *
FROM dds.stgArkonaGLPTRNS
WHERE gtctl# = '25097xx'
  AND gtjrnl IN ('SVI', 'SWA', 'SCA')
  
SELECT *
FROM #gross
  

-- maybe this help at getting to service gross for a uc

SELECT a.stocknumber, a.saledate, left(a.saletype, 8),a.vehiclecost, 
  b.gtdate, b.gtacct, b.gtjrnl, b.gtdoc#, b.gtdesc, b.gttamt, c.gmdesc
-- SELECT *
FROM ucinv_tmp_sales a
LEFT JOIN dds.stgArkonaGLPTRNS b on a.stocknumber = b.gtctl#
LEFT JOIN dds.stgArkonaGLPMAST c on b.gtacct = c.gmacct
  AND c.gmyear = 2015
WHERE a.storecode = 'ry1'
  AND a.saledate = '04/16/2015'
ORDER BY a.stocknumber, b.gtacct    


SELECT gtctl#, SUM(gttamt)
FROM dds.stgArkonaGLPTRNS
WHERE gtctl# = '25317XX'
GROUP BY gtctl#

SELECT * FROM dps.vehiclecosts WHERE stocknumber = '25317XX'
ALL recon done after sale

-- look at this one compared to dps.vehiclecosts
--SELECT gtacct, gmdesc, SUM(gttamt) FROM (
SELECT page,line,al2, SUM(gttamt) FROM (
SELECT a.stocknumber, a.saledate, left(a.saletype, 8),a.vehiclecost, 
  b.gtdate, b.gtacct, b.gtjrnl, b.gtdoc#, b.gtdesc, b.gttamt, c.gmdesc, c.gmtype,
  al2, al3, glaccount, gmcoa, page, line
-- SELECT *
FROM ucinv_tmp_sales a
LEFT JOIN dds.stgArkonaGLPTRNS b on a.stocknumber = b.gtctl#
LEFT JOIN dds.stgArkonaGLPMAST c on b.gtacct = c.gmacct
  AND c.gmyear = 2015
INNER JOIN dds.zcb d on c.gmacct = d.glaccount 
  AND d.name = 'gmfs gross'
  AND d.dl5 = 'used'  
WHERE a.stocknumber = '25317XX'  
--) x GROUP BY gtacct, gmdesc, gmtype
) x GROUP BY page,line,al2


why IS 312.97 IN recon to vsu::164700
AND the rest IS svi::164700
because the 312.97 IS dated the sale date, ALL the rest IS after sale date


SELECT *
FROM ucinv_tmp_sales
WHERE saleDate > '01/01/2015'

SELECT storecode, yearmonth, page,line,
  SUM(CASE WHEN al2 = 'COGS' THEN gttamt ELSE 0 END) AS COGS,
  SUM(CASE WHEN al2 = 'Sales' THEN gttamt ELSE 0 END) AS Sales
FROM (  
SELECT aa.yearmonth, a.storecode, a.stocknumber, a.saledate, left(a.saletype, 8),a.vehiclecost, 
  b.gtdate, b.gtacct, b.gtjrnl, b.gtdoc#, b.gtdesc, b.gttamt, c.gmdesc, c.gmtype,
  al2, al3, glaccount, gmcoa, page, line
-- SELECT *
FROM ucinv_tmp_sales a
INNER JOIN dds.day aa on a.saledate = aa.thedate
LEFT JOIN dds.stgArkonaGLPTRNS b on a.stocknumber = b.gtctl#
LEFT JOIN dds.stgArkonaGLPMAST c on b.gtacct = c.gmacct
  AND c.gmyear = 2015
INNER JOIN dds.zcb d on c.gmacct = d.glaccount 
  AND d.name = 'gmfs gross'
  AND d.dl5 = 'used'  
WHERE a.saleDate > '01/01/2015'
) X GROUP BY storecode, yearmonth, page, line

some vehicles with multiple sales account entries, corrections (C/E), cert <=> non-cert,
  change sale amt, change sale date
shooting for looking at sales acct entries AS the sale date
SELECT stocknumber FROM (
SELECT aa.yearmonth, a.storecode, a.stocknumber, a.saledate, left(a.saletype, 8),a.vehiclecost, 
  b.gtdate, b.gtacct, b.gtjrnl, b.gtdoc#, b.gtdesc, b.gttamt, c.gmdesc, c.gmtype,
  al2, al3, glaccount, gmcoa, page, line
-- SELECT *
FROM ucinv_tmp_sales a
INNER JOIN dds.day aa on a.saledate = aa.thedate
LEFT JOIN dds.stgArkonaGLPTRNS b on a.stocknumber = b.gtctl#
LEFT JOIN dds.stgArkonaGLPMAST c on b.gtacct = c.gmacct
  AND c.gmyear = 2015
INNER JOIN dds.zcb d on c.gmacct = d.glaccount 
  AND d.name = 'gmfs gross'
  AND d.dl5 = 'used'  
  AND d.al2 = 'Sales'
  AND d.dl4 = 'Sales'
WHERE a.saleDate > '01/01/2015'
) x GROUP BY stocknumber HAVING COUNT(*) > 1

SELECT aa.yearmonth, a.storecode, a.stocknumber, a.saledate, left(a.saletype, 8),a.vehiclecost, 
  b.gtdate, b.gtacct, b.gtjrnl, b.gtdoc#, b.gtdesc, b.gttamt, c.gmdesc, c.gmtype,
  dl4, al2, al3, glaccount, gmcoa, page, line
-- SELECT *
FROM ucinv_tmp_sales a
INNER JOIN dds.day aa on a.saledate = aa.thedate
LEFT JOIN dds.stgArkonaGLPTRNS b on a.stocknumber = b.gtctl#
LEFT JOIN dds.stgArkonaGLPMAST c on b.gtacct = c.gmacct
  AND c.gmyear = 2015
INNER JOIN dds.zcb d on c.gmacct = d.glaccount 
  AND d.name = 'gmfs gross'
  AND d.dl5 = 'used'  
  AND d.al2 = 'Sales'
WHERE a.stocknumber IN (
  SELECT stocknumber FROM (
  SELECT aa.yearmonth, a.storecode, a.stocknumber, a.saledate, left(a.saletype, 8),a.vehiclecost, 
    b.gtdate, b.gtacct, b.gtjrnl, b.gtdoc#, b.gtdesc, b.gttamt, c.gmdesc, c.gmtype,
    al2, al3, glaccount, gmcoa, page, line
  -- SELECT *
  FROM ucinv_tmp_sales a
  INNER JOIN dds.day aa on a.saledate = aa.thedate
  LEFT JOIN dds.stgArkonaGLPTRNS b on a.stocknumber = b.gtctl#
  LEFT JOIN dds.stgArkonaGLPMAST c on b.gtacct = c.gmacct
    AND c.gmyear = 2015
  INNER JOIN dds.zcb d on c.gmacct = d.glaccount 
    AND d.name = 'gmfs gross'
    AND d.dl5 = 'used'  
    AND d.al2 = 'Sales'
    AND d.dl4 = 'Sales'
  WHERE a.saleDate > '01/01/2015'
  ) x GROUP BY stocknumber HAVING COUNT(*) > 1)
AND dl4 = 'sales'


ok narrowing glptrns to sales accounts looks pretty good for sale amount
the problem for sale date IS WHEN there are multiple postings to the sales account
(unwinds, corrections) i DO NOT know how to discern which posting represents the
sale date
*/
-- each transaction
DROP TABLE ucinv_tmp_gross_1;
CREATE TABLE ucinv_tmp_gross_1 (
  the_date date constraint NOT NULL,
  stocknumber cichar(20) constraint NOT NULL,
  account cichar(10) constraint NOT NULL,
  account_type cichar(12) constraint NOT NULL,
  department cichar(12) constraint NOT NULL,
  amount integer constraint NOT NULL) IN database;  
INSERT INTO ucinv_tmp_gross_1
SELECT a.gtdate, a.gtctl#, a.gtacct, left(al2,12), left(dl4,12), 
  -1 * cast(round(coalesce(gttamt, 0), 0) as sql_integer) as gttamt
FROM dds.stgarkonaglptrns a
INNER JOIN dds.zcb d on a.gtacct = d.glaccount 
  AND d.name = 'gmfs gross'
  AND d.dl5 = 'used'  
--  AND d.al2 = 'Sales'
--  AND d.dl4 = 'Sales'
WHERE gtdate > '01/01/2012';
--GROUP BY a.gtdate, a.gtctl#, a.gtacct  
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_gross_1','ucinv_tmp_gross_1.adi','the_date','the_date','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_gross_1','ucinv_tmp_gross_1.adi','stocknumber','stocknumber','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_gross_1','ucinv_tmp_gross_1.adi','account','account','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_gross_1','ucinv_tmp_gross_1.adi','department','department','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_gross_1','ucinv_tmp_gross_1.adi','account_type','account_type','',2,512,'');

DROP TABLE ucinv_tmp_gross_2;
CREATE TABLE ucinv_tmp_gross_2 (
  stocknumber cichar(20) constraint NOT NULL, 
  sale_amount integer constraint NOT NULL,
  cost_of_sale integer constraint NOT NULL,
  sales_gross integer constraint NOT NULL,
  fi_sale_amount integer constraint NOT NULL,
  fi_cost integer constraint NOT NULL,
  fi_gross integer constraint NOT NULL,
  constraint pk primary key (stocknumber)) IN database; 
INSERT INTO ucinv_tmp_gross_2
SELECT stocknumber, 
  SUM(CASE WHEN department = 'sales' and account_type = 'sales' THEN amount ELSE 0 END) AS sale_amount, 
  SUM(CASE WHEN department = 'sales' and account_type = 'COGS' THEN amount ELSE 0 END) AS cost_of_sale,
  SUM(CASE WHEN department = 'sales' THEN amount ELSE 0 END) AS sales_gross,
  SUM(CASE WHEN department = 'f&i' and account_type = 'sales' then amount else 0 end) as fi_sale_amount,
  SUM(CASE WHEN department = 'f&i' and account_type = 'COGS' then amount else 0 end) as fi_cost,
  SUM(CASE WHEN department = 'f&i' THEN amount ELSE 0 END) AS fi_gross
-- SELECT *  
FROM ucinv_tmp_gross_1 
GROUP BY stocknumber;  

--SELECT *
--FROM ucinv_tmp_Gross_2


/*  
  ALL this fucking sale date shit IS NOT getting better, just DO front & back
  END gross for now, get the fucking sale date shit later
SELECT *
FROM ucinv_tmp_gross_1
WHERE stocknumber IN (
  SELECT stocknumber
  FROM ucinv_tmp_gross_1
  GROUP BY stocknumber 
  HAVING COUNT(*) > 1) 
  ORDER BY stocknumber

SELECT the_date, stocknumber, account, SUM(amount) AS amount
FROM ucinv_tmp_gross_1
WHERE stocknumber IN (
  SELECT stocknumber
  FROM ucinv_tmp_gross_1
  GROUP BY stocknumber 
  HAVING COUNT(*) > 1) 
GROUP BY the_date, stocknumber, account
HAVING SUM(amount) <> 0 
ORDER BY stocknumber

SELECT *
FROM ucinv_tmp_gross_1 a
LEFT JOIN (
SELECT b.thedate AS capdate, a.stocknumber
FROM dds.factVehicleSale a
INNER JOIN dds.day b on a.cappeddatekey = b.datekey) b on a.stocknumber = b.stocknumber
--WHERE month(a.the_date) <> month(b.capdate)
ORDER BY a.stocknumber
*/
/*
SELECT yearmonth, storecode,  
  SUM(CASE WHEN days_avail BETWEEN 0 AND 14 THEN gross ELSE 0 END) AS "0-14 Gross",
  SUM(CASE WHEN days_avail BETWEEN 0 AND 14 THEN 1 ELSE 0 END) AS "0-14 Units",
  CASE 
    WHEN SUM(CASE WHEN days_avail BETWEEN 0 AND 14 THEN 1 ELSE 0 END) = 0 THEN 0
  ELSE   
    round(SUM(CASE WHEN days_avail BETWEEN 0 AND 14 THEN gross ELSE 0 END)/SUM(CASE WHEN days_avail BETWEEN 0 AND 14 THEN 1 ELSE 0 END), 0) 
  END AS Gross_Per,
  SUM(CASE WHEN days_avail BETWEEN 15 AND 28 THEN gross ELSE 0 END) AS "15-28 Gross",
  SUM(CASE WHEN days_avail BETWEEN 15 AND 28 THEN 1 ELSE 0 END) AS "15-28 Units", 
  CASE 
    WHEN SUM(CASE WHEN days_avail BETWEEN 15 AND 28 THEN 1 ELSE 0 END) = 0 THEN 0
  ELSE 
    round(SUM(CASE WHEN days_avail BETWEEN 15 AND 28 THEN gross ELSE 0 END)/SUM(CASE WHEN days_avail BETWEEN 15 AND 28 THEN 1 ELSE 0 END), 0) 
  END AS Gross_Per,
  SUM(CASE WHEN days_avail BETWEEN 29 AND 60 THEN gross ELSE 0 END) AS "29-60 Gross",
  SUM(CASE WHEN days_avail BETWEEN 29 AND 60 THEN 1 ELSE 0 END) AS "29-60 Units",
  CASE 
    WHEN SUM(CASE WHEN days_avail BETWEEN 29 AND 60 THEN 1 ELSE 0 END) = 0 THEN 0
  ELSE 
    round(SUM(CASE WHEN days_avail BETWEEN 29 AND 60 THEN gross ELSE 0 END)/SUM(CASE WHEN days_avail BETWEEN 29 AND 60 THEN 1 ELSE 0 END), 0) 
  END AS Gross_Per,
  SUM(CASE WHEN days_avail > 60 THEN gross ELSE 0 END) AS "Over 60 Gross",  
  SUM(CASE WHEN days_avail > 60 THEN 1 ELSE 0 END) AS "Over 60 Units",
  CASE 
    WHEN SUM(CASE WHEN days_avail > 60 THEN 1 ELSE 0 END) = 0 THEN 0
  ELSE 
    round(SUM(CASE WHEN days_avail > 60 THEN gross ELSE 0 END)/SUM(CASE WHEN days_avail > 60 THEN 1 ELSE 0 END), 0) 
  END AS Gross_Per
FROM (
SELECT a.*, coalesce(b.gross, 0) AS gross, d.yearmonth
FROM ucinv_tmp_avail_4 a
LEFT JOIN (
  SELECT stocknumber, -1*(round(SUM(amount), 0)) AS gross
  FROM ucinv_tmp_gross_1 a
  GROUP BY stocknumber) b on a.stocknumber = b.stocknumber
--LEFT JOIN dps.vehiclecosts c on a.stocknumber = c.stocknumber  
LEFT JOIN dds.day d on a.thedate = d.thedate
WHERE a.disposition <> 'NA'
  AND year(a.thedate) > 2013
  AND a.thedate = sale_date
  AND a.sale_Type = 'Retail'
) x where yearmonth < 201507 GROUP BY yearmonth, storecode
ORDER BY storecode, yearmonth
*/