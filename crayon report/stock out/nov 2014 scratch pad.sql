SELECT a.thedate, a.stocknumber, a.priceband, 
  e.description AS shape, f.description AS size,
  CASE 
    WHEN right(TRIM(a.stocknumber), 1) IN ('a','b','c','d','e') THEN 'trade'
    ELSE 'purchase'
  END AS source
FROM ucinv_price_by_day a
LEFT JOIN dps.VehicleInventoryItems b on a.stocknumber = b.stocknumber
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN dps.MakeModelClassifications d on c.make = d.make
  AND c.model = d.model
INNER JOIN dps.typDescriptions e on d.vehicleType = e.typ
INNER JOIN dps.typDescriptions f on d.vehicleSegment = f.typ
LEFT JOIN ucinv_available g on a.stocknumber = g.stocknumber
LEFT JOIN ucinv_pulled h on a.stocknumber = h.stocknumber
  