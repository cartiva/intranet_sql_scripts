-- ADD recon sales
SELECT sale_date, (year(sale_date) * 100) + month(sale_date) AS year_month, 
  sale_type, sale_amount, sales_gross
-- SELECT *
FROM ucinv_tmp_sales_activity
WHERE storecode = 'ry1'
  AND sale_type = 'retail'
  AND year(sale_Date) = 2014

  
 
SELECT year_month, COUNT(*) AS _the_count, SUM(sales_gross)/COUNT(*) AS pvr,
  SUM(recon_sales) AS recon
FROM (  
  SELECT sale_date, (year(sale_date) * 100) + month(sale_date) AS year_month, 
    sale_type, sale_amount, sales_gross, recon_sales
  -- SELECT *
  FROM ucinv_tmp_sales_activity
  WHERE storecode = 'ry1'
    AND sale_type = 'retail'
    AND sale_date < '10/01/2015'
    AND year(sale_Date) = 2014) x  
GROUP BY year_month    


SELECT year_month, price_band, COUNT(*) AS the_count, 
  SUM(sales_gross)/COUNT(*) AS pvr, 
  SUM(recon_sales)/COUNT(*) AS recon_per_vehicle
FROM (  
  SELECT sale_date, (year(sale_date) * 100) + month(sale_date) AS year_month, 
    sale_type, 
    CASE
      WHEN sale_amount BETWEEN 0 AND 9999 THEN '0 - 10k'
      WHEN sale_amount BETWEEN 10000 AND 14999 THEN '10 - 15k'
      WHEN sale_amount BETWEEN 15000 AND 19999 THEN '15 - 20k'
      WHEN sale_amount BETWEEN 20000 AND 29999 THEN '20 - 30k'
      WHEN sale_amount BETWEEN 30000 AND 34999 THEN '30 - 35k'
      ELSE '35k +'
    END AS price_band,
    sales_gross, recon_sales
  -- SELECT *
  FROM ucinv_tmp_sales_activity
  WHERE storecode = 'ry1'
    AND sale_type = 'retail'
    AND sale_date < '10/01/2015'
    AND year(sale_Date) = 2014) x  
GROUP BY year_month, price_band    



SELECT price_band,
  max(CASE WHEN year_month = 201401 THEN the_count end) as "Jan 14 Sold", 
  max(CASE WHEN year_month = 201401 THEN pvr end) as "Jan 14 PVR",
  max(CASE WHEN year_month = 201401 THEN recon_per_veh end) as "Jan 14 Recon Per Veh",
  max(CASE WHEN year_month = 201402 THEN the_count end) as "Feb 14 Sold", 
  max(CASE WHEN year_month = 201402 THEN pvr end) as "Feb 14 PVR",
  max(CASE WHEN year_month = 201402 THEN recon_per_veh end) as "Feb 14 Recon Per Veh",
  max(CASE WHEN year_month = 201403 THEN the_count end) as "Mar 14 Sold", 
  max(CASE WHEN year_month = 201403 THEN pvr end) as "Mar 14 PVR",
  max(CASE WHEN year_month = 201403 THEN recon_per_veh end) as "Mar 14 Recon Per Veh",
  max(CASE WHEN year_month = 201404 THEN the_count end) as "Apr 14 Sold", 
  max(CASE WHEN year_month = 201404 THEN pvr end) as "Apr 14 PVR",
  max(CASE WHEN year_month = 201404 THEN recon_per_veh end) as "Apr 14 Recon Per Veh",
  max(CASE WHEN year_month = 201405 THEN the_count end) as "May 14 Sold", 
  max(CASE WHEN year_month = 201405 THEN pvr end) as "May 14 PVR",
  max(CASE WHEN year_month = 201405 THEN recon_per_veh end) as "May 14 Recon Per Veh",
  max(CASE WHEN year_month = 201406 THEN the_count end) as "Jun 14 Sold", 
  max(CASE WHEN year_month = 201406 THEN pvr end) as "Jun 14 PVR",
  max(CASE WHEN year_month = 201406 THEN recon_per_veh end) as "Jun 14 Recon Per Veh",
  max(CASE WHEN year_month = 201407 THEN the_count end) as "Jul 14 Sold", 
  max(CASE WHEN year_month = 201407 THEN pvr end) as "Jul 14 PVR",
  max(CASE WHEN year_month = 201407 THEN recon_per_veh end) as "Jul 14 Recon Per Veh",
  max(CASE WHEN year_month = 201408 THEN the_count end) as "Aug 14 Sold", 
  max(CASE WHEN year_month = 201408 THEN pvr end) as "Aug 14 PVR",
  max(CASE WHEN year_month = 201408 THEN recon_per_veh end) as "Aug 14 Recon Per Veh",
  max(CASE WHEN year_month = 201409 THEN the_count end) as "Sep 14 Sold", 
  max(CASE WHEN year_month = 201409 THEN pvr end) as "Sep 14 PVR",
  max(CASE WHEN year_month = 201409 THEN recon_per_veh end) as "Sep 14 Recon Per Veh",
  max(CASE WHEN year_month = 201410 THEN the_count end) as "Oct 14 Sold", 
  max(CASE WHEN year_month = 201410 THEN pvr end) as "Oct 14 PVR",
  max(CASE WHEN year_month = 201410 THEN recon_per_veh end) as "Oct 14 Recon Per Veh",
  max(CASE WHEN year_month = 201411 THEN the_count end) as "Nov 14 Sold", 
  max(CASE WHEN year_month = 201411 THEN pvr end) as "Nov 14 PVR",
  max(CASE WHEN year_month = 201411 THEN recon_per_veh end) as "Nov 14 Recon Per Veh",
  max(CASE WHEN year_month = 201412 THEN the_count end) as "Dec 14 Sold", 
  max(CASE WHEN year_month = 201412 THEN pvr end) as "Dec 14 PVR",  
  max(CASE WHEN year_month = 201412 THEN recon_per_veh end) as "Dec 14 Recon Per Veh"                    
FROM (   
  SELECT year_month, price_band, COUNT(*) AS the_count, 
    SUM(sales_gross)/COUNT(*) AS pvr, 
    SUM(recon_sales)/COUNT(*) AS recon_per_veh
  FROM (  
    SELECT sale_date, (year(sale_date) * 100) + month(sale_date) AS year_month, 
      sale_type, 
      CASE
        WHEN sale_amount BETWEEN 0 AND 9999 THEN '0 - 10k'
        WHEN sale_amount BETWEEN 10000 AND 14999 THEN '10 - 15k'
        WHEN sale_amount BETWEEN 15000 AND 19999 THEN '15 - 20k'
        WHEN sale_amount BETWEEN 20000 AND 29999 THEN '20 - 30k'
        WHEN sale_amount BETWEEN 30000 AND 34999 THEN '30 - 35k'
        ELSE '35k +'
      END AS price_band,
      sales_gross, recon_sales
    -- SELECT *
    FROM ucinv_tmp_sales_activity
    WHERE storecode = 'ry1'
      AND sale_type = 'retail'
      AND sale_date < '10/01/2015'
      AND year(sale_Date) = 2014) x  
  GROUP BY year_month, price_band ) y   
GROUP BY price_band




SELECT price_band,
  max(CASE WHEN year_month = 201501 THEN the_count end) as "Jan 15 Sold", 
  max(CASE WHEN year_month = 201501 THEN pvr end) as "Jan 15 PVR",
  max(CASE WHEN year_month = 201501 THEN recon_per_veh end) as "Jan 15 Recon Per Veh",
  max(CASE WHEN year_month = 201502 THEN the_count end) as "Feb 15 Sold", 
  max(CASE WHEN year_month = 201502 THEN pvr end) as "Feb 15 PVR",
  max(CASE WHEN year_month = 201502 THEN recon_per_veh end) as "Feb 15 Recon Per Veh",
  max(CASE WHEN year_month = 201503 THEN the_count end) as "Mar 15 Sold", 
  max(CASE WHEN year_month = 201503 THEN pvr end) as "Mar 15 PVR",
  max(CASE WHEN year_month = 201503 THEN recon_per_veh end) as "Mar 15 Recon Per Veh",
  max(CASE WHEN year_month = 201504 THEN the_count end) as "Apr 15 Sold", 
  max(CASE WHEN year_month = 201504 THEN pvr end) as "Apr 15 PVR",
  max(CASE WHEN year_month = 201504 THEN recon_per_veh end) as "Apr 15 Recon Per Veh",
  max(CASE WHEN year_month = 201505 THEN the_count end) as "May 15 Sold", 
  max(CASE WHEN year_month = 201505 THEN pvr end) as "May 15 PVR",
  max(CASE WHEN year_month = 201505 THEN recon_per_veh end) as "May 15 Recon Per Veh",
  max(CASE WHEN year_month = 201506 THEN the_count end) as "Jun 15 Sold", 
  max(CASE WHEN year_month = 201506 THEN pvr end) as "Jun 15 PVR",
  max(CASE WHEN year_month = 201506 THEN recon_per_veh end) as "Jun 15 Recon Per Veh",
  max(CASE WHEN year_month = 201507 THEN the_count end) as "Jul 15 Sold", 
  max(CASE WHEN year_month = 201507 THEN pvr end) as "Jul 15 PVR",
  max(CASE WHEN year_month = 201507 THEN recon_per_veh end) as "Jul 15 Recon Per Veh",
  max(CASE WHEN year_month = 201508 THEN the_count end) as "Aug 15 Sold", 
  max(CASE WHEN year_month = 201508 THEN pvr end) as "Aug 15 PVR",
  max(CASE WHEN year_month = 201508 THEN recon_per_veh end) as "Aug 15 Recon Per Veh",
  max(CASE WHEN year_month = 201509 THEN the_count end) as "Sep 15 Sold", 
  max(CASE WHEN year_month = 201509 THEN pvr end) as "Sep 15 PVR",
  max(CASE WHEN year_month = 201509 THEN recon_per_veh end) as "Sep 15 Recon Per Veh",
  max(CASE WHEN year_month = 201510 THEN the_count end) as "Oct 15 Sold", 
  max(CASE WHEN year_month = 201510 THEN pvr end) as "Oct 15 PVR",
  max(CASE WHEN year_month = 201510 THEN recon_per_veh end) as "Oct 15 Recon Per Veh",
  max(CASE WHEN year_month = 201511 THEN the_count end) as "Nov 15 Sold", 
  max(CASE WHEN year_month = 201511 THEN pvr end) as "Nov 15 PVR",
  max(CASE WHEN year_month = 201511 THEN recon_per_veh end) as "Nov 15 Recon Per Veh",
  max(CASE WHEN year_month = 201512 THEN the_count end) as "Dec 15 Sold", 
  max(CASE WHEN year_month = 201512 THEN pvr end) as "Dec 15 PVR",  
  max(CASE WHEN year_month = 201512 THEN recon_per_veh end) as "Dec 15 Recon Per Veh"                    
FROM (   
  SELECT year_month, price_band, COUNT(*) AS the_count, 
    SUM(sales_gross)/COUNT(*) AS pvr, 
    SUM(recon_sales)/COUNT(*) AS recon_per_veh
  FROM (  
    SELECT sale_date, (year(sale_date) * 100) + month(sale_date) AS year_month, 
      sale_type, 
      CASE
        WHEN sale_amount BETWEEN 0 AND 9999 THEN '0 - 10k'
        WHEN sale_amount BETWEEN 10000 AND 15999 THEN '10 - 15k'
        WHEN sale_amount BETWEEN 15000 AND 19999 THEN '15 - 20k'
        WHEN sale_amount BETWEEN 20000 AND 29999 THEN '20 - 30k'
        WHEN sale_amount BETWEEN 30000 AND 34999 THEN '30 - 35k'
        ELSE '35k +'
      END AS price_band,
      sales_gross, recon_sales
    -- SELECT *
    FROM ucinv_tmp_sales_activity
    WHERE storecode = 'ry1'
      AND sale_type = 'retail'
      AND sale_date < '10/01/2015'
      AND year(sale_Date) = 2015) x  
  GROUP BY year_month, price_band ) y   
GROUP BY price_band


-- same thing for honda
-- except he (john o) wants front & back pvr

SELECT price_band,
  max(CASE WHEN year_month = 201401 THEN the_count end) as "Jan 14 Sold", 
  max(CASE WHEN year_month = 201401 THEN sales_pvr end) as "Jan 15 Sales PVR",
  max(CASE WHEN year_month = 201401 THEN fi_pvr end) as "Jan 14 FI PVR",
  max(CASE WHEN year_month = 201402 THEN the_count end) as "Feb 14 Sold", 
  max(CASE WHEN year_month = 201402 THEN sales_pvr end) as "Feb 14 Sales PVR",
  max(CASE WHEN year_month = 201402 THEN fi_pvr end) as "Feb 14 FI PVR",
  max(CASE WHEN year_month = 201403 THEN the_count end) as "Mar 14 Sold", 
  max(CASE WHEN year_month = 201403 THEN sales_pvr end) as "Mar 14 Sales PVR",
  max(CASE WHEN year_month = 201403 THEN fi_pvr end) as "Mar 14 FI PVR",
  max(CASE WHEN year_month = 201404 THEN the_count end) as "Apr 14 Sold", 
  max(CASE WHEN year_month = 201404 THEN sales_pvr end) as "Apr 14 Sales PVR",
  max(CASE WHEN year_month = 201404 THEN fi_pvr end) as "Apr 14 FI PVR",
  max(CASE WHEN year_month = 201405 THEN the_count end) as "May 14 Sold", 
  max(CASE WHEN year_month = 201405 THEN sales_pvr end) as "May 14 Sales PVR",
  max(CASE WHEN year_month = 201405 THEN fi_pvr end) as "May 14 FI PVR",
  max(CASE WHEN year_month = 201406 THEN the_count end) as "Jun 14 Sold", 
  max(CASE WHEN year_month = 201406 THEN sales_pvr end) as "Jun 14 Sales PVR",
  max(CASE WHEN year_month = 201406 THEN fi_pvr end) as "Jun 14 FI PVR",
  max(CASE WHEN year_month = 201407 THEN the_count end) as "Jul 14 Sold", 
  max(CASE WHEN year_month = 201407 THEN sales_pvr end) as "Jul 14 Sales PVR",
  max(CASE WHEN year_month = 201407 THEN fi_pvr end) as "Jul 14 FI PVR",
  max(CASE WHEN year_month = 201408 THEN the_count end) as "Aug 14 Sold", 
  max(CASE WHEN year_month = 201408 THEN sales_pvr end) as "Aug 14 Sales PVR",
  max(CASE WHEN year_month = 201408 THEN fi_pvr end) as "Aug 14 FI PVR",
  max(CASE WHEN year_month = 201409 THEN the_count end) as "Sep 14 Sold", 
  max(CASE WHEN year_month = 201409 THEN sales_pvr end) as "Sep 14 Sales PVR",
  max(CASE WHEN year_month = 201409 THEN fi_pvr end) as "Sep 14 FI PVR",
  max(CASE WHEN year_month = 201410 THEN the_count end) as "Oct 14 Sold", 
  max(CASE WHEN year_month = 201410 THEN sales_pvr end) as "Oct 14 Sales PVR",
  max(CASE WHEN year_month = 201410 THEN fi_pvr end) as "Oct 14 FI PVR",
  max(CASE WHEN year_month = 201411 THEN the_count end) as "Nov 14 Sold", 
  max(CASE WHEN year_month = 201411 THEN sales_pvr end) as "Nov 14 Sales PVR",
  max(CASE WHEN year_month = 201411 THEN fi_pvr end) as "Nov 14 FI PVR",
  max(CASE WHEN year_month = 201412 THEN the_count end) as "Dec 14 Sold", 
  max(CASE WHEN year_month = 201412 THEN sales_pvr end) as "Dec 14 Sales PVR",  
  max(CASE WHEN year_month = 201412 THEN fi_pvr end) as "Dec 14 FI PVR"    
FROM (  
  SELECT year_month, price_band, COUNT(*) AS the_count, 
    SUM(sales_gross)/COUNT(*) AS sales_pvr, 
    SUM(fi_gross)/COUNT(*) AS fi_pvr
  FROM (  
    SELECT sale_date, (year(sale_date) * 100) + month(sale_date) AS year_month, 
      sale_type, 
      CASE
        WHEN sale_amount BETWEEN 0 AND 5999 THEN '0 - 6k'
        WHEN sale_amount BETWEEN 6000 AND 9999 THEN '06 - 10k'
        WHEN sale_amount BETWEEN 10000 AND 13999 THEN '10 - 14k'
        WHEN sale_amount BETWEEN 14000 AND 19999 THEN '14 - 20k'
        WHEN sale_amount BETWEEN 20000 AND 25999 THEN '20 - 26k'
        ELSE '26k +'
      END AS price_band,
      sales_gross, fi_gross
    FROM ucinv_tmp_sales_Activity
    WHERE storecode = 'ry2'
      AND sale_type = 'retail'
      AND sale_date < '11/01/2015'
      AND year(sale_date) = 2014) x
  GROUP BY year_month, price_band) y
GROUP BY price_band      


SELECT price_band,
  max(CASE WHEN year_month = 201501 THEN the_count end) as "Jan 15 Sold", 
  max(CASE WHEN year_month = 201501 THEN sales_pvr end) as "Jan 15 Sales PVR",
  max(CASE WHEN year_month = 201501 THEN fi_pvr end) as "Jan 15 FI PVR",
  max(CASE WHEN year_month = 201502 THEN the_count end) as "Feb 15 Sold", 
  max(CASE WHEN year_month = 201502 THEN sales_pvr end) as "Feb 15 Sales PVR",
  max(CASE WHEN year_month = 201502 THEN fi_pvr end) as "Feb 15 FI PVR",
  max(CASE WHEN year_month = 201503 THEN the_count end) as "Mar 15 Sold", 
  max(CASE WHEN year_month = 201503 THEN sales_pvr end) as "Mar 15 Sales PVR",
  max(CASE WHEN year_month = 201503 THEN fi_pvr end) as "Mar 15 FI PVR",
  max(CASE WHEN year_month = 201504 THEN the_count end) as "Apr 15 Sold", 
  max(CASE WHEN year_month = 201504 THEN sales_pvr end) as "Apr 15 Sales PVR",
  max(CASE WHEN year_month = 201504 THEN fi_pvr end) as "Apr 15 FI PVR",
  max(CASE WHEN year_month = 201505 THEN the_count end) as "May 15 Sold", 
  max(CASE WHEN year_month = 201505 THEN sales_pvr end) as "May 15 Sales PVR",
  max(CASE WHEN year_month = 201505 THEN fi_pvr end) as "May 15 FI PVR",
  max(CASE WHEN year_month = 201506 THEN the_count end) as "Jun 15 Sold", 
  max(CASE WHEN year_month = 201506 THEN sales_pvr end) as "Jun 15 Sales PVR",
  max(CASE WHEN year_month = 201506 THEN fi_pvr end) as "Jun 15 FI PVR",
  max(CASE WHEN year_month = 201507 THEN the_count end) as "Jul 15 Sold", 
  max(CASE WHEN year_month = 201507 THEN sales_pvr end) as "Jul 15 Sales PVR",
  max(CASE WHEN year_month = 201507 THEN fi_pvr end) as "Jul 15 FI PVR",
  max(CASE WHEN year_month = 201508 THEN the_count end) as "Aug 15 Sold", 
  max(CASE WHEN year_month = 201508 THEN sales_pvr end) as "Aug 15 Sales PVR",
  max(CASE WHEN year_month = 201508 THEN fi_pvr end) as "Aug 15 FI PVR",
  max(CASE WHEN year_month = 201509 THEN the_count end) as "Sep 15 Sold", 
  max(CASE WHEN year_month = 201509 THEN sales_pvr end) as "Sep 15 Sales PVR",
  max(CASE WHEN year_month = 201509 THEN fi_pvr end) as "Sep 15 FI PVR",
  max(CASE WHEN year_month = 201510 THEN the_count end) as "Oct 15 Sold", 
  max(CASE WHEN year_month = 201510 THEN sales_pvr end) as "Oct 15 Sales PVR",
  max(CASE WHEN year_month = 201510 THEN fi_pvr end) as "Oct 15 FI PVR",
  max(CASE WHEN year_month = 201511 THEN the_count end) as "Nov 15 Sold", 
  max(CASE WHEN year_month = 201511 THEN sales_pvr end) as "Nov 15 Sales PVR",
  max(CASE WHEN year_month = 201511 THEN fi_pvr end) as "Nov 15 FI PVR",
  max(CASE WHEN year_month = 201512 THEN the_count end) as "Dec 15 Sold", 
  max(CASE WHEN year_month = 201512 THEN sales_pvr end) as "Dec 15 Sales PVR",  
  max(CASE WHEN year_month = 201512 THEN fi_pvr end) as "Dec 15 FI PVR"    
FROM (  
  SELECT year_month, price_band, COUNT(*) AS the_count, 
    SUM(sales_gross)/COUNT(*) AS sales_pvr, 
    SUM(fi_gross)/COUNT(*) AS fi_pvr
  FROM (  
    SELECT sale_date, (year(sale_date) * 100) + month(sale_date) AS year_month, 
      sale_type, 
      CASE
        WHEN sale_amount BETWEEN 0 AND 5999 THEN '0 - 6k'
        WHEN sale_amount BETWEEN 6000 AND 9999 THEN '06 - 10k'
        WHEN sale_amount BETWEEN 10000 AND 13999 THEN '10 - 14k'
        WHEN sale_amount BETWEEN 14000 AND 19999 THEN '14 - 20k'
        WHEN sale_amount BETWEEN 20000 AND 25999 THEN '20 - 26k'
        ELSE '26k +'
      END AS price_band,
      sales_gross, fi_gross
    FROM ucinv_tmp_sales_Activity
    WHERE storecode = 'ry2'
      AND sale_type = 'retail'
      AND sale_date < '11/01/2015'
      AND year(sale_date) = 2015) x
  GROUP BY year_month, price_band) y
GROUP BY price_band      