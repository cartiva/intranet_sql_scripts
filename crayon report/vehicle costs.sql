--PROCEDURE UpdateVehicleCostTable

SELECT gtco#, gtjrnl, gtacct, gtctl#, sum(gttamt) AS gttamt, b.gmtype, b.gmdesc
FROM dds.stgArkonaGLPTRNS a
LEFT JOIN dds.stgArkonaGLPMAST b on a.gtacct = b.gmacct
  AND b.gmyear = 2014
WHERE gtacct IN (
  '124000','124100','144600','144601','144800','145000','145001','145200', 
  '164600','164601','164700','164701','164800','165000','165001','165100', 
  '165101','165200','224000','224100','244600','244601','244602','244610', 
  '244620','244800','245000','245001','245002','245010','245020','245200', 
  '264600','264601','264602','264610','264620','264701','264702','264710', 
  '264720','264800','265000','265001','265002','265010','265020','265100', 
  '265101','265102','265110','265120','265200')
AND year(gtdate) = 2014 
GROUP BY gtco#, gtjrnl, gtacct, gtctl#, b.gmtype, b.gmdesc


SELECT *
FROM dds.stgArkonaGLPMAST
WHERE gmyear = 2014
AND gmacct IN (
  '124000','124100','144600','144601','144800','145000','145001','145200', 
  '164600','164601','164700','164701','164800','165000','165001','165100', 
  '165101','165200','224000','224100','244600','244601','244602','244610', 
  '244620','244800','245000','245001','245002','245010','245020','245200', 
  '264600','264601','264602','264610','264620','264701','264702','264710', 
  '264720','264800','265000','265001','265002','265010','265020','265100', 
  '265101','265102','265110','265120','265200')

-- sale accounts : gmtype = '4' 
-- total inventory : gmtype = 1'
SELECT gtacct, b.gmtype
FROM dds.stgArkonaGLPTRNS a
LEFT JOIN dds.stgArkonaGLPMAST b on a.gtacct = b.gmacct
  AND b.gmyear = 2014
WHERE gtacct IN (
  '124000','124100','144600','144601','144800','145000','145001','145200', 
  '164600','164601','164700','164701','164800','165000','165001','165100', 
  '165101','165200','224000','224100','244600','244601','244602','244610', 
  '244620','244800','245000','245001','245002','245010','245020','245200', 
  '264600','264601','264602','264610','264620','264701','264702','264710', 
  '264720','264800','265000','265001','265002','265010','265020','265100', 
  '265101','265102','265110','265120','265200')
AND year(gtdate) = 2014 
GROUP BY gtacct, b.gmtype
ORDER BY b.gmtype, a.gtacct


SELECT gtco#, gtjrnl, gtacct, gtctl#, sum(gttamt) AS gttamt, b.gmtype, b.gmdesc,
-- well, this IS NOT working 19927a
SELECT gtctl#, 
  abs(SUM(CASE WHEN gmtype = '4' THEN gttamt ELSE 0 END)) AS soldAmount,
  abs(SUM(CASE WHEN gmtype = '1' THEN gttamt ELSE 0 END)) AS cost
FROM dds.stgArkonaGLPTRNS a
LEFT JOIN dds.stgArkonaGLPMAST b on a.gtacct = b.gmacct
  AND b.gmyear IN (2013,2014)
WHERE gtacct IN (
  '124000','124100','144600','144601','144800','145000','145001','145200', 
  '164600','164601','164700','164701','164800','165000','165001','165100', 
  '165101','165200','224000','224100','244600','244601','244602','244610', 
  '244620','244800','245000','245001','245002','245010','245020','245200', 
  '264600','264601','264602','264610','264620','264701','264702','264710', 
  '264720','264800','265000','265001','265002','265010','265020','265100', 
  '265101','265102','265110','265120','265200')
AND year(gtdate) = 2014 
GROUP BY gtctl#

SELECT *
FROM dds.stgArkonaGLPTRNS
WHERE gtctl# = '19927A'

-- 11/18/14
want some combination of dept, account type, doc type (gtdtyp) AND journal
to generate the account list 
GTDTYP   Doc Type        B          Deals                
GTDTYP   Doc Type        C          Checks                
GTDTYP   Doc Type        D          Deposits                
GTDTYP   Doc Type        I          Inventory Purchase/Stock In            
GTDTYP   Doc Type        J          Conversion or GJE                
GTDTYP   Doc Type        O          Invoice/PO Entry                
GTDTYP   Doc Type        P          Parts Ticket                
GTDTYP   Doc Type        R          Cash Receipts                
GTDTYP   Doc Type        S          Service Tickets                
GTDTYP   Doc Type        W          Handwrittn Checks                
GTDTYP   Doc Type        X          Bank Rec (Fee, Service Charges, etc.)   


AccountType 
1         Asset 
2         Liability 
3         Equity 
4         Sale 
5         CostofSale 
6         Income 
7         Other Income 
8         Expense 
9         Other Expense 


SELECT a.gtacct, a.gtdtyp, a.gtjrnl, b.gmdept, b.gmtype  
--INTO #wtf
FROM dds.stgArkonaGLPTRNS a
LEFT JOIN dds.stgArkonaGLPMAST b on a.gtacct = b.gmacct
  AND b.gmyear IN (2013,2014)
WHERE gtacct IN (
  '124000','124100','144600','144601','144800','145000','145001','145200', 
  '164600','164601','164700','164701','164800','165000','165001','165100', 
  '165101','165200','224000','224100','244600','244601','244602','244610', 
  '244620','244800','245000','245001','245002','245010','245020','245200', 
  '264600','264601','264602','264610','264620','264701','264702','264710', 
  '264720','264800','265000','265001','265002','265010','265020','265100', 
  '265101','265102','265110','265120','265200')
AND gtdate BETWEEN '09/01/2014' AND '10/31/2014'
AND b.gmtype = '4'
GROUP BY a.gtacct, a.gtdtyp, a.gtjrnl, b.gmdept, b.gmtype  

-- so, does this give me the same list of accounts
-- hmm, looks decent
SELECT a.gtacct, a.gtdtyp, a.gtjrnl, b.gmdept, b.gmtype  
--INTO #wtf
FROM dds.stgArkonaGLPTRNS a
LEFT JOIN dds.stgArkonaGLPMAST b on a.gtacct = b.gmacct
  AND b.gmyear IN (2013,2014)
WHERE  gtdate BETWEEN '09/01/2014' AND '10/31/2014'
  AND b.gmtype = '4'
  AND b.gmdept = 'UC'
  AND a.gtjrnl = 'VSU'
GROUP BY a.gtacct, a.gtdtyp, a.gtjrnl, b.gmdept, b.gmtype  

SELECT a.stockNumber, b.thedate, c.saleType, d.gttamt
FROM dds.factVehicleSale a
INNER JOIN dds.day b on a.cappedDateKey = b.datekey
INNER JOIN dds.dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
  AND c.vehicleTypeCode = 'U'
LEFT JOIN (
  SELECT a.gtctl#, SUM(a.gttamt) AS gttamt
  FROM dds.stgArkonaGLPTRNS a
  inner join dds.stgArkonaGLPMAST b on a.gtacct = b.gmacct
    AND b.gmyear = 2014
    AND a.gtdate > '12/31/2013'
    AND b.gmtype = '4'
    AND b.gmdept = 'UC'
    AND a.gtjrnl = 'VSU'
   GROUP BY a.gtctl#) d on a.stocknumber = d.gtctl#  
WHERE b.thedate BETWEEN '01/01/2014' AND '10/31/2014'


-- fuck my focus/scope IS ALL aver the fucking place
-- let's start with straight replacing the functionality of arkonaGlDownloader
-- 1. glDetailsTable

DELETE FROM dps.glDetailsTable;
INSERT INTO dps.glDetailsTable(companyNumber, journal, transactionDate, accountNumber,
  controlNumber, transactionAmount)
SELECT gtco#, gtjrnl, gtdate, gtacct, gtctl#, gttamt
FROM dds.stgArkonaGLPTRNS a
WHERE  gtdate BETWEEN '01/01/2014' AND curdate()
  AND gtacct IN (
    '124000','124100','144600','144601','144800','145000','145001','145200', 
    '164600','164601','164700','164701','164800','165000','165001','165100', 
    '165101','165200','224000','224100','244600','244601','244602','244610', 
    '244620','244800','245000','245001','245002','245010','245020','245200', 
    '264600','264601','264602','264610','264620','264701','264702','264710', 
    '264720','264800','265000','265001','265002','265010','265020','265100', 
    '265101','265102','265110','265120','265200');
    
-- 2. vehicleCosts
fuck me, i DO NOT even need this, to restore the current crayon report i just
need to run the delete/INSERT INTO glDetailsTable, THEN run the 
dps stored procs:  updateVehicleCostTable, createGrossGlogTable

SELECT c.*,
  coalesce((SELECT ABS(SUM(transactionAmount))
    FROM dps.glDetailsTable
    WHERE controlNumber = c.controlNumber
      AND accountNumber IN ('144800', '145200','144600', '144601', '145000', 
        '145001','244800', '245200','244600', '244601', '244602', '244610', 
        '244620', '245000', '245001', '245002', '245010', '245020')), 0) AS soldAmount,
  coalesce((SELECT SUM(transactionAmount)
    FROM dps.glDetailsTable
    WHERE controlNumber = c.controlNumber
      AND accountNumber IN ('124000','124100','224000', '224100','324000', '324100')
      AND journal IN ('SVI','SWA','SCQ')), 0) AS reconForInventoryItem,
  coalesce((SELECT ABS(SUM(transactionAmount))
    FROM dps.glDetailsTable
    WHERE controlNumber = c.controlNumber
      AND journal IN ('SVI','SWA','SCQ')), 0) AS reconTotalForVehicle,
  coalesce((SELECT SUM(transactionAmount)
    FROM dps.glDetailsTable
    WHERE controlNumber = c.controlNumber
      AND accountNumber IN ('164800', '165200','164600', '164601', '164700', 
        '164701', '165000', '165001', '165100', '165101','264800', '265200',
        '264600', '264601', '264602', '264610', '264620', '264701', '264702', 
        '264710', '264720','265000', '265001', '265002', '265010', '265020', 
        '265100', '265101', '265102', '265110', '265120')
      AND journal = 'VSU'), 0) AS costOfSale,   
  coalesce((SELECT SUM(transactionAmount)
    FROM dps.glDetailsTable
    WHERE controlNumber = c.controlNumber
      AND accountNumber IN ('164800', '165200','164600', '164601', '164700', 
        '164701', '165000', '165001', '165100', '165101','264800', '265200',
        '264600', '264601', '264602', '264610', '264620', '264701', '264702', 
        '264710', '264720','265000', '265001', '265002', '265010', '265020', 
        '265100', '265101', '265102', '265110', '265120')
      AND journal <> 'VSU'), 0) AS costPostSale,
  coalesce((SELECT ABS(SUM(transactionAmount))
    FROM dps.glDetailsTable
    WHERE controlNumber = c.controlNumber
      AND accountNumber IN ('124000', '124100','224000', '224100')), 0) AS totalCostInventoryItem, 
  CASE 
    WHEN EXISTS (
      SELECT 1 FROM dps.glDetailsTable 
      WHERE controlNumber = c.controlNumber 
        AND accountNumber IN ('144800', '145200','244800', '245200')) THEN 'Wholesale'
    WHEN EXISTS (
      SELECT 1 FROM dps.glDetailsTable 
      WHERE controlNumber = c.controlNumber 
        AND accountNumber IN ('144600', '144601', '145000', '145001','244600', 
          '244601', '244602', '244610', '244620', '245000', '245001', '245002', 
          '245010', '245020')) THEN 'Retail'
  END AS statusFromGl,
  c.dateSoldFromGL,
                                             
FROM (
  SELECT a.controlNumber,  b.VehicleInventoryItemID, d.theDate AS dateSoldFromGL
  FROM dps.glDetailsTable a
  INNER JOIN dps.VehicleInventoryItems b on a.controlNumber = b.stocknumber
  INNER JOIN dds.factVehicleSale c on a.controlNumber = c.stockNumber
  INNER JOIN dds.day d on c.cappedDateKey = d.dateKey
  GROUP BY a.controlNumber, b.VehicleInventoryItemID, d.theDate) c