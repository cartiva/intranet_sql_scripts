-- everyone who IS a ptoAdmin, exclude board of dir
SELECT a.authByDepartment, a.authByPosition, c.firstname, c.lastname
-- SELECT *
FROM ptoAuthorization a  
INNER JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position  
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true 
WHERE a.authByLevel <> 1  
GROUP BY a.authByDepartment, a.authByPosition, c.firstname, c.lastname  
  
