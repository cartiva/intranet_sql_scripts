EXECUTE PROCEDURE 
  sp_DropReferentialIntegrity ('ptoPositions-ptoDepartmentPositions'); 
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'ptoPositions-ptoDepartmentPositions',
     'ptoPositions', 
     'ptoDepartmentPositions', 
     'FK2', 
     1, 
     1, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_DropReferentialIntegrity ('ptoDepartments-ptoDepartmentPositions'); 
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'ptoDepartments-ptoDepartmentPositions',
     'ptoDepartments', 
     'ptoDepartmentPositions', 
     'FK1', 
     1, 
     1, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 

EXECUTE PROCEDURE 
  sp_DropReferentialIntegrity ('ptoDeptPos-ptoAuth1');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'ptoDeptPos-ptoAuth1',
     'ptoDepartmentPositions', 
     'ptoAuthorization', 
     'FK1', 
     1, 
     1, 
     NULL/* Enter Fail table path here. */,
     '', 
     '');      

EXECUTE PROCEDURE 
  sp_DropReferentialIntegrity ('ptoDeptPos-ptoAuth2');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'ptoDeptPos-ptoAuth2',
     'ptoDepartmentPositions', 
     'ptoAuthorization', 
     'FK2', 
     1, 
     1, 
     NULL/* Enter Fail table path here. */,
     '', 
     '');     

EXECUTE PROCEDURE 
  sp_DropReferentialIntegrity ('ptoDeptPos-ptoPosFul');        
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'ptoDeptPos-ptoPosFul',
     'ptoDepartmentPositions', 
     'ptoPositionFulfillment', 
     'FK1', 
     1, 
     1, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 
     
-- no need for ri on ptoClosure at this time, it will be regenerated
-- after each modification anyway
EXECUTE PROCEDURE 
  sp_DropReferentialIntegrity ('ptoDeptPos-ptoClosure1');  
EXECUTE PROCEDURE 
  sp_DropReferentialIntegrity ('ptoDeptPos-ptoClosure2');      
