/*
http://kylecordes.com/2008/transitive-closure

DROP TABLE ptoClosureTest;
CREATE TABLE ptoClosureTest (
  parentDept cichar(30),
  parentPos cichar(30),
  childDept cichar(30),
  childPos cichar(30),
  depth integer);
  
need a CURSOR that loops thru non leaf nodes, 
that becomes the value for parent  


-- 8/31:

maybe the secret to my mind fuck IS to concatenate the dept/position rather
than individual equalities! IN the JOIN conditions

think i am fucking up USING closureTest AND ptoClosure
get rid of ptoClosureTest
*/
/**/
DECLARE @i integer;
DECLARE @cur CURSOR AS
  SELECT distinct uc.ancestorDepartment, uc.ancestorPosition,
    u.authForDepartment, u.authForPosition
  FROM ptoClosure uc, ptoAuthorization u
--  WHERE uc.childDepartment = u.authByDepartment AND uc.childPosition = u.authByPosition
--    AND uc.depth = @i - 1;
  WHERE trim(uc.descendantDepartment)+ ':' + trim(uc.descendantPosition) =
    TRIM(u.authByDepartment) 
    + ':' + TRIM(u.authByPosition)
  AND uc.depth = @i - 1;
@i = 1;    

DELETE FROM ptoClosure;
INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
  descendantPosition, depth)
SELECT DISTINCT authByDepartment, authByPosition, authByDepartment, authByPosition, 0 
FROM ptoAuthorization; 
-- root   
UPDATE ptoClosure
SET topmost = true
WHERE descendantPosition = 'board of directors';
-- AND one self referencing (depth = 0) row for each leaf node
INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
  descendantPosition, depth, lowest)
SELECT authForDepartment, authForPosition, authForDepartment, authForPosition, 0, true 
FROM ptoAuthorization a
WHERE NOT EXISTS (
  SELECT 1
  FROM  ptoAuthorization
  WHERE authByDepartment = a.authForDepartment
    AND authByPosition = a.authForPosition);
    
WHILE @i < (SELECT MAX(authForLevel) + 1 FROM ptoAuthorization) DO 
OPEN @cur;
TRY
  WHILE FETCH @cur DO
    INSERT INTO ptoClosure values(@cur.ancestorDepartment, @cur.ancestorPosition,
      @cur.authForDepartment, @cur.authForPosition, @i, false, false);
  END WHILE;
FINALLY
  CLOSE @cur;
END TRY;  
@i = @i + 1;
END WHILE;
/**/
/* 
that did NOT WORK, TRY it without the CURSOR, 

DECLARE @i integer;
@i = ;
WHILE @i < 6 DO 
  INSERT INTO ptoClosureTest
  SELECT uc.ancestorDepartment, uc.ancestorPosition,
    u.authForDepartment, u.authForPosition, @i
  FROM ptoClosure uc, ptoAuthorization u
  WHERE trim(uc.descendantDepartment)+ ':' + trim(uc.descendantPosition) =
    TRIM(u.authByDepartment) + ':' + TRIM(u.authByPosition)
  AND uc.depth = @i - 1;
  @i = (SELECT MAX(depth) FROM ptoClosureTest);
END WHILE;
*/