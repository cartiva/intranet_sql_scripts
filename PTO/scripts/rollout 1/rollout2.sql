-- pto_requests
DELETE FROM pto_requests;
INSERT INTO pto_requests (employeeNumber,theDate,hours,requestStatus,requestType)
select left(userid, 7), [scheduled date],MAX(hours) AS hours,
  LEFT([request status], 12),LEFT([allocation type], 24)
FROM pto_compli_timeoff a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
WHERE b.employeenumber IS NOT NULL 
GROUP BY userid, [request status],[scheduled date],[allocation type];
/******************************************************************************/
-- pto at rollout
DECLARE @rollout date;
@rollout = curdate();   
DELETE FROM pto_at_rollout;
INSERT INTO pto_at_rollout 
SELECT c.employeenumber, pto2014start AS ptofrom,
--< *a1*
--  pto2015start - 1  
  CASE 
     WHEN year(ptoanniversary) = 2013 and cast(timestampadd(sql_tsi_year, 1, ptoanniversary) AS sql_date) > curdate()
       THEN cast(createTimestamp(2014, month(ptoanniversary), 
          dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date) -1
     ELSE pto2015start - 1 
--/> *a1*     
  END AS ptothru,
  ptocategory,
  timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 AS multiplier,
  pto2014start, pto2015start,
  CASE 
    WHEN ptocategory = 0 THEN 0
    ELSE round(timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 * (
      SELECT ptohours
      FROM pto_categories
      WHERE ptocategory = c.ptocategory), 0)
  END AS ptoearnedatrollout
FROM (                                                                        
  SELECT a.*, b.ptocategory,
--< *a*  
--    CASE year(ptoanniversary)
--      WHEN 2013 THEN cast(createTimestamp(2014, month(ptoanniversary), 
--        dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date)
--      ELSE cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
--    END AS pto2014start, 
    CASE 
      WHEN year(ptoanniversary) = 2013 and cast(timestampadd(sql_tsi_year, 1, ptoanniversary) AS sql_date) < curdate()
        THEN cast(createTimestamp(2014, month(ptoanniversary), 
          dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date)
--      WHEN year(ptoanniversary) = 2013 and cast(timestampadd(sql_tsi_year, 1, ptoanniversary) AS sql_date) < curdate()
      ELSE cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
    END AS pto2014start,  
--/> *a*      
    cast(createTimestamp(2015, month(ptoanniversary), 
      dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date) AS pto2015start 
  FROM ptoEmployees a
  INNER JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
  WHERE @rollout BETWEEN b.fromdate AND b.thrudate) c;

-- pto_employee_pto_allocation  
/******************************************************************************/
DELETE FROM pto_employee_pto_allocation;
/******************************************************************************/
DECLARE @employeenumber string;
DECLARE @cur CURSOR AS 
  SELECT employeenumber
  FROM ptoEmployees;
-- @employeenumber = '150120';

OPEN @cur;
TRY 
  WHILE FETCH @cur DO
    @employeeNumber = @cur.employeenumber;
    INSERT INTO pto_employee_pto_allocation
    SELECT a.employeenumber, a.pto2014Start, a.ptoThru, //a.pto2015Start - 1, 
      a.ptoAtRollout
    FROM pto_at_rollout a
    INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber    
    WHERE a.employeenumber = @employeenumber;  
    
    INSERT INTO pto_employee_pto_allocation
    SELECT top 1 a.employeenumber, e.thrudate + 1, c.thrudate, d.ptoHours
    FROM ptoemployees a
    INNER JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
    INNER JOIN pto_categories d on c.ptocategory = d.ptocategory  
    INNER JOIN pto_employee_pto_allocation e on a.employeenumber = e.employeenumber
      AND e.thrudate = (
        SELECT MAX(thruDate)
        FROM pto_employee_pto_allocation
        WHERE employeenumber = @employeenumber)
    WHERE a.employeenumber = @employeenumber
      AND e.thrudate + 1 BETWEEN c.fromdate AND c.thrudate
    ORDER BY c.ptoCategory;
    
    INSERT INTO pto_employee_pto_allocation      
    SELECT top 1 a.employeenumber, e.thrudate + 1, c.thrudate, d.ptoHours
    FROM ptoemployees a
    INNER JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
    INNER JOIN pto_categories d on c.ptocategory = d.ptocategory  
    INNER JOIN pto_employee_pto_allocation e on a.employeenumber = e.employeenumber
      AND e.thrudate = (
        SELECT MAX(thruDate)
        FROM pto_employee_pto_allocation
        WHERE employeenumber = @employeenumber)
    WHERE a.employeenumber = @employeenumber
      AND e.thrudate + 1 BETWEEN c.fromdate AND c.thrudate
    ORDER BY c.ptoCategory;
    
    INSERT INTO pto_employee_pto_allocation
    SELECT top 1 a.employeenumber, e.thrudate + 1, c.thrudate, d.ptoHours
    FROM ptoemployees a
    INNER JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
    INNER JOIN pto_categories d on c.ptocategory = d.ptocategory  
    INNER JOIN pto_employee_pto_allocation e on a.employeenumber = e.employeenumber
      AND e.thrudate = (
        SELECT MAX(thruDate)
        FROM pto_employee_pto_allocation
        WHERE employeenumber = @employeenumber)
    WHERE a.employeenumber = @employeenumber
      AND e.thrudate + 1 BETWEEN c.fromdate AND c.thrudate
    ORDER BY c.ptoCategory;
    
    INSERT INTO pto_employee_pto_allocation
    SELECT top 1 a.employeenumber, e.thrudate + 1, c.thrudate, d.ptoHours
    FROM ptoemployees a
    INNER JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
    INNER JOIN pto_categories d on c.ptocategory = d.ptocategory  
    INNER JOIN pto_employee_pto_allocation e on a.employeenumber = e.employeenumber
      AND e.thrudate = (
        SELECT MAX(thruDate)
        FROM pto_employee_pto_allocation
        WHERE employeenumber = @employeenumber)
    WHERE a.employeenumber = @employeenumber
      AND e.thrudate + 1 BETWEEN c.fromdate AND c.thrudate
    ORDER BY c.ptoCategory;  
  END WHILE;
FINALLY
  CLOSE @cur;
END TRY; 
 
-- pto_used
DELETE FROM pto_used WHERE source = 'Time Clock';
INSERT INTO pto_used (employeeNumber, theDate, hours, source)
SELECT m.employeenumber, theDate, ptoHours, 'Time Clock'
FROM ptoEmployees m
LEFT JOIN dds.edwEmployeeDim p on m.employeenumber = p.employeenumber
  AND p.currentrow = true
  AND p.activecode = 'a'
INNER JOIN (
  SELECT c.employeenumber, b.thedate, 
    a.vacationhours + a.ptohours AS ptoHours
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.day b on a.datekey = b.datekey
    AND b.theyear = 2014
  INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey  
    AND c.currentrow = true
    AND c.activecode = 'a'
  WHERE a.vacationhours > 0 OR  a.ptohours > 0) n on m.employeenumber = n.employeenumber
WHERE p.lastname IS NOT NULL; 

/******************************************************************************/  

-- lear ascension

INSERT INTO ptoDepartments (department)
values ('RY2');

UPDATE ptoDepartmentPositions
SET department = 'RY2'
WHERE department = 'RY2 Sales'
  AND position = 'General Manager';

INSERT INTO ptoPositions (position)
values ('Desk Manager');
INSERT INTO ptoDepartmentPositions (department, position, level, ptoPolicyHtml)
values('RY2 Sales','Desk Manager',3,'no_policy_html');

INSERT INTO ptoAuthorization (authByDepartment, authByPosition, authForDepartment,
  authForPosition, authByLevel, authForLevel)
values ('RY2','General Manager','RY2 Sales','Desk Manager',2,3);

DELETE FROM ptoPositionFulfillment
WHERE employeeNumber = '242460';
INSERT INTO ptoPositionFulfillment (department,position,employeeNumber)
values('RY2 Sales','Desk Manager','242460');

INSERT INTO ptoPositions (position)
values ('Fixed Director');

UPDATE ptoDepartmentPositions
SET department = 'RY1',
    position = 'Fixed Director'
WHERE department = 'market fixed'
  AND position = 'director';  

UPDATE ptoAuthorization
SET authByDepartment = 'RY2',
    authByPosition = 'General Manager'
WHERE authForDepartment = 'RY2 main shop'
  AND authForPosition = 'manager';   
   

BEGIN TRANSACTION;
TRY   
UPDATE ptoDepartments
SET department = 'RY1 Parts'
WHERE department = 'Parts';  

INSERT INTO ptoDepartments (department)
values ('RY2 Parts');

INSERT INTO ptoDepartmentPositions (department,position, level,ptoPolicyHtml)
values ('RY2 Parts','Manager',3,'parts_policy_html');

INSERT INTO ptoDepartmentPositions (department,position, level,ptoPolicyHtml)
values ('RY2 Parts','Sales Consultant',4,'parts_policy_html');

INSERT INTO ptoDepartmentPositions (department,position, level,ptoPolicyHtml)
values ('RY2 Parts','Shipping & Receiving',4,'parts_policy_html');
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
  
BEGIN TRANSACTION;
TRY  
UPDATE ptoPositionFulfillment
SET department = 'RY2 Parts',
    position = 'Sales Consultant'
WHERE employeenumber = '274225';

UPDATE ptoPositionFulfillment
SET department = 'RY2 Parts',
    position = 'Shipping & Receiving'
WHERE employeenumber = '264220';

UPDATE ptoPositionFulfillment
SET department = 'RY2 Parts',
    position = 'Manager'
WHERE employeenumber = '2106350';  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 

BEGIN TRANSACTION;
TRY  
INSERT INTO ptoAuthorization (authByDepartment,authByPosition,AuthForDepartment,
  AuthForPosition,authByLevel,authForLevel)
values('RY2','General Manager','RY2 Parts','Manager',2,3);

INSERT INTO ptoAuthorization (authByDepartment,authByPosition,AuthForDepartment,
  AuthForPosition,authByLevel,authForLevel)
values('RY2 Parts','Manager','RY2 Parts','Sales Consultant',3,4);

INSERT INTO ptoAuthorization (authByDepartment,authByPosition,AuthForDepartment,
  AuthForPosition,authByLevel,authForLevel)
values('RY2 Parts','Manager','RY2 Parts','Shipping & Receiving',3,4);
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 

BEGIN TRANSACTION;
TRY
INSERT into ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
values('RY1 PDQ','Assistant Manager',5,'no_policy_html');

UPDATE ptoDepartmentPositions
SET level = 6
-- SELECT * FROM ptoDepartmentPositions
WHERE department = 'ry1 pdq'
  AND position LIKE '%tech%';

UPDATE ptoAuthorization
SET authByPosition = 'Assistant Manager',
    authByLevel = 5,
    authForLevel = 6 
-- SELECT * FROM ptoAuthorization
WHERE authForDepartment = 'ry1 pdq'
  AND authForPosition LIKE '%tech%';  
  
INSERT INTO ptoAuthorization (authByDepartment, authByPosition, authForDepartment,
  authForPosition, authByLevel, authForLevel)
values ('RY1 PDQ','Manager','RY1 PDQ','Assistant Manager',4,5); 
  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    

BEGIN TRANSACTION;
TRY

UPDATE ptoDepartmentPositions
SET level = 5
--SELECT * FROM ptoDepartmentPositions
WHERE department = 'ry1 main shop'
  AND position = 'Lead Technician (A)';

UPDATE ptoAuthorization
SET authByDepartment = 'RY1 Main Shop',
    authByPosition = 'Dispatcher',
    authByLevel = 4,
    authForLevel = 5
-- SELECT * FROM ptoAuthorization
WHERE authForDepartment = 'ry1 main shop'
  AND authForPosition LIKE '%tech%';
  
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY; 

/******************************************************************************/  
-- transfers
-- kyle bragunier
DELETE 
FROM employeeAppAuthorization
WHERE username IN (
SELECT username
FROM tpemployees
WHERE lastname = 'bragunier'); 

DELETE FROM tpEmployees
WHERE firstname = 'kyle'
  AND lastname = 'bragunier';  

-- adam lindquist  
BEGIN TRANSACTION;
TRY 
UPDATE tpEmployees
SET employeenumber = '185801'
WHERE employeenumber = '285800';

DELETE 
FROM employeeAppAuthorization
WHERE username = (
  SELECT username 
  FROM tpEmployees
  WHERE employeenumber = '185801')  
AND appcode = 'sco';

INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'alindquist@gfhonda.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';  
  
UPDATE ptoEmployees
SET employeenumber = '185801'
WHERE employeenumber = '285800';

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;   

UPDATE ptoPositionFulfillment
SET department = 'RY1 PDQ'
WHERE employeenumber = '185801';

-- kelsie mcmillin
UPDATE ptoEmployees
SET employeeNumber = '293210'
WHERE employeenumber = '193210';

UPDATE ptoPositionFulfillment
SET department = 'RY2 Sales',
    position = 'Reception'
WHERE employeenumber = '293210';   
/******************************************************************************/  
  