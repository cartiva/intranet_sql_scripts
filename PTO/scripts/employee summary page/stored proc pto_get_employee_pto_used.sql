alter PROCEDURE pto_get_employee_pto_used (
  employeeNumber cichar(9),
  employeeNumber cichar(9) output,
  theDate date output,
  hours double output,
  theType cichar(9)output)
BEGIN
/*
shows hours used/req/appr for current pto period only

EXECUTE PROCEDURE pto_get_employee_pto_used ('196341');

*/
DECLARE @employeeNumber string;
@employeeNumber = (SELECT employeeNumber FROM __input);
INSERT INTO __output
SELECT top 100 *
  FROM( 
    SELECT a.employeenumber, a.theDate, a.hours, 'Used'
    FROM pto_used a 
    INNER JOIN pto_employee_pto_allocation b on a.employeeNumber = b.employeeNumber
      AND curdate() BETWEEN b.fromDate AND b.thruDate
    WHERE a.employeenumber = @employeeNumber
      AND a.theDate BETWEEN b.fromDate AND b.thruDate
    UNION 
    SELECT a.employeenumber, a.theDate, a.hours, 'Approved'
    FROM pto_requests a
    INNER JOIN pto_employee_pto_allocation b on a.employeeNumber = b.employeeNumber
      AND curdate() BETWEEN b.fromDate AND b.thruDate
    WHERE a.employeenumber = @employeeNumber
      AND a.theDate BETWEEN curdate() AND b.thruDate
      AND a.requestType = 'Paid Time Off'
      AND a.requestStatus = 'Approved'
    UNION 
    SELECT a.employeenumber, a.theDate, a.hours, 'Requested'
    FROM pto_requests a
    INNER JOIN pto_employee_pto_allocation b on a.employeeNumber = b.employeeNumber
      AND curdate() BETWEEN b.fromDate AND b.thruDate
    WHERE a.employeenumber = @employeeNumber
      AND a.theDate BETWEEN curdate() AND b.thruDate
      AND a.requestType = 'Paid Time Off'
      AND a.requestStatus = 'Pending') x
ORDER BY theDate DESC;  
END;  