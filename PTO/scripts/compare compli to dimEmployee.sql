/*
9/4/14
export users IS the extract FROM compli export users, minus a few superfluous attributes
status: 1: currently employed
        2: termed
*/
-- currently employed IN compli, termed IN edwEmployeeDim 
SELECT a.userid, a.firstname, a.lastname, a.dateofhire, a.status, b.name, b.hiredate, b.termdate
FROM exportusers a
LEFT JOIN edwEmployeeDim b on a.userid = b.employeenumber
  AND b.currentrow = true
WHERE status = 1 AND termdate <> '12/31/9999'  

-- EXISTS IN compli, NOT IN edwEmployeeDim 
SELECT *
FROM exportusers a
LEFT JOIN edwEmployeeDim b on a.userid = b.employeenumber
WHERE b.employeenumber IS NULL 

-- currently employed, NOT IN compli
SELECT *
FROM edwEmployeeDim a 
LEFT JOIN exportusers b on a.employeenumber = b.userid
WHERE b.userid IS NULL
  AND a.currentrow = true
  AND a.activecode = 'a'
  
-- currently employed IN edwEmployeeDim, termed IN compli (status 2)
SELECT a.storecode, a.employeenumber, a.name, b.*
SELECT a.employeenumber, trim(a.firstname) + ' ' +  a.lastname
FROM edwEmployeeDim a 
LEFT JOIN exportusers b on a.employeenumber = b.userid
WHERE b.status = 2
  AND a.currentrow = true
  AND a.activecode = 'a' 
  AND a.hiredate <= curdate() 
  
-- names don't match
SELECT left(trim(a.firstname) + ' ' + a.lastname, 30) AS "Arkona Name", trim(b.firstname) + ' ' + b.lastname AS "Compli Name"
FROM edwEmployeeDim a
INNER JOIN exportUsers b on a.employeenumber = b.userid
WHERE a.currentrow = true
  AND (a.firstname <> b.firstname OR a.lastname <> b.lastname)  
  
-- employeenumbers
SELECT left(trim(a.firstname) + ' ' + a.lastname, 30) AS "Arkona Name", 
  trim(b.firstname) + ' ' + b.lastname AS "Compli Name",
  a.employeenumber, b.userid
FROM edwEmployeeDim a
INNER JOIN exportUsers b on a.firstname = b.firstname
  AND a.lastname = b.lastname 
  AND a.currentrow = true
  AND a.activecode = 'a'
WHERE a.currentrow = true
  AND a.employeenumber <> b.userid
  

