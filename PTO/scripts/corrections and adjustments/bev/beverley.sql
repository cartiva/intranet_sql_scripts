

I have decided that I should have 6 days vacation remaining between now and May 21, 2015, my new anniversary date.  If we go by that date, 
I technically should have had 20 days vacation for 2014 
( and previous years as well, but we won�t go there).  
So I am going to accept those days. Please let Ben know that 
my hours of PTO should read 48.  

Thanks!

Bev Longoria



SELECT a.*, c.*, d.*
FROM ptoEmployees a
INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
LEFT JOIN pto_employee_pto_allocation c on a.employeenumber = c.employeenumber
LEFT JOIN pto_adjustments d on a.employeenumber = d.employeenumber
WHERE b.lastname = 'longoria'

INSERT INTO pto_adjustments(employeenumber,fromdate,hours,reason,notes)
values('187675','01/01/2014', -36, 'adjustment','I have decided that I should have 6 days vacation remaining between now and May 21, 2015, my new anniversary date.  If we go by that date, I technically should have had 20 days vacation for 2014 ( and previous years as well, but we won�t go there).  So I am going to accept those days. Please let Ben know that my hours of PTO should read 48.')

UPDATE ptoEmployees
SET ptoAnniversary = '05/21/1990',
    anniversaryType = 'Other'
where employeenumber = '187675' 

select *
FROM pto_employee_pto_allocation
WHERE employeenumber = '187675'  

DELETE 
FROM pto_employee_pto_allocation
WHERE employeenumber = '187675'  
  AND fromdate BETWEEN '02/21/2015' AND '02/21/2024';
  
UPDATE pto_employee_pto_allocation
SET thruDate = '05/20/2015'
where employeenumber = '187675' 
  AND fromdate = '01/01/2014';
  
UPDATE pto_employee_pto_allocation
SET fromDate = '05/21/2015'
where employeenumber = '187675' 
  AND thruDate = '12/31/9999';  

