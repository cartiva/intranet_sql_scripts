-- arkona shows 6/23/08, per joseph AND joel it should be 9/11/2006

SELECT left(a.username, 25) as username, a.password, left(a.firstname,20) AS firstname,
  left(a.lastname,20) AS lastname, a.employeenumber, b.ptoAnniversary, b.anniversaryType,
  c.fromdate, c.thrudate, c.hours, d.hours, d.reason, d.notes
FROM tpEmployees a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
INNER JOIN pto_employee_pto_allocation c on b.employeenumber = c.employeenumber
LEFT JOIN pto_adjustments d on c.employeenumber = d.employeenumber
  AND c.fromdate = d.fromdate
WHERE a.lastname = 'rud'
ORDER BY c.fromdate

SELECT DISTINCT anniversarytype FROM ptoemployees

UPDATE ptoEmployees
  SET ptoAnniversary = '09/11/2006',
      anniversaryType = 'Other'
WHERE employeenumber = '2119120'

-- josephs tenure at rollout 8 years
SELECT timestampdiff(sql_tsi_year,
cast('09/11/2006 00:00:00' AS sql_timestamp), 
  cast('11/01/2014 00:00:00' AS sql_timestamp))
FROM system.iota


DECLARE @ptoFrom date;
DECLARE @ptoThru date;
DECLARE @employeeNumber string;
DECLARE @ptoCategory integer;
DECLARE @ptoAnniversary date;
DECLARE @rollout date;
@ptoAnniversary = '09/11/2006';
@ptoFrom = '01/01/2014';
@ptoThru = '09/10/2015';
@employeeNumber = '2119120';
@rollout = '11/02/2014';
@ptoCategory = (
  SELECT b.ptoCategory
  FROM ptoEmployees a
  LEFT JOIN pto_categories b on 
    timestampdiff(sql_tsi_year, ptoAnniversary, @rollout) - 1 
      BETWEEN b.fromYearsTenure AND b.thruYearsTenure
  WHERE a.employeenumber = @employeeNumber);

BEGIN TRANSACTION;
TRY 
  SELECT employeenumber, @ptoFrom AS ptoFrom, @ptoThru AS ptoThru,
    round(timestampdiff(sql_tsi_day, @ptoFrom, @ptoThru)/365.0 * (
        SELECT ptohours
        FROM pto_categories
        WHERE ptocategory = @ptoCategory), 0) AS hours
  INTO #current      
  FROM ptoEmployees
  WHERE employeenumber = @employeeNumber;

  SELECT c.employeenumber, fromDate, thruDate, 
    case year(thruDate)
      when 9999 then 192
      else ptoHours
     END AS ptoHours
  INTO #omg   
  FROM (
    SELECT a.*, b.n,
      cast(timestampadd(sql_tsi_year, n, ptoAnniversary) AS sql_date) AS fromDate,
      CASE b.n
        WHEN 20 THEN CAST('12/31/9999' AS sql_date)
        ELSE cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, n + 1, 
          ptoAnniversary)) AS sql_date) 
      END AS thruDate
    FROM ptoEmployees a
    LEFT JOIN dds.tally b on 1 = 1
      AND b.n BETWEEN 0 AND 20
    WHERE a.employeenumber = @employeeNumber) c
  LEFT JOIN pto_categories d on timestampdiff(sql_tsi_year, c.ptoAnniversary, c.thrudate) - 1
    BETWEEN d.fromYearsTenure AND d.thruYearsTenure 
  LEFT JOIN #current z on z.ptoThru = c.thruDate 
  WHERE thruDate > curdate()
    AND z.employeeNumber IS NULL;

  DELETE FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeeNumber;
  INSERT INTO pto_employee_pto_allocation(employeeNumber,fromDate,thruDate,hours) 
  SELECT *
  FROM #current
  UNION
  SELECT *
  FROM #omg;  
  COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  


