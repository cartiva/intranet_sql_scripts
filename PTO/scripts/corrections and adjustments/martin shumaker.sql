martin shumaker
emp# '1126060'
time clock for sept 2014 shows 56 hours pto clocked
he actually only got paid for 16 of those hours

-- clock data
SELECT b.thedate, a.* 
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
  AND b.yearmonth = 201409
WHERE employeekey IN (
  SELECT employeekey
  FROM dds.edwEmployeeDim
  WHERE employeenumber = '1126060')
AND vacationhours + ptohours <> 0
  
-- paychecks
-- yhdcmm: check date month
-- yhdvac: vacation taken
-- yhdsck: sick leave taken
Select yhdcmm, yhdvac, yhdsck
from dds.stgArkonaPYHSHDTA
WHERE yhdemp = '1126060'
AND yhdcyy = 14
-- AND yhdcmm IN (8,9,10)
AND yhdvac + yhdsck <> 0

-- glptrns
-- distcode: RAY
-- vac/pto: 20% each to 12401, 12402, 12404, 12405, 12406
SELECT *
FROM dds.stgArkonaGLPTRNS
WHERE gtctl# = '1126060'
  AND gtacct IN ('12401','12402','12404','12405','12406')
  AND gtjrnl = 'PAY'
  AND gtdate BETWEEN '09/01/2014' AND '09/30/2014'

-- pto_used  
SELECT *
FROM pto_used  
WHERE employeenumber = '1126060'
ORDER BY thedate 

    
UPDATE dds.edwClockHoursFact
SET ptohours = 0
-- select * FROM dds.edwClockHoursFact 
WHERE employeekey = 2469
  AND datekey IN (
    SELECT datekey 
    FROM dds.day 
    WHERE thedate IN('09/12/2014','09/24/2014','09/25/2014','09/26/2014','09/29/2014'));
    
DELETE 
-- SELECT *
FROM pto_used
WHERE thedate IN('09/12/2014','09/24/2014','09/25/2014','09/26/2014','09/29/2014')
  AND employeenumber = '1126060';
  
  
lindsey bohlman 17410 AND Debra Koenen 179693 each donate 8 hours to martin

INSERT INTO pto_used (employeenumber, thedate, source, hours)
values('17410','01/02/2015','Donate',8);
INSERT INTO pto_used (employeenumber, thedate, source, hours)
values('179693','01/02/2015','Donate',8);

-- the date IN pto_adustments IS the fromDate FROM the employees current pto period
INSERT INTO pto_adjustments(employeenumber, fromdate, hours, reason, notes)
values('1126060', '01/01/2014',8,'Donation', 'Donated by lindsey bohlman 17410');

-- uh oh, pk violation, pk IS employeenumber & date
INSERT INTO pto_adjustments(employeenumber, fromdate, hours, reason, notes)
values('1126060', '01/01/2014',8,'Donation', 'Donated by Debra Koenen 179693');

UPDATE pto_adjustments
SET hours = 16,
    notes = 'Donated by Lindsey Bohlman 17410 and Debra Koenen 179693, 8 hrs each'    
WHERE employeenumber = '1126060'
  AND fromDate = '01/01/2014'
