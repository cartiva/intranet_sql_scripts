larry humble
anniv FROM 11/1/06 to 05/01/2005

SELECT *
FROM ptoEmployees a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
WHERE b.lastname = 'humble'

SELECT *
FROM pto_employee_pto_allocation
WHERE employeenumber = '168800'

UPDATE ptoEmployees
SET ptoAnniversary = '05/01/2005',
    anniversaryType = 'Other'
WHERE employeenumber = '168800';


/*
 DROP TABLE #current;
 DROP TABLE #omg;
*/
DECLARE @ptoFrom date;
DECLARE @ptoThru date;
DECLARE @employeeNumber string;
DECLARE @ptoCategory integer;
DECLARE @ptoAnniversary date;
DECLARE @rollout date;
@ptoAnniversary = '05/01/2005';
@ptoFrom = '01/01/2014';
@ptoThru = '04/30/2015';
@employeeNumber = '168800';
@rollout = '11/02/2014';
@ptoCategory = (
SELECT b.ptoCategory
FROM ptoEmployees a
LEFT JOIN pto_categories b on 
  timestampdiff(sql_tsi_year, ptoAnniversary, @rollout) - 1 
    BETWEEN b.fromYearsTenure AND b.thruYearsTenure
WHERE a.employeenumber = @employeeNumber);
BEGIN TRANSACTION;
TRY 
  SELECT employeenumber, @ptoFrom AS ptoFrom, @ptoThru AS ptoThru,
    round(timestampdiff(sql_tsi_day, @ptoFrom, @ptoThru)/365.0 * (
        SELECT ptohours
        FROM pto_categories
        WHERE ptocategory = @ptoCategory), 0) AS hours
  INTO #current      
  FROM ptoEmployees
  WHERE employeenumber = @employeeNumber;
  
  SELECT c.employeenumber, fromDate, thruDate, 
    case year(thruDate)
      when 9999 then 192
      else ptoHours
     END AS ptoHours
  INTO #omg   
  FROM (
    SELECT a.*, b.n,
      cast(timestampadd(sql_tsi_year, n, ptoAnniversary) AS sql_date) AS fromDate,
      CASE b.n
        WHEN 20 THEN CAST('12/31/9999' AS sql_date)
        ELSE cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, n + 1, 
          ptoAnniversary)) AS sql_date) 
      END AS thruDate
    FROM ptoEmployees a
    LEFT JOIN dds.tally b on 1 = 1
      AND b.n BETWEEN 0 AND 20
    WHERE a.employeenumber = @employeeNumber) c
  LEFT JOIN pto_categories d on timestampdiff(sql_tsi_year, c.ptoAnniversary, c.thrudate) - 1
    BETWEEN d.fromYearsTenure AND d.thruYearsTenure 
  LEFT JOIN #current z on z.ptoThru = c.thruDate 
  WHERE thruDate > curdate()
    AND z.employeeNumber IS NULL;
  DELETE FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeeNumber;
  INSERT INTO pto_employee_pto_allocation(employeeNumber,fromDate,thruDate,hours) 
  SELECT *
  FROM #current
  UNION
  SELECT *
  FROM #omg;  
  COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  