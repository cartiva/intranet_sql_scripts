
-- requested/scheduled    
-- question for ben: aubrey IS entering absent time ?!?
compli
reports
       time off tracker
            time allocation detail
                 include pending requests
                 SET date range
                 ALL users
            export to excel (save AS compli_timeoff)
            save AS csv
            edit csv: 
              DELETE rows 1,2 
              DELETE ALL columns except user id, scheduled date,
                hours, allocation type
                DELETE the spaces IN those COLUMN names 
ads architect
  import csv (file name will be something LIKE compli_timeoff.csv)
  
10/28
DO NOT DELETE any columns, save the entire edited csv AS pto_compli_timeoff.csv

 
SELECT *
FROM pto_compli_timeoff

ALTER TABLE pto_compli_timeoff
ALTER COLUMN [user id] userid cichar(9)

/*
CREATE TABLE Time_Allocation_Detail_20140919063633 ( 
      employeeNumber CIChar( 7 ),
      requestStatus CIChar( 10 ),
      scheduledDate Date,
      hours Double( 2 ),
      allocationType CIChar( 24 )) IN DATABASE;
*/ 

/*
execute procedure sp_DropReferentialIntegrity('ptoEmployees-pto_requests');
DROP TABLE pto_requests;     

9/30, due to compli dups, simplify this for now
*/  

CREATE TABLE pto_requests (
  employeeNumber cichar(7) constraint NOT NULL,
  theDate date constraint NOT NULL,
  hours numeric(6,2) constraint NOT NULL,
--  requestStatus cichar(10) constraint NOT NULL,
--  requestType cichar(24) constraint NOT NULL,
  constraint pk primary key (employeeNumber, theDate)) IN database;

EXECUTE PROCEDURE sp_CreateIndex90( 'pto_requests', 'pto_requests.adi', 'theDate', 'theDate', '', 2, 1024, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 'pto_requests', 'pto_requests.adi', 'FK', 'employeeNumber', '', 2, 1024, '' );
-- UPDATE & DELETE: cascade
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoEmployees-pto_requests', 'ptoEmployees', 'pto_requests', 'FK', 1, 1, NULL, '', '');

DELETE FROM pto_requests;  
INSERT INTO pto_requests
SELECT userid, scheduleddate, hours
FROM compli_timeoff-10-16
WHERE allocationtype = 'paid time off'
GROUP BY userid, scheduleddate, hours;


/* dups IN compli ************************************************************/
-- oh great, compli allows multiple time off events for an employee
-- on a single day, ie, dups
SELECT [user id], [scheduled date]
FROM compli_timeoff
GROUP BY [user id], [scheduled date]
HAVING COUNT(*) > 1

SELECT a.*
FROM compli_timeoff a
INNER JOIN (
  SELECT [user id], [scheduled date]
  FROM compli_timeoff
  GROUP BY [user id], [scheduled date]
  HAVING COUNT(*) > 1) b on a.[user id] = b.[user id]
    AND a.[scheduled date] = b.[scheduled date]
ORDER BY a.[user id], a.[scheduled date]    

ok, so here IS what we are going to DO
change toe pto_requests TABLE to just employeenumber, date & hours,  limit 
  compli information to Paid Time Off, AND GROUP BY those 3 fields 
SELECT [user id], [scheduled date]
FROM compli_timeoff
WHERE allocationType = 'paid time off'
GROUP BY [user id], [scheduled date]
HAVING COUNT(*) > 1


SELECT userid, scheduleddate, hours
FROM compli_timeoff
WHERE allocationtype = 'paid time off'
GROUP BY userid, scheduleddate, hours
/* dups IN compli ************************************************************/

10/28
-- currently only dups are for unpaid time off
SELECT userid, [request status],[scheduled date],[allocation type]
FROM pto_compli_timeoff
GROUP BY userid, [request status],[scheduled date],[allocation type]
HAVING COUNT(*) > 1

select userid, [scheduled date],MAX(hours) AS hours,[request status],[allocation type]
FROM pto_compli_timeoff
GROUP BY userid, [request status],[scheduled date],[allocation type]

SELECT *
FROM pto_requests

ALTER TABLE pto_requests
ADD COLUMN requestStatus cichar(12) constraint NOT NULL position 4
ADD COLUMN requestType cichar(24) constraint NOT NULL position 5

DELETE FROM pto_requests;
INSERT INTO pto_requests

select userid, [scheduled date],MAX(hours) AS hours,[request status],[allocation type]
FROM pto_compli_timeoff
GROUP BY userid, [request status],[scheduled date],[allocation type]

SELECT *
FROM pto_compli_timeoff
WHERE userid = '1113965' AND [scheduled date] = '11/03/2014'

SELECT * FROM pto_compli_timeoff

/*
execute procedure sp_DropReferentialIntegrity('ptoEmployees-pto_requests');
DROP TABLE pto_requests;     

*/  

CREATE TABLE pto_requests (
  employeeNumber cichar(7) constraint NOT NULL,
  theDate date constraint NOT NULL,
  hours numeric(6,2) constraint NOT NULL,
  requestStatus cichar(12) constraint NOT NULL,
  requestType cichar(24) constraint NOT NULL,
  constraint pk primary key (employeeNumber, theDate, requestType)) IN database;

EXECUTE PROCEDURE sp_CreateIndex90( 'pto_requests', 'pto_requests.adi', 'FK', 'employeeNumber', '', 2, 1024, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 'pto_requests', 'pto_requests.adi', 'theDate', 'theDate', '', 2, 1024, '' );
-- UPDATE & DELETE: cascade
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoEmployees-pto_requests', 'ptoEmployees', 'pto_requests', 'FK', 1, 1, NULL, '', '');

DELETE FROM pto_requests;
INSERT INTO pto_requests (employeeNumber,theDate,hours,requestStatus,requestType)
select left(userid, 7) AS employeeNumber, [scheduled date] AS theDate,MAX(hours) AS hours,
  LEFT([request status], 12) AS requestStatus,LEFT([allocation type], 24) AS requestType
FROM pto_compli_timeoff a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
WHERE b.employeenumber IS NOT NULL 
GROUP BY userid, [request status],[scheduled date],[allocation type];


-- same date, same employee, same type, different status
SELECT employeenumber, thedate, requesttype FROM (
select left(userid, 7) AS employeeNumber, [scheduled date] AS theDate,MAX(hours) AS hours,
  LEFT([request status], 12) AS requestStatus,LEFT([allocation type], 24) AS requestType
FROM pto_compli_timeoff a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
WHERE b.employeenumber IS NOT NULL 
GROUP BY userid, [request status],[scheduled date],[allocation type]
) x GROUP BY employeenumber,thedate,requesttype
HAVING COUNT(*) > 1

DELETE 
FROM pto_compli_timeoff
WHERE userid = '1135710'
  AND [scheduled date] = '12/03/2014'
  AND [request status] = 'Pending'

-- 11/16 use pto_tmp_compli_timeoff
ALTER PROCEDURE pto_update_requests()
BEGIN
/*
*/
BEGIN TRANSACTION;
TRY 
DELETE FROM pto_requests;
INSERT INTO pto_requests (employeeNumber,theDate,hours,requestStatus,requestType)
select left(userid, 7) AS employeeNumber, 
  cast(trim(substring(scheduledDate,6,2)) + '/' + trim(right(TRIM(scheduledDate), 2)) + 
    '/' + LEFT(scheduledDate, 4) AS sql_date) AS theDate,
  MAX(hours) AS hours, LEFT(requestStatus, 12) AS requestStatus,
  LEFT(allocationType, 24) AS requestType
FROM pto_tmp_compli_timeoff a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
WHERE b.employeenumber IS NOT NULL 
GROUP BY userid, requestStatus, scheduledDate, allocationType;
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  
END;