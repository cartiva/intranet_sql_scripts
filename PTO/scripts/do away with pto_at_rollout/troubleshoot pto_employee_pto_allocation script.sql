SELECT *
FROM pto_employee_pto_allocation
ORDER BY employeenumber, fromdate

problems: 110060
          1100750
          
SELECT employeenumber, hours
FROM pto_employee_pto_allocation
GROUP BY employeenumber, hours
HAVING COUNT(*) > 1       

SELECT *
FROM pto_employee_pto_category_dates
WHERE employeenumber = '186210'

SELECT *
FROM pto_employee_pto_allocation
WHERE employeenumber = '186210'

SELECT *
FROM (
SELECT employeenumber, hours
FROM pto_employee_pto_allocation
GROUP BY employeenumber, hours
HAVING COUNT(*) > 1) a
INNER JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
-- it IS NOT ALL category 0 at rollout, just a shitload of them
select *
FROM pto_at_rollout a
LEFT JOIN (
SELECT employeenumber, hours
FROM pto_employee_pto_allocation
GROUP BY employeenumber, hours
HAVING COUNT(*) > 1) b on a.employeenumber = b.employeenumber
WHERE ptocategory = 0
 AND b.employeenumber IS NULL 
 
select *
FROM ptoemployees a
LEFT JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
LEFT JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
  AND c.fromdate > pto2014start
LEFT JOIN pto_categories d on c.ptocategory = d.ptocategory  
WHERE a.employeenumber = '178200'  
ORDER BY c.ptoCategory

-- YIKES, here are a shitload of problemas
-- these were the cat 0 problems
-- !!!!!!!!!!   
select *
FROM pto_employee_pto_allocation
WHERE fromdate > thrudate

-- 10/15 cat 0 fixed
-- these 6 remain AND THEY ARE NOT A PROBLEM, simply a repeating of pto hours
-- allocated over more than one time period, ALL HAVING to DO with cat3 AND cat4
-- believe me, they are ok
SELECT employeenumber, hours
FROM pto_employee_pto_allocation
GROUP BY employeenumber, hours
HAVING COUNT(*) > 1  

SELECT *
FROM pto_employee_pto_allocation  
WHERE employeenumber IN (
  SELECT employeenumber
  FROM pto_employee_pto_allocation
  GROUP BY employeenumber, hours
  HAVING COUNT(*) > 1)
ORDER BY employeenumber, fromdate  

-- last(?) problem, IF someone IS cat 4 at rollout, pto_employee_pto_allocation 
-- will have a single row, AND the thrudate for that row will NOT be correct

SELECT *
FROM pto_employee_pto_allocation
WHERE employeenumber IN (
  SELECT employeenumber
  FROM pto_employee_pto_allocation
  GROUP BY employeenumber
  HAVING COUNT(*) = 1)
  
-- yikes, this looks problematic AS well  
SELECT *
FROM pto_employee_pto_allocation a
LEFT JOIN ptoemployees b on a.employeenumber = b.employeenumber
WHERE a.employeenumber IN (
  SELECT employeenumber
  FROM pto_employee_pto_allocation
  GROUP BY employeenumber
  HAVING COUNT(*) = 2)  
ORDER BY a.employeenumber, a.fromdate  

emp         anniv        FROM         thru       hours
is
1102195     03/03/03     01/01/14     03/02/15   177
                         03/03/15     03/02/2104 192
s/b                                               
1102195     03/03/03     01/01/14     03/02/15   177
                         03/03/15     03/02/23   152
                         03/03/23     03/02/99   192                    

-- yikes, AND this too
SELECT *
FROM pto_employee_pto_allocation a
LEFT JOIN ptoemployees b on a.employeenumber = b.employeenumber
WHERE a.employeenumber IN (
  SELECT employeenumber
  FROM pto_employee_pto_allocation
  GROUP BY employeenumber
  HAVING COUNT(*) = 3)  
ORDER BY a.employeenumber, a.fromdate  

emp         anniv        FROM         thru       hours
is
110052     11/01/10      01/01/14     10/31/15   205
                         11/01/15     10/31/30   152
                         11/01/30     10/31/99   192
s/b                                               
110052     11/01/10      01/01/14     10/31/15   205
                         11/01/15     10/31/20   112
                         11/01/20     10/31/30   152
                         11/01/30     10/31/99   192                         
                         
-- i believe this query picks out those with a problem
-- !!!!!!!!!!   
SELECT a.*, year(a.fromdate) - year(b.ptoAnniversary) AS tenure
FROM pto_employee_pto_allocation a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
WHERE year(a.fromdate) - year(b.ptoAnniversary) NOT IN (
  SELECT fromYearsTenure
  FROM pto_categories)
-- AND year(fromdate) <> 2014
AND year(fromdate) NOT IN (2014,2015)
ORDER BY a.employeenumber, a.fromdate   

------------------------------------------------------------------------------
-- ALL the above IS ok
-- new problemas

-- uh oh
-- every emp should have a row WHERE fromdate < today
-- these are ALL 2013 ptoAnniversary AND change to ptocat 1 BETWEEN now AND 12/31/14
SELECT COUNT(DISTINCT employeenumber) FROM (
SELECT a.*, b.ptoAnniversary
FROM pto_employee_pto_allocation a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
WHERE NOT EXISTS (
  SELECT 1
  FROM pto_employee_pto_allocation
  WHERE employeenumber = a.employeenumber
    AND fromdate < curdatE()) 
) x      
    
-- !!!!!!!!!!       
SELECT *
FROM pto_at_rollout a
WHERE NOT EXISTS (
  select 1
  from pto_at_rollout
  where employeenumber = a.employeenumber
    and pto2014start < curdate())    
    
/*
ok, changed  pto_at_rollout so that now passes, but
say, 11020, this IS what i now have
is   
110020     12/09/13      01/01/14     12/08/15   0
                         12/09/15     12/28/23   112
                         12/09/23     12/08/33   152
                         12/09/33     12/08/99   192    
s/b                        
110020     12/09/13      rollout      12/08/14   0
                         12/09/14     12/08/15   72
                         12/09/15     12/28/23   112
                         12/09/23     12/08/33   152
                         12/09/33     12/08/99   192                          
*/  
-- !!!!!!!!!!                       
SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '110020'
-- ok, now that IS fixed
SELECT x.*, z.ptoanniversary
FROM (
  select *
  FROM pto_employee_pto_allocation a
  WHERE hours = 0) x
LEFT JOIN ( 
  select *
  FROM pto_employee_pto_allocation a
  WHERE hours = 72) y on x.employeenumber = y.employeenumber
LEFT JOIN ptoEmployees z on x.employeenumber = z.employeenumber  
WHERE y.employeenumber IS NULL
ORDER BY x.employeenumber                     

-- fuck me continues
-- second row has funky FROM date
-- think it IS a pto_employee_ issue
the problem IS SELECTing the fromdate FROM pto_employee_pto_category_dates, whereas
  it should be the previous pto_employee_pto_allocation.thrudate + 1
select * from pto_employee_pto_allocation where employeenumber = '1123340'
-- !!!!!!!!
select * FROM pto_employee_pto_allocation WHERE year(fromdate) < 2014
-- ok, reconfigured script to use correct dates
-- looks pretty good

except
2014 -> 2015 FROM off BY 1 day on those with hiredate IN 2013
SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '110020'
fixed with a tweak to ptothru IN pto_at_rollout

-- !!!!!!!!
SELECT employeenumber
FROM (
  SELECT dayofmonth(fromdate), employeenumber
  FROM pto_employee_pto_allocation
  WHERE year(fromdate) <> 2014
  GROUP BY dayofmonth(fromdate), employeenumber
) x
GROUP BY employeenumber
HAVING COUNT(*) > 1

-- !!!!!!!!
SELECT employeenumber
FROM (
  SELECT dayofmonth(thrudate), employeenumber
  FROM pto_employee_pto_allocation
--  WHERE year(fromdate) <> 2014
  GROUP BY dayofmonth(thrudate), employeenumber
) x
GROUP BY employeenumber
HAVING COUNT(*) > 1

-- !!!!!!!!
SELECT employeenumber, fromdate
FROM pto_employee_pto_allocation
GROUP BY employeenumber, fromdate
HAVING COUNT(*) > 1
-- !!!!!!!!
SELECT employeenumber, thrudate
FROM pto_employee_pto_allocation
GROUP BY employeenumber, thrudate
HAVING COUNT(*) > 1

-- scan the data
SELECT a.*, b.ptoanniversary, c.ptoAtRollout
FROM pto_employee_pto_allocation a
LEFT JOIN ptoemployees b on a.employeenumber = b.employeenumber
LEFT JOIN pto_at_rollout c on a.employeenumber = c.employeenumber
ORDER BY a.employeenumber, fromdate

-- one row per employeenumber for today
SELECT employeenumber FROM (
SELECT a.*, b.ptoanniversary
FROM pto_employee_pto_allocation a
LEFT JOIN ptoemployees b on a.employeenumber = b.employeenumber
WHERE curdate() BETWEEN fromdate AND thrudate
) x GROUP BY employeenumber HAVING COUNT(*) > 1

-- one row per employeenumber/date for 2014 -> 2016
SELECT a.thedate, b.employeenumber
FROM dds.day a
LEFT JOIN pto_employee_pto_allocation b on a.thedate BETWEEN b.fromdate AND b.thrudate
WHERE a.thedate BETWEEN '01/01/2014' AND '01/01/2016'
GROUP BY a.thedate, b.employeenumber
HAVING COUNT(*) > 1

