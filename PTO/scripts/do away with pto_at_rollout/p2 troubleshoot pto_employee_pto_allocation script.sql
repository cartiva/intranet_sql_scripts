/* 
10/14/
CLOSE, but this generates a shitload of anomalies
SELECT employeenumber, hours
FROM pto_employee_pto_allocation
GROUP BY employeenumber, hours
HAVING COUNT(*) > 1       

10/15/
this looks good
*/

/*
DECLARE @employeenumber string;

@employeenumber = '1123340';
SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = @employeenumber;

DELETE FROM pto_employee_pto_allocation WHERE employeenumber = @employeenumber;
*/

DECLARE @employeenumber string;
DECLARE @cur CURSOR AS 
  SELECT employeenumber
  FROM ptoEmployees;
  
-- @employeenumber = '150120';
-- DELETE FROM pto_employee_pto_allocation;
OPEN @cur;
TRY 
  WHILE FETCH @cur DO
    @employeeNumber = @cur.employeenumber;
    
    INSERT INTO pto_employee_pto_allocation
    SELECT a.employeenumber, a.pto2014Start, a.ptoThru, //a.pto2015Start - 1, 
      a.ptoAtRollout
    FROM pto_at_rollout a
    INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber    
    WHERE a.employeenumber = @employeenumber;  
     
/*
    INSERT INTO pto_employee_pto_allocation
    SELECT top 1 a.employeenumber, c.fromdate, c.thrudate, d.ptoHours
    FROM ptoemployees a
    INNER JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
    INNER JOIN pto_categories d on c.ptocategory = d.ptocategory 
    WHERE a.employeenumber = @employeenumber
      AND (
        SELECT thruDate + 1
        FROM pto_employee_pto_allocation z
        WHERE employeenumber = @employeenumber
          AND thrudate = (
            SELECT MAX(thruDate)
            FROM pto_employee_pto_allocation
            WHERE employeenumber = z.employeenumber)) BETWEEN c.fromdate AND c.thrudate
    ORDER BY c.ptoCategory;   
*/
    INSERT INTO pto_employee_pto_allocation
    SELECT top 1 a.employeenumber, e.thrudate + 1, c.thrudate, d.ptoHours
    FROM ptoemployees a
    INNER JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
    INNER JOIN pto_categories d on c.ptocategory = d.ptocategory  
    INNER JOIN pto_employee_pto_allocation e on a.employeenumber = e.employeenumber
      AND e.thrudate = (
        SELECT MAX(thruDate)
        FROM pto_employee_pto_allocation
        WHERE employeenumber = @employeenumber)
    WHERE a.employeenumber = @employeenumber
      AND e.thrudate + 1 BETWEEN c.fromdate AND c.thrudate
    ORDER BY c.ptoCategory;
    
    INSERT INTO pto_employee_pto_allocation      
    SELECT top 1 a.employeenumber, e.thrudate + 1, c.thrudate, d.ptoHours
    FROM ptoemployees a
    INNER JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
    INNER JOIN pto_categories d on c.ptocategory = d.ptocategory  
    INNER JOIN pto_employee_pto_allocation e on a.employeenumber = e.employeenumber
      AND e.thrudate = (
        SELECT MAX(thruDate)
        FROM pto_employee_pto_allocation
        WHERE employeenumber = @employeenumber)
    WHERE a.employeenumber = @employeenumber
      AND e.thrudate + 1 BETWEEN c.fromdate AND c.thrudate
    ORDER BY c.ptoCategory;
    
    INSERT INTO pto_employee_pto_allocation
    SELECT top 1 a.employeenumber, e.thrudate + 1, c.thrudate, d.ptoHours
    FROM ptoemployees a
    INNER JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
    INNER JOIN pto_categories d on c.ptocategory = d.ptocategory  
    INNER JOIN pto_employee_pto_allocation e on a.employeenumber = e.employeenumber
      AND e.thrudate = (
        SELECT MAX(thruDate)
        FROM pto_employee_pto_allocation
        WHERE employeenumber = @employeenumber)
    WHERE a.employeenumber = @employeenumber
      AND e.thrudate + 1 BETWEEN c.fromdate AND c.thrudate
    ORDER BY c.ptoCategory;
    
    INSERT INTO pto_employee_pto_allocation
    SELECT top 1 a.employeenumber, e.thrudate + 1, c.thrudate, d.ptoHours
    FROM ptoemployees a
    INNER JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
    INNER JOIN pto_categories d on c.ptocategory = d.ptocategory  
    INNER JOIN pto_employee_pto_allocation e on a.employeenumber = e.employeenumber
      AND e.thrudate = (
        SELECT MAX(thruDate)
        FROM pto_employee_pto_allocation
        WHERE employeenumber = @employeenumber)
    WHERE a.employeenumber = @employeenumber
      AND e.thrudate + 1 BETWEEN c.fromdate AND c.thrudate
    ORDER BY c.ptoCategory;        
  END WHILE;
FINALLY
  CLOSE @cur;
END TRY;  
  