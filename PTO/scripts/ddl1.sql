DROP TABLE compliDepartments;
CREATE TABLE compliDepartments (
  market cichar (12) constraint NOT NULL,
  store cichar(3) constraint NOT NULL,
  department cichar(24) constraint NOT NULL,
  fromDate date,
  thruDate date constraint NOT NULL default '12/31/9999',
  constraint pk primary key (market, store, department)) IN database;

DELETE FROM compliDepartments;
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','All', 'All', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','All', 'Guest Experience', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','All', 'BDC', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','All', 'Office', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','RY1', 'Body Shop', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','RY1', 'Detail', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','RY1', 'Car Wash', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','RY1', 'Main Shop', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','RY1', 'Parts', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','RY1', 'PDQ', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','RY1', 'Sales', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','RY2', 'Main Shop', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','RY2', 'PDQ', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','RY2', 'Sales', '01/01/2014');
INSERT INTO compliDepartments (market, store, department, fromDate)
values('Grand Forks','RY2', 'Service', '01/01/2014');

CREATE TABLE compliCompare ( 
      employeeNumber CIChar( 9 ),
      firstName CIChar( 25 ),
      lastName CIChar( 25 ),
      compliSupervisor CIChar( 51 ),
      compliJob CIChar( 40 ),
      fbDepartment CIChar( 24 ),
      fbPosition CIChar( 30 ),
      arkSupervisor CIChar( 52 ),
      arkDepartmentCode CIChar( 3 ),
      arkDepartment CIChar( 30 ),
      arkDistCode CIChar( 4 ),
      arkDistDescription CIChar( 25 ),
      arkJob CIChar( 30 )) IN DATABASE;

UPDATE compliCompare
SET employeenumber = TRIM(employeenumber);
  
DROP TABLE compliTmpPositions;
CREATE TABLE compliTmpPositions ( 
      fbStore cichar(3),
      fbDepartment cichar(24),
      fbPosition CIChar( 30 ),
      fbPositionType CIChar( 24 ),
      fbPositionTypeClassification CIChar( 12 )) IN DATABASE;  

SELECT COUNT(*) FROM (
SELECT DISTINCT fbStore,fbDepartment,fbPosition,fbPositionType,fbPositionTypeClassification
FROM compliTmpPositions) x;         
      


DROP TABLE compliPositions;

CREATE TABLE compliPositions ( 
      fbStore cichar(3),
      fbDepartment cichar(24),
      fbPosition CIChar( 30 ),
      fbPositionType CIChar( 24 ),
      fbPositionTypeClassification CIChar( 12 )) IN DATABASE;  

    
SELECT * FROM compliPositions

SELECT trim(fbStore) + ' ' + trim(fbDepartment) + ' ' + trim(fbPosition)
   + ' ' + trim(fbPositionType) + ' ' + trim(fbPositionTypeClassification)
FROM compliPositions

select DISTINCT trim(fbStore) + ' ' +  fbDepartment FROM compliPositions

SELECT *
FROM ((
  SELECT 
    CASE WHEN fbStore = 'All' AND fbDepartment = 'BDC' THEN 
      trim(fbPosition) + ' ' + trim(fbPositionType) + ' ' + trim(fbPositionTypeClassification) END AS BDC
  FROM compliPositions) a,
  (SELECT        
    CASE WHEN fbStore = 'All' AND fbDepartment = 'Guest Experience' THEN 
      trim(fbPosition) + ' ' + trim(fbPositionType) + ' ' + trim(fbPositionTypeClassification) END AS Guest
  FROM compliPositions) b   
  
SELECT trim(fbPosition) + ' ' + trim(fbPositionType) + ' ' + trim(fbPositionTypeClassification)
FROM compliPositions
WHERE fbStore = 'ry1' AND fbDepartment = 'body shop'

CREATE TABLE compliTmpFbPositions ( 
      employeenumber cichar(9),
      fbDepartment cichar(30),
      fbPosition CIChar( 80 )) IN DATABASE;  


-- SELECT employeenumber FROM (
SELECT distinct a.employeenumber, a.firstName, a.lastName, a.compliSupervisor, a.compliJob,
  b.pyDeptCode, b.pyDept, b.distCode, b.distribution, d.yrtext, 
  (SELECT TRIM(firstname) + ' ' + trim(lastname) FROM edwEmployeeDim 
    WHERE employeenumber = c.yrmgrn AND currentrow = true) AS mgr,
    e.fbDepartment, e.fbPosition
FROM compliCompare a
LEFT JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
LEFT JOIN stgArkonaPYPRHEAD c on b.employeenumber = c.yrempn
  AND c.yrjobd <> ''
  AND b.storecode = c.yrco#
LEFT JOIN stgArkonaPYPRJOBD d on c.yrjobd = d.yrjobd
  AND d.yrtext NOT IN ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE', '')
  AND c.yrco# = d.yrco#  
LEFT JOIN compliTmpFbPositions e on a.employeenumber = e.employeenumber  
--) x GROUP BY employeenumber HAVING COUNT(*) > 1  
ORDER BY distCode

UPDATE compliCompare
  SET fbDepartment = x.fbDepartment,
      fbPosition = x.fbPosition,
      arkSupervisor = x.mgr,
      arkDepartmentCode = x.pyDeptCode,
      arkDepartment = x.pydept,
      arkDistCode = x.distCode,
      arkDistDescription = x.distribution,
      arkJob = x.yrtext
FROM (
  SELECT distinct a.employeenumber, a.firstName, a.lastName, a.compliSupervisor, a.compliJob,
    b.pyDeptCode, b.pyDept, b.distCode, b.distribution, d.yrtext, 
    (SELECT TRIM(firstname) + ' ' + trim(lastname) FROM edwEmployeeDim 
      WHERE employeenumber = c.yrmgrn AND currentrow = true) AS mgr,
      e.fbDepartment, e.fbPosition
  FROM compliCompare a
  LEFT JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  LEFT JOIN stgArkonaPYPRHEAD c on b.employeenumber = c.yrempn
    AND c.yrjobd <> ''
    AND b.storecode = c.yrco#
  LEFT JOIN stgArkonaPYPRJOBD d on c.yrjobd = d.yrjobd
    AND d.yrtext NOT IN ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE', '')
    AND c.yrco# = d.yrco#  
  LEFT JOIN compliTmpFbPositions e on a.employeenumber = e.employeenumber) x
WHERE compliCompare.employeenumber = x.employeenumber  
      