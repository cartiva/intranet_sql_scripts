-- new HR position
  INSERT INTO ptoPositions (position) values('Assistant');
  INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
  values('HR','Assistant',5,'no_policy_html');
-- 3. auth BY HR Manager
  INSERT INTO ptoAuthorization (authByDepartment,authByPosition, 
    authForDepartment,authForPosition,authByLevel,AuthForLevel)
  values('HR','Manager','HR','Assistant',4,5);   
  
-- new Detail position
-- Position Prep Technician already exists
  INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
  values('Detail','Prep Technician',5,'detail_policy_html');
-- 3. auth BY HR Manager
  INSERT INTO ptoAuthorization (authByDepartment,authByPosition, 
    authForDepartment,authForPosition,authByLevel,AuthForLevel)
  values('Detail','Assistant Manager','Detail','Prep Technician',4,5);     

BEGIN TRANSACTION;  
TRY
-- 1. DELETE termed folks
  DELETE 
--  SELECT *
  FROM employeeAppAuthorization
  WHERE username IN (
    SELECT username
  	FROM tpemployees
  	WHERE employeenumber IN (
      SELECT a.employeenumber
      FROM ptoEmployees a
      INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
        AND b.currentrow = true
        AND b.active = 'Term'));    
  DELETE
  -- SELECT * 
  FROM tpemployees
  WHERE employeenumber IN (
    SELECT a.employeenumber
    FROM ptoEmployees a
    INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
      AND b.currentrow = true
      AND b.active = 'Term');  
  DELETE
  -- SELECT *
  FROM ptoEmployees 
  WHERE employeenumber IN (
    SELECT a.employeenumber
    FROM ptoEmployees a
    INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
      AND b.currentrow = true
      AND b.active = 'Term'); 
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;         
      
      
      
      
      
      