pdq
ned euliss no longer the manager, now a main shop service writer
ALL pdq pto authorization to be done BY pdq asst mgr (nick neumann)
nick neumann authorized by ry1 service:director (andrew neumann)

Ned Euliss emp# 141060

SELECT fullname, position
FROM tpemployees a
INNER JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
WHERE b.department = 'ry1 pdq'



SELECT *
FROM ptoEmployees a
INNER JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
WHERE a.employeenumber = (
  SELECT employeenumber
  FROM tpemployees
  WHERE lastname = 'euliss')

UPDATE  
SELECT * FROM ptoPositionFulfillment
WHERE employeenumber = '141060'  

-- positions authorized BY PDQ Manager  
SELECT *
FROM ptoAuthorization
WHERE authByDepartment = 'RY1 PDQ'
  AND authByPosition = 'Manager'  

change these to Asst Manager

UPDATE ptoAuthorization
SET authByPosition = 'Assistant Manager'
WHERE authForDepartment = 'RY1 PDQ'
  AND authForPosition IN ('Advisor','Upperbay Technician','Lowerbay Technician');

AND change asst mgr authorizer to service director

UPDATE ptoAuthorization
SET authByDepartment = 'RY1 Service',
    authByPosition = 'Director'
-- SELECT * FROM ptoAuthorization
WHERE authForDepartment = 'RY1 PDQ'
  AND authForPosition = 'Assistant Manager' ; 
  
now that there are no positions authorized BY pdq manager
SELECT *
FROM ptoAuthorization
WHERE authByDepartment = 'RY1 PDQ'
  AND authByPosition = 'Manager'  
change Neds position  

UPDATE ptoPositionFulfillment
SET department = 'RY1 Drive',
    position = 'Advisor'
WHERE employeenumber = '141060';

/* 
authorizer positions with out fulfillment 
SELECT *
FROM ptoAuthorization a
LEFT JOIN ptoDepartmentPositions b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
LEFT JOIN ptoPositionFulfillment c on b.department = c.department
  AND b.position = c.position   
WHERE c.employeenumber IS NULL   
*/

-- Body Shop
Mark Steinke IS now a writer, ALL body shop pto authorized BY randy sattler

SELECT * FROM tpemployees WHERE lastname = 'steinke'

SELECT *
FROM employeeappauthorization
WHERE appname = 'pto' 
  AND (
    username LIKE '%sattler%'
    OR username LIKE '%steinke%'
    OR username LIKE '%crose%')
ORDER BY username  

DELETE 
FROM employeeAppAuthorization
WHERE username = 'msteinke@rydellcars.com'
  AND appname = 'pto';
  
INSERT INTO employeeAppAuthorization
SELECT 'msteinke@rydellcars.com', appname,appseq,appcode,approle,functionality,
  appDepartmentKey
FROM employeeAppAuthorization
WHERE username = 'crose@rydellcars.com'
  AND appname = 'pto'; 
  
/*
Will you please make the changes in Vision so that all digital sales consultants 
and all sales product specialists have the digital team leader (Bryn Seay) 
as their PTO admin? Bryn will also need the PTO admin function.
*/

SELECT * FROM tpemployees WHERE lastname = 'seay'
bseay@rydellcars.com
Gosioux19
1124625

new position: RY1 Sales:Digital Team Leader

SELECT * FROM ptopositions WHERE position LIKE '%digi%'

INSERT INTO ptoPositions (position) values ('Digital Team Leader');
INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
values ('RY1 Sales','Digital Team Leader',3,'no_policy_html');

UPDATE ptoPositionFulfillment
-- SELECT * FROM ptoPositionFulfillment
SET position = 'Digital Team Leader'
WHERE employeenumber = '1124625';

UPDATE ptoAuthorization
SET authByDepartment = 'RY1 Sales',
    authByPosition = 'Digital Team Leader'
-- select * FROM ptoAuthorization
WHERE authforposition IN ('Digital Sales Consultant', 'Sales Product Specialist');

INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
  authForPosition)
values ('HR','Director','RY1 Sales','Digital Team Leader');  

select *
FROM employeeAppAuthorization
WHERE appname = 'pto'
  AND (username LIKE 'nneumann%' OR username LIKE 'bseay%')
  
DELETE 
FROM employeeAppAuthorization  
WHERE username = 'bseay@rydellcars.com'
  AND appname = 'pto';
  
INSERT INTO employeeAppAuthorization 
SELECT 'bseay@rydellcars.com',appname,appseq,appcode,approle,functionality,appDepartmentKey
FROM employeeAppAuthorization  
WHERE username = 'nneumann@rydellcars.com'
  AND appname = 'pto';