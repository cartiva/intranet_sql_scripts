7/28/15
v3.9.1

Jon, would you please switch Mike Delohery so that he is reporting to 
Anthony Michael for PTO rather than Nick Shirek?

which translates to: RY1 Sales:Digital Coordinator auth by RY1:General Sales Manager, not
             RY1 Sales:New Car Manager

BEGIN TRANSACTION;
TRY 
UPDATE ptoDepartmentPositions
SET level = 4
-- SELECT * FROM ptodepartmentpositions
WHERE department = 'RY1 Sales'
  AND position = 'Digital Coordinator';

             
UPDATE ptoAuthorization
SET authByPosition = 'General Sales Manager',
    authbylevel = 3,
    authforlevel = 4
--SELECT * FROM ptoAuthorization      
WHERE authforposition = 'Digital Coordinator';    

COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  
