SELECT *
FROM ptodepartmentpositions
where department = 'ry2 sales'

SELECT * FROM ptopositions ORDER BY position

ALL sales consultants auth BY lear
bjerk, christopher can now be a team leader
bushaw should be team 1 NOT PT (per compli)
ALL sales consultants should be Sales Consultant, NOT team 1, team 2
ALL sales consultants bumped to level 3

SELECT a.username, a.password, a.employeenumber, a.firstname, a.lastname, c.department, c.position, 
  d.authbyposition, f.lastname
FROM tpemployees a
INNER JOIN ptoemployees b on a.employeenumber = b.employeenumber
INNER JOIN ptoPositionFulfillment c on b.employeenumber = c.employeenumber
INNER JOIN ptoAuthorization d on c.department = d.authfordepartment AND c.position = d.authforposition
INNER JOIN ptopositionfulfillment e on d.authbydepartment = e.department AND d.authbyposition = e.position
INNER JOIN tpemployees f on e.employeenumber = f.employeenumber
WHERE c.department = 'ry2 sales'

termed employees: lautenschlager, gonitzke, price
DELETE FROM tpEmployees: RI takes care of empAppAuth, but NOT ptoEmployees
DELETE FROM ptoEmployees: ri takes care of ALL other pto tables

select * from ptodepartmentpositions WHERE department = 'ry2 sales'
select * from ptoauthorization 
SELECT * FROM tpemployees WHERE lastname = 'bjerk'
SELECT * FROM ptoPositionFulfillment WHERE employeenumber = '212120'

BEGIN TRANSACTION;
TRY
-- 1 DELETE lautenschlager, gonitzke, price
  DELETE 
  FROM employeeAppAuthorization
  WHERE username IN (
    SELECT username
	FROM tpemployees
	WHERE employeenumber IN ('283326','242056','2112451'));
  DELETE 
  FROM tpemployees
  WHERE employeenumber IN ('283326','242056','2112451');
  DELETE
  FROM ptoEmployees 
  WHERE employeenumber IN ('283326','242056','2112451');
-- 2 UPDATE chris bjerk to team leader
  UPDATE ptoPositionFulfillment
  SET position = 'Team Leader'
  WHERE employeenumber = '212120';  
-- 3 CREATE RY2 Sales:Sales Consultant
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
  values('RY2 Sales','Sales Consultant',3,'no_policy_html');
-- 4 configure authorization  
  INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
    authForPosition, authByLevel, authForLevel)
  values('RY2','General Manager','RY2 Sales','Sales Consultant',2,3);	
-- 5 CONVERT position for ALL consultants
  UPDATE ptoPositionFulfillment  
  SET position = 'Sales Consultant'
  WHERE (
    (department = 'RY2 Sales' AND position = 'Sales Consultant (PT)')
  	OR
  	(department = 'RY2 Sales' AND position = 'Team 1 Sales Consultant')
  	OR
	  (department = 'RY2 Sales' AND position = 'Team 2 Sales Consultant'));	
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  