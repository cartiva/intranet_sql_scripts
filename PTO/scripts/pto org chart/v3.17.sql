Ned has informed me that his Assistant Manager will be moving into 
another position in the dealership in about a month. 
Will you please move all Upperbay and Lowerbay Technicians so that 
their PTO authorizer is the PDQ Manager?

PDQ Techs now authorized BY PDQ Manager rather than PDQ Assistant Manager

SELECT *
FROM ptoAuthorization
WHERE authForDepartment = 'RY1 PDQ'

UPDATE ptoAuthorization
SET authByPosition = 'Manager'
WHERE authForDepartment = 'RY1 PDQ'
  AND authForPosition IN ('Upperbay Technician','Lowerbay Technician')