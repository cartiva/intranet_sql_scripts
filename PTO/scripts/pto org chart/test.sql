BEGIN TRANSACTION;
TRY 
-- new RY2 Sales position 
  INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
  values('RY2 Sales','General Sales Manager',3,'no_policy_html');
  
-- auth BY GM
  INSERT INTO ptoAuthorization (authByDepartment,authByPosition, 
    authForDepartment,authForPosition,authByLevel,AuthForLevel)
  values('RY2','General Manager','RY2 Sales','General Sales Manager',2, 3);   
  
-- change tom aubul to GSM
  UPDATE ptoPositionFulfillment  
  SET position = 'General Sales Manager'    
  WHERE employeenumber = (
    SELECT employeenumber
    FROM tpEmployees
    WHERE lastname = 'aubol'); 

-- olderbak to team leader
  UPDATE ptoPositionFulfillment
  SET position = 'Team Leader'
  WHERE employeenumber = (
    SELECT employeenumber
    FROM tpemployees
    WHERE lastname = 'olderbak');
    
-- team leader, sales manager no longer authorized anybody
  UPDATE ptoAuthorization
  SET authByDepartment = 'RY2 Sales',
      authByPosition = 'General Sales Manager'
--  select * FROM ptoAuthorization
  WHERE authfordepartment = 'ry2 sales'
    AND (
      authforposition = 'team leader'
      OR
      authforposition = 'sales consultant');      
    
-- remove ry2 sales:sales manager        
-- remove ry2 sales:desk manager  
-- this should, via RI, DELETE rows FROM ptoPositionFulfillment, ptoAuthorization 
  DELETE 
--  SELECT *
  FROM ptoDepartmentPositions
  WHERE department = 'RY2 Sales'
    AND position IN ('Desk Manager','Sales Consultant (PT)',
      'Team 1 Sales Consultant','Team 2 Sales Consultant', 'Sales Manager');
  
-- change levels
  UPDATE ptoDepartmentPositions
  SET level = 4
  WHERE department = 'RY2 Sales'
    AND (
      position = 'team leader'
      OR
      position = 'sales consultant');
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;       