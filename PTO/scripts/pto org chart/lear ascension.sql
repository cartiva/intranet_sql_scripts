
-- 1. 

INSERT INTO ptoDepartments (department)
values ('RY2');
/*
SELECT *
FROM ptoDepartmentPositions
WHERE position = 'general manager'
*/
UPDATE ptoDepartmentPositions
SET department = 'RY2'
WHERE department = 'RY2 Sales'
  AND position = 'General Manager';
/*  
SELECT * FROM ptoPositionFulfillment WHERE employeenumber = (SELECT DISTINCT employeenumber FROM dds.edwEmployeeDim WHERE lastname = 'lear' AND currentrow = true AND active = 'active')  
SELECT * FROM ptoClosure WHERE ancestorDepartment = 'RY2'
*/
-- 2.
INSERT INTO ptoPositions (position)
values ('Desk Manager');
INSERT INTO ptoDepartmentPositions (department, position, level, ptoPolicyHtml)
values('RY2 Sales','Desk Manager',3,'no_policy_html');

INSERT INTO ptoAuthorization (authByDepartment, authByPosition, authForDepartment,
  authForPosition, authByLevel, authForLevel)
values ('RY2','General Manager','RY2 Sales','Desk Manager',2,3);
/*
SELECT * from ptoDepartmentPositions WHERE department = 'ry2 sales'

-- no one assigned to these positions
SELECT * 
FROM ptoDepartmentPositions a
LEFT JOIN ptoPositionFulfillment b on a.department = b.department 
  AND a.position = b.position
WHERE b.department IS NULL;

-- ptoEmployees with no position
SELECT *
FROM ptoEmployees a
LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
WHERE b.employeenumber IS NULL 

SELECT * FROM ptoPositionFulfillment WHERE employeenumber = (SELECT DISTINCT employeenumber FROM dds.edwEmployeeDim WHERE lastname = 'tweten' AND currentrow = true AND active = 'active')  

*/
-- tweten
DELETE FROM ptoPositionFulfillment
WHERE employeeNumber = '242460';
INSERT INTO ptoPositionFulfillment (department,position,employeeNumber)
values('RY2 Sales','Desk Manager','242460');

-- 3.
INSERT INTO ptoPositions (position)
values ('Fixed Director');

UPDATE ptoDepartmentPositions
SET department = 'RY1',
    position = 'Fixed Director'
WHERE department = 'market fixed'
  AND position = 'director';
  
-- 4.
/*
SELECT *
FROM ptoAuthorization
WHERE authForDepartment = 'RY2 main shop'
  AND authForPosition = 'manager'
*/  


UPDATE ptoAuthorization
SET authByDepartment = 'RY2',
    authByPosition = 'General Manager'
WHERE authForDepartment = 'RY2 main shop'
  AND authForPosition = 'manager';    
      
-- 5.
/*
helgeson: 264220 : shipping & receiving
johnson : 274225 : sales consultant
olson : 2106350 : inventory manager, no longer a position, chad becomes parts mgr

select a.*, b.name, b.pydeptcode, b.pydept, b.distcode, b.fullparttime
FROM ptoPositionFulfillment a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'active'
WHERE a.department = 'parts'
  AND b.storecode = 'RY2'
*/
BEGIN TRANSACTION;
TRY   
UPDATE ptoDepartments
SET department = 'RY1 Parts'
WHERE department = 'Parts';  

INSERT INTO ptoDepartments (department)
values ('RY2 Parts');

INSERT INTO ptoDepartmentPositions (department,position, level,ptoPolicyHtml)
values ('RY2 Parts','Manager',3,'parts_policy_html');

INSERT INTO ptoDepartmentPositions (department,position, level,ptoPolicyHtml)
values ('RY2 Parts','Sales Consultant',4,'parts_policy_html');

INSERT INTO ptoDepartmentPositions (department,position, level,ptoPolicyHtml)
values ('RY2 Parts','Shipping & Receiving',4,'parts_policy_html');
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
  
BEGIN TRANSACTION;
TRY  
UPDATE ptoPositionFulfillment
SET department = 'RY2 Parts',
    position = 'Sales Consultant'
WHERE employeenumber = '274225';

UPDATE ptoPositionFulfillment
SET department = 'RY2 Parts',
    position = 'Shipping & Receiving'
WHERE employeenumber = '264220';

UPDATE ptoPositionFulfillment
SET department = 'RY2 Parts',
    position = 'Manager'
WHERE employeenumber = '2106350';  

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 

BEGIN TRANSACTION;
TRY  
INSERT INTO ptoAuthorization (authByDepartment,authByPosition,AuthForDepartment,
  AuthForPosition,authByLevel,authForLevel)
values('RY2','General Manager','RY2 Parts','Manager',2,3);

INSERT INTO ptoAuthorization (authByDepartment,authByPosition,AuthForDepartment,
  AuthForPosition,authByLevel,authForLevel)
values('RY2 Parts','Manager','RY2 Parts','Sales Consultant',3,4);

INSERT INTO ptoAuthorization (authByDepartment,authByPosition,AuthForDepartment,
  AuthForPosition,authByLevel,authForLevel)
values('RY2 Parts','Manager','RY2 Parts','Shipping & Receiving',3,4);
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 

-- 6.
/*
new position ry1 pdq : assistant manager
ADD to ptoDepartmentPositions
ALL pdq techs changed to level 6 
AND auth BY asst mgr
*/
-- new position of ry1 pdq asst mgr
BEGIN TRANSACTION;
TRY
INSERT into ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
values('RY1 PDQ','Assistant Manager',5,'no_policy_html');

UPDATE ptoDepartmentPositions
SET level = 6
-- SELECT * FROM ptoDepartmentPositions
WHERE department = 'ry1 pdq'
  AND position LIKE '%tech%';

UPDATE ptoAuthorization
SET authByPosition = 'Assistant Manager',
    authByLevel = 5,
    authForLevel = 6 
-- SELECT * FROM ptoAuthorization
WHERE authForDepartment = 'ry1 pdq'
  AND authForPosition LIKE '%tech%';  
  
INSERT INTO ptoAuthorization (authByDepartment, authByPosition, authForDepartment,
  authForPosition, authByLevel, authForLevel)
values ('RY1 PDQ','Manager','RY1 PDQ','Assistant Manager',4,5); 
  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    

-- 7
/*
ry1 main shop lead tech no longer auth, ALL techs auth BY dispatcher
main shop lead tech to level 5
*/

BEGIN TRANSACTION;
TRY

UPDATE ptoDepartmentPositions
SET level = 5
--SELECT * FROM ptoDepartmentPositions
WHERE department = 'ry1 main shop'
  AND position = 'Lead Technician (A)';

UPDATE ptoAuthorization
SET authByDepartment = 'RY1 Main Shop',
    authByPosition = 'Dispatcher',
    authByLevel = 4,
    authForLevel = 5
-- SELECT * FROM ptoAuthorization
WHERE authForDepartment = 'ry1 main shop'
  AND authForPosition LIKE '%tech%';
  
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    

-- after ALL these are done should run ptoClosure script;