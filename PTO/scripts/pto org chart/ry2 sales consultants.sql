10/16/14
RY2 Sales:Team Leader IS a position that can be fulfilled be multiple people,
  AND current structure shows this position AS pto admin for sales cons
AS such, this means that there IS no way FROM this structure to tell
  who the pto admin IS for a Sales Consultant
move sales consultant to RY2 Sales:Sales Manager
got Ben Fs ok  
  
-- yikes, first change ptoDeparmentPositions.level
-- does this need some RI, thinking yep, but i don't really want level to be
-- part of the ptoDepartmentPosistions PK, which would be required IN advantage
-- fuck me, yet more poorly thought out modelling
-- oh well
-- IN the mean time, this query can be used AS an assertion
SELECT *
FROM ptoDepartmentPositions a
INNER JOIN ptoAuthorization b on a.department = b.authByDepartment
  AND a.position = b.authByPosition
WHERE a.level <> b.authByLevel  
UNION 
SELECT *
FROM ptoDepartmentPositions a
INNER JOIN ptoAuthorization b on a.department = b.authForDepartment
  AND a.position = b.authForPosition
WHERE a.level <> b.authForLevel; 

/*
thinking of doing this AS an admin, no way to know what the new level would be
outside the context of ptoAuthorization, so the new level would have to be 
determined AS a result of configuring (visually) IN the admin page AND derived
FROM the subsequent reorgainzation (who admins the position IN the new structure)
*/
SELECT * -- current level 4, no change IN level, just the authorization
FROM ptoDepartmentPositions
WHERE department = 'RY2 Sales' AND position = 'sales consultant'

-- 10/6, fuuuuuuck, just hard code the necesary changes, go through this  few
-- times AND maybe i will figure out what admin functionality looks LIKE



2. UPDATE ptoAuthorization AS requd

  SELECT *
  FROM ptoAuthorization
  WHERE authForDepartment = 'ry2 sales'
    AND authForPosition in ('sales consultant', 'sales consultant (pt)')    

    
  UPDATE ptoAuthorization
  SET authByPosition = 'Sales Manager' 
  WHERE authForDepartment = 'ry2 sales'
    AND authForPosition in ('sales consultant', 'sales consultant (pt)'); 
    
      
3. repopulate ptoClosure
  -- fuck updating AND editing, just regenerate anytime ptoAuthorization changes,
  -- only takes ~200 msec
  DECLARE @i integer;
  DECLARE @cur CURSOR AS
    SELECT distinct uc.ancestorDepartment, uc.ancestorPosition,
      u.authForDepartment, u.authForPosition
    FROM ptoClosure uc, ptoAuthorization u
    WHERE uc.descendantDepartment = u.authByDepartment 
      AND uc.descendantPosition = u.authByPosition   
    AND uc.depth = @i - 1;
  @i = 1;    
  DELETE FROM ptoClosure;
  -- self referencing row (depth = 0) for each position that IS NOT a leaf
  INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
    descendantPosition, depth)
  SELECT DISTINCT authByDepartment, authByPosition, authByDepartment, authByPosition, 0 
  FROM ptoAuthorization; 
  -- root   
  UPDATE ptoClosure
  SET topmost = true
  WHERE descendantPosition = 'board of directors';
  -- AND one self referencing (depth = 0) row for each leaf node
  INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
    descendantPosition, depth, lowest)
  SELECT authForDepartment, authForPosition, authForDepartment, authForPosition, 0, true 
  FROM ptoAuthorization a
  WHERE NOT EXISTS (
    SELECT 1
    FROM  ptoAuthorization
    WHERE authByDepartment = a.authForDepartment
      AND authByPosition = a.authForPosition);
      
  WHILE @i < (SELECT MAX(authForLevel) + 1 FROM ptoAuthorization) DO 
  OPEN @cur;
  TRY
    WHILE FETCH @cur DO
      INSERT INTO ptoClosure values(@cur.ancestorDepartment, @cur.ancestorPosition,
        @cur.authForDepartment, @cur.authForPosition, @i, false, false);
    END WHILE;
  FINALLY
    CLOSE @cur;
  END TRY;  
  @i = @i + 1;
  END WHILE;      
      