4/28/15
v3.7

structural changes

personnel changes

-- positions NOT currently filled
SELECT *
FROM ptoDepartmentPositions a
LEFT JOIN (
  SELECT department, position
  FROM ptoPositionFulfillment
  GROUP BY department, position) b on a.department = b.department AND a.position = b.position
WHERE b.department IS NULL   

SELECT a.department, a.position, coalesce(b.theCount, 0)
FROM ptoDepartmentPositions a
LEFT JOIN (
  SELECT department, position, COUNT(*) AS theCount
  FROM  ptoPositionFulfillment 
  GROUP BY department, position) b on a.department = b.department AND a.position = b.position
ORDER BY a.department, a.position  

this IS going to be interesting, eg, ben foster, still the HR Director
but also a sales team leader - never mind, his only role IN pto Auth IS
HR Director, team leaders DO NOT auth PTO, new/used managers do.

1.
No more fixed director, cahalan IS now RY1:GM

Jim Lynch replaced BY Anthony Michaels AS RY1:GSM, DO NOT know Jim new role

2. UC Sales Manager: Ben Knudson

3. NC Sales Manager: Nick Shirek

4. UC Team Leaders: RJ, James Weber, Al Berry

5. NC Team Leaders: Tom Aubul,Ben Foster, Josh Barker 

-- 5/11/15

been struggling with this for several days now, time to tuck it up AND move
the fuck on
1. need to clearly define structural changes separate FROM personnel assignments
     AND THEN fucking make the changes, 
     THEN figure out what to DO about personnel assignments

this looks LIKE a reasonable list of possible structural changes
FROM http://schinckel.net/2014/11/22/postgres-tree-shootout-part-1%3A-introduction./
these ALL will reference rows IN ptoAuthorization

--< structure -------------------------------------------------------structure-<
Insertions
1. Insert a single leaf node
2. Insert a single root node (all existing root nodes will then point to this)
3. Insert a single node partway through the tree, with the �replaced� node 
     becoming a child of this (and this one keeps it�s children)
4. Insert a single node partway through the tree, with this node�s parent�s 
     existing children all becoming children of the new node.
5. INSERT a single node partway through the tree, assign parent & children

Removals
1. Remove a single leaf node
2. Remove a single root node (all children of this are promoted to root nodes)
3. Remove a single node partway through the tree: all children of this node 
      then have their grand-parent as their parent.
4. Remove a subtree (a single non-root node and it�s descendants)
5. Remove a whole tree (a single root node and all descendants)
6. Remove all descendants of a specific node (but not the node itself)
++
7. Remove a single node partway through the tree: all children of this node then have 
     a single specified existing node (not the grandparent) as their parent.
8. Remove a single node partway through the tree: all children of this node then have 
     a new single specified node as their parent. Requires new node to be added
     prior to the removal    

Moves
1. Move a subtree from one parent to another
2. Move a single leaf node to a different parent
3. Move a root node into a tree
4. Make a subtree into a tree (turn a node into a root node).
5. Move all children of a node to a different parent

Modify
1. Change only the name of a node


  
insertions  
  RY1 Sales:Used Car Team Leader (5) level 5, parent: Used Car Sales Manager, children: none
  RY1 Sales:Used Car Sales Manager (5) level 4 parent:RY1 Sales:General Sales Manager,
    children: RY1 Sales:Used Car Team Leader, RY1 Sales:Used Car Sales Consultant
  RY1 Sales:Assistant Finance Director (5) level 4 parent: RY1 Sales:General Sales Manager    
  RY1 Sales:Fleet Sales Consultant (5) level 4 parent: RY1 Sales:General Sales Manager  
  RY1 Sales:New Car Team Leader (5) level 5, parent: New Car Sales Manager, children: none
  RY1 Sales:Sales Trainer (5) level 3, parent: RY1 Sales:General Manager, children: none
  HR:Manager (5) level 4, parent: HR:Director, children: none
  RY2:Custodian (1) level 3, parent: RY2:General MAnager, children: none
  
moves
  RY1 Sales:Finance Manager (2) parent: RY1 Sales:General Sales Manager
  RY1 Sales:New Car Inventory Manager (2) level 5 to level 4, parent: RY1 Sales:General Sales Manager
  RY1 Sales:Used Car Inventory Manager (2) level 3 to level 4 parent: RY1 Sales:General Sales Manager,

removals:
  RY1:Fixed Director (7) - fuck me, of course, the first one i pick does NOT match
         any of the above categories, so, what IS it THEN
         (7) Remove a single node partway through the tree: ALL children of this node
             will NOT THEN have Market:BOD AS their parent, but RY1:General Mgr                               
  RY1 Sales:Team Leader (1)
  RY1 Sales:sales support (1)
  RY1 Sales:Sales Consultant (1) -- sc are new OR used
  RY1 Sales:Used Car Buyer (8) parent: RY1 Sales:Used Car Sales Mgr  
  RY2:Sales Manager Trainee (1)
  HR:Intern (1)
BEGIN TRANSACTION;
TRY 
-- insertions: positions, departmentPositions
  INSERT INTO ptoPositions values('Used Car Team Leader');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
    values('RY1 Sales','Used Car Team Leader', 5, 'no_policy_html');
  INSERT INTO ptoPositions values('New Car Team Leader');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
    values('RY1 Sales','New Car Team Leader', 5, 'no_policy_html');   
  INSERT INTO ptoPositions values('Used Car Sales Manager');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
    values('RY1 Sales','Used Car Sales Manager', 4, 'no_policy_html');   
  INSERT INTO ptoPositions values('Assistant Finance Director');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
    values('RY1 Sales','Assistant Finance Director', 4, 'no_policy_html'); 
  INSERT INTO ptoPositions values('Fleet Sales Consultant');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
    values('RY1 Sales','Fleet Sales Consultant', 4, 'no_policy_html');    
  INSERT INTO ptoPositions values('Sales Trainer');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
    values('RY1 Sales','Sales Trainer', 3, 'no_policy_html');     
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
    values('HR','Manager', 4, 'no_policy_html');     
-- insertions: ptoAuthorization    
  INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
    values('RY1 Sales','Used Car Sales Manager','RY1 Sales','Used Car Team Leader',4,5);   
  UPDATE ptoAuthorization
    SET authByPosition = 'Used Car Sales Manager' 
  WHERE authByPosition = 'Used Car Buyer';    
  UPDATE ptoAuthorization
  SET authForPosition = 'Manager'
--  select * FROM ptoauthorization 
  WHERE authfordepartment = 'hr'
    AND authForPosition = 'intern';  
  INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
    values('RY1 Sales','General Sales Manager','RY1 Sales','Used Car Sales Manager',3,4); 
  INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
    values('RY1 Sales','General Sales Manager','RY1 Sales','Assistant Finance Director',3,4);      
  INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
    values('RY1 Sales','General Sales Manager','RY1 Sales','Fleet Sales Consultant',3,4); 
  INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
    values('RY1 Sales','New Car Sales Manager','RY1 Sales','New Car Team Leader',4,5);
  INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
    values('RY1','General Manager','RY1 Sales','Sales Trainer',2, 3);      
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;    

BEGIN TRANSACTION;
TRY     
-- removals
  UPDATE ptoAuthorization
  SET authByPosition = 'General Manager'
  WHERE authByDepartment = 'RY1'
    AND authByPosition = 'Fixed Director';   
-- aha, DELETE position or deptPosition OR both 
-- AND what about ri with ptoPosFul & auth AND closure
-- WHERE i am at IS i DO NOT care about history, this org chart structure
-- IS for current reality only      
-- redo ri for ALL these org chart structure tables

-- DO NOT DELETE FROM positions IF the position EXISTS IN another
-- department-position context, eg team leader
-- so, have chosen to remove ptoDepartmentPositions rows only
  DELETE FROM ptoDepartmentPositions WHERE department = 'RY1' AND position = 'Fixed Director'; 
  DELETE FROM ptoDepartmentPositions WHERE department = 'RY1 Sales' AND position = 'Team Leader';
  DELETE FROM ptoDepartmentPositions WHERE department = 'RY1 Sales' AND position = 'Sales Support';
  DELETE FROM ptoDepartmentPositions WHERE department = 'RY1 Sales' AND position = 'Sales Consultant';
  DELETE FROM ptoDepartmentPositions WHERE department = 'RY1 Sales' AND position = 'Used Car Buyer';   
  DELETE FROM ptoDepartmentPositions WHERE department = 'RY2' AND position = 'Store Manager Trainee';         
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;       
     
BEGIN TRANSACTION;
TRY
-- moves 
  UPDATE ptoDepartmentPositions
  SET level = 4
  WHERE department = 'RY1 Sales'
    AND position = 'Finance Manager';   
  UPDATE ptoAuthorization
  SET authByPosition = 'General Sales Manager',
      authByLevel = 3,
      authForLevel = 4      
--  SELECT * FROM ptoAuthorization
  WHERE authForDepartment = 'RY1 Sales'
    AND authForPosition = 'Finance Manager';
    
  UPDATE ptoDepartmentPositions
  SET level = 4
  WHERE department = 'RY1 Sales'
    AND position = 'New Car Inventory Manager';   
  UPDATE ptoAuthorization
  SET authByPosition = 'General Sales Manager',
      authByLevel = 3,
      authForLevel = 4      
--  SELECT * FROM ptoAuthorization
  WHERE authForDepartment = 'RY1 Sales'
    AND authForPosition = 'New Car Inventory Manager';  
    
  UPDATE ptoDepartmentPositions
  SET level = 4
  WHERE department = 'RY1 Sales'
    AND position = 'Used Car Inventory Manager';   
  UPDATE ptoAuthorization
  SET authByDepartment = 'RY1 Sales',
      authByPosition = 'General Sales Manager',
      authByLevel = 3,
      authForLevel = 4      
--  SELECT * FROM ptoAuthorization
  WHERE authForDepartment = 'RY1 Sales'
    AND authForPosition = 'Used Car Inventory Manager';       
    
IF (
SELECT COUNT(*) FROM (    
-- the lack of RI for ensuring integrity of level BETWEEN ptoDepartmentPositions
-- AND ptoAuthorization, this query exposes any anomalies  
  SELECT *
  FROM (
    SELECT a.authByDepartment, a.authByPosition, a.authByLEvel, b.level,
      a.authForDepartment, a.authForPosition, a.authForLevel, c.level
    FROM ptoAuthorization a
    LEFT JOIN ptoDepartmentPositions b on a.authByDepartment = b.Department
      AND a.authByPosition = b.position
    LEFT JOIN ptoDepartmentPositions c on a.authForDepartment = c.Department
      AND a.authForPosition = c.position) x   
  WHERE (
    authByLevel <> level
    OR authForLevel <> level_1)) y) <> 0 THEN 
RAISE levelMismatch (100, 'oops');


END IF;    
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;   

-- repopulate ptoClosure
DECLARE @i integer;
DECLARE @cur CURSOR AS
  SELECT distinct uc.ancestorDepartment, uc.ancestorPosition,
    u.authForDepartment, u.authForPosition
  FROM ptoClosure uc, ptoAuthorization u
  WHERE uc.descendantDepartment = u.authByDepartment 
    AND uc.descendantPosition = u.authByPosition   
  AND uc.depth = @i - 1;
BEGIN TRANSACTION;  
TRY 
@i = 1;    
DELETE FROM ptoClosure;
-- self referencing row (depth = 0) for each position that IS NOT a leaf
INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
  descendantPosition, depth)
SELECT DISTINCT authByDepartment, authByPosition, authByDepartment, authByPosition, 0 
FROM ptoAuthorization; 
-- root   
UPDATE ptoClosure
SET topmost = true
WHERE descendantPosition = 'board of directors';
-- AND one self referencing (depth = 0) row for each leaf node
INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
  descendantPosition, depth, lowest)
SELECT authForDepartment, authForPosition, authForDepartment, authForPosition, 0, true 
FROM ptoAuthorization a
WHERE NOT EXISTS (
  SELECT 1
  FROM  ptoAuthorization
  WHERE authByDepartment = a.authForDepartment
    AND authByPosition = a.authForPosition);
    
WHILE @i < (SELECT MAX(authForLevel) + 1 FROM ptoAuthorization) DO 
OPEN @cur;
TRY
  WHILE FETCH @cur DO
    INSERT INTO ptoClosure values(@cur.ancestorDepartment, @cur.ancestorPosition,
      @cur.authForDepartment, @cur.authForPosition, @i, false, false);
  END WHILE;
FINALLY
  CLOSE @cur;
END TRY;  
@i = @i + 1;
END WHILE;
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  

--/> structure ------------------------------------------------------structure-/>

--< personnel ----------------------------------------------------- personnel -<

-- this query loolks pretty good, what it does NOT show me IS, for those with
--   NULL vision info, IS it just that that person does NOT exist IN ptoPosFul
--   OR does that person NOT exist IN ptoEmp/tpEmp 
-- ALL full time employees WHERE compli position <> vision position
/*

DO the vision page pto admin UPDATE before generating this TABLE
 ** nick shireck - transfer
 ** kalissa kruse - pto anniv date?
 ** roberson - ry2 custodian, new position?
*/
-- DROP TABLE #wtf;
SELECT a.storecode, a.employeenumber, left(a.firstname, 20) AS firstname, 
  left(a.lastname, 20) as lastname, a.fullparttime, 
  a.distcode, a.distribution, a.pydeptcode, left(a.pydept, 15) as pydept, 
  a.payrollclass, 
  left(e.yrtext,25) AS arkona_position,   b.department AS vision_dept, 
  b.position AS vision_position, c.title AS compli_position
-- SELECT COUNT(*)  
INTO #wtf
FROM dds.edwEmployeeDim a
LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
LEFT JOIN pto_compli_users c on a.employeenumber = c.userid
LEFT JOIN dds.stgArkonaPYPRHEAD d on a.storecode = d.yrco# 
  AND a.employeenumber = d.yrempn
LEFT JOIN dds.stgArkonaPYPRJOBD e on d.yrco# = e.yrco# 
  AND d.yrjobd = e.yrjobd  
WHERE a.currentrow = true
  AND a.active = 'active'
  AND a.fullparttime = 'full'
  AND 
    CASE
      -- vision dept + vision position = compli title
      WHEN trim(coalesce(b.department,'')) + ' ' + trim(coalesce(b.position,'')) = trim(c.title) THEN 1 = 0
      ELSE coalesce(b.position,'') <> c.title
    END 
--ORDER BY a.storecode, a.pydeptcode, a.distcode, a.lastname
ORDER BY a.storecode, a.lastname, firstname   

select a.employeenumber, a.firstname, a.lastname, a.arkona_position, a.vision_dept,
  a.vision_position, a.compli_position, b.username AS ptoUser, 
  CASE
    WHEN (
      SELECT COUNT(*)
      FROM employeeAppAuthorization
      WHERE username = c.username
        AND appcode = 'pto') > 0 THEN 'X'
    ELSE ''
  END AS hasPto, c.username AS tpUser, c.password
FROM #wtf a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
LEFT JOIN tpEmployees c on a.employeenumber = c.employeenumber
WHERE a.storecode = 'ry1' 
  AND a.pydeptcode IN ('01','02')
--AND pydeptcode NOT IN ('04','4a','29','11','03','18','14',
--  '27','20','05','06','25','24','84','22','94','26') 
  AND a.distcode NOT IN ('dri')
ORDER BY a.lastname, a.firstname

-- AND here are the personnel changes
DECLARE @user string;
DECLARE @password string;
BEGIN TRANSACTION;
TRY 
INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales','New Car Sales Consultant','12545');
UPDATE ptoPositionFulfillment
SET position = 'Used Car Sales Consultant'
WHERE employeenumber = '17190';

@user = 'taubol@rydellcars.com';
@password = 'opc8Lu7e';
INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
  membergroup, storecode, fullname)
SELECT @user, firstname, lastname, employeenumber, @password, 
  'Fuck You', 'RY1', TRIM(firstName) + ' ' + TRIM(lastName)
-- SELECT *
FROM dds.edwEmployeeDim 
WHERE lastname = 'aubol' AND currentrow = true AND active = 'active';
INSERT INTO employeeAppAuthorization (username,appname,appcode,approle,
  functionality,appseq)
SELECT @user, a.appName, a.appCode, a.appRole, a.functionality, a.appseq
FROM applicationMetaData a
WHERE appName IN ('customerFeedback','digitalmetrics');
INSERT INTO employeeAppAuthorization (username,appname,appcode,approle,
  functionality,appseq)
SELECT @user, a.appName, a.appCode, a.appRole, a.functionality, a.appseq
FROM applicationMetaData a
WHERE appName = 'pto'
  AND approle = 'ptoemployee';
INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales','New Car Team Leader','17534');
-- al berry, no longer a pto manager
UPDATE ptoPositionFulfillment
SET position = 'New Car Team Leader'
WHERE employeenumber = '110052';
INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales','New Car Sales Consultant','110100');
UPDATE ptoPositionFulfillment
SET position = 'Used Car Team Leader'
WHERE employeenumber = '113550';
@user = 'aberry@rydellcars.com';
DELETE FROM employeeAppAuthorization
WHERE username = @user
  AND appname = 'pto';
INSERT INTO employeeAppAuthorization (username,appname,appcode,approle,
  functionality,appseq)
SELECT @user, a.appName, a.appCode, a.appRole, a.functionality, a.appseq
FROM applicationMetaData a
WHERE appName = 'pto'
  AND approle = 'ptoemployee';
-- cahalan
UPDATE ptoPositionFulfillment
SET employeenumber = '123100'
WHERE position = 'General Manager'
  AND department = 'RY1';

INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales','Used Car Sales Consultant','123600');
INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales','New Car Sales Consultant','133017');

-- rj no longer a pto admin
INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales','Used Car Team Leader','140500');
@user = 'rerickson@rydellcars.com';
DELETE FROM employeeAppAuthorization
WHERE username = @user
  AND appname = 'pto';
INSERT INTO employeeAppAuthorization (username,appname,appcode,approle,
  functionality,appseq)
SELECT @user, a.appName, a.appCode, a.appRole, a.functionality, a.appseq
FROM applicationMetaData a
WHERE appName = 'pto'
  AND approle = 'ptoemployee';

UPDATE ptoPositionFulfillment
SET position = 'Fleet Sales Consultant'
WHERE employeenumber = '145840';


INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales','Assistant Finance Director','150040');
@user = 'cgarceau@rydellcars.com';
INSERT INTO employeeAppAuthorization (username,appname,appcode,approle,
  functionality,appseq)
SELECT @user, a.appName, a.appCode, a.appRole, a.functionality, a.appseq
FROM applicationMetaData a
WHERE appName = 'pto'
  AND approle = 'ptoemployee';
  
INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales','New Car Sales Consultant','167600');

@user = 'bknudson@rydellcars.com';
@password = 'Yav16Wgn';
INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
  membergroup, storecode, fullname)
SELECT @user, firstname, lastname, employeenumber, @password, 
  'Fuck You', 'RY1', TRIM(firstName) + ' ' + TRIM(lastName)
-- SELECT *
FROM dds.edwEmployeeDim 
WHERE lastname = 'knudson' AND firstname = 'benjamin' AND currentrow = true AND active = 'active';
INSERT INTO employeeAppAuthorization (username,appname,appcode,approle,
  functionality,appseq)
SELECT @user, a.appName, a.appCode, a.appRole, a.functionality, a.appseq
FROM applicationMetaData a
WHERE appName IN ('customerFeedback','digitalmetrics');
INSERT INTO employeeAppAuthorization (username,appname,appcode,approle,
  functionality,appseq)
SELECT @user, a.appName, a.appCode, a.appRole, a.functionality, a.appseq
FROM applicationMetaData a
WHERE appName = 'pto'
  AND approle = 'ptomanager';
INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales', 'Used Car Sales Manager','179380');
-- jim lynch, change position & no longer a pto admin
UPDATE ptoPositionFulfillment
SET position = 'Sales Trainer'
WHERE employeenumber = '189295';
@user = 'jlynch@rydellcars.com';
DELETE FROM employeeAppAuthorization
WHERE username = @user
  AND appname = 'pto';
INSERT INTO employeeAppAuthorization (username,appname,appcode,approle,
  functionality,appseq)
SELECT @user, a.appName, a.appCode, a.appRole, a.functionality, a.appseq
FROM applicationMetaData a
WHERE appName = 'pto'
  AND approle = 'ptoemployee';
-- change position
UPDATE ptoPositionFulfillment
SET position = 'Used Car Sales Consultant'
WHERE employeenumber = '190502';
-- change position
UPDATE ptoPositionFulfillment
SET position = 'Used Car Sales Consultant'
WHERE employeenumber = '190502';
-- a michaels: no vision position, no pto access
INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales','General Sales Manager','195460');
@user = 'amichael@rydellcars.com';
INSERT INTO employeeAppAuthorization (username,appname,appcode,approle,
  functionality,appseq)
SELECT @user, a.appName, a.appCode, a.appRole, a.functionality, a.appseq
FROM applicationMetaData a
WHERE appName = 'pto'
  AND approle = 'ptomanager';
-- nick shirek: tpEmployees: change username to nshirek@rydellcars.com, emp# to 1126300
-- ptoEmployees: change username to nshirek@rydellcars.com, emp# to 1126300
-- no vision position
UPDATE tpEmployees
SET employeenumber = '1126300',
    username = 'nshirek@rydellcars.com'
WHERE employeenumber = '2126300';
UPDATE ptoEmployees
SET employeenumber = '1126300',
    username = 'nshirek@rydellcars.com'
WHERE employeenumber = '2126300';     
INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales','New Car Sales Manager','1126300'); 

UPDATE ptoPositionFulfillment
SET position = 'Fleet Sales Consultant'
WHERE employeenumber = '1130690';

UPDATE ptoPositionFulfillment
SET position = 'Used Car Team Leader'
WHERE employeenumber = '1147810';

INSERT INTO ptoPositionFulfillment (department,position,employeenumber)
values('RY1 Sales','New Car Sales Consultant','1148210'); 

-- cahalan
UPDATE ptoPositionFulfillment
SET employeenumber = '123100'
WHERE department = 'RY1'
  AND position = 'General Manager';

-- tweten 
@user = 'mtweten@gfhonda.com';
@password = 'aCr50CtY';
INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
  membergroup, storecode, fullname)
SELECT @user, firstname, lastname, employeenumber, @password, 
  'Fuck You', 'RY2', TRIM(firstName) + ' ' + TRIM(lastName)
-- SELECT *
FROM dds.edwEmployeeDim 
WHERE lastname = 'tweten' AND currentrow = true AND active = 'active';
INSERT INTO employeeAppAuthorization (username,appname,appcode,approle,
  functionality,appseq)
SELECT @user, a.appName, a.appCode, a.appRole, a.functionality, a.appseq
FROM applicationMetaData a
WHERE appName IN ('customerFeedback','digitalmetrics');
INSERT INTO employeeAppAuthorization (username,appname,appcode,approle,
  functionality,appseq)
SELECT @user, a.appName, a.appCode, a.appRole, a.functionality, a.appseq
FROM applicationMetaData a
WHERE appName = 'pto'
  AND approle = 'ptoemployee';

COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  


   
-- who are nc AND who are uc
  
SELECT a.employeenumber, TRIM(b.firstname) + ' ' + TRIM(b.lastname), 
  b.pydept  
  FROM dds.dimSalesPerson a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
WHERE a.storecode = 'ry1'
  AND LEFT(a.employeenumber, 1) = '1'   
  AND a.active = true    
  AND b.currentrow = true
  AND b.active = 'active'
  AND b.distcode = 'SALE'
ORDER BY pydept, TRIM(b.firstname) + ' ' + TRIM(b.lastname) 

-- 5/7/15
-- ry1 sales dept only
SELECT a.employeenumber, a.firstname, a.lastname, a.distcode, a.distribution,
  a.pydept, a.payrollclass, left(e.yrtext,25) AS arkona_position,
  b.department AS vision_dept, b.position AS vision_position, c.title AS compli_position
FROM dds.edwEmployeeDim a
LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
LEFT JOIN pto_compli_users c on a.employeenumber = c.userid
LEFT JOIN dds.stgArkonaPYPRHEAD d on a.storecode = d.yrco# 
  AND a.employeenumber = d.yrempn
LEFT JOIN dds.stgArkonaPYPRJOBD e on d.yrco# = e.yrco# 
  AND d.yrjobd = e.yrjobd  
WHERE distcode IN ( 'sale' ,'team', 'new','salm','sthp','uscm','used','gsm','nci' ) 
  AND currentrow = true
  AND active = 'active'
  AND storecode = 'ry1'
ORDER BY a.distcode, a.lastname  

-- ALL full time employees
SELECT a.storecode, a.employeenumber, left(a.firstname, 20) AS firstname, 
  left(a.lastname, 20) as lastname, a.fullparttime, 
  a.distcode, a.distribution, a.pydeptcode, left(a.pydept, 15) as pydept, 
  a.payrollclass, 
  left(e.yrtext,25) AS arkona_position,   b.department AS vision_dept, 
  b.position AS vision_position, c.title AS compli_position
FROM dds.edwEmployeeDim a
LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
LEFT JOIN pto_compli_users c on a.employeenumber = c.userid
LEFT JOIN dds.stgArkonaPYPRHEAD d on a.storecode = d.yrco# 
  AND a.employeenumber = d.yrempn
LEFT JOIN dds.stgArkonaPYPRJOBD e on d.yrco# = e.yrco# 
  AND d.yrjobd = e.yrjobd  
WHERE a.currentrow = true
  AND a.active = 'active'
  AND a.fullparttime = 'full'
--ORDER BY a.storecode, a.pydeptcode, a.distcode, a.lastname
ORDER BY a.storecode, a.lastname, firstname    

-- 5/11/15  time to get this shit done
-- arkona - compli - vision positions

SELECT locationName, title, COUNT(*)
FROM pto_compli_users
WHERE status = '1'
GROUP by locationName, title

-- this IS going to be distracting, anomalies ALL over the fucking place
-- ry2 sc should be team 1 OR team 2, these 2 are not
SELECT * FROM pto_compli_users WHERE status = '1' and locationname = 'ry2' AND title = 'sales consultant'

SELECT a.storecode, left(e.yrtext,25) AS arkona_position, COUNT(*)
FROM dds.edwEmployeeDim a
LEFT JOIN dds.stgArkonaPYPRHEAD d on a.storecode = d.yrco# 
  AND a.employeenumber = d.yrempn
LEFT JOIN dds.stgArkonaPYPRJOBD e on d.yrco# = e.yrco# 
  AND d.yrjobd = e.yrjobd 
WHERE a.currentrow = true
  AND a.active = 'active'  
GROUP BY a.storecode, left(e.yrtext,25)
-- ADD IN dept, dist, payrollclass
SELECT a.storecode, a.distcode, a.distribution,
  a.pydept, a.payrollclass, left(e.yrtext,25) AS arkona_position, COUNT(*)
FROM dds.edwEmployeeDim a
LEFT JOIN dds.stgArkonaPYPRHEAD d on a.storecode = d.yrco# 
  AND a.employeenumber = d.yrempn
LEFT JOIN dds.stgArkonaPYPRJOBD e on d.yrco# = e.yrco# 
  AND d.yrjobd = e.yrjobd 
WHERE a.currentrow = true
  AND a.active = 'active'  
GROUP BY a.storecode, a.distcode, a.distribution,
  a.pydept, a.payrollclass, left(e.yrtext,25) 
  
SELECT a.department, a.position, COUNT(*)
FROM ptoDepartmentPositions a
LEFT JOIN ptoPositionFulfillment b on a.department = b.department
  AND a.position = b.position
GROUP BY a.department, a.position  

-- unfulfilled positions
SELECT *
FROM ptoDepartmentPositions a
LEFT JOIN ptoPositionFulfillment b on a.department = b.department
  AND a.position = b.position
WHERE b.department IS NULL   