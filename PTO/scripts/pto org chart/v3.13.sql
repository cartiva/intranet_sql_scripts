/*
lots of moving parts, getting confused about the ORDER IN which the changes
need to be made, authorization vs departmentpositions
these positions ALL go away (ry2 sales) sales manager, desk manager, 
      sales consultant(PT), team 1 sales consultant, team 2 sales consultant
the problem IS sales manager     
*/ 
BEGIN TRANSACTION;
TRY 
-- new RY2 Sales position 
  INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
  values('RY2 Sales','General Sales Manager',3,'no_policy_html');
  
-- auth BY GM
  INSERT INTO ptoAuthorization (authByDepartment,authByPosition, 
    authForDepartment,authForPosition,authByLevel,AuthForLevel)
  values('RY2','General Manager','RY2 Sales','General Sales Manager',2, 3);   
  
-- change tom aubul to GSM
  UPDATE ptoPositionFulfillment  
  SET position = 'General Sales Manager'    
  WHERE employeenumber = (
    SELECT employeenumber
    FROM tpEmployees
    WHERE lastname = 'aubol'); 

-- olderbak to team leader
  UPDATE ptoPositionFulfillment
  SET position = 'Team Leader'
  WHERE employeenumber = (
    SELECT employeenumber
    FROM tpemployees
    WHERE lastname = 'olderbak');
    
-- team leader, sales manager no longer authorized anybody
  UPDATE ptoAuthorization
  SET authByDepartment = 'RY2 Sales',
      authByPosition = 'General Sales Manager'
--  select * FROM ptoAuthorization
  WHERE authfordepartment = 'ry2 sales'
    AND (
      authforposition = 'team leader'
      OR
      authforposition = 'sales consultant');      
    
-- remove ry2 sales:sales manager        
-- remove ry2 sales:desk manager  
-- this should, via RI, DELETE rows FROM ptoPositionFulfillment, ptoAuthorization 
  DELETE 
--  SELECT *
  FROM ptoDepartmentPositions
  WHERE department = 'RY2 Sales'
    AND position IN ('Desk Manager','Sales Consultant (PT)',
      'Team 1 Sales Consultant','Team 2 Sales Consultant', 'Sales Manager');
  
-- change levels
  UPDATE ptoDepartmentPositions
  SET level = 4
  WHERE department = 'RY2 Sales'
    AND (
      position = 'team leader'
      OR
      position = 'sales consultant');
      
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;            
       


SELECT a.employeenumber, b.fullname, c.department, c.position,
  g.fullname, e.department, e.position
FROM ptoEmployees a
INNER JOIN tpemployees b on a.employeenumber = b.employeenumber
INNER JOIN ptoPositionFulfillment c on a.employeenumber = c.employeenumber
INNER JOIN ptoAuthorization d on c.department = d.authfordepartment
  AND c.position = d.authforposition
INNER JOIN ptoPositionFulFillment e on d.authbydepartment = e.department
  AND d.authbyposition = e.position
INNER JOIN ptoemployees f on e.employeenumber = f.employeenumber
INNER JOIN tpemployees g on f.employeenumber = g.employeenumber   
WHERE c.department = 'ry2 sales'
ORDER BY c.position

SELECT *
FROM ptoauthorization
  WHERE authfordepartment = 'ry2 sales'
    AND authforposition = 'sales manager'
    
SELECT *
FROM ptoauthorization
  WHERE authbydepartment = 'ry2 sales'
    AND authbyposition in ('sales manager', 'team leader')  
    
SELECT *
FROM ptoauthorization
  WHERE authbydepartment = 'ry2 sales'
    AND authbyposition = 'team leader'  
    
SELECT *
FROM ptoDepartmentPositions a
WHERE department = 'RY2 Sales'
  AND NOT EXISTS (  
    SELECT 1
    FROM ptoPositionFulfillment
    WHERE department = a.department
      AND position = a.position)       
      
select *
FROM tpemployees
WHERE lastname = 'aubol'      