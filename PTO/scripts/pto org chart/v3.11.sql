SELECT locationid, title FROM (
SELECT locationid, supervisorname, title
FROM pto_compli_users
WHERE status = '1'
GROUP BY locationid, supervisorname, title --ORDER BY title
) x GROUP BY locationid, title HAVING COUNT(*) > 1


SELECT *
from pto_Compli_users
WHERE status = '1'
AND locationid = 'ry1'
AND title IN ('F&I Admin','Metal Technician (A)')

SELECT * FROM ptodepartmentpositions WHERE position = 'F&I Admin'

SELECT *
from pto_Compli_users a
LEFT JOIN dds.edwEmployeeDim b on a.userid = b.employeenumber
  AND b.currentrow = true
WHERE status = '1'
  AND termdate < curdate()
  
SELECT *
FROM tpemployees  
WHERE lastname = 'swanson'
kristen swanson: 1134524 - kswanson@rydellcars.com
SELECT * FROM ptopositionfulfillment WHERE employeenumber = '1134524'
  
need new body shop position,  lead f&i admin position
f&i admin auth by  Michelle who IS auth BY Jeri
michelle cochram: 126200 - mcochran@rydellcars.com
SELECT *
FROM tpemployees  
WHERE lastname = 'cochran'

SELECT *
FROM employeeAppAuthorization
WHERE username = 'msteinke@rydellcars.com'
AND appname = 'pto'

SELECT *
FROM employeeAppAuthorization
WHERE username = 'mcochran@rydellcars.com'
AND appname = 'pto'

-- oh shit, a bunch of terms, oh well, let's DO it
SELECT a.employeenumber
FROM ptoEmployees a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'Term'
  
  
BEGIN TRANSACTION;
TRY
-- 1. DELETE termed folks
  DELETE 
--  SELECT *
  FROM employeeAppAuthorization
  WHERE username IN (
    SELECT username
  	FROM tpemployees
  	WHERE employeenumber IN (
      SELECT a.employeenumber
      FROM ptoEmployees a
      INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
        AND b.currentrow = true
        AND b.active = 'Term'));    
  DELETE
  -- SELECT * 
  FROM tpemployees
  WHERE employeenumber IN (
    SELECT a.employeenumber
    FROM ptoEmployees a
    INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
      AND b.currentrow = true
      AND b.active = 'Term');  
  DELETE
  FROM ptoEmployees 
  WHERE employeenumber IN (
    SELECT a.employeenumber
    FROM ptoEmployees a
    INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
      AND b.currentrow = true
      AND b.active = 'Term');  
-- 2. new body shop position
  INSERT INTO ptoPositions (position) values('Prep Technician');
  INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
  values('Body Shop','Prep Technician',5,'collision_center_policy_html');
-- 3. auth BY Production Manager
  INSERT INTO ptoAuthorization (authByDepartment,authByPosition, 
    authForDepartment,authForPosition,authByLevel,AuthForLevel)
  values('Body Shop','Production Manager','Body Shop','Prep Technician',4,5); 
-- 4. assign kristen swanson to the new position
  UPDATE ptoPositionFulfillment
  SET department = 'Body Shop',
      position = 'Prep Technician'
  WHERE employeenumber = '1134524';
-- 5. new office position
  INSERT INTO ptoPositions (position) values('Lead F&I Admin');
  INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
  values('Office','Lead F&I Admin',4,'fi_admin_policy_html');
  UPDATE ptoAuthorization
  SET authByPosition = 'Lead F&I Admin'
  WHERE authForPosition = 'F&I Admin';
-- 6. auth BY Controller
  INSERT INTO ptoAuthorization (authByDepartment,authByPosition, 
    authForDepartment,authForPosition,authByLevel,AuthForLevel)
  values('Office','Controller','Office','Lead F&I Admin',3,4);   
-- 7. assign michelle to new position
  UPDATE ptoPositionFulfillment
  SET position = 'Lead F&I Admin'
  WHERE employeenumber = '126200';
-- 8. give michelle access to pto admin
  UPDATE employeeAppAuthorization
  SET approle = 'ptomanager'
  WHERE username = 'mcochran@rydellcars.com'
    AND appname = 'pto';
  INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
  values('mcochran@rydellcars.com','pto',800,'pto','ptomanager','pto admin');    
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;      