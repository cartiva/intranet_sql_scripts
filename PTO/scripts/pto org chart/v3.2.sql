SELECT b.firstname, b.lastname, c.department, c.position, b.active
FROM ptoEmployees a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
LEFT JOIN ptoPositionFulfillment c on a.employeenumber = c.employeenumber
WHERE position = 'porter'

SELECT *
FROM ptoDepartmentPositions
ORDER BY department, position
-- currently vacant positions
SELECT TRIM(a.department) + ' : ' + TRIM(a.position)
FROM ptoDepartmentPositions a
LEFT JOIN ptoPositionFulfillment b on a.department = b.department AND a.position = b.position
WHERE b.department IS NULL 

-- 1.
UPDATE ptoPositions
SET position = 'Help Desk'
-- SELECT * FROM ptoPositions
WHERE position = 'IT Specialist';

-- 2. new employee IN new position Nicholas Larsen AS ry1 main shop porter
-- 2a new position 
INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
values('RY1 Main Shop','Porter',5,'no_policy_html');

INSERT INTO ptoAuthorization (authByDepartment,authByPosition,
  authForDepartment,authForPosition,authByLevel,authForLevel)
values('RY1 Main Shop','Dispatcher','RY1 Main Shop','Porter',4,5);

-- 2b new employee: modify PROCEDURE pto_insert_new_employee to ADD the employee
                 
-- 3. rename BDC to Call Center
UPDATE ptoDepartments
SET department = 'Call Center'
-- SELECT * FROM ptoDepartments
WHERE department = 'bdc';

-- 4. rename position Operator to Service Operator
UPDATE ptoPositions
SET position = 'Service Operator'
-- SELECT * FROM ptoPositions 
WHERE position = 'operator';

-- 5. new position General Operator auth BY Call Center:Team Leader
INSERT INTO ptoPositions (position) values('General Operator');
INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
values('Call Center','General Operator',6,'no_policy_html');
INSERT INTO ptoAuthorization (authByDepartment,authByPosition,
  authForDepartment,authForPosition,authByLevel,authForLevel)
values('Call Center','Team Leader','Call Center','General Operator',5,6);  

-- 6. shayla salvione, move FROM guest experience:reception to call center:general operator
UPDATE ptoPositionFulfillment
SET department = 'Call Center',
    position = 'General Operator'
-- SELECT * FROM ptoPositionFulfillment 
WHERE employeenumber = (
  SELECT employeenumber 
  FROM dds.edwEmployeeDim 
  WHERE firstname = 'shayla' 
    AND currentrow = true);
    
select a.department, a.position, c.name
FROM ptoDepartmentPositions a
LEFT JOIN ptoPositionFulfillment b on a.department = b.department AND a.position = b.position
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
WHERE a.department LIKE 'guest%'

SELECT * FROM dds.edwEmployeeDim WHERE name LIKE '%maris%'

SELECT * 
FROM ptoDepartmentPositions
WHERE position = 'operator'
                 