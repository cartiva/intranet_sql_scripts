SELECT *
FROM ptoauthorization
ORDER BY authbydepartment,authbyposition, authfordepartment, authforposition

-- leaf nodes: these auth no one
SELECT authForDepartment, authForPosition 
FROM ptoAuthorization a
WHERE NOT EXISTS (
  SELECT 1
  FROM  ptoAuthorization
  WHERE authByDepartment = a.authForDepartment
    AND authByPosition = a.authForPosition)
-- parent nodes: these ALL auth someone    
SELECT DISTINCT authByDepartment, authByPosition
FROM ptoAuthorization    
/*
EXECUTE PROCEDURE sp_DropReferentialIntegrity('ptoDeptPos-ptoClosure1');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('ptoDeptPos-ptoClosure2');
DROP TABLE ptoClosure;
*/
CREATE TABLE ptoClosure (
  ancestorDepartment cichar(30) constraint NOT NULL,
  ancestorPosition cichar(30) constraint NOT NULL,
  descendantDepartment cichar(30) constraint NOT NULL,
  descendantPosition cichar(30) constraint NOT NULL,
  depth integer constraint NOT NULL,
  topmost logical constraint NOT NULL default 'False',
  lowest logical constraint NOT NULL default 'False',
  constraint pk primary key (ancestorDepartment,ancestorPosition,descendantDepartment,descendantPosition)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoClosure', 'ptoClosure', 'FK1', 'ancestorDepartment;ancestorPosition', '', 2, 1024, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoClosure', 'ptoClosure.adi', 'FK2', 'descendantDepartment;descendantPosition', '', 2, 1024, '' );
-- UPDATE:cascade, DELETE:restrict
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoDeptPos-ptoClosure1', 'ptoDepartmentPositions', 'ptoClosure', 'FK1', 1, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoDeptPos-ptoClosure2', 'ptoDepartmentPositions', 'ptoClosure', 'FK2', 1, 2, NULL, '', '');
-- these are ALL parent nodes
-- one row for each parent with depth = 0 (self referencing)
DELETE FROM ptoClosure;
INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
  descendantPosition, depth)
SELECT DISTINCT authByDepartment, authByPosition, authByDepartment, authByPosition, 0 
FROM ptoAuthorization; 
-- root   
UPDATE ptoClosure
SET topmost = true
WHERE descendantPosition = 'board of directors';
-- AND one self referencing (depth = 0) row for each leaf node
INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
  descendantPosition, depth, lowest)
SELECT authForDepartment, authForPosition, authForDepartment, authForPosition, 0, true 
FROM ptoAuthorization a
WHERE NOT EXISTS (
  SELECT 1
  FROM  ptoAuthorization
  WHERE authByDepartment = a.authForDepartment
    AND authByPosition = a.authForPosition);
-- AND now, ALL the iterim paths

SELECT * FROM ptoClosure
-- this works but IS too literal (hard coded)
SELECT p.ancestorDepartment, p.ancestorPosition, c.descendantDepartment, c.descendantPosition,
  p.depth + c.depth + 1 AS depth
FROM ptoClosure p, ptoClosure c
WHERE p.descendantDepartment = 'Market' AND p.descendantPosition = 'Board of Directors'
  AND c.ancestorDepartment = 'Market' AND c.ancestorPosition = 'Executive Manager'

SELECT *
FROM ptoAuthorization
WHERE authByDepartment = 'Market' AND authByPosition = 'Board of Directors'

-- http://kylecordes.com/2008/transitive-closure
DECLARE @i integer;
@i = 1;
WHILE @i < 6 DO 
SELECT *
FROM ptoClosure uc, ptoAuthorization u
WHERE uc.descendantDepartment = u.authByDepartment AND uc.descendantPosition = u.authByPosition
  AND uc.depth = @1 - 1;
@i = @i + 1;
END WHILE;

authBy IS parent to the child authFor
authFor IS the child to the parent authBy
for each row
so, IS this already a fucking adjacency list ?!?!?!?
pairs of nodes connected BY the edges
Transitive: If A is an ancestor of B and B is an ancestor of C, then A is an ancestor of C
 
Irreflexive: If A is an ancestor of B, then B is never an ancestor of A
 
0-1:0-N A can have zero, one or many children. A can have zero or one parents.


-- ok, this makes sense, IN the actual authorization TABLE, no one authories
-- themselves
select * 
FROM ptoAuthorization
WHERE authByDepartment = authForDepartment
AND authByPosition = authForPosition
-- ok, this makes sense, authFor IS unique
SELECT authForDepartment, authForPosition
FROM ptoAuthorization
GROUP BY authForDepartment, authForPosition
HAVING COUNT(*) > 1

-- 8/29 
after too much struggling, going to TRY something different
1st challenge IS to just fucking populate the ptoClosure TABLE, i think maintenance
will be easier, but the initial load IS fucking me up, so
ptoDepartmentPositions: ADD attribute level

--                        market:BOD                 level 1
--                            |
--           market:ex mgr ------- ry2Sales:gen mgr  level 2
--                |                      |
--              it:mgr           ry2Sales:sales mgr  level 3
etc, etc
THEN IN ptoAuthorization ADD authByLevel, AND authForLevel
THEN, the depth IN ptoClosure IS authForLevel - authByLevel   
anyway, seems LIKE it would work    

ALTER TABLE ptoDepartmentPositions
ADD COLUMN level integer;  

select *
FROM ptoDepartmentPositions

ALTER TABLE ptoAuthorization
ADD COLUMN authByLevel integer
ADD column authForLevel integer;

UPDATE ptoAuthorization
SET authByLevel = (
  SELECT level
  FROM ptoDepartmentPositions
  WHERE department = ptoAuthorization.authByDepartment
    AND position = ptoAuthorization.authByPosition);
    
UPDATE ptoAuthorization
SET authForLevel = (
  SELECT level
  FROM ptoDepartmentPositions
  WHERE department = ptoAuthorization.authForDepartment
    AND position = ptoAuthorization.authForPosition);    

-- depth for every one of these rows should be 1    
SELECT *
FROM ptoAuthorization    
WHERE authForLevel - authByLevel <> 1;

-- yikes, now it gets weird again, how to populate closure
closure includes one row for every edge
already have the self referencing rows (depth = 0) AND root
every node IS an ancestor of level 1

-- this gives me the closure rows for root (markte:BOD)
INSERT INTO ptoClosure
SELECT distinct a.authByDepartment, a.authByPosition, b.authForDepartment, b.authForPosition,
  b.authForLevel - a.authByLevel AS depth, false, false
FROM ptoAuthorization a, ptoAuthorization b
WHERE a.authByLevel = 1;

that was an easy one, how the fuck DO i DO the rest
there IS nothing IN ptoAuthorization to connect market:ex mgr to it:it specialist
so am i back to HAVING to fucking hard code every fucking thing for the
initial load



-- 8/31/
thinking one of the mind fucks i need to stop indulging IS separating dept/pos
IN attempt to generate anything, the composite key IS comprised of both fields, 
i need to remember that

-- looking good -- missing leaf designation
-- no, lowest only shows up on those rows with depth of 0 (self referencing)
-- NOT sure, IN that CASE, what sense lowest, topmost matters IN ptoClosure
-- a carry over FROM the notion of implementing AS a bridge (dimensional)
-- yahoo, this works
-- total number of rows = SUM(ptoAuthorization.authForLevel) - 1

-- fuck updating AND editing, just regenerate anytime ptoAuthorization changes,
-- only takes ~200 msec
DECLARE @i integer;
DECLARE @cur CURSOR AS
  SELECT distinct uc.ancestorDepartment, uc.ancestorPosition,
    u.authForDepartment, u.authForPosition
  FROM ptoClosure uc, ptoAuthorization u
  WHERE uc.descendantDepartment = u.authByDepartment 
    AND uc.descendantPosition = u.authByPosition   
  AND uc.depth = @i - 1;
@i = 1;    
DELETE FROM ptoClosure;
-- self referencing row (depth = 0) for each position that IS NOT a leaf
INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
  descendantPosition, depth)
SELECT DISTINCT authByDepartment, authByPosition, authByDepartment, authByPosition, 0 
FROM ptoAuthorization; 
-- root   
UPDATE ptoClosure
SET topmost = true
WHERE descendantPosition = 'board of directors';
-- AND one self referencing (depth = 0) row for each leaf node
INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
  descendantPosition, depth, lowest)
SELECT authForDepartment, authForPosition, authForDepartment, authForPosition, 0, true 
FROM ptoAuthorization a
WHERE NOT EXISTS (
  SELECT 1
  FROM  ptoAuthorization
  WHERE authByDepartment = a.authForDepartment
    AND authByPosition = a.authForPosition);
    
WHILE @i < (SELECT MAX(authForLevel) + 1 FROM ptoAuthorization) DO 
OPEN @cur;
TRY
  WHILE FETCH @cur DO
    INSERT INTO ptoClosure values(@cur.ancestorDepartment, @cur.ancestorPosition,
      @cur.authForDepartment, @cur.authForPosition, @i, false, false);
  END WHILE;
FINALLY
  CLOSE @cur;
END TRY;  
@i = @i + 1;
END WHILE;

-- andrew
SELECT descendantDepartment, descendantPosition, depth
FROM ptoClosure
WHERE ancestorDepartment = 'RY1 service'
  AND ancestorPosition = 'Director'
  AND depth <> 0
ORDER BY descendantDepartment, depth, descendantPosition
-- ben c
SELECT descendantDepartment, descendantPosition, depth
FROM ptoClosure
WHERE ancestorDepartment = 'Market Fixed'
  AND ancestorPosition = 'Director'
  AND depth <> 0
ORDER BY descendantDepartment, depth, descendantPosition

-- 9/2
validation of ptoClosure relative to ptoAuthorization
select *
FROM ptoClosure a
WHERE depth = 1

-- 9/19
--ALL people for whose pto andrew IS ultimately responsible
SELECT f.lastname, f.firstname
FROM ptoClosure a
INNER JOIN ptoPositionFulfillment b on a.ancestorDepartment = b.department
  AND a.ancestorPosition = b.position
INNER JOIN ptoEmployees c on b.employeenumber = c.employeenumber  
INNER JOIN dds.edwEmployeeDim d on c.employeeNumber = d.employeeNumber
  AND d.currentrow = true
INNER JOIN ptoPositionFulfillment e on a.descendantDepartment = e.department
  AND a.descendantPosition = e.position
INNER JOIN dds.edwEmployeeDim f on e.employeenumber = f.employeenumber
  AND f.currentrow = true    
WHERE d.lastname = 'neumann'