
 
-- ALL admins AND reportees   
-- FROM auth
SELECT left(trim(c.firstname) + ' ' + c.lastname, 25) AS administrator, c.employeenumber, 
  /**/left(trim(b.department) + ':' + b.position, 36) AS title, /**/
  left(trim(f.firstname) + ' ' + f.lastname, 25) AS employee, f.employeenumber, 
  /**/left(trim(d.authForDepartment) + ':' + d.authForPosition, 38) AS empTitle/**/
FROM (
  SELECT authByDepartment, authByPosition
  FROM ptoAuthorization
  WHERE authByLevel <> 1 
    AND authByDepartment NOT IN ('ry1 sales','ry2 sales')
  GROUP BY authByDepartment, authByPosition) a
LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
  AND c.active = 'active'  
LEFT JOIN ptoAuthorization d on a.authByDepartment = d.authByDepartment
  AND a.authByPosition = d.authByPosition 
LEFT JOIN ptoPositionFulfillment e on d.authForDepartment = e.department
  AND d.authForPosition = e.position
LEFT JOIN dds.edwEmployeeDim f on e.employeeNumber = f.employeenumber
  AND f.currentrow = true
  AND f.active = 'active'  
  AND f.fullparttime = 'full'
WHERE f.employeenumber IS NOT NULL    
ORDER BY title

-- ADD admins email address
SELECT left(trim(c.firstname) + ' ' + c.lastname, 25) AS administrator, /*c.employeenumber,*/
  cc.email AS [Admin email],
  /*left(trim(b.department) + ':' + b.position, 36) AS title, */
  left(trim(f.firstname) + ' ' + f.lastname, 25) AS employee, f.employeenumber, 
  /**/left(trim(d.authForDepartment) + ':' + d.authForPosition, 38) AS empTitle/**/
FROM (
  SELECT authByDepartment, authByPosition
  FROM ptoAuthorization
  WHERE authByLevel <> 1
    AND authByDepartment NOT IN ('ry1 sales','ry2 sales')
  GROUP BY authByDepartment, authByPosition) a
LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
  AND c.active = 'active' 
LEFT JOIN pto_compli_users cc on c.employeenumber = cc.userid  -- admin email
LEFT JOIN ptoAuthorization d on a.authByDepartment = d.authByDepartment
  AND a.authByPosition = d.authByPosition 
LEFT JOIN ptoPositionFulfillment e on d.authForDepartment = e.department
  AND d.authForPosition = e.position
LEFT JOIN dds.edwEmployeeDim f on e.employeeNumber = f.employeenumber
  AND f.currentrow = true
  AND f.active = 'active'  
  AND f.fullparttime = 'full'
WHERE f.employeenumber IS NOT NULL    
ORDER BY c.firstname, c.lastname, empTitle;

-- ADD employee's :compli username
-- this list needs to generate the employees vision login info (username & password)
-- 11/3, this IS what i sent to ben
SELECT left(trim(c.firstname) + ' ' + c.lastname, 25) AS administrator, /*c.employeenumber,*/
  cc.email AS [Admin email],
  /*left(trim(b.department) + ':' + b.position, 36) AS title, */
  left(trim(f.firstname) + ' ' + f.lastname, 25) AS employee, /*f.employeenumber, *'
  /*left(trim(d.authForDepartment) + ':' + d.authForPosition, 38) AS empTitle*/
--  g.email AS comUserName,
  h.username, h.password
FROM (
  SELECT authByDepartment, authByPosition
  FROM ptoAuthorization
  WHERE authByLevel <> 1
    AND authByDepartment NOT IN ('ry1 sales','ry2 sales')
  GROUP BY authByDepartment, authByPosition) a
LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
  AND c.active = 'active' 
LEFT JOIN pto_compli_users cc on c.employeenumber = cc.userid  -- admin email FROM compli
LEFT JOIN ptoAuthorization d on a.authByDepartment = d.authByDepartment
  AND a.authByPosition = d.authByPosition 
LEFT JOIN ptoPositionFulfillment e on d.authForDepartment = e.department
  AND d.authForPosition = e.position
LEFT JOIN dds.edwEmployeeDim f on e.employeeNumber = f.employeenumber
  AND f.currentrow = true
  AND f.active = 'active'  
  AND f.fullparttime = 'full'
  AND f.payrollclass = 'hourly'
LEFT JOIN pto_compli_users g on f.employeenumber = g.userid -- empl email FROM compli  
LEFT JOIN tpEmployees h on f.employeenumber = h.employeenumber -- empl vision
WHERE f.employeenumber IS NOT NULL    
--  AND g.email <> h.username collate ads_Default_ci
ORDER BY c.firstname, c.lastname, f.firstname, f.lastname
-----------------------

SELECT *
FROM dds.edwEmployeeDim a
WHERE currentrow = true
  AND active = 'active'
  AND payrollclass = 'hourly'
  AND fullparttime = 'full'
  AND NOT EXISTS (
    SELECT 1
    FROM pto_exclude
    WHERE employeenumber = a.employeenumber)
    

SELECT *
FROM(   
  SELECT authByDepartment, authByPosition
  FROM ptoAuthorization 
  WHERE authByLevel <> 1
  GROUP BY authByDepartment, authByPosition) a 
LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position    
WHERE b.department IS NULL   

-- termed employees IN compli
SELECT *
FROM pto_compli_users
WHERE status = '1'
  AND userid IN (
  SELECT employeenumber
  FROM dds.edwEmployeeDim
  WHERE termdate <= curdate()
    AND currentrow = true)
-- termed employees IN ptoEmp    
select *
FROM ptoEmployees    
WHERE employeenumber IN (
  SELECT employeenumber
  FROM dds.edwEmployeeDim
  WHERE termdate <= curdate()
    AND currentrow = true)
-- inactive employees IN ptoEmp   
select *
FROM ptoEmployees    
WHERE employeenumber IN (
  SELECT userid
  FROM pto_compli_users
  WHERE status = '2')