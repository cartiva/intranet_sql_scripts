SELECT *
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
  AND b.theyear = 2014
INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey  
  AND c.currentrow = true
  AND c.activecode = 'a'
WHERE a.vacationhours > 0 OR  a.ptohours > 0  


SELECT m.employeenumber, p.payrollclass, p.lastname, p.firstname, 
  coalesce(n.vacHours,0) AS vacHoursConsumed,
  coalesce(n.ptoHours,0) AS ptoHoursconsumed,
  coalesce(n.vacHours,0) + coalesce(n.ptoHours,0) AS totalPtoConsumed
FROM ptoEmployees m
LEFT JOIN dds.edwEmployeeDim p on m.employeenumber = p.employeenumber
  AND p.currentrow = true
  AND p.activecode = 'a'
LEFT JOIN (
  SELECT c.employeenumber, c.lastname, c.firstname, 
    SUM(a.vacationhours) AS vacHours, SUM(a.ptohours) AS ptoHours
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.day b on a.datekey = b.datekey
    AND b.theyear = 2014
  INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey  
    AND c.currentrow = true
    AND c.activecode = 'a'
  WHERE a.vacationhours > 0 OR  a.ptohours > 0
  GROUP BY c.employeenumber, c.lastname, c.firstname) n on m.employeenumber = n.employeenumber
WHERE p.lastname IS NOT NULL 
ORDER BY p.lastname


SELECT b.thedate, c.name, a.vacationhours, a.ptohours
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
  AND b.theyear = 2014
INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey  
  AND c.currentrow = true
  AND c.activecode = 'a'
WHERE (a.vacationhours > 0 OR  a.ptohours > 0)
AND c.employeenumber = '1149500'  
  
  
SELECT a.*, b.vacHoursConsumed, b.ptoHoursConsumed, b.totalPtoConsumed, 
  CASE WHEN b.vacHoursConsumed IS NULL THEN 'NO LONGER EMPLOYED' ELSE '' END 
FROM book3 a
LEFT JOIN (
  SELECT m.employeenumber, p.payrollclass, p.lastname, p.firstname, 
    coalesce(n.vacHours,0) AS vacHoursConsumed,
    coalesce(n.ptoHours,0) AS ptoHoursconsumed,
    coalesce(n.vacHours,0) + coalesce(n.ptoHours,0) AS totalPtoConsumed 
    
  FROM ptoEmployees m
  LEFT JOIN dds.edwEmployeeDim p on m.employeenumber = p.employeenumber
    AND p.currentrow = true
    AND p.activecode = 'a'
  LEFT JOIN (
    SELECT c.employeenumber, c.lastname, c.firstname, 
      SUM(a.vacationhours) AS vacHours, SUM(a.ptohours) AS ptoHours
    FROM dds.edwClockHoursFact a
    INNER JOIN dds.day b on a.datekey = b.datekey
      AND b.theyear = 2014
    INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey  
      AND c.currentrow = true
      AND c.activecode = 'a'
    WHERE a.vacationhours > 0 OR  a.ptohours > 0
    GROUP BY c.employeenumber, c.lastname, c.firstname) n on m.employeenumber = n.employeenumber
  WHERE p.lastname IS NOT NULL) b on a.employeenumber = b.employeenumber  
