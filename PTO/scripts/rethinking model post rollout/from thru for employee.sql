DECLARE @employeeNumber string;
DECLARE @date date;
DECLARE @fromDate date;
DECLARE @thruDate date;
@employeenumber = '164015';
@date = curdate() + 1000;
-- *a*
IF (
  SELECT year(thruDate)
  FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeeNumber
    AND @date BETWEEN fromDate AND thruDate) = 9999 THEN 
    @fromDate = (
      SELECT cast(createtimestamp(year(@date), 
        (SELECT month(ptoAnniversary) FROM ptoEmployees WHERE employeenumber = @employeenumber),
        (SELECT dayOfMonth(ptoAnniversary) FROM ptoEmployees WHERE employeenumber = @employeenumber),
        0,0,0,0) AS sql_date)
      FROM system.iota);
ELSE @fromDate = (
  SELECT fromDate
  FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeenumber
    AND @date BETWEEN fromDate AND thruDate);  
ENDIF;
IF (
  SELECT year(thruDate)
  FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeeNumber
    AND @date BETWEEN fromDate AND thruDate) = 9999 THEN 
    @thruDate = cast(timestampAdd(sql_tsi_day, -1, timestampadd(sql_tsi_year, 1, @fromDate)) AS sql_date);
ELSE @thruDate = (
  SELECT thruDate
  FROM pto_employee_pto_allocation
  WHERE employeenumber = @employeenumber
    AND @date BETWEEN fromDate AND thruDate);   
ENDIF;     
--@thruDate = cast(timestampAdd(sql_tsi_day, -1, timestampadd(sql_tsi_year, 1, @fromDate)) AS sql_date);
SELECT @date, @fromdate, @thrudate FROM system.iota;    