-- the folks whose current period expiration IS imminent, 
-- should current period extend another year?
SELECT a.*, b.name, b.distcode, fullparttime, d.ptoAnniversary 
FROM pto_employee_pto_allocation a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'active'
LEFT JOIN pto_exclude c on a.employeenumber = c.employeenumber
LEFT JOIN ptoEmployees d on a.employeenumber = d.employeenumber
WHERE curdate() BETWEEN a.fromdate AND a.thrudate 
  AND c.employeenumber IS NULL 
ORDER BY thrudate


-- this IS definitely anomalous data
-- first period IS less than 1 year
-- 11/8/14 conv with ben f, these are ok, CLOSE enuf for govt WORK, no need to
--   extend initial period, simply allow period roll over to happen, some of 
--   them have taken a few hours, chalk it up to the old policy of accruing
--   pto days during tenure 0 year
SELECT a.*
FROM pto_employee_pto_allocation a
WHERE a.employeenumber = '1130510'

SELECT a.*, b.name, a.thrudate - a.fromdate
FROM pto_employee_pto_allocation a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'active'
LEFT JOIN pto_exclude c on a.employeenumber = c.employeenumber
WHERE fromDate = (
  SELECT MIN(fromDate)
  FROM pto_employee_pto_allocation
  WHERE employeenumber = a.employeenumber)
AND a.thrudate - a.fromdate < 364  
AND c.employeenumber IS NULL 

-- shit, the lindsey sitch
-- current period should start 1/1/14 but isn't !?!?!?
-- these fromdates should be 1/1/14 for proper calculation of used pto based on current period
-- anniversary before 2014,  thinking the fromDate on ALL these should be 1/1/14
-- need to filter out part timers, excludes, non hourly
SELECT c.employeenumber, c.name, b.ptoAnniversary, a.*, c.fullparttime, 
  c.payrollclass, c.distribution
--INTO #badFromDate  
FROM pto_employee_pto_allocation a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
INNER JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND currentrow = true
LEFT JOIN pto_exclude d on a.employeenumber = d.employeenumber  
WHERE year(a.fromdate) = 2014
  AND b.ptoAnniversary < '01/01/2014'
  AND a.fromdate <> '01/01/2014'
  AND d.employeenumber IS NULL 
  AND c.fullparttime = 'full'
  AND c.payrollclass = 'hourly'
  AND a.fromdate = (
    SELECT MIN(fromDate)
    FROM pto_employee_pto_allocation
    WHERE employeenumber = a.employeenumber)
ORDER BY name    

SELECT * FROM #badFromDate


SELECT b.name, a.
FROM pto_employee_pto_allocation a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'active'
WHERE year(fromdate) = year(thrudate)

SELECT a.*, b.name, b.distcode, fullparttime, d.ptoAnniversary 
FROM pto_employee_pto_allocation a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'active'
LEFT JOIN pto_exclude c on a.employeenumber = c.employeenumber
LEFT JOIN ptoEmployees d on a.employeenumber = d.employeenumber
WHERE year(fromdate) = year(thrudate)
-----------------------------------

SELECT *
FROM ptoEmployees a
LEFT JOIN pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
WHERE a.employeenumber = '1135450' '164015' '1106399' '136170'


SELECT *
FROM ptoEmployees a
LEFT JOIN (
  SELECT employeenumber, MAX(fromDate) AS maxDate
  FROM pto_employee_pto_allocation
  GROUP BY employeenumber) b on a.employeenumber = b.employeenumber
WHERE month(a.ptoAnniversary) <> month(b.maxDate)
  AND dayofmonth(a.ptoAnniversary) <> dayofmonth(b.maxDate)
  
UPDATE ptoEmployees
SET ptoAnniversary = '02/18/2013'
WHERE employeenumber = '1135450'  

SELECT *
FROM pto_employee_pto_allocation
WHERE employeenumber = '1135450'  

--------------------------------------------------------------------------

SELECT *
FROM pto_employee_pto_allocation a
WHERE fromDate = (
  SELECT min(fromDate)
  FROM pto_employee_pto_allocation
  WHERE employeenumber = a.employeenumber)
  
  
  
SELECT a.*, b.name
FROM pto_employee_pto_allocation a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
LEFT JOIN pto_exclude c on a.employeenumber = c.employeenumber
WHERE curdate() BETWEEN a.fromdate AND a.thrudate 
  AND c.employeenumber IS NULL 
-- ORDER BY thrudate
AND year(thruDate) = 2014  


-- type 2 OR term changes FROM edwEmployeeDim 
SELECT *
FROM dds.edwEmployeeDim 
WHERE employeenumber IN (
  SELECT employeenumber
  FROM dds.edwEmployeeDim 
  WHERE employeekeyfromdate > '10/31/2014')
OR termdate BETWEEN '11/01/2014' AND curdate()  
ORDER BY employeenumber, employeekey
