/*
EXECUTE PROCEDURE sp_DropReferentialIntegrity('pto_allocation-pto_adjustments');
DROP TABLE pto_adjustments;
*/
CREATE TABLE pto_adjustments (
  employeenumber cichar(7) constraint NOT null,
  fromDate date constraint NOT null,
  hours double constraint NOT null,
  reason cichar(24) constraint NOT null,
  constraint PK primary key (employeeNumber,fromDate,hours,reason)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 'pto_adjustments', 'pto_adjustments.adi', 
  'FK', 'employeeNumber;fromDate', '', 2, 1024, '' );
-- cascade: UPDATE, DELETE: cascade
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('pto_allocation-pto_adjustments', 
  'pto_employee_pto_allocation', 'pto_adjustments', 'FK', 1, 1, NULL, '', '');
--<----------------------------------------------------------------------------------/> 
ALTER TABLE ptoEmployees
ADD COLUMN active logical constraint NOT NULL default 'true';  
UPDATE ptoEmployees
SET active = true;
--<----------------------------------------------------------------------------------/> 
/*
fix the bad fromDates for folks that started IN 2013
current period should be FROM 1/1/14, NOT 8/13/14, 3/11/14, etc,
the only fromDates IN 2014 should be for people hired IN 2014
*/
SELECT c.employeenumber, c.name, b.ptoAnniversary, a.*, c.fullparttime, 
  c.payrollclass, c.distribution
INTO #badFromDate  
FROM pto_employee_pto_allocation a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
INNER JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND currentrow = true
LEFT JOIN pto_exclude d on a.employeenumber = d.employeenumber  
WHERE year(a.fromdate) = 2014
  AND b.ptoAnniversary < '01/01/2014'
  AND a.fromdate <> '01/01/2014'
  AND d.employeenumber IS NULL 
  AND c.fullparttime = 'full'
  AND c.payrollclass = 'hourly'
  AND a.fromdate = (
    SELECT MIN(fromDate)
    FROM pto_employee_pto_allocation
    WHERE employeenumber = a.employeenumber);
    
UPDATE pto_employee_pto_allocation
SET fromDate = '01/01/2014'
WHERE employeenumber IN (SELECT employeenumber FROM #badFromDate)
  AND fromDate IN (SELECT fromDate FROM #badFromDate);
/*      
-- AND test it
-- this IS ok, returns no rows
SELECT c.employeenumber, c.name, b.ptoAnniversary, a.*, c.fullparttime, 
  c.payrollclass, c.distribution
FROM pto_employee_pto_allocation a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
INNER JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND currentrow = true
LEFT JOIN pto_exclude d on a.employeenumber = d.employeenumber  
WHERE year(a.fromdate) = 2014
  AND b.ptoAnniversary < '01/01/2014'
  AND a.fromdate <> '01/01/2014'
  AND d.employeenumber IS NULL 
  AND c.fullparttime = 'full'
  AND c.payrollclass = 'hourly'
  AND a.fromdate = (
    SELECT MIN(fromDate)
    FROM pto_employee_pto_allocation
    WHERE employeenumber = a.employeenumber);
-- but this shows that i LEFT out kyle bragunier, chatur tamang  
SELECT a.*, b.ptoAnniversary, c.name, c.payrollclass, c.fullparttime
FROM pto_employee_pto_allocation a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
INNER JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND currentrow = true
  AND c.payrollclass = 'hourly'
  AND c.fullparttime = 'full'
WHERE year(b.ptoAnniversary) = 2013 
  AND fromDate <> '01/01/2014'
  AND a.fromdate = (
    SELECT MIN(fromDate)
    FROM pto_employee_pto_allocation
    WHERE employeenumber = a.employeenumber)   
-- and this shows that kyle bragunier, chatur tamang & jeff bear are fucked up 
-- these are ALL folks that on rehiring were given a ptoAnniversary = orig hire date
-- IN addition, jeff bear current period shows 9/13/12 - 9/12/20
-- the modification to pto_employee_pto_allocation should fix that (a row for each period)
SELECT a.*, b.name
FROM pto_employee_pto_allocation a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE year(fromdate) < 2014    

SELECT *
FROM pto_employee_pto_allocation
WHERE employeenumber IN (
  SELECT a.employeenumber
  FROM pto_employee_pto_allocation a
  LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE year(fromdate) < 2014)  

there needs to be no past history, no current period should be FROM 
before 1/1/14  
AND fix them  
*/
DELETE 
FROM pto_employee_pto_allocation
WHERE hours = 0
  AND employeenumber IN ('118310','111210','1135450');
  
DELETE 
FROM pto_employee_pto_allocation
WHERE hours = 72
  AND employeenumber IN ('111210');  
  
update pto_employee_pto_allocation
SET fromDate = '01/01/2014'
WHERE employeeNumber = '118310'
  AND fromDate = '02/04/2014';
  
update pto_employee_pto_allocation
SET fromDate = '01/01/2014'
WHERE employeeNumber = '1135450'
  AND fromDate = '02/18/2014';  
  
update pto_employee_pto_allocation
SET fromDate = '01/01/2014',
    thruDate = '09/12/2015'
WHERE employeeNumber = '111210'
  AND fromDate = '09/13/2012';  
  
INSERT INTO pto_employee_pto_allocation(employeenumber,fromdate,thrudate,hours)
values('111210','09/13/2015','09/12/2020', 112);   

--<----------------------------------------------------------------------------------/> 
SELECT a.*, b.ptoAnniversary, c.name,
  timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thrudate) - 1 AS currentTenure
INTO #current  
FROM pto_employee_pto_allocation a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
WHERE curdate() BETWEEN a.fromDate AND a.thruDate;

--<----------------------------------------------------------------------------------/> 
SELECT *
INTO #omg
FROM (
  SELECT x.*,
    CASE x.n
      WHEN 20 THEN cast('12/31/9999' AS sql_date)
      ELSE CAST(timestampadd(sql_tsi_day, -1, 
        (timestampadd(sql_tsi_year, 1, periodFromDate))) AS sql_date) 
    END AS periodThruDate
  FROM (
    SELECT a.*, b.ptoAnniversary, 
      timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thrudate) - 1 AS tenure,
      c.*, d.*, 
      CASE n
        WHEN 1 THEN a.fromDate
        WHEN 20 THEN a.fromDate
        ELSE cast(timestampadd(sql_tsi_year, n, b.ptoAnniversary) AS sql_date)
      END AS periodFromDate  
    -- SELECT *      
    FROM pto_employee_pto_allocation a
    LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
    LEFT JOIN pto_categories c on timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thrudate) - 1
      BETWEEN c.fromYearsTenure AND c.thruYearsTenure
    LEFT JOIN dds.tally d on d.n BETWEEN c.fromYearsTenure AND c.thruYearsTenure 
      AND d.n BETWEEN 1 AND 20 
    LEFT JOIN #current z on a.employeenumber = z.employeenumber
      AND a.fromDate = z.fromDate  
    WHERE z.employeenumber IS NULL) x) y  
--      AND a.employeenumber = '111210') x) y
WHERE periodFromDate >= fromDate;
--<----------------------------------------------------------------------------------/> 
BEGIN TRANSACTION;
try
DELETE FROM pto_employee_pto_allocation;
INSERT INTO pto_employee_pto_allocation
SELECT employeenumber, fromdate, thrudate, hours
FROM #current  
union
select employeenumber, periodFromDate, periodThruDate, hours
FROM #omg;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
--<----------------------------------------------------------------------------------/> 
alter PROCEDURE pto_get_employee_pto
   ( 
      employeeNumber CICHAR ( 9 ),
      employeeNumber CICHAR ( 9 ) OUTPUT,
      name CICHAR ( 52 ) OUTPUT,
      store CICHAR ( 12 ) OUTPUT,
      originalHireDate DATE OUTPUT,
      latestHireDate DATE OUTPUT,
      department CICHAR ( 30 ) OUTPUT,
      position CICHAR ( 30 ) OUTPUT,
      ptoAdministrator CICHAR ( 52 ) OUTPUT,
      ptoAnniversary DATE OUTPUT,
      ptoHoursEarned DOUBLE ( 15 ) OUTPUT,
      ptoHoursUsed DOUBLE ( 15 ) OUTPUT,
      ptoHoursRemaining DOUBLE ( 15 ) OUTPUT,
      title CICHAR ( 63 ) OUTPUT,
      ptoFromDate DATE OUTPUT,
      ptoThruDate DATE OUTPUT,
      ptoPolicyHtml cichar(60) output,
      ptoHoursApproved integer output,
      ptoHoursRequested integer output,
      totalDaysInPeriod integer output,
      remainingDaysInPeriod integer output
   ) 
BEGIN 
/*
doug bohm '116185'
jay olson '1106399'
crystal moulds  
loren shereck '1126040'
rudy robles '1117600'
travis b '121362'

10/28 ADD 
  requests AND approved

11/8/14 
*a*
  modified return correct period thru date WHEN pto_alloc thrudate = 12/31/9999
  shit, from/thru date also used IN requested, approved, used, adjustments
    need to get a fix on from/thru immediately AND use them throughout
    define the period up top,  use it everywhere

ptoUsed: some managers enter pto time IN advance, time clock data FROM the
  future should NOT be included IN used pto to date
    
  fixed total days IN period
*b*
  include pto_adjustments INTO ptoHoursEarned
*c*
  limit requested & approved to current period  
  
EXECUTE PROCEDURE pto_get_employee_pto ('17410');

*/           
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
@employeenumber = (SELECT employeenumber from __input);
-- *a*
IF (
  SELECT year(thruDate)
  FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeeNumber
    AND curdate() BETWEEN fromDate AND thruDate) = 9999 THEN 
    @fromDate = (
      SELECT cast(createtimestamp(year(curdate()), 
        (SELECT month(ptoAnniversary) FROM ptoEmployees WHERE employeenumber = @employeenumber),
        (SELECT dayOfMonth(ptoAnniversary) FROM ptoEmployees WHERE employeenumber = @employeenumber),
        0,0,0,0) AS sql_date)
      FROM system.iota);
ELSE @fromDate = (
  SELECT fromDate
  FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeenumber
    AND curdate() BETWEEN fromDate AND thruDate);  
ENDIF;    
IF (
  SELECT year(thruDate)
  FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeeNumber
    AND curdate() BETWEEN fromDate AND thruDate) = 9999 THEN 
    @thruDate = cast(timestampAdd(sql_tsi_day, -1, timestampadd(sql_tsi_year, 1, @fromDate)) AS sql_date);
ELSE @thruDate = (
  SELECT thruDate
  FROM pto_employee_pto_allocation
  WHERE employeenumber = @employeenumber
    AND curdate() BETWEEN fromDate AND thruDate);   
ENDIF;    
    
INSERT INTO __output
select h.employeenumber, h.name, h.store, h.originalHireDate, h.latestHireDate,
  h.department, h.position, 
  h.ptoAdmin, h.ptoAnniversary, 
-- *b*  
  h.hours + coalesce(n.ptoAdjustment,0) AS ptoHoursEarned,
  coalesce(i.ptoUsed, 0) AS ptoUsed, 
  h.hours  + coalesce(n.ptoAdjustment,0) - coalesce(i.ptoUsed, 0) - 
    coalesce(k.ptoHoursApproved,0) - coalesce(m.ptoHoursRequested,0) AS ptoHoursRemaining,
  TRIM(h.department) + ':' + h.position,
-- *a*
  @fromDate AS ptoFromDate, @thruDate AS ptoThruDate,
  j.ptoPolicyHtml,
  coalesce(k.ptoHoursApproved,0) AS ptoHoursApproved, 
  coalesce(m.ptoHoursRequested,0) AS ptoHoursRequested,
  timestampdiff(sql_tsi_day, @fromDate, @thruDate) + 1 AS totalDaysInPeriod,
  timestampdiff(sql_tsi_day, curdate(), @thrudate) AS remainingDaysInPeriod 
FROM (
  SELECT a.employeenumber, a.ptoAnniversary, 
    TRIM(b.firstname) + ' ' + b.lastname as name, b.storecode,
    b.fullPartTime, 
    c.department, c.position,
    trim(f.firstname) + ' ' + TRIM(f.lastname) AS ptoAdmin,
    g.hours, 
-- *a*     
    @fromDate AS fromDate,
    @thruDate AS thruDate,
    CASE b.storecode
      WHEN 'RY1' THEN 'RY1 - GM'
      WHEN 'RY2' THEN 'RY2 - Honda'
    END AS store,
    gg.ymhdto AS originalHireDate, gg.ymhdte AS latestHireDate
  FROM ptoEmployees a
  INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  INNER JOIN ptoPositionFulfillment c on a.employeenumber = c.employeenumber  
  INNER JOIN ptoAuthorization d on c.department = d.authForDepartment
    AND c.position = d.authForPosition
  INNER JOIN ptoPositionFulfillment e on d.authByDepartment = e.department 
    AND d.authByPosition = e.position 
  INNER JOIN dds.edwEmployeeDim f on e.employeenumber = f.employeenumber 
    AND f.currentrow = true 
  INNER JOIN pto_employee_pto_allocation g on a.employeenumber = g.employeenumber
    -- return hours only
    AND curdate() BETWEEN fromDate AND thruDate
  LEFT JOIN dds.stgArkonaPYMAST gg on a.employeenumber = gg.ymempn    
  WHERE a.employeenumber = @employeeNumber) h
LEFT JOIN ( --ptoUsed: LEFT JOIN, may NOT be any used
  SELECT a.employeenumber, SUM(a.hours) AS ptoUsed
  FROM pto_used a  
  WHERE a.employeenumber = @employeeNumber
-- *a*
-- limit to curdate(), some managers enter pto time IN advance
     AND a.theDate BETWEEN @fromDate AND curdate()
  GROUP BY a.employeenumber) i on h.employeenumber = i.employeenumber
LEFT JOIN ptoDepartmentPositions j on h.department = j.department
  AND h.position = j.position
-- *c*  
LEFT JOIN (
  SELECT employeeNumber, SUM(hours) AS ptoHoursApproved
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Approved'
-- *a*    
    AND thedate BETWEEN curdate() AND @thruDate        
  GROUP BY employeeNumber) k on h.employeenumber = k.employeeNumber  
-- *c*  
LEFT JOIN ( 
  SELECT employeeNumber, SUM(hours) AS ptoHoursRequested
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Pending'
-- *a* 
    AND thedate BETWEEN curdate() AND @thruDate    
  GROUP BY employeeNumber) m on h.employeeNumber = m.employeeNumber  
-- *b*
LEFT JOIN (
  SELECT employeenumber, fromDate, SUM(hours) AS ptoAdjustment
  FROM pto_adjustments
  GROUP BY employeeNumber, fromDate) n on h.employeeNumber = n.employeenumber
    AND h.fromDate = n.fromDate;   
END;

--<----------------------------------------------------------------------------------/> 
-- pto_adjustments
-- lindsey
INSERT INTO pto_adjustments (employeeNumber,fromDate,hours,reason)
SELECT employeenumber, fromdate, 50, 'Hiring Stip'
FROM pto_employee_pto_allocation
WHERE curdate() BETWEEN fromDate AND thruDate
  AND employeenumber = '17410';  
-- kim & dawn
INSERT INTO pto_adjustments (employeeNumber,fromDate,hours,reason)
SELECT employeenumber, fromdate, 40, 'Rollover'
FROM pto_employee_pto_allocation
WHERE curdate() BETWEEN fromDate AND thruDate
  AND employeenumber IN ('196341','163096');  
--<----------------------------------------------------------------------------------/> 
-- fix heffernan allocation
UPDATE pto_employee_pto_allocation
SET thrudate = '07/14/2015'
WHERE employeenumber = '164015'
  AND fromdate = '01/01/2014';

UPDATE pto_employee_pto_allocation
SET fromdate = '07/15/2015'
WHERE employeenumber = '164015'
  AND thruDate = '12/31/9999';
--<----------------------------------------------------------------------------------/> 
CREATE TABLE pto_tmp_manager_employees (
      employeeNumber cichar(9),
      name CICHAR ( 52 ),
      department CICHAR ( 30 ),
      title CICHAR ( 30 ),
      ptoAnniversary DATE,
      ptoExpiringWeeks integer,
      ptoHoursEarned DOUBLE ( 15 ),
      ptoHoursRequested integer,
      ptoHoursApproved integer,
      ptoHoursUsed DOUBLE ( 15 ),
      ptoHoursRemaining DOUBLE) IN database;
--<----------------------------------------------------------------------------------/> 
ALTER PROCEDURE pto_get_manager_employees (
      employeeNumber CICHAR ( 9 ),
      employeeNumber cichar(9) output,
      name CICHAR ( 52 ) OUTPUT,
      department CICHAR ( 30 ) OUTPUT,
      title CICHAR ( 30 ) OUTPUT,
      ptoAnniversary DATE OUTPUT,
      ptoExpiringWeeks integer output,
      ptoHoursEarned DOUBLE ( 15 ) OUTPUT,
      ptoHoursRequested integer output,
      ptoHoursApproved integer output,
      ptoHoursUsed DOUBLE ( 15 ) OUTPUT,
      ptoHoursRemaining DOUBLE ( 15 ) OUTPUT)
      
BEGIN
/*       
11/02/14 need to exclude part timers  
11/9/14
  need to include pto_adjustments INTO ptoEarned
  AND limit request/approved to the current period for each employee
  think i am going to have to DO subqueries IN the SELECT statement
  OR a CURSOR
  the problem, of course, IS that the from/thru period IS different
  for every employee
  
EXECUTE PROCEDURE pto_get_manager_employees ('157995');
*/  

DECLARE @employeeNumber string;

DECLARE @employeesCursor CURSOR AS 
  SELECT d.employeeNumber
  FROM ptoAuthorization a
  INNER JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
    AND a.authByPosition = b.position
  INNER JOIN ptoPositionFulfillment c on a.authForDepartment = c.department
    AND a.authForPosition = c.position  
  INNER JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
    AND d.currentrow = true 
    AND d.active = 'active' 
    AND d.fullparttime = 'full'
  WHERE b.employeenumber = @employeenumber;
@employeeNumber = (select employeenumber from __input); --'157995';
DELETE FROM pto_tmp_manager_employees;
OPEN @employeesCursor;
TRY
  WHILE FETCH @employeesCursor DO
    INSERT INTO pto_tmp_manager_employees
    SELECT employeenumber, name, department, position, ptoAnniversary, 
      timestampdiff(sql_tsi_week, curdate(), ptoThruDate) AS ptoExpiringWeeks,
      ptoHoursEarned, ptoHoursRequested, ptoHoursApproved, ptoHoursUsed,
      ptoHoursRemaining
    FROM (EXECUTE PROCEDURE pto_get_employee_pto(@employeesCursor.employeeNumber)) a;
  END WHILE;
FINALLY
  CLOSE @employeesCursor;
END TRY;    
INSERT INTO __output
SELECT top 200 *
FROM pto_tmp_manager_employees
ORDER BY ptoExpiringWeeks;
END;
--<----------------------------------------------------------------------------------/> 
-- change ptoAnniversary
-- jared duckstad, '136170' 9/30/02 -> '09/29/1999', 'Other'
-- jay olson, '1106399', 2/11/02 -> '08/19/1994', 'Original'
-- michael schwann, '1124436' , 2/27/12 -> '05/16/2005' 'Original'
  
  /*
DROP TABLE #current;
DROP TABLE #omg;
*/
DECLARE @rollout date;
DECLARE @employeeNumber string;
DECLARE @newAnniversary date;
DECLARE @anniversaryType string;
@rollout = '11/02/2014';   
@employeenumber = '1124436';
@newAnniversary = '05/16/2005';
@anniversaryType = 'Original';
/**/
BEGIN TRANSACTION;
TRY 
UPDATE ptoEmployees
SET ptoAnniversary = @newAnniversary,
    anniversaryType = @anniversaryType
WHERE employeeNumber = @employeeNumber;
DELETE FROM pto_employee_pto_allocation
WHERE employeeNumber = @employeeNumber;

SELECT c.employeenumber, pto2014start AS ptofrom,
  CASE 
     WHEN year(ptoanniversary) = 2013 and cast(timestampadd(sql_tsi_year, 1, ptoanniversary) AS sql_date) > curdate()
       THEN cast(createTimestamp(2014, month(ptoanniversary), 
          dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date) -1
     ELSE pto2015start - 1   
  END AS ptothru,
  CASE 
    WHEN ptocategory = 0 THEN 0
    ELSE round(timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 * (
      SELECT ptohours
      FROM pto_categories
      WHERE ptocategory = c.ptocategory), 0)
  END AS hours
INTO #current  
FROM (                                                                        
  SELECT a.*, b.ptocategory,
    CASE 
      WHEN year(ptoanniversary) = 2013 and cast(timestampadd(sql_tsi_year, 1, ptoanniversary) AS sql_date) < curdate()
        THEN cast(createTimestamp(2014, month(ptoanniversary), 
          dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date)
      ELSE cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
    END AS pto2014start,      
    cast(createTimestamp(2015, month(ptoanniversary), 
      dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date) AS pto2015start 
  FROM ptoEmployees a
  INNER JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
  WHERE @rollout BETWEEN b.fromdate AND b.thrudate AND a.employeenumber = @employeenumber) c;
 
/**/
SELECT c.employeenumber, fromDate, thruDate, 
  case year(thruDate)
    when 9999 then 192
    else ptoHours
   END AS ptoHours
INTO #omg   
FROM (
  SELECT a.*, b.n,
    cast(timestampadd(sql_tsi_year, n, ptoAnniversary) AS sql_date) AS fromDate,
    CASE b.n
      WHEN 20 THEN CAST('12/31/9999' AS sql_date)
      ELSE cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, n + 1, 
        ptoAnniversary)) AS sql_date) 
    END AS thruDate
  FROM ptoEmployees a
  LEFT JOIN dds.tally b on 1 = 1
    AND b.n BETWEEN 0 AND 20
  WHERE a.employeenumber = @employeeNumber) c
LEFT JOIN pto_categories d on timestampdiff(sql_tsi_year, c.ptoAnniversary, c.thrudate) - 1
  BETWEEN d.fromYearsTenure AND d.thruYearsTenure 
LEFT JOIN #current z on z.ptoThru = c.thruDate 
WHERE thruDate > curdate()
  AND z.employeeNumber IS NULL;

IF (
  SELECT COUNT(*)
  FROM #omg) = 1 THEN
    INSERT INTO pto_employee_pto_allocation  
    SELECT *
    FROM #current;
    INSERT INTO pto_employee_pto_allocation
    SELECT employeeNumber, ptoThru + 1, '12/31/9999', 192
    FROM #current;
ELSE      
  INSERT INTO pto_employee_pto_allocation  
  SELECT *
  FROM #current
  UNION 
  SELECT * 
  FROM #omg;  
ENDIF;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
--<----------------------------------------------------------------------------------/> 
--<----------------------------------------------------------------------------------/> 
--<----------------------------------------------------------------------------------/> 
--<----------------------------------------------------------------------------------/> 
--<----------------------------------------------------------------------------------/> 
--<----------------------------------------------------------------------------------/> 
--<----------------------------------------------------------------------------------/> 
--<----------------------------------------------------------------------------------/> 
--<----------------------------------------------------------------------------------/> 