first whack, simply edit the employeenumber IN ptoEmployees, let RI DO the rest,
  which should accomodate bringing over used pto for the old employeenumber
  AND keeps the anniversary date AS the anniversary date FROM the old store
THEN adjust position/authorization AS needed
IF they currently exist IN tpEmployees, no changes are needed, for now, 
the transferred folks are not getting a new email address to reflect where
they are currently employed
we are NOT deleting the compli record AND creating another, simply editing it

so, first, who are the transfers
USING the notion of new pto employees (NOT yet IN ptoEmployees based on employeenumber)


!!!! need to ADD a xfr test to hr admin !!!!!!!!!!!!
possibly also to edwEmployeeDim ETL
employeenumber IN tpEmployees

 
SELECT a.*, b.employeenumber, b.hiredate, b.termdate
FROM ( -- from pto_get_new_employees():
  SELECT a.employeenumber, a.firstname, a.lastname, a.hiredate, 
    d.ymhdto,
    CASE
      WHEN e.userid IS NULL THEN false
      ELSE true
    END AS inCompli
  FROM dds.edwEmployeeDim a
  LEFT JOIN pto_exclude b on a.employeenumber = b.employeenumber
  LEFT JOIN ptoEmployees c on a.employeenumber = c.employeenumber
  LEFT JOIN dds.stgArkonaPYMAST d on a.employeenumber = d.ymempn
  LEFT JOIN pto_compli_users e on a.employeenumber = userid
  WHERE a.currentRow = true
    AND a.active = 'active'
    AND a.payrollclass = 'hourly'
    AND a.fullparttime = 'full'
    AND b.employeenumber IS NULL
    AND c.employeenumber IS NULL) a
LEFT JOIN dds.edwEmployeeDim b on a.lastname = b.lastname AND a.firstname = b.firstname
  AND a.employeenumber <> b.employeenumber     
WHERE b.employeenumber IS NOT NULL   

-- use EXISTS, NOT definitive but potential xfrs
-- need the old employeenumber for referencing the ptoEmployees records
-- aha, no old pto row for Kyle, so no row to edit, need to manually check
-- for ptoUsed, etc
SELECT *
FROM ( 
  SELECT a.employeenumber, a.firstname, a.lastname, a.hiredate, 
    d.ymhdto, 
    CASE
      WHEN e.userid IS NULL THEN false
      ELSE true
    END AS inCompli,
    (SELECT DISTINCT employeenumber
      FROM dds.edwEmployeeDim 
      WHERE lastname = a.lastname
        AND firstname = a.firstname
        AND employeenumber <> a.employeenumber) AS oldEmployeeNumber    
  FROM dds.edwEmployeeDim a
  LEFT JOIN pto_exclude b on a.employeenumber = b.employeenumber
  LEFT JOIN ptoEmployees c on a.employeenumber = c.employeenumber
  LEFT JOIN dds.stgArkonaPYMAST d on a.employeenumber = d.ymempn
  LEFT JOIN pto_compli_users e on a.employeenumber = userid
  WHERE a.currentRow = true
    AND a.active = 'active'
    AND a.payrollclass = 'hourly'
    AND a.fullparttime = 'full'
    AND b.employeenumber IS NULL
    AND c.employeenumber IS NULL
    AND EXISTS (
      SELECT 1
      FROM dds.edwEmployeeDim
      WHERE lastname = a.lastname
        AND firstname = a.firstname
        AND employeenumber <> a.employeenumber)) m
LEFT JOIN ptoEmployees n on m.oldEmployeenumber = n.employeeNumber        
      
-- this shows terms, but also includes 2 transfers     
SELECT *
FROM ptoEmployees a
WHERE NOT EXISTS (
  SELECT 1
  FROM dds.edwEmployeeDim 
  WHERE employeenumber = a.employeenumber
    AND currentrow = true
    AND active = 'active')
    
DO a UNION of used,req,appr AND pivot

SELECT employeenumber, 
  SUM(CASE WHEN cat = 'used' THEN coalesce(hours, 0) else 0 END) AS used,
  SUM(CASE WHEN cat = 'appr' THEN coalesce(hours, 0) else 0 END) AS appr,
  SUM(CASE WHEN cat = 'req' THEN coalesce(hours, 0) else 0 END) AS req,
  SUM(coalesce(hours,0)) AS total
FROM (  
  SELECT 'used' AS cat,a.employeenumber, SUM(a.hours) AS hours
  FROM pto_used a
  WHERE a.theDate BETWEEN '01/01/2014' AND curdate()
  GROUP BY a.employeenumber
  union
  SELECT 'appr',employeeNumber, SUM(hours) 
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Approved'
    AND thedate >= curdate()
  GROUP BY employeeNumber
  union
  SELECT 'req',employeeNumber, SUM(hours) 
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Pending'
    AND thedate >= curdate() 
  GROUP BY employeeNumber) x
WHERE employeenumber IN ('118310','185801','293210','218310','285800','193210')  
GROUP BY employeenumber  
    
-- ok, lets just forge ahead, rbar  
--< kyle bragunier - just LIKE a new user -------------------------------------<
-- except he ALL - fucking - ready got 2 rows IN tpEmployees

SELECT firstname, lastname
FROM tpemployees
GROUP BY firstname,lastname HAVING COUNT(*) > 1

ADD a unique index to tpEmployees (firstname,lastname)
after deleting ALL of kyles rows



SELECT firstname, lastname
FROM tpemployees
GROUP BY firstname,lastname HAVING COUNT(*) > 1

DELETE 
FROM employeeAppAuthorization
WHERE username IN (
SELECT username
FROM tpemployees
WHERE lastname = 'bragunier'); 

DELETE FROM tpEmployees
WHERE firstname = 'kyle'
  AND lastname = 'bragunier';  

delete FROM ptoEmployees WHERE employeenumber = '118310'  
-- ok, since i am treating this AS a new entry with an old anniv date (no existing used/req/appr)
he can be added via the hr admin, use compli for values  
looks LIKE it worked ok
--/> kyle bragunier - just LIKE a new user ------------------------------------/>

-- actual transfers
--< Adam Lindquist ------------------------------------------------------------<
/*

EXISTS IN tpEmployees & ptoEmployees & (sco)ServiceWriters fuck fuck fuck
*/
select * FROM tpemployees WHERE username LIKE 'alindq%'
SELECT * FROM ptoEmployees WHERE employeenumber IN ('185801','285800');
SELECT * FROM servicewriters WHERE lastname = 'lindquist'

so,what i propose doing (outside of fucking sco) IS to 
tpEmployees AND ptoEmployees: UPDATE (285800 -> 185801) the employeenumber 
employeeAppAuthorizatin: remove sco, ADD pto (ptomanager)
positionFulfillment: ry1 pdq:asst mgr

select *
FROM pto_employee_pto_allocation
WHERE employeenumber IN ('185801','285800');

BEGIN TRANSACTION;
TRY 
UPDATE tpEmployees
SET employeenumber = '185801'
WHERE employeenumber = '285800';

DELETE 
FROM employeeAppAuthorization
WHERE username = (
  SELECT username 
  FROM tpEmployees
  WHERE employeenumber = '185801')  
AND appcode = 'sco';

INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'alindquist@gfhonda.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';  
  
UPDATE ptoEmployees
SET employeenumber = '185801'
WHERE employeenumber = '285800';

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 

UPDATE ptoPositionFulfillment
-- SELECT * FROM ptoPositionFulfillment
SET department = 'RY1 PDQ'
WHERE employeenumber = '185801';
  
--/> Adam Lindquist -----------------------------------------------------------/>

--< Kelsie Mmcmillin ----------------------------------------------------------<
1. reference current crecords IN compli AND arkona
2. check for used/req/appr on old employeenumber
3. check for rows IN current system
SELECT * FROM pto_compli_users WHERE email like 'kmcm%'
select * FROM tpemployees WHERE username LIKE 'kmc%'
SELECT * FROM employeeAppAuthorization WHERE username LIKE 'kmcm%'
SELECT * FROM ptoEmployees WHERE employeenumber IN ('193210','293210');
SELECT * FROM ptoPositionFulfillment WHERE employeenumber IN ('193210','293210');

tpEmployees: UPDATE employeenumber -- no row yet
employeeAppAuthorization: no changes requd -- no row yet
ptoEmployees: UPDATE employeenumber
ptoPositionFulfillment: bds:operator -> ry2 sales:reception

UPDATE ptoEmployees
SET employeeNumber = '293210'
WHERE employeenumber = '193210';

UPDATE ptoPositionFulfillment
SET department = 'RY2 Sales',
    position = 'Reception'
WHERE employeenumber = '293210';    

SELECT *
--/> Kelsie Mmcmillin ---------------------------------------------------------/>




--< Dan Wiebusch -------------------------------------------------------------<
SELECT * FROM pto_compli_users WHERE email LIKE 'dwie%'
SELECT * FROM tpemployees WHERE username LIKE 'dwie%'
SELECT * FROM employeeappauthorization WHERE username = 'dwiebusch@gfhonda.com'
SELECT * FROM ptoEmployees WHERE username like 'dwie%'
SELECT * FROM ptoPositionFulfillment WHERE employeenumber = '1148720'
His reccord IN compli has already been updated to reflect his employment at RY1

check for used pto under honda emp#

SELECT b.thedate, c.employeenumber, a.*
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
WHERE b.thedate BETWEEN '01/01/2014' AND curdate()    
  AND c.lastname = 'wiebusch'
  AND a.vacationhours + a.ptohours > 0
 
SELECT yhdemp, yhsaly,
   yhdtgp as gross,
   yhdvac as vacHours,
   yhdhol as holHours,
   yhdsck  as ptoHours,
   yhsaly*(yhdvac+yhdhol+yhdsck) AS totalPtoPay,
cast(CREATETIMESTAMP( 2000+yhdeyy, yhdemm, yhdedd, 0, 0, 0, 0 ) AS sql_date) AS payrollenddate,
cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date) AS checkdate
FROM dds.stgArkonaPYHSHDTA   
WHERE ypbcyy = 114
  AND yhdemp = '2148720'
  AND yhdtgp <> 0
  AND yhdvac + yhdsck > 0

BEGIN TRANSACTION;
TRY      
-- 1. tpEmployees, this one IS different, the email address IN compli was changed FROM gfhonda to rydellcars
--      updating tp.username should cascade to employeeAppAuthorization.username
--      no changes IN access 

  UPDATE tpEmployees
  SET username = 'dwiebusch@rydellcars.com',
      employeenumber = '1148720'
  WHERE fullname = 'Daniel Wiebusch';    
  
-- 2. ptoEmployees already has the correct username (username IS NOT IN any other pto TABLE)
--      employeenumer should cascade (ptoPositionFulfillment, pto_used, 
--      pto_requests, pto_Employee_pto_allocation
  UPDATE ptoEmployees
  SET employeenumber = '1148720'
  WHERE username = 'dwiebusch@rydellcars.com';
  
-- 3. ptoPositionFulfillment, FROM RY2 PDQ:Advisor to IT:Help Desk  
  UPDATE ptoPositionFulfillment
  SET department = 'IT',
      position = 'Help Desk'
  WHERE employeenumber = '1148720';      
  
-- pto_used FROM previous employeenumber  
  INSERT INTO pto_used(employeenumber,thedate,source,hours)
  values('1148720', '05/30/2014', 'Transfer Between Stores', 8); 
  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 

SELECT a.*
FROM pto_used a
LEFT JOIN pto_used_sources b on a.source = b.source
WHERE b.source IS NULL; 


--/> Dan Wiebusch -----------------------------------------------------------/>




--< Larry Stadstad -------------------------------------------------------------<
SELECT * FROM pto_compli_users WHERE email LIKE 'lsta%'
SELECT * FROM tpemployees WHERE username LIKE 'lsta%'
SELECT * FROM employeeappauthorization WHERE username LIKE 'lsta%'
SELECT * FROM ptoEmployees WHERE username like 'lsta%'
SELECT * FROM ptoPositionFulfillment WHERE employeenumber = '1130690'
His reccord IN compli has already been updated to reflect his employment at RY1

RY1: 1130690, lstadstad@rydellcars.com,  RY1 Sales:New Car Sales Consultant
RY2: 2130690, lstadstad@gfhonda.com, RY2 Sales:Sales Consultant
He currently has no used pto

SELECT b.thedate, c.employeenumber, a.*
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
WHERE b.thedate BETWEEN '01/01/2014' AND curdate()    
  AND c.lastname = 'stadstad'
  AND a.vacationhours + a.ptohours > 0
 
SELECT yhdemp, yhsaly,
   yhdtgp as gross,
   yhdvac as vacHours,
   yhdhol as holHours,
   yhdsck  as ptoHours,
   yhsaly*(yhdvac+yhdhol+yhdsck) AS totalPtoPay,
cast(CREATETIMESTAMP( 2000+yhdeyy, yhdemm, yhdedd, 0, 0, 0, 0 ) AS sql_date) AS payrollenddate,
cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date) AS checkdate
FROM dds.stgArkonaPYHSHDTA   
WHERE ypbcyy = 114
  AND yhdemp = '2130690'
  AND yhdtgp <> 0
  AND yhdvac + yhdsck > 0

BEGIN TRANSACTION;
-- ri prevents this TRANSACTION, creates a lock 
TRY      
-- 1. tpEmployees, this one IS different, the email address IN compli was changed FROM gfhonda to rydellcars
--      updating tp.username should cascade to employeeAppAuthorization.username
--      no changes IN access 

  UPDATE tpEmployees
  SET username = 'lstadstad@rydellcars.com',
      employeenumber = '1130690'
  WHERE fullname = 'Larry Stadstad';    
  
-- 2. employeenumer should cascade (ptoPositionFulfillment, pto_used, 
--      pto_requests, pto_Employee_pto_allocation
  UPDATE ptoEmployees
  SET employeenumber = '1130690',
      username = 'lstadstad@rydellcars.com'
  WHERE employeenumber = '2130690';
  
-- 3. ptoPositionFulfillment, FROM RY2 Sales:Sales Consultant to RY1 Sales:New Car Sales Consultant
  UPDATE ptoPositionFulfillment
  SET department = 'RY1 Sales',
      position = 'New Car Sales Consultant'
  WHERE employeenumber = '1130690';      
  
-- pto_used FROM previous employeenumber, no used pto for honda

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 

SELECT a.*
FROM pto_used a
LEFT JOIN pto_used_sources b on a.source = b.source
WHERE b.source IS NULL; 


--/> Larry Stadstad -----------------------------------------------------------/>

-- 11/3/15 
dan lizakowski FROM ry2 to ry1
tom aubol FROM ry1 to ry2

1. UPDATE tpEmployees:
            employenumber, username, storecode          
2. employeeAppAuthorization
3. ptoEmployees: 
     employeenumber
4. ptoPositionFulfillment
     employeenumber should have cascaded FROM 3
       department, position
5. tool access      
       
lizakowski       
SELECT * FROM tpemployees WHERE lastname = 'lizakowski' 

DECLARE @full_name string;
DECLARE @new_username string;
DECLARE @old_username string;
DECLARE @new_employee_number string;
DECLARE @old_employee_number string;
DECLARE @new_storecode string;
  
BEGIN TRANSACTION;
TRY   
  @full_name = 'Daniel Lizakowski';
  @new_username = 'dlizakowski@rydellcars.com';
  @new_employee_number = '186850';
  @old_employee_number = '285820';
  @new_storecode = 'RY1';
  @old_username = 'dlizakowski@gfhonda.com';
  
  -- WHEN role IS substantially different simply DELETE FROM empAppAuth, 
  -- ELSE this can be updated via cascade on tpEmployees update.
  DELETE FROM employeeAppAuthorization
  WHERE username = @old_username;
  
  UPDATE tpemployees
  SET username = @new_username,
      employeenumber = @new_employee_number,
      storecode = @new_storecode
  WHERE fullname = @full_name;
  
  INSERT INTO employeeAppAuthorization (userName, appName, appSeq, appCode, 
    appRole, functionality)
  SELECT @new_username, appname, appseq, appcode, approle, functionality
  FROM applicationmetadata
  WHERE appcode = 'pto'
    AND approle = 'ptoemployee';
  
  UPDATE ptoEmployees
  SET employeenumber = @new_employee_number,
      username = @new_username
  WHERE employeenumber = @old_employee_number;
    
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;    
END TRY;    
-- this would NOT EXECUTE within the transaction
UPDATE ptoPositionFulfillment
SET department = 'RY1 Sales',
    position = 'New Car Sales Consultant'
WHERE employeenumber = @new_employee_number;                 



-- 11/3/15 
tom aubol FROM ry1 to ry2

1. UPDATE tpEmployees:
            employenumber, username, storecode          
2. employeeAppAuthorization
3. ptoEmployees: 
     employeenumber
4. ptoPositionFulfillment
     employeenumber should have cascaded FROM 3
       department, position
5. tool access      
       
aubol       
SELECT * FROM tpemployees WHERE lastname = 'aubol' 

DECLARE @full_name string;
DECLARE @new_username string;
DECLARE @old_username string;
DECLARE @new_employee_number string;
DECLARE @old_employee_number string;
DECLARE @new_storecode string;
  
BEGIN TRANSACTION;
TRY   
  @full_name = 'Thomas Aubol';
  @new_username = 'taubol@gfhonda.com';
  @new_employee_number = '27534';
  @old_employee_number = '17534';
  @new_storecode = 'RY2';
  @old_username = 'taubol@rydellcars.com';
  
  -- WHEN role IS substantially different simply DELETE FROM empAppAuth, 
  -- ELSE this can be updated via cascade on tpEmployees update.
--  DELETE FROM employeeAppAuthorization
--  WHERE username = @old_username;
  
  UPDATE tpemployees
  SET username = @new_username,
      employeenumber = @new_employee_number,
      storecode = @new_storecode
  WHERE fullname = @full_name;
  
--  INSERT INTO employeeAppAuthorization (userName, appName, appSeq, appCode, 
--    appRole, functionality)
--  SELECT 'dlizakowski@rydellcars.com', appname, appseq, appcode, approle, functionality
--  FROM applicationmetadata
--  WHERE appcode = 'pto'
--    AND approle = 'ptoemployee';
  
  UPDATE ptoEmployees
  SET employeenumber = @new_employee_number,
      username = @new_username
  WHERE employeenumber = @old_employee_number;
    
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;    
END TRY;    
-- this would NOT EXECUTE within the transaction
UPDATE ptoPositionFulfillment
SET department = 'RY2 Sales',
    position = 'Sales Manager'
WHERE employeenumber = @new_employee_number;                 

-- 3/1/16
Nick Shirek - back to ry2 AS GSM

SELECT * FROM tpemployees WHERE lastname = 'shirek' 

DECLARE @full_name string;
DECLARE @new_username string;
DECLARE @old_username string;
DECLARE @new_employee_number string;
DECLARE @old_employee_number string;
DECLARE @new_storecode string;
  
BEGIN TRANSACTION;
TRY   
  @full_name = 'Nicholas Shirek';
  @new_username = 'nshirek@gfhonda.com';
  @new_employee_number = '2126300';
  @old_employee_number = '1126300';
  @new_storecode = 'RY2';
  @old_username = 'nshirek@rydellcars.com';

  UPDATE tpemployees
  SET username = @new_username,
      employeenumber = @new_employee_number,
      storecode = @new_storecode
  WHERE fullname = @full_name;
  
  UPDATE ptoEmployees
  SET employeenumber = @new_employee_number,
      username = @new_username
  WHERE employeenumber = @old_employee_number;
    
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;    
END TRY;    
-- this would NOT EXECUTE within the transaction
UPDATE ptoPositionFulfillment
SET department = 'RY2 Sales',
    position = 'General Sales Manager'
WHERE employeenumber = @new_employee_number;       