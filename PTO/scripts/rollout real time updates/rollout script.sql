EXECUTE PROCEDURE sp_CreateIndex90( 'pto_requests', 'pto_requests.adi', 
  'theDate', 'theDate', '', 2, 1024, '' );
----------------------------------------------------------------------------------
CREATE PROCEDURE pto_update_requests()
BEGIN
/*
*/
BEGIN TRANSACTION;
TRY 
DELETE FROM pto_requests;
INSERT INTO pto_requests (employeeNumber,theDate,hours,requestStatus,
  requestType)
select left(userid, 7) AS employeeNumber, 
  cast(trim(substring(scheduledDate,6,2)) + '/' + 
    trim(right(TRIM(scheduledDate), 2)) + 
    '/' + LEFT(scheduledDate, 4) AS sql_date) AS theDate,
  MAX(hours) AS hours, LEFT(requestStatus, 12) AS requestStatus,
  LEFT(allocationType, 24) AS requestType
FROM pto_tmp_compli_timeoff a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
WHERE b.employeenumber IS NOT NULL 
GROUP BY userid, requestStatus, scheduledDate, allocationType;
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  
END;
----------------------------------------------------------------------------------
DROP TABLE pto_compli_users;
CREATE TABLE pto_compli_users ( 
      userid CIChar( 9 ) constraint NOT NULL,
      firstName CIChar( 13 ),
      lastName CIChar( 14 ),
      email CIChar( 30 ),
      supervisorID CIChar( 13 ),
      supervisorName CIChar( 60 ),
      locationID CIChar( 10 ),
      locationName CIChar( 10 ),
      dateofHire Date,
      title CIChar( 31 ),
      status cichar(1),
      policyAssigned cichar(1),
      policySigned cichar(1),
      trainingAssigned cichar(1),
      trainingCompleted cichar(1),
      formsAssigned cichar(1),
      formsCompleted cichar(1),
      formsInNotification cichar(1),
      exclusionary CIChar( 10 ),
      constraint PK primary key (userid)
      ) IN DATABASE;
----------------------------------------------------------------------------------      
CREATE PROCEDURE pto_update_compli_users()
BEGIN
/*
*/
BEGIN TRANSACTION;
TRY 
DELETE FROM pto_compli_users;
INSERT INTO pto_compli_users (userid,firstName,lastName,email,supervisorID,
  supervisorName,locationID,locationName,
  dateofHire,title,status,policyAssigned,
  policySigned,trainingAssigned,trainingCompleted,
  formsAssigned,formsCompleted,formsInNotification,
  exclusionary)
select userid,firstName,lastName,email,supervisorID,
  supervisorName,locationID,locationName,
  dateofHire,title,trim(status),trim(policyAssigned),
  trim(policySigned),trim(trainingAssigned),trim(trainingCompleted),
  trim(formsAssigned),trim(formsCompleted),trim(formsInNotification),
  exclusionary
FROM pto_tmp_compli_users;
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  
END;
----------------------------------------------------------------------------------      
CREATE PROCEDURE pto_update_pto_used()
BEGIN
/*
*/
BEGIN TRANSACTION;
TRY 
DELETE FROM pto_used WHERE source = 'Time Clock';
INSERT INTO pto_used (employeeNumber, theDate, hours, source)
SELECT m.employeenumber, theDate, ptoHours, 'Time Clock'
FROM ptoEmployees m
LEFT JOIN dds.edwEmployeeDim p on m.employeenumber = p.employeenumber
  AND p.currentrow = true
  AND p.activecode = 'a'
INNER JOIN (
  SELECT c.employeenumber, b.thedate, 
    a.vacationhours + a.ptohours AS ptoHours
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.day b on a.datekey = b.datekey
    AND b.theyear = 2014
  INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey  
  WHERE a.vacationhours > 0 OR  a.ptohours > 0) n on m.employeenumber = n.employeenumber
WHERE p.lastname IS NOT NULL; 
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  
END;
----------------------------------------------------------------------------------      
-- clean up some cruft
-- tables
EXECUTE PROCEDURE sp_RenameDDObject('ptoClosureTest','zUnused_ptoClosureTest', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject('pto_at_rollout','zUnused_pto_at_rollout', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject('pto_compli_timeoff','zUnused_pto_compli_timeoff', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject('pto_employee_pto_category_dates','zUnused_pto_employee_pto_category_dates', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject('pto_formula_clean_annDate','zUnused_pto_formula_clean_annDate', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject('pto_match_bf','zUnused_pto_match_bf', 1, 0);
