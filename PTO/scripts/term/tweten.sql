/*
1/13/16
Rethinking Terms

the pto structure IS the only source of information regarding an individuals
pto earnings AND usage
This came painfully to mind WHEN after terminating an employee BY
deleting ALL ptoEmployee , pto_used, pto_allocation etc rows, HR needed to 
know what the PTO status was at te time of termination

So, the new policy, effective immediately:
Vision:
    tpEmployees & employeeAppAuthorization: DELETE, no need for history

    pto: only UPDATE tpoEmployees.active to false
         NO MORE DELETING
         Still need to flesh out process of termed employee IS a pto authorizer
         
Tool: UPDATE users.active to False
      UPDATE ThruTS IN ContactMechanisms, ApplicationUsers, PartyRelationships, PartyPrivileges         
    
*/    

-- vision 
-- verify that this IS the correct person
DECLARE @username string;
DECLARE @isAdmin integer;
--@username = 'bjohnson2@rydellcars.com';
@username = (
  SELECT username 
  FROM tpEmployees
  WHERE lastname = 'carlson'
    AND firstname = 'christopher');
-- does he admin anyone's pto
@isAdmin = (
  SELECT COUNT(*)
  FROM ptoAuthorization a
  INNER JOIN (
    SELECT *
    FROM ptoPositionFulfillment
    WHERE employeenumber = (
      SELECT employeenumber
      FROM tpemployees
      WHERE username = @username)) b on a.authByDepartment = b.department
    AND a.authByPosition =  b.position);     
-- SELECT @isAdmin FROM system.iota;      
IF @isAdmin = 0 THEN 
-- no, go ahead AND whack it all 
  BEGIN TRANSACTION;
  TRY 
    DELETE 
    FROM employeeappauthorization
    WHERE username = @username; 
    
    DELETE 
    FROM tpEmployees
    WHERE username = @username;
    
    UPDATE ptoEmployees
    SET active = False
    WHERE username = @username;
  COMMIT WORK;
  CATCH ALL
    ROLLBACK;
    RAISE;
  END TRY;
ENDIF;  

-- tool
-- leave people, he may no longer be an employee,but he IS still a person
-- partyid remains
-- SELECT * FROM dps.people WHERE partyid = 'a9cae609-bc67-ca45-9fa5-746012615935'
-- SELECT * FROM dps.users WHERE username LIKE '%carlson%'
DECLARE @PartyID string;
DECLARE @NowTS timestamp;

@PartyID = (SELECT PartyID FROM dps.users WHERE username = 'ccarlson');
@NowTS = (SELECT now() FROM system.iota);

BEGIN TRANSACTION;
TRY 
  UPDATE dps.contactmechanisms
  SET ThruTS = @NowTS
  WHERE PartyID = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE dps.users
  SET Active = False
  WHERE partyid = @PartyID;
  
  UPDATE dps.ApplicationUsers
  SET ThruTS = @NowTS
  WHERE partyid = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE dps.PartyRelationships
  SET ThruTS = @NowTS
  WHERE partyid2 = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE dps.PartyPrivileges
  SET ThruTS = @NowTS
  WHERE PartyID = @PartyID
  AND ThruTS IS NULL;
  
  COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END;   
