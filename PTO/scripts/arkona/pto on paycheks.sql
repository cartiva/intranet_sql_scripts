-- terry harmon, 
SELECT * 
FROM stgArkonaPYHSHDTA 
WHERE yhdemp = (SELECT employeenumber FROM edwEmployeeDim WHERE lastname = 'harmon' and firstname = 'terry' AND currentrow = true)
ORDER BY ypbcyy DESC, ypbnum DESC 

-- no clock hour since 1/22/15
SELECT b.thedate, a.*
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  AND c.lastname = 'harmon'
  AND c.firstname = 'terry'
--  AND c.currentrow = true
WHERE a.clockhours + a.vacationhours + a.ptohours + a.holidayhours > 0  
ORDER BY b.thedate DESC   

SELECT * FROM (
SELECT month(checkdate) as month, year(checkdate) AS year, 
  SUM(vacHours) AS vac, SUM(ptohours) AS pto, SUM(vacHours) + SUM(ptohours) AS total
FROM (
  SELECT yhdemp, yhsaly,
     yhdtgp as gross,
     yhdvac as vacHours,
     yhdhol as holHours,
     yhdsck  as ptoHours,
     yhsaly*(yhdvac+yhdhol+yhdsck) AS totalPtoPay,
  cast(CREATETIMESTAMP( 2000+yhdeyy, yhdemm, yhdedd, 0, 0, 0, 0 ) AS sql_date) AS payrollenddate,
  cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date) AS checkdate
  FROM dds.stgArkonaPYHSHDTA   
  WHERE ypbcyy IN (114,115)
    AND yhdemp = '162428'
    AND yhdtgp <> 0) x
--  AND yhdvac + yhdsck > 0

GROUP BY year, month) y
UNION
SELECT 99, 2015, 
  SUM(vacHours) AS vac, SUM(ptohours) AS pto, SUM(vacHours) + SUM(ptohours) AS total
FROM (
SELECT yhdemp, yhsaly,
   yhdtgp as gross,
   yhdvac as vacHours,
   yhdhol as holHours,
   yhdsck  as ptoHours,
   yhsaly*(yhdvac+yhdhol+yhdsck) AS totalPtoPay,
cast(CREATETIMESTAMP( 2000+yhdeyy, yhdemm, yhdedd, 0, 0, 0, 0 ) AS sql_date) AS payrollenddate,
cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date) AS checkdate
FROM dds.stgArkonaPYHSHDTA   
WHERE ypbcyy IN (114,115)
  AND yhdemp = '162428'
  AND yhdtgp <> 0
--  AND yhdvac + yhdsck > 0
) x
ORDER BY year,month


 
    
ADD a source of paid NOT clocked    

INSERT INTO pto_used_sources values('Misc Paid');

INSERT INTO pto_used
SELECT '162428', checkdate, 'Misc Paid', pto
FROM (
  SELECT checkdate, ptohours + vachours AS pto
  FROM(
    SELECT yhdemp, yhsaly,
       yhdtgp as gross,
       yhdvac as vacHours,
       yhdhol as holHours,
       yhdsck  as ptoHours,
       yhsaly*(yhdvac+yhdhol+yhdsck) AS totalPtoPay,
    cast(CREATETIMESTAMP( 2000+yhdeyy, yhdemm, yhdedd, 0, 0, 0, 0 ) AS sql_date) AS payrollenddate,
    cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date) AS checkdate
    FROM dds.stgArkonaPYHSHDTA   
    WHERE ypbcyy IN (114,115)
      AND yhdemp = '162428'
      AND yhdtgp <> 0) x
  WHERE checkdate > '02/01/2014'
    AND ptohours + vachours > 0) z;