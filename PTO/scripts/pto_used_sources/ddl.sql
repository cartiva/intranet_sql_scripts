DROP TABLE pto_used_sources;
CREATE TABLE pto_used_sources(
  source cichar(60),
  constraint PK primary key (source)) IN database;
  
INSERT INTO pto_used_sources values('Body Shop non hourly');
INSERT INTO pto_used_sources values('Sales Dept');
INSERT INTO pto_used_sources values('Time Clock');
INSERT INTO pto_used_sources values('Transfer Between Stores');
INSERT INTO pto_used_sources values('Donate');


SELECT COUNT(*)
FROM pto_used a
LEFT JOIN pto_used_sources b on a.source = b.source
WHERE b.source IS NULL; 