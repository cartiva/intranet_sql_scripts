ALTER TABLE ptoEmployees
ADD COLUMN pto_anniversary date;

SELECT *
FROM pto_formula_clean_annDate 

-- DROP TABLE pto_categories;
CREATE TABLE pto_categories (
  ptoCategory integer constraint NOT NULL,
  ptoHours integer constraint NOT NULL,
  fromYearsTenure integer constraint NOT NULL,
  thruYearsTenure integer constraint NOT NULL,
  constraint pk primary key (ptoCategory)) IN database;  
INSERT INTO pto_categories values (0,0,0,0);
INSERT INTO pto_categories values (1,72,1,1);
INSERT INTO pto_categories values (2,112,2,9);
INSERT INTO pto_categories values (3,152,10,19);
INSERT INTO pto_categories values (4,192,20,100);  

-- ptoEmployees.pto_anniversary
SELECT a.*, b.firstname, b.lastname, c.*
FROM ptoEmployees a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.activeCode = 'a'
LEFT JOIN pto_formula_clean_annDate c on a.employeenumber = c.employeenumber  

UPDATE ptoEmployees
SET pto_anniversary = (
  SELECT anniversaryDate
  FROM pto_formula_clean_annDate 
  WHERE employeenumber = ptoEmployees.employeenumber);

-- ptoEmployees with no anniversary_date
SELECT a.*, b.firstname, b.lastname, b.hiredate, c.ymhdte, c.ymhdto
FROM ptoEmployees a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'active'
LEFT JOIN dds.stgArkonaPYMAST c on b.employeenumber = c.ymempn 
WHERE a.pto_anniversary IS NULL 
ORDER BY a.employeenumber

-- exec decision, ptoEmployees: no history, WHEN someone IS termed,
--    remove the row ???, yeah, i think so
DELETE FROM ptoPositionFulfillment WHERE employeenumber = '1109826';
DELETE FROM ptoEmployees WHERE employeenumber = '1109826';

-- AND UPDATE ptoEmployees with no anniversary_date (hired after ben's spreadsheet)
UPDATE ptoEmployees
SET pto_anniversary = (
  SELECT hiredate
  FROM dds.edwEmployeeDim 
  WHERE employeenumber = ptoEmployees.employeenumber
    AND currentrow = true
    AND activeCode = 'a')
WHERE employeenumber IN (
  SELECT employeenumber
  FROM ptoEmployees
  WHERE pto_anniversary IS null);

--pto_employee_pto_catgegory_dates
ALTER TABLE pto_employee_pto_category_dates
ALTER COLUMN pto_category ptoCategory integer
ALTER COLUMN from_date fromDate date
ALTER COLUMN thru_date thruDate date

CREATE TABLE pto_employee_pto_category_dates (
  ptoCategory integer constraint NOT NULL,
  employeeNumber cichar(7) constraint NOT NULL,
  fromDate date constraint NOT NULL,
  thruDate date constraint NOT NULL,
  constraint pk primary key (ptoCategory,employeenumber)) IN database;
  
EXECUTE PROCEDURE sp_CreateIndex90( 'pto_employee_pto_category_dates', 'pto_employee_pto_category_dates.adi', 'FK1', 'ptoCategory', '', 2, 1024, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 'pto_employee_pto_category_dates', 'pto_employee_pto_category_dates.adi', 'FK2', 'employeeNumber', '', 2, 1024, '' );
-- cascade: UPDATE, DELETE: cascade
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('pto_categories-pto_cat_dates', 'ptoCategories', 'pto_employee_pto_category_dates', 'FK1', 1, 1, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoEmployees-pto_cat_dates', 'ptoEmployees', 'pto_employee_pto_category_dates', 'FK2', 1, 1, NULL, '', '');
DELETE FROM pto_employee_pto_category_dates;
INSERT INTO pto_employee_pto_category_dates (ptoCategory, employeeNumber, 
  fromDate, thruDate)
SELECT b.ptocategory, a.employeeNumber, 
  cast(timestampadd(sql_tsi_year, b.fromYearsTenure, a.ptoAnniversary) AS sql_date) AS from_date,
  cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, b.thruYearsTenure + 1, a.ptoAnniversary)) AS sql_date) AS thru_date
FROM ptoEmployees a, pto_categories b;

-- every row IN ptoEmployees EXISTS IN pto_employee_pto_category_dates
SELECT *
FROM ptoEmployees a
WHERE NOT EXISTS (
  SELECT 1
  FROM pto_employee_pto_category_dates
  WHERE employeenumber = a.employeenumber);
-- every employeenumber IN pto_employee_pto_category_dates has 5 rows
SELECT employeenumber
FROM pto_employee_pto_category_dates
GROUP BY employeenumber 
HAVING COUNT(*) <> 5;
-- FROM one category to the next, one day apart
SELECT *
FROM pto_employee_pto_category_dates a, 
  pto_employee_pto_category_dates b
WHERE a.employeenumber = b.employeenumber
  AND a.ptoCategory = b.ptoCategory -1
  AND a.thruDate <> cast(timestampadd(sql_tsi_day, -1, b.fromDate) AS sql_date)