Will you please add the role of Systems Engineer to the PTO org structure? 
For the time being, he will report directly to Ben Cahalan. 
If he needs to be associated with a department, he should go to IT for now.

INSERT INTO ptoPositions(position)
values('Systems Engineer');
INSERT INTO ptoDepartmentPositions(department,position,level,ptopolicyhtml)
values('IT','Systems Engineer',4, 'no_policy_html');
INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
  authForPosition,authByLevel,authForLevel)
values('RY1','General Manager','IT','Systems Engineer',3,4);  