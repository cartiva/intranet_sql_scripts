/*
new positions for RY1 Sales: New Car Sales Consultant, Used Car Sales Consultant
  
SELECT *
FROM ptodepartmentpositions
WHERE position = 'sales consultant'



SELECT storecode, employeenumber, fullparttime, left(pydept, 12) distcode, distribution, 
  left(TRIM(firstname) + ' ' + lastname ,35)
FROM dds.edwEmployeeDim
WHERE currentrow = true
  AND active = 'active'
  AND pydeptcode IN ('01','02')
  AND storecode = 'ry1'
  AND distcode NOT IN ('dri','own','own2')
ORDER BY pydept, distcode, firstname

SELECT a.userid, a.firstname, a.lastname, a.email, LEFT(a.supervisorName,20)
FROM pto_compli_users a
WHERE (a.userid = '113550' OR a.supervisorid = '113550')
  AND a.status = '1'
ORDER BY a.firstname


SELECT a.userid, a.firstname, a.lastname, a.email, LEFT(a.supervisorName,20),
  b.*
FROM pto_compli_users a
LEFT JOIN (
SELECT storecode, employeenumber, fullparttime, left(pydept, 12) distcode, distribution, 
  left(TRIM(firstname) + ' ' + lastname ,35)
FROM dds.edwEmployeeDim
WHERE currentrow = true
  AND active = 'active'
  AND pydeptcode IN ('01','02')
  AND storecode = 'ry1'
  AND distcode NOT IN ('dri','own','own2')) b on a.userid = b.employeenumber
WHERE (a.userid = '113550' OR a.supervisorid = '113550')
  AND a.status = '1'

-- als folks IN ptoEmployees 
-- only sam foster missing
-- DROP TABLE #new;
*/
SELECT a.userid, a.firstname, a.lastname, a.title, a.email, 
  LEFT(a.supervisorName,15) AS compliSuper,
  c.username AS tpUser, b.username AS ptoUser,
  CASE WHEN b.employeenumber IS NULL THEN NULL ELSE 'Yes' END AS inPTO,
--  CASE WHEN /*a.email*/c.username = b.username THEN 'Yes' ELSE b.username END AS userAgree,
  CASE WHEN c.employeenumber IS NULL THEN NULL ELSE 'Yes' END AS tpEmp,
  CASE WHEN d.username IS NULL THEN NULL ELSE 'Yes' END AS hasPto,
  left(TRIM(e.department) + ':' + e.position,35) AS ptoPos,
  TRIM(h.firstname) + ' ' + h.lastname AS ptoAdmin
INTO #new  
FROM pto_compli_users a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
LEFT JOIN tpEmployees c on a.userid = c.employeenumber
LEFT JOIN (
  SELECT DISTINCT username
  FROM employeeAppAuthorization
  WHERE appcode = 'pto') d on c.username = d.username
LEFT JOIN ptoPositionFulfillment e on b.employeenumber = e.employeenumber
LEFT JOIN ptoAuthorization f on e.department = f.authForDepartment
  AND e.position = f.authForPosition
LEFT JOIN ptoPositionFulfillment g on f.authByDepartment = g.department
  AND f.authByPosition = g.position
LEFT JOIN dds.edwEmployeeDim h on g.employeenumber = h.employeenumber
  AND h.currentrow = true    
WHERE (a.userid = '113550' OR a.supervisorid = '113550')
  AND a.status = '1'
ORDER BY a.firstname;

-- rjs folks IN ptoEmployees  '140500'
SELECT a.userid, a.firstname, a.lastname, a.title, a.email, 
  LEFT(a.supervisorName,15) AS compliSuper,
  c.username AS tpUser, b.username AS ptoUser,
  CASE WHEN b.employeenumber IS NULL THEN NULL ELSE 'Yes' END AS inPTO,
--  CASE WHEN /*a.email*/c.username = b.username THEN 'Yes' ELSE b.username END AS userAgree,
  CASE WHEN c.employeenumber IS NULL THEN NULL ELSE 'Yes' END AS tpEmp,
  CASE WHEN d.username IS NULL THEN NULL ELSE 'Yes' END AS hasPto,
  left(TRIM(e.department) + ':' + e.position,35) AS ptoPos,
  TRIM(h.firstname) + ' ' + h.lastname AS ptoAdmin
INTO #used  
FROM pto_compli_users a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
LEFT JOIN tpEmployees c on a.userid = c.employeenumber
LEFT JOIN (
  SELECT DISTINCT username
  FROM employeeAppAuthorization
  WHERE appcode = 'pto') d on c.username = d.username
LEFT JOIN ptoPositionFulfillment e on b.employeenumber = e.employeenumber
LEFT JOIN ptoAuthorization f on e.department = f.authForDepartment
  AND e.position = f.authForPosition
LEFT JOIN ptoPositionFulfillment g on f.authByDepartment = g.department
  AND f.authByPosition = g.position
LEFT JOIN dds.edwEmployeeDim h on g.employeenumber = h.employeenumber
  AND h.currentrow = true  
WHERE (a.userid = '140500' OR a.supervisorid = '140500')
  AND a.status = '1'
ORDER BY a.firstname;

/*
-- closure on jimmy, 
SELECT a.*, c.name, b.employeenumber
FROM (
  SELECT descendantDepartment, descendantPosition, depth
  FROM ptoClosure
  WHERE ancestorDepartment = 'RY1 Sales'
    AND ancestorPosition = 'General Sales Manager') a
LEFT JOIN ptoPositionFulfillment b on a.descendantDepartment = b.department
  AND a.descendantPosition = b.position
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true  
LEFT JOIN   
ORDER BY a.depth, a.descendantDepartment, a.descendantPosition  

*/

-- 12/15/14 - first DO the org chart changes v3.3
/*
-- ok, now, DO used first
SELECT * FROM #used
SELECT * FROM pto_employee_pto_allocation WHERE employeenumber IN (SELECT userid FROM #used)
1. change position ptoPositionFulfillment
they ALL already exist IN ptoEmployees AND usernames agree
2. ben dalen already has tpEmployee access FROM his days AS a writer, with a username of
  bdalen@rydellchev, change his username, remove ALL that AND just 
  give him pto
3. INSERT INTO tpEmployees WHERE there IS NOT currently a record  
4. ADD pto to employeeAppAuthorization for each of these guys
-- new
SELECT * FROM #new;
SELECT * FROM pto_employee_pto_allocation WHERE employeenumber IN (SELECT userid FROM #new)
1. change position IN ptoPositionFulfillment
2. al berry already has tpEmployee access FROM his days AS a writer, with a username of
  aberry@rydellchev, change his username, remove ALL the service stuff AND ADD ptomanager
3. INSERT INTO tpEmployees WHERE there IS NOT currently a record  
4. ADD pto to employeeAppAuthorization for each of these guys  


-- can NOT DO these  within the TRANSACTION, RI locks empAppauthorization,
-- so DELETE can NOT happen until after COMMIT of change on tpEmployees
*/
-- 2.
--SELECT * FROM tpEmployees WHERE username LIKE 'bdale%'  
--select * FROM employeeAppAuthorization WHERE username LIKE 'bdale%'
-- SELECT * FROM tpemployees WHERE username LIKE 'bdal%'

UPDATE tpEmployees
SET username = 'bdalen@rydellcars.com'
WHERE username = 'bdalen@rydellchev.com';

DELETE FROM employeeAppAuthorization
WHERE username = 'bdalen@rydellcars.com';


--2. al berry already has tpEmployee access FROM his days AS a writer, with a username of
--  aberry@rydellchev, change his username, remove ALL the service stuff AND ADD ptomanager
--  SELECT * FROM employeeappauthorization WHERE username = 'aberry@rydellchev.com'
UPDATE tpEmployees
SET username = 'aberry@rydellcars.com'
WHERE username = 'aberry@rydellchev.com';
DELETE FROM employeeAppAuthorization
WHERE username = 'aberry@rydellcars.com'
  AND appcode = 'sco';

BEGIN TRANSACTION;
TRY
-- 1. change position ptoPositionFulfillment
TRY
UPDATE ptoPositionFulfillment
SET position = 'Used Car Sales Consultant'
WHERE employeenumber IN (
  SELECT userid
  FROM #used
  WHERE title = 'Used Car Sales Consultant');
CATCH ALL
  RAISE Used(100, __errtext );
END TRY;   
-- 3.
TRY 
INSERT INTO tpEmployees(username,firstname,lastname,employeenumber,password,
  membergroup,storecode,fullname)
SELECT a.ptoUser, b.firstname, b.lastname, a.userid, 
  (SELECT generate_password(length(trim(ptoUser) + TRIM(b.firstname)), 
        cast(trim(a.userid) AS sql_integer)) FROM system.iota) AS password, 
  'Fuck You', b.storecode,
  TRIM(b.firstname) + ' ' + b.lastname
FROM #used a
LEFT JOIN dds.edwEmployeeDim b on a.userid = b.employeenumber
  AND b.currentrow = true 
WHERE a.tpUser IS NULL;
CATCH ALL
  RAISE Used(300, '3');
END TRY;  
-- 4.
TRY 
INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
SELECT b.ptoUser, a.appname,a.appseq,a.appcode,a.approle,a.functionality
FROM ApplicationMetaData a
INNER JOIN #used b on 1 = 1
WHERE a.appcode = 'pto' 
  AND a.approle = 'ptoemployee'
  AND b.title = 'used car sales consultant';
INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
SELECT b.ptoUser, a.appname,a.appseq,a.appcode,a.approle,a.functionality
FROM ApplicationMetaData a
INNER JOIN #used b on 1 = 1
WHERE a.appcode = 'pto' 
  AND a.approle = 'ptomanager'
  AND b.title = 'used car buyer';  
CATCH ALL
  RAISE Used(400, '4');
END TRY;  
-- 1. change position ptoPositionFulfillment
TRY 
UPDATE ptoPositionFulfillment
SET position = 'New Car Sales Consultant'
WHERE employeenumber IN (
  SELECT userid
  FROM #new
  WHERE title = 'New Car Sales Consultant');  
CATCH ALL
  RAISE New(100, __errtext );
END TRY;
-- 3. INSERT INTO tpEmployees WHERE there IS NOT currently a record  
-- SELECT * FROM #new;
TRY 
INSERT INTO tpEmployees(username,firstname,lastname,employeenumber,password,
  membergroup,storecode,fullname)
SELECT a.ptoUser, b.firstname, b.lastname, a.userid, 
  (SELECT generate_password(length(trim(ptoUser) + TRIM(b.firstname)), 
        cast(trim(a.userid) AS sql_integer)) FROM system.iota) AS password, 
  'Fuck You', b.storecode,
  TRIM(b.firstname) + ' ' + b.lastname
FROM #new a
LEFT JOIN dds.edwEmployeeDim b on a.userid = b.employeenumber
  AND b.currentrow = true 
WHERE a.tpUser IS NULL
  AND inPto IS NOT NULL;  -- skip the new kid for now 
CATCH ALL
  RAISE New(200, __errtext );
END TRY;  
-- 4. ADD pto to employeeAppAuthorization for each of these guys    
TRY 
INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
SELECT b.ptoUser, a.appname,a.appseq,a.appcode,a.approle,a.functionality
FROM ApplicationMetaData a
INNER JOIN #new b on 1 = 1
WHERE a.appcode = 'pto' 
  AND a.approle = 'ptoemployee'
  AND b.title IN ('New Car Sales Consultant','New Car Inventory Manager',
    'Finance Manager','New Car Technology Specialist')
  AND b.inPto IS NOT NULL;  -- skip the new kid for now
INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
SELECT b.ptoUser, a.appname,a.appseq,a.appcode,a.approle,a.functionality
FROM ApplicationMetaData a
INNER JOIN #new b on 1 = 1
WHERE a.appcode = 'pto' 
  AND a.approle = 'ptomanager'
  AND b.title = 'New Car Sales Manager';    
CATCH ALL
  RAISE New(300, __errtext );
END TRY;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  


-- ry2, org chart part already done, will see IF it IS complete AND correct
--   looks ok, ry2 sales consultants are NOT separated INTO new AND used

SELECT a.userid, a.firstname, a.lastname, a.title, a.email, 
  LEFT(a.supervisorName,15) AS compliSuper,
  c.username AS tpUser, b.username AS ptoUser,
  CASE WHEN b.employeenumber IS NULL THEN NULL ELSE 'Yes' END AS inPTO,
--  CASE WHEN /*a.email*/c.username = b.username THEN 'Yes' ELSE b.username END AS userAgree,
  CASE WHEN c.employeenumber IS NULL THEN NULL ELSE 'Yes' END AS tpEmp,
  CASE WHEN d.username IS NULL THEN NULL ELSE 'Yes' END AS hasPto,
  left(TRIM(e.department) + ':' + e.position,35) AS ptoPos,
  TRIM(h.firstname) + ' ' + h.lastname AS ptoAdmin
INTO #ry2  
FROM pto_compli_users a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
LEFT JOIN tpEmployees c on a.userid = c.employeenumber
LEFT JOIN (
  SELECT DISTINCT username
  FROM employeeAppAuthorization
  WHERE appcode = 'pto') d on c.username = d.username
LEFT JOIN ptoPositionFulfillment e on b.employeenumber = e.employeenumber
LEFT JOIN ptoAuthorization f on e.department = f.authForDepartment
  AND e.position = f.authForPosition
LEFT JOIN ptoPositionFulfillment g on f.authByDepartment = g.department
  AND f.authByPosition = g.position
LEFT JOIN dds.edwEmployeeDim h on g.employeenumber = h.employeenumber
  AND h.currentrow = true  
WHERE a.locationid = 'ry2'
  AND (a.title IN ('sales consultant') OR userid IN ('2126300','2106225'))
  AND a.status = '1'
  ORDER BY a.firstname;
  
/*
looks LIKE just need tpEmployees AND empAppAuth
again, leave out newbies (for new) ie those NOT yet IN ptoEmployees

select * FROM #ry2;
*/

BEGIN TRANSACTION;
TRY
INSERT INTO tpEmployees(username,firstname,lastname,employeenumber,password,
  membergroup,storecode,fullname)
SELECT a.ptoUser, b.firstname, b.lastname, a.userid, 
  (SELECT generate_password(length(trim(ptoUser) + TRIM(b.firstname)), 
        cast(trim(a.userid) AS sql_integer)) FROM system.iota) AS password, 
  'Fuck You', b.storecode,
  TRIM(b.firstname) + ' ' + b.lastname
FROM #ry2 a
LEFT JOIN dds.edwEmployeeDim b on a.userid = b.employeenumber
  AND b.currentrow = true 
WHERE a.tpUser IS NULL
  AND inPto IS NOT NULL;  -- skip the new kid for now 
  
INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
SELECT b.ptoUser, a.appname,a.appseq,a.appcode,a.approle,a.functionality
FROM ApplicationMetaData a
INNER JOIN #ry2 b on 1 = 1
WHERE a.appcode = 'pto' 
  AND a.approle = 'ptoemployee'
  AND b.title IN ('Sales Consultant','Sales Manager')
  AND b.inPto IS NOT NULL;  -- skip the new kid for now  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    


-- pto used
BEGIN TRANSACTION;
TRY
insert into pto_used values('110052','01/31/2014','Sales Dept',40);
insert into pto_used values('110052','03/31/2014','Sales Dept',40);
insert into pto_used values('123425','09/30/2014','Sales Dept',24);
insert into pto_used values('128530','03/31/2014','Sales Dept',80);
insert into pto_used values('145840','02/28/2014','Sales Dept',64);
insert into pto_used values('145840','05/31/2014','Sales Dept',16);
insert into pto_used values('150045','07/31/2014','Sales Dept',40);
insert into pto_used values('150045','08/31/2014','Sales Dept',8);
insert into pto_used values('161325','01/31/2014','Sales Dept',40);
insert into pto_used values('161325','02/28/2014','Sales Dept',80);
insert into pto_used values('168800','02/28/2014','Sales Dept',24);
insert into pto_used values('174150','05/31/2014','Sales Dept',40);
insert into pto_used values('181665','01/31/2014','Sales Dept',40);
insert into pto_used values('181665','05/31/2014','Sales Dept',40);
insert into pto_used values('181665','10/31/2014','Sales Dept',16);
insert into pto_used values('189100','04/30/2014','Sales Dept',16);
insert into pto_used values('1109851','10/31/2014','Sales Dept',40);
insert into pto_used values('1109815','02/28/2014','Sales Dept',48);
insert into pto_used values('1109815','06/30/2014','Sales Dept',16);
insert into pto_used values('1109815','08/31/2014','Sales Dept',8);
insert into pto_used values('1109815','09/30/2014','Sales Dept',16);
insert into pto_used values('1135518','03/31/2014','Sales Dept',56);
insert into pto_used values('1135518','07/31/2014','Sales Dept',56);
insert into pto_used values('1147250','01/31/2014','Sales Dept',40);
insert into pto_used values('1147250','05/31/2014','Sales Dept',16);
insert into pto_used values('1147250','07/31/2014','Sales Dept',24);
insert into pto_used values('1147250','08/31/2014','Sales Dept',24);
insert into pto_used values('1147250','09/30/2014','Sales Dept',32);
insert into pto_used values('1147250','10/31/2014','Sales Dept',24);
insert into pto_used values('1147810','03/31/2014','Sales Dept',40);
-- violates pk
--insert into pto_used values('1147810','10/31/2014','Sales Dept',16);
--insert into pto_used values('1147810','10/31/2014','Sales Dept',24);
-- replace with single entry
insert into pto_used values('1147810','10/31/2014','Sales Dept',40);
insert into pto_used values('212120','07/31/2014','Sales Dept',32);
insert into pto_used values('285820','10/31/2014','Sales Dept',32);
insert into pto_used values('2106225','07/31/2014','Sales Dept',40);
insert into pto_used values('2126300','08/31/2014','Sales Dept',32);
insert into pto_used values('2126300','10/31/2014','Sales Dept',48);
insert into pto_used values('2130690','02/28/2014','Sales Dept',40);
insert into pto_used values('2130690','07/31/2014','Sales Dept',40);
insert into pto_used values('242460','09/30/2014','Sales Dept',8);
insert into pto_used values('2150210','08/31/2014','Sales Dept',40);
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  


-- newbies
-- everything looks ok, DO these manually, one at a time AND check the results
-- AS i go
SELECT c.firstname, c.lastname, b.userid, b.title, c.hiredate, d.dateofhire
FROM (
  SELECT userid, email, title
    FROM (
    SELECT * FROM #ry2
    UNION
    SELECT * FROM #used
    UNION
    SELECT * FROM #new) a
  WHERE inpto IS NULL) b
LEFT JOIN dds.edwEmployeeDim c on b.userid = c.employeenumber
  AND c.currentrow = true  
LEFT JOIN pto_compli_users d on b.userid = d.userid;

EXECUTE PROCEDURE pto_insert_new_employee('148080','RY1 Sales','New Car Sales Consultant',0,'11/17/2014');

EXECUTE PROCEDURE pto_insert_new_employee('2112451','RY2 Sales','Sales Consultant',0,'10/06/2014');  

EXECUTE PROCEDURE pto_insert_new_employee('2141225','RY2 Sales','Sales Consultant',0,'10/08/2014');
  
EXECUTE PROCEDURE pto_insert_new_employee('283521','RY2 Sales','Sales Consultant',0,'10/06/2014');    


list of usernames AND passwords for ben

SELECT userid, b.fullname, b.username, b.password, a.ptoadmin
FROM (
  SELECT * FROM #ry2
  UNION
  SELECT * FROM #used
  UNION
  SELECT * FROM #new) a
LEFT JOIN tpemployees b on a.userid = b.employeenumber

-- 2/10/15
/*
inserts generated FROM ben f's spreadsheet sales consultant pto_cut 2-10-15
*/
insert into pto_used values('110052','01/31/2015','Sales Dept',40);
insert into pto_used values('123425','11/30/2014','Sales Dept',32);
insert into pto_used values('123425','12/31/2014','Sales Dept',24);
insert into pto_used values('128530','01/31/2015','Sales Dept',56);
insert into pto_used values('145840','01/31/2015','Sales Dept',152);
insert into pto_used values('148110','12/31/2014','Sales Dept',8);
insert into pto_used values('150045','12/31/2014','Sales Dept',32);
insert into pto_used values('150045','01/31/2015','Sales Dept',32);
insert into pto_used values('161325','01/31/2015','Sales Dept',72);
insert into pto_used values('181665','11/30/2014','Sales Dept',24);
insert into pto_used values('181665','01/31/2015','Sales Dept',40);
insert into pto_used values('189100','11/30/2014','Sales Dept',40);
insert into pto_used values('1109815','11/30/2014','Sales Dept',32);
insert into pto_used values('1109815','12/31/2014','Sales Dept',40);
insert into pto_used values('1147250','01/31/2015','Sales Dept',32);
insert into pto_used values('222300','11/30/2014','Sales Dept',80);
insert into pto_used values('285820','11/30/2014','Sales Dept',24);
insert into pto_used values('2102196','10/31/2014','Sales Dept',20);
insert into pto_used values('2102196','12/31/2014','Sales Dept',40);
insert into pto_used values('2106225','01/31/2015','Sales Dept',32);
insert into pto_used values('2150210','11/30/2014','Sales Dept',40);


  