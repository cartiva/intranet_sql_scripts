the whole exercise IS about synchronizing tpEmployees.username AND ptoEmployees.username
such that the credentials for compli AND vision are the same

-- exist IN both tp AND pto
SELECT a.username AS tpUser, b.username AS ptoUser, a.employeenumber
-- SELECT *
FROM tpEmployees a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
WHERE a.username <> b.username

SELECT *
FROM tpemployees a
WHERE NOT EXISTS (
  SELECT 1
  FROM ptoemployees
  WHERE employeenumber = a.employeenumber)
  
SELECT *
FROM ptoEmployees a
WHERE NOT EXISTS (
  SELECT 1
  FROM tpEmployees
  WHERE employeenumber = a.employeenumber)  
  
-- exist IN tp AND have auth to anything
SELECT a.employeenumber, a.username, COUNT(*)
FROM tpEmployees a
INNER JOIN employeeAppAuthorization b on a.username = b.username
GROUP BY a.employeenumber, a.username

-- exist IN tp AND have no auth
SELECT *
FROM tpEmployees c
LEFT JOIN (
  SELECT a.employeenumber, a.username, COUNT(*)
  FROM tpEmployees a
  INNER JOIN employeeAppAuthorization b on a.username = b.username
  GROUP BY a.employeenumber, a.username) d on c.username = d.username
WHERE d.username IS NULL 

-- exist IN pto AND have auth to anything
SELECT a.employeenumber, a.username, COUNT(*) 
FROM ptoEmployees a
INNER JOIN employeeAppAuthorization b on a.username = b.username
GROUP BY a.employeenumber, a.username

-- exist IN pto AND have no auth
select *
FROM ptoEmployees c
LEFT JOIN (
  SELECT a.employeenumber, a.username, COUNT(*) 
  FROM ptoEmployees a
  INNER JOIN employeeAppAuthorization b on a.username = b.username
  GROUP BY a.employeenumber, a.username) d on c.username = d.username
WHERE d.username IS NULL 