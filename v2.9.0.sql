-- goodbye pdq

DELETE 
FROM employeeAppAuthorization
WHERE appname = 'pdq'; 
/*
-- main shop production
-- DROP TABLE #flag;
SELECT thedate, techkey, coalesce(flaghours, 0) + coalesce(ptlhrs, 0) AS flagHours, shopTime
INTO #flag 
FROM (
  SELECT b.storecode, a.thedate, c.techkey, c.technumber, 
    round(SUM(CASE WHEN b.opcodekey NOT IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS flagHours,
    round(SUM(CASE WHEN b.opcodekey IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS shopTime  
  FROM dds.day a
  LEFT JOIN dds.factRepairOrder b on a.datekey = b.flagdatekey
  LEFT JOIN dds.dimTech c on b.techKey = c.techkey
  WHERE thedate BETWEEN '01/01/2013' AND curdate() 
    AND c.techkey IS NOT NULL 
  GROUP BY  a.thedate, b.storecode, c.technumber, c.techkey, c.technumber) m
LEFT JOIN (
  SELECT ptco#, ptdate, pttech, SUM(ptlhrs) AS ptlhrs
  FROM dds.stgArkonaSDPXTIM
  GROUP BY ptco#, ptdate, pttech) n on m.thedate = n.ptdate
    AND m.storecode = n.ptco#
    AND m.technumber = n.pttech;

-- clockhours
-- DROP TABLE #clock;
SELECT b.thedate, employeekey, SUM(clockHours) AS clockHours
INTO #clock
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
WHERE thedate BETWEEN '01/01/2013' AND curdate() -1
  AND a.employeekey IN (
    SELECT distinct employeekey
    FROM tmpDeptTechCensus)
GROUP BY thedate, employeekey;    

-- date info
-- DROP TABLE #day;
SELECT thedate, dayOfWeek, monthOfYear, monthName, weekDay, weekend, holiday,
  sundayToSaturdayWeek, yearmonth,
  (SELECT MIN(thedate) FROM dds.day WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek) AS weekStartDate,
  (SELECT MAX(thedate) FROM dds.day WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek) AS weekEndDate,
  (SELECT MIN(thedate) FROM dds.day WHERE yearmonth = a.yearmonth) AS monthStart,
  (SELECT MAX(thedate) FROM dds.day WHERE yearmonth = a.yearmonth) AS monthEnd,
  SundayToSaturdayWeekSelectFormat
INTO #day  
FROM dds.day a
WHERE datetype = 'date'
  AND thedate BETWEEN '01/01/2013' AND curdate() - 1;
  
-- let's narrow that down to non repeating (and relevant) columns    
-- DROP TABLE #ben;
SELECT a.thedate, a.dayofweek, a.monthofyear, a.monthname, a.holiday, 
  a.sundaytosaturdayweek, a.yearmonth, a.weekstartdate, a.weekenddate,
  b.storecode, b.techkey, b.employeekey, b.employeenumber, 
--  b.expr AS descName,
  b.description, 
  b.techNumber, b.flagDeptCode, b.firstname, b.lastname, 
--   coalesce(b.termDate, CAST('12/31/9999' AS sql_date)) AS termdate,
  b.team, coalesce(c.flaghours, 0) AS flagHours, 
  coalesce(c.shopTime, 0) AS shopTime,
  coalesce(d.clockhours, 0) AS clockhours, 
  SundayToSaturdayWeekSelectFormat
INTO #ben  
FROM #day a
-- LEFT JOIN #census b on a.thedate = b.thedate  
LEFT JOIN tmpDeptTechCensus b on a.thedate = b.thedate
LEFT JOIN #flag c on a.thedate = c.thedate
  AND b.techkey = c.techkey
LEFT JOIN #clock d on a.thedate = d.thedate
  AND b.employeekey = d.employeekey ; 

DROP TABLE tmpBen;
CREATE TABLE tmpBen ( 
      theDate Date constraint NOT NULL,
      dayOfWeek Integer constraint NOT NULL,
      monthOfYear Integer constraint NOT NULL,
      monthName CIChar( 12 ) constraint NOT NULL,
      holiday Logical constraint NOT NULL,
      sundayToSaturdayWeek Integer constraint NOT NULL,
      yearMonth Integer constraint NOT NULL,
      weekStartDate Date constraint NOT NULL,
      weekEndDate Date constraint NOT NULL,
      storecode CIChar( 3 ) constraint NOT NULL,
      techKey Integer constraint NOT NULL,
      employeeKey Integer,
      employeeNumber CIChar( 7 ) constraint NOT NULL,
      descName CIChar( 25 ) constraint NOT NULL,
      techNumber CIChar( 3 ) constraint NOT NULL,
      flagDeptCode CIChar( 2 ) constraint NOT NULL,
      firstname CIChar( 25 ),
      lastname CIChar( 25 ),
--      termDate Date constraint NOT NULL,
      team CIChar( 25 ) default '0' constraint NOT NULL,
      flagHours Double( 15 ) default '0' constraint NOT NULL,
      shopTime double (15) default '0' constraint NOT NULL,
      clockHours Double( 15 ),
      SundayToSaturdayWeekSelectFormat cichar(100),
      constraint PK primary key (thedate, storecode, technumber))  IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpBen','tmpBen.adi','theDate', 'theDate','',2,512,'');  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpBen','tmpBen.adi','sundayToSaturdayWeek', 'sundayToSaturdayWeek','',2,512,''); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpBen','tmpBen.adi','yearMonth', 'yearMonth','',2,512,'');      
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpBen','tmpBen.adi','team', 'team','',2,512,'');  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpBen','tmpBen.adi','storeCode', 'storeCode','',2,512,'');               

--DELETE FROM tmpBen;   
INSERT INTO tmpBen
select * FROM #ben;   
*/

ALTER PROCEDURE benGetProductionAtAGlanceWeeks (
  weekStartDate date output,
  weekEndDate date output,
  flagHours double output,
  clockHours double output,
  target integer output,
  label cichar (15) output)  
BEGIN 
/*
EXECUTE PROCEDURE benGetProductionAtAGlanceWeeks();
*/
INSERT INTO __output  
SELECT top 12 a.*, 
  (SELECT COUNT(*) * 200
    FROM dds.day
    WHERE dayofweek BETWEEN 2 AND 6
      AND holiday = false 
      AND thedate BETWEEN a.weekStartDate AND a.weekEndDate) AS target,
  (SELECT trim(mmdd) + ' to ' + 
    (select trim(mmdd) 
      from dds.day 
      where thedate = a.weekEndDate) 
    FROM dds.day 
    WHERE thedate = a.weekStartDate) AS label    
FROM (
  SELECT weekStartDate, weekEndDate, coalesce(SUM(flaghours), 0) AS flagHours,
    coalesce(SUM(clockHours), 0) AS clockHours
  FROM tmpBen
  WHERE sundayToSaturdayWeek BETWEEN (
    SELECT DISTINCT sundayToSaturdayWeek - 11
    FROM tmpBen
    WHERE thedate = curdate() - 1) 
    AND (
    SELECT DISTINCT sundayToSaturdayWeek
    FROM tmpBen
    WHERE thedate = curdate() - 1)
  AND storecode = 'ry1'  
  AND flagdeptCode = 'mr'  
  GROUP BY weekStartDate, weekEndDate) a
ORDER BY weekStartDate;
END;    

alter PROCEDURE benGetProductionAtAGlanceMonths (
  yearMonth integer output,
  flagHours double output,
  clockHours double output,
  target integer output,
--  label cichar (15) output) 
  label date output) 
BEGIN 
/*
7/15 libby wants the x axis label returned AS a date so she can format it
       with moment.js
	   
EXECUTE PROCEDURE benGetProductionAtAGlanceMonths();
*/
INSERT INTO __output  
SELECT top 12 b.*
FROM (
  SELECT top 12 a.*, 
    (SELECT COUNT(*) * 200
      FROM dds.day
      WHERE dayofweek BETWEEN 2 AND 6
        AND holiday = false 
        AND yearmonth = a.yearmonth) AS target,
--    (SELECT distinct yearMonthShort 
--      FROM dds.day
--      WHERE yearMonth = a.yearMonth) AS label      
    (SELECT thedate
	  FROM dds.day
	  WHERE yearMonth = a.yearMonth
	    AND firstDayOfMonth = true)   
  FROM (      
    SELECT yearmonth, coalesce(SUM(flaghours), 0) AS flagHours,
      coalesce(SUM(clockHours), 0) AS clockHours
    FROM tmpBen
    WHERE yearmonth BETWEEN (
      SELECT DISTINCT yearmonth
      FROM tmpBen
      WHERE thedate = curdate() - 365) AND (
      SELECT DISTINCT yearmonth
      FROM tmpBen
      WHERE thedate = curdate() - 1)
    AND storecode = 'ry1'  
    AND flagdeptCode = 'mr'  
    GROUP BY yearmonth) a
  ORDER BY yearMonth DESC) b
ORDER BY yearmonth;  
END;   

alter PROCEDURE benGetProductionAtAGlancePace (
  flagHoursToDate double output,
  clockHoursToDate double output,
  workingDays integer output,
  elapsedWorkingDays integer output,
  flagPace double output,
  clockPace double output,
  proficiencyToDate double output,
  proficiencyPace double output)  
BEGIN 
/*

EXECUTE PROCEDURE benGetProductionAtAGlancePace()) a;
*/   
-- pacing
DECLARE @workingDaysInMonth integer;
DECLARE @elapsedWorkingDaysInMonth integer;
@workingDaysInMonth = (
  SELECT COUNT(*) -- working days IN month
  FROM dds.day
  WHERE dayOfWeek BETWEEN 2 AND 6 
    AND holiday = false
    AND yearmonth = (
      SELECT yearmonth
      FROM dds.day
      WHERE thedate = curdate() - 1)); 
      
@elapsedWorkingDaysInMonth = (    
  SELECT COUNT(*) 
  FROM dds.day
  WHERE dayOfWeek BETWEEN 2 AND 6 
    AND holiday = false
    AND thedate < curdate() - 1
    AND yearmonth = (
      SELECT yearmonth
      FROM dds.day
      WHERE thedate = curdate() - 1));   
INSERT INTO __output    
SELECT x.*, round(flaghoursToDate/clockHoursToDate, 2),
  round(flagPace/clockPace, 2) 
FROM (     
  SELECT SUM(flagHours) AS flagHoursToDate, SUM(clockHours) clockHoursToDate,
    @workingDaysInMonth AS workingDays, @elapsedWorkingDaysInMonth AS elapsedWorkingDays,
    round(SUM(flagHours) * @workingDaysInMonth/@elapsedWorkingDaysInMonth, 2) AS flagPace,
    round(SUM(clockHours) * @workingDaysInMonth/@elapsedWorkingDaysInMonth, 2) AS clockPace
  FROM tmpBen
  WHERE theDate < curdate()
    AND storecode = 'ry1'
    AND flagDeptCode = 'mr'
    AND yearmonth = (
      SELECT yearmonth
      FROM dds.day
      WHERE thedate = curdate() - 1)) x; 
END;    


alter PROCEDURE benGetProductionDetailsWeek(
  theDate date,
  typeInfo cichar(5) output,
  team cichar(25) output,
  tech cichar(51) output,
  flagHours double output,
  shopTime double output,
  clockHours double output,
  proficiency double output,
  currentlyEmployed cichar(3) output,
  dateHeader cichar(100) output)   
BEGIN
/*
SELECT typeInfo, LEFT(team, 12) AS team, LEFT(tech, 20) AS tech,
  flaghours, clockhours, proficiency
FROM (  
EXECUTE PROCEDURE benGetProductionDetailsWeek( '07/16/2014')) a;
*/
DECLARE @sundayToSaturdayWeek integer;
DECLARE @endDate date;
@sundayToSaturdayWeek = (
  SELECT DISTINCT sundayToSaturdayWeek
  from tmpBen
  where theDate = (
    select theDate
    from __input));
@endDate = (
  SELECT MAX(thedate)
  FROM tmpBen
  WHERE sundayToSaturdayWeek = @sundayToSaturdayWeek);     
INSERT INTO __output
SELECT top 500 x.*, y.SundayToSaturdayWeekSelectFormat
FROM (
  SELECT 'Team' as typeInfo, team, 
    left(cast(null as sql_char), 51) as tech, SUM(flagHours) AS flagHours,
    SUM(shopTime) AS shopTime, 
    SUM(clockHours) AS clockHours, 
    CASE SUM(clockHours)
      WHEN 0 THEN 0
      ELSE round(SUM(flagHours + shopTime)/SUM(clockHours), 2) 
    END AS prof, '' AS currentlyEmployed
  FROM tmpBen
  WHERE storecode = 'ry1'
    AND flagdeptcode = 'mr' 
    AND sundaytosaturdayweek = @sundayToSaturdayWeek
  GROUP BY weekStartDate, weekEndDate, team
  -- tech details
  UNION 
  SELECT 'Tech' AS typeInfo, team, tech, flagHours, shopTime, clockHours, prof,
    coalesce(currentlyEmployed, 'Yes')
  FROM (   
    SELECT employeenumber, team, 
      coalesce(TRIM(firstname) + ' ' + lastname, descName) AS tech, 
      SUM(flagHours) AS flagHours,
      SUM(shopTime) AS shopTime, 
      SUM(clockHours) AS clockHours, 
      CASE SUM(clockHours)
        WHEN 0 THEN 0
        ELSE round(SUM(flagHours + shopTime)/SUM(clockHours), 2) 
      END AS prof 
    FROM tmpBen
    WHERE storecode = 'ry1'
      AND flagdeptcode = 'mr' 
      AND sundaytosaturdayweek = @sundayToSaturdayWeek
    GROUP BY employeenumber, weekStartDate, weekEndDate, team, 
      coalesce(TRIM(firstname) + ' ' + lastname, descName)) m
  LEFT JOIN ( -- currently employed
    SELECT a.storecode, a.employeenumber, b.termdate, c.flagDeptCode,
      CASE 
        WHEN b.termdate < curdate() THEN 'No'
        ELSE
          CASE 
            WHEN a.flagDeptCode <> c.flagDeptCode THEN 'No' 
            ELSE 'Yes'
          END
      END AS currentlyEmployed      
    FROM tmpBen a
    LEFT JOIN dds.edwEmployeeDim b on a.storecode = b.storecode
      and a.employeeNumber = b.employeenumber
      AND b.currentrow = true
    LEFT JOIN dds.dimTech c on a.storecode = c.storecode
      AND a.employeenumber = c.employeenumber
      AND c.currentrow = true  
    WHERE a.thedate = @endDate
      AND a.storecode = 'ry1'
      AND a.employeenumber <> 'na'
      AND a.flagDeptCode = 'mr') n on m.employeenumber = n.employeenumber) x      
LEFT JOIN (
  SELECT DISTINCT SundayToSaturdayWeekSelectFormat
  from tmpBen
  WHERE sundayToSaturdayWeek = @sundayToSaturdayWeek) y on 1 = 1   
ORDER BY team, tech;
END;


ALTER PROCEDURE benGetProductionDetailsMonth(
  yearMonth integer,
  typeInfo cichar(5) output,
  team cichar(25) output,
  tech cichar(51) output,
  flagHours double output,
  shopTime double output,
  clockHours double output,
  proficiency double output,
  currentlyEmployed cichar(3) output,
  dateHeader cichar(24) output)   
BEGIN
/*
SELECT typeInfo, LEFT(team, 15) AS team, LEFT(tech, 25) AS tech,
  flaghours, clockhours, proficiency
FROM (  
EXECUTE PROCEDURE benGetProductionDetailsMonth(201407)) a;
*/
DECLARE @yearMonth integer;
DECLARE @endDate date;
@yearMonth = (SELECT yearMonth FROM __input);
@endDate = (
  SELECT MAX(theDate)
  FROM tmpBen
  WHERE yearMonth = @yearMonth);
INSERT INTO __output
SELECT top 500 x.*, y.dateHeader
FROM (
  SELECT 'Team' as typeInfo, team, 
    left(cast(null as sql_char), 51) as tech, 
    SUM(flagHours) AS flagHours, SUM(shopTime) AS shopTime, 
    SUM(clockHours) AS clockHours, 
    CASE SUM(clockHours)
      WHEN 0 THEN 0
      ELSE round(SUM(flagHours + shopTime)/SUM(clockHours), 2) 
    END AS prof, '' AS currentlyEmployed
  FROM tmpBen
  WHERE storecode = 'ry1'
    AND flagdeptcode = 'mr' 
    AND yearmonth = @yearMonth
  GROUP BY yearmonth, team
  -- tech details
  UNION 
  SELECT 'Tech' AS typeInfo, team, tech, flagHours, shopTime, clockHours, prof,
    coalesce(currentlyEmployed, 'Yes')
  FROM (    
    SELECT employeenumber, team, 
      coalesce(TRIM(firstname) + ' ' + lastname, descName) AS tech, 
      SUM(flagHours) AS flagHours, SUM(shopTime) AS shopTime,
      SUM(clockHours) AS clockHours, 
      CASE SUM(clockHours)
        WHEN 0 THEN 0
        ELSE round(SUM(flagHours + shopTime)/SUM(clockHours), 2) 
      END AS prof 
    FROM tmpBen
    WHERE storecode = 'ry1'
      AND flagdeptcode = 'mr' 
      AND yearmonth = @yearMonth
    GROUP BY employeenumber, yearmonth, team, 
      coalesce(TRIM(firstname) + ' ' + lastname, descName)) m
  LEFT JOIN ( -- currently employed
    SELECT a.storecode, a.employeenumber, b.termdate, c.flagDeptCode,
      CASE 
        WHEN b.termdate < curdate() THEN 'No'
        ELSE
          CASE 
            WHEN a.flagDeptCode <> c.flagDeptCode THEN 'No' 
            ELSE 'Yes'
          END
      END AS currentlyEmployed      
    FROM tmpBen a
    LEFT JOIN dds.edwEmployeeDim b on a.storecode = b.storecode
      and a.employeeNumber = b.employeenumber
      AND b.currentrow = true
    LEFT JOIN dds.dimTech c on a.storecode = c.storecode
      AND a.employeenumber = c.employeenumber
      AND c.currentrow = true  
    WHERE a.thedate = @endDate
      AND a.storecode = 'ry1'
      AND a.employeenumber <> 'na'
      AND a.flagDeptCode = 'mr') n on m.employeenumber = n.employeenumber ) x
LEFT JOIN (
  SELECT distinct trim(monthName) + ' ' + CAST(year(thedate) AS sql_Char) AS dateHeader
  FROM tmpBen
  WHERE yearMonth = @yearMonth) y on 1 = 1 
ORDER BY team, tech;
END;

-- Main shop production -------------------------------------------------------
INSERT INTO applicationMetaData values (
  'teampay', -- appName
  'tp', -- appCode,
  100,-- appSeq
  'manager', -- appRole
  'production summary', -- functionality
  'sublink_mainshop_production_summary_html',-- configMapMarkupKey
  'stp.manager.summary',-- pubSub
  'leftnavbar',-- navType
  'production summary',-- navText
  100-- navSeq
  ); 
  
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, 18 
FROM tpEmployees a, applicationMetaData b
WHERE (
  a.username = 'mhuot@rydellchev.com' OR
  a.username = 'rsattler@rydellchev.com' or
  firstname = 'trent' OR
  firstname = 'jeri' OR
  (firstname = 'benjamin' AND lastname <> 'dalen') OR
  username = 'brian@rydellchev.com' OR
  firstname = 'andrew' or
  lastname = 'steinke' OR
  lastname = 'espelund' OR
  lastname = 'sorum' OR
  lastname = 'longoria')
  AND b.appcode = 'tp'
  AND b.approle = 'manager' 
  AND b.functionality = 'production summary'
  AND b.navSeq = 100;  
  
-- body shop estimators -------------------------------------------------------
INSERT INTO applicationRoles (appName, role, appCode)
values ('teampay','estimator','tp');

INSERT INTO applicationMetaData values (
  'teampay', -- appName
  'tp', -- appCode,
  101,-- appSeq
  'estimator', -- appRole
  'dropdown', -- functionality
  'mainlink_bodyshop_est_estimator_html',-- configMapMarkupKey
  '',-- pubSub
  'leftnavbar',-- navType
  'estimator (app header)',-- navText
  10-- navSeq
  );
  
INSERT INTO applicationMetaData values (
  'teampay', -- appName
  'tp', -- appCode,
  101,-- appSeq
  'estimator', -- appRole
  'estimator summary', -- functionality
  'sublink_bodyshop_est_estimator_summary_html',-- configMapMarkupKey
  'btp.estimator.summary',-- pubSub
  'leftnavbar',-- navType
  'estimator summary',-- navText
  20-- navSeq
  );  
  
INSERT INTO applicationMetaData values (
  'teampay', -- appName
  'tp', -- appCode,
  101,-- appSeq
  'manager', -- appRole
  'estimator summary', -- functionality
  'sublink_bodyshop_est_mgr_summary_html',-- configMapMarkupKey
  'btp.manager.summary',-- pubSub
  'leftnavbar',-- navType
  'estimator summary',-- navText
  85-- navSeq
  );     
  
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, 13
FROM tpEmployees a, applicationMetaData b
WHERE (
  a.username = 'rsattler@rydellchev.com' OR
  a.username = 'mhuot@rydellchev.com' or
  firstname = 'trent' OR
  firstname = 'jeri' OR
  (firstname = 'benjamin' AND lastname <> 'dalen') OR
  username = 'brian@rydellchev.com' OR
  firstname = 'andrew' or
  lastname = 'steinke' OR
  lastname = 'espelund' OR
  lastname = 'sorum')
  AND b.appcode = 'tp'
  AND b.approle = 'manager' 
  AND b.functionality = 'estimator summary'
  AND b.navSeq = 85;       