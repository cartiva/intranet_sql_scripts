DECLARE @username string;
DECLARE @appcode string;
DECLARE @approle string;
-- SELECT * FROM tpemployees WHERE lastname like 'old%'
@username = 'rerickson@rydellcars.com';------------------------------------------
/*
customer feedback:
    @appcode = 'cstfb';
    @approle = 'all';
crayon report:   
    @appcode = 'inv';
    @approle = 'usedManager'; 
sco writer:   
    @appcode = 'sco';
    @approle = 'writer';     
*/
@appcode = 'inv';------------------------------------------------------------
@approle = 'usedManager';--------------------------------------------------------------

IF (
  SELECT COUNT(*)
  FROM tpEmployees
  WHERE username =  @username) <> 1 THEN 
  RAISE username (100, 'Invalid username');
END IF; 
INSERT INTO employeeAppAuthorization (username,appname,appseq,appcode,approle,
  functionality)
SELECT @username, a.appName, a.appseq, a.appCode, a.appRole, 
  a.functionality
FROM applicationMetaData a
WHERE appcode = @appcode
  AND approle = @approle;