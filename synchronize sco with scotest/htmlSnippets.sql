snippets get added to scotest during dev, those changes need to be persisted to sco

-- for now, at least, this IS the natural key
SELECT storecode, membergroup, app, appcode, page, coalesce(description, 'x')
FROM htmlsnippets
GROUP BY storecode, membergroup, app, appcode, page, coalesce(description, 'x')
HAVING COUNT(*) > 1

-- snippets are different BETWEEN sco AND scotest
SELECT a.*
FROM scotest.htmlsnippets a
INNER JOIN htmlsnippets b on a.storecode = b.storecode
  AND a.membergroup = b.membergroup
  AND a.app = b.app
  AND a.appcode = b.appcode
  AND a.page = b.page
  AND coalesce(a.description, 'x') = coalesce(b.description, 'x')
  AND a.snippet <> b.snippet

  