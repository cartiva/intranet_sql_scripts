-- fb CASE 340
-- ADD do_not_call attribute to dimCustomer

ALTER TABLE dimCustomer
ADD COLUMN do_not_call logical not null default 'False';

UPDATE dimCustomer
SET do_not_call = false 

SELECT *
FROM stgArkonaBOPNAME
WHERE BNACBP = 'N'

SELECT bnco#, bnkey FROM stgArkonaBOPNAME group BY bnco#, bnkey HAVING COUNT(*) > 1

SELECT * FROM dimcustomer

UPDATE dimCustomer
SET do_not_call = True
FROM (
  SELECT bnco#, bnkey
  FROM stgArkonaBOPNAME
  WHERE BNACBP = 'N') x
WHERE dimCustomer.storecode = x.bnco#
  AND dimCustomer.bnkey = x.bnkey
  
SELECT * FROM dimcustomer WHERE do_not_call = true ORDER BY fullname
SELECT do_not_call, COUNT(*) FROM dimcustomer GROUP BY do_not_call

 
ALTER TABLE xfmDimCustomer
ADD COLUMN do_not_call logical not null default 'False';

UPDATE xfmDimCustomer
SET do_not_call = false;

select * FROM xfmDimCustomer

SELECT b.do_not_call, a.* 
FROM scotest.warrantyros a
LEFT JOIN dimcustomer b on a.customer = b.fullname

SELECT *
FROM dimcustomer
WHERE do_not_call = true



SELECT * 
FROM scotest.warrantyros
WHERE followupcomplete = false

SELECT * FROM dimcustomer WHERE fullname = 'MORTENSON, CHRISTOPHER'