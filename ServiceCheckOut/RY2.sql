/*
SELECT DISTINCT name, dl1,dl2,dl3,dl4,dl5,dl6,dl7,dl8
FROM dds.zcb
WHERE name = 'cb2'
  AND dl2 <> 'ry3'
  AND dl3 = 'fixed'
*/
5/31/13
created scoJonDev AND ads for development (so i have an active link to dds data)
copied FROM sco 5.31.13
/*
this gets done IN the overall sp overhal
1.
CREATE PROCEDURE GetWritersForManager
   ( 
      managerEEUserName CICHAR ( 50 ),
      writerName CICHAR ( 60 ) OUTPUT,
      writerEEUserName CICHAR ( 50 ) OUTPUT
   ) 
BEGIN 

INSERT INTO __output
SELECT top 20 trim(FirstName) + ' ' + TRIM(lastName), eeusername
FROM Servicewriters
ORDER BY firstname;


END;
*/
2. ADD ry2 metrics (ServiceWriterMetrics)
ry2 metrics NOT needed:
  seq   metric
   1    Cashiering
   3    Open Warranty Calls
        rosCashieredByWriter
        rosCashiered
  
INSERT INTO servicewritermetrics
SELECT 'RY2', metric, seq, additive, cumulative, datatype, def
FROM servicewritermetrics
WHERE metric NOT IN ('Cashiering','Open Warranty Calls','rosCashieredByWriter','rosCashiered');
--3. ADD ry2 writers
-- DELETE FROM servicewriters WHERE storecode = 'ry2';
INSERT INTO servicewriters values (
  'RY2', 'Joel Dangerfield', 'Dangerfield', 'Joel','658','RYDEJOELD','23061',true,0, 'jdangerfield@gfhonda.com');
INSERT INTO servicewriters values (
  'RY2', 'Dave Pederson', 'Pederson', 'Dave','623','RYDEDAVEPE','2106410',true,0, 'dpederson@gfhonda.com');  
INSERT INTO servicewriters values (
  'RY2', 'Andrew Neumann', 'Neumann', 'Andrew','124','RYDEANDREW','2102195',true,0, 'aneumann@gfhonda.com');  
  
4. rewrite sco\serviceWriterMetricData.sql as sco\serviceWriterMetricData-RY2.sql
AND run it

/************* ASSUMPTIONS ***********************/
inspection opcode for ry2 IS 24z
/************* ASSUMPTIONS ***********************/

5. warranty calls do NOT cut it, need a call list for any ro ON any honda/nissan
--DROP TABLE HondaNissanROs
/*
since this list contains ANY ro ON a honda OR nissan (Main Shop OR PDQ)
AND we DO NOT currently have PDQ writer information (DO NOT want to model depts yet)
include the writer 
*/
-- DROP TABLE HondaNissanROs;
CREATE TABLE RY2ROsForCalls ( 
--      eeUserName CIChar( 50 ),
      Writer cichar(25),
      ro CIChar( 9 ),
      CloseDate Date,
      Customer CIChar( 30 ),
      HomePhone CIChar( 12 ),
      WorkPhone CIChar( 12 ),
      CellPhone CIChar( 12 ),
      Vin CIChar( 17 ),
      Vehicle CIChar( 60 ),
      FollowUpComplete Logical) IN DATABASE;
-- DROP TABLE HondaNissanCalls      
CREATE TABLE RY2ROCalls ( 
      ro CIChar( 9 ),
      CallTS TimeStamp,
      eeUserName CIChar( 50 ),
      description Memo) IN DATABASE;     
/*
trying to use the same basic script AS used for getting warranty ros, returns 
a shitload of no phone #s
the problem turns out to be IN the JOIN to bopname
warranty uses custkey & store to JOIN factro AND bopname, with honda this results
IN a lot of NULL bopname rows
SELECT a.ro, a.customername, bnco#, bnsnam, bnphon, bnbphn, bncphon 
FROM dds.factro a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
LEFT JOIN dds.stgArkonaBOPNAME c ON a.storecode = c.bnco#
  AND a.customerkey = c.bnkey  
WHERE a.void = false
  AND a.customerkey <> 0 -- inventory
  AND a.storecode = 'ry2'
  AND b.thedate BETWEEN curdate() - 7 AND curdate()  
but IF i take out the store, get a shitload of multiple rows  
SELECT a.ro, a.customername, a.customerkey, bnco#, bnsnam, bnphon, bnbphn, bncphon 
FROM dds.factro a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
LEFT JOIN dds.stgArkonaBOPNAME c ON a.customerkey = c.bnkey  
WHERE a.void = false
  AND a.customerkey <> 0 -- inventory
  AND a.storecode = 'ry2'
  AND b.thedate BETWEEN curdate() - 7 AND curdate()  
AND a.customerkey IN ( 
  SELECT bnkey FROM (
    SELECT *
    FROM dds.stgArkonaBOPNAME
    WHERE bnkey IN (
      SELECT customerkey
      FROM dds.factro a
      INNER JOIN dds.day b ON a.opendatekey = b.datekey
      WHERE a.void = false
        AND a.customerkey <> 0 -- inventory
        AND a.storecode = 'ry2'
        AND b.thedate BETWEEN curdate() - 7 AND curdate())
    ) x GROUP BY bnkey HAVING COUNT(*) > 1)   
WHERE, AS this shows the "bogus" customerkey appears to be the one assigned to RY2  

conclusion: use customerkey & name to JOIN factro to bopname, good enough
further testing showed a couple being LEFT out, but, again, good enough
*/  
-- DELETE FROM RY2ROsForCalls;  
INSERT INTO RY2ROsForCalls   
--SELECT coalesce(f.name, f.description), a.ro, b.thedate AS CloseDate, a.customername AS Customer,

SELECT LEFT(description, position(' ' IN description) - 1),  a.ro, b.thedate AS CloseDate, a.customername AS Customer,
  CASE 
    WHEN c.bnphon  = '0' THEN 'No Phone'
    WHEN c.bnphon  = '' THEN 'No Phone'
    WHEN c.bnphon IS NULL THEN 'No Phone'
    ELSE trim(c.bnphon)
  END AS HomePhone, 
  CASE 
    WHEN c.bnbphn  = '0' THEN 'No Phone'
    WHEN c.bnbphn  = '' THEN 'No Phone'
    WHEN c.bnbphn IS NULL THEN 'No Phone'
    ELSE trim(c.bnbphn)
  END  AS WorkPhone, 
  CASE 
    WHEN c.bncphon  = '0' THEN 'No Phone'
    WHEN c.bncphon  = '' THEN 'No Phone'
    WHEN c.bncphon IS NULL THEN 'No Phone'
    ELSE trim(c.bncphon)
  END  AS CellPhone, a.vin, trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle,
  false as FollowUpComplete
FROM dds.factro a
INNER JOIN -- ros cashiered BETWEEN today AND 3 days ago
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN curdate() - 3 AND curdate()    
LEFT JOIN dds.stgArkonaBOPNAME c ON a.customerkey = c.bnkey  
  AND a.customername = c.bnsnam  
LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
 LEFT JOIN dds.dimServiceWriter f ON a.storecode = f.storecode
  AND a.writerid = f.writernumber
  AND b.thedate BETWEEN f.ServiceWriterKeyFromDate AND f.ServiceWriterKeyThruDate
WHERE a.storecode = 'ry2'
  AND a.customerkey <> 0
  AND a.void = false
  AND e.immake IN ('NISSAN','HONDA')
  AND NOT EXISTS (
    SELECT 1 
    FROM RY2ROsForCalls
    WHERE ro = a.ro); 
 
-- 6/2/13
-- test the storedprocs
fuck, i am ALL twisted up about modeling the hierarchy
what IS the minimum i need for v1.1
mind fucks
1. ry2 caller data IS NOT tied to a writer 
2. ben ry1 & ry2, which shows ON his landing page, how does he see the otehr location
3. ry2, updating the data correctly, this one seems LIKE the low hanging fruit
4. honda nissan calls are NOT writer metrics, it IS departmental metrics - mgr & caller only
5. WHERE IS the line BETWEEN modeling AND hard coding to get to the release
6. so WHEN a manager logs IN, sp.GetWritersForManager returns a list of writers
based ON what!
based ON the managers eeusername, which tells me what
his his role
what IS a role, WHERE does the departmental aspect fit IN
-- 6/3 going goofy with nested sets for dept
/* not now
CREATE TABLE Departments (
  description cichar(24),
  lft integer,
  rgt integer) IN database;
  
INSERT INTO Departments values('GF',1,48);
INSERT INTO Departments values('RY1',2,27);
INSERT INTO Departments values('Fixed',3,24);
INSERT INTO Departments values('Mechanical',4,19);
INSERT INTO Departments values('Main Shop',5,12);
INSERT INTO Departments values('Drive',6,7);
INSERT INTO Departments values('Shop',8,9);
INSERT INTO Departments values('Other',10,11);
INSERT INTO Departments values('PDQ',13,14);
INSERT INTO Departments values('Detail',15,16);
INSERT INTO Departments values('Car Wash',17,18);
INSERT INTO Departments values('Parts',20,21);
INSERT INTO Departments values('Body Shop',22,23);
INSERT INTO Departments values('Variable',25,26);
INSERT INTO Departments values('RY2',28,47);
INSERT INTO Departments values('Fixed',29,44);
INSERT INTO Departments values('Mechanical',30,41);
INSERT INTO Departments values('Main Shop',31,38);
INSERT INTO Departments values('Drive',32,33);
INSERT INTO Departments values('Shop',34,35);
INSERT INTO Departments values('Other',36,37);
INSERT INTO Departments values('PDQ',39,40);
INSERT INTO Departments values('Parts',42,43);
INSERT INTO Departments values('Variable',45,46);  

CREATE TABLE Positions (
  Description cichar(24),
  lft integer,
  rgt integer) IN database;
INSERT INTO Positions values ('Fixed Ops Director',1,48);  -- ben, this IS NOT right
INSERT INTO Positions values ('Manager',5,12); -- mike
INSERT INTO Positions values ('Manager',6,7); -- al
INSERT INTO Positions values ('Manager',31,38); -- andrew
INSERT INTO Positions values ('Service Writer',6,7); -- ry1 main shop writers
INSERT INTO Positions values ('Service Writer',32,33); -- ry2 main shop writers
INSERT INTO Positions values ('Service Follow Up',36,37); -- ry2 service caller
*/  
-- DROP TABLE Departments;
CREATE TABLE Departments (
  StoreCode cichar(3),
  Department cichar(24)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Departments','Departments.adi','PK','StoreCode;Department','',2051,512,'' ); 
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Departments','Table_Primary_Key', 'PK', 'APPEND_FAIL', 'Departmentsfail');    
INSERT INTO Departments values ('RY1','Fixed');
INSERT INTO Departments values ('RY1','Main Shop'); 
INSERT INTO Departments values ('RY1','Drive'); 
INSERT INTO Departments values ('RY2','Fixed');
INSERT INTO Departments values ('RY2','Main Shop'); 
INSERT INTO Departments values ('RY2','Drive'); 
INSERT INTO Departments values ('RY2','Other');

-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('Dept-Positions'); DROP TABLE Positions;
CREATE TABLE Positions (
  StoreCode cichar(3), -- FK
  Department cichar(24), -- FK --at some point this may be an internal organization partyid
  Position cichar(24)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Positions','Positions.adi','PK','StoreCode;Department;Position','',2051,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Positions','Positions.adi','FK','StoreCode;Department','',2,512,'' );    
EXECUTE PROCEDURE sp_ModifyTableProperty('Positions','Table_Primary_Key','PK', 'APPEND_FAIL', 'Positionsfail');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'Dept-Positions','Departments','Positions','FK',2,2,NULL,'','');   
INSERT INTO Positions values ('RY1','Fixed', 'Director');
INSERT INTO Positions values ('RY1','Main Shop', 'Manager');
INSERT INTO Positions values ('RY1','Drive', 'Manager');
INSERT INTO Positions values ('RY1','Drive', 'Service Writer');    
INSERT INTO Positions values ('RY2','Fixed', 'Director');
INSERT INTO Positions values ('RY2','Main Shop', 'Manager');
INSERT INTO Positions values ('RY2','Other', 'Service Follow Up');
INSERT INTO Positions values ('RY2','Drive', 'Service Writer'); 

-- DROP TABLE People;
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('People-WarrantyCalls');
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('Position-Fulfillment2');
CREATE TABLE People (
  eeUserName cichar(50),
  FullName cichar(40),
  FirstName cichar(40),
  LastName cichar(40)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'People','People.adi','PK','eeUserName','',2051,512,'' ); 
EXECUTE PROCEDURE sp_ModifyTableProperty('People','Table_Primary_Key','PK', 'APPEND_FAIL', 'Peoplefail');    
INSERT INTO People
SELECT eeusername, fullname, LEFT(fullname, position(' ' IN fullname) - 1),
  substring(fullname, position(' ' IN fullname) + 1, 25)
FROM servicewriters; 
INSERT INTO People values ('mhuot@rydellchev.com', 'Mike Huot','Mike','Huot' );
INSERT INTO People values ('aberry@rydellchev.com', 'Al Berry','Al','Berry');
INSERT INTO People values ('bcahalan@rydellchev.com', 'Ben Cahalan','Ben','Cahalan');
INSERT INTO People values ('dbrekke@gfhonda.com', 'Devon Brekke','Devon','Brekke');

--EXECUTE PROCEDURE sp_DropReferentialIntegrity('Position-Fulfillment1'); EXECUTE PROCEDURE sp_DropReferentialIntegrity('Position-Fulfillment2');DROP TABLE PositionFulfillment;
CREATE TABLE PositionFulfillment (
  StoreCode cichar(3),
  Department cichar(24),
  Position cichar(24),
  eeUserName cichar(50),
  FromTS timestamp,
  ThruTS timestamp) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
  'PositionFulfillment','PositionFulfillment.adi','PK','StoreCode;Department;Position;eeUserName;FromTS','',2051,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
  'PositionFulfillment','PositionFulfillment.adi','FK1','StoreCode;Department;Position','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
  'PositionFulfillment','PositionFulfillment.adi','FK2','eeUserName','',2,512,'' );    
EXECUTE PROCEDURE sp_ModifyTableProperty('PositionFulfillment','Table_Primary_Key','PK', 'APPEND_FAIL', 'PositionFulfillmentfail');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
  'Position-Fulfillment1','Positions','PositionFulfillment','FK1',2,2,NULL,'','');  
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
  'Position-Fulfillment2','People','PositionFulfillment','FK2',2,2,NULL,'','');         
INSERT INTO PositionFulfillment values('RY1','Fixed', 'Director','bcahalan@rydellchev.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY1','Main Shop', 'Manager','mhuot@rydellchev.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY1','Drive', 'Manager','aberry@rydellchev.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY1','Drive', 'Service Writer','rodt@rydellchev.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY1','Drive', 'Service Writer','tbursinger@rydellchev.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY1','Drive', 'Service Writer','jtyrrell@rydellchev.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY1','Drive', 'Service Writer','dwiebusch@rydellchev.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY1','Drive', 'Service Writer','kespelund@rydellchev.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY1','Drive', 'Service Writer','mflikka@rydellchev.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY1','Drive', 'Service Writer','kcarlson@rydellchev.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY2','Fixed', 'Director','bcahalan@rydellchev.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY2','Main Shop', 'Manager','aneumann@gfhonda.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp)); 
INSERT INTO PositionFulfillment values('RY2','Drive', 'Service Writer','aneumann@gfhonda.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY2','Drive', 'Service Writer','dpederson@gfhonda.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY2','Drive', 'Service Writer','jdangerfield@gfhonda.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionFulfillment values('RY2','Other', 'Service Follow Up','dbrekke@gfhonda.com', '04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));

-- make sure i can derive the writers at UPPER levels, mike, ben
-- starting to mind fuck ON who manages whom
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('Position-ReportsTo1'); EXECUTE PROCEDURE sp_DropReferentialIntegrity('Position-ReportsTo2');DROP TABLE PositionReportsTo;
CREATE TABLE PositionReportsTo (
  SupStoreCode cichar(3),
  SupDepartment cichar(24),
  SupPosition cichar(24),
  SubStoreCode cichar(3),
  SubDepartment cichar(24),
  SubPosition cichar(24),
  FromTS timestamp,
  ThruTS timestamp) IN database;
-- indexes  
EXECUTE PROCEDURE sp_CreateIndex90( 
  'PositionReportsTo','PositionReportsTo.adi','PK','SupStoreCode;SupDepartment;SupPosition;SubStoreCode;SubDepartment;SubPosition;FromTS','',2051,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
  'PositionReportsTo','PositionReportsTo.adi','FK1','SupStoreCode;SupDepartment;SupPosition','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
  'PositionReportsTo','PositionReportsTo.adi','FK2','SubStoreCode;SubDepartment;SubPosition','',2,512,'' );  
-- PK    
EXECUTE PROCEDURE sp_ModifyTableProperty('PositionReportsTo','Table_Primary_Key','PK', 'APPEND_FAIL', 'PositionReportsTofail');
-- RI
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
  'Position-ReportsTo1','Positions','PositionReportsTo','FK1',2,2,NULL,'','');  
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
  'Position-Reportsto2','Positions','PositionReportsTo','FK2',2,2,NULL,'','');    
INSERT INTO PositionReportsTo values(
  'RY1','Fixed','Director','RY1','Main Shop','Manager','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));  
INSERT INTO PositionReportsTo values(
  'RY2','Fixed','Director','RY2','Main Shop','Manager','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));    
INSERT INTO PositionReportsTo values(
  'RY1','Main Shop','Manager','RY1','Drive','Manager','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));   
INSERT INTO PositionReportsTo values(
  'RY1','Drive','Manager','RY1','Drive','Service Writer','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionReportsTo values(
  'RY2','Main Shop','Manager','RY2','Drive','Service Writer','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));  
INSERT INTO PositionReportsTo values(
  'RY2','Main Shop','Manager','RY2','Other','Service Follow Up','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));    
  
--DROP TABLE tasks;  
CREATE TABLE Tasks (
  Task cichar(45),
  Caption cichar(60),
  displayclass cichar(24)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Tasks','Tasks.adi','PK','Task','',2051,512,'' ); 
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Tasks','Table_Primary_Key', 'PK', 'APPEND_FAIL', 'Tasksfail');    
INSERT INTO Tasks values ('Checkout','Daily Checkout','checkout');
INSERT INTO Tasks values ('Update Aged ROs','View Aged ROs','aged'); 
INSERT INTO Tasks values ('Update Warranty Calls','View Warranty Calls','warranty'); 
INSERT INTO Tasks values ('Update RY2 Calls','View RY2 Follow Up Calls','ry2');
/*
EXECUTE PROCEDURE sp_DropReferentialIntegrity( 'Position-Tasks1');
EXECUTE PROCEDURE sp_DropReferentialIntegrity( 'Position-Tasks2');
DROP TABLE PositionTasks;
*/
CREATE TABLE PositionTasks (
  StoreCode cichar(3),
  Department cichar(24),
  Position cichar(24),
  Task cichar(45), 
  FromTS timestamp,
  ThruTS timestamp) IN database;
-- indexes :: TableName,FileName,TagName,Expression,Condition,Options,PageSize,Collation
EXECUTE PROCEDURE sp_CreateIndex90( 
  'PositionTasks','PositionTasks.adi','PK','StoreCode;Department;Position;Task;FromTS','',2051,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
  'PositionTasks','PositionTasks.adi','FK1','StoreCode;Department;Position','',2,512,'' );     
EXECUTE PROCEDURE sp_CreateIndex90( 
  'PositionTasks','PositionTasks.adi','FK2','Task','',2,512,'' ); 
-- PK    
EXECUTE PROCEDURE sp_ModifyTableProperty('PositionTasks','Table_Primary_Key','PK', 'APPEND_FAIL', 'PositionTasksfail'); 
-- RI
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
  'Position-Tasks1','Positions','PositionTasks','FK1',2,2,NULL,'','');  
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
  'Position-Tasks2','Tasks','PositionTasks','FK2',2,2,NULL,'','');  
INSERT INTO PositionTasks values (
  'RY1','Drive','Manager', 'Update Aged ROs','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));  
INSERT INTO PositionTasks values (
  'RY1','Drive','Manager', 'Update Warranty Calls','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));    
INSERT INTO PositionTasks values (
  'RY1','Drive','Service Writer', 'Update Aged ROs','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));  
INSERT INTO PositionTasks values (
  'RY1','Drive','Service Writer', 'Update Warranty Calls','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionTasks values (
  'RY1','Drive','Service Writer', 'Checkout','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));  
INSERT INTO PositionTasks values (
  'RY1','Main Shop','Manager', 'Update Aged ROs','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));  
INSERT INTO PositionTasks values (
  'RY1','Main Shop','Manager', 'Update Warranty Calls','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));     
INSERT INTO PositionTasks values (
  'RY2','Main Shop','Manager', 'Update Aged ROs','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));       
INSERT INTO PositionTasks values (
  'RY2','Main Shop','Manager', 'Update RY2 Calls','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));
INSERT INTO PositionTasks values (
  'RY2','Drive','Service Writer', 'Update Aged ROs','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));    
--INSERT INTO PositionTasks values (
--  'RY2','Drive','Service Writer', 'Update RY2 Calls','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));  
INSERT INTO PositionTasks values (
  'RY2','Other','Service Follow Up', 'Update RY2 Calls','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp)); 
INSERT INTO PositionTasks values (
  'RY2','Drive','Service Writer', 'Checkout','04/01/2013 01:00:00', CAST('12/31/9999 01:00:00' AS sql_timestamp));    

-- now that mgrs can UPDATE, this no longer works
EXECUTE PROCEDURE sp_DropReferentialIntegrity('SW-WarrantyCalls');
-- replace it with   
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('People-WarrantyCalls');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'People-WarrantyCalls','People','WarrantyCalls','EEUSERNAME',1,2,NULL,'',''); 
     

  
    
SELECT a.fullname, b.storecode, b.position
FROM people a
LEFT JOIN PositionFulfillment b ON a.eeusername = b.eeusername 
WHERE b.thruts > now()
ORDER BY b.storecode, b.position

-- ALL people reporting to mhuot
SELECT * 
FROM positions a
INNER JOIN PositionFulfillment b ON a.storecode = b.storecode
  AND a.department = b.department
  AND a.position = b.position
INNER JOIN people c ON b.eeusername = c.eeusername
  AND c.eeusername = 'mhuot@rydellchev.com'
  
-- need to think IN terms of the questions that need to be answered
based ON eeusername
  what tasks can this person perform (LEFT nav task list)
  to whose pages should this person have access (LEFT nav people list
  (grouping BY store? - ben)
  
  
-- 6/8 nested sets with depth added

CREATE TABLE DeptNS (
  description cichar(24),
  lft integer,
  rgt integer,
  depth integer) IN database;
  
INSERT INTO DeptNS values('GF',1,48,1);
INSERT INTO DeptNS values('RY1',2,27,2);
INSERT INTO DeptNS values('Fixed',3,24,3);
INSERT INTO DeptNS values('Mechanical',4,19,4);
INSERT INTO DeptNS values('Main Shop',5,12,5);
INSERT INTO DeptNS values('Drive',6,7,6);
INSERT INTO DeptNS values('Shop',8,9,6);
INSERT INTO DeptNS values('Other',10,11,6);
INSERT INTO DeptNS values('PDQ',13,14,5);
INSERT INTO DeptNS values('Detail',15,16,5);
INSERT INTO DeptNS values('Car Wash',17,18,5);
INSERT INTO DeptNS values('Parts',20,21,4);
INSERT INTO DeptNS values('Body Shop',22,23,4);
INSERT INTO DeptNS values('Variable',25,26,3);
INSERT INTO DeptNS values('RY2',28,47,2);
INSERT INTO DeptNS values('Fixed',29,44,3);
INSERT INTO DeptNS values('Mechanical',30,41,4);
INSERT INTO DeptNS values('Main Shop',31,38,5);
INSERT INTO DeptNS values('Drive',32,33,6);
INSERT INTO DeptNS values('Shop',34,35,6);
INSERT INTO DeptNS values('Other',36,37,6);
INSERT INTO DeptNS values('PDQ',39,40,5);
INSERT INTO DeptNS values('Parts',42,43,4);
INSERT INTO DeptNS values('Variable',45,46,3);  

CREATE TABLE PositionNS (
  Description cichar(24),
  lft integer,
  rgt integer) IN database;
INSERT INTO PositionNS values ('Fixed Ops Director',3,24);  
INSERT INTO PositionNS values ('Fixed Ops Director',29,44);
INSERT INTO PositionNS values ('Manager',5,12); -- mike
INSERT INTO PositionNS values ('Manager',6,7); -- al
INSERT INTO PositionNS values ('Manager',31,38); -- andrew
INSERT INTO PositionNS values ('Service Writer',6,7); -- ry1 main shop writers
INSERT INTO PositionNS values ('Service Writer',32,33); -- ry2 main shop writers
INSERT INTO PositionNS values ('Service Follow Up',36,37); -- ry2 service caller


-- 6/8 stored procs 
  
SELECT *
FROM serviceWriterMetricData 
WHERE thedate = curdate() -1

-- 6/9
ry2 OPEN calls IS NOT really serviceWriterMetricData 
so WHEN AND WHERE DO i UPDATE the tables

/*
-- user AND tasks
SELECT a.task, a.caption
FROM tasks a
INNER JOIN positiontasks b ON a.task = b.task
  AND b.thruts > now()
INNER JOIN positions c ON b.storecode = c.storecode
  AND b.department = c.department
  AND b.position = c.position
INNER JOIN positionfulfillment d ON c.storecode = d.storecode
  AND c.department = d.department
  AND c.position = d.position
  AND d.thruts > now()
INNER JOIN people e ON d.eeusername = e.eeusername
  AND e.eeusername = 'rodt@rydellchev.com'

-- get people list 
DECLARE @eeusername string;
--@eeusername = 'mhuot@rydellchev.com';  
@eeusername = 'aneumann@gfhonda.com'; 
--@eeusername = 'dpederson@gfhonda.com'; 
IF @eeusername IN ('mhuot@rydellchev.com','aberry@rydellchev.com','bcahalan@rydellchev.com') THEN 
  SELECT a.eeusername, a.fullname
  FROM people a
  INNER JOIN Positionfulfillment b ON a.eeusername = b.eeusername
    AND b.storecode = 'ry1'
    AND b.position = 'Service Writer';
ELSEIF @eeusername = 'aneumann@gfhonda.com' THEN 
  SELECT a.eeusername, a.fullname
  FROM people a
  INNER JOIN Positionfulfillment b ON a.eeusername = b.eeusername
    AND b.storecode = 'ry2'
    AND b.position = 'Service Writer';
ELSE 
  SELECT '' FROM system.iota;
END IF;  
*/

-- 6/10
-- aged ros
fucking andrew
IF he IS logged IN AS manager OR IF he IS logged IN AS writer
IF manager should see ALL aged ros
IF writer should see his aged ros
DECLARE @id string;
--@id = 'dpederson@gfhonda.com'; 
--@id = 'aneumann@gfhonda.com';
@id = 'mhuot@rydellchev.com';  
--INSERT INTO __output
SELECT b.firstname, a.ro, c.mmddyy AS opendate, a.customer, a.rostatus, a.vin, a.vehicle, 
  d.statusUpdate, openDate
FROM AgedRos a
INNER JOIN ServiceWriters b ON a.eeusername = b.eeusername
  AND CASE
    WHEN @id IN ('mhuot@rydellchev.com','aberry@rydellchev.com') THEN b.storecode = 'RY1'
    WHEN @id = 'aneumann@gfhonda.com' THEN b.storecode = 'RY2'
    ELSE b.eeusername = @id 
  END 
LEFT JOIN dds.day c ON a.opendate = c.thedate
LEFT JOIN (
  SELECT *
  FROM AgedROUpdates x
  WHERE updateTS = (
    SELECT MAX(updatets)
    FROM AgedRoUpdates
    WHERE ro = x.ro)) d ON a.ro = d.ro; 
    
SELECT *
FROM warrantyros    
  