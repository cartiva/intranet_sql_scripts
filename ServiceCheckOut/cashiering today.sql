 
 
 SELECT m.sundaytosaturdayweek, 0, fullname, m.writerid, 'Cashiering',
  sun, mon, tue, wed, thu, fri, sat, eeusername, 1  
--SELECT *
FROM ( -- sunday to saturday
  SELECT sundaytosaturdayweek, z.writerid, w.fullname, w.eeusername, 
    max(CASE WHEN dayname = 'sunday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS sun,
    max(CASE WHEN dayname = 'monday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS mon,
    max(CASE WHEN dayname = 'tuesday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS tue,
    max(CASE WHEN dayname = 'wednesday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS wed,
    max(CASE WHEN dayname = 'thursday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS thu,
    max(CASE WHEN dayname = 'friday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS fri,
    max(CASE WHEN dayname = 'saturday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS sat
  FROM (
    SELECT sundaytosaturdayweek, dayname, writerid, COUNT(*) AS Total,  
      SUM(CASE WHEN y.storecode IS NULL THEN 0 ELSE 1 END) AS WriterCashiered
    FROM (

      SELECT b.sundaytosaturdayweek, b.thedate, dayname, a.ro, a.writerid, f.gquser
      FROM factROToday a
      INNER JOIN 
        dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate = curdate()
      INNER JOIN factROLineToday c ON a.ro = c.ro    
      INNER JOIN tmpGLPTRNS e ON a.ro = e.gtdoc# -- cashiered ros only
      INNER JOIN tmpGLPDTIM f ON e.gttrn# = f.gqtrn#  
      INNER JOIN ServiceWriters g ON a.storecode = g.storecode
        AND a.writerid = g.writernumber
        AND NOT EXISTS (
          SELECT 1 
          FROM factROLineToday
          WHERE ro = a.ro
            AND paytype IN ('w','i'))
        AND NOT EXISTS (
          SELECT 1
          FROM factROLineToday
          WHERE ro = a.ro
            AND servicetype <> 'mr')      
        AND a.void = false      
      GROUP BY b.sundaytosaturdayweek, b.thedate, b.dayname, a.ro, a.writerid, f.gquser ) x


    LEFT JOIN servicewriters y ON x.gquser = y.dtUserName 
    GROUP BY sundaytosaturdayweek, dayname, writerid) z 
  LEFT JOIN servicewriters w ON z.writerid = w.writernumber 
  GROUP BY sundaytosaturdayweek, z.writerid, w.fullname, w.eeusername) m
  
  
  

UPDATE writerlandingpage
  SET Thu = trim(CAST(a.WriterCashiered AS sql_char)) + ' / ' + trim(CAST(a.total AS sql_char))  
FROM (  
    SELECT sundaytosaturdayweek, dayname, writerid, COUNT(*) AS Total,  
      SUM(CASE WHEN y.storecode IS NULL THEN 0 ELSE 1 END) AS WriterCashiered
    FROM (
      SELECT b.sundaytosaturdayweek, b.thedate, dayname, a.ro, a.writerid, f.gquser
      FROM factROToday a
      INNER JOIN 
        dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate = curdate()
      INNER JOIN factROLineToday c ON a.ro = c.ro    
      INNER JOIN tmpGLPTRNS e ON a.ro = e.gtdoc# -- cashiered ros only
      INNER JOIN tmpGLPDTIM f ON e.gttrn# = f.gqtrn#  
      INNER JOIN ServiceWriters g ON a.storecode = g.storecode
        AND a.writerid = g.writernumber
        AND NOT EXISTS (
          SELECT 1 
          FROM factROLineToday
          WHERE ro = a.ro
            AND paytype IN ('w','i'))
        AND NOT EXISTS (
          SELECT 1
          FROM factROLineToday
          WHERE ro = a.ro
            AND servicetype <> 'mr')      
        AND a.void = false      
      GROUP BY b.sundaytosaturdayweek, b.thedate, b.dayname, a.ro, a.writerid, f.gquser ) x
    LEFT JOIN servicewriters y ON x.gquser = y.dtUserName 
    GROUP BY sundaytosaturdayweek, dayname, writerid) a
WHERE writerlandingpage.weekid = a.sundaytosaturdayweek
  AND writerlandingpage.writernumber = a.writerid   
  AND metric = 'Cashiering'  

UPDATE writerlandingpage
SET thu = ''
WHERE weekid = 483  