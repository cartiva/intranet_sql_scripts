v1.7
new stored proc: GetWarrantyAdminPdqROs

the warranty admin snippet was changed, so:


UPDATE htmlsnippets
SET snippet = (
  SELECT snippet
  FROM scotest.htmlsnippets
  WHERE storecode = 'ALL' 
    AND membergroup = 'WarrantyAdmin')
WHERE storecode = 'ALL' 
  AND membergroup = 'WarrantyAdmin'