SELECT b.thedate, a.writerid, a.ro
--SELECT servicetype, COUNT(*)
FROM dds.factro a
INNER JOIN -- cashiered ros only
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN curdate() - 60 AND curdate() -1
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber    


