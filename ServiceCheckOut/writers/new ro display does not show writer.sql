FROM sp.GetROHeader
previously ros were only displayed IN sco, therefore joining to sco.servicewriters 
was ok
now, showing ros IN tp, need to show ros that are NOT only those of sco writers

  SELECT distinct a.ro AS RO, b.rostatus AS ROStatus, 
    left(c.DayName,3) + ' ' + c.mmdd AS OpenDate, 
    iif(d.thedate = '12/31/9999', '',left(d.DayName,3) + ' ' + d.mmdd) AS CloseDate,
    e.fullname AS Customer,
--    h.fullname AS WriterFirstName, 
    trim(h.firstname) + ' ' + h.lastname AS writerName,
    g.writernumber
  FROM dds.todayFactRepairOrder a
  LEFT JOIN dds.dimRoStatus b ON a.rostatuskey = b.rostatuskey
  LEFT JOIN dds.day c ON a.opendatekey = c.datekey
  LEFT JOIN dds.day d ON a.finalclosedatekey = d.datekey
  LEFT JOIN dds.dimcustomer e ON a.customerkey = e.customerkey
  LEFT JOIN dds.dimVehicle f ON a.vehiclekey = f.vehiclekey
  LEFT JOIN dds.dimServiceWriter g ON a.servicewriterkey = g.servicewriterkey
--  LEFT JOIN ServiceWriters h ON g.writernumber = h.writernumber
  LEFT JOIN dds.edwEmployeeDim h on g.employeenumber = h.employeenumber
    AND c.thedate BETWEEN h.employeekeyfromdate AND h.employeekeythrudate
  LEFT JOIN dds.dimRoComment i ON a.rocommentkey = i.rocommentkey
  WHERE a.ro IN ('16140439','16140469')
  

-- one issue IS dimServiceWriters vs sco.ServiceWriters
-- another IS writers without an employeenumber (Bev ...)

-- here's bev
UPDATE dds.dimServiceWriter
SET employeeNumber = '187675'
WHERE storecode = 'RY1'
  AND writernumber = '717'
      
      