ADD gary ramberg AS a new writer
1.dds.dimServicewriter: fix gary ramberg IN dimServiceWriter.sql
2.
INSERT INTO "People" VALUES( 'gramberg@rydellcars.com', 'Gary Ramberg', 'Gary', 'Ramberg', 'password', 'ServiceWriter', 'RY1' );

INSERT INTO ServiceWriters values('RY1','Gary Ramberg','Ramberg','Gary','734','RYDEGARYRA','1114105',true,0,'gramberg@rydellcars.com');

INSERT INTO PositionFulfillment values('RY1','Drive','Service Writer','gramberg@rydellcars.com', CAST('11/04/2013 01:00:00' AS sql_timestamp),
  CAST('12/31/9999 01:00:00' AS sql_timestamp))
  
-- 1/15/14

-- ben dalen 
INSERT INTO "People" VALUES( 'bdalen@rydellchev.com', 'Benjamin Dalen', 'Benjamin', 'Dalen', 'password', 'ServiceWriter', 'RY1' );

INSERT INTO ServiceWriters values('RY1','Benjamin Dalen','Dalen','Benjamin','723','RYDEBENDAL','131100',true,0,'bdalen@rydellchev.com');

INSERT INTO PositionFulfillment values('RY1','Drive','Service Writer','bdalen@rydellchev.com', CAST('11/06/2014 01:00:00' AS sql_timestamp),
  CAST('12/31/9999 01:00:00' AS sql_timestamp));
  
-- 5/2/15
people TABLE no longer IN use
needs vision access   

INSERT INTO ServiceWriters values('RY1','Zachary Sieracki','Sierack','Zachary','443','ZSIERA','1126110',true,0,'zsieracki@rydellcars.com');

INSERT INTO PositionFulfillment values('RY1','Drive','Service Writer','zsieracki@rydellcars.com', '04/06/2015','12/31/9999');

select * FROM ptoemployees WHERE username = 'zsieracki@gfhonda.com'

select * FROM tpemployees WHERE username = 'zsieracki@gfhonda.com'

select * FROM employeeappauthorization WHERE username = 'zsieracki@gfhonda.com'

select * FROM employeeappauthorization WHERE username like 'tbur%'

INSERT INTO employeeAppAuthorization (username, appname, appseq,appcode,approle,functionality)
select 'zsieracki@gfhonda.com', appname, appseq, appCode, appRole, functionality
FROM applicationmetadata
WHERE appcode = 'sco'
  AND approle = 'writer'
  
-- 6/10/15  
-- Stacey Piche
-- select * FROM dds.dimservicewriter ORDER BY servicewriterkey
-- 449

INSERT INTO ServiceWriters (storecode, fullname, lastname, firstname, writernumber, 
  dtUserName, employeenumber, active, eeMemberID, userName)
SELECT a.storecode, a.name, a.lastname, a.firstname, '449',
  'SPICH0', a.employeenumber, true, 0, b.username
FROM dds.edwEmployeeDim a
INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
WHERE a.lastname = 'piche'
  AND a.currentrow = true;
  
INSERT INTO PositionFulfillment values('RY1','Drive','Service Writer','spiche@rydellcars.com', '05/01/2015','12/31/9999');
  


SELECT * FROM dds.stgarkonapdpphdr WHERE ptswid = '449'

-- 11/17/16
-- ned euliss  
writer number: 471

INSERT INTO ServiceWriters (storecode, fullname, lastname, firstname, writernumber, 
  dtUserName, employeenumber, active, eeMemberID, userName)  
SELECT a.storecode, a.name, a.lastname, a.firstname, '471',
  'RYDENEDEUL', a.employeenumber, true, 0, b.username
FROM dds.edwEmployeeDim a
INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
WHERE a.lastname = 'euliss'
  AND a.currentrow = true;
  
INSERT INTO PositionFulfillment values('RY1','Drive','Service Writer','neuliss@rydellcars.com', '10/20/2016','12/31/9999');

INSERT into employeeappauthorization
select 'neuliss@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey 
FROM employeeappauthorization
WHERE username = 'tbursinger@rydellchev.com'
  AND appcode = 'sco';
