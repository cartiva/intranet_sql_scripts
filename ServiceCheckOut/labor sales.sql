/*  
4/2 change outputs to strings instead of numbers
    AND the landing page IS DAILY SNAPSHOTS, NOT running total
    the checkout page IS the total week to date
    
*/    


INSERT INTO writerlandingpage
SELECT sundaytosaturdayweek, 0, fullname, writerid, 'Labor Sales',
  trim(cast(sunLS AS sql_char)),trim(cast(monLS AS sql_char)),trim(cast(tueLS AS sql_char)),
  trim(cast(wedLS AS sql_char)),trim(cast(thuLS AS sql_char)),trim(cast(friLS AS sql_char)),
  trim(cast(satLS AS sql_char)),
  trim(cast(sunLS+monLS+tueLS+wedLS+thuLS+friLS+satLS AS sql_char)), eeusername, 5     
FROM (
  SELECT sundaytosaturdayweek, writerid, fullname, eeusername, 
    coalesce(MAX(CASE WHEN dayname = 'sunday' THEN laborsales END), 0) AS sunLS,
    coalesce(MAX(CASE WHEN dayname = 'Monday' THEN laborsales END), 0) AS monLS,
    coalesce(MAX(CASE WHEN dayname = 'Tuesday' THEN laborsales END), 0) AS tueLS,
    coalesce(MAX(CASE WHEN dayname = 'Wednesday' THEN laborsales END), 0) AS wedLS,
    coalesce(MAX(CASE WHEN dayname = 'Thursday' THEN laborsales END), 0) AS thuLS,
    coalesce(MAX(CASE WHEN dayname = 'Friday' THEN laborsales END), 0) AS friLS,
    coalesce(MAX(CASE WHEN dayname = 'Saturday' THEN laborsales END), 0) AS satLS   
  FROM (  
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
  LEFT JOIN (  
    SELECT finalclosedate, writerid, fullname, eeusername, round(cast(abs(sum(laborsales)) AS sql_double),0) AS laborsales
    FROM (
      SELECT b.thedate AS finalclosedate, a.writerid, a.ro, a.laborsales, d.fullname, d.eeusername
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN dds.factroline c ON a.ro = c.ro
        AND c.lineflaghours > 0 -- only ros with flag hours 
      INNER JOIN ServiceWriters d ON a.storecode = d.storecode
        AND a.writerid = d.writernumber    
      WHERE a.void = false   
      GROUP BY b.thedate, a.writerid, a.ro, a.laborsales, d.fullname, d.eeusername) x
    GROUP by finalclosedate, writerid, fullname, eeusername) b ON a.thedate = b.finalclosedate
  WHERE fullname IS NOT NULL     
  GROUP BY sundaytosaturdayweek, writerid, fullname, eeusername) c  