    SELECT id, right(TRIM(dialedNumber), 10) AS dialedNumber, 
      right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds,
      replace(replace(b.filename, 'E:\Recordings','.'),'\\GF-H-CALLCOPY\Recordings','.') AS fileName
    FROM sh.extCall a
    LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
      AND a.extension = b.extension
      AND LEFT(trim(a.sipCallId), 15) = b.guid    
    WHERE callType = 3
      AND length(TRIM(dialedNumber)) = 12
      
      

UPDATE tmprocalls
SET recordingFileName = replace(replace(replace(replace(recordingFileName, '.\', 'http://172.17.196.70:3000/'), '\', '/'), '.wav',''), '.cca','')
WHERE recordingFileName LIKE '%.cca'      


SELECT record


SELECT filename, 
left(trim(replace(replace(replace(replace(fileName, 'E:\Recordings\','http://172.17.196.70:3030/'), '\','/'),'.wav',''), '.cca','')), 100) 
FROM sh.callCopyRecordings
