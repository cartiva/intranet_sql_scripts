-- 1. ro info
DROP TABLE tmpRoPhoneNumbers1;
CREATE TABLE tmpRoPhoneNumbers1 (
  ro cichar(9) constraint NOT NULL,
  customerKey integer constraint NOT NULL, 
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  constraint pk primary key (ro))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers1','tmpRoPhoneNumbers1.adi','customerKey',
   'customerKey','',2,512,'' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers1','tmpRoPhoneNumbers1.adi','roVin',
   'ro;vin','',2,512,'' );       
INSERT INTO tmpRoPhoneNumbers1
SELECT distinct a.ro, a.customerKey, e.vin, a.rocreatedTS, b.thedate AS openDate, 
  c.thedate AS closeDate, d.theDate AS fcDate
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.opendatekey = b.datekey
INNER JOIN dds.day c on a.closedatekey = c.datekey
INNER JOIN dds.day d on a.finalclosedatekey = d.datekey
INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey
WHERE a.customerKey NOT IN ( -- eliminate ros for inventory vehicles
  SELECT customerKey
  FROM dds.dimCustomer
  WHERE fullname = 'inventory'
   OR lastname IN ('CARWASH','SHOP TIME'))
AND b.theyear = 2014;

-- 2.
-- phone numbers FROM scheduler
DROP TABLE tmpRoPhoneNumbers2;
CREATE TABLE tmpRoPhoneNumbers2 (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  schedPhone cichar(10) constraint NOT NULL,
  schedPhoneRight7 cichar(7) constraint NOT NULL,
  constraint pk primary key (ro, schedPhone))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','roVin',
   'ro;vin','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','schedPhone',
   'schedPhone','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','schedPhoneRight7',
   'schedPhoneRight7','',2,512,'' );    
INSERT INTO tmpRoPhoneNumbers2
SELECT h.ro, h.vin, h.roCreatedTS, h.openDate, h.closeDate, h.fcDate,
  TRIM(schedPhone), right(TRIM(schedPhone), 7) 
FROM (
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.homephone) AS schedPhone
FROM dds.stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.homephone) <> '' 
-- GROUP BY b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, a.homephone
UNION 
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.bestPhoneToday)
FROM dds.stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.bestPhoneToday) <> '' 
--GROUP BY a.ro, a.bestPhoneToday
UNION 
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.cellPhone)
FROM dds.stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.cellPhone) <> '') h;
--GROUP BY a.ro, a.cellPhone
   
-- 3.
-- phone numbers FROM dimCustomer
DROP TABLE tmpRoPhoneNumbers3;
CREATE TABLE tmpRoPhoneNumbers3 (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  arkPhone cichar(10) constraint NOT NULL,
  arkPhoneRight7 cichar(7) constraint NOT NULL,
  constraint pk primary key (ro, arkPhone))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','roVin',
   'ro;vin','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','arkPhone',
   'arkPhone','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','arkPhoneRight7',
   'arkPhoneRight7','',2,512,'' );      
INSERT INTO tmpRoPhoneNumbers3
SELECT ro, vin, roCreatedTS, openDate, closeDate, fcDate, trim(arkPhone),
  right(TRIM(arkPhone),7) AS arkPhone7
FROM (
  SELECT a.*, b.homephone AS arkPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.homePhone), 'x')) = 7 or length(coalesce(trim(b.homePhone), 'x')) = 10
  UNION
  SELECT a.*, b.businessPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.businessPhone), 'x')) = 7  OR length(coalesce(trim(b.businessPhone), 'x')) = 10
  UNION
  SELECT a.*, b.cellPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.cellPhone), 'x')) = 7 OR length(coalesce(trim(b.cellPhone), 'x')) = 10) g; 

 
-- 4.
-- all phone numbers for an ro
-- each ro can have 0:6 phone numbers
DROP TABLE tmpRoPhoneNumbers;
CREATE TABLE tmpRoPhoneNumbers (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  customerPhoneNumber cichar(10) default '0' constraint NOT NULL,
  customerPhoneNumberRight7 cichar(7) default '0' constraint NOT NULL,
  constraint pk primary key (ro, customerPhoneNumber))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','ro',
   'ro','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','customerPhoneNumber',
   'customerPhoneNumber','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','customerPhoneNumberRight7',
   'customerPhoneNumberRight7','',2,512,'' ); 
INSERT INTO tmpRoPhoneNumbers
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  coalesce(b.schedPhone, '0'), coalesce(b.schedPhoneRight7, '0')
FROM tmpRoPhoneNumbers1 a
LEFT JOIN (
  SELECT * 
  FROM tmpRoPhoneNumbers2
  UNION 
  SELECT * 
  FROM tmpRoPhoneNumbers3) b on a.ro = b.ro;
  
  
-- include extCall.id for eventual linking to extConnect AND call copy
-- 5/21 ADD recordingFileName
DROP TABLE tmpRoCalls;
CREATE TABLE tmpRoCalls (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  customerPhoneNumber cichar(10) constraint NOT NULL,
  customerPhoneNumberRight7 cichar(7) constraint NOT NULL,
  callStartTS timestamp constraint NOT NULL,
  callDate date constraint NOT NULL,
  callDuration integer default '0' constraint NOT NULL, 
  extCallId integer constraint NOT NULL, 
  recordingFileName cichar(100),
  constraint pk primary key (ro, customerPhoneNumber, callStartTS))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','ro',
   'ro','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','roCreatedTS',
   'roCreatedTS','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','customerPhoneNumberRight7',
   'customerPhoneNumberRight7','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','callStartTS',
   'callStartTS','',2,512,'' );    
   
--thinking i need to DO an INNER JOIN to extcall, ALL i want IN this TABLE are 
--actual calls, there IS no need to list phone numbers for ros that have NOT been called
-- AND IF i am deriving only actual calls, don't need the coalescing
INSERT INTO tmpRoCalls
/*
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date), 
  max(b.durationSeconds) AS durationSeconds, max(id)
-- SELECT COUNT(*)   
FROM tmpRoPhoneNumbers a
INNER JOIN (
  SELECT id, right(TRIM(dialedNumber), 10) AS dialedNumber, 
    right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds
  FROM sh.extCall
  WHERE callType = 3
    AND length(TRIM(dialedNumber)) = 12) b 
      on a.customerPhoneNumber = b.dialedNumber
        AND b.startTime > a.roCreatedTS    
GROUP BY a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date);       
*/
 
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date), 
  max(b.durationSeconds) AS durationSeconds, max(id),
  b.fileName
-- SELECT COUNT(*)   
FROM tmpRoPhoneNumbers a
INNER JOIN (
  SELECT id, right(TRIM(dialedNumber), 10) AS dialedNumber, 
    right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds, 
    replace(replace(b.filename, 'E:\Recordings','.'),'\\GF-H-CALLCOPY\Recordings','.') AS fileName
  FROM sh.extCall a
  LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
    AND a.extension = b.extension
    AND LEFT(trim(a.sipCallId), 15) = b.guid
  WHERE callType = 3
    AND length(TRIM(dialedNumber)) = 12 ) b 
      on a.customerPhoneNumber = b.dialedNumber
        AND b.startTime > a.roCreatedTS    
GROUP BY a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date), b.fileName;  