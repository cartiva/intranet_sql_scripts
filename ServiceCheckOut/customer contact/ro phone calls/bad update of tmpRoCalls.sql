SELECT a.ro, b.*
FROM tmpRoPhoneNumbers a
LEFT JOIN sh.extcall b on a.customerphonenumber = b.dialednumber10
  AND CAST(b.startTime AS sql_date) = '06/25/2014'
  AND b.calltype = 3
WHERE a.ro IN (
  SELECT ro
  FROM warrantycalls
  WHERE CAST(callts AS sql_date) = '06/25/2014')
ORDER BY a.ro  

select * FROM sh.extcall



  SELECT distinct a.ro, a.customerKey, e.vin, a.rocreatedTS, b.thedate AS openDate, 
    c.thedate AS closeDate, d.theDate AS fcDate
  FROM dds.factRepairOrder a
  INNER JOIN dds.day b on a.opendatekey = b.datekey
  INNER JOIN dds.day c on a.closedatekey = c.datekey
  INNER JOIN dds.day d on a.finalclosedatekey = d.datekey
  INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey
WHERE a.ro = '16156396'

the problem IS that tmpRoCalls.closeDate/fcDate are NOT getting updated
ro 16156396
was called on 6/25/14
SELECT *
FROM sh.extcall
WHERE dialednumber10 = '7013603199'
but
SELECT *
FROM tmpRoCalls
WHERE ro = '16156396'
shows no fcDate
because even though
SELECT *
FROM tmpRoPhoneNumbers1
WHERE ro = '16156396'
shows a fcDate
WHEN tpRoCalls IS updated, the WHERE clause includes  
  WHERE NOT EXISTS (
    SELECT 1
    FROM tmpRoCalls
    WHERE ro = e.ro
      AND customerPhoneNumber = e.customerPhoneNumber
      AND startTime = e.startTime
since that call already exists  
wait, that fucking call does NOT exist yet          


7/1/14 Problems:


1. need to update ro info, row gets created when close/fcDates are 12/31/9999, ro
gets closed, that info never gets updated
2. this makes no fucking sense, DELETE all data from everything from
tmpRoPhoneNumxxx every 5 minutes, base ro info into tmpRoPhoneNumbers1 comes from
factRepairOrder WHICH DOES NOT CHANGE ALL DAY LONG! seems i remember thinking, 
this would be the overnight, never got around to thinking out real time
Needs to be updated thruout the day FROM todayFactRepairOrder AND todaySchedulerAppointments
Which also means dimCustomer needs to be updated real time, is it currently (AS part of todayFactRepairOrder - YES)?
New calls get entered thruout the day because extCall/callCopyRecordings are
simply continuously updated, but the ro/phone number info is not being updated
thruout the day.

So, what does the real time UPDATE process look LIKE:
  RO info:
    new ro
	existing ro, UPDATE closeDate, fcDate
	
i DO NOT believe that i need a todayTmpRoCalls, just supplement it with new ros,
  AND UPDATE the dates WHERE applicable	
need to figure out dependency AND ORDER IN which to run ALL this shit

-- 7/2
IN facdt, what i am thinking, IS that once the base data IS IN there, IS there any
need to DO anything except real time? IS it necessary to DO a delete/repopulate
with 90 days worth of data over night

but still, going to have the same issue, need to UPDATE ro.dates on each run
  
oh cool, todayFactRepairOrder updates both dimCustomer AND dimVehicle  

couple things are going to have to happen
  1. UPDATE existing data with dates
  2. new (today) data
  
-- 7/3
WHERE IN the flow to UPDATE the ro info  
duh, it has to be at the END, ALL i really care about
IS that the ro info IN tmpRoCalls IS correct


  
ALTER PROCEDURE xfmTmpRoCalls
   ( 
   ) 
BEGIN 
/*
6/3
  changed filename url
6/22/14
  extCall now has a field: dialedNumber10, use it
6/26/14
  *a*  bad field name, fails silently IN the EXISTS clauses  
7/4/14
  initial load run against factRepairOrder, stgRydellServiceAppointments
  subsequently run daily every 5 minutes after completion of todayFactRepairOrder
    

EXECUTE procedure xfmTmpRoCalls();
*/
BEGIN TRANSACTION;
TRY 
  TRY
    DELETE FROM tmpRoPhoneNumbers1;
    INSERT INTO tmpRoPhoneNumbers1
    SELECT distinct a.ro, a.customerKey, e.vin, a.rocreatedTS, b.thedate AS openDate, 
      c.thedate AS closeDate, d.theDate AS fcDate
    FROM dds.todayfactRepairOrder a  -- dds.factRepairOrder a
    INNER JOIN dds.day b on a.opendatekey = b.datekey
    INNER JOIN dds.day c on a.closedatekey = c.datekey
    INNER JOIN dds.day d on a.finalclosedatekey = d.datekey
    INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey
    WHERE a.customerKey NOT IN ( -- eliminate ros for inventory vehicles
      SELECT customerKey
      FROM dds.dimCustomer
      WHERE fullname = 'inventory'
       OR lastname IN ('CARWASH','SHOP TIME'));
  CATCH ALL
    RAISE xfmTmpRoCalls(1, 'tmpRoPhoneNumbers1 ' + __errtext);
  END TRY;  
  TRY
    DELETE FROM tmpRoPhoneNumbers2;
    INSERT INTO tmpRoPhoneNumbers2
    SELECT h.ro, h.vin, h.roCreatedTS, h.openDate, h.closeDate, h.fcDate,
      TRIM(schedPhone), right(TRIM(schedPhone), 7) 
    FROM (
    SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
      trim(a.homephone) AS schedPhone
    FROM dds.todaySchedulerAppointments a -- dds.stgRydellServiceAppointments a
    INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
    WHERE trim(a.homephone) <> '' 
    UNION 
    SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
      trim(a.bestPhoneToday)
    FROM dds.todaySchedulerAppointments a -- dds.stgRydellServiceAppointments a
    INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
    WHERE trim(a.bestPhoneToday) <> '' 
    UNION 
    SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
      trim(a.cellPhone)
    FROM dds.todaySchedulerAppointments a -- dds.stgRydellServiceAppointments a
    INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
    WHERE trim(a.cellPhone) <> '') h;
  CATCH ALL
    RAISE xfmTmpRoCalls(2, 'tmpRoPhoneNumbers2 ' + __errtext);
  END TRY;  
  TRY
    DELETE FROM tmpRoPhoneNumbers3;
    INSERT INTO tmpRoPhoneNumbers3
    SELECT ro, vin, roCreatedTS, openDate, closeDate, fcDate, trim(arkPhone),
      right(TRIM(arkPhone),7) AS arkPhone7
    FROM (
      SELECT a.*, b.homephone AS arkPhone
      FROM tmpRoPhoneNumbers1 a
      INNER JOIN dds.dimCustomer b
      on a.customerKey = b.customerKey
      WHERE length(coalesce(trim(b.homePhone), 'x')) = 7 or length(coalesce(trim(b.homePhone), 'x')) = 10
      UNION
      SELECT a.*, b.businessPhone
      FROM tmpRoPhoneNumbers1 a
      INNER JOIN dds.dimCustomer b
      on a.customerKey = b.customerKey
      WHERE length(coalesce(trim(b.businessPhone), 'x')) = 7  OR length(coalesce(trim(b.businessPhone), 'x')) = 10
      UNION
      SELECT a.*, b.cellPhone
      FROM tmpRoPhoneNumbers1 a
      INNER JOIN dds.dimCustomer b
      on a.customerKey = b.customerKey
      WHERE length(coalesce(trim(b.cellPhone), 'x')) = 7 OR length(coalesce(trim(b.cellPhone), 'x')) = 10) g; 
  CATCH ALL
    RAISE xfmTmpRoCalls(3, 'tmpRoPhoneNumbers3 ' + __errtext);
  END TRY;  
  TRY
    DELETE FROM tmpRoPhoneNumbers; 
    INSERT INTO tmpRoPhoneNumbers
    SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
      coalesce(b.schedPhone, '0'), coalesce(b.schedPhoneRight7, '0')
    FROM tmpRoPhoneNumbers1 a
    LEFT JOIN (
      SELECT * 
      FROM tmpRoPhoneNumbers2
      UNION 
      SELECT * 
      FROM tmpRoPhoneNumbers3) b on a.ro = b.ro;  
  CATCH ALL
    RAISE xfmTmpRoCalls(4, 'tmpRoPhoneNumbers ' + __errtext);
  END TRY;  
  TRY
    INSERT INTO tmpRoCalls
    SELECT *
    FROM (
      SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
        a.customerPhoneNumber, a.customerPhoneNumberRight7,
        b.startTime, CAST(b.startTime AS sql_date), 
        max(b.durationSeconds) AS durationSeconds, max(id), 
        b.filename
      FROM tmpRoPhoneNumbers a
      INNER JOIN (
        SELECT id, dialedNumber10,
          right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds, 
  		    left(trim(replace(replace(replace(replace(fileName, 'E:\Recordings\','https://mediaserver.cartiva.com:3000/'), '\','/'),'.wav',''), '.cca','')), 100) AS fileName
        FROM sh.extCall a
        LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
          AND a.extension = b.extension
          AND LEFT(trim(a.sipCallId), 15) = b.guid
        WHERE callType = 3
          AND length(TRIM(dialedNumber)) = 12 ) b 
            on a.customerPhoneNumber = b.dialedNumber10
              AND b.startTime > a.roCreatedTS    
      GROUP BY a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
        a.customerPhoneNumber, a.customerPhoneNumberRight7,
        b.startTime, CAST(b.startTime AS sql_date), b.fileName) e  
    WHERE NOT EXISTS (
      SELECT 1
      FROM tmpRoCalls
      WHERE ro = e.ro
        AND customerPhoneNumber = e.customerPhoneNumber
        AND callStartTs = e.startTime);	  
    UPDATE tmpRoCalls
    SET closeDate = x.closeDate,
        fcDate = x.fcDate
    FROM tmpRoPhoneNumbers x
    WHERE tmpRoCalls.ro = x.ro
      AND (tmpRoCalls.closeDate <> x.closeDate or tmpRoCalls.fcDate <> x.fcDate); 	 
  	 
  CATCH ALL
    RAISE tmpRoCalls(5, 'tmpRoCalls ' + __errtext);
  END TRY;  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

END;

  
SELECT COUNT(*) FROM (EXECUTE PROCEDURE getWarrantyRosCompleted()) a -- 201

-- holy shit a zillion with no recording
SELECT cast(startTime AS sql_date), COUNT(*) FROM (
      SELECT id, dialedNumber10,
        right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds, 
		    left(trim(replace(replace(replace(replace(fileName, 'E:\Recordings\','https://mediaserver.cartiva.com:3000/'), '\','/'),'.wav',''), '.cca','')), 100) AS fileName
      FROM sh.extCall a
      LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
        AND a.extension = b.extension
        AND LEFT(trim(a.sipCallId), 15) = b.guid
      WHERE callType = 3
        AND length(TRIM(dialedNumber)) = 12
) x WHERE filename IS NULL 	GROUP BY cast(startTime AS sql_date)	


SELECT n AS fr, n + 29 AS thr
FROM dds.tally
WHERE mod(n,30) = 0
AND n < 1000

-- interesting, but the basic issue remains why the fuck so many with no recordings
SELECT fr, thr, count(*) FROM (
      SELECT id, dialedNumber10,
        right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds, c.fr, c.thr

      FROM sh.extCall a
      LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
        AND a.extension = b.extension
        AND LEFT(trim(a.sipCallId), 15) = b.guid
	  LEFT JOIN (
	    SELECT n AS fr, n + 29 AS thr
        FROM dds.tally
        WHERE mod(n,30) = 0
          AND n < 1000) c on a.durationSeconds BETWEEN c.fr AND c.thr
      WHERE callType = 3
        AND length(TRIM(dialedNumber)) = 12
		AND filename IS NULL 
) x GROUP BY fr, thr	

-- recording volume NOT up to usual level until 4/28/14
SELECT thedate, COUNT(*) 
FROM sh.callCopyRecordings	
GROUP BY thedate 
ORDER BY thedate

-- holy shit a zillion with no recording
-- so, limit the call dates
-- still a lot
SELECT cast(startTime AS sql_date), COUNT(*) FROM (
      SELECT id, dialedNumber10,
        right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds, 
		    left(trim(replace(replace(replace(replace(fileName, 'E:\Recordings\','https://mediaserver.cartiva.com:3000/'), '\','/'),'.wav',''), '.cca','')), 100) AS fileName
      FROM sh.extCall a
      LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
        AND a.extension = b.extension
        AND LEFT(trim(a.sipCallId), 15) = b.guid
      WHERE callType = 3
        AND length(TRIM(dialedNumber)) = 12
		AND cast(startTime AS sql_date) > '04/27/2014'
) x WHERE filename IS NULL 	GROUP BY cast(startTime AS sql_date)	

-- holy shit a zillion with no recording
-- so, limit the call dates
-- still a lot, about 1/2 are less than 20 sec calls
SELECT cast(startTime AS sql_date), 
  sum(case when durationSeconds < 20 then 1 else 0 end) as lessThan20sec,
  COUNT(*) FROM (
      SELECT id, dialedNumber10,
        right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds, 
		    left(trim(replace(replace(replace(replace(fileName, 'E:\Recordings\','https://mediaserver.cartiva.com:3000/'), '\','/'),'.wav',''), '.cca','')), 100) AS fileName
      FROM sh.extCall a
      LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
        AND a.extension = b.extension
        AND LEFT(trim(a.sipCallId), 15) = b.guid
      WHERE callType = 3
        AND length(TRIM(dialedNumber)) = 12
		AND cast(startTime AS sql_date) > '04/27/2014'
) x WHERE filename IS NULL 	GROUP BY cast(startTime AS sql_date)	

-- 7/4
-- of course this IS NOT the horror story i expected (only discrepancies are those
-- with a fcdate of 7/3), ALL the tmpRoPhoneNum... tables are DELETE AND repopoulate
-- the real issue IS with tmpRoCalls, which IS NOT getting the dates updated
select *
FROM (
  select a.ro, a.opendate, a.closedate, a.fcdate
  FROM tmpRoPhoneNumbers a
  group by a.ro, a.opendate, a.closedate, a.fcdate) g
LEFT JOIN (
  SELECT c.ro, d.thedate AS oDate, e.thedate AS cDate, f.thedate AS fcDate
  FROM dds.factRepairOrder c
  INNER JOIN dds.day d on c.opendatekey = d.datekey
  INNER JOIN dds.day e on c.closedatekey = e.datekey
  INNER JOIN dds.day f on c.finalclosedatekey = f.datekey
  WHERE d.thedate > '04/01/2014') h on g.ro = h.ro
WHERE g.fcdate <> h.fcdate  
  AND LEFT(g.ro, 2) = '16'
ORDER BY h.fcdate

-- this IS the horror story i expected
select *
FROM (
  select a.ro, a.opendate, a.closedate, a.fcdate
  FROM tmpRoCalls a
  group by a.ro, a.opendate, a.closedate, a.fcdate) g
LEFT JOIN (
  SELECT c.ro, d.thedate AS oDate, e.thedate AS cDate, f.thedate AS fcDate
  FROM dds.factRepairOrder c
  INNER JOIN dds.day d on c.opendatekey = d.datekey
  INNER JOIN dds.day e on c.closedatekey = e.datekey
  INNER JOIN dds.day f on c.finalclosedatekey = f.datekey
  WHERE d.thedate > '04/01/2014') h on g.ro = h.ro
WHERE g.fcdate <> h.fcdate  
  AND LEFT(g.ro, 2) = '16'
ORDER BY h.fcdate


SELECT COUNT(*) FROM (EXECUTE PROCEDURE getWarrantyRosCompleted()) a -- 177

UPDATE tmpRoCalls
SET closeDate = x.cDate,
    fcDate = x.fcDate
FROM (
  SELECT c.ro, d.thedate AS oDate, e.thedate AS cDate, f.thedate AS fcDate
  FROM dds.factRepairOrder c
  INNER JOIN dds.day d on c.opendatekey = d.datekey
  INNER JOIN dds.day e on c.closedatekey = e.datekey
  INNER JOIN dds.day f on c.finalclosedatekey = f.datekey
  WHERE d.thedate > '04/01/2014') x
WHERE tmpRoCalls.ro = x.ro
  AND (tmpRoCalls.closeDate <> x.cDate or tmpRoCalls.fcDate <> x.fcDate) 
  
  
select name, sql_script
FROM system.storedprocedures
WHERE lower(CAST(sql_script AS sql_longvarchar)) LIKE '%tmprocalls%'

select name, sql_script
FROM system.storedprocedures
WHERE contains(lower(CAST(sql_script AS sql_longvarchar)),'tmprocalls')

-- 7/6
several bad assumptions
see fb CASE 321