create PROCEDURE getRoPhoneCalls (
  ro cichar(9),
  ro cichar(9) output,
  lastContactTS timestamp output,
  lastContactDuration integer output)
BEGIN
/*
EXECUTE PROCEDURE getRoPhoneCalls('16152847');
*/
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output
SELECT top 100 @ro, callStartTS, callDuration
FROM tmpRoCalls
WHERE ro = @ro
ORDER BY callStartTS DESC;
END;  

