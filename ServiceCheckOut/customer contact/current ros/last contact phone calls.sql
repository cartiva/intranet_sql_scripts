/*
1. ADD index on ro to currentRos

lots of larger questions

need to incorporate todayFactRepairOrder

*/

-- this returns way the fuck too may rows for shit LIKE 16151707 (parts truck, customerpay)
-- limiting to outgoing calls works some wonders
SELECT distinct a.ro, d.thedate, b.rocreatedts, a.customer, c.homephone, c.businessphone, c.cellphone,   
  case c.emailvalid
    when true then c.email
    ELSE 'none'
  END AS email, 
  case c.email2valid
    when true then c.email2 
    else 'none'
  end as email2,
  e.starttime, e.endtime, e.extension, e.durationseconds, e.dialednumber, 
  e.dialednumberright7, e.calltype
FROM currentros a
INNER JOIN dds.factrepairorder b on a.ro = b.ro
INNER JOIN dds.dimcustomer c on b.customerkey = c.customerkey
INNER JOIN dds.day d on b.opendatekey = d.datekey
LEFT JOIN sh.extCall e on 
  right(TRIM(c.homephone), 7) = e.dialedNumberRight7
  AND e.starttime > b.rocreatedts
  AND e.calltype = 3
WHERE b.paymenttypekey IN (
  SELECT paymenttypekey
  FROM dds.dimpaymenttype
  WHERE paymenttypecode IN ('c','s','w'))
  
-- most recent call IN date range of current ros  
SELECT dialednumberRight7, starttime, durationSeconds
FROM sh.extcall a
WHERE calltype = 3
  AND CAST(starttime AS sql_date) IN (
    SELECT openDate
	FROM currentros)  
  AND starttime = (
    SELECT MAX(starttime)
	FROM sh.extcall
	WHERE dialednumberRight7 = a.dialedNumberRight7)	

SELECT distinct a.ro, b.rocreatedts, a.customer, 
  c.homephone, c.businessphone, c.cellphone  
-- SELECT *   
FROM currentros a
INNER JOIN dds.factrepairorder b on a.ro = b.ro
INNER JOIN dds.dimcustomer c on b.customerkey = c.customerkey

DROP TABLE #wtf;
SELECT * 
INTO #wtf
FROM (
  SELECT distinct a.ro, b.rocreatedts, a.customer, 
    right(trim(c.homephone), 7) AS homephone, 
	right(trim(c.businessphone), 7) AS businessphone, 
	right(trim(c.cellphone), 7) AS cellphone  
  FROM currentros a
  INNER JOIN dds.factrepairorder b on a.ro = b.ro
  INNER JOIN dds.dimcustomer c on b.customerkey = c.customerkey
union  
  SELECT distinct a.ro, b.rocreatedts, a.customer, 
    right(trim(c.homephone), 7) AS homephone, 
	right(trim(c.businessphone), 7) AS businessphone, 
	right(trim(c.cellphone), 7) AS cellphone  
  FROM currentros a
  INNER JOIN dds.todayfactrepairorder b on a.ro = b.ro
  INNER JOIN dds.dimcustomer c on b.customerkey = c.customerkey  
  WHERE CAST(b.rocreatedts AS sql_Date) = curdate()  ) a
LEFT JOIN ( -- returns a single row per dialed number
  SELECT dialednumberRight7, starttime, durationSeconds
  FROM sh.extcall a
  WHERE calltype = 3 -- outgoing calls
    AND CAST(starttime AS sql_date) IN (
      SELECT openDate
  	  FROM currentros)  
    AND starttime = (
      SELECT MAX(starttime)
  	  FROM sh.extcall
  	  WHERE dialednumberRight7 = a.dialedNumberRight7)) b 
  on b.starttime > a.rocreatedts 
    AND ( -- need to account for HAVING called any 1 of 3 phone numbers
	  a.homephone = b.dialedNumberRight7  
      OR a.businessphone = b.dialednumberright7
      OR a.cellphone = b.dialedNumberRight7);

-- ok, #wtf gives one row per ro/dialedNumber for each current ro

select * FROM #wtf

-- essential lastContact info for single row ROs
SELECT ro, 
  CASE WHEN dialednumberright7 IS NULL THEN '' else 'Phone' END AS lastContactMeans,
  CASE WHEN dialednumberright7 IS NULL THEN CAST(NULL AS sql_timestamp) ELSE starttime END AS lastContactTS,
  CASE WHEN dialednumberright7 IS NULL THEN CAST(NULL AS sql_integer) ELSE durationSeconds END AS lastContactDuration
FROM #wtf
WHERE ro IN (
  SELECT ro
  FROM #wtf
  GROUP BY ro HAVING COUNT(*) = 1)

SELECT *
FROM #wtf
WHERE ro IN (
  SELECT ro
  FROM #wtf
  GROUP BY ro HAVING COUNT(*) > 1)
  
SELECT ro, MAX(starttime) AS starttime
FROM #wtf
WHERE ro IN (
  SELECT ro
  FROM #wtf
  GROUP BY ro HAVING COUNT(*) > 1) 
GROUP BY ro   

-- essential lastContac info for multiple row ROs with most recent starttime
SELECT a.ro, 'Phone', a.starttime, a.durationSeconds
FROM #wtf a
INNER JOIN (
  SELECT ro, MAX(starttime) AS starttime
  FROM #wtf
  WHERE ro IN (
    SELECT ro
    FROM #wtf
    GROUP BY ro HAVING COUNT(*) > 1) 
  GROUP BY ro) b on a.ro = b.ro AND a.starttime = b.starttime

-- bingo, i DO believe
-- essential lastContact info for single row ROs
SELECT ro, 
  CASE WHEN dialednumberright7 IS NULL THEN '' else 'phone' END AS lastContactMeans,
  CASE WHEN dialednumberright7 IS NULL THEN CAST(NULL AS sql_timestamp) ELSE starttime END AS lastContactTS,
  CASE WHEN dialednumberright7 IS NULL THEN CAST(NULL AS sql_integer) ELSE durationSeconds END AS lastContactDuration
FROM #wtf
WHERE ro IN (
  SELECT ro
  FROM #wtf
  GROUP BY ro HAVING COUNT(*) = 1)
UNION 
-- essential lastContac info for multiple row ROs with most recent starttime
SELECT a.ro, 'phone', a.starttime, a.durationSeconds
FROM #wtf a
INNER JOIN (
  SELECT ro, MAX(starttime) AS starttime
  FROM #wtf
  WHERE ro IN (
    SELECT ro
    FROM #wtf
    GROUP BY ro HAVING COUNT(*) > 1) 
  GROUP BY ro) b on a.ro = b.ro AND a.starttime = b.starttime  

  
CREATE TABLE tmpCurrentRoLastContact (
  ro cichar(9),
  lastContactMeans cichar(12),
  lastContactTS timestamp,
  lastContactDuration integer,
  constraint pk primary key (ro));
 
DELETE FROM tmpCurrentRoLastContact;   
INSERT INTO tmpCurrentRoLastContact
-- bingo, i DO believe
-- essential lastContact info for single row ROs
SELECT ro, 
  CASE WHEN dialednumberright7 IS NULL THEN '' else 'phone' END AS lastContactMeans,
  CASE WHEN dialednumberright7 IS NULL THEN CAST(NULL AS sql_timestamp) ELSE starttime END AS lastContactTS,
  CASE WHEN dialednumberright7 IS NULL THEN CAST(NULL AS sql_integer) ELSE durationSeconds END AS lastContactDuration
FROM #wtf
WHERE ro IN (
  SELECT ro
  FROM #wtf
  GROUP BY ro HAVING COUNT(*) = 1)
UNION 
-- essential lastContac info for multiple row ROs with most recent starttime
SELECT a.ro, 'phone', a.starttime, a.durationSeconds
FROM #wtf a
INNER JOIN (
  SELECT ro, MAX(starttime) AS starttime
  FROM #wtf
  WHERE ro IN (
    SELECT ro
    FROM #wtf
    GROUP BY ro HAVING COUNT(*) > 1) 
  GROUP BY ro) b on a.ro = b.ro AND a.starttime = b.starttime;	
