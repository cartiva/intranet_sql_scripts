UPDATE extCall
SET dialedNumber10 = right(TRIM(dialedNumber), 10)


SELECT length(dialedNumber), COUNT(*)
FROM extCall
GROUP BY length(dialedNumber)

SELECT *
FROM extCall
WHERE dialedNumber = ''



SELECT a.sipCallID, a.startTime, a.extension, a.durationSeconds, b.*
FROM sh.extCall a
LEFT JOIN callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
  AND a.extension = b.extension
  AND LEFT(trim(a.sipCallId), 15) = b.guid
WHERE sh.calltype = 3
  AND CAST(startTime AS sql_date) BETWEEN curdate() - 4 AND curdate() -1
  AND durationSeconds > 1
--  AND b.ident IS NULL
ORDER BY a.extension 



ro 16156100
on the page, shows 2 calls on 6/20
1 with no time
1 with no recording found
why that?

SELECT *
FROM tmpRoCalls
WHERE ro = '16156100'
  AND calldate = '06/20/2014'
shows 2 calls:
  duration 7 seconds no recording: id 1842873
  duration 3 seconds: id: 1844879 https://mediaserver.cartiva.com:3000/20140620/5893/5893-14-38-08
SELECT *
FROM sh.extcall
WHERE id IN (1842873, 1844879)    
ok

An issue IS that a row IS being generated IN sh.callCopyRecordings for files
with an etxtension of .cca