EXECUTE PROCEDURE sp_zaptable('writerlandingpage');
INSERT INTO writerlandingpage
SELECT m.sundaytosaturdayweek, 0, fullname, m.writerid, 'Cashiering',
  sun, mon, tue, wed, thu, fri, sat, total, eeusername, 1  
--SELECT *
FROM ( -- sunday to saturday
  SELECT sundaytosaturdayweek, z.writerid, w.fullname, w.eeusername, 
    max(CASE WHEN dayname = 'sunday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS sun,
    max(CASE WHEN dayname = 'monday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS mon,
    max(CASE WHEN dayname = 'tuesday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS tue,
    max(CASE WHEN dayname = 'wednesday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS wed,
    max(CASE WHEN dayname = 'thursday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS thu,
    max(CASE WHEN dayname = 'friday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS fri,
    max(CASE WHEN dayname = 'saturday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS sat
  FROM (
    SELECT sundaytosaturdayweek, dayname, writerid, COUNT(*) AS Total,  
      SUM(CASE WHEN y.storecode IS NULL THEN 0 ELSE 1 END) AS WriterCashiered
    FROM (
      SELECT b.sundaytosaturdayweek, b.thedate, dayname, a.ro, a.writerid, f.gquser
      FROM dds.factro a
      INNER JOIN 
        dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN dds.factroline c ON a.ro = c.ro    
      INNER JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# -- cashiered ros only
      INNER JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
      INNER JOIN ServiceWriters g ON a.storecode = g.storecode
        AND a.writerid = g.writernumber
        AND NOT EXISTS (
          SELECT 1 
          FROM dds.factroline
          WHERE ro = a.ro
            AND paytype IN ('w','i'))
        AND NOT EXISTS (
          SELECT 1
          FROM dds.factroline
          WHERE ro = a.ro
            AND servicetype <> 'mr')      
        AND a.void = false      
      GROUP BY b.sundaytosaturdayweek, b.thedate, b.dayname, a.ro, a.writerid, f.gquser) x
    LEFT JOIN servicewriters y ON x.gquser = y.dtUserName 
    GROUP BY sundaytosaturdayweek, dayname, writerid) z 
  LEFT JOIN servicewriters w ON z.writerid = w.writernumber 
  GROUP BY sundaytosaturdayweek, z.writerid, w.fullname, w.eeusername) m
LEFT JOIN ( -- total
  SELECT sundaytosaturdayweek, writerid,
    trim(TRIM(CAST(sum(writercashiered) AS sql_char)) + ' / ' + TRIM(CAST(sum(total) AS sql_char))) AS total
  FROM (
    SELECT sundaytosaturdayweek, dayname, writerid, COUNT(*) AS Total,  
      SUM(CASE WHEN y.storecode IS NULL THEN 0 ELSE 1 END) AS WriterCashiered
    FROM (
      SELECT b.sundaytosaturdayweek, b.thedate, dayname, a.ro, a.writerid, f.gquser
      FROM dds.factro a
      INNER JOIN 
        dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN dds.factroline c ON a.ro = c.ro    
      INNER JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# -- cashiered ros only
      INNER JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
      INNER JOIN ServiceWriters g ON a.storecode = g.storecode
        AND a.writerid = g.writernumber
        AND NOT EXISTS (
          SELECT 1 
          FROM dds.factroline
          WHERE ro = a.ro
            AND paytype IN ('w','i'))
        AND NOT EXISTS (
          SELECT 1
          FROM dds.factroline
          WHERE ro = a.ro
            AND servicetype <> 'mr')      
        AND a.void = false      
      GROUP BY b.sundaytosaturdayweek, b.thedate, b.dayname, a.ro, a.writerid, f.gquser) x
    LEFT JOIN servicewriters y ON x.gquser = y.dtUserName 
    GROUP BY sundaytosaturdayweek, dayname, writerid) e
  GROUP BY sundaytosaturdayweek, writerid ) n ON m.writerid = n.writerid AND m.sundaytosaturdayweek = n.sundaytosaturdayweek;
  
--EXECUTE PROCEDURE sp_zaptable('AgedROs');
DELETE FROM agedros;
INSERT INTO AgedROs 
SELECT f.eeUsername, a.ro, b.thedate, a.customername, 
  CASE d.ptrsts
    WHEN '1' THEN 'Open'
    WHEN '2' THEN 'In-Process'
    WHEN '3' THEN 'Appr-PD'
    WHEN '4' THEN 'Cashier'
    WHEN '5' THEN 'Cashier/D'
    WHEN '6' THEN 'Pre-Inv'
    WHEN '7' THEN 'Odom Rq'
    WHEN '9' THEN 'Parts-A'
    WHEN 'L' THEN 'G/L Error'
    WHEN 'P' THEN 'PI Rq'
    ELSE 'WTF'
  END AS status, 
  a.vin, 
  trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle
FROM dds.factro a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
INNER JOIN dds.day c ON a.finalclosedatekey = c.datekey
  AND c.datetype <> 'date'
INNER JOIN dds.stgArkonaPDPPHDR d ON a.ro = d.ptdoc# -- ? eliminate those NOT IN pdpphdr
LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
INNER JOIN ServiceWriters f ON a.storecode = f.storecode
  AND a.writerid = f.writernumber
  AND void = false
  AND curdate() - b.thedate > 7; 

DELETE FROM writerlandingpage WHERE metric = 'Open Aged ROs';   
INSERT INTO writerlandingpage  
SELECT sundaytosaturdayweek, 0, fullname, writerid, 'Open Aged ROs',
  max(CASE WHEN dayname = 'Sunday' THEN howmany END) AS Sunday,
  max(CASE WHEN dayname = 'Monday' THEN howmany END) AS Monday,
  max(CASE WHEN dayname = 'Tuesday' THEN howmany END) AS Tuesday,
  max(CASE WHEN dayname = 'Wednesday' THEN howmany END) AS Wednesday,
  max(CASE WHEN dayname = 'Thursday' THEN howmany END) AS Thursday,
  max(CASE WHEN dayname = 'Friday' THEN howmany END) AS Friday,
  max(CASE WHEN dayname = 'Saturday' THEN howmany END) AS Saturday,
  '-', eeUserName, 2
FROM (       
  SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, b.writerid, b.fullname, 
    b.eeUserName, cast(COUNT(*) AS sql_char) AS howmany
  FROM (  
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
  LEFT JOIN (
    SELECT b.thedate AS opendate, c.thedate AS closedate, a.storecode, a.writerid, 
      a.ro, d.fullname, d.eeUserName
    FROM dds.factro a
    LEFT JOIN dds.day b ON a.opendatekey = b.datekey
    LEFT JOIN dds.day c ON a.finalclosedatekey = c.datekey
    INNER JOIN ServiceWriters d ON a.storecode = d.storecode
      AND a.writerid = d.writernumber
    WHERE void = false
--    AND writerid = '402'
      AND b.thedate > '01/31/2012'
      AND a.storecode = 'RY1') b ON b.opendate < a.thedate - 7 AND b.closedate > a.thedate    
  GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, b.writerid, b.fullname, b.eeUserName) x   
GROUP BY sundaytosaturdayweek, fullname, writerid, eeUserName;

EXECUTE PROCEDURE sp_zaptable('WarrantyROs');
INSERT INTO WarrantyROs
SELECT f.eeUserName, a.ro, b.thedate AS CloseDate, a.customername AS Customer, 
  CASE 
    WHEN c.bnphon  = '0' THEN 'No Phone'
    WHEN c.bnphon  = '' THEN 'No Phone'
    WHEN c.bnphon IS NULL THEN 'No Phone'
    ELSE trim(c.bnphon)
  END AS HomePhone, 
  CASE 
    WHEN c.bnbphn  = '0' THEN 'No Phone'
    WHEN c.bnbphn  = '' THEN 'No Phone'
    WHEN c.bnbphn IS NULL THEN 'No Phone'
    ELSE trim(c.bnbphn)
  END  AS WorkPhone, a.vin, TRIM(e.immake) + ' ' + coalesce(TRIM(e.immodl),'') AS vehicle,
  false as FollowUpComplete
FROM dds.factro a
INNER JOIN -- cashiered ros only
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN curdate() - 60 AND curdate()    
LEFT JOIN dds.stgArkonaBOPNAME c ON a.storecode = c.bnco#
  AND a.customerkey = c.bnkey    
LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
INNER JOIN ServiceWriters f ON a.storecode = f.storecode 
  AND a.writerid = f.writernumber
  AND EXISTS (
    SELECT 1 
    FROM dds.factroline
    WHERE ro = a.ro
      AND paytype = 'w')
  AND a.void = false;
  


INSERT INTO writerlandingpage
SELECT sundaytosaturdayweek, 0, fullname, writernumber,'Open Warranty Calls',
  sun,mon,tue,wed,thu,fri,sat,'-',eeusername, 3
FROM (  
  SELECT sundaytosaturdayweek, 0, fullname, writernumber,
    trim(cast(SUM(CASE WHEN dayname = 'sunday' THEN howmany END) AS sql_char)) AS sun,
    trim(cast(SUM(CASE WHEN dayname = 'monday' THEN howmany END) AS sql_char)) AS mon,
    trim(cast(SUM(CASE WHEN dayname = 'tuesday' THEN howmany END) AS sql_char)) AS tue,
    trim(cast(SUM(CASE WHEN dayname = 'wednesday' THEN howmany END) AS sql_char)) AS wed,
    trim(cast(SUM(CASE WHEN dayname = 'thursday' THEN howmany END) AS sql_char)) AS thu,
    trim(cast(SUM(CASE WHEN dayname = 'friday' THEN howmany END) AS sql_char)) AS fri,
    trim(cast(SUM(CASE WHEN dayname = 'saturday' THEN howmany END) AS sql_char)) AS sat,
    eeusername, 3
  FROM (  
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, b.writernumber, 
      b.fullname, b.eeUserName, COUNT(*) AS howmany
    FROM (  
      SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
      FROM dds.day a
      WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
    LEFT JOIN (  
      SELECT b.writernumber, b.fullname, b.eeusername, a.ro, a.closedate
      FROM warrantyros a
      LEFT JOIN servicewriters b ON a.eeusername = b.eeusername
      WHERE a.followupcomplete = false) b ON b.closedate < a.thedate
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, b.writernumber, 
      b.fullname, b.eeUserName) c  
  GROUP BY sundaytosaturdayweek, writernumber, fullname, eeusername) a;  

INSERT INTO writerlandingpage  
SELECT a.sundaytosaturdayweek, 0, fullname, a.writerid, 'Avg Hours/RO',
  trim(cast(a.sunday AS sql_char)),trim(cast(a.monday AS sql_char)),trim(cast(a.tuesday AS sql_char)),
  trim(cast(a.wednesday AS sql_char)),trim(cast(a.thursday AS sql_char)),trim(cast(a.friday AS sql_char)), 
  trim(cast(a.saturday AS sql_char)),trim(cast(b.total AS sql_char)),
  eeusername, 4  
--SELECT a.*, b.total 
FROM(
  SELECT sundaytosaturdayweek, writerid, fullname, eeusername, 
    max(CASE WHEN dayname = 'Sunday' THEN AvgPerRO END) AS Sunday,
    max(CASE WHEN dayname = 'Monday' THEN AvgPerRO END) AS Monday,
    max(CASE WHEN dayname = 'Tuesday' THEN AvgPerRO END) AS Tuesday,
    max(CASE WHEN dayname = 'Wednesday' THEN AvgPerRO END) AS Wednesday,
    max(CASE WHEN dayname = 'Thursday' THEN AvgPerRO END) AS Thursday,
    max(CASE WHEN dayname = 'Friday' THEN AvgPerRO END) AS Friday,
    max(CASE WHEN dayname = 'Saturday' THEN AvgPerRO END) AS Saturday
  FROM ( -- sunday - saturday
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
  LEFT JOIN (
    SELECT finalclosedate, writerid, fullname, eeusername,
      CASE howmany
        WHEN 0 THEN '0'
        ELSE cast(round(flaghours/howmany, 1) AS sql_char)
      END AS AvgPerRO
    FROM (
      SELECT finalclosedate, writerid, fullname, eeusername, COUNT(*) AS howmany, SUM(flaghours) AS flaghours
      FROM (
        SELECT b.thedate AS finalclosedate, a.writerid, a.ro, d.fullname, eeusername,
          SUM(c.lineflaghours) AS flaghours
        FROM dds.factro a
        INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
            AND b.datetype = 'date'
            AND b.thedate BETWEEN '03/03/2013' AND curdate()
        INNER JOIN dds.factroline c ON a.ro = c.ro
          AND c.lineflaghours > 0 -- only ros with flag hours 
        INNER JOIN ServiceWriters d ON a.storecode = d.storecode
          AND a.writerid = d.writernumber    
        WHERE a.void = false   
        GROUP BY b.thedate, a.writerid, a.ro, d.fullname, eeusername) x
      GROUP by finalclosedate, writerid,fullname, eeusername) y) b ON a.thedate = b.finalclosedate
  GROUP BY sundaytosaturdayweek, writerid, fullname, eeusername) a    
LEFT JOIN ( -- total
  SELECT sundaytosaturdayweek, writerid, round(SUM(flaghours)/SUM(howmany), 1) AS total
  FROM (
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
  LEFT JOIN (  
    SELECT finalclosedate, writerid, sum(flaghours) as flaghours, COUNT(*) AS howmany
    FROM (
      SELECT b.thedate AS finalclosedate, a.writerid, a.ro,
        SUM(c.lineflaghours) AS flaghours
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN dds.factroline c ON a.ro = c.ro
        AND c.lineflaghours > 0 -- only ros with flag hours 
      INNER JOIN ServiceWriters d ON a.storecode = d.storecode
        AND a.writerid = d.writernumber    
      WHERE a.void = false   
      GROUP BY b.thedate, a.writerid, a.ro) a
    GROUP BY finalclosedate, writerid) b ON a.thedate = b.finalclosedate
  GROUP BY sundaytosaturdayweek, writerid)b ON a.sundaytosaturdayweek = b.sundaytosaturdayweek
    AND a.writerid = b.writerid;  
    
INSERT INTO writerlandingpage
SELECT sundaytosaturdayweek, 0, fullname, writerid, 'Labor Sales',
  trim(cast(sunLS AS sql_char)),trim(cast(monLS AS sql_char)),trim(cast(tueLS AS sql_char)),
  trim(cast(wedLS AS sql_char)),trim(cast(thuLS AS sql_char)),trim(cast(friLS AS sql_char)),
  trim(cast(satLS AS sql_char)),
  trim(cast(sunLS+monLS+tueLS+wedLS+thuLS+friLS+satLS AS sql_char)), eeusername, 5     
FROM (
  SELECT sundaytosaturdayweek, writerid, fullname, eeusername, 
    coalesce(MAX(CASE WHEN dayname = 'sunday' THEN laborsales END), 0) AS sunLS,
    coalesce(MAX(CASE WHEN dayname = 'Monday' THEN laborsales END), 0) AS monLS,
    coalesce(MAX(CASE WHEN dayname = 'Tuesday' THEN laborsales END), 0) AS tueLS,
    coalesce(MAX(CASE WHEN dayname = 'Wednesday' THEN laborsales END), 0) AS wedLS,
    coalesce(MAX(CASE WHEN dayname = 'Thursday' THEN laborsales END), 0) AS thuLS,
    coalesce(MAX(CASE WHEN dayname = 'Friday' THEN laborsales END), 0) AS friLS,
    coalesce(MAX(CASE WHEN dayname = 'Saturday' THEN laborsales END), 0) AS satLS   
  FROM (  
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
  LEFT JOIN (  
    SELECT finalclosedate, writerid, fullname, eeusername, round(cast(abs(sum(laborsales)) AS sql_double),0) AS laborsales
    FROM (
      SELECT b.thedate AS finalclosedate, a.writerid, a.ro, a.laborsales, d.fullname, d.eeusername
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN dds.factroline c ON a.ro = c.ro
        AND c.lineflaghours > 0 -- only ros with flag hours 
      INNER JOIN ServiceWriters d ON a.storecode = d.storecode
        AND a.writerid = d.writernumber    
      WHERE a.void = false   
      GROUP BY b.thedate, a.writerid, a.ro, a.laborsales, d.fullname, d.eeusername) x
    GROUP by finalclosedate, writerid, fullname, eeusername) b ON a.thedate = b.finalclosedate
  WHERE fullname IS NOT NULL     
  GROUP BY sundaytosaturdayweek, writerid, fullname, eeusername) c;
  
INSERT INTO writerlandingpage
SELECT a.sundaytosaturdayweek, 0, fullname, a.writerid, 'RO''s Opened',
  sun, mon, tue, wed, thu, fri, sat, total, eeusername, 6 
FROM ( -- sunday to saturday
  SELECT sundaytosaturdayweek, writerid, fullname, eeusername,
    MAX(CASE WHEN dayname = 'sunday' THEN opened END) AS sun,
    MAX(CASE WHEN dayname = 'monday' THEN opened END) AS mon,
    MAX(CASE WHEN dayname = 'tuesday' THEN opened END) AS tue,
    MAX(CASE WHEN dayname = 'wednesday' THEN opened END) AS wed,
    MAX(CASE WHEN dayname = 'thursday' THEN opened END) AS thu,
    MAX(CASE WHEN dayname = 'friday' THEN opened END) AS fri,
    MAX(CASE WHEN dayname = 'saturday' THEN opened END) AS sat
  FROM (  
    SELECT sundaytosaturdayweek, dayname, fullname, eeusername, a.writerid,  
      trim(cast(coalesce(a.opened, 0) AS sql_char)) AS opened
    FROM (
      SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
      FROM dds.day a
      WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) z  
    LEFT JOIN ( -- openedros
      SELECT b.thedate, a.writerid, COUNT(*) AS opened
      FROM dds.factro a
      INNER JOIN dds.day b ON a.opendatekey = b.datekey
        AND b.datetype = 'date'
        AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false
      GROUP BY b.thedate, a.writerid) a ON a.thedate = z.thedate 
    LEFT JOIN servicewriters c ON a.writerid = c.writernumber) x 
  GROUP BY sundaytosaturdayweek, fullname, eeusername, writerid) a
LEFT JOIN (  
  SELECT sundaytosaturdayweek, a.writerid,  
    trim(cast(SUM(opened) AS sql_char)) AS total
  FROM (
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) z  
  LEFT JOIN ( -- openedros
    SELECT b.thedate, a.writerid, COUNT(*) AS opened
    FROM dds.factro a
    INNER JOIN dds.day b ON a.opendatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate BETWEEN '03/03/2013' AND curdate()
    INNER JOIN ServiceWriters c ON a.storecode = c.storecode
      AND a.writerid = c.writernumber 
    WHERE a.void = false
    GROUP BY b.thedate, a.writerid) a ON a.thedate = z.thedate 
  GROUP BY sundaytosaturdayweek, a.writerid) b ON a.sundaytosaturdayweek = b.sundaytosaturdayweek
  AND a.writerid = b.writerid;

INSERT INTO writerlandingpage
SELECT r.sundaytosaturdayweek, 0, fullname, r.writerid, 'Inspections Requested',
  sun, mon, tue, wed, thu, fri, sat, total, eeusername, 7  
FROM ( -- sunday to saturday
  SELECT sundaytosaturdayweek, writerid, fullname, eeusername,
    MAX(CASE WHEN dayname = 'sunday' THEN inspections + ' / ' + closed END) AS sun,
    MAX(CASE WHEN dayname = 'monday' THEN inspections + ' / ' + closed END) AS mon,
    MAX(CASE WHEN dayname = 'tuesday' THEN inspections + ' / ' + closed END) AS tue,
    MAX(CASE WHEN dayname = 'wednesday' THEN inspections + ' / ' + closed END) AS wed,
    MAX(CASE WHEN dayname = 'thursday' THEN inspections + ' / ' + closed END) AS thu,
    MAX(CASE WHEN dayname = 'friday' THEN inspections + ' / ' + closed END) AS fri,
    MAX(CASE WHEN dayname = 'saturday' THEN inspections + ' / ' + closed END) AS sat
  FROM (  
    SELECT sundaytosaturdayweek, dayname, fullname, eeusername, a.writerid,  
      trim(cast(coalesce(b.inspections, 0) AS sql_char)) AS inspections, 
      trim(cast(coalesce(a.closed, 0) AS sql_char)) AS closed
    FROM (
      SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
      FROM dds.day a
      WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) z  
    LEFT JOIN ( -- closed ros
      SELECT b.thedate, a.writerid, COUNT(*) AS closed
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
        AND b.datetype = 'date'
        AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false
      GROUP BY b.thedate, a.writerid) a ON a.thedate = z.thedate 
    LEFT join ( -- closed ros with inspection lines
      SELECT b.thedate, a.writerid, COUNT(*) AS inspections
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false 
        AND EXISTS (
          SELECT 1
          FROM dds.factroline
          WHERE ro = a.ro
          AND opcode in ('23I','23D', '23IW'))  
      GROUP BY b.thedate, a.writerid) b ON a.thedate = b.thedate AND a.writerid = b.writerid  
    LEFT JOIN servicewriters c ON a.writerid = c.writernumber) x 
  GROUP BY sundaytosaturdayweek, fullname, eeusername, writerid) r
LEFT JOIN ( -- total
  SELECT sundaytosaturdayweek, writerid, trim(cast(SUM(inspections) AS sql_char)) + ' / ' + trim(cast(SUM(closed) AS sql_char)) AS total
  FROM ( 
    SELECT sundaytosaturdayweek, dayname, a.writerid, fullname, eeusername, 
      coalesce(b.inspections, 0) AS inspections, 
      coalesce(a.closed, 0) AS closed
    FROM (
      SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
      FROM dds.day a
      WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) z  
    LEFT JOIN ( -- closed ros
      SELECT b.thedate, a.writerid, COUNT(*) AS closed
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
        AND b.datetype = 'date'
        AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false
      GROUP BY b.thedate, a.writerid) a ON a.thedate = z.thedate 
    LEFT join ( -- closed ros with inspection lines
      SELECT b.thedate, a.writerid, COUNT(*) AS inspections
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false 
        AND EXISTS (
          SELECT 1
          FROM dds.factroline
          WHERE ro = a.ro
          AND opcode in ('23I','23D', '23IW'))  
      GROUP BY b.thedate, a.writerid) b ON a.thedate = b.thedate AND a.writerid = b.writerid  
    LEFT JOIN servicewriters c ON a.writerid = c.writernumber) x
    GROUP BY sundaytosaturdayweek, writerid) s ON r.writerid = s.writerid
      AND r.sundaytosaturdayweek = s.sundaytosaturdayweek;  
  
  
  
  
          
