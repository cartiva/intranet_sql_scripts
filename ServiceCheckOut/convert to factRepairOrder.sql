50conversion

ALTER TABLE AgedRos
ALTER COLUMN customer customer cichar(50)
ALTER COLUMN rostatus RoStatus cichar(24);

ALTER TABLE WarrantyROs
ALTER COLUMN customer Customer cichar(50)
ALTER COLUMN WorkPhone WorkPhone cichar(20);

ALTER TABLE RY2ROsForCalls
ALTER COLUMN Customer Customer cichar(50)
ALTER COLUMN WorkPhone WorkPhone cichar(20);

DROP TABLE factRoLineToday;
DROP TABLE factROToday;
DROP TABLE tmpFactRepairOrder;
DROP TABLE tmpPDPPDET;
DROP TABLE tmpPDPPHDR;
DROP TABLE tmpRO;
DROP TABLE tmpRoKeys;
DROP TABLE tmpROLine;
DROP TABLE tmpRoLineKeys;
DROP TABLE tmpSDPRDET;
DROP TABLE tmpSDPRHDR;
DROP TABLE tmpSDPRTXTaToday;
DROP TABLE tmpSDPRTXTToday;
DROP TABLE xfmROToday;
DROP TABLE zRODetailToday;
DROP TABLE zROHeaderToday;

-- this was the sp that processed factROToday/LineToday 
EXECUTE PROCEDURE sp_RenameDDObject('stgRO', 'zUnused_stgRO', 10, 0);

-- ALTER the output parameter defs IN these stored procs to accomodate 
--   the new field defs, eg customer cichar(50)
alter PROCEDURE GetAgedROs
alter PROCEDURE GetWarrantyROs
alter PROCEDURE GetROToUpdate
alter PROCEDURE GetRY2ROsForCalls
alter PROCEDURE GetRY2ROToUpdate
alter PROCEDURE GetWarrantyROToUpdate

