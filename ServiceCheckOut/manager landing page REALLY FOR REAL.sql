/*    
SELECT * FROM serviceWriterMetricData  

SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, 
    a.fullname, a.writernumber, left(a.metric, 20) AS metric, a.seq, a.additive, 
    b.today, b.thisweek, todaydisplay, thisweekdisplay
FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
  SELECT *
  FROM dds.day a, servicewriters b, servicewritermetrics c
  WHERE a.SundayToSaturdayWeek  = (
    SELECT SundayToSaturdayWeek 
    FROM dds.day
    WHERE thedate = curdate()-7)) a
LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
  AND a.eeusername = b.eeusername 
  AND a.metric = b.metric    
AND a.additive = false  
    

cashiering = rosCashieredByWriter '/' rosCashiered
Avg Hours/RO = flagHours/rosClosedWithFlagHours
Inspections Requested = rosClosedWithInspection '/' rosClosed    

types of metrics
      purely additive (coalesce NULL to 0) eg ros closed, ros opened, serviceWriterMetrics.additive = true
      OPEN aged,  OPEN warranty  running total, never NULL, additive daily, but NOT weekly   
      CSI: running total, never NULL 
      ratios :: comprised of 2 OR more purely additive metrics
      
        
SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive,
  CASE a.additive
    WHEN true THEN SUM(coalesce(today, 0)) 
    ELSE   
      CASE
        WHEN a.metric IN ('Open Aged ROs','Open Warranty Calls') THEN SUM(coalesce(today, 0)) 
        ELSE 0
      END
  END 

FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
  SELECT *
  FROM dds.day a, servicewriters b, servicewritermetrics c
  WHERE a.SundayToSaturdayWeek  = (
    SELECT SundayToSaturdayWeek 
    FROM dds.day
    WHERE thedate = curdate()-7)
      AND c.metric <> 'CSI') a
LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
  AND a.eeusername = b.eeusername 
  AND a.metric = b.metric    
--WHERE a.additive = false  
GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive
ORDER BY a.thedate, a.seq
*/
-- taking a fuck it DO multiple unioned queries approach
-- OPEN aged, OPEN warranty can NOT DO SUM for weekly total

DECLARE @date date;
@date = curdate();
--DELETE FROM managerlandingpage;
INSERT INTO managerlandingpage
SELECT e.sundaytosaturdayweek, e.metric, 
  max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
  max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
  max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
  max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
  max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
  max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
  max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
  cast(cast(SUM(e.today) AS sql_double) AS sql_char) AS thisweek,
  e.seq
FROM (  
  SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
    SUM(coalesce(today, 0)) AS today
  FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
    SELECT *
    FROM dds.day a, servicewriters b, servicewritermetrics c
    WHERE a.SundayToSaturdayWeek  = (
      SELECT SundayToSaturdayWeek 
      FROM dds.day
      WHERE thedate = @date)
        AND c.metric IN ('Labor Sales','ROs Opened','ROs Closed','Walk Arounds')) a
  LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
    AND a.eeusername = b.eeusername 
    AND a.metric = b.metric    
  GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
GROUP BY e.sundaytosaturdayweek, e.metric, e.seq  


UNION 
-- the problem IS that this number IS NOT additive accross days
-- it should be the most recent value
SELECT e.sundaytosaturdayweek, e.metric, 
  max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
  max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
  max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
  max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
  max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
  max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
  max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
  cast(cast(SUM(e.today) AS sql_double) AS sql_char) AS thisweek,
  e.seq
FROM (  
  SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
    SUM(coalesce(today, 0)) AS today
  FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
    SELECT *
    FROM dds.day a, servicewriters b, servicewritermetrics c
    WHERE a.SundayToSaturdayWeek  = (
      SELECT SundayToSaturdayWeek 
      FROM dds.day
      WHERE thedate = @date)
        AND c.metric IN ('Open Aged ROs','Open Warranty Calls')) a
  LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
    AND a.eeusername = b.eeusername 
    AND a.metric = b.metric    
  GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
GROUP BY e.sundaytosaturdayweek, e.metric, e.seq  
 
union


SELECT x.SundayToSaturdayWeek, 'Cashiering', trim(x.sun) + ' / ' + y.sun, trim(x.mon) + ' / ' + y.mon,
  trim(x.tue) + ' / ' + y.tue, trim(x.wed) + ' / ' + y.wed, trim(x.thu) + ' / ' + y.thu,
  trim(x.fri) + ' / ' + y.fri, trim(x.sat) + ' / ' + y.sat, trim(x.thisweek) + ' / ' + y.thisweek,1 AS seq
FROM (
  SELECT e.sundaytosaturdayweek, e.metric, 
    max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
    max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
    max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
    max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
    max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
    max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
    max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
    cast(SUM(cast(e.today AS sql_double)) AS sql_char) AS thisweek,
    e.seq
  FROM (  
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
      SUM(coalesce(today, 0)) AS today
    FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
      SELECT *
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
        SELECT SundayToSaturdayWeek 
        FROM dds.day
        WHERE thedate = @date)
          AND c.metric = 'rosCashieredByWriter') a
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric    
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x 
  LEFT JOIN (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      cast(SUM(cast(e.today AS sql_double)) as sql_char)AS thisweek,
      e.seq
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
            AND c.metric  = 'rosCashiered') a
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek 

UNION


SELECT x.SundayToSaturdayWeek, 'Avg Hours/RO', 
  CASE y.sun WHEN 0 then '0' ELSE cast(round(x.sun/y.sun, 1) AS sql_char) END, 
  CASE y.mon WHEN 0 then '0' ELSE cast(round(x.mon/y.mon, 1) AS sql_char) END, 
  CASE y.tue WHEN 0 then '0' ELSE cast(round(x.tue/y.tue, 1) AS sql_char) END, 
  CASE y.wed WHEN 0 then '0' ELSE cast(round(x.wed/y.wed, 1) AS sql_char) END, 
  CASE y.thu WHEN 0 then '0' ELSE cast(round(x.thu/y.thu, 1) AS sql_char) END, 
  CASE y.fri WHEN 0 then '0' ELSE cast(round(x.fri/y.fri, 1) AS sql_char) END, 
  CASE y.sat WHEN 0 then '0' ELSE cast(round(x.sat/y.sat, 1) AS sql_char) END, 
  CASE y.thisweek WHEN 0 then '0' ELSE cast(round(x.thisweek/y.thisweek, 1) AS sql_char) END, 
  4 AS seq
FROM (
  SELECT e.sundaytosaturdayweek, e.metric, 
    max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
    max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
    max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
    max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
    max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
    max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
    max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
    SUM(cast(e.today AS sql_double)) AS thisweek,
    e.seq
  FROM ( 
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
      SUM(coalesce(today, 0)) AS today
    FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
      SELECT *
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
        SELECT SundayToSaturdayWeek 
        FROM dds.day
        WHERE thedate = @date)
          AND c.metric = 'flagHours') a
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric    
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x  
LEFT JOIN (
  SELECT e.sundaytosaturdayweek, e.metric, 
    max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
    max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
    max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
    max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
    max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
    max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
    max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
    SUM(cast(e.today AS sql_double)) AS thisweek,
    e.seq
  FROM ( 
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
      SUM(coalesce(today, 0)) AS today
    FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
      SELECT *
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
        SELECT SundayToSaturdayWeek 
        FROM dds.day
        WHERE thedate = @date)
          AND c.metric = 'rosClosedWithFlagHours') a
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric    
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq)  y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek 
  
UNION   
  
SELECT x.SundayToSaturdayWeek, 'Inspections Requested', trim(x.sun) + ' / ' + y.sun, trim(x.mon) + ' / ' + y.mon,
  trim(x.tue) + ' / ' + y.tue, trim(x.wed) + ' / ' + y.wed, trim(x.thu) + ' / ' + y.thu,
  trim(x.fri) + ' / ' + y.fri, trim(x.sat) + ' / ' + y.sat, trim(x.thisweek) + ' / ' + y.thisweek,
  8 AS seq
FROM (
  SELECT e.sundaytosaturdayweek, e.metric, 
    max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
    max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
    max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
    max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
    max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
    max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
    max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
    cast(SUM(cast(e.today AS sql_double)) AS sql_char) AS thisweek,
    e.seq
  FROM (  
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
      SUM(coalesce(today, 0)) AS today
    FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
      SELECT *
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
        SELECT SundayToSaturdayWeek 
        FROM dds.day
        WHERE thedate = @date)
          AND c.metric = 'rosClosedWithInspectionLine') a
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric    
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x 
  LEFT JOIN (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      cast(SUM(cast(e.today AS sql_double)) as sql_char)AS thisweek,
      e.seq
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
            AND c.metric  = 'ROs Closed') a
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek 

  
/*
need to get to number of checkouts per day
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( 
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = curdate() -7)
            AND c.metric  = 'walk arounds') a
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq 
*/