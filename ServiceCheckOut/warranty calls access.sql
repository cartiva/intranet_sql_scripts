SELECT b.*
FROM tpemployees a
LEFT JOIN employeeAppAuthorization b on a.username = b.username
WHERE lastname = 'hinrichs'

SELECT b.*
FROM tpemployees a
LEFT JOIN employeeAppAuthorization b on a.username = b.username
WHERE lastname = 'espelund'
  AND appcode = 'sco'
  
INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('tgagnon@rydellcars.com','ServiceCheckOut',300,'sco','manager','dropdown');

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('tgagnon@rydellcars.com','ServiceCheckOut',300,'sco','manager','warranty calls');

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('tgagnon@rydellcars.com','ServiceCheckOut',300,'sco','manager','completed warranty calls');

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('jkilmer@rydellcars.com','ServiceCheckOut',300,'sco','manager','dropdown');

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('jkilmer@rydellcars.com','ServiceCheckOut',300,'sco','manager','warranty calls');

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('jkilmer@rydellcars.com','ServiceCheckOut',300,'sco','manager','completed warranty calls');

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('emilee@rydellcars.com','ServiceCheckOut',300,'sco','manager','dropdown');

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('emilee@rydellcars.com','ServiceCheckOut',300,'sco','manager','warranty calls');

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('emilee@rydellcars.com','ServiceCheckOut',300,'sco','manager','completed warranty calls');

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('mhinrichs@rydellcars.com','ServiceCheckOut',300,'sco','manager','dropdown');

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('mhinrichs@rydellcars.com','ServiceCheckOut',300,'sco','manager','warranty calls');

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
values ('mhinrichs@rydellcars.com','ServiceCheckOut',300,'sco','manager','completed warranty calls');

alter PROCEDURE GetWarrantyRos WHERE clause AS follows replacing hard coded
manager username ids: 

...
WHERE 
  CASE
--    WHEN @id NOT IN ('mhuot@rydellchev.com','aberry@rydellchev.com', 'kespelund@rydellchev.com','aneumann@rydellcars.com') THEN a.userName = @id
    WHEN EXISTS (
      SELECT 1
      FROM employeeappauthorization
      WHERE username = @id
        AND appcode = 'sco'
        AND approle = 'manager'
        AND functionality = 'warranty calls') THEN 1 = 1
    ELSE a.username = @id   
  END
...  