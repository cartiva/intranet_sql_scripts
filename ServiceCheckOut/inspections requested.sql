INSERT INTO writerlandingpage
SELECT r.sundaytosaturdayweek, 0, fullname, r.writerid, 'Inspections Requested',
  sun, mon, tue, wed, thu, fri, sat, total, eeusername, 7  
FROM ( -- sunday to saturday
  SELECT sundaytosaturdayweek, writerid, fullname, eeusername,
    MAX(CASE WHEN dayname = 'sunday' THEN inspections + ' / ' + closed END) AS sun,
    MAX(CASE WHEN dayname = 'monday' THEN inspections + ' / ' + closed END) AS mon,
    MAX(CASE WHEN dayname = 'tuesday' THEN inspections + ' / ' + closed END) AS tue,
    MAX(CASE WHEN dayname = 'wednesday' THEN inspections + ' / ' + closed END) AS wed,
    MAX(CASE WHEN dayname = 'thursday' THEN inspections + ' / ' + closed END) AS thu,
    MAX(CASE WHEN dayname = 'friday' THEN inspections + ' / ' + closed END) AS fri,
    MAX(CASE WHEN dayname = 'saturday' THEN inspections + ' / ' + closed END) AS sat
  FROM (  
    SELECT sundaytosaturdayweek, dayname, fullname, eeusername, a.writerid,  
      trim(cast(coalesce(b.inspections, 0) AS sql_char)) AS inspections, 
      trim(cast(coalesce(a.closed, 0) AS sql_char)) AS closed
    FROM (
      SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
      FROM dds.day a
      WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) z  
    LEFT JOIN ( -- closed ros
      SELECT b.thedate, a.writerid, COUNT(*) AS closed
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
        AND b.datetype = 'date'
        AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false
      GROUP BY b.thedate, a.writerid) a ON a.thedate = z.thedate 
    LEFT join ( -- closed ros with inspection lines
      SELECT b.thedate, a.writerid, COUNT(*) AS inspections
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false 
        AND EXISTS (
          SELECT 1
          FROM dds.factroline
          WHERE ro = a.ro
          AND opcode in ('23I','23D', '23IW'))  
      GROUP BY b.thedate, a.writerid) b ON a.thedate = b.thedate AND a.writerid = b.writerid  
    LEFT JOIN servicewriters c ON a.writerid = c.writernumber) x 
  GROUP BY sundaytosaturdayweek, fullname, eeusername, writerid) r
LEFT JOIN ( -- total
  SELECT sundaytosaturdayweek, writerid, trim(cast(SUM(inspections) AS sql_char)) + ' / ' + trim(cast(SUM(closed) AS sql_char)) AS total
  FROM ( 
    SELECT sundaytosaturdayweek, dayname, a.writerid, fullname, eeusername, 
      coalesce(b.inspections, 0) AS inspections, 
      coalesce(a.closed, 0) AS closed
    FROM (
      SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
      FROM dds.day a
      WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) z  
    LEFT JOIN ( -- closed ros
      SELECT b.thedate, a.writerid, COUNT(*) AS closed
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
        AND b.datetype = 'date'
        AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false
      GROUP BY b.thedate, a.writerid) a ON a.thedate = z.thedate 
    LEFT join ( -- closed ros with inspection lines
      SELECT b.thedate, a.writerid, COUNT(*) AS inspections
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false 
        AND EXISTS (
          SELECT 1
          FROM dds.factroline
          WHERE ro = a.ro
          AND opcode in ('23I','23D', '23IW'))  
      GROUP BY b.thedate, a.writerid) b ON a.thedate = b.thedate AND a.writerid = b.writerid  
    LEFT JOIN servicewriters c ON a.writerid = c.writernumber) x
    GROUP BY sundaytosaturdayweek, writerid) s ON r.writerid = s.writerid
      AND r.sundaytosaturdayweek = s.sundaytosaturdayweek