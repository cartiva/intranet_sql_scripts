
select b.firstname, a.ro, a.closedate, d.gquser, gqts
FROM warrantyros a
LEFT JOIN serviceWriters b on a.username = b.username
LEFT JOIN dds.stgArkonaGLPTRNS c on a.ro = c.gtctl#
LEFT JOIN dds.stgArkonaGLPDTIM d on c.gttrn# = d.gqtrn#
WHERE a.followupcomplete = false
  AND month(a.closedate) = 8
ORDER BY a.ro, d.gqts

DROP TABLE #wtf;
select b.firstname, a.ro, a.closedate, d.gquser, a.insertedTS, MAX(gqts) AS transTime
INTO #wtf
FROM warrantyros a
LEFT JOIN serviceWriters b on a.username = b.username
LEFT JOIN dds.stgArkonaGLPTRNS c on a.ro = c.gtctl#
LEFT JOIN dds.stgArkonaGLPDTIM d on c.gttrn# = d.gqtrn#
WHERE a.followupcomplete = false
--  AND month(a.closedate) = 8
GROUP BY b.firstname, a.ro, a.closedate, d.gquser, a.insertedTS  
ORDER BY a.ro;

SELECT *
FROM #wtf;

SELECT firstname, ro, closedate, timestampdiff(sql_tsi_minute, transtime, insertedts)
FROM #wtf

SELECT firstname, ro, closedate, gquser, transtime
FROM #wtf a
WHERE transtime = (
  SELECT max(transtime)
  FROM #wtf
  WHERE ro = a.ro
  GROUP BY firstname, ro, closedate)
  
select * FROM serviceWriterMetricData  
WHERE thedate = '08/29/2014'
  AND metric = 'Open Warranty Calls'