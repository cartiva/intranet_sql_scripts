
        
SELECT c.storecode, a.ro, b.thedate, b.dayName, f.*
FROM dds.factro a
INNER JOIN 
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.sundaytosaturdayweek = 495
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber
  AND c.storecode = 'ry1' -- ry2 mod
LEFT JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# 
LEFT JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#    
WHERE EXISTS (
    SELECT 1
    FROM dds.stgArkonaGLPTRNS
    WHERE gtdoc# = a.ro)
  AND NOT EXISTS (
    SELECT 1 
    FROM dds.factroline
    WHERE ro = a.ro
      AND paytype IN ('w','i'))
  AND NOT EXISTS (
    SELECT 1
    FROM dds.factroline
    WHERE ro = a.ro
      AND servicetype <> 'mr')      
  AND a.void = false 

  
SELECT f.gquser, a.ro
FROM dds.factro a
INNER JOIN 
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.sundaytosaturdayweek = (SELECT sundaytosaturdayweek FROM dds.day WHERE thedate = curdate())
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber
  AND c.storecode = 'ry1' -- ry2 mod
LEFT JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# 
LEFT JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#    
WHERE EXISTS (
    SELECT 1
    FROM dds.stgArkonaGLPTRNS
    WHERE gtdoc# = a.ro)
  AND NOT EXISTS (
    SELECT 1 
    FROM dds.factroline
    WHERE ro = a.ro
      AND paytype IN ('w','i'))
  AND NOT EXISTS (
    SELECT 1
    FROM dds.factroline
    WHERE ro = a.ro
      AND servicetype <> 'mr')      
  AND a.void = false   
GROUP BY f.gquser  

-- this IS pretty close
SELECT gquser, COUNT(*)
FROM (
  SELECT a.ro, c.fullname, e.gquser
  FROM dds.factro a
  INNER JOIN 
    dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.sundaytosaturdayweek = (SELECT sundaytosaturdayweek FROM dds.day WHERE thedate = curdate())
      AND b.thedate < curdate()
  INNER JOIN ServiceWriters c ON a.storecode = c.storecode
    AND a.writerid = c.writernumber
    AND c.storecode = 'ry1' -- ry2 mod
  LEFT JOIN dds.stgArkonaGLPTRNS d ON a.ro = d.gtdoc#
    AND d.gtacct = '146000'
  LEFT JOIN dds.stgArkonaGLPDTIM e ON d.gttrn# = e.gqtrn#     
  WHERE NOT EXISTS (
    SELECT 1 
    FROM dds.factroline
    WHERE ro = a.ro
      AND paytype IN ('w','i'))
    AND NOT EXISTS (
      SELECT 1
      FROM dds.factroline
      WHERE ro = a.ro
        AND servicetype <> 'mr')      
    AND a.void = false 
    AND d.gtco# IS NOT NULL
  GROUP BY a.ro, c.fullname, e.gquser) x
GROUP BY gquser   



SELECT a.thedate AS opendate, ro, b.writerid, b.createdts, 
  (SELECT thedate FROM day WHERE datekey = b.finalclosedatekey) AS finalclosedate,
  c.gttamt, d.*
SELECT gquser, COUNT(*)  
FROM dds.day a
inner JOIN dds.factro b ON a.datekey = b.opendatekey
  AND LEFT(b.ro, 3) = '160'
--  AND writerid IN ('720', '403')
  AND void = false
inner JOIN dds.stgArkonaGLPTRNS c ON b.ro = c.gtdoc#  
  AND c.gtacct = '146000'
LEFT JOIN dds.stgArkonaGLPDTIM d ON c.gttrn# = d.gqtrn#    
WHERE a.thedate BETWEEN curdate() - 90 AND curdate()
GROUP BY gquser