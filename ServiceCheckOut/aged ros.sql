--SELECT b.thedate AS opendate, a.*
-- need real time status
-- 3/27 this looks pretty good for basic info
SELECT a.writerid, a.ro, b.thedate, a.customername, a.vin, 
  CASE d.ptrsts
    WHEN '1' THEN 'Open'
    WHEN '2' THEN 'In-Process'
    WHEN '3' THEN 'Appr-PD'
    WHEN '4' THEN 'Cashier'
    WHEN '5' THEN 'Cashier/D'
    WHEN '6' THEN 'Pre-Inv'
    WHEN '7' THEN 'Odom Rq'
    WHEN '9' THEN 'Parts-A'
    WHEN 'L' THEN 'G/L Error'
    WHEN 'P' THEN 'PI Rq'
    ELSE 'WTF'
  END AS status, 
  trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle
FROM dds.factro a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
INNER JOIN dds.day c ON a.finalclosedatekey = c.datekey
  AND c.datetype <> 'date'
INNER JOIN dds.stgArkonaPDPPHDR d ON a.ro = d.ptdoc# -- ? eliminate those NOT IN pdpphdr
LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
WHERE writerid IN ('714','720','705')
--WHERE writerid = '714'
  AND void = false
  AND curdate() - b.thedate > 7
  
CREATE TABLE AgedROs (
  eeUserName cichar(50),
  ro cichar(9),
  openDate date,
  customer cichar(30),
  roStatus cichar(12),
  vin cichar(17),
  vehicle cichar(60)) IN database;

INSERT INTO AgedROs 
SELECT f.eeUsername, a.ro, b.thedate, a.customername, 
  CASE d.ptrsts
    WHEN '1' THEN 'Open'
    WHEN '2' THEN 'In-Process'
    WHEN '3' THEN 'Appr-PD'
    WHEN '4' THEN 'Cashier'
    WHEN '5' THEN 'Cashier/D'
    WHEN '6' THEN 'Pre-Inv'
    WHEN '7' THEN 'Odom Rq'
    WHEN '9' THEN 'Parts-A'
    WHEN 'L' THEN 'G/L Error'
    WHEN 'P' THEN 'PI Rq'
    ELSE 'WTF'
  END AS status, 
  a.vin, 
  trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle
FROM dds.factro a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
INNER JOIN dds.day c ON a.finalclosedatekey = c.datekey
  AND c.datetype <> 'date'
INNER JOIN dds.stgArkonaPDPPHDR d ON a.ro = d.ptdoc# -- ? eliminate those NOT IN pdpphdr
LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
INNER JOIN ServiceWriters f ON a.storecode = f.storecode
  AND a.writerid = f.writernumber
  AND void = false
  AND curdate() - b.thedate > 7; 
  
CREATE TABLE AgedROUpdates (
  ro cichar(9),
  updateTS timestamp,
  eeUserName cichar(50),
  statusUpdate memo);  
  
SELECT a.*, b.statusUpdate
FROM AgedRos a
LEFT JOIN AgedROUpdates b ON a.ro = b.ro  

DROP PROCEDURE getagedros;
CREATE PROCEDURE GetAgedROs (
  eeUsername cichar(50),
  ro cichar(9) output,
  openDate date output,
  customer cichar(30) output,
  roStatus cichar(12) output,
  vin cichar(17) output,
  vehicle cichar(60) output,
  statusUpdate memo output)
BEGIN
/*
EXECUTE PROCEDURE getagedros('rodt@rydellchev.com');
*/
DECLARE @id string;
@id = (SELECT eeUsername FROM __input);
INSERT INTO __output
SELECT a.ro, a.opendate, a.customer, a.rostatus, a.vin, a.vehicle, b.statusUpdate
FROM AgedRos a
LEFT JOIN (
  SELECT *
  FROM AgedROUpdates x
  WHERE updateTS = (
    SELECT MAX(updatets)
    FROM AgedRoUpdates
    WHERE ro = x.ro)) b ON a.ro = b.ro 
WHERE a.eeUsername = @id;
END;
  
INSERT INTO agedroupdates values ('16097952',timestampadd(sql_tsi_day,-3,now()),'rodt@rydellchev.com','how the fuck can this be IN cashier status since fucking september?!?!?') 
INSERT INTO agedroupdates values ('16097952',timestampadd(sql_tsi_day,-2,now()),'rodt@rydellchev.com','wait, seriously, are you kidding me?')
INSERT INTO agedroupdates values ('16097952',timestampadd(sql_tsi_day,-1,now()),'rodt@rydellchev.com','FUCK YOU!!!!!!!!!!!!!!!!!!!!!!!!!') 
 
DROP PROCEDURE InsertAgedROUpdate;
CREATE PROCEDURE InsertAgedROUpdate (
  ro cichar(9),
  eeUserName cichar(50),
  statusUpdate memo)
BEGIN
/*
EXECUTE PROCEDURE InsertAgedROUpdate('16097952','rodt@rydellchev.com','you are killing me here')
*/
INSERT INTO AgedRoUpdates values(
  (SELECT ro FROM __input),
  now(),
  (SELECT eeUserName FROM __input), 
  (SELECT statusUpdate FROM __input));
END;  

DROP PROCEDURE getrotoupdate; 
CREATE PROCEDURE GetROToUpdate (
  ro cichar(9),
  openDate date output,
  customer cichar(30) output,
  roStatus cichar(12) output,
  vin cichar(17) output,
  vehicle cichar(60) output,
  updatets timestamp output,
  updateBy cichar(50) output,
  statusUpdate memo output)
/*
EXECUTE PROCEDURE GetROToUpdate('16097952');
*/
BEGIN 
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output    
SELECT a.opendate, a.customer, a.rostatus, a.vin, a.vehicle, 
  b.updatets, b.eeusername, b.statusUpdate
FROM AgedRos a
LEFT JOIN AgedROUpdates  b ON a.ro = b.ro 
WHERE a.ro = @ro; 
END;    
/*
OPEN lines turns out to be a bit more complicated
 -- 3/27 this looks pretty good for basic info
 -- ADD OPEN lines

SELECT * FROM dds.stgArkonaPDPPHDR
SELECT * FROM dds.stgArkonaPDPPDET
SELECT a.ptdoc#, b.ptline, b.ptdate, b.ptltyp, b.ptcmnt, b.ptlopc, b.ptcrlo 
FROM dds.stgArkonaPDPPHDR a
LEFT JOIN dds.stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
WHERE a.ptdtyp = 'RO'
  AND a.ptdoc# <> ''
ORDER BY a.ptdoc#, b.ptline

SELECT a.writerid, a.ro, b.thedate, a.customername, a.vin, 
  CASE d.ptrsts
    WHEN '1' THEN 'Open'
    WHEN '2' THEN 'In-Process'
    WHEN '3' THEN 'Appr-PD'
    WHEN '4' THEN 'Cashier'
    WHEN '5' THEN 'Cashier/D'
    WHEN '6' THEN 'Pre-Inv'
    WHEN '7' THEN 'Odom Rq'
    WHEN '9' THEN 'Parts-A'
    WHEN 'L' THEN 'G/L Error'
    WHEN 'P' THEN 'PI Rq'
    ELSE 'WTF'
  END AS status, 
  TRIM(e.immake) + ' ' + TRIM(e.immodl) AS vehicle
FROM dds.factro a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
INNER JOIN dds.day c ON a.finalclosedatekey = c.datekey
  AND c.datetype <> 'date'
INNER JOIN dds.stgArkonaPDPPHDR d ON a.ro = d.ptdoc# -- ? eliminate those NOT IN pdpphdr
LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
--WHERE writerid IN ('714','720','705')
WHERE writerid = '714'
  AND void = false
  AND curdate() - b.thedate > 7
*/

-- 3/31 
-- need to UPDATE past data AND current data

-- ON 3/3/31 what were the aged ros
SELECT b.thedate AS opendate, c.thedate AS closedate, a.storecode, a.writerid, a.ro
FROM dds.factro a
LEFT JOIN dds.day b ON a.opendatekey = b.datekey
LEFT JOIN dds.day c ON a.finalclosedatekey = c.datekey
WHERE void = false

-- this looks ok
SELECT a.*, b.*
FROM (  
  SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
  FROM dds.day a
  WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
LEFT JOIN (
  SELECT b.thedate AS opendate, c.thedate AS closedate, a.storecode, a.writerid, a.ro
  FROM dds.factro a
  LEFT JOIN dds.day b ON a.opendatekey = b.datekey
  LEFT JOIN dds.day c ON a.finalclosedatekey = c.datekey
  INNER JOIN ServiceWriters d ON a.storecode = d.storecode 
    AND a.writerid = d.writernumber
  WHERE void = false
    AND b.thedate > '12/31/2012'
    AND a.storecode = 'RY1') b ON b.opendate < a.thedate - 7 AND b.closedate > a.thedate
    
    
SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, b.writerid, b.fullname, 
  b.eeUserName, cast(COUNT(*) AS sql_char) AS howmany
FROM (  
  SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
  FROM dds.day a
  WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
LEFT JOIN (
  SELECT b.thedate AS opendate, c.thedate AS closedate, a.storecode, a.writerid, 
    a.ro, d.fullname, d.eeUserName
  FROM dds.factro a
  LEFT JOIN dds.day b ON a.opendatekey = b.datekey
  LEFT JOIN dds.day c ON a.finalclosedatekey = c.datekey
  INNER JOIN ServiceWriters d ON a.storecode = d.storecode 
    AND a.writerid = d.writernumber
  WHERE void = false
--  AND writerid = '720'
    AND b.thedate > '01/31/2012'
    AND a.storecode = 'RY1') b ON b.opendate < a.thedate - 7 AND b.closedate > a.thedate    
GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, b.writerid, b.fullname, b.eeUserName    
ORDER BY a.thedate    

-- ok now, INSERT INTO writerlandingpage
-- ok, that worked, backed filled 3/3 thru today (sunday the 31st)
INSERT INTO writerlandingpage  
SELECT sundaytosaturdayweek, 0, fullname, writerid, 'Open Aged ROs',
  max(CASE WHEN dayname = 'Sunday' THEN howmany END) AS Sunday,
  max(CASE WHEN dayname = 'Monday' THEN howmany END) AS Monday,
  max(CASE WHEN dayname = 'Tuesday' THEN howmany END) AS Tuesday,
  max(CASE WHEN dayname = 'Wednesday' THEN howmany END) AS Wednesday,
  max(CASE WHEN dayname = 'Thursday' THEN howmany END) AS Thursday,
  max(CASE WHEN dayname = 'Friday' THEN howmany END) AS Friday,
  max(CASE WHEN dayname = 'Saturday' THEN howmany END) AS Saturday,
  '-', eeUserName, 2
FROM (       
  SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, b.writerid, b.fullname, 
    b.eeUserName, cast(COUNT(*) AS sql_char) AS howmany
  FROM (  
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
  LEFT JOIN (
    SELECT b.thedate AS opendate, c.thedate AS closedate, a.storecode, a.writerid, 
      a.ro, d.fullname, d.eeUserName
    FROM dds.factro a
    LEFT JOIN dds.day b ON a.opendatekey = b.datekey
    LEFT JOIN dds.day c ON a.finalclosedatekey = c.datekey
    INNER JOIN ServiceWriters d ON a.storecode = d.storecode
      AND a.writerid = d.writernumber
    WHERE void = false
--    AND writerid = '402'
      AND b.thedate > '01/31/2012'
      AND a.storecode = 'RY1') b ON b.opendate < a.thedate - 7 AND b.closedate > a.thedate    
  GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, b.writerid, b.fullname, b.eeUserName) x   
GROUP BY sundaytosaturdayweek, fullname, writerid, eeUserName

-- now need to UPDATE it daily, throughout the day!
-- basically, this becomes, what are the aged ros NOW
-- zap AND reload TABLE AgedROs ??
-- yeah z/r AgedROs each time AND UPDATE landingpage

-- this IS ALL well AND good but does NOT UPDATE real time data
-- based ON factRO
SELECT writerid, COUNT(*) FROM (
    SELECT b.thedate AS opendate, c.thedate AS closedate, a.storecode, a.writerid, 
      a.ro, d.fullname, d.eeUserName
    FROM dds.factro a
    LEFT JOIN dds.day b ON a.opendatekey = b.datekey
    LEFT JOIN dds.day c ON a.finalclosedatekey = c.datekey
    INNER JOIN ServiceWriters d ON a.storecode = d.storecode
      AND a.writerid = d.writernumber
    WHERE void = false
      AND a.storecode = 'RY1'
      AND b.thedate BETWEEN '01/01/2012' AND curdate() - 8
      AND c.thedate >= curdate()
) x GROUP BY writerid      

-- 4/22

DELETE FROM AgedROs;
INSERT INTO AgedROs
SELECT f.eeUsername, a.ro, b.thedate, a.customername, 
  CASE d.ptrsts
    WHEN '1' THEN 'Open'
    WHEN '2' THEN 'In-Process'
    WHEN '3' THEN 'Appr-PD'
    WHEN '4' THEN 'Cashier'
    WHEN '5' THEN 'Cashier/D'
    WHEN '6' THEN 'Pre-Inv'
    WHEN '7' THEN 'Odom Rq'
    WHEN '9' THEN 'Parts-A'
    WHEN 'L' THEN 'G/L Error'
    WHEN 'P' THEN 'PI Rq'
    ELSE 'WTF'
  END AS status, 
  a.vin, 
  trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle
FROM dds.factro a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
INNER JOIN dds.day c ON a.finalclosedatekey = c.datekey
  AND c.datetype <> 'date'
INNER JOIN dds.stgArkonaPDPPHDR d ON a.ro = d.ptdoc# -- ? eliminate those NOT IN pdpphdr
LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
INNER JOIN ServiceWriters f ON a.storecode = f.storecode
  AND a.writerid = f.writernumber
  AND void = false
  AND curdate() - b.thedate > 7;
