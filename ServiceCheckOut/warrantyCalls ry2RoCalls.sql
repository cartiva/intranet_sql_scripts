ALTER TABLE warrantyCalls
ADD COLUMN statusUpdateWithDateName cichar(250);

ALTER TABLE ry2RoCalls
ADD COLUMN statusUpdateWithDateName cichar(250);

UPDATE warrantyCalls
SET statusUpdateWithDateName = x.wtf
FROM (
  SELECT a.*, a.ro, now(), a.eeUserName, a.description, 
  left(CAST((SELECT mmdd FROM dds.day WHERE thedate = cast(callTS as sql_date)) AS sql_char)  collate ads_default_ci + ' ' +
    TRIM((SELECT firstname FROM people WHERE eeusername = a.eeUserName)) + ': ' + 
    a.description, 250) AS wtf
  FROM warrantyCalls a) x
WHERE warrantyCalls.ro = x.ro
  AND warrantyCalls.callTS = x.callTS;  
  
UPDATE ry2RoCalls
SET statusUpdateWithDateName = x.wtf
FROM (
  SELECT a.*, a.ro, now(), a.eeUserName, a.description, 
  left(CAST((SELECT mmdd FROM dds.day WHERE thedate = cast(callTS as sql_date)) AS sql_char)  collate ads_default_ci + ' ' +
    TRIM((SELECT firstname FROM people WHERE eeusername = a.eeUserName)) + ': ' + 
    a.description, 250) AS wtf
  FROM ry2RoCalls a) x
WHERE ry2RoCalls.ro = x.ro
  AND ry2RoCalls.callTS = x.callTS;  
  
modify procs InsertWarrantyCall, GetWarrantyRos, InsertRY2ServiceCall, GetRY2ROsForCalls 