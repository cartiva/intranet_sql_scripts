SELECT ro, COUNT(*)
FROM stgrydellserviceappointments
GROUP BY ro

SELECT *
FROM stgrydellserviceappointments
WHERE ro IN (
  SELECT ro
  FROM stgrydellserviceappointments
  WHERE ro <> ''
  GROUP BY ro
  HAVING COUNT(*) > 3)
ORDER BY createdts DESC


SELECT a.ro, a.appointmentid,a. createdby, a.advisorstatus, b.*
FROM stgrydellserviceappointments a
LEFT JOIN stgRydellServiceStatuses b ON a.appointmentid = b.appointmentid
WHERE ro = '16123307'

SELECT a.ro, a.appointmentid,a. createdby, a.advisorstatus, b.*
FROM stgrydellserviceappointments a
LEFT JOIN stgRydellServiceStatuses b ON a.appointmentid = b.appointmentid
WHERE a.ro IN (
  SELECT ro
  FROM stgrydellserviceappointments
  WHERE LEFT(ro,2) = '16'
  GROUP BY ro
  HAVING COUNT(*) > 3)
ORDER BY a.ro DESC 