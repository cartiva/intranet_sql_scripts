
talking to lib, 
a mgr logs IN, net promoter shows 2 options, 
log IN AS a writer just the header

log IN AS a mgr

Headers                 Details
Service Stats           View Aged ROs
                        View Warranty Calls
Net Promoter            View Score
                        View Surveys
log IN AS a writer
Headers                 Details
Service Stats           View Aged ROs
                        View Warranty Calls
Net Promoter            - none -    

ADD a COLUMN to tasks: TaskHeader
ALTER TABLE tasks 
ADD COLUMN TaskHeader cichar(50);  

ADD a row to Tasks for Surveys  
INSERT INTO tasks (task,caption,displayclass,TaskHeader)
values ('NP Surveys', 'View Surveys', 'npsurveys','Net Promoter'); 
INSERT INTO PositionTasks (StoreCode, Department, Position, Task, FromTS, ThruTS)
SELECT StoreCode, Department, Position, 'NP Surveys', now(), ThruTS
FROM PositionTasks
WHERE task = 'NP Scores'
  AND Position = 'Manager'
  AND Department = 'Main Shop';                                       

CREATE TABLE LeftNavBar(
  eeusername cichar(50),
  Header1 cichar(50), 
  Header1DisplayClass cichar(50),
  Detail1A cichar(50),
  Detail1ADisplayClass cichar(50),
  Detail1B cichar(50),
  Detail1BDisplayClass cichar(50),
  Header2 cichar(50), 
  Header2DisplayClass cichar(50),
  Detail2A cichar(50),
  Detail2ADisplayClass cichar(50),
  Detail2B cichar(50),
  Detail2BDisplayClass cichar(50)) IN database;

DELETE FROM LeftNavBar;    
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('mhuot@rydellchev.com', 
  'Service Stats','header',
  'View Aged ROs','aged','View Warranty Calls','warranty',
  'Net Promoter','header',
  'View NP Scores','npscores','NP Surveys','npsurveys'); 
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('aberry@rydellchev.com', 
  'Service Stats','header',
  'View Aged ROs','aged','View Warranty Calls','warranty',
  'Net Promoter','header',
  'View NP Scores','npscores','NP Surveys','npsurveys');    

INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('rodt@rydellchev.com', 
  'Service Stats','header',
  'View Aged ROs','aged','View Warranty Calls','warranty',
  'Net Promoter','header',
  NULL,NULL,NULL,NULL);   
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('johnk@rydellchev.com', 
  'Service Stats','header',
  'View Aged ROs','aged','View Warranty Calls','warranty',
  'Net Promoter','header',
  NULL,NULL,NULL,NULL);  
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('jtyrrell@rydellchev.com', 
  'Service Stats','header',
  'View Aged ROs','aged','View Warranty Calls','warranty',
  'Net Promoter','header',
  NULL,NULL,NULL,NULL);   
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('kcarlson@rydellchev.com', 
  'Service Stats','header',
  'View Aged ROs','aged','View Warranty Calls','warranty',
  'Net Promoter','header',
  NULL,NULL,NULL,NULL);     
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('kespelund@rydellchev.com', 
  'Service Stats','header',
  'View Aged ROs','aged','View Warranty Calls','warranty',
  'Net Promoter','header',
  NULL,NULL,NULL,NULL);       
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('mflikka@rydellchev.com', 
  'Service Stats','header',
  'View Aged ROs','aged','View Warranty Calls','warranty',
  'Net Promoter','header',
  NULL,NULL,NULL,NULL);    
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('mflikka@rydellchev.com', 
  'Service Stats','header',
  'View Aged ROs','aged','View Warranty Calls','warranty',
  'Net Promoter','header',
  NULL,NULL,NULL,NULL);  
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('tbursinger@rydellchev.com', 
  'Service Stats','header',
  'View Aged ROs','aged','View Warranty Calls','warranty',
  'Net Promoter','header',
  NULL,NULL,NULL,NULL);   
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('aneumann@gfhonda.com', 
  'Service Stats','header',
  'View Aged ROs','aged','View RY2 Follow Up Calls','ry2',
  NULL,NULL,
  NULL,NULL,NULL,NULL);     
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('dpederson@gfhonda.com', 
  'Service Stats','header',
  'View Aged ROs','aged',NULL,NULL,
  NULL,NULL,
  NULL,NULL,NULL,NULL);     
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('jdangerfield@gfhonda.com', 
  'Service Stats','header',
  'View Aged ROs','aged',NULL,NULL,
  NULL,NULL,
  NULL,NULL,NULL,NULL);    
  
INSERT INTO LeftNavBar ( eeusername,
  Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,Detail2B,Detail2BDisplayClass)
values('dbrekke@gfhonda.com', 
  'Service Stats','header',
  'View RY2 Follow Up Calls','ry2',NULL,NULL,
  NULL,NULL,
  NULL,NULL,NULL,NULL);  
  
create PROCEDURE GetLeftNavBar(
  eeusername cichar(50),
  Header1 cichar(50) output, 
  Header1DisplayClass cichar(50) output,
  Detail1A cichar(50) output,
  Detail1ADisplayClass cichar(50) output,
  Detail1B cichar(50) output,
  Detail1BDisplayClass cichar(50) output,
  Header2 cichar(50) output, 
  Header2DisplayClass cichar(50) output,
  Detail2A cichar(50) output,
  Detail2ADisplayClass cichar(50) output,
  Detail2B cichar(50) output,
  Detail2BDisplayClass cichar(50) output,
  json1 memo output,
  json2 memo output)
BEGIN
/*
EXECUTE PROCEDURE GetLeftNavBar('rodt@rydellchev.com');
*/
DECLARE @user string;
@user = (SELECT eeusername FROM __input);
INSERT INTO __output 
SELECT   Header1,Header1DisplayClass,
  Detail1A,Detail1ADisplayClass,
  Detail1B,Detail1BDisplayClass,
  Header2,Header2DisplayClass,
  Detail2A,Detail2ADisplayClass,
  Detail2B,Detail2BDisplayClass,
/**/  
  '{"Header":"' + TRIM(coalesce(Header1, '')) + '",' + 
  '"HeaderDisplayClass":"' + TRIM(coalesce(Header1DisplayClass, '')) + '",' +
  '"Detail":[{"Caption":"' + TRIM(coalesce(Detail1A, '')) + '",' +
  '"DetailDisplayClass":"' + TRIM(coalesce(Detail1ADisplayClass, '')) + '"},{' + 
  '"Caption":"' + TRIM(coalesce(Detail1B, '')) + '",' +
  '"DetailDisplayClass":"' + TRIM(coalesce(Detail1BDisplayClass, '')) + '"}]}' AS json1,
  '{"Header":"' + TRIM(coalesce(Header2, '')) + '",' + 
  '"HeaderDisplayClass":"' + TRIM(coalesce(Header2DisplayClass, '')) + '",' +
  '"Detail":[{"Caption":"' + TRIM(coalesce(Detail2A, '')) + '",' +
  '"DetailDisplayClass":"' + TRIM(coalesce(Detail2ADisplayClass, '')) + '"},{' + 
  '"Caption":"' + TRIM(coalesce(Detail2B, '')) + '",' +
  '"DetailDisplayClass":"' + TRIM(coalesce(Detail2BDisplayClass, '')) + '"}]}' AS json2   
/**/  
/* -- with leading AND trailing []
  '[{"Header":"' + TRIM(coalesce(Header1, '')) + '",' + 
  '"HeaderDisplayClass":"' + TRIM(coalesce(Header1DisplayClass, '')) + '",' +
  '"Detail":[{"Caption":"' + TRIM(coalesce(Detail1A, '')) + '",' +
  '"DetailDisplayClass":"' + TRIM(coalesce(Detail1ADisplayClass, '')) + '"},{' + 
  '"Caption":"' + TRIM(coalesce(Detail1B, '')) + '",' +
  '"DetailDisplayClass":"' + TRIM(coalesce(Detail1BDisplayClass, '')) + '"}]}]' AS json1,
  '[{"Header":"' + TRIM(coalesce(Header2, '')) + '",' + 
  '"HeaderDisplayClass":"' + TRIM(coalesce(Header2DisplayClass, '')) + '",' +
  '"Detail":[{"Caption":"' + TRIM(coalesce(Detail2A, '')) + '",' +
  '"DetailDisplayClass":"' + TRIM(coalesce(Detail2ADisplayClass, '')) + '"},{' + 
  '"Caption":"' + TRIM(coalesce(Detail2B, '')) + '",' +
  '"DetailDisplayClass":"' + TRIM(coalesce(Detail2BDisplayClass, '')) + '"}]}]' AS json2  
*/  
FROM LeftNavBar
WHERE eeusername = @user;
END;




{
    "firstName": "John",
    "lastName": "Smith",
    "age": 25,
    "address": {
        "streetAddress": "21 2nd Street",
        "city": "New York",
        "state": "NY",
        "postalCode": 10021
    },
    "phoneNumbers": [
        {
            "type": "home",
            "number": "212 555-1234"
        },
        {
            "type": "fax",
            "number": "646 555-4567"
        }
    ]
}


[{
  "Header1":"Service Stats",
  "Header1DisplayClass":"header",
  "Detail1A":"View Aged ROs",
  "Detail1ADisplayClass":"aged",
  "Detail1B":"View Warranty Calls",
  "Detail1BDisplayClass":"warranty",
  "Header2":"Net Promoter",
  "Header2DisplayClass":"header",
  "Detail2A":"View NP Scores",
  "Detail2ADisplayClass":"npscores",
  "Detail2B":"NP Surveys",
  "Detail2BDisplayClass":"npsurveys"}]
  
-- these both validate AS good JSON  
  
{
  "Header":"Service Stats",
  "HeaderDisplayClass":"header",
  "Detail": [
      {
          "Caption":"View Aged ROs",
          "DetailDisplayClass":"aged"
      },
      {
          "Caption":"View Warranty Calls",
          "DetailDisplayClass":"warranty"      
      }
  ]
}

{
  "Header":"Net Promoter",
  "HeaderDisplayClass":"header",
  "Detail": [
      {
          "Caption":"View NP Scores",
          "DetailDisplayClass":"npscores"
      },
      {
          "Caption":"NP Surveys",
          "DetailDisplayClass":"npsurveys"      
      }
  ]
}
SELECT a.*,
  '{"Header":"' + TRIM(Header1) + '",' + 
  '"HeaderDisplayClass":"' + TRIM(Header1DisplayClass) + '",' +
  '"Detail":[{"Caption":"' + TRIM(Detail1A) + '",' +
  '"DetailDisplayClass":"' + TRIM(Detail1ADisplayClass) + '"},{' + 
  '"Caption":"' + TRIM(Detail1B) + '",' +
  '"DetailDisplayClass":"' + TRIM(Detail1BDisplayClass) + '"}]}' AS json1,
  '{"Header":"' + TRIM(Header2) + '",' + 
  '"HeaderDisplayClass":"' + TRIM(Header2DisplayClass) + '",' +
  '"Detail":[{"Caption":"' + TRIM(Detail2A) + '",' +
  '"DetailDisplayClass":"' + TRIM(Detail2ADisplayClass) + '"},{' + 
  '"Caption":"' + TRIM(Detail2B) + '",' +
  '"DetailDisplayClass":"' + TRIM(Detail2BDisplayClass) + '"}]}' AS json2 
FROM leftnavbar a WHERE eeusername = 'mhuot@rydellchev.com' 

SELECT * FROM leftnavbar WHERE eeusername = 'mhuot@rydellchev.com' 

create PROCEDURE GetLeftNavBar(
  eeusername cichar(50),
  DataType cichar(50) output,
  Label cichar(50) output, 
  Class cichar(50) output,
  seq integer output)
BEGIN
/*
EXECUTE PROCEDURE GetLeftNavBar('mhuot@rydellchev.com');
*/
DECLARE @user string;
@user = (SELECT eeusername FROM __input);
INSERT INTO __output 
SELECT top 100 *
FROM (
  SELECT 'Header' AS DataType,Header1 AS Label, Header1DisplayClass AS Class, 1 AS seq
  FROM leftnavbar a 
  WHERE eeusername = @user
  UNION 
  SELECT 'Header' AS DataType,Header2 AS Label, Header2DisplayClass AS Class, 4 AS seq
  FROM leftnavbar a 
  WHERE eeusername = @user
  UNION
  SELECT 'Detail' AS DataType,Detail1A AS Label, Detail1ADisplayClass AS Class, 2 AS seq
  FROM leftnavbar a 
  WHERE eeusername = @user
  UNION
  SELECT 'Detail' AS DataType,Detail1B AS Label, Detail1BDisplayClass AS Class, 3 AS seq
  FROM leftnavbar a 
  WHERE eeusername = @user
  UNION
  SELECT 'Detail' AS DataType,Detail2A AS Label, Detail2ADisplayClass AS Class, 5 AS seq
  FROM leftnavbar a 
  WHERE eeusername = @user
  UNION
  SELECT 'Detail' AS DataType,Detail2B AS Label, Detail2BDisplayClass AS Class, 6 AS seq
  FROM leftnavbar a 
  WHERE eeusername = @user) x
WHERE Label IS NOT NULL
ORDER BY seq; 
END;


   
   
   