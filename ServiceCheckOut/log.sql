CREATE TABLE log (
  ts timestamp,
  eeUserName cichar(50),
  storedProc cichar(50),
  calledBy cichar(50),
  spParam1 cichar(25),
  spParam2 cichar(25),
  spParam3 cichar(25),
  spParam4 cichar(25), 
  spParam5 cichar(25),
  spParam6 cichar(25),
  result memo) IN database;