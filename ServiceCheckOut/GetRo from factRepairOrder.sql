-- ADAM LINDQUIST (ry2 eg '2686258') showing up AS Wiebusch
-- Header
SELECT distinct a.ro AS RO, b.rostatus AS ROStatus, 
  left(c.DayName,3) + ' ' + c.mmdd AS OpenDate, 
  iif(d.thedate = '12/31/9999', '',left(d.DayName,3) + ' ' + d.mmdd) AS CloseDate,
  e.fullname AS Customer,
  CASE 
    WHEN e.homephone  = '0' THEN 'No Phone'
    WHEN e.homephone  = '' THEN 'No Phone'
    WHEN e.homephone IS NULL THEN 'No Phone'
    ELSE left(e.homephone, 12)   
  END AS HomePhone,
  CASE 
    WHEN e.businessphone  = '0' THEN 'No Phone'
    WHEN e.businessphone  = '' THEN 'No Phone'
    WHEN e.businessphone IS NULL THEN 'No Phone'
    ELSE left(e.businessphone, 12)   
  END AS WorkPhone,  
  CASE 
    WHEN e.cellphone  = '0' THEN 'No Phone'
    WHEN e.cellphone  = '' THEN 'No Phone'
    WHEN e.cellphone IS NULL THEN 'No Phone'
    ELSE left(e.cellphone, 12)   
  END AS CellPhone,  
  iif(length(trim(e.email)) < 3, 'None', e.email) AS CustomerEMail,
  trim(TRIM(f.make) + ' ' + TRIM(f.model)) AS Vehicle,
  h.fullname AS WriterFirstName, 
  left(cast(i.Comment AS sql_char), 500) AS PleaseNote
FROM dds.factRepairOrder a
LEFT JOIN dds.dimRoStatus b ON a.rostatuskey = b.rostatuskey
LEFT JOIN dds.day c ON a.opendatekey = c.datekey
LEFT JOIN dds.day d ON a.finalclosedatekey = d.datekey
LEFT JOIN dds.dimcustomer e ON a.customerkey = e.customerkey
LEFT JOIN dds.dimVehicle f ON a.vehiclekey = f.vehiclekey
LEFT JOIN dds.dimServiceWriter g ON a.servicewriterkey = g.servicewriterkey
LEFT JOIN ServiceWriters h ON g.writernumber = h.writernumber
LEFT JOIN dds.dimRoComment i ON a.rocommentkey = i.rocommentkey
WHERE a.ro = '16121172'

-- detail
SELECT a.ro AS RO, a.line AS Line, b.LineStatus, c.PaymentTypeCode AS PayType, 
  d.opcode AS OpCode, d.description AS OpCodeDescr,
  e.complaint AS Complaint, e.cause AS Cause, h.opcode AS CorCode, 
  h.description AS CorCodeDescr, e.correction AS Correction
FROM dds.factrepairorder a
LEFT JOIN dds.dimLineStatus b ON a.statuskey = b.linestatuskey
LEFT JOIN dds.dimPaymentType c ON a.paymenttypekey = c.paymenttypekey
LEFT JOIN dds.dimOpCode d ON a.opcodekey = d.opcodekey
LEFT JOIN dds.dimCCC e ON a.cccKey = e.ccckey
LEFT JOIN dds.dimCorCodeGroup f ON a.corCodeGroupKey = f.CorCodeGroupKey
LEFT JOIN dds.brCorCodeGroup g ON f.CorCodeGroupKey = g.CorCodeGroupKey
LEFT JOIN dds.dimOpCode h ON g.opcodekey = h.opcodekey
WHERE a.ro = '16127118'