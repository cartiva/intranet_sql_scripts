/* this IS to be run against SCO to UPDATE it to match 1.3 functionality FROM scoTest*/

UPDATE PositionFulfillment
SET ThruTS = CAST('08/06/2013 01:00:00' AS sql_timestamp)
WHERE eeusername = 'dwiebusch@rydellchev.com';

--DROP TABLE testsnippets;
Create Table snippets(
   storecode CIChar( 3 ),
   membergroup CIChar( 24 ),
   snippet Memo,
   seq Integer );
INSERT INTO "snippets" VALUES( 'RY1', 'ServiceManager', 
'<!-- Service Stats -->' +
'<nav class="menu-item">' +
'  <a class="mainlink" href="#managerservicestats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>' +
'  <ul class="submenu">' +
'    <li><span class="sublink"><a href="#viewagedros">View Aged ROs</a></span></li>' +
'    <li><span class="sublink"><a href="#viewwarrantycalls">View Warranty Calls</a></span></li>' +
'  </ul>' +
'</nav>' +
'<!-- .menu-item -->', 1 );
INSERT INTO "snippets" VALUES( 'RY1', 'ServiceManager', 
'<!-- Net Promoter -->' +
'<nav class="menu-item">' +
'  <a class="mainlink" href="#managernetpromoter"><span class="glyphicon glyphicon-signal"></span>Net Promoter<span class="arrow-right"></span></a>' +
'  <ul class="submenu">' +
'    <li><span class="sublink"><a href="#managernpsurveys">View Surveys</a></span></li>' +
'  </ul>' +
'</nav>' +
'<!-- .menu-item -->', 2 );
INSERT INTO "snippets" VALUES( 'RY1', 'ServiceManager', 
'<!-- Employees -->' +
'<nav class="menu-item">'+
'  <a class="mainlink employees" href="#employees"><span class="glyphicon glyphicon-user"></span><em>Apps by Employee</em><span class="arrow-right"></span></a>' +
'</nav>' +
'<!-- .menu-item -->', 3 );
INSERT INTO "snippets" VALUES( 'RY1', 'ServiceWriter', 
'<!-- Service Stats -->' +
'<nav class="menu-item">' +
'  <a class="mainlink" href="#servicestats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>' +
'  <ul class="submenu">' +
'    <li><span class="sublink"><a href="#viewagedros">View Aged ROs</a></span></li>' +
'    <li><span class="sublink"><a href="#viewwarrantycalls">View Warranty Calls</a></span></li>' +
'  </ul>' +
'</nav>' +
'<!-- .menu-item -->', 1 );
INSERT INTO "snippets" VALUES( 'RY1', 'ServiceWriter', 
'<!-- Net Promoter -->' +
'<nav class="menu-item">' +
'  <a class="mainlink" href="#netpromoter"><span class="glyphicon glyphicon-signal"></span>Net Promoter<span class="arrow-right"></span></a>' +
'</nav>' +
'<!-- .menu-item -->', 2 );
INSERT INTO "snippets" VALUES( 'RY2', 'ServiceManager', 
'<!-- Employees -->' +
'<nav class="menu-item">' +
'  <a class="mainlink employees" href="#employees"><span class="glyphicon glyphicon-user"></span><em>Apps by Employee</em><span class="arrow-right"></span></a>' +
'</nav>' +
'<!-- .menu-item -->', 2 );
INSERT INTO "snippets" VALUES( 'RY2', 'ServiceManager', 
'<!-- Service Stats -->' +
'<nav class="menu-item">' +
'  <a class="mainlink" href="#managerservicestats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>' +
'  <ul class="submenu">' +
'    <li><span class="sublink"><a href="#viewagedros">View Aged ROs</a></span></li>' +
'    <li><span class="sublink"><a href="#ry2servicecalls">View Nissan Follow Up Calls</a></span></li>' +
'  </ul>' +
'</nav>' +
'<!-- .menu-item -->', 1 );
INSERT INTO "snippets" VALUES( 'RY2', 'ServiceWriter', 
'<!-- Service Stats -->' +
'<nav class="menu-item">' +
'  <a class="mainlink" href="#servicestats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>' +
'  <ul class="submenu">' +
'    <li><span class="sublink"><a href="#viewagedros">View Aged ROs</a></span></li>' +
'    <li><span class="sublink"><a href="#ry2servicecalls">View Nissan Follow Up Calls</a></span></li>' +
'  </ul>' +
'</nav>' +
'<!-- .menu-item -->', 1 );
INSERT INTO "snippets" VALUES( 'RY2', 'ServiceMisc', 
'<!-- Service Stats -->' +
'<nav class="menu-item">' +
'  <a class="mainlink" href="#ry2servicecalls"><span class="glyphicon glyphicon-pencil"></span>View Nissan Follow Up Calls<span class="arrow-right"></span></a>' +
'</nav>' +
'<!-- .menu-item -->', 1 );

CREATE PROCEDURE GetLeftNavBar
   ( 
      eeusername CICHAR ( 50 ),
      snippet Memo OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getleftnavbar('mhuot@rydellchev.com');
EXECUTE PROCEDURE getleftnavbar('rodt@rydellchev.com');
EXECUTE PROCEDURE getleftnavbar('aneumann@gfhonda.com');
EXECUTE PROCEDURE getleftnavbar('dpederson@gfhonda.com');

*/
DECLARE @user cichar(50);
@user = (SELECT eeUsername FROM __input);
INSERT INTO __output
SELECT top 100 snippet
FROM snippets a
INNER JOIN people b on a.membergroup = b.membergroup
  AND a.storecode = b.storecode 
WHERE b.eeUsername = @user
ORDER BY a.seq;

END;

ALTER PROCEDURE GetWriterLandingPage
   ( 
      eeUserName CICHAR ( 50 ),
      DisplayFirstName CICHAR ( 40 ) OUTPUT,
      DisplayFullName CICHAR ( 40 ) OUTPUT,
      Metric CICHAR ( 45 ) OUTPUT,
      Sun CICHAR ( 12 ) OUTPUT,
      Mon CICHAR ( 12 ) OUTPUT,
      Tue CICHAR ( 12 ) OUTPUT,
      Wed CICHAR ( 12 ) OUTPUT,
      Thu CICHAR ( 12 ) OUTPUT,
      Fri CICHAR ( 12 ) OUTPUT,
      Sat CICHAR ( 12 ) OUTPUT,
      Total CICHAR ( 12 ) OUTPUT,
      seq Integer OUTPUT,
      def CICHAR ( 250 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetWriterLandingPage('rodt@rydellchev.com');
EXECUTE PROCEDURE GetWriterLandingPage('dpederson@gfhonda.com');

need to pivot the data to match the landing page FROM the functional spec:
              sun  mon  tue  thr  fri   sat
Cashiering     3    4    6    4    8     8
Aged OPEN Ros  5    7    0    7    5     7
...
        

  DECLARE @id string;
  @id = (SELECT eeUserName FROM __input);
  INSERT INTO __output  
SELECT metric, Sun,Mon,Tue,Wed,Thu,Fri,Sat,Total
FROM writerlandingpage a
WHERE  weekid = (
    SELECT sundaytosaturdayweek
    FROM dds.day
    WHERE thedate = curdate())
  AND eeUserName = @id;

9/18
    added JOIN to people to return firstname & fullname  
*/


DECLARE @eeusername string;
@eeusername = (SELECT eeusername FROM __input);
INSERT INTO __output 
SELECT g.FirstName, g.FullName, d.metric, trim(d.sun) AS Sun, trim(d.mon) AS Mon, trim(d.tue) AS Tue, 
  trim(d.wed) AS Wed, trim(d.thu) AS Thu, trim(d.fri) AS Fri,
  trim(d.sat) AS Sat, e.thisweekdisplay AS Total, d.seq, d.def
FROM ( -- daily
  SELECT a.sundaytosaturdayweek, 0, a.fullname, a.writernumber, a.metric, 
    max(CASE WHEN a.dayname = 'Sunday' THEN todayDisplay END) AS Sun,
    max(CASE WHEN a.dayname = 'Monday' THEN todayDisplay END) AS Mon,
    max(CASE WHEN a.dayname = 'Tuesday' THEN todayDisplay END) AS Tue,
    max(CASE WHEN a.dayname = 'Wednesday' THEN todayDisplay END) AS Wed,
    max(CASE WHEN a.dayname = 'Thursday' THEN todayDisplay END) AS Thu,
    max(CASE WHEN a.dayname = 'Friday' THEN todayDisplay END) AS Fri,
    max(CASE WHEN a.dayname = 'Saturday' THEN todayDisplay END) AS Sat,
    eeusername, seq, MAX(def) AS def
  FROM (
    SELECT a.*,
      b.todaydisplay,
      b.thisweekdisplay, b.*
    FROM ( -- 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL with seq)
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, b.eeusername, 
        b.fullname, b.writernumber, c.metric, c.seq, c.def
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
        SELECT SundayToSaturdayWeek 
        FROM dds.day
        WHERE thedate = curdate()) -- current week 
--        WHERE thedate = curdate() - 7) -- previous week
        AND c.seq IS NOT NULL
        AND b.storecode = c.storecode) a ----------------------------------------------
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric) a 
  GROUP BY a.sundaytosaturdayweek, a.fullname, a.writernumber, a.metric, eeusername, seq) d
LEFT JOIN ( -- this appears to be working for seq 1,4-7 to get weekly
  SELECT a.eeusername, a.metric, a.SundayToSaturdayWeek, a.thisweekdisplay
  FROM servicewritermetricdata a
  INNER JOIN servicewritermetrics c ON a.metric = c.metric
    AND a.storecode = c.storecode -- this fixes the dups
    AND c.seq IS NOT NULL 
  INNER JOIN (
      SELECT MAX(thedate) AS thedate, eeusername, metric, SundayToSaturdayWeek
      FROM servicewritermetricdata
      WHERE thisweekdisplay IS NOT NULL 
      GROUP BY eeusername, metric, SundayToSaturdayWeek) b ON a.thedate = b.thedate
        and a.eeusername = b.eeusername
        and a.metric = b.metric
        and a.SundayToSaturdayWeek = b.SundayToSaturdayWeek) e ON d.eeusername = e.eeusername
    AND d.metric = e.metric
    AND d.SundayToSaturdayWeek = e.SundayToSaturdayWeek 
LEFT JOIN People g on g.eeusername = @eeusername      
WHERE d.eeUserName = @eeUserName;

END;

-- Table Type of People is ADT
execute procedure sp_DropReferentialIntegrity( 'Position-Fulfillment2' );
execute procedure sp_DropReferentialIntegrity( 'People-WarrantyCalls' );

DROP TABLE People;
Create Table People(
   eeUserName CIChar( 50 ),
   FullName CIChar( 40 ),
   FirstName CIChar( 40 ),
   LastName CIChar( 40 ),
   Password CIChar( 24 ),
   MemberGroup CIChar( 24 ),
   StoreCode CIChar( 3 ) );

EXECUTE PROCEDURE sp_CreateIndex90( 'People', 'People.adi', 'PK', 'eeUserName', '', 2051, 512, NULL );

INSERT INTO "People" VALUES( 'rodt@rydellchev.com', 'Rodney Troftgruben', 'Rodney', 'Troftgruben', 'password', 'ServiceWriter', 'RY1' );
INSERT INTO "People" VALUES( 'tbursinger@rydellchev.com', 'Travis Bursinger', 'Travis', 'Bursinger', 'password', 'ServiceWriter', 'RY1' );
INSERT INTO "People" VALUES( 'jtyrrell@rydellchev.com', 'John Tyrrell', 'John', 'Tyrrell', 'password', 'ServiceWriter', 'RY1' );
INSERT INTO "People" VALUES( 'kespelund@rydellchev.com', 'Kenneth Espelund', 'Kenneth', 'Espelund', 'password', 'ServiceWriter', 'RY1' );
INSERT INTO "People" VALUES( 'dwiebusch@rydellchev.com', 'Daniel Wiebusch', 'Daniel', 'Wiebusch', 'password', 'ServiceWriter', 'RY1' );
INSERT INTO "People" VALUES( 'mflikka@rydellchev.com', 'Matt Flikka', 'Matt', 'Flikka', 'password', 'ServiceWriter', 'RY1' );
INSERT INTO "People" VALUES( 'kcarlson@rydellchev.com', 'Ken Carlson', 'Ken', 'Carlson', 'password', 'ServiceWriter', 'RY1' );
INSERT INTO "People" VALUES( 'jdangerfield@gfhonda.com', 'Joel Dangerfield', 'Joel', 'Dangerfield', 'password', 'ServiceWriter', 'RY2' );
INSERT INTO "People" VALUES( 'dpederson@gfhonda.com', 'Dave Pederson', 'Dave', 'Pederson', 'password', 'ServiceWriter', 'RY2' );
INSERT INTO "People" VALUES( 'aneumann@gfhonda.com', 'Andrew Neumann', 'Andrew', 'Neumann', 'password', 'ServiceManager', 'RY2' );
INSERT INTO "People" VALUES( 'mhuot@rydellchev.com', 'Mike Huot', 'Mike', 'Huot', 'password', 'ServiceManager', 'RY1' );
INSERT INTO "People" VALUES( 'aberry@rydellchev.com', 'Al Berry', 'Al', 'Berry', 'password', 'ServiceManager', 'RY1' );
INSERT INTO "People" VALUES( 'bcahalan@rydellchev.com', 'Ben Cahalan', 'Ben', 'Cahalan', 'password', 'ServiceManager', 'RY1' );
INSERT INTO "People" VALUES( 'dbrekke@gfhonda.com', 'Devon Brekke', 'Devon', 'Brekke', 'password', 'ServiceMisc', 'RY2' );
INSERT INTO "People" VALUES( 'acrane@rydellcars.com', 'Aubrey Crane', 'Aubrey', 'Crane', 'password', 'DetailManager', 'RY1' );
INSERT INTO "People" VALUES( 'johnk@rydellchev.com', 'Jon Kazmierczak', 'Jon', 'Kazmierczak', 'password', 'ServiceWriter', 'RY1' );

EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('Position-Fulfillment2',
  'People','PositionFulfillment','FK2', 
  2,2, NULL,'',''); 

EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('People-WarrantyCalls',
  'People','WarrantyCalls','EEUSERNAME', 
  1,2, NULL,'','');  
     
ALTER PROCEDURE GetWritersForManager
   ( 
      ManagerEEUserName CICHAR ( 50 ),
      writerEEUserName CICHAR ( 50 ) OUTPUT,
      writerName CICHAR ( 60 ) OUTPUT
   ) 
BEGIN 
/*

execute procedure GetWritersForManager('dwiebusch@rydellchev.com');
execute procedure GetWritersForManager('aberry@rydellchev.com');
execute procedure GetWritersForManager('aneumann@gfhonda.com');

9/18 added ThruTS > now() to PositionFulfillment to address the "Wiebusch" problem
*/
DECLARE @eeusername string;
@eeusername = (SELECT ManagereeUserName FROM __input);

--SELECT top 20 trim(FirstName) + ' ' + TRIM(lastName), eeusername
--FROM Servicewriters
--ORDER BY firstname;

IF @eeusername IN ('mhuot@rydellchev.com','aberry@rydellchev.com','bcahalan@rydellchev.com') THEN 
  INSERT INTO __output 
  SELECT a.eeusername, a.fullname
  FROM people a
  INNER JOIN Positionfulfillment b ON a.eeusername = b.eeusername
    AND b.storecode = 'ry1'
    AND b.position = 'Service Writer'
	AND b.ThruTS > now();
ELSEIF @eeusername = 'aneumann@gfhonda.com' THEN 
  INSERT INTO __output
  SELECT a.eeusername, a.fullname
  FROM people a
  INNER JOIN Positionfulfillment b ON a.eeusername = b.eeusername
    AND b.storecode = 'ry2'
    AND b.position = 'Service Writer'
	AND b.ThruTS > now();
ELSE 
  INSERT INTO __output
  SELECT '','' FROM system.iota;
END IF; 

END;
