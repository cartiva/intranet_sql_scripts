ALTER PROCEDURE getCstfbDepartmentEmailScores
   ( 
      storeCode CICHAR ( 3 ),
      department CICHAR ( 12 ),
      transactions Integer OUTPUT,
      emailAddresses Integer OUTPUT,
      emailCaptureRate DOUBLE ( 2 ) OUTPUT
   ) 
BEGIN 
/*
depts: Sales, Service, MainShop, PDQ, BodyShop

7/21/14
  fb CASE 323: discrepancy BETWEEN displayed(market/store/dept) score 
    AND sparkline score (and the subsequent end point data for the
    expanded spark line graph) for the same market/store/dept
  sp.xfmCstfbEmailData populates tables cstfbEmailData AND cstfbEmailDataRolling, 
    but these stored procs (getCstfbMarketEmailScores, 
    getCstfbStoreEmailScores, getCstfbDepartmentEmailScores) for generating 
    the score are based on  cstfbEmailData and redo ALL the calculations 
    that are done IN the  populating of cstfbEmailDataRolling. 
    Redo these sp's to use that data rather than recalculating   

11/14/14
  break out new,used,detail
      
EXECUTE PROCEDURE getCstfbDepartmentEmailScores('ry1', 'detail');

*/
DECLARE @storeCode string;
DECLARE @department string;
@storeCode = (SELECT storeCode FROM __input);
@department = (SELECT UPPER(department) FROM __input);

INSERT INTO __output

select transactions, validEmail, emailCaptureRate
FROM cstfbEmailDataRolling
WHERE storecode = @storeCode
  AND 
    CASE @department
      WHEN 'SALES' THEN department = 'Sales' AND subdepartment = 'all'
      WHEN 'NEW' THEN subDepartment = 'NEW'
      WHEN 'USED' THEN subDepartment = 'USED'
      WHEN 'SERVICE' THEN department = 'Service' AND subdepartment = 'all'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
    END
  AND thrudate = (
    SELECT MAX(thrudate)
    FROM cstfbEmailDataRolling);  
END;


