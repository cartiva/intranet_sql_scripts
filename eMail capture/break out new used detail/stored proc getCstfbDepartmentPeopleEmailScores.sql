ALTER PROCEDURE getCstfbDepartmentPeopleEmailScores
   ( 
      storeCode CICHAR ( 3 ),
      department CICHAR ( 12 ),
      employee CICHAR ( 52 ) OUTPUT,
      transactions Integer OUTPUT,
      emailAddresses Integer OUTPUT,
      emailCaptureRate DOUBLE ( 2 ) OUTPUT
   ) 
BEGIN 
/*
depts: Sales, Service, MainShop, PDQ, BodyShop
this IS never called against service, just the sub depts

11/14/14 breakout new,used,detail (subdepartments)

EXECUTE PROCEDURE getCstfbDepartmentPeopleEmailScores('ry1', 'new');
*/
DECLARE @storeCode string;
DECLARE @department string;
@storeCode = (SELECT storeCode FROM __input);
@department = (SELECT UPPER(department) FROM __input);

IF @department = 'SALES' THEN 
  INSERT INTO __output
  SELECT employeeName, theCount, emails, round(100.0 * emails/theCount, 1)/100 
  FROM (
    select employeeName, storeCode, coalesce(COUNT(*), 0) AS theCount, 
      coalesce(SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END), 0) AS emails
    FROM cstfbEmailData
    WHERE storeCode = @storeCode
      AND transactionType = 'Sale'
      AND transactionDate BETWEEN curdate()-90 AND curdate()
    GROUP BY storeCode, transactionType, employeeName) a;
END IF;    

IF @department IN ('NEW','USED') THEN 
  INSERT INTO __output
  SELECT employeeName, theCount, emails, round(100.0 * emails/theCount, 1)/100 
  FROM (
    select employeeName, storeCode, coalesce(COUNT(*), 0) AS theCount, 
      coalesce(SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END), 0) AS emails
    FROM cstfbEmailData
    WHERE storeCode = @storeCode
      AND transactionType = 'Sale'
      AND transactionDate BETWEEN curdate()-90 AND curdate()
      AND subDepartment = 
        CASE @department
          WHEN 'NEW' THEN 'NEW'
          WHEN 'USED' THEN 'USED'
        END
    GROUP BY storeCode, transactionType, employeeName) a;
END IF;    

IF @department = 'SERVICE' THEN
  INSERT INTO __output
  SELECT employeeName, theCount, emails, round(100.0 * emails/theCount, 1)/100
  FROM (
    select employeeName, storeCode, COUNT(*) AS theCount, 
      SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS emails
    FROM cstfbEmailData
    WHERE storeCode = @storeCode
      AND transactionType = 'RO'
      AND transactionDate BETWEEN curdate()-90 AND curdate()
    GROUP BY storeCode, transactionType, employeeName) a;
END IF;  

IF @department IN ('MAINSHOP', 'PDQ', 'BODYSHOP','DETAIL') THEN 
INSERT INTO __output
SELECT employeeName, theCount, emails, round(100.0 * emails/theCount, 1)/100
FROM (
  select employeeName, storeCode, COUNT(*) AS theCount, 
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS emails
  FROM cstfbEmailData
  WHERE storeCode = @storeCode
    AND transactionType = 'RO'
    AND transactionDate BETWEEN curdate()-90 AND curdate()
    AND subDepartment = 
      CASE UPPER(@department)
        WHEN 'MAINSHOP' THEN 'MR'
        WHEN 'PDQ' THEN 'QL'
        WHEN 'BODYSHOP' THEN 'BS'
        WHEN 'DETAIL' THEN 'RE'
    END    
  GROUP BY storeCode, transactionType, subDepartment, employeeName) a;
END IF;  

END;



