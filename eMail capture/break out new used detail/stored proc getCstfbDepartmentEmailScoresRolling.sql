ALTER PROCEDURE getCstfbDepartmentEmailScoresRolling
   ( 
      storeCode CICHAR ( 3 ),
      department CICHAR ( 12 ),
      fromDate DATE OUTPUT,
      thruDate DATE OUTPUT,
      transactions Integer OUTPUT,
      validEmail Integer OUTPUT,
      emailCaptureRate DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
11/14/14  break out new,used,detail

EXECUTE PROCEDURE getCstfbDepartmentEmailScoresRolling('RY1','detail');

*/
DECLARE @store string;
DECLARE @department string;

@store = (SELECT StoreCode FROM __input);
@department = upper((SELECT Department FROM __input));
INSERT INTO __output
SELECT top 100 fromDate, thruDate, transactions, validEmail, emailCaptureRate
FROM cstfbEmailDataRolling
WHERE storeCode = @store
  AND thruDate IN (
    SELECT top 12 thedate
    FROM dds.day
    WHERE thedate between curdate() - 90 AND curdate()
      AND mod(curdate() - thedate - 7, 7) = 0
    ORDER BY theDate DESC)  
  AND 
    CASE @department
      WHEN 'SALES' THEN department = 'Sales' AND subDepartment = 'All'
      WHEN 'NEW' THEN subDepartment = 'NEW'
      WHEN 'USED' THEN subDepartment = 'USED'
      WHEN 'SERVICE' THEN department = 'Service' AND subDepartment = 'All'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
    END
ORDER BY thruDate;         




END;


