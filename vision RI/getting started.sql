ken espelund wants his vision username changed to rydellcars FROM rydellchev
-- exising ri on tpEmployees prevent this
delete
FROM tpEmployees
WHERE firstname = 'jon'

-- refer to smartdraw: sco-2
-- due to bad ri, simply updating tpEmployees will NOT UPDATE a shit load of tables
SELECT *
FROM warrantyros a
WHERE NOT EXISTS (
  SELECT 1
  FROM tpemployees
  WHERE username = a.username)
  
SELECT *
FROM agedros a
WHERE NOT EXISTS (
  SELECT 1
  FROM tpemployees
  WHERE username = a.username)  
  
-- at least, no eeusername still IN use  
SELECT parent
FROM system.columns
WHERE name = 'eeusername'  

SELECT left(parent, 50)
FROM system.columns
WHERE name = 'username'
  AND parent NOT LIKE 'zUnused%'
  
SELECT LEFT(a.name, 35) AS name, LEFT(a.ri_primary_table,25) AS primaryTable,
  LEFT(a.ri_foreign_table,25) AS foreignTable, LEFT(a.ri_foreign_index, 25) AS foreignIndex,
  CASE a.ri_updaterule  WHEN 2 THEN 'Restrict' ELSE 'Cascade' END AS updRule,
  CASE a.ri_deleterule  WHEN 2 THEN 'Restrict' ELSE 'Cascade' END AS delRule,
  b.index_expression AS forIndexExpression
--SELECT *  
FROM system.relations a
LEFT JOIN system.indexes b on a.ri_foreign_table = b.parent AND a.ri_foreign_index = b.name
ORDER BY primaryTable

-- does renaming a TABLE rename the TABLE IN system.indexes, relations ???