/*
      Sent CICHAR ( 12 ) OUTPUT,
      Returned CICHAR ( 12 ) OUTPUT,
      ResponseRate CICHAR ( 12 ) OUTPUT,
      Score CICHAR ( 12 ) OUTPUT
*/      
-- get market scores
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = (
  SELECT MAX(thedate) - 90 AS fromDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());	
@thruDate = (
  SELECT MAX(thedate) AS thruDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate()); 
SELECT COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate;
  
-- get storescores  
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @store string;
@store = 'RY2';
@fromDate = (
  SELECT MAX(thedate) - 90 AS fromDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());	
@thruDate = (
  SELECT MAX(thedate) AS thruDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate()); 
SELECT COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = @store;
    
-- get departmentscores    
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @store string;
DECLARE @department string;
@store = 'RY1';
@department = 'PDQ';
@fromDate = (
  SELECT MAX(thedate) - 90 AS fromDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());	
@thruDate = (
  SELECT MAX(thedate) AS thruDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate()); 
SELECT COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = @store
  AND 
    CASE @department
      WHEN 'SALES' THEN transactionType = 'Sales'
      WHEN 'SERVICE' THEN transactionType = 'Service'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
    END;
    
    
-- get departmentpeoplescores    
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @store string;
DECLARE @department string;
@store = 'RY1';
@department = 'PDQ';
@fromDate = (
  SELECT MAX(thedate) - 90 AS fromDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());	
@thruDate = (
  SELECT MAX(thedate) AS thruDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate()); 
SELECT employeeName, COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,    
  CASE COUNT(*)
    WHEN 0 THEN 0
    ELSE 
      round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * .01/COUNT(*)), 2) 
  END AS responseRate,
  CASE SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
    WHEN 0 THEN 0
    ELSE 
      (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
        100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
      -
      SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
        /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0
  END  AS Score      
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = @store
  AND 
    CASE @department
      WHEN 'SALES' THEN transactionType = 'Sales'
      WHEN 'SERVICE' THEN transactionType = 'Service'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
    END
GROUP BY employeeName;       