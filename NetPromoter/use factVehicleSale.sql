-- get some good employee names

  SELECT a.ro, max(b.name) AS name, max(b.censusdept) AS censusdept
  FROM dds.factRepairOrder a
  INNER JOIN dds.dimServiceWriter b on a.ServiceWriterKey = b.ServiceWriterKey
  WHERE a.ro IN (
    SELECT ronumber
    FROM xfmDigitalAirStrike
    WHERE CAST(surveysentts AS sql_date) > curdate() - 90
      AND year(CAST(surveyresponsets AS sql_date)) <> 1969)
  GROUP BY a.RO
  

-- writer names
SELECT a.ro, b.censusdept, TRIM(c.firstname) + ' ' + TRIM(c.lastName) AS name
FROM dds.factRepairOrder a
INNER JOIN dds.dimServiceWriter b on a.serviceWriterKey = b.serviceWriterKey
INNER JOIN dds.edwEmployeeDim c on b.employeeNumber = c.employeenumber
  AND c.currentrow = true
WHERE a.ro IN (
  SELECT ronumber
  FROM xfmDigitalAirStrike
  WHERE CAST(surveysentts AS sql_date) > curdate() - 90
    AND year(CAST(surveyresponsets AS sql_date)) <> 1969)
GROUP BY a.ro, b.censusdept, TRIM(c.firstname) + ' ' + TRIM(c.lastName)	

-- sales use factVehicleSale
SELECT 
  CASE a.TransactionType
    WHEN 'Sales' THEN c.name
    WHEN 'Service' THEN d.name
  END AS Employee, 
  cast(a.SurveySentTS AS sql_date) AS Date,
  CASE a.TransactionType
    WHEN 'Sales' THEN c.stocknumber
    WHEN 'Service' THEN a.ronumber
  END AS "RO/Stock",
  b.CustomerName, a.ReferralLikelihood, b.CustomerExperience
FROM (
  SELECT DealerName, vin, ronumber, TransactionType, 
    MAX(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
    MAX(ReferralLikelihood) AS ReferralLikelihood
  -- SELECT *
  FROM xfmDigitalAirStrike
  WHERE CAST(surveysentts AS sql_date) > curdate() - 90
    AND year(CAST(surveyresponsets AS sql_date)) <> 1969
  GROUP BY DealerName, vin, ronumber, TransactionType) a
LEFT JOIN xfmDigitalAirStrike b on a.Dealername = b.Dealername AND a.vin = b.vin
  AND a.ronumber = b.ronumber AND a.SurveySentTS = b.SurveySentTS  
LEFT JOIN (  
  SELECT a.storecode, b.vin, a.stocknumber, c.fullname AS name
  FROM dds.factVehicleSale a
  INNER JOIN dds.dimVehicle b on a.vehicleKey =  b.vehicleKey 
  INNER JOIN dds.dimSalesPerson c on a.consultantkey = c.salespersonkey) c on a.vin = c.vin AND iif(a.DealerName = 'Honda of Grand Forks', 'RY2', 'RY1') = c.storecode
LEFT JOIN (
  SELECT a.ro, b.censusdept, TRIM(c.firstname) + ' ' + TRIM(c.lastName) AS name
  FROM dds.factRepairOrder a
  INNER JOIN dds.dimServiceWriter b on a.serviceWriterKey = b.serviceWriterKey
  INNER JOIN dds.edwEmployeeDim c on b.employeeNumber = c.employeenumber
    AND c.currentrow = true
  WHERE a.ro IN (
    SELECT ronumber
    FROM xfmDigitalAirStrike
    WHERE CAST(surveysentts AS sql_date) > curdate() - 90
      AND year(CAST(surveyresponsets AS sql_date)) <> 1969)
  GROUP BY a.ro, b.censusdept, TRIM(c.firstname) + ' ' + TRIM(c.lastName)) d on a.ronumber = d.ro	  
  
  
  SELECT a.ro, max(b.censusdept), max(TRIM(c.firstname) + ' ' + TRIM(c.lastName)) AS name
  FROM dds.factRepairOrder a
  INNER JOIN dds.dimServiceWriter b on a.serviceWriterKey = b.serviceWriterKey
  INNER JOIN dds.edwEmployeeDim c on b.employeeNumber = c.employeenumber
    AND c.currentrow = true
  WHERE a.ro IN (
    SELECT ronumber
    FROM xfmDigitalAirStrike
    WHERE CAST(surveysentts AS sql_date) > curdate() - 90
      AND year(CAST(surveyresponsets AS sql_date)) <> 1969)
  GROUP BY a.ro
  
-- 2/16/14
-- this looks LIKE it will WORK for sp.GetDepartmentPeopleScores, at least for sales
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @store string;
DECLARE @department string;
@fromDate = '11/18/2013';
@thruDate = '02/16/2014';
@store = 'RY2';
@department = 'Sales';

  SELECT x.Name,
    cast(COUNT(*) AS sql_char) AS Sent,
    cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
    trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
    CASE 
      WHEN SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) = 0 THEN '0%'
      ELSE 
      trim(CAST( 
        -- Promoters
        SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
          100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
          THEN 1 ELSE 0 END) 
        -    
        -- Detractors
        SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
          THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
          THEN 1 ELSE 0 END) AS sql_char)) + '%' 
     END AS NetPromoterScore
  FROM (     
    SELECT DealerName, a.vin, ronumber, TransactionType, CustomerName, 
      max(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
      MAX(ReferralLikelihood) AS ReferralLikelihood,
--       b.bqname AS Name
      c.name
    FROM xfmDigitalAirStrike a
    LEFT JOIN (
--      SELECT bmco#,bmvin, max(b.bqname) AS bqname
--      FROM dds.stgArkonaBOPMAST a
--      LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
--        AND b.bqtype = 'S'
--      WHERE year(bmdtcap) = 2013  
--        AND TRIM(bmvin) <> '' 
--      GROUP BY bmco#,bmvin) b on a.vin = b.bmvin AND iif(a.DealerName = 'Honda of Grand Forks', 'RY2', 'RY1') = b.bmco#
    SELECT m.storecode, n.vin, o.fullNAme AS Name
    FROM dds.factVehicleSale m
    INNER JOIN dds.dimVehicle n on m.vehicleKey = n.vehicleKey
    INNER JOIN dds.dimSalesPerson o on m.consultantKey = o.salesPersonKey)  c on a.vin = c.vin 
      AND iif(a.DealerName = 'Honda of Grand Forks', 'RY2', 'RY1') = c.storecode
      
	WHERE CAST(surveysentts AS sql_date) BETWEEN @fromDate AND @thruDate
      AND TransactionType = @department
      AND DealerName = 
        CASE
         WHEN @store = 'RY1' THEN 'Rydell Chevrolet Buick GMC Cadillac'
          WHEN @store = 'RY2' THEN 'Honda of Grand Forks'
        END      
    GROUP BY DealerName, a.vin, ronumber, TransactionType, CustomerName, c.name/*b.bqname*/) x
  GROUP BY x.Name;