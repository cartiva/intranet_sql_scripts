DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '04/01/2014';
@thruDate = '06/30/2014';
-- INSERT INTO __output
SELECT 1, 'Grand Forks' AS Market, '' as Store, '' as Department,
  '' as subDepartment, COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS Score
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
UNION 
SELECT 2, 'Grand Forks', 'GM', '', '', COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = 'RY1'
UNION   
SELECT 3, 'Grand Forks','GM', 'Sales','', COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = 'RY1'
  AND transactionType = 'Sales'
UNION   
SELECT 4, 'Grand Forks','GM', 'Service','', COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = 'RY1'
  AND transactionType = 'Service'  
UNION   
SELECT 6, 'Grand Forks','GM', 'Service','PDQ', COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = 'RY1'
  AND subDepartment = 'QL'
UNION   
SELECT 7, 'Grand Forks','GM', 'Service','Main Shop', COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = 'RY1'
  AND subDepartment = 'MR'  
UNION   
SELECT 8, 'Grand Forks','GM', 'Service','Body Shop', COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = 'RY1'
  AND subDepartment = 'bs'  
UNION   
SELECT 9, 'Grand Forks', 'Honda', '', '', COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = 'RY2'  
UNION   
SELECT 10, 'Grand Forks','Honda', 'Sales','', COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = 'RY2'
  AND transactionType = 'Sales'  
UNION   
SELECT 11, 'Grand Forks','Honda', 'Service','', COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = 'RY2'
  AND transactionType = 'Service'    
UNION   
SELECT 12, 'Grand Forks','Honda', 'Sales','PDQ', COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = 'RY2'
  AND subDepartment = 'QL'    
UNION   
SELECT 13, 'Grand Forks','Honda', 'Sales','Main Shop', COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = 'RY2'
  AND subDepartment = 'MR'   
  
  
