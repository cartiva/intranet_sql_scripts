ALTER PROCEDURE GetDepartmentPeopleScores
   ( 
      StoreCode CICHAR ( 3 ),
      Department CICHAR ( 12 ),
      Name CICHAR ( 52 ) OUTPUT,
      Sent Integer OUTPUT,
      Returned Integer OUTPUT,
      ResponseRate DOUBLE ( 2 ) OUTPUT,
      Score DOUBLE ( 2 ) OUTPUT
   ) 
BEGIN 
/*
2/16/14: fixed sales names
         service names
         service subdept names
11/14/14
  break out new AND used    
  service: detail        
    
EXECUTE PROCEDURE GetDepartmentPeopleScores('RY1', 'detail');

*/
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @store string;
DECLARE @department string;

@store = (SELECT StoreCode FROM __input);
@department = UPPER((SELECT Department FROM __input));

@fromDate = (
  SELECT MAX(thedate) - 89 AS fromDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());
	
@thruDate = (
  SELECT MAX(thedate) AS thruDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());	

INSERT INTO __output
SELECT employeeName, COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,    
  CASE COUNT(*)
    WHEN 0 THEN 0
    ELSE 
      round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * .01/COUNT(*)), 2) 
  END AS responseRate,
  CASE SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
    WHEN 0 THEN 0
    ELSE 
      (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
        100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
      -
      SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
        /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0
  END  AS Score      
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND storeCode = @store
  AND 
    CASE @department
      WHEN 'SALES' THEN transactionType = 'Sales'
      WHEN 'NEW' THEN subDepartment = 'NEW'
      WHEN 'USED' THEN subDepartment = 'USED'
      WHEN 'SERVICE' THEN transactionType = 'Service'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
    END
GROUP BY employeeName;  

END;

