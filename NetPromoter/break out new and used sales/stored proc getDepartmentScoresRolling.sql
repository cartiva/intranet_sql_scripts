alter PROCEDURE GetDepartmentScoresRolling
   ( 
      StoreCode CICHAR ( 3 ),
      Department CICHAR ( 12 ),
      fromDate DATE OUTPUT,
      thruDate DATE OUTPUT,
      ResponseRate DOUBLE ( 2 ) OUTPUT,
      Score DOUBLE ( 2 ) OUTPUT
   ) 
BEGIN 
/*
11/14/14
  break out sales: new AND used   
  service: detail    
  
EXECUTE PROCEDURE GetDepartmentScoresRolling('RY1', 'detail');

*/
DECLARE @store string;
DECLARE @department string;

@store = (SELECT StoreCode FROM __input);
@department = upper((SELECT Department FROM __input));

INSERT INTO __output
SELECT top 100 fromDate, thruDate, responseRate, score
FROM cstfbNpDataRolling
WHERE storeCode = @store
  AND thruDate IN (
    SELECT top 12 theDate as mondays
    FROM dds.day
    WHERE dayofWeek = 2
      AND theDate <= curdate()
    ORDER BY theDate DESC)
  AND 
    CASE @department
      WHEN 'SALES' THEN department = 'Sales' AND subDepartment = 'All'
      WHEN 'NEW' THEN subDepartment = 'NEW'
      WHEN 'USED' THEN subDepartment = 'USED'
      WHEN 'SERVICE' THEN department = 'Service' AND subDepartment = 'All'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
    END
ORDER BY thruDate;	




END;


