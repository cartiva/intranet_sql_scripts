SELECT left(c.stocknumber, 1), MIN(surveySentTs), MAX(surveySentTS), COUNT(*)
FROM dashistory a 
INNER JOIN dds.dimVehicle b on a.vin = b.vin
INNER JOIN dds.factVehicleSale c on b.vehicleKey = c.vehicleKey
INNER JOIN dds.dimCarDealInfo d on c.carDealInfoKey = d.carDealInfoKey
WHERE a.transactiontype = 'sales'
  AND d.vehicleType = 'Used'
GROUP BY left(c.stocknumber, 1)  

SELECT *
FROM dds.factVehicleSale