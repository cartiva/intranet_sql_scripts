ALTER PROCEDURE GetDepartmentScores
   ( 
      StoreCode CICHAR ( 3 ),
      Department CICHAR ( 12 ),
      Sent Integer OUTPUT,
      Returned Integer OUTPUT,
      ResponseRate DOUBLE ( 2 ) OUTPUT,
      Score DOUBLE ( 2 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetDepartmentScores('RY1', 'detail');

12/15/13
  modified to used a fixed date range
  
7/21/14
  fb CASE 323: discrepancy BETWEEN displayed(market/store/dept) score 
    AND sparkline score (and the subsequent end point data for the
    expanded spark line graph) for the same market/store/dept
  sp.populateCstfbNpData populates tables cstfbNpData AND cstfbNpDataRolling, 
    but these stored procs (getMarketScores, getStoreScores, 
    getDepartmentScores) for generating the score are based on
    cstfbNpData and redo ALL the calculations that are done IN the 
    populating of cstfbNpDataRolling. Redo these sp's to use that data 
    rather than recalculating   
11/14/14
  break out sales: new AND used  
  service: detail  
*/
DECLARE @store string;
DECLARE @department string;

@store = (SELECT StoreCode FROM __input);
@department = upper((SELECT Department FROM __input));

INSERT INTO __output

SELECT sent, returned, responserate, score
FROM cstfbNpDataRolling
WHERE storecode = @store
  AND 
    CASE @department
      WHEN 'SALES' THEN department = 'Sales' AND subdepartment = 'ALL'
      WHEN 'NEW' THEN subdepartment = 'NEW'
      WHEN 'USED' THEN subdepartment = 'USED'
      WHEN 'SERVICE' THEN department = 'Service' AND subdepartment = 'ALL'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
    END
  AND thrudate = (
    SELECT MAX(thrudate)
    FROM cstfbNpDataRolling);    

END;

