-- nightly UPDATE
-- flag hours
  SELECT storecode, fromDate, thruDate, storecode, flagDeptCode, round(SUM(flagHours), 2) AS flaghours
  FROM ( -- previous 14 days
    SELECT thedate - 13 AS fromDate, thedate AS thruDate
    FROM dds.day
    WHERE thedate = curdate() -1) a
  LEFT JOIN (
    SELECT a.storecode, thedate, flaghours, c.flagdeptcode
    FROM dds.factrepairorder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimtech c on a.techkey = c.techkey
    WHERE b.thedate BETWEEN curdate() - 13 AND curdate() -1
      AND c.flagdeptcode NOT IN ('QL','NA')) b on b.thedate BETWEEN a.fromDate AND a.thruDate 
  GROUP BY fromDate, thruDate, storecode, flagDeptCode
  
-- clockhours
  SELECT storecode, fromDate, thruDate, storecode, flagDeptCode, round(SUM(clockhours), 2) AS clockHours
  FROM (
    SELECT thedate - 13 AS fromDate, thedate AS thruDate
    FROM dds.day
    WHERE thedate = curdate() -1) m
  LEFT JOIN (
    SELECT d.*, f.clockhours, g.thedate
    FROM ( -- employeenumbers with flagtime IN the last 14 days
      SELECT a.storecode, c.employeenumber, c.flagdeptcode
      FROM dds.factrepairorder a
      INNER JOIN dds.dimtech c on a.techkey = c.techkey
      WHERE c.flagdeptcode NOT IN ('QL','NA')
        AND c.employeenumber <> 'NA' 
        AND a.flagdatekey IN ( -- 14 days
          SELECT datekey
      	  FROM dds.day
      	  WHERE thedate BETWEEN curdate() - 13 AND curdate() -1) 
      GROUP BY a.storecode, c.employeenumber, c.flagdeptcode) d 
    LEFT JOIN dds.edwEmployeeDim e on d.storecode = e.storecode
      AND d.employeenumber = e.employeenumber
    LEFT JOIN dds.edwClockHoursFact f on e.employeekey = f.employeekey
    LEFT JOIN dds.day g on f.datekey = g.datekey
    WHERE g.thedate BETWEEN curdate() - 13 AND curdate() -1) n on n.thedate BETWEEN m.fromdate AND m.thruDate  
  GROUP BY fromDate, thruDate, storecode, flagDeptCode  
  
-- 5/18
getting ALL goofy on the dates
so, define exactly what it IS i want
each night, i want the previous 14 days
so, on 5/18, i want 5/4 -> 5/17  



so this IS giving 1893 mr hours for 5/4 -> 5/17
-- clockhours
  SELECT storecode, fromDate, thruDate, storecode, flagDeptCode, round(SUM(clockhours), 2) AS clockHours
  FROM (
    SELECT thedate - 13 AS fromDate, thedate AS thruDate
    FROM dds.day
    WHERE thedate = curdate() -1) m
  LEFT JOIN (
    SELECT d.*, f.clockhours, g.thedate
    FROM ( -- employeenumbers with flagtime IN the last 14 days
      SELECT a.storecode, c.employeenumber, c.flagdeptcode
      FROM dds.factrepairorder a
      INNER JOIN dds.dimtech c on a.techkey = c.techkey
      WHERE c.flagdeptcode NOT IN ('QL','NA')
        AND c.employeenumber <> 'NA' 
        AND a.flagdatekey IN ( -- 14 days
          SELECT datekey
      	  FROM dds.day
      	  WHERE thedate BETWEEN curdate() - 13 AND curdate() -1) 
      GROUP BY a.storecode, c.employeenumber, c.flagdeptcode) d 
    LEFT JOIN dds.edwEmployeeDim e on d.storecode = e.storecode
      AND d.employeenumber = e.employeenumber
    LEFT JOIN dds.edwClockHoursFact f on e.employeekey = f.employeekey
    LEFT JOIN dds.day g on f.datekey = g.datekey
    WHERE g.thedate BETWEEN curdate() - 13 AND curdate() -1) n on n.thedate BETWEEN m.fromdate AND m.thruDate  
  GROUP BY fromDate, thruDate, storecode, flagDeptCode  
  
but this IS giving me 1978.27 for mr same period
select * FROM (
  SELECT storecode, fromDate, thruDate, storecode, flagDeptCode, round(SUM(clockhours), 2) AS clockHours
  FROM (
    SELECT thedate - 13 AS fromDate, thedate AS thruDate
    FROM dds.day
    WHERE thedate BETWEEN  curdate() - 120 AND curdate() -1) m
  LEFT JOIN (
    SELECT d.*, f.clockhours, g.thedate
    FROM (
      SELECT a.storecode, c.employeenumber, c.flagdeptcode
      FROM dds.factrepairorder a
      INNER JOIN dds.dimtech c on a.techkey = c.techkey
      WHERE c.flagdeptcode NOT IN ('QL','NA')
        AND c.employeenumber <> 'NA' 
        AND a.flagdatekey IN (
          SELECT datekey
      	FROM dds.day
      	WHERE thedate BETWEEN '01/08/2014' AND curdate() -1) 
      GROUP BY a.storecode, c.employeenumber, c.flagdeptcode) d 
    LEFT JOIN dds.edwEmployeeDim e on d.storecode = e.storecode
      AND d.employeenumber = e.employeenumber
    LEFT JOIN dds.edwClockHoursFact f on e.employeekey = f.employeekey
    LEFT JOIN dds.day g on f.datekey = g.datekey
    WHERE g.thedate BETWEEN '01/08/2014' AND curdate()) n on n.thedate BETWEEN m.fromdate AND m.thruDate  
  GROUP BY fromDate, thruDate, storecode, flagDeptCode  
) x WHERE fromdate = '05/04/2014'  

why the difference