/*
4/22/14
FROM greg: for each 2 week period
	 	   the number of full time techs
		   avg clock hours/day for the period, excluding weekends
      a rough actual capacity number
*/	  		   
SELECT *
FROM (
  SELECT a.storecode, c.employeenumber, c.flagdeptcode
  FROM dds.factrepairorder a
  INNER JOIN dds.dimtech c on a.techkey = c.techkey
  WHERE c.flagdeptcode NOT IN ('QL','NA')
    AND c.employeenumber <> 'NA' 
    AND a.flagdatekey IN (
      SELECT datekey
  	FROM dds.day
  	WHERE thedate BETWEEN curdate() - 90 AND curdate() -1) 
  GROUP BY a.storecode, c.employeenumber, c.flagdeptcode) f
LEFT JOIN dds.edwEmployeeDim e on f.storecode = e.storecode 
 AND f.employeenumber = e.employeenumber 
 AND e.currentrow = true
 
 
 SELECT *
 FROM dds.dimtech
 WHERE employeenumber = '167810'
 

SELECT b.thedate, a.*
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.opendatekey = b.datekey
WHERE a.techkey IN (
  SELECT techkey
  FROM dds.dimtech
  WHERE employeenumber = '167810')
 
SELECT ro, line, flaghours, a.techkey, b.description, c.thedate
FROM dds.factRepairORder a
LEFT JOIN dds.dimtech b on a.techkey = b.techkey
LEFT JOIN dds.day c on a.flagdatekey = c.datekey
WHERE ro = '16144549'
ORDER BY line
 
SELECT *
FROM dds.stgarkonasdpxtim 
WHERE ptro# = '16144549'

  
 