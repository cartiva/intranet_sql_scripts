-- need to use writer to get detail
-- 12/16/14  ADD ry2
-- DROP TABLE #ros;
SELECT e.thedate, e.storecode, e.ro, e.area,e.subArea, f.bnadr1 AS Address, f.bncity AS City, f.bnstcd AS State, 
  f.bnzip AS Zip
INTO #ros  
FROM (
  SELECT a.ro, a.storecode,
    MAX(CASE d.defaultServiceType
      WHEN 'MR' THEN 'Service'
      WHEN 'AM' THEN 'Service'
      WHEN 'BS' THEN 'BodyShop'
      WHEN 'QL' THEN 'PDQ'
      WHEN 'RE' THEN 'Detail'
      else 'XXXX'
    END) as area, '' AS subArea,
    b.thedate, c.bnkey--, a.storecode
  FROM dds.factRepairOrder a
  INNER JOIN dds.day  b on a.opendatekey = datekey
  INNER JOIN dds.dimCustomer c on a.customerKey = c.customerKey
--    AND a.storecode = c.storecode -- fuck me, dimCustomer IS ALL RY1 !?!?
  INNER JOIN dds.dimServiceWriter d on a.serviceWriterKey = d.serviceWriterKey
--  WHERE a.storecode = 'ry1'
  WHERE b.theyear = 2014
    AND a.customerKey NOT IN (2,9674,168853,181502,173797,164742,9019,31263) -- eliminate inventory, shop time, ry1, carry in
    AND a.paymentTypeKey <> ( -- eliminate interal
      SELECT PaymentTypeKey
      FROM dds.dimPaymentType
      WHERE PaymentTypeCode = 'I')
  GROUP BY a.ro, a.storecode, b.thedate, c.bnkey, a.storecode) e
LEFT JOIN dds.stgArkonaBOPNAME f on e.bnkey = f.bnkey
--  AND e.storecode = f.bnco# this seems to fuck up the ry2
WHERE e.area <> 'XXXX'
  AND f.bnadr1 IS NOT NULL 
  AND f.bnadr1 <> '';

SELECT storecode, COUNT(*) FROM #ros GROUP BY storecode
-- sales
-- DROP TABLE #sales;
select c.thedate, a.storecode, a.stocknumber, b.vehicleType AS area, b.saleType AS subArea,
  e.bnadr1 AS Address, e.bncity AS City, e.bnstcd AS State, e.bnzip AS Zip
INTO #sales
FROM dds.factVehicleSale a
INNER JOIN dds.dimCarDealInfo b on a.carDealInfoKey = b.carDealInfoKey
  AND b.saletypeCode <> 'W'
INNER JOIN dds.day c on a.approvedDateKey = c.datekey
INNER JOIN dds.dimCustomer d on a.buyerKey = d.customerKey
  AND d.customerKey NOT IN (168853,9019) -- elim ry1
INNER JOIN dds.stgArkonaBOPNAME e on d.bnkey = e.bnkey
--WHERE a.storecode = 'ry1'
WHERE c.theyear = 2014
  AND e.bnadr1 IS NOT NULL
  AND e.bnadr1 <> ''
  AND a.stocknumber <> '';

DROP TABLE heatmap_all;  
CREATE TABLE heatmap_all (
  theDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  identifier cichar(9)constraint NOT NULL,
  area cichar(8)constraint NOT NULL,
  subArea cichar(12),
  address cichar(60)constraint NOT NULL,
  city cichar(20)constraint NOT NULL,
  state cichar(2)constraint NOT NULL,
  zip cichar(9)constraint NOT NULL,
  constraint pk primary key (identifier));
  
delete FROM heatmap_all;   
INSERT INTO heatmap_all
SELECT *
FROM (
  SELECT * FROM #sales
  UNION ALL
  SELECT * FROM #ros) a
WHERE stocknumber NOT IN (-- occasional annoying dup
  SELECT stocknumber 
  FROM (
    SELECT * FROM #sales
    UNION ALL
    SELECT * FROM #ros) c
  GROUP BY stocknumber 
  HAVING COUNT(*) > 1 );
 
DROP TABLE heatmap_unique_addresses;      
CREATE TABLE heatmap_unique_addresses (
  address cichar(60)constraint NOT NULL,
  city cichar(20)constraint NOT NULL,
  state cichar(2)constraint NOT NULL,
  zip cichar(9)constraint NOT NULL,
  constraint pk primary key (address,city,state,zip));  

INSERT INTO heatmap_unique_addresses  
SELECT address, city, state, zip
FROM heatmap_all      
GROUP BY address, city, state, zip;

DROP TABLE heatmap_bodyshop;
CREATE TABLE heatmap_bodyshop (
  theDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  identifier cichar(9)constraint NOT NULL,
  area cichar(8)constraint NOT NULL,
  subArea cichar(12),
  address cichar(60)constraint NOT NULL,
  city cichar(20)constraint NOT NULL,
  state cichar(2)constraint NOT NULL,
  zip cichar(9)constraint NOT NULL,
  constraint pk primary key (identifier));
INSERT INTO heatmap_bodyshop
select * FROM heatmap_all WHERE area = 'bodyshop';  

DROP TABLE heatmap_service;  
CREATE TABLE heatmap_service (
  theDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  identifier cichar(9)constraint NOT NULL,
  area cichar(8)constraint NOT NULL,
  subArea cichar(12),
  address cichar(60)constraint NOT NULL,
  city cichar(20)constraint NOT NULL,
  state cichar(2)constraint NOT NULL,
  zip cichar(9)constraint NOT NULL,
  constraint pk primary key (identifier));  
  
INSERT INTO heatmap_service
select * FROM heatmap_all WHERE area = 'service';      

DROP TABLE heatmap_pdq;
CREATE TABLE heatmap_pdq (
  theDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  identifier cichar(9)constraint NOT NULL,
  area cichar(8)constraint NOT NULL,
  subArea cichar(12),
  address cichar(60)constraint NOT NULL,
  city cichar(20)constraint NOT NULL,
  state cichar(2)constraint NOT NULL,
  zip cichar(9)constraint NOT NULL,
  constraint pk primary key (identifier)); 
INSERT INTO heatmap_pdq
select * FROM heatmap_all WHERE area = 'pdq';   

DROP TABLE heatmap_detail;  
CREATE TABLE heatmap_detail (
  theDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  identifier cichar(9)constraint NOT NULL,
  area cichar(8)constraint NOT NULL,
  subArea cichar(12),
  address cichar(60)constraint NOT NULL,
  city cichar(20)constraint NOT NULL,
  state cichar(2)constraint NOT NULL,
  zip cichar(9)constraint NOT NULL,
  constraint pk primary key (identifier));   
INSERT INTO heatmap_detail
select * FROM heatmap_all WHERE area = 'detail';   
 
DROP TABLE heatmap_new;  
CREATE TABLE heatmap_new (
  theDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  identifier cichar(9)constraint NOT NULL,
  area cichar(8)constraint NOT NULL,
  subArea cichar(12),
  address cichar(60)constraint NOT NULL,
  city cichar(20)constraint NOT NULL,
  state cichar(2)constraint NOT NULL,
  zip cichar(9)constraint NOT NULL,
  constraint pk primary key (identifier));  
INSERT INTO heatmap_new
select * FROM heatmap_all WHERE area = 'new';    

DROP TABLE heatmap_used;  
CREATE TABLE heatmap_used (
  theDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  identifier cichar(9)constraint NOT NULL,
  area cichar(8)constraint NOT NULL,
  subArea cichar(12),
  address cichar(60)constraint NOT NULL,
  city cichar(20)constraint NOT NULL,
  state cichar(2)constraint NOT NULL,
  zip cichar(9)constraint NOT NULL,
  constraint pk primary key (identifier)); 
INSERT INTO heatmap_used
select * FROM heatmap_all WHERE area = 'used';    