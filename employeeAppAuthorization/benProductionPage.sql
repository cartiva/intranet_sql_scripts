INSERT INTO applicationMetaData values (
  'teampay', -- appName
  'tp', -- appCode,
  100,-- appSeq
  'manager', -- appRole
  'production summary', -- functionality
  'sublink_mainshop_production_summary_html',-- configMapMarkupKey
  'stp.manager.summary',-- pubSub
  'leftnavbar',-- navType
  'production summary',-- navText
  100-- navSeq
  ); 

/*  
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, 18
FROM tpEmployees a, applicationMetaData b
WHERE a.username = 'mhuot@rydellchev.com'
  AND b.appcode = 'tp'
  AND b.approle = 'manager' 
  AND b.functionality = 'production summary'
  AND b.navSeq = 100;
  
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, 18
FROM tpEmployees a, applicationMetaData b
WHERE a.username = 'rsattler@rydellchev.com'
  AND b.appcode = 'tp'
  AND b.approle = 'manager' 
  AND b.functionality = 'production summary'
  AND b.navSeq = 100;
*/  
/*
DELETE FROM employeeAppAuthorization
WHERE username = 'mhuot@rydellchev.com'
  AND appcode = 'tp'
  AND functionality = 'production summary'
  AND appseq = 100;
  
DELETE FROM employeeAppAuthorization
WHERE username = 'rsattler@rydellchev.com'
  AND appcode = 'tp'
  AND approle = 'manager' 
  AND functionality = 'production summary';  
  
delete from applicationMetaData 
WHERE appcode =  'tp'
  AND approle = 'manager'
  and functionality = 'production summary'
  AND appseq = 100;
*/

/*
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, 18
FROM tpEmployees a, applicationMetaData b
WHERE a.username = 'rsattler@rydellchev.com'
  AND b.appcode = 'tp'
  AND b.approle = 'manager' 
  AND b.functionality = 'production summary'
  AND b.navSeq = 100;
*/
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, 18 
FROM tpEmployees a, applicationMetaData b
WHERE (
  a.username = 'mhuot@rydellchev.com' OR
  a.username = 'rsattler@rydellchev.com' or
  firstname = 'trent' OR
  firstname = 'jeri' OR
  (firstname = 'benjamin' AND lastname <> 'dalen') OR
  username = 'brian@rydellchev.com' OR
  firstname = 'andrew' or
  lastname = 'steinke' OR
  lastname = 'espelund' OR
  lastname = 'sorum')
  AND b.appcode = 'tp'
  AND b.approle = 'manager' 
  AND b.functionality = 'production summary'
  AND b.navSeq = 100;   