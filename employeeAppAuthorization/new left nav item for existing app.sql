-- completed warranty calls

-- app meta data

INSERT INTO applicationMetaData
SELECT DISTINCT appname, appcode, appseq, approle, 'completed warranty calls',
  'sublink_sco_mgr_completed_calls_html', 'sco.manager.completedCalls',
  'leftnavbar','completed warranty calls',60
FROM applicationMetaData
WHERE appname = 'servicecheckout'
  AND approle = 'manager';
  
INSERT INTO applicationMetaData  
SELECT DISTINCT appname, appcode, appseq, approle, 'completed warranty calls',
  'sublink_sco_writer_completed_calls_html', 'sco.writer.completedCalls',
  'leftnavbar','completed warranty calls',50
FROM applicationMetaData
WHERE appname = 'servicecheckout'
  AND approle = 'writer';  
  
-- access
-- everyone who currently has access to sco 

select * FROM employeeappauthorization
-- managers
INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,
  functionality)
SELECT *
FROM (
  SELECT distinct username
  FROM employeeappauthorization 
  WHERE appname = 'servicecheckout'
    AND approle = 'manager') a,
(
  SELECT appname, appseq, appcode, approle, functionality
  FROM applicationMetaData
  where appname = 'servicecheckout'
    AND functionality = 'completed warranty calls'
    AND approle = 'manager') b;
  
  
-- writers
INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,
  functionality)
SELECT *
FROM (
  SELECT distinct username
  FROM employeeappauthorization 
  WHERE appname = 'servicecheckout'
    AND approle = 'writer') a,
(
  SELECT appname, appseq, appcode, approle, functionality
  FROM applicationMetaData
  where appname = 'servicecheckout'
    AND functionality = 'completed warranty calls'
    AND approle = 'writer') b  