SELECT * FROM employeeAppAuthorization
WHERE username = 'tbursinger@rydellchev.com'

DELETE FROM employeeAppAuthorization
WHERE username = 'jandrews@cartiva.com'
  AND appcode = 'sco'
 

INSERT INTO employeeAppAuthorization (username, appname, appseq, appcode, approle,
  functionality)
select 'jandrews@cartiva.com', appname, appseq, appcode, approle, functionality
FROM applicationMetaData
WHERE appcode = 'sco'
  AND approle = 'manager'
  AND functionality IN ('dropdown', 'current ros','aged ros')
  
SELECT * FROM employeeAppAuthorization
WHERE username like 'kespe%'
  
-- 6/25
OPEN questions:
  IS it necessary to distinguish  BETWEEN current AND aged ros? for bdc
    view, are NOT ALL OPEN ros the same?
  market vs location - IS bdc handling service calls for honda?
    IS any one at honda updating notes IN current/aged ros?
  body shop vs main shop - OPEN ros are NOT currently being exposed for body
    shop, IS bdc handling any body shop calls?
  
need to ADD sco/current & aged ros to bdc folks
initially, only for ry1 writers
manager LIKE, ie, OPEN ros (whether current OR aged) for ALL  GM writers
thinking of adding appDeptKey to empAppAuth for sco/manager

SELECT appname, appcode, approle, functionality
FROM applicationMEtaData
GROUP BY appname, appcode, approle, functionality
HAVING COUNT(*) > 1

SELECT DISTINCT appname, appcode, approle, functionality, appDepartmentKey
FROM employeeAppAuthorization
WHERE appDepartmentKey IS NOT NULL 

employeeAppAuthorization.appDepartmentKey used IN
  sp.getApplicationNavigation

so, the immediate challenge IS to configure the system such that, IF i am a bdc
  employee, WHEN i log IN to bdc, i have writer stats -> current ros, aged ros  