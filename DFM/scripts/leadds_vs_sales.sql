-- ADD proc
/*
ALTER TABLE dfm_metrics
ADD COLUMN metric_type cichar(24);

UPDATE dfm_metrics
SET metric_type = 'funnel';

INSERT INTO dfm_metrics (metric, metric_type) values('eLeads', 'leads_vs_sales');
INSERT INTO dfm_metrics (metric, metric_type) values('eLead Sales', 'leads_vs_sales');
INSERT INTO dfm_metrics (metric, metric_type) values('Chat Leads', 'leads_vs_sales');
INSERT INTO dfm_metrics (metric, metric_type) values('Chat Sales', 'leads_vs_sales');
INSERT INTO dfm_metrics (metric, metric_type) values('Phone Leads', 'leads_vs_sales');
INSERT INTO dfm_metrics (metric, metric_type) values('Phone Sales', 'leads_vs_sales');
INSERT INTO dfm_metrics (metric, metric_type) values('Showroom Leads', 'leads_vs_sales');
INSERT INTO dfm_metrics (metric, metric_type) values('Showroom Sales', 'leads_vs_sales');
*/
EXECUTE PROCEDURE sp_RenameDDObject( 'dfm_update', 'zUnused_dfm_update', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'dfm_get_all_data', 'zUnused_dfm_get_all_data', 10, 0);

CREATE PROCEDURE dfm_get_ytd_leads_vs_sales_data (
  name cichar(60),
  year cichar(4),
  store CICHAR ( 60 ) OUTPUT,
  the_month CICHAR ( 3 ) OUTPUT,
  the_year CICHAR ( 4 ) OUTPUT,
  eleads DOUBLE ( 15 ) OUTPUT,
  elead_sales DOUBLE ( 15 ) OUTPUT,
  chat_leads DOUBLE ( 15 ) OUTPUT,
  chat_sales DOUBLE ( 15 ) OUTPUT,
  phone_leads DOUBLE ( 15 ) OUTPUT,
  phone_sales DOUBLE ( 15 ) OUTPUT,
  showroom_leads DOUBLE ( 15 ) OUTPUT,
  showroom_sales DOUBLE ( 15 ) OUTPUT)
BEGIN
/*
EXECUTE procedure dfm_get_ytd_leads_vs_sales_data('DeKalb-Sycamore Chevrolet GMC Cadillac', '2015')
*/ 
DECLARE @name string;
DECLARE @year string;
@name = (SELECT name FROM __input);
@year = (SELECT year FROM __input);
INSERT INTO __output
SELECT b.name, 'YTD', b.the_year, c.percentage, d.percentage,
  e.percentage,f.percentage,g.percentage,h.percentage,i.percentage,j.percentage
FROM (
  SELECT a.name, a.the_year, a.yearmonth
  FROM dfm_data a
  INNER JOIN dfm_metrics b on a.metric = b.metric
    AND b.metric_type = 'leads_vs_sales'
  WHERE name = @name
    AND the_year = @year
  GROUP BY a.name, a.the_year, a.yearmonth) b
LEFT JOIN (
  SELECT SUM(percentage) AS percentage
  FROM dfm_data 
  WHERE name = @name
    AND the_year = @year
   AND metric = 'eLeads') c on 1 = 1
LEFT JOIN (
  SELECT SUM(percentage) AS percentage
  FROM dfm_data 
  WHERE name = @name
    AND the_year = @year
   AND metric = 'eLead Sales') d on 1 = 1   
LEFT JOIN (
  SELECT SUM(percentage) AS percentage
  FROM dfm_data 
  WHERE name = @name
    AND the_year = @year
   AND metric = 'Chat Leads') e on 1 = 1  
LEFT JOIN (
  SELECT SUM(percentage) AS percentage
  FROM dfm_data 
  WHERE name = @name
    AND the_year = @year
   AND metric = 'Chat Sales') f on 1 = 1
LEFT JOIN (
  SELECT SUM(percentage) AS percentage
  FROM dfm_data 
  WHERE name = @name
    AND the_year = @year
   AND metric = 'Phone Leads') g on 1 = 1
LEFT JOIN (
  SELECT SUM(percentage) AS percentage
  FROM dfm_data 
  WHERE name = @name
    AND the_year = @year
   AND metric = 'Phone Sales') h on 1 = 1   
LEFT JOIN (
  SELECT SUM(percentage) AS percentage
  FROM dfm_data 
  WHERE name = @name
    AND the_year = @year
   AND metric = 'Showroom Leads') i on 1 = 1  
LEFT JOIN (
  SELECT SUM(percentage) AS percentage
  FROM dfm_data 
  WHERE name = @name
    AND the_year = @year
   AND metric = 'Showroom Sales') j on 1 = 1;

END;     
  
  
CREATE PROCEDURE dfm_update_funnel_metrics
   ( 
      name CICHAR ( 60 ),
      yearmonth Integer,
      eLead_Contact_perc DOUBLE ( 15 ),
      Appointments_to_Leads_Phone DOUBLE ( 15 ),
      Appointments_to_Leads_Internet DOUBLE ( 15 ),
      Appointment_Show_Rate_All DOUBLE ( 15 ),
      closing_perc_on_shown_appt_phone DOUBLE ( 15 ),
      closing_perc_on_shown_appt_Internet DOUBLE ( 15 ),
      Closing_Ratio_on_Phone_Leads DOUBLE ( 15 ),
      Closing_Ratio_on_Internet_Leads DOUBLE ( 15 ),
      pass_fail LOGICAL OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE dfm_update_funnel_metrics('Alexandria Motors',201511,.1,.2,.3,.4,.6,.6,.7,.8);
*/
DECLARE @name string;
DECLARE @yearmonth integer;
DECLARE @eLead_Contact_perc DOUBLE;
DECLARE @Appointments_to_Leads_Phone DOUBLE;
DECLARE @Appointments_to_Leads_Internet DOUBLE;
DECLARE @Appointment_Show_Rate_All DOUBLE ( 15 );
DECLARE @closing_perc_on_shown_appt_phone DOUBLE ( 15 );
DECLARE @closing_perc_on_shown_appt_Internet DOUBLE ( 15 );
DECLARE @Closing_Ratio_on_Phone_Leads DOUBLE ( 15 );
DECLARE @Closing_Ratio_on_Internet_Leads DOUBLE ( 15); 
DECLARE @month cichar(3);
DECLARE @year cichar(4);
@name = (SELECT name FROM __input);
@yearmonth = (SELECT yearmonth FROM __input);
@eLead_Contact_perc  = (SELECT eLead_Contact_perc FROM __input);
@Appointments_to_Leads_Phone  = (SELECT Appointments_to_Leads_Phone FROM __input);
@Appointments_to_Leads_Internet  = (SELECT Appointments_to_Leads_Internet FROM __input);
@Appointment_Show_Rate_All  = (SELECT Appointment_Show_Rate_All FROM __input);
@closing_perc_on_shown_appt_phone  = (SELECT closing_perc_on_shown_appt_phone FROM __input);
@closing_perc_on_shown_appt_Internet  = (SELECT closing_perc_on_shown_appt_Internet FROM __input);
@Closing_Ratio_on_Phone_Leads  = (SELECT Closing_Ratio_on_Phone_Leads FROM __input);
@Closing_Ratio_on_Internet_Leads  = (SELECT Closing_Ratio_on_Internet_Leads FROM __input);
@month = (
  SELECT distinct LEFT(monthname, 3) 
  FROM dds.day 
  WHERE yearmonth = @yearmonth);
@year = (
  SELECT DISTINCT CAST(theyear AS sql_char)
  FROM dds.day
  WHERE yearmonth = @yearmonth);  
BEGIN TRANSACTION;
TRY 
IF EXISTS (
  SELECT 1
  FROM dfm_data a
  INNER JOIN dfm_metrics b on a.metric = b.metric
  WHERE name = @name
    AND yearmonth = @yearmonth
    AND metric_type = 'funnel') 
THEN
  UPDATE dfm_data
  SET percentage = @eLead_Contact_perc
  WHERE name = @name
    AND metric = 'eLead Contact %'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @Appointments_to_Leads_Phone
  WHERE name = @name
    AND metric = 'Appointments to Leads (Phone)'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @Appointments_to_Leads_Internet
  WHERE name = @name
    AND metric = 'Appointments to Leads (Internet)'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @Appointment_Show_Rate_All
  WHERE name = @name
    AND metric = 'Appointment Show Rate (all)'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @closing_perc_on_shown_appt_phone
  WHERE name = @name
    AND metric = 'closing % on shown appointments (phone)'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @closing_perc_on_shown_appt_Internet
  WHERE name = @name
    AND metric = 'closing % on shown appointments (Internet)'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @Closing_Ratio_on_Phone_Leads
  WHERE name = @name
    AND metric = 'Closing Ratio on Phone Leads'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @Closing_Ratio_on_Internet_Leads
  WHERE name = @name
    AND metric = 'Closing Ratio on Internet Leads'   
    AND yearmonth = @yearmonth;                      
ELSE 
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'eLead Contact %',@month,@year,@eLead_Contact_perc,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'Appointments to Leads (Phone)',@month,@year,@Appointments_to_Leads_Phone,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'Appointments to Leads (Internet)',@month,@year,@Appointments_to_Leads_Internet,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'Appointment Show Rate (all)',@month,@year,@Appointment_Show_Rate_All,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'closing % on shown appointments (phone)',@month,@year,@closing_perc_on_shown_appt_phone,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'closing % on shown appointments (Internet)',@month,@year,@closing_perc_on_shown_appt_Internet,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'Closing Ratio on Phone Leads',@month,@year,@Closing_Ratio_on_Phone_Leads,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'Closing Ratio on Internet Leads' ,@month,@year,@Closing_Ratio_on_Internet_Leads,@yearmonth);            
END IF; 
COMMIT WORK;
INSERT INTO __output values(true);
CATCH ALL
  ROLLBACK WORK;
  INSERT INTO __output values(fase);
END TRY;
END;



CREATE PROCEDURE dfm_update_sales_metrics
   ( 
      name CICHAR ( 60 ),
      yearmonth Integer,
      eleads DOUBLE ( 15 ),
      elead_sales DOUBLE ( 15 ),
      chat_leads DOUBLE ( 15 ),
      chat_sales DOUBLE ( 15 ),
      phone_leads DOUBLE ( 15 ),
      phone_sales DOUBLE ( 15 ),
      showroom_leads DOUBLE ( 15 ),
      showroom_sales DOUBLE ( 15 ),
      pass_fail LOGICAL OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE dfm_update_funnel_metrics('DeKalb-Sycamore Chevrolet GMC Cadillac',201507,.1,.2,.3,.4,.6,.6,.7,.8);
*/
DECLARE @name string;
DECLARE @yearmonth integer;
DECLARE @eleads DOUBLE;
DECLARE @elead_sales DOUBLE;
DECLARE @chat_leads DOUBLE;
DECLARE @chat_sales DOUBLE ( 15 );
DECLARE @phone_leads DOUBLE ( 15 );
DECLARE @phone_sales DOUBLE ( 15 );
DECLARE @showroom_leads DOUBLE ( 15 );
DECLARE @showroom_sales DOUBLE ( 15); 
DECLARE @month cichar(3);
DECLARE @year cichar(4);
@name = (SELECT name FROM __input);
@yearmonth = (SELECT yearmonth FROM __input);
@eleads  = (SELECT eleads FROM __input);
@elead_sales  = (SELECT elead_sales FROM __input);
@chat_leads  = (SELECT chat_leads FROM __input);
@chat_sales  = (SELECT chat_sales FROM __input);
@phone_leads  = (SELECT phone_leads FROM __input);
@phone_sales  = (SELECT phone_sales FROM __input);
@showroom_leads  = (SELECT showroom_leads FROM __input);
@showroom_sales  = (SELECT showroom_sales FROM __input);
@month = (
  SELECT distinct LEFT(monthname, 3) 
  FROM dds.day 
  WHERE yearmonth = @yearmonth);
@year = (
  SELECT DISTINCT CAST(theyear AS sql_char)
  FROM dds.day
  WHERE yearmonth = @yearmonth);  
BEGIN TRANSACTION;
TRY 
IF EXISTS (
  SELECT 1
  FROM dfm_data a
  INNER JOIN dfm_metrics b on a.metric = b.metric
  WHERE name = @name
    AND yearmonth = @yearmonth
    AND metric = 'leads_vs_sales') 
THEN
  UPDATE dfm_data
  SET percentage = @eleads
  WHERE name = @name
    AND metric = 'eleads'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @elead_sales
  WHERE name = @name
    AND metric = 'elead sales'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @chat_leads
  WHERE name = @name
    AND metric = 'chat leads'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @chat_sales
  WHERE name = @name
    AND metric = 'chat sales'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @phone_leads
  WHERE name = @name
    AND metric = 'phone leads'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @phone_sales
  WHERE name = @name
    AND metric = 'phone sales'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @showroom_leads
  WHERE name = @name
    AND metric = 'showroom leads'
    AND yearmonth = @yearmonth;
  UPDATE dfm_data
  SET percentage = @showroom_sales
  WHERE name = @name
    AND metric = 'showroom sales'   
    AND yearmonth = @yearmonth;                      
ELSE 
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'eleads',@month,@year,@eleads,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'elead sales',@month,@year,@elead_sales,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'chat leads',@month,@year,@chat_leads,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'chat sales',@month,@year,@chat_sales,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'phone leads',@month,@year,@phone_leads,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'phone sales',@month,@year,@phone_sales,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'showroom leads',@month,@year,@showroom_leads,@yearmonth);
  INSERT INTO dfm_data (name,metric,the_month,the_year,percentage,yearmonth)
  values(@name,'showroom sales' ,@month,@year,@showroom_sales,@yearmonth);            
END IF; 
COMMIT WORK;
INSERT INTO __output values(true);
CATCH ALL
  ROLLBACK WORK;
  INSERT INTO __output values(false);
END TRY;

END;


CREATE PROCEDURE dfm_get_leads_vs_sales_data
   ( 
      store CICHAR ( 60 ) OUTPUT,
      the_month CICHAR ( 3 ) OUTPUT,
      the_year CICHAR ( 4 ) OUTPUT,
      eleads DOUBLE ( 15 ) OUTPUT,
      elead_sales DOUBLE ( 15 ) OUTPUT,
      chat_leads DOUBLE ( 15 ) OUTPUT,
      chat_sales DOUBLE ( 15 ) OUTPUT,
      phone_leads DOUBLE ( 15 ) OUTPUT,
      phone_sales DOUBLE ( 15 ) OUTPUT,
      showroom_leads DOUBLE ( 15 ) OUTPUT,
      showroom_sales DOUBLE ( 15 ) OUTPUT,
      yearmonth Integer OUTPUT,
      month_and_year CICHAR ( 8 ) OUTPUT
   ) 
BEGIN 
/*

2/14/15 v2 WAAAY Faster        
EXECUTE PROCEDURE dfm_get_leads_vs_sales_data();
*/
INSERT INTO __output
SELECT top 1000 b.name,b.the_month,b.the_year,c.percentage,d.percentage,
  e.percentage,f.percentage,g.percentage,h.percentage,i.percentage,j.percentage,
  b.yearmonth, b.the_month + ' ' + b.the_year
FROM (
  SELECT a.name, a.the_month, a.the_year, a.yearmonth
  FROM dfm_data a
  INNER JOIN dfm_metrics b on a.metric = b.metric
    AND b.metric_type = 'leads_vs_sales'
  group by a.name, a.the_month, a.the_year, a.yearmonth) b
LEFT JOIN dfm_data c on b.name = c.name
  AND b.yearmonth = c.yearmonth
  AND c.metric = 'eLeads'
LEFT JOIN dfm_data d on b.name = d.name
  AND b.yearmonth = d.yearmonth
  AND d.metric = 'eLead Sales'
LEFT JOIN dfm_data e on b.name = e.name
  AND b.yearmonth = e.yearmonth
  AND e.metric = 'Chat Leads)'
LEFT JOIN dfm_data f on b.name = f.name
  AND b.yearmonth = f.yearmonth
  AND f.metric = 'Chat Sales'
LEFT JOIN dfm_data g on b.name = g.name
  AND b.yearmonth = g.yearmonth
  AND g.metric = 'Phone Leads)'
LEFT JOIN dfm_data h on b.name = h.name
  AND b.yearmonth = h.yearmonth
  AND h.metric = 'Phone Sales'
LEFT JOIN dfm_data i on b.name = i.name
  AND b.yearmonth = i.yearmonth
  AND i.metric = 'Showroom Leads'
LEFT JOIN dfm_data j on b.name = j.name
  AND b.yearmonth = j.yearmonth
  AND j.metric = 'Showroom Sales'   
ORDER BY b.yearmonth DESC, b.name;            




END;



CREATE PROCEDURE dfm_get_funnel_data
   ( 
      store CICHAR ( 60 ) OUTPUT,
      the_month CICHAR ( 3 ) OUTPUT,
      the_year CICHAR ( 4 ) OUTPUT,
      eLead_Contact_perc DOUBLE ( 15 ) OUTPUT,
      Appointments_to_Leads_Phone DOUBLE ( 15 ) OUTPUT,
      Appointments_to_Leads_Internet DOUBLE ( 15 ) OUTPUT,
      Appointment_Show_Rate_All DOUBLE ( 15 ) OUTPUT,
      closing_perc_on_shown_appt_phone DOUBLE ( 15 ) OUTPUT,
      closing_perc_on_shown_appt_Internet DOUBLE ( 15 ) OUTPUT,
      Closing_Ratio_on_Phone_Leads DOUBLE ( 15 ) OUTPUT,
      Closing_Ratio_on_Internet_Leads DOUBLE ( 15 ) OUTPUT,
      yearmonth Integer OUTPUT,
      month_and_year CICHAR ( 8 ) OUTPUT
   ) 
BEGIN 
/*
2/9/15: what greg wants IS pivoted data
        1 row per store/date, metrics AS columns
        
2/14/15 v2 WAAAY Faster        
EXECUTE PROCEDURE dfm_get_funnel_data();
*/
INSERT INTO __output
-- v2
SELECT top 1000 b.name,b.the_month,b.the_year,c.percentage,d.percentage,
  e.percentage,f.percentage,g.percentage,h.percentage,i.percentage,j.percentage,
  b.yearmonth, b.the_month + ' ' + b.the_year
FROM (
  SELECT a.name, a.the_month, a.the_year, a.yearmonth
  FROM dfm_data a
  INNER JOIN dfm_metrics b on a.metric = b.metric
    AND b.metric_type = 'funnel'
  group by a.name, a.the_month, a.the_year, a.yearmonth) b
LEFT JOIN dfm_data c on b.name = c.name
  AND b.yearmonth = c.yearmonth
  AND c.metric = 'eLead Contact %'
LEFT JOIN dfm_data d on b.name = d.name
  AND b.yearmonth = d.yearmonth
  AND d.metric = 'Appointments to Leads (Phone)'
LEFT JOIN dfm_data e on b.name = e.name
  AND b.yearmonth = e.yearmonth
  AND e.metric = 'Appointments to Leads (Internet)'
LEFT JOIN dfm_data f on b.name = f.name
  AND b.yearmonth = f.yearmonth
  AND f.metric = 'Appointment Show Rate (all)'
LEFT JOIN dfm_data g on b.name = g.name
  AND b.yearmonth = g.yearmonth
  AND g.metric = 'closing % on shown appointments (phone)'
LEFT JOIN dfm_data h on b.name = h.name
  AND b.yearmonth = h.yearmonth
  AND h.metric = 'closing % on shown appointments (Internet)'
LEFT JOIN dfm_data i on b.name = i.name
  AND b.yearmonth = i.yearmonth
  AND i.metric = 'Closing Ratio on Phone Leads'
LEFT JOIN dfm_data j on b.name = j.name
  AND b.yearmonth = j.yearmonth
  AND j.metric = 'Closing Ratio on Internet Leads'   
ORDER BY b.yearmonth DESC, b.name;            




END;


--DROP TABLE dfm_contacts;
CREATE TABLE dfm_contacts (
  dealership cichar(60) constraint NOT NULL,
  contact cichar(60) constraint NOT NULL,
  email cichar(100),
  office_phone cichar(16),
  cell_phone cichar(16),
  crm cichar(60)) IN database;

EXECUTE PROCEDURE sp_CreateIndex90( 
   'dfm_contacts','dfm_contacts.adi','FK1','dealership','',2,512,'');  
   
   
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'dfm_store_contacts',
     'dfm_stores', 
     'dfm_contacts', 
     'FK1', 
     1, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 