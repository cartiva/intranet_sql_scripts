-- DROP PROCEDURE dfm_get_stores;
CREATE PROCEDURE dfm_get_stores (
  name cichar(60) output)
BEGIN
/*
EXECUTE PROCEDURE dfm_get_stores();
*/
INSERT INTO __output
SELECT top 100 name
FROM dfm_stores
ORDER BY name;
END;  

DROP PROCEDURE dfm_get_all_data;
ALTER PROCEDURE dfm_get_all_data(
  store cichar(60) output,
  the_month cichar(3) output,
  the_year cichar(4) output,
  eLead_Contact double output,
  Internet_Appointments_to_Leads double output,
  Phone_Appointments_to_Leads double output
  )
BEGIN
/*
2/9/15: what greg wants IS pivoted data
        1 row per store/date, metrics AS columns
EXECUTE PROCEDURE dfm_get_all_data();
*/
INSERT INTO __output
SELECT a.name, a.the_month, a.the_year,
  MAX(
    CASE WHEN metric = 'eLead Contact' THEN (
      SELECT percentage
      FROM dfm_data
      WHERE name = a.name
        AND the_month = a.the_month
        AND the_year = a.the_year
        AND metric = 'eLead Contact') 
    END) AS [eLead Contact],
  MAX(
    CASE WHEN metric = 'Internet Appointments to Leads' THEN (
      SELECT percentage
      FROM dfm_data
      WHERE name = a.name
        AND the_month = a.the_month
        AND the_year = a.the_year
        AND metric = 'Internet Appointments to Leads') 
    END) AS [Internet Appointments to Leads],
  MAX(
    CASE WHEN metric = 'Phone Appointments to Leads' THEN (
      SELECT percentage
      FROM dfm_data
      WHERE name = a.name
        AND the_month = a.the_month
        AND the_year = a.the_year
        AND metric = 'Phone Appointments to Leads') 
    END) AS [Phone Appointments to Leads]            
FROM dfm_data a
group by a.name, a.the_month, a.the_year;
END;

