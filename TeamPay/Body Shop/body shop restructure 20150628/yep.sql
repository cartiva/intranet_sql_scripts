after being off team pay for a month, now they want to go back
changes: 
  1 team, 18 techs, includes brian peterson AND arnie iverson
  closed hours NOT flag hours

  -- these are the techs
DROP TABLE #techs;  
SELECT c.lastname, c.firstname, c.employeenumber, cast(0.0000 AS sql_numeric (8,4)) AS perc,
  
-- SELECT COUNT(*)
INTO #techs
FROM tptechs a
INNER JOIN tpteamtechs b on a.techkey = b.techkey
  AND b.thrudate > curdate()
LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true  
WHERE departmentkey = 13
UNION 
SELECT b.lastname, b.firstname, b.employeenumber, cast(0.0000 AS sql_numeric (8,4))
FROM dds.dimtech a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE flagdeptcode = 'bs'
AND a.active = true
AND a.currentrow = true
AND a.technumber IN ('213','209')

-- ben needs to see avg paycheck for nov 2014 - jan 2015
select b.lastname, b.firstname, b.employeenumber, sum(yhdtgp)/COUNT(*) AS "avg gross", COUNT(*)
-- select b.lastname, b.firstname, b.employeenumber, a.*
FROM dds.stgarkonapyhshdta a
INNER JOIN (
    SELECT c.lastname, c.firstname, c.employeenumber
    -- SELECT COUNT(*)
    FROM tptechs a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND b.thrudate > curdate()
    LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
      AND c.currentrow = true  
    WHERE departmentkey = 13
    UNION 
    SELECT b.lastname, b.firstname, b.employeenumber
    FROM dds.dimtech a
    LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
      AND b.currentrow = true
    WHERE flagdeptcode = 'bs'
    AND a.active = true
    AND a.currentrow = true
    AND a.technumber IN ('213','209')) b on a.yhdemp = b.employeenumber
WHERE a.yhdco# = 'ry1'
  AND ((yhdeyy = 14 AND yhdemm IN (11,12)) OR (yhdeyy = 15 AND yhdemm = 1))
GROUP BY b.lastname, b.firstname, b.employeenumber
ORDER BY lastname, firstname


-- figure "big team" productivity for ben to see
-- 11/6/14 -> 6/13/15
-- flag hours
-- DROP TABLE #flag;
SELECT a.lastname, a.firstname, a.employeenumber, b.technumber, 
  d.biweeklypayperiodstartdate, d.biweeklypayperiodenddate,
  round(sum(c.flaghours), 2) AS flaghours
INTO #flag  
FROM (    
  SELECT c.lastname, c.firstname, c.employeenumber
  -- SELECT COUNT(*)
  FROM tptechs a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND b.thrudate > curdate()
  LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
    AND c.currentrow = true  
  WHERE departmentkey = 13
  UNION 
  SELECT b.lastname, b.firstname, b.employeenumber
  FROM dds.dimtech a
  LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE flagdeptcode = 'bs'
  AND a.active = true
  AND a.currentrow = true
  AND a.technumber IN ('213','209')) a    
INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
  AND b.active = true  
INNER JOIN dds.factRepairOrder c on b.techkey = c.techkey  
INNER JOIN dds.day d on c.closedatekey = datekey
  AND d.thedate BETWEEN '11/06/2014' AND '06/13/2015'
GROUP BY a.lastname, a.firstname, a.employeenumber, b.technumber, 
  d.biweeklypayperiodstartdate, d.biweeklypayperiodenddate  
  
SELECT *
INTO #clock
FROM (    
  SELECT c.lastname, c.firstname, c.employeenumber
  -- SELECT COUNT(*)
  FROM tptechs a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND b.thrudate > curdate()
  LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
    AND c.currentrow = true  
  WHERE departmentkey = 13
  UNION 
  SELECT b.lastname, b.firstname, b.employeenumber
  FROM dds.dimtech a
  LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE flagdeptcode = 'bs'
  AND a.active = true
  AND a.currentrow = true
  AND a.technumber IN ('213','209')) a 
INNER JOIN (       
  SELECT b.biweeklypayperiodstartdate, b.biweeklypayperiodenddate, 
    c.employeenumber, SUM(clockhours) AS clockhours
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.day b on a.datekey = b.datekey
    AND b.thedate BETWEEN '11/06/2014' AND '06/13/2015'
  LEFT JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
  GROUP BY b.biweeklypayperiodstartdate, b.biweeklypayperiodenddate, c.employeenumber) b on a.employeenumber = b.employeenumber     
  
SELECT a.lastname, a.firstname, a.employeenumber, 
  a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
  sum(a.flaghours) AS flaghours, sum(b.clockhours) AS clockhours,
  case when sum(b.clockhours) = 0 then 0 else round(sum(a.flaghours)/sum(b.clockhours), 2) END AS prof
FROM #flag a
LEFT JOIN #clock b on a.employeenumber = b.employeenumber AND a.biweeklypayperiodstartdate = b.biweeklypayperiodstartdate
GROUP BY a.lastname, a.firstname, a.employeenumber, 
  a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate
union
SELECT 'z', 'shop', 'na', 
  a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
  sum(a.flaghours) AS flag, sum(b.clockhours) AS clock,
  round(sum(a.flaghours)/sum(b.clockhours), 2)
FROM #flag a
LEFT JOIN #clock b on a.employeenumber = b.employeenumber AND a.biweeklypayperiodstartdate = b.biweeklypayperiodstartdate
GROUP BY a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate
ORDER BY a.biweeklypayperiodstartdate, a.lastname

select * from #techs

SELECT a.lastname, a.firstname, a.employeenumber, c.expr*100 AS tech_perc, 
  round(c.expr*378, 2) AS tech_rate, 
  a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
  b.shop_flag, b.shop_clock, b.shop_prof, 
  round(c.expr*378*shop_prof*clockhours, 2) AS comm_pay, d.avg_pay_chk
FROM (  
  SELECT a.lastname, a.firstname, a.employeenumber, 
    a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
    sum(a.flaghours) AS flaghours, sum(b.clockhours) AS clockhours,
    case when sum(b.clockhours) = 0 then 0 else round(sum(a.flaghours)/sum(b.clockhours), 2) END AS prof
  FROM #flag a
  LEFT JOIN #clock b on a.employeenumber = b.employeenumber AND a.biweeklypayperiodstartdate = b.biweeklypayperiodstartdate
  GROUP BY a.lastname, a.firstname, a.employeenumber, 
    a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate) a
LEFT JOIN (
  SELECT a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
    sum(a.flaghours) AS shop_flag, sum(b.clockhours) AS shop_clock,
    round(sum(a.flaghours)/sum(b.clockhours), 2) AS shop_prof
  FROM #flag a
  LEFT JOIN #clock b on a.employeenumber = b.employeenumber AND a.biweeklypayperiodstartdate = b.biweeklypayperiodstartdate
  GROUP BY a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate) b on a.biweeklypayperiodstartdate = b.biweeklypayperiodstartdate
LEFT JOIN #techs c on a.employeenumber = c.employeenumber  
LEFT JOIN (
  SELECT employeenumber, round(SUM(comm_pay)/COUNT(*), 2) AS avg_pay_chk
  FROM (
  SELECT a.*, b.*, round(c.expr*378*shop_prof*clockhours, 2) AS comm_pay
    FROM (  
    SELECT a.lastname, a.firstname, a.employeenumber, 
      a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
      sum(a.flaghours) AS flaghours, sum(b.clockhours) AS clockhours,
      case when sum(b.clockhours) = 0 then 0 else round(sum(a.flaghours)/sum(b.clockhours), 2) END AS prof
    FROM #flag a
    LEFT JOIN #clock b on a.employeenumber = b.employeenumber AND a.biweeklypayperiodstartdate = b.biweeklypayperiodstartdate
    GROUP BY a.lastname, a.firstname, a.employeenumber, 
      a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate) a
  LEFT JOIN (
    SELECT a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
      sum(a.flaghours) AS shop_flag, sum(b.clockhours) AS shop_clock,
      round(sum(a.flaghours)/sum(b.clockhours), 2) AS shop_prof
    FROM #flag a
    LEFT JOIN #clock b on a.employeenumber = b.employeenumber AND a.biweeklypayperiodstartdate = b.biweeklypayperiodstartdate
    GROUP BY a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate) b on a.biweeklypayperiodstartdate = b.biweeklypayperiodstartdate
  LEFT JOIN #techs c on a.employeenumber = c.employeenumber  ) x
  GROUP BY employeenumber) d on a.employeenumber = d.employeenumber
ORDER BY a.biweeklypayperiodstartdate, a.lastname  