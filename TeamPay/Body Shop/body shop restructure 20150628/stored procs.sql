alter PROCEDURE GetTpPayPeriods
   ( 
      department CICHAR ( 12 ),
      payPeriodDisplay CICHAR ( 100 ) OUTPUT,
      payPeriodIndicator Integer OUTPUT
   ) 
BEGIN 
/*
2-8-14: return 8 payperiods
2/22/14: change width of payPeriodDisplay
3/21/14: ADD pdq

6/2/14
  increase body shop to 10 periods (just LIKE main shop)
  
6/30/15
-- *b*
  body shop back on team pay, ALL techs on one team
  effective 6/28/15, ben wants to expose no history  
  
EXECUTE PROCEDURE GetTpPayPeriods('bodyshop');
*/
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @fromPayPeriodSeq integer;
DECLARE @thruPayPeriodSeq integer;
DECLARE @department string;
@department = (SELECT department FROM __input);
@thrupayperiodseq = (
  SELECT distinct payperiodseq FROM tpData WHERE thedate = curdate());
@fromPayPeriodSeq = @thrupayperiodseq - 7;
IF @department = 'mainshop' THEN 
  INSERT INTO __output
  SELECT top 10 payperiodselectformat, payperiodseq - @thruPayPeriodSeq
  FROM (
    SELECT DISTINCT payperiodselectformat, payperiodseq
    FROM tpdata 
    WHERE payperiodseq BETWEEN  @frompayperiodseq AND @thrupayperiodseq) x
  ORDER BY payperiodseq - @thruPayPeriodSeq DESC ;

ELSE IF @department = 'bodyshop' THEN 
  INSERT INTO __output
  -- *a*
  SELECT top 10 payperiodselectformat, payperiodseq - @thruPayPeriodSeq
  FROM (
    SELECT DISTINCT payperiodselectformat, payperiodseq
    FROM tpdata 
    WHERE payperiodseq BETWEEN  @frompayperiodseq AND @thrupayperiodseq
  -- *c*
      AND payperiodseq >= (
        SELECT distinct payperiodseq
        FROM tpdata
        WHERE thedate = '06/28/2015')) x
  ORDER BY payperiodseq - @thruPayPeriodSeq DESC;
/*
ELSE IF @department = 'pdq' THEN
INSERT INTO __output
SELECT top 4 payperiodselectformat, payperiodseq - @thruPayPeriodSeq
FROM (
  SELECT DISTINCT payperiodselectformat, payperiodseq
  FROM pdqWriterPayrollData 
  WHERE payperiodseq BETWEEN  @frompayperiodseq AND @thrupayperiodseq) x
ORDER BY payperiodseq - @thruPayPeriodSeq DESC ;
END;
*/
END;
END;

END;

alter PROCEDURE xfmTpData
   ( 
   ) 
BEGIN 
/*
2/27/14
  v.2.2.0
    new row
      ADD bodyshop to base values for new row
	  change daterange to 01/12/2014 - curdate()
	  team census/budget: bs mod
3/11/14	  
  added UPDATE date to teamProf to combine body shop paint teams (cody - 18, pete - 19)
  INTO a single proficiency number
  
7/24/14
*a* base new row, SELECT the current techHourlyRate  
*b*: total hack to accomodate cody johnson being on a team 12/28 -> 1/10 but
       his hours (clock & flag) should NOT be part of the team calculations
       had to DO it here because clock AND flaghours overwrite existing past
       data IN tpData
6/30/15
  body shop coming back on to team pay, ALL techs on one team
  flag hours based on CLOSE date NOT flag date
  also took the opportunity to reduce the scrape periods
  	  
EXECUTE PROCEDURE xfmTpData();
*/
BEGIN TRANSACTION; -- new row   
TRY
  TRY 
    -- base values for a new row
-- main Shop	
    INSERT INTO tpData(DateKey, TheDate, PayPeriodStart,PayPeriodEnd,PayPeriodSeq,
      DayOfPayPeriod, DayName, 
      DepartmentKey, TechKey, TechNumber, EmployeeNumber, FirstName, LastName)  
    SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
      a.biweeklypayperiodsequence, dayinbiweeklypayperiod, a.DayName,
      (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'mainshop' AND dl5 = 'Shop'),
      b.TechKey, b.technumber, b.employeenumber, b.firstname, b.lastname
    FROM dds.day a, tpTechs b 
    WHERE a.thedate BETWEEN '01/11/2015' AND curdate()
	    AND b.departmentKey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'mainshop' AND dl5 = 'Shop')
      AND NOT EXISTS (
        SELECT 1
        FROM tpdata
        WHERE thedate = a.thedate
          AND techkey = b.techkey)
      AND EXISTS (-- 12/5, techs gone, but still exist IN tpTechs, don't want them IN tpData
        SELECT 1
        FROM tpTeamTechs
        WHERE techkey = b.techkey
          AND a.thedate BETWEEN fromdate AND thrudate); 
-- bs	
    INSERT INTO tpData(DateKey, TheDate, PayPeriodStart,PayPeriodEnd,PayPeriodSeq,
      DayOfPayPeriod, DayName, 
      DepartmentKey, TechKey, TechNumber, EmployeeNumber, FirstName, LastName)  
    SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
      a.biweeklypayperiodsequence, dayinbiweeklypayperiod, a.DayName,
      (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop'),
      b.TechKey, b.technumber, b.employeenumber, b.firstname, b.lastname
    FROM dds.day a, tpTechs b 
    WHERE a.thedate BETWEEN '01/11/2015' AND curdate()
	  AND b.departmentKey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop')
      AND NOT EXISTS (
        SELECT 1
        FROM tpdata
        WHERE thedate = a.thedate
          AND techkey = b.techkey)
      AND EXISTS (-- 12/5, techs gone, but still exist IN tpTechs, don't want them IN tpData
        SELECT 1
        FROM tpTeamTechs
        WHERE techkey = b.techkey
          AND a.thedate BETWEEN fromdate AND thrudate); 	  
    -- payPeriodSelectFormat for new row
    UPDATE tpdata
    SET payPeriodSelectFormat = z.ppsel
    FROM (
      SELECT payperiodseq, TRIM(pt1) + ' ' + TRIM(pt2) AS ppsel
        FROM (  
        SELECT distinct payperiodstart, payperiodend, payperiodseq, 
          (SELECT trim(monthname) + ' '
            + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
            FROM dds.day
            WHERE thedate = a.payperiodstart) AS pt1,
          (SELECT trim(monthname) + ' ' 
            + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
            + TRIM(CAST(theyear AS sql_char))
            FROM dds.day
            WHERE thedate = a.payperiodend) AS pt2
        FROM tpdata a) y) z
    WHERE tpdata.payperiodseq = z.payperiodseq;  
    -- teamname/key for new row
    UPDATE tpData
    SET TeamKey = x.teamkey,
        teamname = x.teamname
    FROM (    
      SELECT *
      FROM ( 
        SELECT thedate
        FROM tpData
        GROUP BY thedate) a
      INNER JOIN (  
        SELECT a.techkey, c.teamkey, c.teamname, b.fromdate, b.thrudate
        FROM tptechs a
        INNER JOIN tpteamtechs b on a.techkey = b.techkey
        INNER JOIN tpteams c on b.teamkey = c.teamkey) b on a.thedate BETWEEN b.fromdate AND b.thrudate) x
    WHERE tpData.TechKey = x.techkey
      AND tpData.theDate = x.theDate; 
	         
    -- team census, budget for new row
    /* bs mod	
        UPDATE tpData 
        SET TeamCensus = x.census,
            teamBudget = x.Budget
        FROM ( 
          SELECT *
          FROM tpTeamValues) x
        WHERE tpData.payPeriodStart = x.fromDate
          AND tpData.teamkey = x.teamkey;  
    */	             
    UPDATE tpData
      SET teamCensus = x.census,
          teamBudget = x.budget
      FROM tpTeamValues x
      WHERE (tpData.teamCensus IS NULL OR tpData.teamBudget IS NULL) 
        AND tpData.teamKey = x.teamKey
        AND tpData.theDate BETWEEN x.fromDate AND x.thruDate; 
		  
-- *a*			  
    -- techteamPercentage: FROM tpTechValues for new row
	-- AND techHourlyRate
    UPDATE tpData
    SET TechTeamPercentage = x.TechTeamPercentage,
	    techHourlyRate = x.previousHourlyRate
    FROM (
      SELECT a.thedate, b.techkey, b.techTeamPercentage, b.previousHourlyRate
      FROM tpData a
      INNER JOIN tpTechValues b on a.techKey = b.techKey
        AND a.thedate BETWEEN b.fromdate AND b.thrudate) x
    WHERE tpData.TechKey = x.TechKey
      AND tpData.thedate = x.theDate;    
    -- TechTFRRate for new row 
    UPDATE tpdata
    SET TechTFRRate = techteampercentage * teambudget;    
    -- TechHourlyRate for new row
--    UPDATE tpdata
--    SET techhourlyrate = (
--      SELECT distinct previoushourlyrate
--      FROM tptechvalues
--      WHERE techkey = tpdata.techkey);  
            
  CATCH ALL 
    RAISE;-- tpdata(1, 'New Row ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- techs
TRY
  TRY   
    -- ClockHoursDay    
    UPDATE tpdata
    SET TechClockhoursDay = x.clockhours,
        TechVacationHoursDay = x.Vacation,
        TechPTOHoursDay = x.PTO,
        TechHolidayHoursDay = x.Holiday
    FROM (    
      SELECT b.thedate, c.employeenumber, SUM(clockhours) AS clockhours,
        SUM(coalesce(VacationHours,0)) AS vacation,
        SUM(coalesce(PTOHours,0)) AS pto,
        SUM(coalesce(HolidayHours,0)) AS holiday
      FROM dds.edwClockHoursFact a
      INNER JOIN dds.day b on a.datekey = b.datekey
        AND b.thedate BETWEEN '01/11/2015' AND curdate()
      LEFT JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
      GROUP BY b.thedate, c.employeenumber) x 
    WHERE tpdata.thedate = x.thedate
      AND tpdata.employeenumber = x.employeenumber;
    -- ClockHoursPPTD
    UPDATE tpData
    SET TechClockHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday, SUM(b.techclockhoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey; 
    -- TechVacationPPTD  
    UPDATE tpData
    SET TechVacationHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechVacationHoursDay, SUM(b.TechVacationHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechVacationHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;   
    -- TechPTOHoursPPTD
    UPDATE tpData
    SET TechPTOHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechPTOHoursDay, SUM(b.TechPTOHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechPTOHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;  
    -- TechHolidayHoursPPTD
    UPDATE tpData
    SET TechHolidayHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechHolidayHoursDay, SUM(b.TechHolidayHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechHolidayHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;      
-- flag hours  
    -- TechFlagHoursDay 
    -- main shop
    UPDATE tpData  
    SET TechFlagHoursDay = x.flaghours
    FROM (
      SELECT a.technumber, a.thedate, round(sum(coalesce(b.flaghours, 0)), 2) AS flaghours
      FROM tpData a
      LEFT JOIN (
        SELECT d.thedate, ro, line, c.technumber, a.flaghours
        FROM dds.factRepairOrder a  
        INNER JOIN dds.dimTech c on a.techkey = c.techkey
        INNER JOIN dds.day d on a.flagdatekey = d.datekey
        WHERE flaghours <> 0
          AND a.storecode = 'ry1'
          AND a.flagdatekey IN (
            SELECT datekey
            FROM dds.day
            WHERE thedate BETWEEN '01/11/2015' AND curdate())   
          AND c.technumber IN (
            SELECT technumber
            FROM tpTechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
      GROUP BY a.technumber, a.thedate) x
    WHERE tpdata.departmentkey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'mainshop' AND dl5 = 'Shop')
      AND tpdata.thedate = x.thedate
      AND tpdata.technumber = x.technumber;     
    -- TechFlagHoursDay 
    -- body shop
    UPDATE tpData  
    SET TechFlagHoursDay = x.flaghours
    FROM (
      SELECT a.technumber, a.thedate, round(sum(coalesce(b.flaghours, 0)), 2) AS flaghours
      FROM tpData a
      LEFT JOIN (
        SELECT d.thedate, ro, line, c.technumber, a.flaghours
        FROM dds.factRepairOrder a  
        INNER JOIN dds.dimTech c on a.techkey = c.techkey
        INNER JOIN dds.day d on a.closedatekey = d.datekey
        WHERE flaghours <> 0
          AND a.storecode = 'ry1'
          AND a.closedatekey IN (
            SELECT datekey
            FROM dds.day
            WHERE thedate BETWEEN '01/11/2015' AND curdate())   
          AND c.technumber IN (
            SELECT technumber
            FROM tpTechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
      GROUP BY a.technumber, a.thedate) x
    WHERE tpdata.departmentkey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop')
      AND tpdata.thedate = x.thedate
      AND tpdata.technumber = x.technumber;         
    -- TechFlagHoursPPTD  
    UPDATE tpData
    SET TechFlagHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday, SUM(b.techflaghoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;   
-- adjustments
    UPDATE tpData
    SET TechFlagHourAdjustmentsDay = x.Adj
    FROM (
      SELECT a.technumber, a.lastname, a.thedate, coalesce(Adj, 0) AS Adj
      FROM tpData a
      LEFT JOIN (  
        select pttech, ptdate, round(SUM(ptlhrs), 2) AS adj
        FROM dds.stgArkonaSDPXTIM a
        INNER JOIN tpTechs b on a.pttech = b.technumber
        GROUP BY pttech, ptdate) b on a.technumber = b.pttech AND a.thedate = b.ptdate) x    
    WHERE tpData.technumber = x.technumber
      AND tpData.thedate = x.thedate;
    -- tech flag time adjustments pptd 
    UPDATE tpData
    SET TechFlagHourAdjustmentsPPTD = x.pptd
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechFlagHourAdjustmentsDay, SUM(b.TechFlagHourAdjustmentsDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechFlagHourAdjustmentsDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;       
-- *b*      
    UPDATE tpData
      SET techClockHoursDay = 0,
          techVacationHoursDay = 0,
          techPtoHoursDay = 0,
          techHolidayHoursDay = 0,
          techClockHoursPPTD = 0,
          techVacationHoursPPTD = 0,
          techPtoHoursPPTD = 0,
          techHolidayHoursPPTD = 0, 
          techFlagHoursDay = 0,   
          TechFlagHoursPPTD = 0,  
          TechFlagHourAdjustmentsDay = 0,
          TechFlagHourAdjustmentsPPTD = 0
    WHERE payperiodstart = '12/28/2014'
      AND lastname = 'johnson' AND firstname = 'cody';      
  CATCH ALL 
    RAISE tpdata(2, 'Techs ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- teams
TRY
  TRY  
    -- teamclockhoursDay
    UPDATE tpData 
    SET teamclockhoursday = x.teamclockhoursday
    FROM (
      SELECT thedate, teamkey, SUM(techclockhoursday) AS teamclockhoursday
      FROM tpdata
      GROUP BY thedate, teamkey) x 
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;  
    -- teamclockhours pptd
    UPDATE tpData
    SET teamclockhourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;       
    -- teamflaghoursday
    UPDATE tpData
    SET TeamFlagHoursDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(techflaghoursday) AS hours
      FROM tpdata 
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;
    -- teamflaghour pptd  
    UPDATE tpData
    SET teamflaghourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;  
    -- TeamFlagHourAdjustmentsDay
    UPDATE tpData
    SET TeamFlagHourAdjustmentsDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(TechFlagHourAdjustmentsDay) AS hours
      FROM tpdata 
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey; 
    -- TeamFlagHourAdjustmentsPPTD 
    UPDATE tpData
    SET TeamFlagHourAdjustmentsPPTD = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x 
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;   
    -- team prof day          
    UPDATE tpdata
    SET teamprofday = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhoursday = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhoursday, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay) AS TotalFlag, 
          teamclockhoursday
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay), 
          teamclockhoursday) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;      
    -- team prof pptd
    UPDATE tpdata
    SET teamprofpptd = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhourspptd = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhourspptd, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS TotalFlag, 
          teamclockhourspptd
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD), 
          teamclockhourspptd) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;        
  CATCH ALL 
    RAISE tpdata(3, 'Teams ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

/*
BEGIN TRANSACTION; -- paint teams combined proficiency
TRY
  TRY  
    UPDATE tpdata
    SET teamprofday = x.prof
    FROM (
      SELECT b.*,
        CASE 
          WHEN totalFlag = 0 OR totalClock = 0 THEN 0
          ELSE round(100 * totalFlag/totalClock, 2)
        END AS prof  
    	FROM (
          SELECT thedate, 
            SUM(teamFlagHoursDay + teamTrainingHoursDay 
              + teamFlagHourAdjustmentsDay) AS totalFlag,
            SUM(teamClockHoursDay) AS totalClock
          FROM ( 
            SELECT distinct thedate, teamkey,
              teamflaghoursday, teamTrainingHoursDay, 
              TeamFlagHourAdjustmentsDay, teamclockhoursday
            FROM tpdata
            WHERE teamkey IN (18,19)) a 
          GROUP BY thedate) b) x 
    WHERE tpData.teamKey IN (18,19)
      AND tpData.theDate = x.theDate;	  		

    UPDATE tpdata
    SET teamprofPPTD = x.prof
    FROM (
      SELECT b.*,
        CASE 
          WHEN totalFlag = 0 OR totalClock = 0 THEN 0
          ELSE round(100 * totalFlag/totalClock, 2)
        END AS prof  
    	FROM (
          SELECT thedate, 
            SUM(teamFlagHoursPPTD + teamTrainingHoursPPTD 
              + teamFlagHourAdjustmentsPPTD) AS totalFlag,
            SUM(teamClockHoursPPTD) AS totalClock
          FROM ( 
            SELECT distinct thedate, teamkey,
              teamflaghoursPPTD, teamTrainingHoursPPTD, 
              TeamFlagHourAdjustmentsPPTD, teamclockhoursPPTD
            FROM tpdata
            WHERE teamkey IN (18,19)) a 
          GROUP BY thedate) b) x 
    WHERE tpData.teamKey IN (18,19)
      AND tpData.theDate = x.theDate;	          
  CATCH ALL 
    RAISE tpdata(4, 'Paint Teams Proficiency ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
*/


END;


alter PROCEDURE todayXfmTpData
   ( 
   ) 
BEGIN 
/*
3/11/14
  added UPDATE date to teamProf to combine body shop paint teams (cody - 18, pete - 19)
  INTO a single proficiency number
  
6/30/15
  body shop coming back on to team pay, ALL techs on one team
  flag hours based on CLOSE date NOT flag date
  also took the opportunity to reduce the scrape periods
    
EXECUTE PROCEDURE todayXfmTpData();
*/
BEGIN TRANSACTION; -- clock hours
TRY
  TRY 
    UPDATE tpdata
    SET TechClockHoursDay = x.ClockHours
    FROM (
      SELECT b.thedate, c.employeenumber, round(SUM(clockhours), 4) AS clockhours
      FROM dds.factClockHoursToday a
      INNER JOIN dds.day b on a.datekey = b.datekey
        AND b.thedate = curdate()
      LEFT JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
      WHERE c.employeenumber IN (
        SELECT DISTINCT employeenumber
        FROM tpData)
      GROUP BY b.thedate, c.employeenumber) x  
    WHERE tpdata.thedate = x.thedate
      AND tpdata.employeenumber = x.employeenumber;  
    -- ClockHoursPPTD
    UPDATE tpData
    SET TechClockHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday, SUM(b.techclockhoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;   
      
    -- teamclockhoursDay
    UPDATE tpData 
    SET teamclockhoursday = x.teamclockhoursday
    FROM (
      SELECT thedate, teamkey, SUM(techclockhoursday) AS teamclockhoursday
      FROM tpdata
      GROUP BY thedate, teamkey) x 
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;  
    -- teamclockhours pptd
    UPDATE tpData
    SET teamclockhourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;           
  CATCH ALL 
    RAISE todayTpData(1, 'Clock Hours ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- flag hours
TRY
  TRY   
    -- TechFlagHoursDay 
    -- main shop
    UPDATE tpData  
    SET TechFlagHoursDay = x.flaghours
    FROM (
      SELECT a.technumber, a.thedate, round(sum(coalesce(b.flaghours, 0)), 2) AS flaghours
      FROM tpData a
      LEFT JOIN (    
        SELECT d.thedate, ro, line, c.technumber, a.flaghours
        FROM dds.todayFactRepairOrder a
        INNER JOIN dds.dimTech c on a.techkey = c.techkey
        INNER JOIN dds.day d on a.flagdatekey = d.datekey
        WHERE a.flaghours <> 0
          AND d.thedate = curdate()
          AND c.technumber IN (
            SELECT technumber
            FROM tptechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
      WHERE a.thedate = curdate()      
      GROUP BY a.technumber, a.thedate) x
    WHERE tpdata.departmentkey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'mainshop' AND dl5 = 'Shop')
      AND tpdata.thedate = x.thedate
      AND tpdata.technumber = x.technumber;      
    -- TechFlagHoursDay 
    -- body shop
    UPDATE tpData  
    SET TechFlagHoursDay = x.flaghours
    FROM (
      SELECT a.technumber, a.thedate, round(sum(coalesce(b.flaghours, 0)), 2) AS flaghours
      FROM tpData a
      LEFT JOIN (    
        SELECT d.thedate, ro, line, c.technumber, a.flaghours
        FROM dds.todayFactRepairOrder a
        INNER JOIN dds.dimTech c on a.techkey = c.techkey
        INNER JOIN dds.day d on a.closedatekey = d.datekey
        WHERE a.flaghours <> 0
          AND d.thedate = curdate()
          AND c.technumber IN (
            SELECT technumber
            FROM tptechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
      WHERE a.thedate = curdate()      
      GROUP BY a.technumber, a.thedate) x
    WHERE tpdata.departmentkey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop')
      AND tpdata.thedate = x.thedate
      AND tpdata.technumber = x.technumber;        
    -- TechFlagHoursPPTD  
    UPDATE tpData
    SET TechFlagHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday, SUM(b.techflaghoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;      
    -- teamflaghoursday
    UPDATE tpData
    SET TeamFlagHoursDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(techflaghoursday) AS hours
      FROM tpdata 
      WHERE thedate = curdate()
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;  
    -- teamflaghour pptd  
    UPDATE tpData
    SET teamflaghourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;    
  CATCH ALL 
    RAISE tpdata(2, 'Flag Hours ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- team proficiency
TRY
  TRY  
    UPDATE tpdata
    SET teamprofday = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhoursday = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhoursday, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay) AS TotalFlag, 
          teamclockhoursday
        FROM tpdata
        WHERE thedate = curdate()
        GROUP BY thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay), 
          teamclockhoursday) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;     
    -- team prof pptd
    UPDATE tpdata
    SET teamprofpptd = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhourspptd = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhourspptd, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS TotalFlag, 
          teamclockhourspptd
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD), 
          teamclockhourspptd) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;           
  CATCH ALL 
    RAISE tpdata(3, 'Team Proficiency ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
/*
BEGIN TRANSACTION; -- paint teams combined proficiency
TRY
  TRY  
    UPDATE tpdata
    SET teamprofday = x.prof
    FROM (
      SELECT b.*,
        CASE 
          WHEN totalFlag = 0 OR totalClock = 0 THEN 0
          ELSE round(100 * totalFlag/totalClock, 2)
        END AS prof  
    	FROM (
          SELECT thedate, 
            SUM(teamFlagHoursDay + teamTrainingHoursDay 
              + teamFlagHourAdjustmentsDay) AS totalFlag,
            SUM(teamClockHoursDay) AS totalClock
          FROM ( 
            SELECT distinct thedate, teamkey,
              teamflaghoursday, teamTrainingHoursDay, 
              TeamFlagHourAdjustmentsDay, teamclockhoursday
            FROM tpdata
            WHERE teamkey IN (18,19)) a 
          GROUP BY thedate) b) x 
    WHERE tpData.teamKey IN (18,19)
      AND tpData.theDate = x.theDate;	  		

    UPDATE tpdata
    SET teamprofPPTD = x.prof
    FROM (
      SELECT b.*,
        CASE 
          WHEN totalFlag = 0 OR totalClock = 0 THEN 0
          ELSE round(100 * totalFlag/totalClock, 2)
        END AS prof  
    	FROM (
          SELECT thedate, 
            SUM(teamFlagHoursPPTD + teamTrainingHoursPPTD 
              + teamFlagHourAdjustmentsPPTD) AS totalFlag,
            SUM(teamClockHoursPPTD) AS totalClock
          FROM ( 
            SELECT distinct thedate, teamkey,
              teamflaghoursPPTD, teamTrainingHoursPPTD, 
              TeamFlagHourAdjustmentsPPTD, teamclockhoursPPTD
            FROM tpdata
            WHERE teamkey IN (18,19)) a 
          GROUP BY thedate) b) x 
    WHERE tpData.teamKey IN (18,19)
      AND tpData.theDate = x.theDate;	          
  CATCH ALL 
    RAISE tpdata(3, 'Paint Teams Proficiency ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
*/         

END;

ALTER PROCEDURE GetTpTechROsByDay
   ( 
      userName CICHAR ( 50 ),
      payPeriodIndicator Integer,
      techName CICHAR ( 60 ) OUTPUT,
      flagDate DATE OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*

12/25/13
  modified to accomodate restructured factRepairOrder
1/2/14
  need to ADD todayFactRepairORder for real time updating of tech page
1/13/14
  GROUP BY ro, remove line  
2/10/14 *a*
  WHILE looking at previous payperiod returns ro FROM today
  eg 16144742 (2-10) shows for payperiod 1-26 -> 2/8  
3/5
  modify to username AS the only input param   
3/11/14 *c*
  holy shit, double counting flag hours WHEN exact same row EXISTS IN
  factRepairOrder AND todayFactRepairOrder
  the fucking issue seems to be unioning on flaghours AS a double sees the
  same values AS different, therefore returns the row twice AND summing the flag hours
  short term bandaid, round flag hours IN the base queries before unioning
2/19/15
*a*  reinstating matthew ramirez INTO team pay means that there are multiple
     rows IN tpTechs for him, need to get the current row only     
  
6/30/15
  body shop back on team pay, ALL techs on one team
  flag hours based on CLOSE date NOT flag date
   
EXECUTE PROCEDURE GetTpTechROsByDay('sanderson@rydellcars.com', 0);  
   
*/

DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;  
DECLARE @curPayPeriodSeq integer;  
DECLARE @payperiodseq integer;  
DECLARE @departmentKey integer;

@UserName = (SELECT username FROM __input);

-- *a*
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName
    AND a.thruDate = '12/31/9999'); 
@departmentKey = (
  SELECT departmentkey
  FROM tpTechs
  WHERE techNumber = @TechNumber
    AND thruDate = '12/31/9999');    
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);  
IF @departmentkey = (
  SELECT departmentkey 
  FROM zdepartments
  WHERE dl2 = 'ry1'
    AND dl3 = 'Fixed'
    AND dl4 = 'mainshop'
    AND dl5 = 'Shop') THEN 
  INSERT INTO __output
  SELECT top 1000 *
  FROM ( -- main shop
    SELECT techname, flagdate, ro, round(SUM(flaghours), 2) AS flaghours
    FROM (
      SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
    	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
      FROM dds.factRepairOrder a  
      INNER JOIN dds.dimTech c on a.techkey = c.techkey
      INNER JOIN tpTechs cc on c.technumber = cc.technumber
      INNER JOIN dds.day d on a.flagdatekey = d.datekey
      WHERE flaghours <> 0
        AND a.storecode = 'ry1'
        AND a.flagdatekey IN (
          SELECT datekey
          FROM dds.day
          WHERE thedate BETWEEN @fromDate AND @thruDate)
        AND c.technumber = @techNumber 
      UNION
      SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
    	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
      FROM dds.todayfactRepairOrder a  
      INNER JOIN dds.dimTech c on a.techkey = c.techkey
      INNER JOIN tpTechs cc on c.technumber = cc.technumber
      INNER JOIN dds.day d on a.flagdatekey = d.datekey
      WHERE flaghours <> 0
        AND a.storecode = 'ry1'  
  -- *a*          
        AND a.flagdatekey in (
          select datekey
          from dds.day
          WHERE thedate BETWEEN @fromDate AND @thruDate)                
        AND c.technumber = @techNumber) y
    GROUP BY techname, flagdate, ro) x	
  ORDER BY techName, flagDate, ro; 
ELSE IF @departmentkey = ( -- body shop
  SELECT departmentkey
  FROM zdepartments
  WHERE dl2 = 'ry1'
    AND dl3 = 'Fixed'
    AND dl4 = 'bodyshop') THEN 
  INSERT INTO __output
  SELECT top 1000 *
  FROM (
    SELECT techname, flagdate, ro, round(SUM(flaghours), 2) AS flaghours
    FROM (
      SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
    	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
      FROM dds.factRepairOrder a  
      INNER JOIN dds.dimTech c on a.techkey = c.techkey
      INNER JOIN tpTechs cc on c.technumber = cc.technumber
      INNER JOIN dds.day d on a.closedatekey = d.datekey
      WHERE flaghours <> 0
        AND a.storecode = 'ry1'
        AND a.closedatekey IN (
          SELECT datekey
          FROM dds.day
          WHERE thedate BETWEEN @fromDate AND @thruDate)
        AND c.technumber = @techNumber 
      UNION
      SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
    	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
      FROM dds.todayfactRepairOrder a  
      INNER JOIN dds.dimTech c on a.techkey = c.techkey
      INNER JOIN tpTechs cc on c.technumber = cc.technumber
      INNER JOIN dds.day d on a.closedatekey = d.datekey
      WHERE flaghours <> 0
        AND a.storecode = 'ry1'  
  -- *a*          
        AND a.closedatekey in (
          select datekey
          from dds.day
          WHERE thedate BETWEEN @fromDate AND @thruDate)                
        AND c.technumber = @techNumber) y
    GROUP BY techname, flagdate, ro) x	
  ORDER BY techName, flagDate, ro; 
END;    
END;          





END;


alter PROCEDURE GetTpTechRoTotals
   ( 
      userName CICHAR ( 50 ),
      payPeriodIndicator Integer,
      techName CICHAR ( 60 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*

12/25/13
  modified to accomodate restructured factRepairOrder
1/2/14
  need to ADD todayFactRepairORder for real time updating of tech page  
2/10/14 *a*
  WHILE looking at previous payperiod returns ro FROM today
  eg 16144742 (2-10) shows for payperiod 1-26 -> 2/8  
  once that was fixed, realized the total was also incorrect, same fix 
3/5
  modify to username AS the only input param      
  
3/11/14 *c*
  holy shit, double counting flag hours WHEN exact same row EXISTS IN
  factRepairOrder AND todayFactRepairOrder
  the fucking issue seems to be unioning on flaghours AS a double sees the
  same values AS different, therefore returns the row twice AND summing the flag hours
  short term bandaid, round flag hours IN the base queries before unioning  
  
2/19/15
*a*  reinstating matthew ramirez INTO team pay means that there are multiple
     rows IN tpTechs for him, need to get the current row only   
     
6/30/15
  body shop back on team pay, ALL techs on one team
  flag hours based on CLOSE date NOT flag date     
     
EXECUTE PROCEDURE GetTpTechRoTotals('alindom@rydellcars.com',0);        
*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;  
DECLARE @curPayPeriodSeq integer;  
DECLARE @payperiodseq integer; 
DECLARE @departmentKey integer; 

@UserName = (SELECT username FROM __input);

-- *a*
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName
    AND a.thruDate = '12/31/9999'); 
@departmentKey = (
  SELECT departmentkey
  FROM tpTechs
  WHERE techNumber = @TechNumber
    AND thruDate = '12/31/9999');     
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);  
IF @departmentkey = ( -- main shop
  SELECT departmentkey 
  FROM zdepartments
  WHERE dl2 = 'ry1'
    AND dl3 = 'Fixed'
    AND dl4 = 'mainshop'
    AND dl5 = 'Shop') THEN   
  INSERT INTO __output 
  SELECT techname, round(SUM(flaghours), 2)
  FROM (
    SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
      	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
      FROM dds.factRepairOrder a  
      INNER JOIN dds.dimTech c on a.techkey = c.techkey
      INNER JOIN tpTechs cc on c.technumber = cc.technumber
      INNER JOIN dds.day d on a.flagdatekey = d.datekey
      WHERE flaghours <> 0
        AND a.storecode = 'ry1'
        AND a.flagdatekey IN (
          SELECT datekey
          FROM dds.day
          WHERE thedate BETWEEN @fromDate AND @thruDate) 
        AND c.technumber = @techNumber 
    UNION  
    SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
      	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
      FROM dds.todayfactRepairOrder a  
      INNER JOIN dds.dimTech c on a.techkey = c.techkey
      INNER JOIN tpTechs cc on c.technumber = cc.technumber
      INNER JOIN dds.day d on a.flagdatekey = d.datekey
      WHERE flaghours <> 0
        AND a.storecode = 'ry1'
  -- *a*          
        AND a.flagdatekey in (
          select datekey
          from dds.day
          WHERE thedate BETWEEN @fromDate AND @thruDate)                
        AND c.technumber = @techNumber) y
  GROUP BY techname; 
ELSE IF @departmentkey = ( -- body shop
  SELECT departmentkey
  FROM zdepartments
  WHERE dl2 = 'ry1'
    AND dl3 = 'Fixed'
    AND dl4 = 'bodyshop') THEN 
  INSERT INTO __output 
  SELECT techname, round(SUM(flaghours), 2)
  FROM (
    SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
      	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
      FROM dds.factRepairOrder a  
      INNER JOIN dds.dimTech c on a.techkey = c.techkey
      INNER JOIN tpTechs cc on c.technumber = cc.technumber
      INNER JOIN dds.day d on a.closedatekey = d.datekey
      WHERE flaghours <> 0
        AND a.storecode = 'ry1'
        AND a.closedatekey IN (
          SELECT datekey
          FROM dds.day
          WHERE thedate BETWEEN @fromDate AND @thruDate) 
        AND c.technumber = @techNumber 
    UNION  
    SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
      	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
      FROM dds.todayfactRepairOrder a  
      INNER JOIN dds.dimTech c on a.techkey = c.techkey
      INNER JOIN tpTechs cc on c.technumber = cc.technumber
      INNER JOIN dds.day d on a.closedatekey = d.datekey
      WHERE flaghours <> 0
        AND a.storecode = 'ry1'
  -- *a*          
        AND a.closedatekey in (
          select datekey
          from dds.day
          WHERE thedate BETWEEN @fromDate AND @thruDate)                
        AND c.technumber = @techNumber) y
  GROUP BY techname;     
END;
END;




END;


alter PROCEDURE xfmTmpBen
   ( 
   ) 
BEGIN 
/*
7/23/14
  based on the script Z:\E\Intranet SQL Scripts\v2.9.0.sql, which generated the
    initial data (since 1/1/2013)
  this deletes AND repopulates tmpBen with the most recent 40 days of data
  this IS flag & time clock data based on tmpDeptTechCensus
  used for RydellVision pages Body Shop->Production Summary 
    AND Main Shop->Production Summary 
    
  depends on :
    tmpDeptTechCensus
    factRepairOrder
    edwClockHoursFact 
    
7/1/15
  body shop went off AND now IS coming back on to team pay, one of the changes
  IS that they nos base flag hours on the CLOSE date, AS opposed to flag date
  this script populates tmpBen exclusively on flagdate    
  
EXECUTE PROCEDURE xfmTmpBen();
*/
  BEGIN TRANSACTION;
  TRY
    DELETE 
    FROM tmpBen
    WHERE thedate BETWEEN curdate() - 41 AND curdate() - 1;
    INSERT INTO tmpBen -- body shop: CLOSE date
    SELECT a.thedate, a.dayofweek, a.monthofyear, a.monthname, a.holiday, 
      a.sundaytosaturdayweek, a.yearmonth, a.weekstartdate, a.weekenddate,
      b.storecode, b.techkey, b.employeekey, b.employeenumber, 
      b.description, 
      b.techNumber, b.flagDeptCode, b.firstname, b.lastname, 
      b.team, coalesce(c.flaghours, 0) AS flagHours, 
      coalesce(c.shopTime, 0) AS shopTime,
      coalesce(d.clockhours, 0) AS clockhours, 
      SundayToSaturdayWeekSelectFormat 
    FROM (
      SELECT thedate, dayOfWeek, monthOfYear, monthName, weekDay, weekend, holiday,
        sundayToSaturdayWeek, yearmonth,
        (SELECT MIN(thedate) FROM dds.day WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek) AS weekStartDate,
        (SELECT MAX(thedate) FROM dds.day WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek) AS weekEndDate,
        (SELECT MIN(thedate) FROM dds.day WHERE yearmonth = a.yearmonth) AS monthStart,
        (SELECT MAX(thedate) FROM dds.day WHERE yearmonth = a.yearmonth) AS monthEnd,
        SundayToSaturdayWeekSelectFormat
      FROM dds.day a
      WHERE datetype = 'date'
        AND thedate BETWEEN curdate() - 41 AND curdate() - 1) a
    LEFT JOIN tmpDeptTechCensus b on a.thedate = b.thedate
      AND b.flagdeptcode = 'bs'
    LEFT JOIN (
      SELECT thedate, techkey, coalesce(flaghours, 0) + coalesce(ptlhrs, 0) AS flagHours, shopTime
      FROM (
        SELECT b.storecode, a.thedate, c.techkey, c.technumber, 
          round(SUM(CASE WHEN b.opcodekey NOT IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS flagHours,
          round(SUM(CASE WHEN b.opcodekey IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS shopTime  
        FROM dds.day a
        LEFT JOIN dds.factRepairOrder b on a.datekey = b.closedatekey
        LEFT JOIN dds.dimTech c on b.techKey = c.techkey
        WHERE thedate BETWEEN curdate() - 41 AND curdate() - 1
          AND c.techkey IS NOT NULL 
        GROUP BY  a.thedate, b.storecode, c.technumber, c.techkey, c.technumber) m
    LEFT JOIN (
      SELECT ptco#, ptdate, pttech, SUM(ptlhrs) AS ptlhrs
      FROM dds.stgArkonaSDPXTIM
      GROUP BY ptco#, ptdate, pttech) n on m.thedate = n.ptdate
        AND m.storecode = n.ptco#
        AND m.technumber = n.pttech) c on a.thedate = c.thedate
      AND b.techkey = c.techkey
    LEFT JOIN (
      SELECT b.thedate, employeekey, SUM(clockHours) AS clockHours
      FROM dds.edwClockHoursFact a
      INNER JOIN dds.day b on a.datekey = b.datekey
      WHERE thedate BETWEEN curdate() - 41 AND curdate() - 1
        AND a.employeekey IN (
          SELECT distinct employeekey
          FROM tmpDeptTechCensus)
      GROUP BY thedate, employeekey) d on a.thedate = d.thedate
        AND b.employeekey = d.employeekey; 
        
    INSERT INTO tmpBen -- all except body shop: flag date
    SELECT a.thedate, a.dayofweek, a.monthofyear, a.monthname, a.holiday, 
      a.sundaytosaturdayweek, a.yearmonth, a.weekstartdate, a.weekenddate,
      b.storecode, b.techkey, b.employeekey, b.employeenumber, 
      b.description, 
      b.techNumber, b.flagDeptCode, b.firstname, b.lastname, 
      b.team, coalesce(c.flaghours, 0) AS flagHours, 
      coalesce(c.shopTime, 0) AS shopTime,
      coalesce(d.clockhours, 0) AS clockhours, 
      SundayToSaturdayWeekSelectFormat
    FROM (
      SELECT thedate, dayOfWeek, monthOfYear, monthName, weekDay, weekend, holiday,
        sundayToSaturdayWeek, yearmonth,
        (SELECT MIN(thedate) FROM dds.day WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek) AS weekStartDate,
        (SELECT MAX(thedate) FROM dds.day WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek) AS weekEndDate,
        (SELECT MIN(thedate) FROM dds.day WHERE yearmonth = a.yearmonth) AS monthStart,
        (SELECT MAX(thedate) FROM dds.day WHERE yearmonth = a.yearmonth) AS monthEnd,
        SundayToSaturdayWeekSelectFormat
      FROM dds.day a
      WHERE datetype = 'date'
        AND thedate BETWEEN curdate() - 41 AND curdate() - 1) a
    LEFT JOIN tmpDeptTechCensus b on a.thedate = b.thedate
      AND b.flagdeptcode <> 'bs'
    LEFT JOIN (
      SELECT thedate, techkey, coalesce(flaghours, 0) + coalesce(ptlhrs, 0) AS flagHours, shopTime
      FROM (
        SELECT b.storecode, a.thedate, c.techkey, c.technumber, 
          round(SUM(CASE WHEN b.opcodekey NOT IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS flagHours,
          round(SUM(CASE WHEN b.opcodekey IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS shopTime  
        FROM dds.day a
        LEFT JOIN dds.factRepairOrder b on a.datekey = b.flagdatekey
        LEFT JOIN dds.dimTech c on b.techKey = c.techkey
        WHERE thedate BETWEEN curdate() - 41 AND curdate() - 1
          AND c.techkey IS NOT NULL 
        GROUP BY  a.thedate, b.storecode, c.technumber, c.techkey, c.technumber) m
    LEFT JOIN (
      SELECT ptco#, ptdate, pttech, SUM(ptlhrs) AS ptlhrs
      FROM dds.stgArkonaSDPXTIM
      GROUP BY ptco#, ptdate, pttech) n on m.thedate = n.ptdate
        AND m.storecode = n.ptco#
        AND m.technumber = n.pttech) c on a.thedate = c.thedate
      AND b.techkey = c.techkey
    LEFT JOIN (
      SELECT b.thedate, employeekey, SUM(clockHours) AS clockHours
      FROM dds.edwClockHoursFact a
      INNER JOIN dds.day b on a.datekey = b.datekey
      WHERE thedate BETWEEN curdate() - 41 AND curdate() - 1
        AND a.employeekey IN (
          SELECT distinct employeekey
          FROM tmpDeptTechCensus)
      GROUP BY thedate, employeekey) d on a.thedate = d.thedate
        AND b.employeekey = d.employeekey; 
        
                
  COMMIT WORK;
  CATCH ALL
    ROLLBACK;
    RAISE;
  END TRY;      

END;




