DECLARE @date date;
@date = '02/08/2014';

select x.*, round(TotalCommPay - transition, 2) AS [Comm - Transition]
FROM (
  select c.teamname AS Team, lastname, firstname, technumber, employeenumber, 
    techhourlyrate AS HourlyRate, round(techtfrrate, 2) AS CommRate, 
    techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
    round(techclockhourspptd*TeamProfPPTD/100, 2) as CommHours, 
    round(round(techtfrrate, 2)*round(TeamProfPPTD*techclockhourspptd/100, 2), 2) AS CommPay,
    techvacationhourspptd AS VacationHours,
    techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
--    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) + 
    round(round(techtfrrate, 2) * round(techclockhourspptd * teamprofpptd/100, 2), 2) + 
      (techvacationhourspptd + techptohourspptd + 
        techholidayhourspptd)*techhourlyrate AS TotalCommPay,
     round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
       techholidayhourspptd)*techhourlyrate, 2) AS Transition
--SELECT *       
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
--  INNER JOIN tpteams c on b.teamkey = c.teamkey
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
    AND c.departmentKey = 13
  WHERE thedate = @date
    AND a.departmentKey = 13
  union
  select c.teamname AS Team, left(cast(NULL AS sql_char), 1),left(cast(NULL AS sql_char), 1),
    left(cast(NULL AS sql_char), 1),left(cast(NULL AS sql_char), 1),
    CAST(NULL AS sql_integer),CAST(NULL AS sql_integer),
    sum(techclockhourspptd) AS ClockHours, sum(TechFlagHoursPPTD) as FlagHours, 
    max(teamprofpptd) AS TeamProf, 
    CAST(NULL AS sql_integer), CAST(NULL AS sql_integer),
    sum(techvacationhourspptd) AS VacationHours,
    sum(techptohourspptd) AS PTOHours, sum(techholidayhourspptd) AS HolidayHours,
    CAST(NULL AS sql_integer),CAST(NULL AS sql_integer)
--SELECT *    
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
--  INNER JOIN tpteams c on b.teamkey = c.teamkey
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
    AND c.departmentKey = 13
  WHERE thedate = @date
    AND a.departmentKey = 13
  GROUP BY c.TeamName) x
  
