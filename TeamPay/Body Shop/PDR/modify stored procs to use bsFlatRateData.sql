alter PROCEDURE getBsFlatRateDetail
   ( 
      username CICHAR ( 50 ),
      payPeriodIndicator Integer,
      date DATE OUTPUT,
      workType CICHAR ( 6 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*

-- with new tables 
GROUP BY date AND workType (pdr/metal)

EXECUTE PROCEDURE getBsFlatRateDetail('bpeterson@rydellcars.com', 0);
*/

DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM bsFlatRateData WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));

@username = (SELECT username FROM __input);

INSERT INTO __output
  SELECT a.theDate, 'Metal' AS workType, 
    a.techMetalFlagHoursDay AS flagHours,
    round(techMetalFlagHoursDay * b.metalRate, 2) AS commissionPay
  FROM bsFlatRateData a
  INNER JOIN bsFlatRateTechs b on a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
  WHERE  payPeriodSeq = @payPeriodSeq
--    AND techMetalFlagHoursDay <> 0  
  UNION
  SELECT theDate, 'PDR', techPdrFlagHoursDay,
    round(techPdrFlagHoursDay * b.pdrRate, 2)
  FROM bsFlatRateData a
  INNER JOIN bsFlatRateTechs b on a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
  WHERE payPeriodSeq = @payPeriodSeq;
--    AND techPdrFlagHoursDay <> 0; 
	

/*
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@UserName = (SELECT username FROM __input);
@employeeNumber = '1110425';
@pdrRate = 14.4;
@metalRate = 19.95;
INSERT INTO __output
SELECT theDate, workType, SUM(flagHours) AS flagHours, SUM(pay) AS pay
FROM (
    SELECT theDate, 'PDR' as workType, flagHours, 
	  round(@pdrRate * flagHours, 2) AS pay 
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq
	  AND c.opcode = 'PDR'
	
	UNION ALL
	 
    SELECT theDate, 'Metal', flagHours , 
	  round(@metalRate * flagHours, 2) AS pay
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq	
	  AND c.opcode <> 'PDR') x
GROUP BY theDate, workType;	    
	  
*/



END;

alter PROCEDURE getBsFlatRateManagerDetails
   ( 
      department CICHAR ( 12 ),
      payPeriodIndicator Integer,
      username CICHAR ( 50 ) OUTPUT,
      techName CICHAR ( 60 ) OUTPUT,
      workType CICHAR ( 12 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      commissionPay DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 

/*

-- with new tables

EXECUTE PROCEDURE getBsFlatRateManagerDetails('bodyshop', 0);
*/

DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @department string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM bsFlatRateData WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM bsFlatRateData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@department = (SELECT upper(department) FROM __input);
IF @department <> 'BODYSHOP' THEN
  SELECT * FROM system.iota WHERE 1 = 2;
ELSE 
INSERT INTO __output
SELECT username, fullname, workType, SUM(flagHours), SUM(commissionPay)
FROM (
  SELECT a.username, b.fullname, a.theDate, 'Metal' AS workType, 
    a.techMetalFlagHoursDay AS flagHours,
    round(techMetalFlagHoursDay * b.metalRate, 2) AS commissionPay
  FROM bsFlatRateData a
  INNER JOIN bsFlatRateTechs b on a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
  WHERE  payPeriodSeq = @payPeriodSeq
--    AND techMetalFlagHoursDay <> 0  
  UNION
  SELECT a.username, b.fullname, theDate, 'PDR', techPdrFlagHoursDay,
    round(techPdrFlagHoursDay * b.pdrRate, 2)
  FROM bsFlatRateData a
  INNER JOIN bsFlatRateTechs b on a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
  WHERE payPeriodSeq = @payPeriodSeq) z   
--    AND techPdrFlagHoursDay <> 0) z    
GROUP BY username, fullname, workType;
END IF;


/*
--DECLARE @date date;
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
DECLARE @name string;
DECLARE @username string;
DECLARE @department string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@employeeNumber = '1110425';
@name = (
  SELECT fullname
  FROM tpEmployees
  WHERE employeeNumber = @employeeNumber);
@username = (
  SELECT username
  FROM tpEmployees
  WHERE employeeNumber = @employeeNumber);
@pdrRate = 14.4;
@metalRate = 19.95;
@department = (SELECT upper(department) FROM __input);
IF @department <> 'BODYSHOP' THEN
  SELECT * FROM system.iota WHERE 1 = 2;
ELSE 
INSERT INTO __output

    SELECT @username, @name, 'PDR' as workType, sum(flagHours) AS pdrHours, 
	  round(@pdrRate * sum(flagHours), 2) AS pdrPay 
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq
	  AND c.opcode = 'PDR'
	
	UNION ALL
	 
    SELECT @username, @name, 'Metal', sum(flagHours) AS metalHours , 
	  round(@metalRate * sum(flagHours), 2) AS metalPay
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq	
	  AND c.opcode <> 'PDR';
	  
*/	  




END;


alter PROCEDURE getBsFlatRateManagerTotals
   ( 
      department CICHAR ( 12 ),
      payPeriodIndicator Integer,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      proficiency DOUBLE ( 15 ) OUTPUT,
      commissionPay DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 

/*

-- with new tables

EXECUTE PROCEDURE getBsFlatRateManagerTotals('bodyshop', 0);

*/

DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @department string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM bsFlatRateData WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM bsFlatRateData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@department = (SELECT upper(department) FROM __input);
IF @department <> 'BODYSHOP' THEN
  SELECT * FROM system.iota WHERE 1 = 2;
ELSE 
  INSERT INTO __output
  SELECT flagHours, techClockHoursPPTD, techProficiencyPPTD, commissionPay
  FROM (
    SELECT SUM(flagHours) AS flagHours, SUM(commissionPay) AS commissionPay
    FROM (
      SELECT a.username, b.fullname, 
        a.theDate, a.departmentKey, a.techNumber, 
    	'Metal' AS workType, 
        a.techMetalFlagHoursDay AS flagHours,
        round(techMetalFlagHoursDay * b.metalRate, 2) AS commissionPay
      FROM bsFlatRateData a
      INNER JOIN bsFlatRateTechs b on a.departmentKey = b.departmentKey
        AND a.techNumber = b.techNumber
      WHERE  payPeriodSeq = @payPeriodSeq
--        AND techMetalFlagHoursDay <> 0  
      UNION
      SELECT a.username, b.fullname, 
        a.theDate, a.departmentKey, a.techNumber,
        'PDR', techPdrFlagHoursDay,
        round(techPdrFlagHoursDay * b.pdrRate, 2)
      FROM bsFlatRateData a
      INNER JOIN bsFlatRateTechs b on a.departmentKey = b.departmentKey
        AND a.techNumber = b.techNumber
      WHERE payPeriodSeq = @payPeriodSeq) d) e
--        AND techPdrFlagHoursDay <> 0) d) e
  LEFT JOIN (
    SELECT techClockHoursPPTD, techProficiencyPPTD 
    FROM bsFlatRateData 
    WHERE theDate = (
      SELECT MAX(theDate)
  	  FROM bsFlatRateData
  	  WHERE payPeriodSeq = @payPeriodSeq)) f on 1 = 1;	  
END IF;

/*

--DECLARE @date date;
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
DECLARE @otherRate double;
DECLARE @name string;
DECLARE @department string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@employeeNumber = '1110425';
@name = (
  SELECT fullname
  FROM tpEmployees
  WHERE username = @username);
@pdrRate = 14.4;
@metalRate = 19.95;
@otherRate = (
  SELECT hourlyRate
  FROM dds.edwEmployeeDim 
  WHERE employeenumber = @employeeNumber
    AND employeeKeyFromDate < @fromDate
    AND employeeKeyThruDate > @thruDate);
@department = (SELECT upper(department) FROM __input);	
IF @department <> 'BODYSHOP' THEN
  SELECT * FROM system.iota WHERE 1 = 2;
ELSE 
	
INSERT INTO __output
SELECT flagHours, clockHours, 
  CASE clockHours
    WHEN 0 THEN 0
	ELSE round(1.0 * flagHours/clockHours, 2) 
  END AS proficiency,
  commissionPay 
FROM ( 
  SELECT round(SUM(pdrFlagHours),2) + round(SUM(metalFlagHours), 2) AS flagHours, 
    round(@pdrRate * round(SUM(pdrFlagHours),2), 2) + round(@metalRate * round(SUM(metalFlagHours), 2), 2) AS commissionPay
  FROM (  
    SELECT theDate, @pdrRate AS pdrRate, @metalRate AS metalRate,
      coalesce(CASE WHEN c.opcode = 'PDR' THEN flaghours END, 0) AS pdrFlagHours,
      coalesce(CASE WHEN c.opcode <>'PDR' THEN flaghours END, 0) AS metalFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq) m
    GROUP BY pdrRate, metalRate) n  
LEFT JOIN (
  SELECT sum(clockHours) as clockHours, SUM(vacationHours) + sum(ptoHours) + sum(holidayHours) AS otherHours, 
    @otherRate * (SUM(vacationHours) + sum(ptoHours) + sum(holidayHours)) AS hourlyPay
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.biWeeklyPayPeriodSequence = @payPeriodSeq) o  on 1 = 1;
END IF;

*/



END;



alter PROCEDURE getBsFlatRateOtherHoursDetail
   ( 
      username CICHAR ( 50 ),
      payPeriodIndicator Integer,
      theDate DATE OUTPUT,
      vacationHours DOUBLE ( 15 ) OUTPUT,
      ptoHours DOUBLE ( 15 ) OUTPUT,
      holidayHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*  

-- with new tables

EXECUTE PROCEDURE getBsFlatRateOtherHoursDetail('bpeterson@rydellcars.com', 0);

*/

DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM bsFlatRateData WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM bsFlatRateData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@username = (SELECT username FROM __input);
INSERT INTO __output
SELECT theDate, techVacationHoursDay, techPtoHoursDay, techHolidayHoursDay
FROM bsFlatRateData
WHERE username = @username
  AND payPeriodSeq = @payPeriodSeq;
--  AND techVacationHoursDay + techPtoHoursDay + techHolidayHoursDay <> 0;
  
  
/*
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@UserName = (SELECT username FROM __input);
@employeeNumber = '1110425';


INSERT INTO __output

  SELECT theDate, vacationHours, ptoHours, holidayHours
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.biWeeklyPayPeriodSequence = @payPeriodSeq   
    AND vacationHours + ptoHours + holidayHours <> 0;

*/

END;


alter PROCEDURE getBsFlatRateOtherHoursTotals
   ( 
      username CICHAR ( 50 ),
      payPeriodIndicator Integer,
      vacationHours DOUBLE ( 15 ) OUTPUT,
      ptoHours DOUBLE ( 15 ) OUTPUT,
      holidayHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*  

-- with new tables

  
EXECUTE PROCEDURE getBsFlatRateOtherHoursTotals('bpeterson@rydellcars.com', 0);

*/

DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@username = (SELECT username FROM __input);
INSERT INTO __output
SELECT sum(techVacationHoursDay) AS vacationHous, 
  sum(techPtoHoursDay) AS ptoHours, sum(techHolidayHoursDay) AS holidayHours
FROM bsFlatRateData
WHERE username = @username
  AND payPeriodSeq = @payPeriodSeq;
--  AND techVacationHoursDay + techPtoHoursDay + techHolidayHoursDay <> 0;
  
/*
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@UserName = (SELECT username FROM __input);
@employeeNumber = '1110425';


INSERT INTO __output

  SELECT sum(vacationHours) AS vacationHours, 
    sum(ptoHours) AS ptoHours, sum(holidayHours) AS holdayHours
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.biWeeklyPayPeriodSequence = @payPeriodSeq   
    AND vacationHours + ptoHours + holidayHours <> 0;

*/

END;

alter PROCEDURE getBsFlatRateSummary
   ( 
      username CICHAR ( 50 ),
      payPeriodIndicator Integer,
      techName CICHAR ( 60 ) OUTPUT,
      pdrRate DOUBLE ( 15 ) OUTPUT,
      metalRate DOUBLE ( 15 ) OUTPUT,
      otherRate DOUBLE ( 15 ) OUTPUT,
      proficiency DOUBLE ( 15 ) OUTPUT,
      pdrHours DOUBLE ( 15 ) OUTPUT,
      metalHours DOUBLE ( 15 ) OUTPUT,
      otherHours DOUBLE ( 15 ) OUTPUT,
      pdrPay DOUBLE ( 15 ) OUTPUT,
      metalPay DOUBLE ( 15 ) OUTPUT,
      otherPay DOUBLE ( 15 ) OUTPUT,
      totalCommissionPay DOUBLE ( 15 ) OUTPUT,
      totalGrossPay DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 

/*

-- with new tables

EXECUTE PROCEDURE getBsFlatRateSummary('bpeterson@rydellcars.com', -0);

*/

DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string;
DECLARE @date date;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM bsFlatRateData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM bsFlatRateData
  WHERE payPeriodSeq = @payPeriodSeq);
  
@username = (SELECT username FROM __input);
INSERT INTO __output
SELECT m.fullname, c.pdrRate, c.metalRate, c.otherRate, techProficiencyPPTD, 
  pdrFlagHours, metalFlagHours, otherHours,
  pdrPay, metalPay, 
  round(otherRate * otherHours, 2) AS otherPay,
  pdrPay + metalPay AS totalCommission, 
  pdrPay + metalPay + round(otherRate * otherHours, 2) AS totalGross
FROM (
  SELECT fullname, 
    sum(a.techMetalFlagHoursDay) AS metalFlagHours,
    sum(round(techMetalFlagHoursDay * b.metalRate, 2)) AS metalPay
  FROM bsFlatRateData a
  INNER JOIN bsFlatRateTechs b on a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
  WHERE a.payPeriodSeq = @payPeriodSeq
    AND a.username = @username
--    AND a.techMetalFlagHoursDay <> 0
  GROUP BY fullname) m  
LEFT JOIN (
  SELECT sum(techPdrFlagHoursDay) AS pdrFlagHours,
    sum(round(techPdrFlagHoursDay * b.pdrRate, 2)) AS pdrPay
  FROM bsFlatRateData a
  INNER JOIN bsFlatRateTechs b on a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
  WHERE a.payPeriodSeq = @payPeriodSeq
    AND a.username = @username) n on 1 = 1 
--    AND a.techPdrFlagHoursDay <> 0) n on 1 = 1 
LEFT JOIN bsFlatRateTechs c on c.username = @username
  AND thruDate > curdatE()	
LEFT JOIN (
  SELECT 
    techVacationHoursPPTD + techPtoHoursPPTD + techHolidayHoursPPTD AS otherHours,
    techProficiencyPPTD
  FROM bsFlatRateData
  WHERE username = @username
    AND theDate = @date) d on 1 = 1;  
	
/*	

--DECLARE @date date;
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
DECLARE @otherRate double;
DECLARE @name string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@UserName = (SELECT username FROM __input);
@employeeNumber = '1110425';
@name = (
  SELECT fullname
  FROM tpEmployees
  WHERE username = @username);
@pdrRate = 14.4;
@metalRate = 19.95;
@otherRate = (
  SELECT hourlyRate
  FROM dds.edwEmployeeDim 
  WHERE employeenumber = @employeeNumber
    AND employeeKeyFromDate < @fromDate
    AND employeeKeyThruDate > @thruDate);
INSERT INTO __output
SELECT @name AS name, @pdrRate AS pdrRate, @metalRate AS metalRate, 
  @otherRate AS otherRate, 
  round((pdrFlagHours + metalFlagHours)/clockHours, 2) as proficiency,
  n.pdrFlagHours, n.metalFlagHours, otherHours, pdrPay, metalPay, hourlyPay,
  pdrPay + metalPay AS totalCommissionPay, 
  pdrPay + metalPay + hourlyPay AS totalGrossPay
FROM ( 
  SELECT round(SUM(pdrFlagHours),2) AS pdrFlagHours, 
    round(@pdrRate * round(SUM(pdrFlagHours),2), 2) AS pdrPay,
    round(SUM(metalFlagHours), 2) AS metalFlagHours,
    round(@metalRate * round(SUM(metalFlagHours), 2), 2) AS metalPay
  FROM (  
    SELECT theDate, @pdrRate AS pdrRate, @metalRate AS metalRate,
      coalesce(CASE WHEN c.opcode = 'PDR' THEN flaghours END, 0) AS pdrFlagHours,
      coalesce(CASE WHEN c.opcode <>'PDR' THEN flaghours END, 0) AS metalFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq) m
    GROUP BY pdrRate, metalRate) n  
LEFT JOIN (
  SELECT sum(clockHours) as clockHours, SUM(vacationHours) + sum(ptoHours) + sum(holidayHours) AS otherHours, 
    @otherRate * (SUM(vacationHours) + sum(ptoHours) + sum(holidayHours)) AS hourlyPay
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.biWeeklyPayPeriodSequence = @payPeriodSeq) o  on 1 = 1;

*/

END;

alter PROCEDURE getBsFlatRateTotal
   ( 
      username CICHAR ( 50 ),
      payPeriodIndicator Integer,
      flagHours DOUBLE ( 15 ) OUTPUT,
      commissionPay DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*

-- with new tables

  
EXECUTE PROCEDURE GetBsFlatRateTotal ('bpeterson@rydellcars.com', 0);

*/

DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string;
DECLARE @date date;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM bsFlatRateData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM bsFlatRateData
  WHERE payPeriodSeq = @payPeriodSeq);
  
@username = (SELECT username FROM __input);
INSERT INTO __output
SELECT techPdrFlagHoursPPTD + techMetalFlagHoursPPTD AS flagHours, 
  round(techPdrFlagHoursPPTD * pdrRate, 2) + 
    round(techMetalFlagHoursPPTD * metalRate,2) AS commissionPay
FROM bsFlatRateData a
INNER JOIN bsFlatRateTechs b on a.departmentKey = b.departmentKey
  AND a.techNumber = b.techNumber
  AND b.thruDate > curdate()
WHERE a.username = @username
  AND a.thedate = @date;
  
/*
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@UserName = (SELECT username FROM __input);
@employeeNumber = '1110425';
@pdrRate = 14.4;
@metalRate = 19.95;
INSERT INTO __output
SELECT SUM(flagHours) AS flagHours, round(SUM(pay), 2) AS pay
FROM (
    SELECT flagHours, 
	  round(@pdrRate * flagHours, 2) AS pay 
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq
	  AND c.opcode = 'PDR'
	
	UNION ALL
	 
    SELECT flagHours , 
	  round(@metalRate * flagHours, 2) AS pay
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq	
	  AND c.opcode <> 'PDR') x;	    
	  
*/


END;



