-- SELECT * FROM bsFlatRateData ORDER BY thedate

-- DELETE FROM bsFlatRateData
  
-- base row
TRY 
  INSERT INTO bsFlatRateData (dateKey, theDate, payPeriodStart, payPeriodEnd, 
    payPeriodSeq, dayOfPayPeriod, dayName, payPeriodSelectFormat, 
    storeCode, departmentKey, technumber, employeeNumber, username) 
  SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
    a.biweeklypayperiodsequence, a.dayinbiweeklypayperiod, a.dayname, x.ppsel, 
    storeCode, b.departmentKey, b.techNumber, employeeNumber, b.username
  FROM dds.day a
  LEFT JOIN (
    SELECT aa.storecode, aa.employeenumber, aa.firstname, aa.lastname, 
	  cc.fullname, bb.username, cc.departmentKey, cc.techNumber
    FROM dds.edwEmployeeDim aa
    INNER JOIN tpEmployees bb on aa.employeenumber = bb.employeenumber
	INNER JOIN bsFlatRateTechs cc on bb.employeeNumber = cc.employeeNumber -- from/thru goddamnit
    WHERE aa.currentrow = true
      AND aa.active = 'active') b on 1 = 1        
  LEFT JOIN (
    SELECT biweeklypayperiodsequence, TRIM(pt1) + ' ' + TRIM(pt2) AS ppsel
      FROM (  
      SELECT distinct biweeklypayperiodstartdate, biweeklypayperiodenddate, biweeklypayperiodsequence, 
        (SELECT trim(monthname) + ' '
          + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
          FROM dds.day
          WHERE thedate = w.biweeklypayperiodstartdate) AS pt1,
        (SELECT trim(monthname) + ' ' 
          + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
          + TRIM(CAST(theyear AS sql_char))
          FROM dds.day
          WHERE thedate = w.biweeklypayperiodenddate) AS pt2
      FROM dds.day w
      WHERE w.thedate BETWEEN '01/12/2014' AND curdate()) y) x on a.biweeklypayperiodsequence = x.biweeklypayperiodsequence             
  WHERE a.theDate BETWEEN '01/12/2014' and curdate()
  AND NOT EXISTS (  
    SELECT 1
    FROM bsFlatRateData
    WHERE thedate = a.thedate);         
CATCH ALL 
  RAISE bsFlatRateData(1, 'Base Row ' + __errtext);
END TRY;    

TRY   
/*
  UPDATE bsFlatRateData
  SET techClockHoursDay = x.clockHours,
      techVacationHoursDay = x.vac,
      techPtoHoursDay = x.pto,
      techHolidayHoursDay = x.hol 
  FROM (       
    SELECT a.thedate, a.storecode, a.employeenumber,
      SUM(c.clockHours) AS clockHours,
      SUM(c.vacationHours) AS VAC, SUM(c.ptohours) AS PTO, 
      SUM(c.holidayhours) AS HOL 
    FROM bsFlatRateData a    
    LEFT JOIN dds.edwEmployeeDim b on a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeeNumber
    LEFT JOIN dds.edwClockHoursFact c on a.datekey = c.datekey
      AND b.employeekey = c.employeekey
    GROUP BY a.thedate, a.storecode, a.employeenumber) x 
  WHERE bsFlatRateData.thedate = x.thedate
    AND bsFlatRateData.storeCode = x.storeCode
    AND bsFlatRateData.employeeNumber = x.employeeNumber;    
*/	 	 
-- what the fuck, should be updating based on unique index IN bsFlatRateData,
-- at this point IN time, that IS PK only: theDate/departmentKey/techNumber
-- 4/15  what IS giving me pause, IS that IN dimTech
-- clock hours day     
  UPDATE bsFlatRateData
  SET techClockHoursDay = x.clockHours,
      techVacationHoursDay = x.vac,
      techPtoHoursDay = x.pto,
      techHolidayHoursDay = x.hol 
  FROM (       
    SELECT a.thedate, a.departmentKey, a.techNumber,
      SUM(c.clockHours) AS clockHours,
      SUM(c.vacationHours) AS VAC, SUM(c.ptohours) AS PTO, 
      SUM(c.holidayhours) AS HOL 
    FROM bsFlatRateData a    
    LEFT JOIN dds.edwEmployeeDim b on a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeeNumber
    LEFT JOIN dds.edwClockHoursFact c on a.datekey = c.datekey
      AND b.employeekey = c.employeekey
    GROUP BY a.thedate, a.departmentKey, a.techNumber) x 
  WHERE bsFlatRateData.thedate = x.thedate
    AND bsFlatRateData.departmentKey = x.departmentKey
    AND bsFlatRateData.techNumber = x.techNumber; 
		 
-- clock hours PPTD        
  UPDATE bsFlatRateData
  SET techClockHoursPPTD = x.reg
  FROM (
    SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
      a.techClockHoursDay, SUM(b.techClockHoursDay) AS reg
    FROM bsFlatRateData a, bsFlatRateData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.departmentKey = b.departmentKey
      AND a.techNumber = b.techNumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
      a.techClockHoursDay) x
  WHERE bsFlatRateData.theDate = x.theDate
    AND bsFlatRateData.departmentKey = x.departmentKey
    AND bsFlatRateData.techNumber = x.techNumber; 
    
  UPDATE bsFlatRateData
  SET techVacationHoursPPTD = x.vac
  FROM (
    SELECT  a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
      a.techVacationHoursDay, SUM(b.techVacationHoursDay) AS vac
    FROM bsFlatRateData a, bsFlatRateData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.departmentKey = b.departmentKey
      AND a.techNumber = b.techNumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
      a.techVacationHoursDay) x
  WHERE bsFlatRateData.theDate = x.theDate
    AND bsFlatRateData.departmentKey = x.departmentKey
    AND bsFlatRateData.techNumber = x.techNumber;   
    
  UPDATE bsFlatRateData
  SET techPtoHoursPPTD = x.pto
  FROM (
    SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq,  
	  a.techPtoHoursDay,  SUM(b.techPtoHoursDay) AS pto
    FROM bsFlatRateData a, bsFlatRateData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.departmentKey = b.departmentKey
      AND a.techNumber = b.techNumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
	  a.techPtoHoursDay) x
  WHERE bsFlatRateData.theDate = x.theDate
    AND bsFlatRateData.departmentKey = x.departmentKey
    AND bsFlatRateData.techNumber = x.techNumber;       
	   
  UPDATE bsFlatRateData
  SET techHolidayHoursPPTD = x.hol
  FROM (
    SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq,  
	  a.techHolidayHoursDay, SUM(b.techHolidayHoursDay) AS hol
    FROM bsFlatRateData a, bsFlatRateData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.departmentKey = b.departmentKey
      AND a.techNumber = b.techNumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
	  a.techHolidayHoursDay) x
  WHERE bsFlatRateData.theDate = x.theDate
    AND bsFlatRateData.departmentKey = x.departmentKey
    AND bsFlatRateData.techNumber = x.techNumber;        
CATCH ALL 
  RAISE bsFlatRateData(2, 'ClockHours ' + __errtext);
END TRY;     

-- flag hours day
UPDATE bsFlatRateData
SET techPdrFlagHoursDay = x.techPdrFlagHours
FROM (
  SELECT b.theDate, e.departmentKey, e.techNumber, SUM(flagHours) AS techPdrFlagHours
  FROM dds.factRepairOrder a
  INNER JOIN dds.day b on a.flagDateKey = b.dateKey
  INNER JOIN dds.dimTech c on a.techKey = c.techKey
  INNER JOIN dds.dimOpcode d on a.opcodeKey = d.opcodeKey
  INNER JOIN bsFlatRateTechs e on c.storeCode = e.storeCode
    AND c.techNumber = e.techNumber
  WHERE d.opcode = 'PDR'
  GROUP BY b.theDate, e.departmentKey, e.techNumber) x
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;     
  
UPDATE bsFlatRateData
SET techMetalFlagHoursDay = x.techMetalFlagHours
FROM (
  SELECT b.theDate, e.departmentKey, e.techNumber, SUM(flagHours) AS techMetalFlagHours
  FROM dds.factRepairOrder a
  INNER JOIN dds.day b on a.flagDateKey = b.dateKey
  INNER JOIN dds.dimTech c on a.techKey = c.techKey
  INNER JOIN dds.dimOpcode d on a.opcodeKey = d.opcodeKey
  INNER JOIN bsFlatRateTechs e on c.storeCode = e.storeCode
    AND c.techNumber = e.techNumber
  WHERE d.opcode <> 'PDR'
  GROUP BY b.theDate, e.departmentKey, e.techNumber) x
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;    

-- flag hours PPTD
UPDATE bsFlatRateData
SET techPdrFlagHoursPPTD = x.pdr
FROM (
  SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq,  
    a.techPdrFlagHoursDay, SUM(b.techPdrFlagHoursDay) AS pdr
  FROM bsFlatRateData a, bsFlatRateData b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
    a.techPdrFlagHoursDay) x
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber; 

UPDATE bsFlatRateData
SET techMetalFlagHoursPPTD = x.pdr
FROM (
  SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq,  
    a.techMetalFlagHoursDay, SUM(b.techMetalFlagHoursDay) AS pdr
  FROM bsFlatRateData a, bsFlatRateData b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
    a.techMetalFlagHoursDay) x
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;  
  
-- adjustments day
UPDATE bsFlatRateData
SET techFlagHourAdjustmentsDay = x.adjHours
FROM (
  SELECT *
  FROM (
    SELECT a.theDate, a.departmentKey, a.techNumber, SUM(ptlhrs) AS adjHours
    FROM bsFlatRateData a
    INNER JOIN dds.stgArkonaSDPXTIM b on a.theDate = b.ptdate
      AND a.techNumber = b.pttech  
    GROUP BY a.theDate, a.departmentKey, a.techNumber) c
  WHERE adjHours <> 0) x   
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber; 
  
-- adjustments PPTD
UPDATE bsFlatRateData
SET techFlagHourAdjustmentsPPTD = x.adjHours
FROM (
  SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq,  
 a.techFlagHourAdjustmentsDay, SUM(b.techFlagHourAdjustmentsDay) AS adjHours
  FROM bsFlatRateData a, bsFlatRateData b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
    a.techFlagHourAdjustmentsDay) x
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;  
  
-- proficiency day
UPDATE bsFlatRateData
SET techProficiencyDay = x.prof
FROM (
  SELECT thedate, departmentKey, techNumber, 
    CASE techClockHoursDay
      WHEN 0 THEN 0
      ELSE round(1.0 * (techPdrFlagHoursDay + 
  	  techMetalFlagHoursDay + techFlagHourAdjustmentsDay)/techClockHoursDay, 2) 
    END AS prof
  FROM bsFlatRateData) x   
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;  
  
-- proficiency pptd
UPDATE bsFlatRateData
SET techProficiencyPPTD = x.prof
FROM (
  SELECT thedate, departmentKey, techNumber, 
    CASE techClockHoursPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * (techPdrFlagHoursPPTD + 
	    techMetalFlagHoursPPTD + 
		techFlagHourAdjustmentsPPTD)/techClockHoursPPTD, 2) 
    END AS prof
  FROM bsFlatRateData) x   
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;    