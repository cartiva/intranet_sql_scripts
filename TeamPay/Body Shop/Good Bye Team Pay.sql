5/13/15
body shop IS starting a new pay plan today
DO NOT know the details, but sounds LIKE everyone IS to be paid
based on the proficiency of the entire shop
per ben c, i am no longer involved AND he does NOT want techs
to be able to log INTO vision AND see, hey i would have been paid $xxx
under team pay

i DO NOT know IF this will stick, so, for now, ALL i am doing IS removing
vision access to body shop team pay info for techs (& pdr) AND team leaders


SELECT b.username, b.appseq, b.approle
-- SELECT a.fullname, b.*
FROM employeeappauthorization b 
WHERE b.appcode = 'tp' AND appseq = 101 AND approle IN ('technician','teamlead','flatratetechnician')
-- ORDER BY a.fullname
GROUP BY b.username, b.appseq, b.approle

username	           appseq	  approle	
alindom@rydellcars.com	  101	technician
bpeterson@rydellcars.com	101	flatratetechnician
cgardner@rydellcars.com	  101	technician
crose@rydellcars.com	    101	teamlead
cwalden@rydellcars.com	  101	technician
dperry@rydellcars.com	    101	technician
jolson2@rydellcars.com	  101	technician
jrosenau@rydellcars.com   101	technician
jwalton@rydellcars.com	  101	technician
lshereck@rydellcars.com	  101	teamlead
mpeterson@rydellcars.com	101	technician
mramirez@rydellcars.com	  101	technician
pjacobson@rydellcars.com	101	teamlead
rlene@rydellcars.com	    101	technician
rmavity@rydellcars.com	  101	technician
ssevigny@rydellcars.com	  101	technician
tdriscoll@rydellcars.com	101	teamlead

SELECT * FROM applicationmetadata
WHERE appcode = 'tp'
  AND appseq = 101
  AND approle IN ('technician','teamlead','flatratetechnician')
ORDER BY approle, navseq  

DELETE 
FROM employeeAppAuthorization
WHERE appcode = 'tp'
  AND appseq = 101
  AND approle IN ('technician','teamlead','flatratetechnician');

