/*

ADD Patrick Adam to paint team effective 8/21/16

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
  AND teamname = 'paint team'
ORDER BY firstname

*/

DECLARE @eff_date date;
DECLARE @tech_key integer;
DECLARE @team_key integer;
DECLARE @thru_date date;
DECLARE @department_key integer;
DECLARE @pto_rate double;
DECLARE @census integer;
DECLARE @budget double;
DECLARE @pool_percentage double;
DECLARE @elr integer;
DECLARE @tech_team_percentage double;
@department_key = 13;
@eff_date = '08/21/2016';
@elr = 60;

BEGIN TRANSACTION;
TRY
-- paint team
-- 1 ADD tech, patrick adam to tptechs
-- tptechs
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @department_Key, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @eff_Date
  -- SELECT *
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
      AND b.currentrow = true
    WHERE a.lastname = 'adam'
      AND a.firstname = 'patrick'
      AND a.currentrow = true 
      AND a.active = 'active'; 
-- tpEmployees, ptoEmployees already exists      
-- employeeAppAuth - same AS chris walden
  INSERT INTO employeeAppAuthorization (username,appname,appseq,appcode,approle,
    functionality,appDepartmentKey)
  SELECT 'padam@rydellcars.com', appname,appseq,appcode,approle,
    functionality,appDepartmentKey
  FROM employeeAppAuthorization
  WHERE username = 'cwalden@rydellcars.com'
    AND appcode = 'tp'; 
-- tpTeamTechs  
  -- ADD patrick adam 
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'paint team');
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'adam' AND firstname = 'patrick');
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);	
-- tpTeamValues
  -- CLOSE current row for paint team
  UPDATE tpTeamValues
  SET thrudate = @eff_date - 1
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  @pool_percentage = .32;
  @census = (
    SELECT COUNT(*)
	FROM tpTeamTechs
	WHERE teamkey = @team_key
	  AND thrudate > curdate());
  -- new row  
  INSERT INTO tpTeamValues(teamkey,fromdate,census,poolpercentage,budget)
  values (@team_key, @eff_date, @census, @pool_percentage, 
    @elr * @census * @pool_percentage);	
-- tpTechValues
  -- new row for patrick
  @pto_rate = 21;
  @tech_team_percentage = .132;
  INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)	
  values (@tech_key, @eff_date, @tech_team_percentage, @pto_rate);
  -- UPDATE the rest of the paint team
  -- chris walden
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE lastname = 'walden' AND firstname = 'chris'
	  AND thrudate > curdate());
  @pto_rate = (
    SELECT previoushourlyrate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > curdate());
  @tech_team_percentage = .185; ------------------------
  UPDATE tpTechValues
  SET thrudate = @eff_date - 1
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)	
  values (@tech_key, @eff_date, @tech_team_percentage, @pto_rate);  
  -- mavrik peterson
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE lastname = 'peterson' AND firstname = 'mavrik'
	  AND thrudate > curdate());
  @pto_rate = (
    SELECT previoushourlyrate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > curdate());
  @tech_team_percentage = .175; ------------------------
  UPDATE tpTechValues
  SET thrudate = @eff_date - 1
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)	
  values (@tech_key, @eff_date, @tech_team_percentage, @pto_rate);   
  -- peter jacobson
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE lastname = 'jacobson' AND firstname = 'peter'
	  AND thrudate > curdate());
  @pto_rate = (
    SELECT previoushourlyrate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > curdate());
  @tech_team_percentage = .24; ------------------------
  UPDATE tpTechValues
  SET thrudate = @eff_date - 1
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)	
  values (@tech_key, @eff_date, @tech_team_percentage, @pto_rate);   
  -- robert mavity
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE lastname = 'mavity' AND firstname = 'robert'
	  AND thrudate > curdate());
  @pto_rate = (
    SELECT previoushourlyrate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > curdate());
  @tech_team_percentage = .17; ------------------------
  UPDATE tpTechValues
  SET thrudate = @eff_date - 1
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)	
  values (@tech_key, @eff_date, @tech_team_percentage, @pto_rate);    
--	
  DELETE FROM tpData  
  WHERE departmentkey = @department_key
    AND thedate >= @eff_date
	AND teamkey = @team_key;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  	