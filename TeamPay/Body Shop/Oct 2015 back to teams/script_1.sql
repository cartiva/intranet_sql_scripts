/*
  
  
  xfmTpData: no changes req'd
  todayXfmTpData: no changes req'd  
  GetTpTechROsByDay: no changes req'd
  xfmBsFlatRateData: change flagdate to closedate
  todayXfmBsFlatRateData: change flagdate to closedate
  GetTpPayPeriods: limit view of bs history
  
restart tasks bsFlatRateData & todayBsFlatRateData
 


DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
---- testing
--  @effDate = '06/28/2015';  
--  @newELR = 92.48;
  @departmentKey = 13;
-- production
  @effDate = '10/18/2015';  
--  @newELR = 92.84; 
--  @departmentKey = 18;     

BEGIN TRANSACTION;
TRY
 this portion IS necessary for backfilling the existing payperiod, for testing
DELETE FROM tpData WHERE thedate >= @effDate AND departmentkey = @departmentkey;
-- discont updating bsFlatRateData, deleting current data keeps the pdr section
-- FROM showing on the managers pagge
DELETE FROM bsFlatRateData WHERE thedate >= @effDate;
--DELETE FROM tpTeamValues WHERE fromDate = @effDate AND thruDate = '12/31/9999';
--UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromdate = '05/03/2015'; 

production: assuming this IS being run on Sunday, day 1 of the new pay period,
  the first day of which IS @effDate
  overnight run of tpData generated a row IN tpData for @effDate with the old
  team configs, DELETE that row, it will be repopulated at the END of this script
  

Connection: \\67.135.158.12:6363\DailyCut\scoTest\thursday\copy\sco.ADD
            scotest_bodyshop

per Ben C:
    We will be using close date and at this point there will be no team leaders.  
               
Quick Team
  Techs	 $60.00 	32.0%
  6	Budget	 $115.20   				
  Terry - Terrance Driscoll	22.00%	 $25.34 
  Matt - Matthew Ramirez 13.50%	 $15.55 
  Arnie	- Arnold Iverson 16.00%	 $18.43 
  Aaron	- Aaron Lindom 13.50%	 $15.55 
  Kristine - Kristine Swanson 12.00%	 $13.82 
  Rob	- Robert Mavity 18.50%	 $21.31 


Blue Team
  Techs	 $60.00 	35.0%
  4	Budget	 $84.00 
  		
  Chad - Chad Gardner	22.50%	 $18.90 
  Josh - Joshua Walton	23.80%	 $19.99 
  David	- David Perry 19.00%	 $15.96 
  Chris	- Chris Walden 23.00%	 $19.32 
  
Green Team
  Techs	 $60.00 	35.0%
  4	Budget	 $84.00 
  				
  Loren	- Loren Schereck 27.10%	 $22.76 
  Cory - Cory Rose 27.10%	 $22.76 
  Scott - Scott Sevigny 20.00%	 $16.80 
  Mavrik - Mavrik Peterson 20.50%	 $17.22 
            
Brown Team
  Techs	 $60.00 	35.0%
  4	Budget	 $84.00 
  			
  Ryan - Ryan Lene 27.10%	 $22.76 
  Brandon - Brandon Lamont 18.00%	 $15.12 
  Justin - Justin Olson 18.50%	 $15.54 
  Pete - Peter Jacobson	29%	 $24.36
  
PDR
  Brian Peterson
  one tech, 2 rates: metal: $20/hr, pdr: 14.40/hr  
hmm, make a pdr team, the problem IS the multiple rates 

working with a copy FROM 10/8/15
write the script AS though backdating to currrent payperiod (10/4 -> 10/17)

CLOSE team The Shop AND assoc tpTeamValues AND tpTeamTechs rows
ADD new teams
ADD new techs
assign techs to teams
team values
tech values
empAppAuthorization
*/

DECLARE @user string;
DECLARE @password string;
DECLARE @username string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
DECLARE @endtimes date;
DECLARE @budget double;
---- testing
  @effDate = '10/18/2015';  
  @departmentKey = 13;
  @endtimes = (SELECT thedate FROM dds.day WHERE datetype <> 'date');
BEGIN TRANSACTION;
TRY
/* this portion IS necessary for backfilling the existing payperiod, for testing */  
DELETE FROM tpData WHERE thedate >= @effDate AND departmentkey = @departmentkey;
-- CLOSE team The Shop, AND assoc tpTeamValues AND tpTeamTechs rows
@teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'the shop');
UPDATE tpTeams
SET thrudate = @effDate - 1
WHERE teamkey = @teamkey;
UPDATE tpTeamValues
SET thrudate = @effDate -1
WHERE teamkey = @teamkey
  AND thrudate = @endtimes;
UPDATE tpTeamTechs
SET thrudate = @effDate -1
WHERE teamkey = @teamkey
  AND thrudate = @endtimes;
-- ADD new teams
INSERT INTO tpTeams (departmentkey,teamname,fromdate)
values(13, 'Quick Team', @effdate); 
INSERT INTO tpTeams (departmentkey,teamname,fromdate)
values(13, 'Blue Team', @effdate);
INSERT INTO tpTeams (departmentkey,teamname,fromdate)
values(13, 'Green Team', @effdate);
INSERT INTO tpTeams (departmentkey,teamname,fromdate)
values(13, 'Brown Team', @effdate);
/* ADD new techs, brandon, jamie */
-- techs to teams
-- team values
-- tech values
/*
Quick Team
  Techs	 $60.00 	32.0%
  6	Budget	 $115.20   				
  Terry - Terrance Driscoll	22.00%	 $25.34 
  Matt - Matthew Ramirez 13.50%	 $15.55 
  Arnie	- Arnold Iverson 16.00%	 $18.43 
  Aaron	- Aaron Lindom 13.50%	 $15.55 
  Kristine - Kristine Swanson 12.00%	 $13.82 
  Rob	- Robert Mavity 18.50%	 $21.31 
  
-- oops, kristine NOT a team tech yet  
*/  
-- new techs
INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
  employeeNumber,fromDate) 
SELECT 13, b.techNumber, a.firstName, a.lastName, a.employeeNumber, @effDate
  FROM dds.edwEmployeeDim a
  LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
    AND a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'Swanson' AND a.firstname = 'Kristen'
    AND a.currentrow = true;    
-- teamtechs           
@teamkey = (SELECT teamkey FROM tpTeams WHERE teamname = 'Quick Team');
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Driscoll' AND firstname = 'Terrance' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);  
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Ramirez' AND firstname = 'Matthew' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);  
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Iverson' AND firstname = 'Arnold' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);  
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Lindom' AND firstname = 'Aaron' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Swanson' AND firstname = 'Kristen' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Mavity' AND firstname = 'Robert' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);
-- team values
@census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @teamkey AND thrudate = @endtimes);
@poolperc = .32;
@budget = 115.2;
INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
values(@teamkey,@census,@budget,@poolperc,@effdate);

-- tech values
-- new tech
@techkey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Swanson' AND firstname = 'Kristen' AND thrudate = @endtimes);
@techperc = .12;  
@hourlyrate = (
  SELECT a.hourlyrate
  FROM dds.edwEmployeeDim a
  WHERE a.lastname = 'Swanson' AND a.firstname = 'Kristen'
    AND a.currentrow = true); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

-- existing techs
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Driscoll' AND firstname = 'Terrance' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .22; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Ramirez' AND firstname = 'Matthew' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .135; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Iverson' AND firstname = 'Arnold' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .16; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);    

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Lindom' AND firstname = 'Aaron' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .135; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate); 

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Mavity' AND firstname = 'Robert' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .185; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

/*
Blue Team
  Techs	 $60.00 	35.0%
  4	Budget	 $84.00   		
  Chad - Chad Gardner	22.50%	 $18.90 
  Josh - Joshua Walton	23.80%	 $19.99 
  David	- David Perry 19.00%	 $15.96 
  Chris	- Chris Walden 23.00%	 $19.32 
*/  
-- new techs: none on this team
-- teamtechs           
@teamkey = (SELECT teamkey FROM tpTeams WHERE teamname = 'Blue Team');
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Gardner' AND firstname = 'Chad' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate); 
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Walton' AND firstname = 'Joshua' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Perry' AND firstname = 'David' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Walden' AND firstname = 'Chris' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);

-- team values
@census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @teamkey AND thrudate = @endtimes);
@poolperc = .35;
@budget = 84;
INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
values(@teamkey,@census,@budget,@poolperc,@effdate);

-- tech values
-- existing techs
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Gardner' AND firstname = 'Chad' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .225; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Walton' AND firstname = 'Joshua' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .238; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Perry' AND firstname = 'David' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .19; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Walden' AND firstname = 'Chris' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .23; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

/*
Green Team
  Techs	 $60.00 	35.0%
  4	Budget	 $84.00   				
  Loren	- Loren Schereck 27.10%	 $22.76 
  Cory - Cory Rose 27.10%	 $22.76 
  Scott - Scott Sevigny 20.00%	 $16.80 
  Mavrik - Mavrik Peterson 20.50%	 $17.22 
*/  
-- new techs: none on this team
-- teamtechs           
@teamkey = (SELECT teamkey FROM tpTeams WHERE teamname = 'Green Team');
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Shereck' AND firstname = 'Loren' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate); 
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Rose' AND firstname = 'Cory' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Sevigny' AND firstname = 'Scott' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Peterson' AND firstname = 'Mavrik' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);

-- team values
@census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @teamkey AND thrudate = @endtimes);
@poolperc = .35;
@budget = 84;
INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
values(@teamkey,@census,@budget,@poolperc,@effdate);

-- tech values
-- existing techs
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Shereck' AND firstname = 'Loren' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .271; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Rose' AND firstname = 'Cory' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .271; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Sevigny' AND firstname = 'Scott' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .20; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Peterson' AND firstname = 'Mavrik' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .205; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

/*
Brown Team
  Techs	 $60.00 	35.0%
  4	Budget	 $84.00   			
  Ryan - Ryan Lene 27.10%	 $22.76 
  Brandon - Brandon Lamont 18.00%	 $15.12 
  Justin - Justin Olson 18.50%	 $15.54 
  Pete - Peter Jacobson	29%	 $24.36
*/  
-- new techs
INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
  employeeNumber,fromDate) 
SELECT 13, b.techNumber, a.firstName, a.lastName, a.employeeNumber, @effDate
  FROM dds.edwEmployeeDim a
  LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
    AND a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'Lamont' AND a.firstname = 'Brandon'
    AND a.currentrow = true;  
     
-- tpemployees
@username = 'blamont@rydellcars.com';
@password = 'LaMjmv783n';  
INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
  membergroup, storecode, fullname)
SELECT @username, firstname, lastname, employeenumber, @password, 
  'Fuck You', 'RY1', TRIM(firstName) + ' ' + TRIM(lastName)
FROM dds.edwEmployeeDim 
WHERE lastname = 'Lamont' AND firstname = 'Brandon'
  AND currentrow = true 
  AND active = 'active';  
        
-- teamtechs           
@teamkey = (SELECT teamkey FROM tpTeams WHERE teamname = 'Brown Team');
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Lene' AND firstname = 'Ryan' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate); 
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Lamont' AND firstname = 'Brandon' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Olson' AND firstname = 'Justin' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Jacobson' AND firstname = 'Peter' AND thrudate = @endtimes);
INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
values(@teamkey,@techkey,@effdate);

-- team values
@census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @teamkey AND thrudate = @endtimes);
@poolperc = .35;
@budget = 84;
INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
values(@teamkey,@census,@budget,@poolperc,@effdate);    

-- tech values
-- new tech
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Lamont' AND firstname = 'Brandon' AND thrudate = @endtimes);
@techperc = .18; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT a.hourlyrate
  FROM dds.edwEmployeeDim a
  WHERE a.lastname = 'Lamont' AND a.firstname = 'Brandon'
    AND a.currentrow = true); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

-- tech values
-- existing techs
@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Lene' AND firstname = 'Ryan' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .271; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Olson' AND firstname = 'Justin' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .185; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);

@techKey = (SELECT techkey FROM tptechs 
  WHERE lastname = 'Jacobson' AND firstname = 'Peter' AND thrudate = @endtimes);
UPDATE tpTechValues
SET thrudate = @effdate - 1
WHERE techkey = @techkey
  AND thrudate = @endtimes;
@techperc = .29; --------------------------------------------------------------- 
@hourlyrate = (
  SELECT previousHourlyRate
  FROM tpTechValues
  WHERE techkey = @techkey
    AND thrudate = @effdate - 1); 
INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
values(@techkey, @effdate, @techperc, @hourlyrate);    

-- employeeAppAuthorization
-- remove technician authorization  
DELETE 
-- SELECT *
FROM employeeAppAuthorization
WHERE appname = 'teampay'
  AND appdepartmentkey = 13
  AND approle = 'technician' 
  AND username <> 'bpeterson@rydellcars.com';
  
INSERT INTO employeeappAuthorization    
SELECT m.username, n.appname, n.appseq, n.appcode, n.approle, n.functionality, 
  n.appdepartmentkey
FROM (
  SELECT username
  FROM tpemployees a
  INNER JOIN tptechs b on a.employeenumber = b.employeenumber
  INNER JOIN tpteamtechs c on b.techkey = c.techkey
    AND c.thrudate > curdate()
  INNER JOIN tpteams d on c.teamkey = d.teamkey
    AND d.departmentkey = 13
    AND d.thrudate > curdate() 
  WHERE a.username <> 'bpeterson@rydellcars.com'
    AND NOT EXISTS (
      SELECT 1
      FROM employeeappauthorization
      WHERE username = a.username
        AND appcode = 'tp'
        AND appdepartmentkey = 13)) m,  
(
  SELECT appname, appseq, appcode, approle, functionality, appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'lshereck@rydellcars.com'
    AND appcode = 'tp'
    AND approle = 'teamlead') n;    
    
-- brian peterson has to have the role of flatratetechnician for vision to
-- WORK (call the correct stored procs to display his data on his page)     
  
TRY 
-- AND FINALLY, UPDATE tpdata  
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
  EXECUTE PROCEDURE xfmDeptTechCensus();
  EXECUTE PROCEDURE xfmTmpBen();
  EXECUTE PROCEDURE xfmBsFlatRateData();
  EXECUTE PROCEDURE todayXfmBsFlatRateData();
CATCH ALL
  RAISE xfmTpData(1, __errtext);
END TRY; 
  
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;