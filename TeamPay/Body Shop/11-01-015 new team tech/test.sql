DECLARE @eff_date date;
DECLARE @tech_key integer;
DECLARE @team_key integer;
DECLARE @thru_date date;
DECLARE @department_key integer;
DECLARE @pto_rate double ;
@department_key = 13;
@eff_date = '10/18/2015';
@thru_date = '12/31/9999';
--@tech_key = 66; -- kristen
@team_key = 25; -- quick team


      
BEGIN TRANSACTION;
TRY 
-- tpTechs
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @department_Key, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @eff_Date
  -- SELECT *
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
      AND b.currentrow = true
    WHERE a.lastname = 'swanson'
      AND a.firstname = 'kristen'
      AND a.currentrow = true 
      AND a.active = 'active'
      AND a.currentrow = true; 
-- tpTeamTechs
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'swanson' AND firstname = 'kristen');      
  INSERT INTO tpTeamTechs(teamkey, techkey, fromdate)
  values(@team_key, @tech_key, @eff_date);
-- tpTeamValues
  -- CLOSE previous row
  UPDATE tpTeamValues
  SET thrudate = @eff_date - 1
  WHERE teamkey = @team_key
    AND thrudate = @thru_date;
  -- new row
  INSERT INTO tpTeamValues (teamkey, fromdate, census, poolpercentage, budget)
  values(@team_key, @eff_date, 6, .32, 115.2); 
-- techValues
  -- new row for kristen
  INSERT INTO tpTechValues (techkey, fromdate, techteampercentage, previoushourlyrate)
  values (@tech_key, @eff_date, .12, 12);
  -- UPDATE existing team members              
  @tech_key = 42; -- driscoll
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tptechvalues 
    WHERE techkey = @tech_key
      AND thrudate = @thru_date);
  UPDATE tptechvalues
  SET thrudate = @eff_date - 1
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
  INSERT INTO tpTechValues (techkey, fromdate, techteampercentage, previoushourlyrate)
  values (@tech_key, @eff_date, .22, @pto_rate);

  @tech_key = 45; -- mavity
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tptechvalues 
    WHERE techkey = @tech_key
      AND thrudate = @thru_date);
  UPDATE tptechvalues
  SET thrudate = @eff_date - 1
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
  INSERT INTO tpTechValues (techkey, fromdate, techteampercentage, previoushourlyrate)
  values (@tech_key, @eff_date, .185, @pto_rate);
    
  @tech_key = 52; -- lindom
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tptechvalues 
    WHERE techkey = @tech_key
      AND thrudate = @thru_date);
  UPDATE tptechvalues
  SET thrudate = @eff_date - 1
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
  INSERT INTO tpTechValues (techkey, fromdate, techteampercentage, previoushourlyrate)
  values (@tech_key, @eff_date, .135, @pto_rate);
    
  @tech_key = 57; -- ramirez
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tptechvalues 
    WHERE techkey = @tech_key
      AND thrudate = @thru_date);
  UPDATE tptechvalues
  SET thrudate = @eff_date - 1
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
  INSERT INTO tpTechValues (techkey, fromdate, techteampercentage, previoushourlyrate)
  values (@tech_key, @eff_date, .135, @pto_rate);
    
  @tech_key = 65; -- iverson
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tptechvalues 
    WHERE techkey = @tech_key
      AND thrudate = @thru_date);
  UPDATE tptechvalues
  SET thrudate = @eff_date - 1
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
  INSERT INTO tpTechValues (techkey, fromdate, techteampercentage, previoushourlyrate)
  values (@tech_key, @eff_date, .16, @pto_rate);       
/**/ 

  DELETE 
  FROM tpdata
  WHERE teamkey = @team_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();   
/**/
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;   
 