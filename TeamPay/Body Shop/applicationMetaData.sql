ADD appSeq to primary key of applicationMetaData
applicationMetaData
DROP RI
DROP index PK
DROP table primarykey
ADD index PK 
ADD TABLE PK

employeeAppAuthorization
DROP index pk
DROP TABLE primary key
DROP index FK1
ADD COLUMN to employeeAppAuthorization
ADD index PK
ADD TABLE primary key
ADD index FK1

ADD RI 'appMetaData-empAppAuthorization'

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'appMetaData-empAppAuthorization',
     'ApplicationMetaData', 
     'employeeAppAuthorization', 
     'FK1', 
     1, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 
-- applicationMetaData

EXECUTE PROCEDURE sp_DropReferentialIntegrity('appMetaData-empAppAuthorization');
DROP index applicationMetaData.PK;
ALTER TABLE applicationMetaData DROP constraint primary key;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'applicationMetaData','applicationMetaData.adi','PK','appName;appCode;appRole;functionality;appSeq',
   '',2051,512, '' ); 
    
EXECUTE PROCEDURE sp_ModifyTableProperty ('applicationMetaData','Table_PRIMARY_KEY',
  'PK', 'VALIDATE_RETURN_ERROR',''); 
   
-- employeeAppAuthorization
ALTER TABLE employeeAppAuthorization
ADD COLUMN appSeq integer constraint NOT NULL position 3;
DROP index employeeAppAuthorization.PK;
DROP index employeeAppAuthorization.FK1;     
ALTER TABLE applicationMetaData DROP constraint primary key; 

EXECUTE PROCEDURE sp_CreateIndex90( 
   'employeeAppAuthorization','employeeAppAuthorization.adi','PK','userName;appName;appCode;appRole;functionality;appSeq',
   '',2051,512, '' );  
EXECUTE PROCEDURE sp_ModifyTableProperty ('employeeAppAuthorization','Table_PRIMARY_KEY',
  'PK', 'VALIDATE_RETURN_ERROR','');  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'employeeAppAuthorization','employeeAppAuthorization.adi','FK1','appName;appCode;appRole;functionality;appSeq',
   '',2,512,'' );    
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'appMetaData-empAppAuthorization',
     'ApplicationMetaData', 
     'employeeAppAuthorization', 
     'FK1', 
     1, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     '');        

INSERT INTO applicationMetaData
SELECT appname, appcode, 101, approle, functionality, 
  CASE LEFT(configmapmarkupkey, 3)
    WHEN 'mai' THEN left(replace(configMapMarkupKey, 'mainlink','mainlink_bodyshop'), 50)
    WHEN 'sub' THEN left(replace(configMapMarkupKey, 'sublink','sublink_bodyshop'), 50)
  END,
  replace(pubsub, 'stp','btp'),
  navtype, navtext, navseq
FROM applicationMetaData
WHERE appCode = 'tp';   