--< stored procs ------------------------------------------------- stored procs <

ALTER PROCEDURE getApplicationNavigation
   ( 
      sessionId CICHAR ( 50 ),
      appCode CICHAR ( 6 ),
      appCode CICHAR ( 6 ) OUTPUT,
      configMapMarkupKey CICHAR ( 50 ) OUTPUT,
      navType CICHAR ( 50 ) OUTPUT,
      pubSub CICHAR ( 50 ) OUTPUT,
      applicationDept CICHAR ( 24 ) OUTPUT,
      appSeq Integer OUTPUT
   ) 
BEGIN 
/*
1/24/14
  replaced employeeAppRoles & applicationRoles with employeeAppAuthorization
2/19  
  added appseq & applicationDept: hacking to get body shop working

EXECUTE PROCEDURE getApplicationNavigation ('[d27d66d2-13ea-5547-90e1-4329ec6a863c]','ALL')

EXECUTE PROCEDURE getApplicationNavigation (
(SELECT top 1 sessionid 
  FROM sessions 
  WHERE username = 'rsattler@rydellchev.com' AND validthruts > now()), 'ALL')
*/
DECLARE @username string;
DECLARE @appCode string;
@appCode = (SELECT upper(appCode) FROM __input);
@username = (
  SELECT username
  FROM sessions
  WHERE sessionid = (SELECT sessionId FROM __input) 
    AND now() <= validThruTS);
INSERT INTO __output
  SELECT top 100 distinct a.appCode, a.configMapMarkupKey, a.navType, a.pubSub, 
    bb.dl4, a.appSeq
  FROM applicationmetadata a
  INNER JOIN employeeAppAuthorization b ON a.appname = b.appname 
    AND a.appCode = b.appCode AND a.approle = b.approle
	AND a.functionality = b.functionality
-- ADD appseq	
	AND a.appseq = b.appseq
  LEFT JOIN zdepartments bb on b.appdepartmentkey = bb.departmentkey
  INNER JOIN tpEmployees c on b.username = c.username
  WHERE c.username = @userName
    AND 
      CASE
    	  WHEN @appCode = 'ALL' THEN 1 = 1
    	  WHEN @appCode <> 'ALL' THEN a.appCode = @appCode
    	END
  ORDER BY a.appSeq, a.navSeq;    
END;

alter PROCEDURE GetTpShopTotals
   ( 
      department CICHAR ( 12 ),
      payPeriodIndicator Integer,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
2/20 bodyshop mod
  remove session, ADD department
  
EXECUTE PROCEDURE GetTpShopTotals('mainshop', -0);
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
//DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
DECLARE @department integer;
@department = 
  CASE (SELECT department FROM __input)
    WHEN 'mainshop' THEN 18
	WHEN 'bodyshop' THEN 13
  END;
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
/*  
@sessionID = (SELECT sessionID FROM __input);
IF EXISTS (
  SELECT 1 
  FROM sessions
  WHERE sessionid = @sessionid
  AND now() <= ValidThruTS)
THEN   
*/
  INSERT INTO __output
  SELECT sum(FlagHours), SUM(clockhours), 
    CASE SUM(clockhours)
	    WHEN 0 THEN 0
	    ELSE round(SUM(FlagHours)/SUM(ClockHours), 4)
	  END,
    sum(PayHours), coalesce(SUM(regularPay), 0)
  FROM (
    SELECT a.*, b.RegularPay 
    FROM (
      SELECT distinct teamname, 
        (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD 
		  + TeamFlagHourAdjustmentsPPTD) AS FlagHours,
        teamclockhourspptd AS ClockHours, teamprofpptd,
        round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours
      FROM tpData
      WHERE thedate = @date
	    AND departmentKey = @department) a
    LEFT JOIN (
    SELECT teamname, coalesce(SUM(RegularPay), 0) AS RegularPay
      FROM (
      SELECT distinct teamname, teamkey, technumber, techkey,
        round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
      FROM tpData
      WHERE thedate = @date) r
    GROUP BY teamname) b on a.teamname = b.teamname) s;
//  ENDIF;  




END;

alter PROCEDURE GetTpTeamTotals
   ( 
      department CICHAR ( 12 ),
      payPeriodIndicator Integer,
      teamName CICHAR ( 52 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
2/20 bodyshop mod
  remove session, ADD department
  
EXECUTE PROCEDURE GetTpTeamTotals('bodyshop', 0);
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @department integer;
//DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;

@department = 
  CASE (SELECT department FROM __input)
    WHEN 'mainshop' THEN 18
	WHEN 'bodyshop' THEN 13
  END;
  
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
/*  
@sessionID = (SELECT sessionID FROM __input);
IF EXISTS (
  SELECT 1 
  FROM sessions
  WHERE sessionid = @sessionid
  AND now() <= ValidThruTS)
THEN 
*/
  INSERT INTO __output
  SELECT a.*, b.RegularPay
  FROM (
    SELECT distinct teamname, 
      (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD 
	    + TeamFlagHourAdjustmentsPPTD) AS FlagHours,
      teamclockhourspptd, teamprofpptd/100,
      round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours
    FROM tpData
    WHERE thedate = @date
	  AND departmentKey = @department) a
  LEFT JOIN (
  SELECT teamname, coalesce(SUM(RegularPay), 0) AS RegularPay
    FROM (
    SELECT distinct teamname, teamkey, technumber, techkey,
      round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
    FROM tpData
    WHERE thedate = @date
	  AND departmentKey = @department) r
  GROUP BY teamname) b on a.teamname = b.teamname;
//ENDIF;  



END;


CREATE PROCEDURE getTpEmployeeTotalsForDepartment (
  department cichar(12),
  payPeriodIndicator integer,
      teamName CICHAR ( 52 ) OUTPUT,
      tech CICHAR ( 60 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT)
BEGIN 
/*
execute procedure getTPEmployeeTotalsForDepartment('bodyshop',0)
*/	
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @department integer;
@department = 
  CASE (SELECT department FROM __input)
    WHEN 'mainshop' THEN 18
	WHEN 'bodyshop' THEN 13
  END;
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
      
  INSERT INTO __output
  SELECT top 1000 *
  FROM (
    SELECT teamname, trim(firstname) + ' '  + lastname AS tech, 
      (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd 
	    + TechFlagHourAdjustmentspptd) AS "Flag Hours",
      techclockhourspptd AS "clock hours", 
      teamprofpptd/100 AS "team prof", 
	  round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
      coalesce(round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2), 0) AS "Regular Pay"
    FROM tpdata a
    WHERE thedate = @date
	  AND departmentKey = @department) x
  ORDER BY teamname, tech;
END;  

CREATE PROCEDURE getTpEmployeeTotalsForEmployee
   ( 
      username CICHAR ( 50 ),
      payPeriodIndicator Integer,
      teamName CICHAR ( 52 ) OUTPUT,
      tech CICHAR ( 60 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
execute procedure getTpEmployeeTotalsForEmployee('akempert@rydellcars.com',0)
*/	
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string;
DECLARE @techKey integer;
@username = (SELECT username FROM __input);
@techKey = (
  select a.techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
  WHERE b.username = @username);
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
      
  INSERT INTO __output
  SELECT top 1000 *
  FROM (
    SELECT teamname, trim(firstname) + ' '  + lastname AS tech, 
      (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd 
	    + TechFlagHourAdjustmentspptd) AS "Flag Hours",
      techclockhourspptd AS "clock hours", 
      teamprofpptd/100 AS "team prof", 
	  round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
      coalesce(round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2), 0) AS "Regular Pay"
    FROM tpdata a
    WHERE thedate = @date
	  AND techkey = @techKey) x
  ORDER BY teamname, tech;

END;

CREATE PROCEDURE getTpEmployeeTotalsForTeamlead
   ( 
      username CICHAR ( 50 ),
      payPeriodIndicator Integer,
      teamName CICHAR ( 52 ) OUTPUT,
      tech CICHAR ( 60 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
execute procedure getTpEmployeeTotalsForTeamlead('ngray@rydellchev.com',0)
*/	
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string;
DECLARE @teamKey integer;
@username = (SELECT username FROM __input);
@teamKey = (
  SELECT teamKey
  FROM tpTeamTechs a
  WHERE techKey = (
    SELECT a.techkey
    FROM tpTechs a
    INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    WHERE b.username = @username)
  AND curdate() BETWEEN a.fromDate AND a.thruDate);
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
      
  INSERT INTO __output
  SELECT top 1000 *
  FROM (
    SELECT teamname, trim(firstname) + ' '  + lastname AS tech, 
      (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd 
	    + TechFlagHourAdjustmentspptd) AS "Flag Hours",
      techclockhourspptd AS "clock hours", 
      teamprofpptd/100 AS "team prof", 
	  round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
      coalesce(round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2), 0) AS "Regular Pay"
    FROM tpdata a
    WHERE thedate = @date
	  AND teamkey = @teamKey) x
  ORDER BY teamname, tech;

END;

alter PROCEDURE GetTpTechTotal
   ( 
      SessionID CICHAR ( 50 ),
      payPeriodIndicator Integer,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*

INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 
  'bcahalan@rydellchev.com', timestampadd(sql_tsi_hour, 4, now()) 
FROM system.iota;

2/22 consistent rounding

EXECUTE PROCEDURE getTpTechTotalNew('[6d24944b-8eae-d24c-b646-a6d2b4afb217]', -2);
*/

DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@sessionID = (SELECT sessionID FROM __input);
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
	
INSERT INTO __output
SELECT
  (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd + 
    TechFlagHourAdjustmentspptd) AS "Flag Hours",
  techclockhourspptd AS "clock hours", 
  teamprofpptd/100 AS "team prof", 
  round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
  round(round(techtfrrate, 2)*round(techclockhourspptd*teamprofpptd/100, 2), 2) AS "Regular Pay"
FROM tpdata a
WHERE techkey = @TechKey
  AND thedate = @date;


END;

	
alter PROCEDURE GetTpPayPeriods
   ( 
      payPeriodDisplay CICHAR ( 100 ) OUTPUT,
      payPeriodIndicator Integer OUTPUT
   ) 
BEGIN 
/*
2-8-14: return 8 payperiods
2/22/14: change width of payPeriodDisplay
EXECUTE PROCEDURE GetTpPayPeriods();
*/
-- DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @fromPayPeriodSeq integer;
DECLARE @thruPayPeriodSeq integer;
@thrupayperiodseq = (
  SELECT distinct payperiodseq FROM tpData WHERE thedate = curdate());
@fromPayPeriodSeq = @thrupayperiodseq - 7;
INSERT INTO __output
SELECT top 10 payperiodselectformat, payperiodseq - @thruPayPeriodSeq
FROM (
  SELECT DISTINCT payperiodselectformat, payperiodseq
  FROM tpdata 
  WHERE payperiodseq BETWEEN  @frompayperiodseq AND @thrupayperiodseq) x
ORDER BY payperiodseq - @thruPayPeriodSeq DESC ;
END;	

ALTER PROCEDURE GetTpTechDetail
   ( 
      SessionID CICHAR ( 50 ),
      payPeriodIndicator Integer,
      date DATE OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay Money OUTPUT
   ) 
BEGIN 
/*
2/2/2/14: consistent rounding
EXECUTE PROCEDURE GetTpTechDetail(
  (SELECT top 1 sessionid FROM Sessions
    WHERE username = 'ngray@rydellchev.com'
	ORDER BY validThruTS desc)
  , -1);
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@sessionID = (SELECT sessionID FROM __input);
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
INSERT INTO __output
SELECT thedate, 
  (techflaghoursday + techOtherHoursDay + techTrainingHoursDay + 
    TechFlagHourAdjustmentsDay) AS "Flag Hours",
  techclockhoursday AS "clock hours", 
  coalesce(b.teamprofpptd/100, 0) AS "team prof", 
  coalesce(round(techclockhoursday*b.teamprofpptd/100, 2), 0) AS "Pay Hours",
  coalesce(round(round(techtfrrate, 2)
    * round(techclockhoursday*b.teamprofpptd/100, 2), 2), 0) AS "Regular Pay"
FROM tpdata a
LEFT JOIN (
  SELECT teamprofpptd
  FROM tpdata  
  WHERE techkey = @TechKey
    AND @date BETWEEN payperiodstart AND payperiodend 
    AND thedate = (
      SELECT MAX(thedate)
      FROM tpdata
      WHERE techkey = @TechKey
        AND @date BETWEEN payperiodstart AND payperiodend 
        AND teamprofpptd <> 0)) b on 1 = 1 
WHERE techkey = @TechKey
  AND @date BETWEEN payperiodstart AND payperiodend;

END;

alter PROCEDURE GetTpAdmin
   ( 
--      sessionID CICHAR ( 50 ),
      department cichar(12),
      teamName CICHAR ( 52 ) OUTPUT,
      teamBudget DOUBLE ( 15 ) OUTPUT,
      techName CICHAR ( 60 ) OUTPUT,
      techPercentage DOUBLE ( 15 ) OUTPUT,
      techRate DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*

EXECUTE PROCEDURE getTpAdmin('mainshop')
		
12/7
  IF user IS a manager return ALL teams
  IF user IS a teamleader, return just the team leaders team 
2/15
  replace EmployeeAppRoles WHERE employeeAppAuthorization    
2/24
  replace session with department  
  currently only managers have access to settings, so remove the 
    test for approle 
*/
DECLARE @date date;
DECLARE @department integer;
@date = curdate(); 
@department = 
  CASE (SELECT department FROM __input)
    WHEN 'mainshop' THEN 18
	WHEN 'bodyshop' THEN 13
  END;
INSERT INTO __output
  SELECT top 100 *
  FROM (
    SELECT a.teamname, b.budget, 
	  trim(d.firstname) + ' '  + d.lastname AS techName, 
      e.techTeamPercentage, 
	  round(b.budget * e.techTeamPercentage, 2) AS techRate
    FROM tpTeams a   
    INNER JOIN tpTeamValues b on a.teamKey = b.teamKey  
      AND @date BETWEEN b.fromdate AND b.thrudate 
    INNER JOIN tpTeamTechs c on a.teamkey = c.teamkey
      AND @date BETWEEN c.fromdate AND c.thrudate
    INNER JOIN tpTechs d on c.techkey = d.techkey    
    INNER JOIN tpTechValues e on d.techkey = e.techkey
      AND @date BETWEEN e.fromdate AND e.thrudate
    WHERE @date BETWEEN a.fromdate AND a.thrudate      
        AND a.departmentKey = @department) x
	ORDER BY teamName, techName;

END;

ALTER PROCEDURE xfmTpData
   ( 
   ) 
BEGIN 
/*
2/27/14
  v.2.2.0
    new row
      ADD bodyshop to base values for new row
	  change daterange to 01/12/2014 - curdate()
	  team census/budget: bs mod
EXECUTE PROCEDURE xfmTpData();
*/
BEGIN TRANSACTION; -- new row   
TRY
  TRY 
    -- base values for a new row
-- main Shop	
    INSERT INTO tpData(DateKey, TheDate, PayPeriodStart,PayPeriodEnd,PayPeriodSeq,
      DayOfPayPeriod, DayName, 
      DepartmentKey, TechKey, TechNumber, EmployeeNumber, FirstName, LastName)  
    SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
      a.biweeklypayperiodsequence, dayinbiweeklypayperiod, a.DayName,
      (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'Main Shop' AND dl5 = 'Shop'),
      b.TechKey, b.technumber, b.employeenumber, b.firstname, b.lastname
    FROM dds.day a, tpTechs b 
    WHERE a.thedate BETWEEN '01/12/2014' AND curdate()
      AND NOT EXISTS (
        SELECT 1
        FROM tpdata
        WHERE thedate = a.thedate
          AND techkey = b.techkey)
      AND EXISTS (-- 12/5, techs gone, but still exist IN tpTechs, don't want them IN tpData
        SELECT 1
        FROM tpTeamTechs
        WHERE techkey = b.techkey
          AND a.thedate BETWEEN fromdate AND thrudate); 
-- bs	
    INSERT INTO tpData(DateKey, TheDate, PayPeriodStart,PayPeriodEnd,PayPeriodSeq,
      DayOfPayPeriod, DayName, 
      DepartmentKey, TechKey, TechNumber, EmployeeNumber, FirstName, LastName)  
    SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
      a.biweeklypayperiodsequence, dayinbiweeklypayperiod, a.DayName,
      (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop'),
      b.TechKey, b.technumber, b.employeenumber, b.firstname, b.lastname
    FROM dds.day a, tpTechs b 
    WHERE a.thedate BETWEEN '01/12/2014' AND curdate()
	  AND b.departmentKey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop')
      AND NOT EXISTS (
        SELECT 1
        FROM tpdata
        WHERE thedate = a.thedate
          AND techkey = b.techkey)
      AND EXISTS (-- 12/5, techs gone, but still exist IN tpTechs, don't want them IN tpData
        SELECT 1
        FROM tpTeamTechs
        WHERE techkey = b.techkey
          AND a.thedate BETWEEN fromdate AND thrudate); 		  
    -- payPeriodSelectFormat for new row
    UPDATE tpdata
    SET payPeriodSelectFormat = z.ppsel
    FROM (
      SELECT payperiodseq, TRIM(pt1) + ' ' + TRIM(pt2) AS ppsel
        FROM (  
        SELECT distinct payperiodstart, payperiodend, payperiodseq, 
          (SELECT trim(monthname) + ' '
            + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
            FROM dds.day
            WHERE thedate = a.payperiodstart) AS pt1,
          (SELECT trim(monthname) + ' ' 
            + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
            + TRIM(CAST(theyear AS sql_char))
            FROM dds.day
            WHERE thedate = a.payperiodend) AS pt2
        FROM tpdata a) y) z
    WHERE tpdata.payperiodseq = z.payperiodseq;  
    -- teamname/key for new row
    UPDATE tpData
    SET TeamKey = x.teamkey,
        teamname = x.teamname
    FROM (    
      SELECT *
      FROM ( 
        SELECT thedate
        FROM tpData
        GROUP BY thedate) a
      INNER JOIN (  
        SELECT a.techkey, c.teamkey, c.teamname, b.fromdate, b.thrudate
        FROM tptechs a
        INNER JOIN tpteamtechs b on a.techkey = b.techkey
        INNER JOIN tpteams c on b.teamkey = c.teamkey) b on a.thedate BETWEEN b.fromdate AND b.thrudate) x
    WHERE tpData.TechKey = x.techkey
      AND tpData.theDate = x.theDate; 
	         
    -- team census, budget for new row
    /* bs mod	
        UPDATE tpData 
        SET TeamCensus = x.census,
            teamBudget = x.Budget
        FROM ( 
          SELECT *
          FROM tpTeamValues) x
        WHERE tpData.payPeriodStart = x.fromDate
          AND tpData.teamkey = x.teamkey;  
    */	             
    UPDATE tpData
      SET teamCensus = x.census,
          teamBudget = x.budget
      FROM tpTeamValues x
      WHERE (tpData.teamCensus IS NULL OR tpData.teamBudget IS NULL) 
        AND tpData.teamKey = x.teamKey
        AND tpData.theDate BETWEEN x.fromDate AND x.thruDate; 
		  
    -- techteamPercentage: FROM tpTechValues for new row
    UPDATE tpData
    SET TechTeamPercentage = x.TechTeamPercentage
    FROM (
      SELECT a.thedate, b.techkey, b.techTeamPercentage
      FROM tpData a
      INNER JOIN tpTechValues b on a.techKey = b.techKey
        AND a.thedate BETWEEN b.fromdate AND b.thrudate) x
    WHERE tpData.TechKey = x.TechKey
      AND tpData.thedate = x.theDate;    
    -- TechTFRRate for new row 
    UPDATE tpdata
    SET TechTFRRate = techteampercentage * teambudget;    
    -- TechHourlyRate for new row
    UPDATE tpdata
    SET techhourlyrate = (
      SELECT distinct previoushourlyrate
      FROM tptechvalues
      WHERE techkey = tpdata.techkey);             
  CATCH ALL 
    RAISE tpdata(1, 'New Row ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- techs
TRY
  TRY   
    -- ClockHoursDay    
    UPDATE tpdata
    SET TechClockhoursDay = x.clockhours,
        TechVacationHoursDay = x.Vacation,
        TechPTOHoursDay = x.PTO,
        TechHolidayHoursDay = x.Holiday
    FROM (    
      SELECT b.thedate, c.employeenumber, SUM(clockhours) AS clockhours,
        SUM(coalesce(VacationHours,0)) AS vacation,
        SUM(coalesce(PTOHours,0)) AS pto,
        SUM(coalesce(HolidayHours,0)) AS holiday
      FROM dds.edwClockHoursFact a
      INNER JOIN dds.day b on a.datekey = b.datekey
        AND b.thedate BETWEEN '09/08/2013' AND curdate()
      LEFT JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
      GROUP BY b.thedate, c.employeenumber) x 
    WHERE tpdata.thedate = x.thedate
      AND tpdata.employeenumber = x.employeenumber;
    -- ClockHoursPPTD
    UPDATE tpData
    SET TechClockHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday, SUM(b.techclockhoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey; 
    -- TechVacationPPTD  
    UPDATE tpData
    SET TechVacationHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechVacationHoursDay, SUM(b.TechVacationHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechVacationHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;   
    -- TechPTOHoursPPTD
    UPDATE tpData
    SET TechPTOHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechPTOHoursDay, SUM(b.TechPTOHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechPTOHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;  
    -- TechHolidayHoursPPTD
    UPDATE tpData
    SET TechHolidayHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechHolidayHoursDay, SUM(b.TechHolidayHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechHolidayHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;      
-- flag hours  
    -- TechFlagHoursDay 
    UPDATE tpData  
    SET TechFlagHoursDay = x.flaghours
    FROM (
      SELECT a.technumber, a.thedate, round(sum(coalesce(b.flaghours, 0)), 2) AS flaghours
      FROM tpData a
      LEFT JOIN (
        SELECT d.thedate, ro, line, c.technumber, a.flaghours
        FROM dds.factRepairOrder a  
        INNER JOIN dds.dimTech c on a.techkey = c.techkey
        INNER JOIN dds.day d on a.flagdatekey = d.datekey
        WHERE flaghours <> 0
          AND a.storecode = 'ry1'
          AND a.flagdatekey IN (
            SELECT datekey
            FROM dds.day
            WHERE thedate BETWEEN '09/08/2013' AND curdate())   
          AND c.technumber IN (
            SELECT technumber
            FROM tpTechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
      GROUP BY a.technumber, a.thedate) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.technumber = x.technumber;       
    -- TechFlagHoursPPTD  
    UPDATE tpData
    SET TechFlagHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday, SUM(b.techflaghoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;   
-- adjustments
    UPDATE tpData
    SET TechFlagHourAdjustmentsDay = x.Adj
    FROM (
      SELECT a.technumber, a.lastname, a.thedate, coalesce(Adj, 0) AS Adj
      FROM tpData a
      LEFT JOIN (  
        select pttech, ptdate, round(SUM(ptlhrs), 2) AS adj
        FROM dds.stgArkonaSDPXTIM a
        INNER JOIN tpTechs b on a.pttech = b.technumber
        GROUP BY pttech, ptdate) b on a.technumber = b.pttech AND a.thedate = b.ptdate) x    
    WHERE tpData.technumber = x.technumber
      AND tpData.thedate = x.thedate;
    -- tech flag time adjustments pptd 
    UPDATE tpData
    SET TechFlagHourAdjustmentsPPTD = x.pptd
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechFlagHourAdjustmentsDay, SUM(b.TechFlagHourAdjustmentsDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechFlagHourAdjustmentsDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;       
  CATCH ALL 
    RAISE tpdata(2, 'Techs ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- teams
TRY
  TRY  
    -- teamclockhoursDay
    UPDATE tpData 
    SET teamclockhoursday = x.teamclockhoursday
    FROM (
      SELECT thedate, teamkey, SUM(techclockhoursday) AS teamclockhoursday
      FROM tpdata
      GROUP BY thedate, teamkey) x 
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;  
    -- teamclockhours pptd
    UPDATE tpData
    SET teamclockhourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;       
    -- teamflaghoursday
    UPDATE tpData
    SET TeamFlagHoursDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(techflaghoursday) AS hours
      FROM tpdata 
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;
    -- teamflaghour pptd  
    UPDATE tpData
    SET teamflaghourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;  
    -- TeamFlagHourAdjustmentsDay
    UPDATE tpData
    SET TeamFlagHourAdjustmentsDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(TechFlagHourAdjustmentsDay) AS hours
      FROM tpdata 
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey; 
    -- TeamFlagHourAdjustmentsPPTD 
    UPDATE tpData
    SET TeamFlagHourAdjustmentsPPTD = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x 
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;   
    -- team prof day          
    UPDATE tpdata
    SET teamprofday = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhoursday = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhoursday, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay) AS TotalFlag, 
          teamclockhoursday
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay), 
          teamclockhoursday) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;      
    -- team prof pptd
    UPDATE tpdata
    SET teamprofpptd = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhourspptd = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhourspptd, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS TotalFlag, 
          teamclockhourspptd
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD), 
          teamclockhourspptd) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;        
  CATCH ALL 
    RAISE tpdata(3, 'Teams ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  


END;

--/> stored procs ------------------------------------------------ stored procs />


--< tables ------------------------------------------------------------- tables <

UPDATE zDepartments
  SET dl4 = lcase(replace(dl4,' ',''))
WHERE dl4 <> 'N/A';  	
  
ALTER TABLE tpData
ALTER COLUMN payPeriodSelectFormat payPeriodSelectFormat cichar(100);

--/> tables ------------------------------------------------------------ tables />

 
EXECUTE PROCEDURE sp_RenameDDObject('getTpEmployeeTotals','zUnused_getTpEmployeeTotals', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject('getApplicationAuthorization','zUnused_getApplicationAuthorization', 10, 0);   
