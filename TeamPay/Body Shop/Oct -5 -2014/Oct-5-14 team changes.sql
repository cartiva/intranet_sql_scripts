/*
to take effect on 10/5/14
move matt ramirez FROM team terry to Team Cory
Cory Rose to get .25/hr RAISE

remove matt FROM team terry
Team Terry: 
  tpTeamValues: 
    census: 4 -> 3
    poolPercentage: .315 -> .289
    budget: 75.60 -> 52.02
Team Cory
  tpTeamValues
    census: 3 -> 4
    poolPercentage: .34 -> .317
    budget: 61.20 -> 76.08
    
Team Terry  
  tpTechValues
    Tech      Prev%       New% 
    Driscoll   .308       .448  
    Lindom     .189       .275   
    Beniot     .189       .275    

Team Cory  
  tpTechValues
    Tech      Prev%       New% 
    Cory       .395       .323  
    Scott      .311       .253   
    Dave       .277       .223     
    Matt       .195       .189

*/

DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @elr double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPercentage double;

@effDate = '10/05/2014';
@departmentKey = 13;
@elr = 60;

BEGIN TRANSACTION;
TRY 
  DELETE 
  FROM tpData
  WHERE theDate = @effDate
    AND departmentKey = @departmentKey;
-- tpTeamTechs
  -- remove matt FROM team terry
  @techKey = (
    SELECT techKey
    FROM tpTechs
    WHERE lastname = 'ramirez'); 
  @teamKey = (
    SELECT teamKey
    FROM tpTeams
    WHERE teamName = 'team terry');  
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND techKey = @techKey;
  -- ADD matt to team cory
  @teamKey = (
    SELECT teamKey
    FROM tpTeams
    WHERE teamName = 'team cory'); 
  INSERT INTO tpTeamTechs (teamKey, techKey, fromDate, thruDate)
  values (@teamKey, @techKey, @effDate, '12/31/9999');
-- tpTeamValues
  -- team terry
  @teamKey = (
    SELECT teamKey
    FROM tpTeams
    WHERE teamName = 'team terry'); 
  @census = (
    SELECT COUNT(*)
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());
  @poolPercentage = .289; -----------------------------------------------------
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate = '12/31/9999';
  INSERT INTO tpTeamValues (teamKey,census, budget, poolpercentage, fromdate)
  values (@teamKey, @census, round(@census*@poolPercentage*@elr, 2), 
    @poolPercentage, @effDate); 
-- tpTeamValues
  -- team cory
  @teamKey = (
    SELECT teamKey
    FROM tpTeams
    WHERE teamName = 'team cory'); 
  @census = (
    SELECT COUNT(*)
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());
  @poolPercentage = .317; -----------------------------------------------------
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate = '12/31/9999';  
  INSERT INTO tpTeamValues (teamKey,census, budget, poolpercentage, fromdate)
  values (@teamKey, @census, round(@census*@poolPercentage*@elr, 2), 
    @poolPercentage, @effDate);     
-- tpTechValues
  -- team terry: driscoll
  @techPerc = 0.448; /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'driscoll' AND thrudate  = '12/31/9999');
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techKey = @techKey
      AND thruDate = '12/31/9999');
  -- CLOSE current row
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey
    AND thruDate = '12/31/9999';
  -- new row
  INSERT INTO tpTechValues (techKey,fromDate,thruDate,techTeamPercentage,
    previousHourlyRate)
  values(@techKEy, @effDate, '12/31/9999', @techPerc, @hourlyRate);  
  
  -- team terry: lindom
  @techPerc = 0.275; /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'lindom' AND thrudate  = '12/31/9999');
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techKey = @techKey
      AND thruDate = '12/31/9999');
  -- CLOSE current row
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey
    AND thruDate = '12/31/9999';
  -- new row
  INSERT INTO tpTechValues (techKey,fromDate,thruDate,techTeamPercentage,
    previousHourlyRate)
  values(@techKEy, @effDate, '12/31/9999', @techPerc, @hourlyRate);       
  
  -- team terry: beniot
  @techPerc = 0.275; /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'beniot' AND thrudate  = '12/31/9999');
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techKey = @techKey
      AND thruDate = '12/31/9999');
  -- CLOSE current row
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey
    AND thruDate = '12/31/9999';
  -- new row
  INSERT INTO tpTechValues (techKey,fromDate,thruDate,techTeamPercentage,
    previousHourlyRate)
  values(@techKEy, @effDate, '12/31/9999', @techPerc, @hourlyRate);      
    
-- tpTechValues
  -- team cory: rose
  @techPerc = 0.323; /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'rose' AND thrudate  = '12/31/9999');
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techKey = @techKey
      AND thruDate = '12/31/9999');
  -- CLOSE current row
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey
    AND thruDate = '12/31/9999';
  -- new row
  INSERT INTO tpTechValues (techKey,fromDate,thruDate,techTeamPercentage,
    previousHourlyRate)
  values(@techKEy, @effDate, '12/31/9999', @techPerc, @hourlyRate);      
  
  -- team cory: sevigny
  @techPerc = 0.253; /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'sevigny' AND thrudate  = '12/31/9999');
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techKey = @techKey
      AND thruDate = '12/31/9999');
  -- CLOSE current row
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey
    AND thruDate = '12/31/9999';
  -- new row
  INSERT INTO tpTechValues (techKey,fromDate,thruDate,techTeamPercentage,
    previousHourlyRate)
  values(@techKEy, @effDate, '12/31/9999', @techPerc, @hourlyRate);   
  
  -- team cory: perry
  @techPerc = 0.223; /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'perry' AND thrudate  = '12/31/9999');
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techKey = @techKey
      AND thruDate = '12/31/9999');
  -- CLOSE current row
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey
    AND thruDate = '12/31/9999';
  -- new row
  INSERT INTO tpTechValues (techKey,fromDate,thruDate,techTeamPercentage,
    previousHourlyRate)
  values(@techKEy, @effDate, '12/31/9999', @techPerc, @hourlyRate);      
  
  -- team cory: ramirez
  @techPerc = 0.1893; /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'ramirez' AND thrudate  = '12/31/9999');
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techKey = @techKey
      AND thruDate = '12/31/9999');
  -- CLOSE current row
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey
    AND thruDate = '12/31/9999';
  -- new row
  INSERT INTO tpTechValues (techKey,fromDate,thruDate,techTeamPercentage,
    previousHourlyRate)
  values(@techKEy, @effDate, '12/31/9999', @techPerc, @hourlyRate);    
      
-- tpData  
  EXECUTE PROCEDURE xfmTpData();
   
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;       