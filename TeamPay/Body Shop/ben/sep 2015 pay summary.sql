/*
Jon,
Wondering if you can run some data for me.  I need to know for the body shop from 
5/3/15 - 9/19/15 how much Commission Pay was paid to each tech and how many 
clock hours that tech had, if the tech was not here for that entire date range 
I need to know there hire date.  So just 4 columns, tech name, total commission 
pay, total clock hours, and hire date.  I am not looking for Holiday or PTO time or pay.
 
Thanks!
 
Ben
*/

SELECT a.firstname AS first_name, a.lastname AS last_name, teamname AS team, 
  payperiodstart AS pay_period_start, payperiodend AS pay_period_end, 
  techclockhourspptd AS clock_hours, round(techtfrrate, 2) AS tech_rate, 
  teamprofpptd AS team_prof,
  round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS pay, 
  b.hiredate AS hire_date
FROM tpdata a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE thedate BETWEEN '05/03/2015' AND '09/19/2015'
  AND a.departmentkey = 13
  AND thedate = payperiodend
ORDER BY a.firstname, payperiodstart



SELECT firstname AS first_name, lastname AS last_name, SUM(pay) AS pay, 
  SUM(techclockhourspptd) AS clock_hours, hiredate AS hire_date
FROM (
SELECT a.firstname, a.lastname, teamname, payperiodstart, payperiodend, techclockhourspptd, round(techtfrrate, 2) AS techtfrrate, teamprofpptd,
  round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS pay, b.hiredate
FROM tpdata a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE thedate BETWEEN '05/03/2015' AND '09/19/2015'
  AND a.departmentkey = 13
  AND thedate = payperiodend) x
GROUP BY firstname, lastname, hiredate  
  