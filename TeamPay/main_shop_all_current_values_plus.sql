
select a.teamkey, a.teamname, b.census, b.budget, b.poolpercentage, 
  c.effectivelaborrate, e.techkey, e.technumber, 
  left(trim(e.lastname) + ' ' + e.firstname, 25) AS tech_name, 
  f.techteampercentage, 
  g.techtfrrate,
  census * 91.46 * poolpercentage * f.techteampercentage AS new_tech_rate,
  round((census * 91.46 * poolpercentage * f.techteampercentage) - g.techtfrrate, 2) AS diff,
  census * 91.46 * poolpercentage AS new_budget
FROM tpteams a
INNER JOIN tpTeamValues b on a.teamkey  = b.teamkey
  AND b.thrudate > curdatE()
INNER JOIN tpshopvalues c on 1 = 1 
  AND c.departmentkey = 18
  AND c.thrudate > curdate()  
INNER JOIN tpteamtechs d on a.teamkey = d.teamkey
  AND d.thrudate > curdate()
INNER JOIN tptechs e on d.techkey = e.techkey   
INNER JOIN tptechValues f on e.techkey = f.techkey
  AND f.thrudate > curdate() 
LEFT JOIN tpdata g on f.techkey = g.techkey
  AND g.thedate = curdate()  
WHERE a.departmentkey = 18
  AND a.thrudate > curdatE()
