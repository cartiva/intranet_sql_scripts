UPDATE tpData
SET TechFlagHoursDay = x.flaghours


SELECT technumber, thedate, SUM(flaghours) AS flaghours
INTO #old
FROM (
  SELECT a.technumber, a.thedate, coalesce(b.flaghours,0) AS flaghours
  FROM tpdata a
  LEFT JOIN (
    SELECT gtdate, technumber, round(SUM(flaghours), 2) AS flaghours
    FROM (  
      SELECT DISTINCT gtdate, b.ro, b.description, technumber, b.paymenttypekey, b.flaghours
      FROM #glRO a
      LEFT JOIN #roTech b on a.ro = b.ro
        AND a.paymenttypekey = b.paymenttypekey) a 
  GROUP BY gtdate, technumber) b on a.technumber = b.technumber AND a.thedate = b.gtdate) x
WHERE flaghours <> 0  
GROUP BY technumber, thedate
  
  
DROP TABLE #new;  
SELECT d.thedate, c.technumber, round(sum(a.flaghours * b.weightfactor), 2) AS flaghours
INTO #new
FROM dds.factRepairOrder a  
INNER JOIN dds.brTechGroup b on a.techgroupkey = b.techgroupkey
INNER JOIN dds.dimTech c on b.techkey = c.techkey
INNER JOIN dds.day d on a.flagdatekey = d.datekey
WHERE flaghours <> 0
  AND a.storecode = 'ry1'
  AND a.flagdatekey IN (
    SELECT datekey
    FROM dds.day
    WHERE thedate BETWEEN '09/08/2013' AND curdate())   
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('AM','EM','MR'))   
  AND c.technumber IN (
    SELECT technumber
    FROM tpTechs)     
GROUP BY d.thedate, c.technumber     
  

SELECT *
FROM #old a
full OUTER JOIN #new b on a.technumber = b.technumber AND a.thedate = b.thedate AND a.flaghours = b.flaghours

SELECT *
FROM (
  SELECT month(thedate) AS themonth, technumber, SUM(flaghours)
  FROM #old
  GROUP BY month(thedate), technumber) a
LEFT JOIN (
  SELECT month(thedate) AS themonth, technumber, SUM(flaghours)
  FROM #new
  GROUP BY month(thedate), technumber) b on a.themonth = b.themonth AND a.technumber = b.technumber

SELECT *
FROM #new
WHERE technumber = '599'


SELECT a.technumber, a.thedate, sum(coalesce(b.flaghours, 0)) AS flaghours
FROM tpData a
LEFT JOIN (
  SELECT d.thedate, ro, line, c.technumber, a.flaghours, b.weightfactor
  FROM dds.factRepairOrder a  
  INNER JOIN dds.brTechGroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dds.dimTech c on b.techkey = c.techkey
  INNER JOIN dds.day d on a.flagdatekey = d.datekey
  WHERE flaghours <> 0
    AND a.storecode = 'ry1'
    AND a.flagdatekey IN (
      SELECT datekey
      FROM dds.day
      WHERE thedate BETWEEN '09/08/2013' AND curdate())   
    AND a.servicetypekey IN (
      SELECT servicetypekey
      FROM dds.dimservicetype
      WHERE servicetypecode IN ('AM','EM','MR'))   
    AND c.technumber IN (
      SELECT technumber
      FROM tpTechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
GROUP BY a.technumber, a.thedate   



-- TechFlagHoursDay
UPDATE tpData
SET TechFlagHoursDay = x.flaghours
FROM (
  SELECT a.technumber, a.thedate, coalesce(b.flaghours,0) AS flaghours
  FROM tpdata a
  LEFT JOIN (
    SELECT gtdate, technumber, round(SUM(flaghours), 2) AS flaghours
    FROM (  
      SELECT DISTINCT gtdate, b.ro, b.description, technumber, b.paymenttypekey, b.flaghours
      FROM #glRO a
      LEFT JOIN #roTech b on a.ro = b.ro
        AND a.paymenttypekey = b.paymenttypekey) a 
  GROUP BY gtdate, technumber) b on a.technumber = b.technumber AND a.thedate = b.gtdate) x
WHERE tpdata.thedate = x.thedate
  AND tpdata.technumber = x.technumber; 