/*
ADD tim o'neil to girodat team
to take effect on 10/5/14
incorporate the elr UPDATE at the same time

NEW ELR: tpShopValues
ADD oneil to girodat team: tpEmployees
ADD oneil to girodat team: employeeAppAuthorization
ADD oneil to girodat team: tpTechs
ADD oneil to girodat team: tpTeamTechs
BOTH: tpTeamValues
ADD oneil to girodat team: tpTechValues
Tech      Prev%       New%   prev$     New$
Girodat   .365       .292    28.84     28.83
Winfield  .222       .178    17.54     17.58
McVeigh   .217       .174    17.14     17.18
Stinar    .180       .144    14.22     14.22
ONeil                .212              20.93                                       

tpData        
*/
                    


DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
DECLARE @employeeNumber string;
DECLARE @curPPSeq integer;
DECLARE @prevPPSeq integer;
DECLARE @curFromDate date;
DECLARE @prevFromDate date;
DECLARE @prevThruDate date;
@curPPSeq = (
  SELECT biWeeklyPayPeriodSequence
  FROM dds.day
  WHERE thedate = curdate());
@curFromDate = (
  SELECT min(thedate)
  FROM dds.day  
  WHERE biWeeklyPayPeriodSequence = @curPPSeq);
@prevPPSeq = @curPPSeq - 1;  
@prevFromDate = (
  SELECT min(thedate)
  FROM dds.day  
  WHERE biWeeklyPayPeriodSequence = @prevPPSeq);   
@prevThruDate = (
  SELECT max(thedate)
  FROM dds.day  
  WHERE biWeeklyPayPeriodSequence = @prevPPSeq);  
  
SELECT @curPPSeq,@prevPPSeq,@curFromDate,@prevFromDate,@prevThruDate FROM system.iota;  

@effDate = '10/05/2014';
@departmentKey = 18;
@newELR = 88.94;
/* 
assumes this IS being run on 10/5, 
disable todayFactRepairOrder task
overnight run of tpData generated a row IN tpData for @effDate with the old
team configs, DELETE that row, it will be repopulated at the END of this script
*/
BEGIN TRANSACTION;
TRY 
  DELETE 
  FROM tpData
  WHERE theDate = @effDate
    AND departmentKey = @departmentKey;
  
-- tpShopValues: new ELR: enter new value IN header of maintenance.sql
  -- CLOSE previous pay period
  UPDATE tpShopValues
  SET thruDate = @effDate - 1
  WHERE thruDate > curdate()
    AND departmentKey = @departmentKey;      
  -- INSERT row for new pay period
  INSERT INTO tpShopValues (departmentkey, fromdate, effectivelaborrate)
  values(@departmentKey, @effDate, @newELR);  
   
-- tpEmployees ADD oneil
  @user = 'toneil@rydellcars.com';
  @password = 'wT3jKvb92T';  
  @employeeNumber = '1106421';
  INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
    membergroup, storecode, fullname)
  SELECT @user, firstname, lastname, employeenumber, @password, 
    'Fuck You', 'RY1', TRIM(firstName) + ' ' + TRIM(lastName)
  FROM dds.edwEmployeeDim 
  WHERE employeeNumber = @employeeNumber
    AND currentrow = true;
    
-- employeeAppAuthorization ADD oneil
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 100
    AND b.approle = 'technician'
    AND a.username = 'toneil@rydellcars.com'; 	    
    
-- tpTechs ADD oneil
  insert into tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT 18, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @effDate
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
    WHERE a.employeenumber = @employeeNumber
      AND a.currentrow = true;      
      
-- tpTeamTechs: ADD oneil to girodat
  @teamKey = (
    SELECT teamKey 
    FROM tpTeams 
    WHERE teamName = 'girodat');
  @techKey = ( -- this IS suspect, why the fuck am i USING employeenumber
    SELECT techKey
    FROM tpTechs  
    WHERE employeeNumber = @employeeNumber); 
  INSERT INTO tpTeamTechs (teamKey, techKey, fromDate, thruDate)
  values(@teamKey, @techKey, @effDate, '12/31/9999');
   
-- tpTeamValues ALL main shop (due to new elr) CLOSE currentrow, ADD new row (ala maintenance script)
  TRY 
    UPDATE tpTeamValues
    SET thruDate = @prevThruDate
    WHERE fromDate = @prevFromDate
      AND teamKey IN (
        SELECT teamKey 
        FROM tpTeams
        WHERE departmentKey = @departmentKey);        
    INSERT INTO tpTeamValues (teamKey, Census, Budget, poolPercentage, fromDate)       
    SELECT a.teamKey, b.census, round(b.census*a.poolPercentage * @newELR, 2) as budget, 
      a.poolPercentage, @curFromDate
    FROM tpTeamValues a
    LEFT JOIN (
      SELECT teamkey, COUNT(*) AS census
      FROM tpTeamTechs 
      WHERE thruDate = '12/31/9999'
      GROUP BY teamkey) b on a.teamKey = b.teamKey	  
    LEFT JOIN (
      SELECT effectiveLaborRate AS ELR
      FROM tpShopValues
      WHERE fromDate = @curFromDate) c on 1 = 1  
    WHERE fromDate = @prevFromDate
      AND a.teamKey IN (
        SELECT teamKey
        FROM tpTeams
        WHERE departmentKey = @departmentKey);
  CATCH ALL 
    RAISE admin(2, 'tpTeamValues ' + __errtext);
  END TRY;
  
-- tpTechValues:  ALL techs on Girodat
  -- CLOSE current rows
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey IN (
    SELECT techKey
    FROM tpTeamTechs
    WHERE teamKey = @teamKey
      AND thruDate > curdate())
    AND thruDate > curdate();
-- would LIKE a general UPDATE the whole team query, but get stuck on 
-- articulating the tech/percentage combination    
  -- INSERT new rows
  -- girodat
  @techPerc = .292; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'girodat' AND thrudate > curdate());  
-- within the TRANSACTION, the current row in tpTechValues, thethruDate 
-- has been updated to @effDate - 1 AND can be referenced IN that state    
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate = @effDate - 1);    
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  -- winfield
  @techPerc = .178; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'winfield' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate = @effDate - 1);     
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate); 
  -- mcveigh
  @techPerc = .174; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'mcveigh' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate = @effDate - 1);     
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  -- stinar
  @techPerc = .144; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'stinar' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate = @effDate - 1);   
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  -- oneil
  @techPerc = .212; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'o''neil' AND thrudate > curdate());  
  @hourlyRate = 21.23;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);       
-- tpData  
  EXECUTE PROCEDURE xfmTpData();
   
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;     