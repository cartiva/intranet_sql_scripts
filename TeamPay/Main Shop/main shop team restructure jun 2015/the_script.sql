/*
-- current reality
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
ORDER BY teamname, firstname;

lots of changes:
4 teams get a new tech each of which was previously hourly
1 team loses a tech, termed

shit, need to include the new ELR for ALL teams, NOT just those with new techs
need to UPDATE tpTeamValues for Olson AND Gray
*/
DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
---- testing
--  @effDate = '05/17/2015';  
--  @newELR = 92.48;
--  @departmentKey = 18;
-- production
  @effDate = '05/31/2015';  
  @newELR = 92.84; 
  @departmentKey = 18;     

BEGIN TRANSACTION;
TRY
/* this portion IS necessary for backfilling the existing payperiod, for testing */
--DELETE FROM tpData WHERE thedate > @effDate;
--DELETE FROM tpShopValues WHERE fromDate = @effDate AND departmentKey = @departmentKey;
--UPDATE tpShopValues SET thruDate = '12/31/9999' WHERE thruDate = @effDate AND departmentKey = @departmentKey;
--DELETE FROM tpTeamValues WHERE fromDate = @effDate AND thruDate = '12/31/9999';
--UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromdate = '05/03/2015'; 
/**/
/*
production: assuming this IS being run on Sunday, day 1 of the new pay period,
  the first day of which IS @effDate
  overnight run of tpData generated a row IN tpData for @effDate with the old
  team configs, DELETE that row, it will be repopulated at the END of this script
*/  
--< production -----------------------------------------------------------------<
  DELETE 
  FROM tpData
  WHERE theDate = @effDate
    AND departmentKey = @departmentKey;

-- 1. tpShopValues: new ELR  
    -- CLOSE previous pay period
  UPDATE tpShopValues
  SET thruDate = @effDate - 1
  WHERE thruDate > curdate()
    AND departmentKey = @departmentKey;      
  -- INSERT row for new pay period
  INSERT INTO tpShopValues (departmentkey, fromdate, effectivelaborrate)
  values(@departmentKey, @effDate, @newELR);    
  
--/> production ----------------------------------------------------------------/>

/* approach on a team BY team basis, teams w/no change: Olson, Gray*/
--< Waldbauer -----------------------------------------------------------------<
/* ADD coty perez */
-- first glitch, he IS IN vision AS cperez@gfhonda.com, need to change
--  tpemployees AND ptoemployees to cperez@rydellcars.com
@teamKey = (SELECT teamkey FROM tpteams WHERE teamname = 'waldbauer');
TRY 
-- tpTechs
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @departmentKey, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @effDate
  -- SELECT *
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
    WHERE a.lastname = 'perez'
      AND a.firstname = 'coty'
      AND a.currentrow = true 
      AND a.active = 'active'
      AND b.currentrow = true;  
      
  UPDATE tpEmployees
  SET username = 'cperez@rydellcars.com'
  -- SELECT * FROM tpemployees
  WHERE employeenumber = '1109827';    
  UPDATE ptoEmployees
  SET username = 'cperez@rydellcars.com'
  -- SELECT * FROM ptoemployees
  WHERE employeenumber = '1109827';   
    
-- employeeAppAuthorization
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  -- SELECT *
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 100
    AND b.approle = 'technician'
    AND a.username = 'cperez@rydellcars.com';         
-- tpTeamTechs   
  @techKey = (SELECT techKey FROM tpTechs WHERE employeenumber = '1109827'); 
  @hourlyRate = 15; ----------------------------------------------------------*
  INSERT INTO tpTeamTechs (teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate); 
-- tpTeamValues
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .19; -----------------------------------------------------------*   
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));     
-- tpTechValues ** ALL techs percentage change 
  -- perez
  @techPerc = .2134;
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- waldbauer
  @techkey = (select techkey FROM tptechs WHERE lastname = 'waldbauer');
  @techPerc = .2706; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  -- ortiz
  @techkey = (select techkey FROM tptechs WHERE lastname = 'ortiz');
  @techPerc = .2594; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- kiefer
  @techkey = (select techkey FROM tptechs WHERE lastname = 'kiefer');
  @techPerc = .251; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);          
CATCH ALL 
  RAISE waldbauer(3, __errtext);
END TRY;  
--/> Waldbauer ----------------------------------------------------------------/>

--< Longoria ------------------------------------------------------------------<
/* ADD charles zacha */
@teamKey = (SELECT teamkey FROM tpteams WHERE teamname = 'longoria');
TRY 
-- tpTechs
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @departmentKey, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @effDate
  -- SELECT *
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
    WHERE a.lastname = 'zacha'
      AND a.firstname = 'charles'
      AND a.currentrow = true 
      AND a.active = 'active'
      AND b.currentrow = true; 
-- employeeAppAuthorization
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  -- SELECT *
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 100
    AND b.approle = 'technician'
    AND a.username = 'czacha@rydellcars.com';       
-- tpTeamTechs   
  @techKey = (SELECT techKey FROM tpTechs WHERE employeenumber = '1150920'); 
  @hourlyRate = 17.5; ----------------------------------------------------------*
  INSERT INTO tpTeamTechs (teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate); 
-- tpTeamValues
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .185; -----------------------------------------------------------*   
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));   
-- tpTechValues ** ALL techs percentage change 
  -- zacha
  @techPerc = .526;
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- syverson
  @techkey = (select techkey FROM tptechs WHERE lastname = 'syverson');
  @techPerc = .4703; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);         
CATCH ALL 
  RAISE longoria(3, __errtext);
END TRY;     
--/> Longoria -----------------------------------------------------------------/>

--< Bear ----------------------------------------------------------------------<
/* ADD ihsan inad */
@teamKey = (SELECT teamkey FROM tpteams WHERE teamname = 'bear');
TRY 
-- tpTechs
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @departmentKey, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @effDate
  -- SELECT *
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
    WHERE a.lastname = 'inad'
      AND a.firstname = 'ihsan'
      AND a.currentrow = true 
      AND a.active = 'active'
      AND b.currentrow = true; 
-- employeeAppAuthorization: he already has tp access ... thought he was going 
  --  to be on a team previously, SET him up, they changed their mind, i forgot
  --  to UPDATE employeeAppAuthorization   
-- tpTeamTechs   
  @techKey = (SELECT techKey FROM tpTechs WHERE employeenumber = '170101'); 
  @hourlyRate = 17.5; ----------------------------------------------------------*
  INSERT INTO tpTeamTechs (teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate); 
-- tpTeamValues
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .197; -----------------------------------------------------------*   
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));   
-- tpTechValues ** ALL techs percentage change 
  -- ihsan
  @techPerc = .2333;
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- bear
  @techkey = (select techkey FROM tptechs WHERE lastname = 'bear');
  @techPerc = .311; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- holwerda
  @techkey = (select techkey FROM tptechs WHERE lastname = 'holwerda');
  @techPerc = .219; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);     
  -- strandell
  @techkey = (select techkey FROM tptechs WHERE lastname = 'strandell');
  @techPerc = .2246; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
CATCH ALL 
  RAISE bear(3, __errtext);
END TRY;        
--/> Bear ---------------------------------------------------------------------/>      

--< Girodat -------------------------------------------------------------------<
/* remove ryan winfield */
-- tpTeamTechs
TRY 
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'winfield' AND thrudate > curdate()); 
  @teamKey = (
    SELECT teamKey
    FROM tpTeams WHERE teamName = 'girodat');     
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND techKey = @techKey
    AND thruDate > curdate();    
-- tpTeamValues
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .249; -----------------------------------------------------------*    
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));   
-- tpTechValues ** ALL techs percentage change 
  -- girodat
  @techkey = (select techkey FROM tptechs WHERE lastname = 'girodat');
  @techPerc = .4299; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- mcveigh
  @techkey = (select techkey FROM tptechs WHERE lastname = 'mcveigh');
  @techPerc = .2562; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);    
  -- oneil
  @techkey = (select techkey FROM tptechs WHERE lastname = 'o''neil');
  @techPerc = .3123; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);    
CATCH ALL 
  RAISE girodat(3, __errtext);
END TRY;    
--/> Girodat ------------------------------------------------------------------/> 

--< Anderson ------------------------------------------------------------------<
/* ADD kyle grochow */
@teamKey = (SELECT teamkey FROM tpteams WHERE teamname = 'anderson');
TRY 
-- tpTechs
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @departmentKey, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @effDate
  -- SELECT *
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
    WHERE a.lastname = 'grochow'
      AND a.firstname = 'kyle'
      AND a.currentrow = true 
      AND a.active = 'active'
      AND b.currentrow = true;
-- employeeAppAuthorization
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  -- SELECT *
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 100
    AND b.approle = 'technician'
    AND a.username = 'kgrochow@rydellcars.com';        
-- tpTeamTechs   
  @techKey = (SELECT techKey FROM tpTechs WHERE employeenumber = '152520'); 
  @hourlyRate = 15; ----------------------------------------------------------*
  INSERT INTO tpTeamTechs (teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate);    
-- tpTeamValues
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .2201; -----------------------------------------------------------*   
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));  
-- tpTechValues ** ALL techs percentage change 
  -- grochow
  @techPerc = .2456;
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- anderson
  @techkey = (select techkey FROM tptechs WHERE lastname = 'anderson');
  @techPerc = .4639; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);      
  -- gehrtz
  @techkey = (select techkey FROM tptechs WHERE lastname = 'gehrtz');
  @techPerc = .2905; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);      
CATCH ALL 
  RAISE anderson(3, __errtext);
END TRY;       
--/> Anderson -----------------------------------------------------------------/>     

--< Olson ---------------------------------------------------------------------< 
-- tpTeamValues (new ELR)
TRY 
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Olson');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = (   
    SELECT poolPercentage
    FROM tpTeamValues
    WHERE teamKey = @teamKey 
      AND thruDate > curdate());
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));  
CATCH ALL 
  RAISE Olson(3, __errtext);
END TRY;       
--/> Olson --------------------------------------------------------------------/> 

--< Gray ---------------------------------------------------------------------< 
-- tpTeamValues (new ELR)
TRY 
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Gray');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = (   
    SELECT poolPercentage
    FROM tpTeamValues
    WHERE teamKey = @teamKey 
      AND thruDate > curdate()); 
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));   
CATCH ALL 
  RAISE Gray(3, __errtext);
END TRY;     
--/> Gray --------------------------------------------------------------------/> 

-- AND FINALLY, UPDATE tpdata  
    EXECUTE PROCEDURE xfmTpData();
    EXECUTE PROCEDURE todayxfmTpData();  
    
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    


