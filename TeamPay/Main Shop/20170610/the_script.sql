/*
I have moved Doug Bohm off Team Olson and to hourly pay, he had shoulder surgery and we will 
be using him for very light duty for the next 6 months until he gets back to 100%.
I moved Josh Syverson to Team Olson in his place at $17.29 per flat rate hour. 
That should be the only change for this going into effect 6/11/17. 

select * FROM tptechs WHERE lastname = 'syverson'

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 3
ORDER BY teamname, firstname  

SELECT *
FROM tpteamvalues
WHERE thrudate > curdate()

the from/thru dates on tptechs seem pointless, this IS my latest "operational" thinking
*/


DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '06/11/2017';
@thru_date = '06/10/2017';
@dept_key = 18;
@elr = 91.46;
BEGIN TRANSACTION;
TRY
-- remove doug FROM team olson
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'bohm');
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'olson');
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @team_key
	AND thrudate > curdate();
-- UPDATE doug's row IN tpTechFalues	
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  
-- ADD josh syverson to team olson	
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'syverson');
-- first undo the thru date IN tptechs for josh  
  UPDATE tptechs
  SET thrudate = '12/31/9999'
  WHERE techkey = @tech_key;
-- ADD josh to team olson  
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);
/*
budget remains the same: 91.46, no change to team values
josh's percentage = 17.29/91.46 = 18.90
his pto rate = 17.29, same AS his flat rate, & same AS his pto rate use to be
he still has an OPEN row IN tpTechValues
*/
-- CLOSE old row
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
-- new row
  INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyRate)
  values (@tech_key, @eff_date, 17.29/91.46, 17.29/91.46);
      
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();      

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  
