-- current values
SELECT a.teamKey, a.teamName, b.census, b.budget, b.poolPercentage, d.firstName, d.lastName, 
  d.techKey, e.techTeamPercentage, budget * techTeamPercentage
FROM tpTeams a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thrudate > curdate()
LEFT JOIN tpTeamTechs c on a.teamKey = c.teamKey
LEFT JOIN tpTechs d on c.techKey = d.techKey  
LEFT JOIN tpTechValues e on d.techKey = e.techKey
  AND e.thruDate > curdate()
WHERE a.thrudate > curdate()
  AND a.teamname = 'bear'
  
SELECT username, password
FROM tpemployees
WHERE lastname IN ('bear','holwerda','inad')  
  
10/29/15  

Remove Travis Strandell from Bear team effective in current pay period 10/18 -> 10/31

DECLARE @from_date date;
DECLARE @tech_key integer;
DECLARE @team_key integer;
DECLARE @thru_date date;
@from_date = '10/18/2015';
@thru_date = '12/31/9999';
@tech_key = 26; -- travis
@team_key = 22; -- bear

BEGIN TRANSACTION;
TRY 
/**/
  UPDATE tptechs
  SET thrudate = @from_date -1
  WHERE techkey = @tech_key;
  
  UPDATE tpteamtechs 
  SET thrudate = @from_date - 1
  WHERE teamkey = @team_key
    AND techkey = @tech_key;
    
  UPDATE tpteamvalues
  SET census = 3,
      budget = 54.05
  WHERE teamkey = @team_key
    AND fromdate = @from_date;
/**/    
/* 
  current elr = 91.46
  new budget = 3 * 91.46 * .197 = 54.0529
  jeff old % = .311 * 72.07 = 22.41377
       new % = x * 54.05 = 22.41377
               x = .4147
  kirby new % = x * 54.05 = 15.78333
                x = .292
  inad new % = x * 54.05 = 16.813931
               x = .3111      
*/               
  @tech_key = 24; -- kirby
  UPDATE tptechvalues
  SET techTeamPercentage = .292
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
    
  @tech_key = 55; -- jeff
  UPDATE tptechvalues
  SET techTeamPercentage = .4147
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;  
    
  @tech_key = 63; -- inad
  UPDATE tptechvalues
  SET techTeamPercentage = .311
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
/**/ 
  DELETE 
  FROM tpdata
  WHERE teamkey = 22
    AND thedate >= @from_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();   
/**/
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;   
 
  
  