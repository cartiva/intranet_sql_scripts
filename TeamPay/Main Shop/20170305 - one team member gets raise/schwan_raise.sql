/*
Olson team, schwann gets RAISE, FROM 21.77 to 23.50

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 3
ORDER BY teamname, firstname  

pool percentage: .245 -> .25
AS specified BY andrew:
olson: .3107 -> .305, $27.85
bohm: .1928 -> .189, $17.28
heffernan: .2507 -> .246, $22.47
schwan: .2429 -> .257, #21.77 -> $23.50 
resulted IN $.01 differences IN non changing techs
so
budget = census * elr *  pool_perc: 4 * 91.46 * .25 = 91.46
olson: 27.85/91.46 = .3045
bohm: 17.28/91.46 = .1889
heffernan: 22.47/91.46 = .2457
schwan: 23.50/91.46 = .2569

*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;

@eff_date = '03/05/2017';
@thru_date = '03/04/2017';
@dept_key = 18;
@elr = 91.46;
@team_key = 3;
@pool_perc = .25;
@census = 4;
BEGIN TRANSACTION;
TRY
-- team values
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
	
-- techValues
  -- olson
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'olson' -----------
    AND firstname = 'jay' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3045; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	
  -- bohm
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'bohm' -----------
    AND firstname = 'douglas' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .1889; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   
  -- heffernan
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'heffernan' -----------
    AND firstname = 'josh' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2457; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);     
  -- schwan
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'schwan' -----------
    AND firstname = 'michael' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2569; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);       
  
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND teamkey = @team_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();      

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    