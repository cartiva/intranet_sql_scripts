/*
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
ORDER BY teamname, firstname  


select *
FROM (
  select a.teamname, a.teamkey, COUNT(*)
  FROM tpTeams a
  INNER JOIN tpTeamTechs b on a.teamkey = b.teamkey
    AND b.thrudate > curdate()
  WHERE a.departmentkey = 18  
  GROUP BY a.teamname, a.teamkey) c
LEFT JOIN tpTeamValues d on c.teamkey = d.teamkey
  AND d.thrudate > curdate() 

SELECT *
FROM tpteamvalues
WHERE thrudate > curdate()
*/

-- don't forget to subscribe new techs to teampay
-- AND unsubscribe 
DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '04/03/2016';
@thru_date = '04/02/2016';
@dept_key = 18;
@elr = 91.46;
BEGIN TRANSACTION;
TRY
-- remove tech
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'Syverson');
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'Longoria');
  UPDATE tpTechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key;
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @team_key;
-- new techs:
-- desiree
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,
    fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber,
    @eff_date
  FROM dds.edwEmployeeDim a
  LEFT JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'grant'
    AND a.firstname = 'desiree'
    AND a.currentrow = true;
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'bear' AND thrudate > curdate()); 
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'grant' 
    AND firstname = 'desiree' AND thrudate > curdate());  
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date); 
-- anthony 
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,
    fromdate)   
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber,
    @eff_date
  FROM dds.edwEmployeeDim a
  LEFT JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'krewson'
    AND a.firstname = 'anthony'
    AND a.currentrow = true;   
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'girodat' AND thrudate > curdate()); 
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'krewson' 
    AND firstname = 'anthony' AND thrudate > curdate());  
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);      
-- team waldbauer  
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'waldbauer');
  @pool_perc = .198;
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key
    AND thrudate > curdate());
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
-- waldbauer techs  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'waldbauer' 
    AND firstname = 'alex' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3463;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'ortiz' 
    AND firstname = 'guadalupe' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3318;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate); 
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'kiefer' 
    AND firstname = 'lucas' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3212;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);     
  
-- team girodat
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'girodat');
  @pool_perc = .256;
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key
    AND thrudate > curdate());
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);  
  
-- girodat techs 
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'mcveigh' 
    AND firstname = 'dennis' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .1949;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'girodat' 
    AND firstname = 'jeff' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3296;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'o''neil' 
    AND firstname = 'timothy' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2279;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'krewson' 
    AND firstname = 'anthony' AND thrudate > curdate());  
  @pto_rate = (SELECT hourlyrate FROM dds.edwEmployeeDim WHERE lastname = 'krewson'
    AND firstname = 'anthony' AND currentrow = true);
  @tech_perc = .1655;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   
        
-- team olson
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'olson');
  @pool_perc = .245;
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key
    AND thrudate > curdate());
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);   
  
-- olson techs 
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'bohm' 
    AND firstname = 'douglas' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .1928;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate); 
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'olson' 
    AND firstname = 'jay' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3107;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'heffernan' 
    AND firstname = 'josh' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2507;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);       
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'schwan' 
    AND firstname = 'michael' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2429;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate); 
  
-- team gray
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'gray');
  @pool_perc = .22;
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key
    AND thrudate > curdate());
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);   
  
-- gray techs
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'david' 
    AND firstname = 'gordon' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2424;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);  
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gray' 
    AND firstname = 'nathan' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2687;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate); 
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'thompson' 
    AND firstname = 'wyatt' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2558;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);    
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'winkler' 
    AND firstname = 'zachary' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .219;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate); 
  
-- team longoria
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'longoria');
  @pool_perc = .215;
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key
    AND thrudate > curdate());
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date); 
  
-- longoria techs 
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'zacha' 
    AND firstname = 'charles' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .986;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);    
  
-- team template
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'bear');
  @pool_perc = .197;
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key
    AND thrudate > curdate());
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
  
-- bear techs 
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'inad' 
    AND firstname = 'ihsan' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2333;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);     
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'bear' 
    AND firstname = 'jeffery' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .311;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);    
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'holwerda' 
    AND firstname = 'kirby' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2328;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);     
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'grant' 
    AND firstname = 'desiree' AND thrudate > curdate());  
  @pto_rate = (SELECT hourlyrate FROM dds.edwEmployeeDim WHERE lastname = 'grant'
    AND firstname = 'desiree' AND currentrow = true);
  @tech_perc = .222;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);  
  
-- team anderson
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'anderson');
  @pool_perc = .245;
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key
    AND thrudate > curdate());
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);  
  
-- anderson techs 
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gehrtz' 
    AND firstname = 'clayton' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3004;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate); 
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'grochow' 
    AND firstname = 'kyle' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2429;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate); 
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'anderson' 
    AND firstname = 'shawn' AND thrudate > curdate());  
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .4392;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);    
         
-- don't forget vision subscribe/unsubscribe
  INSERT INTO employeeAppAuthorization 
  SELECT 'akrewson@rydellcars.com', appname, appseq, appcode, approle, 
    functionality, appDepartmentKey
  FROM employeeAppAuthorization
  WHERE username = 'kholwerda@rydellchev.com'
    AND appname IN ('teampay','customerFeedback');
  
  INSERT INTO employeeAppAuthorization 
  SELECT 'dgrant@rydellcars.com', appname, appseq, appcode, approle, 
    functionality, appDepartmentKey
  FROM employeeAppAuthorization
  WHERE username = 'kholwerda@rydellchev.com'
    AND appname IN ('teampay','customerFeedback');
    
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();      

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  
         