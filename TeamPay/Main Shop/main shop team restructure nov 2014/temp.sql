
DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
-- testing
  @effDate = '11/16/2014';  
  @newELR = 89.75;
  @departmentKey = 18;
-- production
--  @effDate = '11/30/2014';  
--  @newELR = asdf;  
--  @departmentKey = 18;  
BEGIN TRANSACTION;
TRY    
/* this portion IS necessary for backfilling the existing payperiod, for testing */
DELETE FROM tpData WHERE thedate > @effDate;
DELETE FROM tpShopValues WHERE fromDate = @effDate AND departmentKey = @departmentKey;
UPDATE tpShopValues SET thruDate = '12/31/9999' WHERE thruDate = @effDate AND departmentKey = @departmentKey;
DELETE FROM tpTeamValues WHERE fromDate = @effDate AND thruDate = '12/31/9999';
UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromdate = '11/02/2014'; 
/**/

--< production -----------------------------------------------------------------<
  DELETE 
  FROM tpData
  WHERE theDate = @effDate
    AND departmentKey = @departmentKey;

-- 1. tpShopValues: new ELR  
    -- CLOSE previous pay period
  UPDATE tpShopValues
  SET thruDate = @effDate - 1
  WHERE thruDate > curdate()
    AND departmentKey = @departmentKey;      
  -- INSERT row for new pay period
  INSERT INTO tpShopValues (departmentkey, fromdate, effectivelaborrate)
  values(@departmentKey, @effDate, @newELR);    
  
--/> production ----------------------------------------------------------------/>
-- 3. tpTeamTechs: remove ct stinar FROM team girodat 
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'stinar' AND thrudate > curdate()); 
  @teamKey = (
    SELECT teamKey
    FROM tpTeams WHERE teamName = 'girodat');     
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND techKey = @techKey
    AND thruDate > curdate(); 
-- 4. tpTeamValues
  -- girodat
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'girodat');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .2375;    
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));  
  -- gray
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'gray');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .20;    
    -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();
    -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));     
  -- Waldbauer
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Waldbauer');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .198;    
    -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();
    -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));   
  -- Olson
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Olson');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .232;    
    -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();
    -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));     
  -- Longoria
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Longoria');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .175;    
    -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();
    -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));  
-- 5. tpTechValues
  -- gray
  @techPerc = .2202; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'gray' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  -- strandell
  @techPerc = .1765; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'strandell' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  -- thompson
  @techPerc = .2248; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'thompson' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- david
  @techPerc = .2134; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'david' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);    
  -- winkler
  @techPerc = .1654; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'winkler' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);    
  -- Waldbauer
  @techPerc = .3463; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'Waldbauer' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- Ortiz
  @techPerc = .3318; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'Ortiz' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);    
  -- Kiefer
  @techPerc = .3212; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'Kiefer' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);      
  -- Girodat
  @techPerc = .3381; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'Girodat' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  -- Winfield
  @techPerc = .2061; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'Winfield' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);    
    -- Mcveigh
  @techPerc = .2015; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'Mcveigh' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);   
  -- O'neil
  @techPerc = .2455; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'O''neil' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);   
  -- Olson
  @techPerc = .3104; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'Olson' AND firstname = 'Jay' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);   
  -- Schwan
  @techPerc = .2388; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'Schwan' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);    
  -- Bohm
  @techPerc = .2036; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'Bohm' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);      
  -- Heffernan
  @techPerc = .2471; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'Heffernan' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);       
  -- Syverson
  @techPerc = .4955; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'Syverson' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);     
  -- holwerda
  @techPerc = .492; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'holwerda' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);        
-- AND FINALLY, UPDATE tpdata  
    EXECUTE PROCEDURE xfmTpData();
    EXECUTE PROCEDURE todayxfmTpData();  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    