SELECT DISTINCT payperiodend, firstname, lastname, round(techtfrrate, 2) AS flatRate
FROM tpdata a
WHERE departmentkey = 18
  AND techkey IN (
    SELECT techkey
    FROM tpteamtechs
    WHERE thrudate > curdate())
  AND payperiodend = (
    SELECT MIN(payperiodend)
    FROM tpdata
    WHERE techkey = a.techkey)    


SELECT DISTINCT payperiodend, firstname, lastname, round(techtfrrate, 2) AS flatRate
FROM tpdata a
WHERE departmentkey = 18
  AND techkey IN (
    SELECT techkey
    FROM tpteamtechs
    WHERE thrudate > curdate())
  AND payperiodend = '09/06/2014'    

  
SELECT DISTINCT firstname, lastname, payperiodend, round(techtfrrate, 2)
FROM tpdata a  
WHERE departmentkey = 18
  AND techkey IN (
    SELECT techkey
    FROM tpteamtechs
    WHERE thrudate > curdate())
    
    
SELECT firstname, lastname, round(MIN(rate), 2) AS minimum, 
  round(MAX(rate), 2) AS maximum, round(avg(rate), 2) AS average
FROM  (
  SELECT DISTINCT firstname, lastname, payperiodend, round(techtfrrate, 2) AS rate 
  FROM tpdata a  
  WHERE departmentkey = 18
    AND techkey IN (
      SELECT techkey
      FROM tpteamtechs
      WHERE thrudate > curdate())) a   
GROUP BY firstname, lastname      


SELECT b.*,
  (SELECT MAX(payperiodend)
    FROM tpdata
    WHERE firstname = b.firstname
      AND lastname = b.lastname
      AND round(techtfrrate,2) = b.minimum
      AND payperiodend < '09/20/2014') AS minDate,
  (SELECT MAX(payperiodend)
    FROM tpdata
    WHERE firstname = b.firstname
      AND lastname = b.lastname
      AND round(techtfrrate,2) = b.maximum) AS maxDate,
   c.flatrate    
FROM (
  SELECT firstname, lastname, round(MIN(rate), 2) AS minimum, 
    round(MAX(rate), 2) AS maximum, round(avg(rate), 2) AS average
  FROM  (
    SELECT DISTINCT firstname, lastname, payperiodend, round(techtfrrate, 2) AS rate 
    FROM tpdata a  
    WHERE departmentkey = 18
      AND techkey IN (
        SELECT techkey
        FROM tpteamtechs
        WHERE thrudate > curdate())
      AND payperiodend < '09/20/2014') a   
  GROUP BY firstname, lastname) b    
LEFT JOIN (
  SELECT DISTINCT payperiodend, firstname, lastname, round(techtfrrate, 2) AS flatRate
  FROM tpdata a
  WHERE departmentkey = 18
    AND techkey IN (
      SELECT techkey
      FROM tpteamtechs
      WHERE thrudate > curdate())
    AND payperiodend = '09/06/2014') c on b.firstname = c.firstname AND b.lastname = c.lastname  