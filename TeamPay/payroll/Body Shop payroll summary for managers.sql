/*
08/24 
  separated body shop AND main shop
  remove transition
  
10/7/14
  fuck me, somehow, bsFlatRateData showed 25 hours metal flag hours for 9/22
  could NOT figure out WHERE it came FROM, but this query (LIKE payroll)
  uses factRepairOrder instead of bsFlatRateData, so the difference showed up
  comparing this output to the vision page
  fuck me
  fuck me
  fuck me  
*/  
DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 13;
@payRollEnd = '07/08/2017';
select x.*-- , round(TotalCommPay - transition, 2) AS [Comm - Transition]
FROM (
  select c.teamname AS Team, lastname, firstname, employeenumber, 
    techhourlyrate AS HourlyRate, round(techtfrrate, 2) AS CommRate, 
    techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
    round(techclockhourspptd*TeamProfPPTD/100, 2) as CommHours, 
    round(round(techtfrrate, 2)*round(TeamProfPPTD*techclockhourspptd/100, 2), 2) AS CommPay,
    techvacationhourspptd AS VacationHours,
    techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
    round(round(techtfrrate, 2) * round(techclockhourspptd * teamprofpptd/100, 2), 2) + 
      (techvacationhourspptd + techptohourspptd + 
        techholidayhourspptd)*techhourlyrate AS TotalPay
--     round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
--       techholidayhourspptd)*techhourlyrate, 2) AS Transition
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
--  INNER JOIN tpteams c on b.teamkey = c.teamkey
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
  WHERE thedate = @payRollEnd
    AND a.departmentKey = @department
  union
  select c.teamname AS Team, left(cast(NULL AS sql_char), 1),
    left(cast(NULL AS sql_char), 1),left(cast(NULL AS sql_char), 1),
    CAST(NULL AS sql_integer),CAST(NULL AS sql_integer),
    sum(techclockhourspptd) AS ClockHours, sum(TechFlagHoursPPTD) as FlagHours, 
    max(teamprofpptd) AS TeamProf, 
    CAST(NULL AS sql_integer), CAST(NULL AS sql_integer),
    sum(techvacationhourspptd) AS VacationHours,
    sum(techptohourspptd) AS PTOHours, sum(techholidayhourspptd) AS HolidayHours,
    CAST(NULL AS sql_integer) -- ,CAST(NULL AS sql_integer)
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
--  INNER JOIN tpteams c on b.teamkey = c.teamkey
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
  WHERE thedate = @payRollEnd
    AND a.departmentKey = @department
  GROUP BY c.TeamName) x

  
/*
PDR
*/ 

-- first cut at a finished output  
-- looks good
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @pdr_rate double;
DECLARE @metal_rate double;
DECLARE @hourly_rate double;
@pdr_rate = (SELECT pdrrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@metal_rate = (SELECT metalrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@hourly_rate = (SELECT otherrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@from_date = '06/25/2017'; --------------------- change dates
@thru_date = '07/08/2017';

SELECT n.*, o.*, pdrPay + metalPay + hourlyPay AS totalPay
FROM ( 
  SELECT pdrRate, round(SUM(pdrFlagHours),2) AS pdrFlagHours, 
    round(pdrRate * round(SUM(pdrFlagHours),2), 2) AS pdrPay,
    metalRate, 
    round(SUM(metalFlagHours), 2) AS metalFlagHours,
    round(metalRate * round(SUM(metalFlagHours), 2), 2) AS metalPay
  FROM (  
    SELECT theDate, @pdr_rate AS pdrRate,
      coalesce(CASE WHEN c.opcode = 'PDR' THEN flaghours END, 0) AS pdrFlagHours,
      @metal_rate AS metalRate,
      coalesce(CASE WHEN c.opcode <>'PDR' THEN flaghours END, 0) AS metalFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.closedatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.theDate BETWEEN @from_date AND @thru_date) m    
    GROUP BY pdrRate, metalRate) n  
LEFT JOIN (
  SELECT @hourly_rate AS hourlyRate, SUM(vacationHours) AS vacationHours, sum(ptoHours) AS ptoHours, 
    sum(holidayHours) AS holidayHours,
    @hourly_rate * (SUM(vacationHours) + SUM(ptoHours) + sum(holidayHours)) AS hourlyPay
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.theDate BETWEEN @from_date AND @thru_date) o  on 1 = 1 
    
     