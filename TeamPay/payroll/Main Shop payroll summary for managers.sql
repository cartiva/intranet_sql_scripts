/*
08/24 
  separated body shop AND main shop
  remove transition
  ADD clock hour bonus

make rounding consistent with what gets sent to kim
AND what shows on individual tech pay pages
what gets rounded:
commPay seems to be off, on kim:
  round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2) AS [Total Commission Pay],
  
9/29
Bonus, after mindfucking myself INTO a frenzy, ben clarified it 
total hours (pto + vac + clock) > 90

clock hours = 80
pto hours = 20
comm pay = 70 * commRate * teamProf
bonus pay = 10 * bonusRate * teamProf
pto pay = 20 * ptoRate

clock hours = 100
pto hours = 16
comm pay = 74 * commRate * teamProf
bonus pay = 26 * bonusRate * teamProf
pto pay = 16 * ptoRate

summary: 
IF total hours > 90
bonus hours = total hours - 90
commission hours = clock hours - bonus hours  
ELSE commision hours = clock hours

eg
  CASE WHEN e.totalHours > 90 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
  round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours   
*/
DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 18;
@payRollEnd = '07/08/2017';
SELECT x.*, 
  round(commHours * teamProf/100 * commRate, 2) AS commPay,
  round(bonusHours * teamProf/100 * bonusRate, 2) AS bonusPay,
  round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS ptoPay,
  round(commHours * teamProf/100 * commRate, 2) +  
    round(bonusHours * teamProf/100 * bonusRate, 2) +
    round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS totalPay  
FROM (  
select d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
  d.bonusRate, d.teamProf, d.flagHours, e.clockHours, e.vacationHours, e.ptoHours, e.holidayHours, 
  e.totalHours,
--  CASE WHEN e.totalHours > 90 THEN 90 else e.clockHours end AS commHours,
--  round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours
  CASE WHEN e.totalHours > 90 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
  round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours  
FROM (
  select c.teamname AS Team, lastname, firstname, employeenumber, 
    techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
    2 * round(techtfrrate, 2) AS BonusRate, teamprofpptd AS teamProf, 
    TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf   
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
    AND b.thrudate > curdate()
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
  WHERE thedate = @payRollEnd
    AND a.departmentKey = @department) d
LEFT JOIN (
  select employeenumber, techclockhourspptd AS clockHours, 
    techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
    techholidayhourspptd AS holidayHours,
    techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
    AND b.thrudate > curdate()
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
  WHERE thedate = @payRollEnd
    AND a.departmentKey = @department) e on d.employeenumber = e.employeenumber) x

ORDER BY team, firstname, lastname

  