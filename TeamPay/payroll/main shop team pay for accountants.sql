select a.teamname, c.employeenumber, c.technumber, 
  c.lastname, c.firstname, d.effectivelaborrate,
  e.census, e.budget, e.poolpercentage, f.techteampercentage,
  round(budget * techteampercentage, 2)
-- SELECT *  
FROM tpteams a
inner join tpteamtechs b on a.teamkey = b.teamkey
  AND '05/20/2014' BETWEEN b.fromdate AND b.thrudate
INNER JOIN tptechs c on b.techkey = c.techkey 
LEFT JOIN tpshopvalues d on 1 = 1
  AND d.departmentkey = 18
  AND '05/20/2014' BETWEEN d.fromdate AND d.thrudate
LEFT JOIN tpteamvalues e on a.teamkey = e.teamkey  
  AND '05/20/2014' BETWEEN e.fromdate AND e.thrudate
LEFT JOIN tptechvalues f on b.techkey = f.techkey 
  AND '05/20/2014' BETWEEN f.fromdate AND f.thrudate
WHERE a.departmentkey = 18
ORDER BY lastname, firstname

