-- sent the results of this to kim
SELECT x.*, [clock hours] * [commission rate] FROM (
SELECT trim(lastname) + ', ' + trim(firstname) AS Name, employeenumber AS [Employee Number], 
  CASE WHEN commpay >= transitionpay THEN commrate ELSE hourlyrate END AS [Commission Rate],
  CASE WHEN commpay >= transitionpay
    THEN round(teamprof*clockhours/100, 2)
    ELSE clockhours
  END AS [Clock Hours],
  CASE WHEN commpay >= transitionpay
    THEN round(commrate*teamprof*clockhours/100, 2)
    
    ELSE round(hourlyrate*clockhours, 2)
  END AS [Total Commission Pay],
  commrate * round(teamprof*clockhours/100, 2) AS CommPay2,
  
  HourlyRate AS [Vac PTO Hol Rate],
  vacationhours + ptohours AS [Vacation PTO Hours],
  round(hourlyrate * (vacationhours + ptohours), 2) AS [Vacation PTO Pay],
  HolidayHours AS [Holiday Hours],
  round(hourlyrate * holidayhours, 2) AS [Holiday Pay],
  CASE WHEN commpay >= transitionpay
    THEN round(commrate*teamprof*clockhours/100, 2) 
      + round(hourlyrate * holidayhours, 2)
      + round(hourlyrate * (vacationhours + ptohours), 2)
    ELSE round(hourlyrate*clockhours, 2)
      + round(hourlyrate * holidayhours, 2)
      + round(hourlyrate * (vacationhours + ptohours), 2)
  END AS [Total Gross Pay]            
FROM (
  select c.teamname AS Team, lastname, firstname, employeenumber, 
    techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
    techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
    techvacationhourspptd AS VacationHours,
    techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) + 
      (techvacationhourspptd + techptohourspptd + 
        techholidayhourspptd)*techhourlyrate AS CommPay,
     round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
       techholidayhourspptd)*techhourlyrate, 2) AS TransitionPay
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
  INNER JOIN tpteams c on b.teamkey = c.teamkey
  WHERE thedate = '12/28/2013') a
  ) x
ORDER BY name


ok, what i have to DO IS , at what ever level a value IS displayed, that value
THEN has to be used IN subsequent calculations
so IF it IS rounded for display, the rounded value has to be used IN calculations
the difference for girodat IS the difference BETWEEN 28.43 * 77.41 = 2200.77 AND 28.4256 * 77.41 = 2200.43

use this query for the base values, no calculations
  select c.teamname AS Team, lastname, firstname, employeenumber, 
    techhourlyrate AS HourlyRate, /**/round(techtfrrate, 2) AS CommRate, 
    techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
    techvacationhourspptd AS VacationHours,
    techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours
/*    
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) + 
      (techvacationhourspptd + techptohourspptd + 
        techholidayhourspptd)*techhourlyrate AS CommPay,
     round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
       techholidayhourspptd)*techhourlyrate, 2) AS TransitionPay
*/       
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
  INNER JOIN tpteams c on b.teamkey = c.teamkey
  WHERE thedate = '12/28/2013'
  AND lastname = 'girodat'
  
-- now DO some calcs on these results
-- bingo
SELECT y.*, /**/round(commrate * commhours, 2), clockhours * teamprof/100 * techtfrrate
FROM (
  SELECT team, lastname, firstname, employeenumber, hourlyrate, commrate, clockhours, flaghours, teamprof,
    /**/round(clockhours * teamprof/100, 2) AS commhours, techtfrrate
  FROM (
    select c.teamname AS Team, lastname, firstname, employeenumber, 
      techhourlyrate AS HourlyRate, /**/round(techtfrrate, 2) AS CommRate, 
      techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
      techvacationhourspptd AS VacationHours,
      techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours, techtfrrate
  /*    
      round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) + 
        (techvacationhourspptd + techptohourspptd + 
          techholidayhourspptd)*techhourlyrate AS CommPay,
       round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
         techholidayhourspptd)*techhourlyrate, 2) AS TransitionPay
  */       
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
    INNER JOIN tpteams c on b.teamkey = c.teamkey
    WHERE thedate = '12/28/2013'
    AND lastname = 'girodat') x) y  
  
leaning toward generating commission, transition, hol/vac/pto each separately  

ok, so at what level DO i round, IN terms of the legal source of payroll data
what are the values that need to be locked IN at a rounded value

TechTFRRate 
somehow, the commissionhours, 
IN sp.GetTpTeamTotals which generates the team displays, 
  i am USING round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours
  
IS commissionhours a value that i should be generating (rounded) AND storing on a daily basis  

the driving force seems to me to be what IS displayed
there needs to be consistency BETWEEN ALL displays at every level
which points towards storing all the values (rounded) AND USING them for calcs

SELECT DISTINCT teamprofpptd FROM tpdata

-- 1/13/14  

-- sent the results of this to kim
-- 1. round commission rate
-- 2. round clockhours*teamprof
SELECT trim(lastname) + ', ' + trim(firstname) AS Name, employeenumber AS [Employee Number], 
--  CASE WHEN commpay >= transitionpay THEN commrate ELSE hourlyrate END AS [Commission Rate],
-- 1
  CASE WHEN commpay >= transitionpay THEN round(commrate, 2) ELSE hourlyrate END AS [Commission Rate],
  CASE WHEN commpay >= transitionpay
    THEN round(teamprof*clockhours/100, 2)
    ELSE clockhours
  END AS [Clock Hours],
  CASE WHEN commpay >= transitionpay
--    THEN round(commrate*teamprof*clockhours/100, 2)
-- 1, 2
    THEN round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2)
    ELSE round(hourlyrate*clockhours, 2)
  END AS [Total Commission Pay],
  HourlyRate AS [Vac PTO Hol Rate],
  vacationhours + ptohours AS [Vacation PTO Hours],
  round(hourlyrate * (vacationhours + ptohours), 2) AS [Vacation PTO Pay],
  HolidayHours AS [Holiday Hours],
  round(hourlyrate * holidayhours, 2) AS [Holiday Pay],
  CASE WHEN commpay >= transitionpay
    THEN round(commrate*teamprof*clockhours/100, 2) 
      + round(hourlyrate * holidayhours, 2)
      + round(hourlyrate * (vacationhours + ptohours), 2)
    ELSE round(hourlyrate*clockhours, 2)
      + round(hourlyrate * holidayhours, 2)
      + round(hourlyrate * (vacationhours + ptohours), 2)
  END AS [Total Gross Pay]    
FROM (
  select c.teamname AS Team, lastname, firstname, employeenumber, 
    techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
    techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
    techvacationhourspptd AS VacationHours,
    techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) + 
      (techvacationhourspptd + techptohourspptd + 
        techholidayhourspptd)*techhourlyrate AS CommPay,
     round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
       techholidayhourspptd)*techhourlyrate, 2) AS TransitionPay
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
  INNER JOIN tpteams c on b.teamkey = c.teamkey
  WHERE thedate = '01/11/2014') a
ORDER BY name  
