alter PROCEDURE GetTpShopTotals
   ( 
      department CICHAR ( 12 ),
      payPeriodIndicator Integer,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT,
	  flagTimeAdjustments double(15) output
   ) 
BEGIN 
/*
2/20 bodyshop mod
  remove session, ADD department
  
7/22  
  ADD flag time adjustments
   
EXECUTE PROCEDURE GetTpShopTotals('mainshop', -2);
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
//DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
DECLARE @department integer;
@department = 
  CASE (SELECT department FROM __input)
    WHEN 'mainshop' THEN 18
	WHEN 'bodyshop' THEN 13
  END;
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
  INSERT INTO __output
  SELECT sum(FlagHours), SUM(clockhours), 
    CASE SUM(clockhours)
	    WHEN 0 THEN 0
	    ELSE round(SUM(FlagHours)/SUM(ClockHours), 4)
	  END,
    sum(PayHours), coalesce(SUM(regularPay), 0),
	SUM(flagTimeAdjustments)
  FROM (
    SELECT a.*, b.RegularPay 
    FROM (
      SELECT distinct teamname, 
        (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD 
		  + TeamFlagHourAdjustmentsPPTD) AS FlagHours,
		TeamFlagHourAdjustmentsPPTD AS flagTimeAdjustments,
        teamclockhourspptd AS ClockHours, teamprofpptd,
        round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours
      FROM tpData
      WHERE thedate = @date
	    AND departmentKey = @department) a
    LEFT JOIN (
    SELECT teamname, coalesce(SUM(RegularPay), 0) AS RegularPay
      FROM (
      SELECT distinct teamname, teamkey, technumber, techkey,
        round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
      FROM tpData
      WHERE thedate = @date) r
    GROUP BY teamname) b on a.teamname = b.teamname) s;
//  ENDIF;  

END;


alter PROCEDURE GetTpTeamTotals
   ( 
      department CICHAR ( 12 ),
      payPeriodIndicator Integer,
      teamName CICHAR ( 52 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT,
	  flagTimeAdjustments double(15) output
   ) 
BEGIN 
/*
2/20 bodyshop mod
  remove session, ADD department

7/22  
  ADD flag time adjustments
     
EXECUTE PROCEDURE GetTpTeamTotals('mainshop', -1);
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @department integer;
DECLARE @curPayPeriodSeq integer;

@department = 
  CASE (SELECT department FROM __input)
    WHEN 'mainshop' THEN 18
	WHEN 'bodyshop' THEN 13
  END;
  
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
  INSERT INTO __output
  SELECT --a.*, 
    a.teamname, a.flaghours, a.teamclockhourspptd, a.teamProf, 
	a.payHours, b.RegularPay, a.TeamFlagHourAdjustmentsPPTD
  FROM (
    SELECT distinct teamname, 
      (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD 
	    + TeamFlagHourAdjustmentsPPTD) AS FlagHours,
      teamclockhourspptd, teamprofpptd/100 AS teamProf,
      round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours,
	  TeamFlagHourAdjustmentsPPTD
    FROM tpData
    WHERE thedate = @date
	  AND departmentKey = @department) a
  LEFT JOIN (
  SELECT teamname, coalesce(SUM(RegularPay), 0) AS RegularPay
    FROM (
    SELECT distinct teamname, teamkey, technumber, techkey,
      round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
    FROM tpData
    WHERE thedate = @date
	  AND departmentKey = @department) r
  GROUP BY teamname) b on a.teamname = b.teamname;


END;

alter PROCEDURE getTpEmployeeTotalsForDepartment
   ( 
      department CICHAR ( 12 ),
      payPeriodIndicator Integer,
      teamName CICHAR ( 52 ) OUTPUT,
      tech CICHAR ( 60 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT,
      username CICHAR ( 50 ) OUTPUT,
	  flagTimeAdjustments double(15) output
   ) 
BEGIN 
/*
3/5
  ADD username AS an output
  
7/22  
  ADD flag time adjustments
    
execute procedure getTPEmployeeTotalsForDepartment('mainshop',-2)
*/	
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @department integer;
@department = 
  CASE (SELECT department FROM __input)
    WHEN 'mainshop' THEN 18
	WHEN 'bodyshop' THEN 13
  END;
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
      
  INSERT INTO __output
  SELECT top 1000 *
  FROM (
    SELECT teamname, trim(firstname) + ' '  + lastname AS tech, 
      (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd 
	    + TechFlagHourAdjustmentspptd) AS "Flag Hours",
      techclockhourspptd AS "clock hours", 
      teamprofpptd/100 AS "team prof", 
	    round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
      coalesce(round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2), 0) AS "Regular Pay",
      (SELECT username FROM tpEmployees WHERE firstname = a.firstname AND lastname = a.lastname),
	  TechFlagHourAdjustmentspptd
    FROM tpdata a
    WHERE thedate = @date
	  AND departmentKey = @department) x
  ORDER BY teamname, tech;


END;

alter PROCEDURE getTpEmployeeTotalsForTeamlead
   ( 
      username CICHAR ( 50 ),
      payPeriodIndicator Integer,
      teamName CICHAR ( 52 ) OUTPUT,
      tech CICHAR ( 60 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT,
	  flagTimeAdjustments double(15) output,
	  username CICHAR ( 50 ) OUTPUT
   ) 
BEGIN 
/*
3/12: IF paint team, show both teams

7/22  
  ADD flag time adjustments
  ADD tech username
  
execute procedure getTpEmployeeTotalsForTeamlead('ngray@rydellchev.com',0)
*/	
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string;
DECLARE @teamKey integer;
@username = (SELECT username FROM __input);
@teamKey = (
  SELECT teamKey
  FROM tpTeamTechs a
  WHERE techKey = (
    SELECT a.techkey
    FROM tpTechs a
    INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    WHERE b.username = @username)
  AND curdate() BETWEEN a.fromDate AND a.thruDate);
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
 
IF @teamKey IN (18,19) THEN
INSERT INTO __output
    SELECT top 100 teamname, trim(firstname) + ' '  + lastname AS tech, 
      (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd 
	    + TechFlagHourAdjustmentspptd) AS "Flag Hours",
      techclockhourspptd AS "clock hours", 
      teamprofpptd/100 AS "team prof", 
	  round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
      coalesce(round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2), 0) AS "Regular Pay",
	  TechFlagHourAdjustmentspptd,
	  (SELECT username FROM tpEmployees WHERE firstname = a.firstname AND a.lastname = lastname)
    FROM tpdata a
    WHERE thedate = curdate()
	  AND teamkey IN (18,19)
	ORDER BY teamname, tech;
ELSE 	      
  INSERT INTO __output
  SELECT top 1000 *
  FROM (
    SELECT teamname, trim(firstname) + ' '  + lastname AS tech, 
      (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd 
	    + TechFlagHourAdjustmentspptd) AS "Flag Hours",
      techclockhourspptd AS "clock hours", 
      teamprofpptd/100 AS "team prof", 
	  round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
      coalesce(round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2), 0) AS "Regular Pay",
	  TechFlagHourAdjustmentspptd,
	  (SELECT username FROM tpEmployees WHERE firstname = a.firstname AND lastname = a.lastname)
    FROM tpdata a
    WHERE thedate = @date
	  AND teamkey = @teamKey) x
  ORDER BY teamname, tech;
END IF;


END;
