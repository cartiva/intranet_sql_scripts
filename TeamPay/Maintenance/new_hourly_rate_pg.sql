﻿-- imports:
--   tptechs
--   tpteamtechs
--   tptechvalues
--   pyptbdta
--   pyhscdta
--   pyhshdta
--   edwClockHoursFact
--   edwEmployeeDim
-- 
-- -- current techs
-- SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
--   round(c.previousHourlyRate, 2) AS prev_rate
-- --INTO #techs
-- FROM ads.ext_tpTechs a
-- INNER JOIN ads.ext_tpTeamTechs b on a.techkey = b.techkey
-- INNER JOIN ads.ext_tpTechValues c on a.techkey = c.techkey
--   AND c.thrudate > current_date
-- WHERE b.thrudate > current_date 
-- 
-- -- 2016 pay
-- SELECT c.firstname, c.lastname, c.employeenumber, sum(b.amount) as paid_2016
-- --INTO #pay_2015
-- FROM dds.ext_pyptbdta a
-- INNER JOIN dds.ext_pyhscdta b on a.company_number = b.company_number
--   AND a.payroll_run_number = b.payroll_run_number
--   AND b.code_type = '1'
--   AND b.code_id IN ('87','79','75') 
-- INNER JOIN (
--   SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
--     round(c.previousHourlyRate, 2) AS prev_rate
--   --INTO #techs
--   FROM ads.ext_tpTechs a
--   INNER JOIN ads.ext_tpTeamTechs b on a.techkey = b.techkey
--   INNER JOIN ads.ext_tpTechValues c on a.techkey = c.techkey
--     AND c.thrudate > current_date
--   WHERE b.thrudate > current_date ) c on TRIM(b.employee_number) = c.employeenumber
-- WHERE a.payroll_cen_year = 116
-- GROUP BY c.firstname, c.lastname, c.employeenumber
-- 
-- 
-- -- clock hours
-- SELECT e.firstname, e.lastname, e.employeenumber, SUM(c.clockhours) AS clock_hours
-- -- INTO #clock_hours_2015
-- FROM dds.day a
-- INNER JOIN dds.day b on a.datekey = b.datekey
--   and b.theyear = 2016
-- INNER JOIN ads.ext_dds_edwClockHoursFact c on a.datekey = c.datekey  
--   AND c.clockhours <> 0
-- INNER JOIN ads.ext_dds_edwEmployeeDim d on c.employeekey = d.employeekey  
-- INNER JOIN (
--   SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
--     round(c.previousHourlyRate, 2) AS prev_rate
--   --INTO #techs
--   FROM ads.ext_tpTechs a
--   INNER JOIN ads.ext_tpTeamTechs b on a.techkey = b.techkey
--   INNER JOIN ads.ext_tpTechValues c on a.techkey = c.techkey
--     AND c.thrudate > current_date
--   WHERE b.thrudate > current_date ) e on d.employeenumber = e.employeenumber
-- GROUP BY e.firstname, e.lastname, e.employeenumber;


select x.departmentkey, x.employeenumber, x.firstname, x.lastname, 
  y.paid_2016, round(z.clock_hours, 2) as clock_hours_2016, 
  x.prev_rate as current_pto_rate, round(y.paid_2016/z.clock_hours, 2) as new_pto_rate,
  round(y.paid_2016/z.clock_hours, 2) - x.prev_rate
   
from (
  -- current techs
  SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
    round(c.previousHourlyRate, 2) AS prev_rate
  --INTO #techs
  FROM ads.ext_tpTechs a
  INNER JOIN ads.ext_tpTeamTechs b on a.techkey = b.techkey
  INNER JOIN ads.ext_tpTechValues c on a.techkey = c.techkey
    AND c.thrudate > current_date
  WHERE b.thrudate > current_date ) x
left join (
  SELECT c.firstname, c.lastname, c.employeenumber, sum(b.amount) as paid_2016
  --INTO #pay_2015
  FROM dds.ext_pyptbdta a
  INNER JOIN dds.ext_pyhscdta b on a.company_number = b.company_number
    AND a.payroll_run_number = b.payroll_run_number
    AND b.code_type = '1'
    AND b.code_id IN ('87','79','75') 
  INNER JOIN (
    SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
      round(c.previousHourlyRate, 2) AS prev_rate
    --INTO #techs
    FROM ads.ext_tpTechs a
    INNER JOIN ads.ext_tpTeamTechs b on a.techkey = b.techkey
    INNER JOIN ads.ext_tpTechValues c on a.techkey = c.techkey
      AND c.thrudate > current_date
    WHERE b.thrudate > current_date ) c on TRIM(b.employee_number) = c.employeenumber
  WHERE a.payroll_cen_year = 116
  GROUP BY c.firstname, c.lastname, c.employeenumber) y on x.employeenumber = y.employeenumber  
left join (
  SELECT e.firstname, e.lastname, e.employeenumber, SUM(c.clockhours) AS clock_hours
  -- INTO #clock_hours_2015
  FROM dds.day a
  INNER JOIN dds.day b on a.datekey = b.datekey
    and b.theyear = 2016
  INNER JOIN ads.ext_dds_edwClockHoursFact c on a.datekey = c.datekey  
    AND c.clockhours <> 0
  INNER JOIN ads.ext_dds_edwEmployeeDim d on c.employeekey = d.employeekey  
  INNER JOIN (
    SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
      round(c.previousHourlyRate, 2) AS prev_rate
    --INTO #techs
    FROM ads.ext_tpTechs a
    INNER JOIN ads.ext_tpTeamTechs b on a.techkey = b.techkey
    INNER JOIN ads.ext_tpTechValues c on a.techkey = c.techkey
      AND c.thrudate > current_date
    WHERE b.thrudate > current_date ) e on d.employeenumber = e.employeenumber
  GROUP BY e.firstname, e.lastname, e.employeenumber) z on x.employeenumber = z.employeenumber
order by departmentkey, lastname;


