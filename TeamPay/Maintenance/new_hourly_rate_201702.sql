-- divide queries up INTO techs, pay, hours, date range, 

-- current techs
-- 2/7/16 Brian Peterson (PDR) IS NOT getting updated, leave him out
SELECT COUNT(*) FROM #techs;
DROP TABLE #techs;
SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, a.employeenumber, 
  round(c.previousHourlyRate, 2) AS prev_rate, c.techTeamPercentage
INTO #techs
FROM tpTechs a
INNER JOIN tpTeamTechs b on a.techkey = b.techkey
INNER JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()
WHERE b.thrudate > curdate()
--UNION 
--SELECT departmentkey, employeenumber, firstname, lastname, employeenumber, round(otherrate, 2) AS prev_rate
--FROM bsflatratetechs  
--WHERE thrudate > curdate();

SELECT * FROM dds.ext_pyptbdta a
-- 2015 pay
-- DROP TABLE #pay_2016;
SELECT c.firstname, c.lastname, c.employeenumber, sum(b.amount) as paid_2016
INTO #pay_2016
FROM dds.ext_pyptbdta a
INNER JOIN dds.ext_pyhscdta b on a.company_number = b.company_number
  AND a.payroll_run_number = b.payroll_run_number
  AND b.code_type = '1'
  AND b.code_id IN ('87','79','75') 
INNER JOIN #techs c on TRIM(b.employee_number) = c.employeenumber
WHERE a.payroll_cen_year = 116
GROUP BY c.firstname, c.lastname, c.employeenumber;

-- date_range
SELECT * FROM #date_range
DROP TABLE #date_range;
SELECT
  cast(trim(substring(CAST(from_date AS sql_char),5,2)) + '/' + 
      trim(substring(CAST(from_date AS sql_char),7,2)) + '/' +
      LEFT(trim(CAST(from_date AS sql_char)),4) AS sql_Date) AS from_date,
  cast(trim(substring(CAST(thru_date AS sql_char),5,2)) + '/' + 
      trim(substring(CAST(thru_date AS sql_char),7,2)) + '/' +
      LEFT(trim(CAST(thru_date AS sql_char)),4) AS sql_Date) AS thru_date  
INTO #date_range          
FROM (
  select MIN(payroll_start_date) AS from_date, MAX(payroll_end_date) AS thru_date
  FROM dds.ext_pyptbdta
  WHERE company_number = 'RY1'
    AND payroll_cen_year = 116) x; 

-- clock hours
DROP TABLE #clock_hours_2016;
SELECT e.firstname, e.lastname, e.employeenumber, SUM(c.clockhours) AS clock_hours
INTO #clock_hours_2016
FROM dds.day a
-- INNER JOIN #date_range b on a.thedate BETWEEN b.from_date AND b.thru_date  
INNER JOIN dds.edwClockHoursFact c on a.datekey = c.datekey  
  AND c.clockhours <> 0
INNER JOIN dds.edwEmployeeDim d on c.employeekey = d.employeekey  
INNER JOIN #techs e on d.employeenumber = e.employeenumber
WHERE a.theyear = 2016
GROUP BY e.firstname, e.lastname, e.employeenumber;
  
SELECT a.*, b.paid_2015, c.clock_hours, round(b.paid_2015/c.clock_hours, 2)
FROM #techs a
LEFT JOIN #pay_2015 b on a.employeenumber = b.employeenumber
LEFT JOIN #clock_hours_2015 c on a.employeenumber = c.employeenumber 
ORDER BY a.departmentkey, a.lastname

SELECT a.*, iif(current_pto_rate > new_pto_rate, 'current', 'new')
FROM (
  SELECT TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
    round(b.paid_2015/c.clock_hours, 2) AS new_pto_rate
  FROM #techs a
  LEFT JOIN #pay_2015 b on a.employeenumber = b.employeenumber
  LEFT JOIN #clock_hours_2015 c on a.employeenumber = c.employeenumber 
  WHERE departmentkey = 13) a
--WHERE iif(current_pto_rate > new_pto_rate, 'current', 'new') = 'current'
ORDER BY a.tech


SELECT a.*, iif(current_pto_rate > new_pto_rate, 'current', 'new')
FROM (
  SELECT TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
    round(b.paid_2015/c.clock_hours, 2) AS new_pto_rate
  FROM #techs a
  LEFT JOIN #pay_2015 b on a.employeenumber = b.employeenumber
  LEFT JOIN #clock_hours_2015 c on a.employeenumber = c.employeenumber 
  WHERE departmentkey = 18) a
ORDER BY a.tech


-- 2/7/116: new pto rate goes INTO effect this pay period
-- IN accordance with ben's new pussy approach to tech pay, IF the new
-- pto rate IS less than current pto rate, don't change it
--
-- also, josh walton (techkey 34) already updated for this pay period AS 
-- part of his techteampercentage UPDATE

SELECT departmentkey, a.employeenumber, TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
  round(b.paid_2016/c.clock_hours, 2) AS new_pto_rate
FROM #techs a
LEFT JOIN #pay_2016 b on a.employeenumber = b.employeenumber
LEFT JOIN #clock_hours_2016 c on a.employeenumber = c.employeenumber 
ORDER by a.departmentkey, a.lastname

-- those techs tag get an increase IN pto rate (TABLE y)
-- this works to accommodate the pussy mode of who gets changed 
SELECT *
FROM (
  SELECT departmentkey, a.employeenumber, TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
    round(b.paid_2016/c.clock_hours, 2) AS new_pto_rate
  FROM #techs a
  LEFT JOIN #pay_2016 b on a.employeenumber = b.employeenumber
  LEFT JOIN #clock_hours_2016 c on a.employeenumber = c.employeenumber) x
LEFT JOIN (
  SELECT departmentkey, a.employeenumber, TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
    round(b.paid_2016/c.clock_hours, 2) AS new_pto_rate
  FROM #techs a
  LEFT JOIN #pay_2016 b on a.employeenumber = b.employeenumber
  LEFT JOIN #clock_hours_2016 c on a.employeenumber = c.employeenumber
  WHERE round(b.paid_2016/c.clock_hours, 2) > a.prev_rate) y on x.employeenumber = y.employeenumber
ORDER by x.departmentkey, x.tech


SELECT a.departmentkey, a.employeenumber, techkey, TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
  round(b.paid_2016/c.clock_hours, 2) AS new_pto_rate
FROM #techs a
INNER JOIN tptechs aa on a.employeenumber = aa.employeenumber
  AND aa.thrudate > curdate()
LEFT JOIN #pay_2016 b on a.employeenumber = b.employeenumber
LEFT JOIN #clock_hours_2016 c on a.employeenumber = c.employeenumber
WHERE round(b.paid_2016/c.clock_hours, 2) > a.prev_rate
ORDER BY a.departmentkey, a.lastname

-- TRY this on 2 techs (42, 51), make sure it ALL works ok 
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @tech_percentage double;
DECLARE @tech_key_cur cursor AS 
  SELECT techkey, round(b.paid_2016/c.clock_hours, 2) AS new_pto_rate, 
    techTeamPercentage
  FROM #techs a
  INNER JOIN tptechs aa on a.employeenumber = aa.employeenumber
    AND aa.thrudate > curdate()
  LEFT JOIN #pay_2016 b on a.employeenumber = b.employeenumber
  LEFT JOIN #clock_hours_2016 c on a.employeenumber = c.employeenumber
  WHERE round(b.paid_2016/c.clock_hours, 2) > a.prev_rate; 
@from_date = '02/05/2017';
@thru_date = @from_date - 1; 
BEGIN TRANSACTION;
TRY 
  OPEN @tech_key_cur;
  WHILE FETCH @tech_key_cur DO 
    @tech_key = @tech_key_cur.techkey;
    @new_pto_rate = @tech_key_cur.new_pto_rate;
    @tech_percentage = @tech_key_cur.techTeamPercentage;
    UPDATE tpTechValues
    SET thrudate = @thru_date 
    WHERE techkey = @tech_key
      AND thrudate > curdate();
    INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
    values (@tech_key, @from_date, @tech_percentage, @new_pto_rate);
    UPDATE tpdata 
    SET techHourlyRate = @new_pto_rate
    WHERE techkey = @tech_key
      AND thedate = @from_date;
  END WHILE;
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  


---------------------------------------------------------------------------------------------------
-- 2/5/17
---------------------------------------------------------------------------------------------------
