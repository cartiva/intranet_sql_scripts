SELECT 
  CASE departmentKey
    WHEN 12 THEN 'RY1'
    WHEN 16 THEN 'RY2'
  END AS store,
  fullname, regularHoursPPTD, otHoursPPTD, lofpptd AS lof, 
  rotatePenetrationPPTD AS rotate, airFilterPenetrationPPTD, 
  nitrogenPenetrationPPTD, alignmentPenetrationPPTD, emailCaptureRate,
  extraCreditPPTD, laborSalesPPTD, totalPointsPPTD
FROM pdqWriterData
WHERE thedate = (
    SELECT MAX(thedate)
    FROM pdqWriterData)
ORDER BY store, fullname    

-- no clock hours for kyle bragunier
fuckety fuck fuck
changed FROM an ry1 employee to an ry2 employee, so tpEmployee, with a pk
of username will NOT accomodate both, no historical context IN tpEmployees
tpEmployees basically becomes sort of a type 2, a username can have 
different employeenumbers over time

so IF i JOIN to tpEmployees on employeenumber (to get what?), i lose history

make the pk username/employeenumber
OR make the primarykey employeenumber, w/ a unique index on username/primarkey
THEN need 2 rows for bragunier

so what does it look LIKE WHEN an employee goes FROM one store to the other


SELECT *
FROM pdqwriterdata
WHERE firstname = 'kyle'

select * FROM tpemployees WHERE lastname = 'bragunier'
select * FROM dds.dimservicewriter WHERE name LIKE '%bragunier%'

-- 3/9 the mantra IS IF a nonkey attribute IS present IN more than one TABLE, 
--     there IS a problem with the model

SELECT employeenumber, lastname
FROM dds.edwEmployeeDim
  WHERE currentrow = true
GROUP BY employeenumber, lastname
HAVING COUNT(*) > 1

-- 3/14
-- base row
  SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, biweeklypayperiodenddate, 
    a.biweeklypayperiodsequence, a.dayinbiweeklypayperiod, a.dayname,
    CASE b.storecode
      WHEN 'RY1' THEN 12
      WHEN 'RY2' THEN 16
    END AS departmentKey, 
    b.serviceWriterKey, b.writerNumber, c.employeenumber,
    c.firstName, c.lastName, trim(c.firstname) + ' ' + c.lastname AS fullName,
    (SELECT username FROM tpEmployees WHERE employeenumber = c.employeenumber)
  FROM dds.day a
  LEFT JOIN dds.dimServiceWriter b on a.thedate BETWEEN b.serviceWriterKeyFromDate AND b.serviceWriterKeyThruDate
    AND b.censusDept = 'QL'
    AND b.active = true
    AND b.employeenumber <> '126425' -- leave out ethan collings
  LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber -- for name only
    AND c.currentrow = true 
  WHERE a.thedate = '03/14/2014'
  

-- so, wtf, edwEmployeeDim.storecode determines which TABLE the writer goes INTO
-- which IS ok, for pay plan, but NOT for store performance  
SELECT a.storecode, a.employeenumber, a.firstname, a.lastname, b.storecode, b.description, 
  b.writernumber
FROM dds.edwEmployeeDim a
INNER JOIN dds.dimServiceWriter b on a.employeenumber = b.employeenumber
  AND b.censusdept = 'ql'
  AND b.currentrow = true
  AND b.active = true
WHERE a.currentrow = true  
  AND a.active = 'active'
ORDER BY b.storecode, b.writernumber

SELECT a.storecode, a.employeenumber, a.firstname, a.lastname, b.*
FROM dds.edwEmployeeDim a
LEFT JOIN tpEmployees b on a.employeenumber = b.employeenumber
WHERE a.currentrow = true
  AND a.employeenumber <> '1148720'
  AND a.active = 'active'
  AND EXISTS (
    SELECT 1
    FROM dds.dimServiceWriter
    WHERE censusdept = 'ql'
      AND currentrow = true
      AND active = true
      AND employeenumber = a.employeenumber)
ORDER BY a.storecode, a.lastname      