/*
-- initial load, only data since 3/9/14
3/28
  remove extraCredit, that IS handled IN the stored procs WHERE extra credit 
  data IS added AND removed
  
4/3/14
  removed NITROINV FROM metric opcodes
    
*/
-- base row
DELETE FROM pdqStoreData;
TRY 
  INSERT INTO pdqStoreData (dateKey, theDate, payPeriodStart, payPeriodEnd, 
    payPeriodSeq, dayOfPayPeriod, dayName, payPeriodSelectFormat, 
    storecode)

SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
  a.biweeklypayperiodsequence, a.dayinbiweeklypayperiod, a.dayname, x.ppsel, 
  b.storecode
FROM dds.day a
LEFT JOIN (
  SELECT 'RY1' AS storeCode
  FROM system.iota
  UNION
  SELECT 'RY2' 
  FROM system.iota) b on 1 = 1        
LEFT JOIN (
  SELECT biweeklypayperiodsequence, TRIM(pt1) + ' ' + TRIM(pt2) AS ppsel
    FROM (  
    SELECT distinct biweeklypayperiodstartdate, biweeklypayperiodenddate, biweeklypayperiodsequence, 
      (SELECT trim(monthname) + ' '
        + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
        FROM dds.day
        WHERE thedate = w.biweeklypayperiodstartdate) AS pt1,
      (SELECT trim(monthname) + ' ' 
        + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
        + TRIM(CAST(theyear AS sql_char))
        FROM dds.day
        WHERE thedate = w.biweeklypayperiodenddate) AS pt2
    FROM dds.day w
    WHERE w.thedate BETWEEN '03/09/2014' AND curdate()) y) x on a.biweeklypayperiodsequence = x.biweeklypayperiodsequence             
WHERE a.thedate BETWEEN '03/09/2014' AND curdate();          
CATCH ALL 
  RAISE pdqStoreData(1, 'Base Row ' + __errtext);
END TRY;  


TRY   
-- lof count per day
  UPDATE pdqStoreData
  SET lofDay = x.lof,
      rotateDay = x.rotate,
	  airFilterDay = x.airFilter,
	  nitrogenDay = x.nitrogen,
	  alignmentDay = x.alignment
  FROM (
    SELECT a.storeCode, b.thedate, 
      SUM(CASE WHEN c.pdqcat1 = 'lof' THEN 1 ELSE 0 END) AS lof,
      SUM(CASE WHEN c.pdqcat1 = 'other' AND pdqcat2 = 'alignment' THEN 1 ELSE 0 END) AS alignment,
      SUM(CASE WHEN c.pdqcat1 = 'other' AND pdqcat2 = 'nitrogen' THEN 1 ELSE 0 END) AS nitrogen,
      SUM(CASE WHEN c.pdqcat1 = 'other' AND pdqcat2 = 'rotate' THEN 1 ELSE 0 END) AS rotate,
      SUM(CASE WHEN c.pdqcat1 = 'other' AND pdqcat2 = 'air filters' THEN 1 ELSE 0 END) AS airFilter
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.closeDateKey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimServiceWriter d on a.serviceWriterKey = d.serviceWriterKey
    WHERE b.thedate BETWEEN '03/09/2014' AND curdate()
      AND c.pdqcat1 <> 'N/A'
      AND d.censusDept = 'QL'
      AND c.opcode <> 'NITROINV'
    GROUP BY a.storeCode, b.thedate ) x  
  WHERE pdqStoreData.theDate = x.theDate
    AND pdqStoreData.storeCode = x.storeCode;           
  -- pptd
  UPDATE pdqStoreData
  SET lofPPTD = x.lof
  FROM (
    SELECT a.thedate, a.storecode, a.payperiodseq, a.lofday, 
      SUM(b.lofday) AS lof
    FROM pdqStoreData a, pdqStoreData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storecode, a.payperiodseq, a.lofday) x
  WHERE pdqStoreData.theDate = x.theDate
    AND pdqStoreData.storeCode = x.storeCode;  
	
  UPDATE pdqStoreData
  SET rotatePPTD = x.rotate
  FROM (
    SELECT a.thedate, a.storecode, a.payperiodseq, a.rotateday, 
      SUM(b.rotateday) AS rotate
    FROM pdqStoreData a, pdqStoreData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storecode, a.payperiodseq, a.rotateday) x
  WHERE pdqStoreData.theDate = x.theDate
    AND pdqStoreData.storeCode = x.storeCode;	
	
  UPDATE pdqStoreData
  SET alignmentPPTD = x.alignment
  FROM (
    SELECT a.thedate, a.storecode, a.payperiodseq, a.alignmentday, 
      SUM(b.alignmentday) AS alignment
    FROM pdqStoreData a, pdqStoreData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storecode, a.payperiodseq, a.alignmentday) x
  WHERE pdqStoreData.theDate = x.theDate
    AND pdqStoreData.storeCode = x.storeCode;		
	
  UPDATE pdqStoreData
  SET airFilterPPTD = x.airFilter
  FROM (
    SELECT a.thedate, a.storecode, a.payperiodseq, a.airFilterDay, 
      SUM(b.airFilterDay) AS airFilter
    FROM pdqStoreData a, pdqStoreData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storecode, a.payperiodseq, a.airFilterDay) x
  WHERE pdqStoreData.theDate = x.theDate
    AND pdqStoreData.storeCode = x.storeCode;		
	
  UPDATE pdqStoreData
  SET nitrogenPPTD = x.nitrogen
  FROM (
    SELECT a.thedate, a.storecode, a.payperiodseq, a.nitrogenDay, 
      SUM(b.nitrogenDay) AS nitrogen
    FROM pdqStoreData a, pdqStoreData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storecode, a.payperiodseq, a.nitrogenDay) x
  WHERE pdqStoreData.theDate = x.theDate
    AND pdqStoreData.storeCode = x.storeCode;	
CATCH ALL 
  RAISE pdqStoreData(2, 'Counts Day & PPTD' + __errtext);
END TRY;       
                
				
-- penetration PPTD
TRY
  UPDATE pdqStoreData
  SET rotatePenetrationPPTD = x.rotate,
      airFilterPenetrationPPTD = x.airfilter,
      nitrogenPenetrationPPTD = x.nitrogen,
      alignmentPenetrationPPTD = x.alignment
  FROM (    
  SELECT thedate, storeCode,
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * rotatePPTD/lofPPTD, 2) 
    END AS rotate,
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * airFilterPPTD/lofPPTD, 2) 
    END AS airfilter,  
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * nitrogenPPTD/lofPPTD, 2) 
    END AS nitrogen,  
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * alignmentPPTD/lofPPTD, 2) 
    END AS alignment    
  FROM pdqStoreData) x
  WHERE pdqStoreData.theDate = x.theDate
    AND pdqStoreData.storeCode = x.storeCode; 
CATCH ALL 
  RAISE pdqStoreData(3, 'Penetration PPTD ' + __errtext);
END TRY; 


TRY
-- email capture
UPDATE pdqStoreData
SET emailCaptureRate = x.email
FROM (
  SELECT thedate, storecode, round(1.0 * valid/theCount, 2) AS email
  FROM (  
    SELECT a.thedate, a.storeCode,
      count(hasValidEmail) AS theCount,  
      sum(coalesce(
        CASE hasValidEmail
          WHEN true THEN 1
          ELSE 0
        END, 0)) AS valid     
    FROM pdqStoreData a    
    LEFT JOIN cstfbEmailData b on a.storeCode = b.storeCode
      AND b.transactionDate BETWEEN a.theDate - 90 AND a.thedate
	  AND b.subDepartment = 'QL'
    GROUP BY a.storeCode, theDate) y	) x  
WHERE pdqStoreData.storeCode = x.storeCode
  AND pdqStoreData.theDate = x.theDate; 	
CATCH ALL 
  RAISE pdqStoreData(4, 'email ' + __errtext);
END TRY; 

-- labor sales
TRY
UPDATE pdqStoreData
SET laborSalesDay = x.laborSales
FROM (
  SELECT a.storecode, b.theDate, SUM(a.laborSales) AS laborSales
  FROM dds.factRepairOrder a
  INNER JOIN dds.day b on a.closeDateKey = b.dateKey
  INNER JOIN dds.dimServiceWriter d on a.serviceWriterKey = d.serviceWriterKey
  WHERE b.thedate BETWEEN '03/09/2014' AND curdate()
    AND d.censusDept = 'QL'
    AND a.serviceTypeKey = (
      SELECT serviceTypeKey
      FROM dds.dimServiceType
      WHERE serviceTypeCode = 'MR')  
    AND a.paymentTypeKey NOT IN (
      SELECT paymentTypeKey 
      FROM dds.dimPaymentType
      WHERE paymentTypeCode IN ('I','W'))
  GROUP BY a.storecode, b.thedate) x
WHERE pdqStoreData.storeCode = x.storeCode
  AND pdqStoreData.theDate = x.theDate; 
  
UPDATE pdqStoreData
SET laborSalesPPTD = x.laborSales
FROM (  
  SELECT a.thedate, a.storeCode, a.payperiodseq, a.laborSalesDay, 
    SUM(b.laborSalesDay) AS laborSales
  FROM pdqStoreData a, pdqStoreData b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.storeCode = b.storeCode
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.storeCode, a.payperiodseq, a.laborSalesDay) x
WHERE pdqStoreData.theDate = x.theDate
  AND pdqStoreData.storeCode = x.storeCode;   
CATCH ALL 
  RAISE pdqStoreData(5, 'labor sales ' + __errtext);
END TRY; 
  
