DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '03/09/2014';
@thruDate = '03/22/2014';
SELECT *
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.flagdateKey = b.datekey
INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
INNER JOIN dds.dimServiceWriter d on a.serviceWriterKey = d.serviceWriterKey
WHERE b.thedate BETWEEN @fromDate AND @thruDate
  AND c.pdqcat1 = 'lof';  
  
  
  

SELECT * FROM dds.dimservicewriter

WHERE pdqcat1 = 'other'
  AND pdqcat2 IN ('alignment','nitrogen','rotate','air filters');

-- basic counts  
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '03/09/2014';
@thruDate = '03/22/2014';
SELECT a.storeCode, b.thedate, 
  SUM(CASE WHEN c.pdqcat1 = 'lof' THEN 1 ELSE 0 END) AS lof,
  SUM(CASE WHEN c.pdqcat1 = 'other' AND pdqcat2 = 'alignment' THEN 1 ELSE 0 END) AS alignment,
  SUM(CASE WHEN c.pdqcat1 = 'other' AND pdqcat2 = 'nitrogen' THEN 1 ELSE 0 END) AS nitrogen,
  SUM(CASE WHEN c.pdqcat1 = 'other' AND pdqcat2 = 'rotate' THEN 1 ELSE 0 END) AS rotate,
  SUM(CASE WHEN c.pdqcat1 = 'other' AND pdqcat2 = 'air filters' THEN 1 ELSE 0 END) AS airFilters
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.closeDateKey = b.datekey
INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
INNER JOIN dds.dimServiceWriter d on a.serviceWriterKey = d.serviceWriterKey
WHERE b.thedate BETWEEN @fromDate AND @thruDate
  AND c.pdqcat1 <> 'N/A'
  AND d.censusDept = 'QL'
GROUP BY a.storeCode, b.thedate  


-- labor sales
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '03/09/2014';
@thruDate = '03/22/2014';
SELECT a.storecode, b.theDate, SUM(a.laborSales) AS laborSales
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.closeDateKey = b.dateKey
INNER JOIN dds.dimServiceWriter d on a.serviceWriterKey = d.serviceWriterKey
WHERE b.thedate BETWEEN @fromDate AND @thruDate
  AND d.censusDept = 'QL'
  AND a.serviceTypeKey = (
    SELECT serviceTypeKey
    FROM dds.dimServiceType
    WHERE serviceTypeCode = 'MR')  
  AND a.paymentTypeKey NOT IN (
    SELECT paymentTypeKey 
    FROM dds.dimPaymentType
    WHERE paymentTypeCode IN ('I','W'))
GROUP BY a.storecode, b.thedate


-- email capture
SELECT storecode, curdate(), round(1.0 * valid/theCount, 2) AS email
FROM (
  SELECT storecode, 
    sum(case when hasValidEmail = true then 1 else 0 end) AS valid,
    COUNT(*) AS theCount
  -- SELECT *
  FROM cstfbEmailData a
  WHERE subdepartment = 'QL'
    AND a.transactionDate BETWEEN curdate() - 90 AND curdate()
  GROUP BY storecode) a  
  