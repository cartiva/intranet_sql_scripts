INSERT INTO applicationmetadata
SELECT *
FROM zzTest.applicationmetadata a
WHERE appcode = 'pdq'
  AND NOT EXISTS (
    SELECT 1
	FROM applicationMetaData
	WHERE appname = a.appname
	  AND appcode = a.appcode
	  AND approle = a.approle
	  AND appseq = a.appseq
	  AND functionality = a.functionality);

INSERT INTO employeeAppAuthorization	  
SELECT *
FROM zzTest.employeeAppAuthorization a
WHERE  NOT EXISTS (
    SELECT 1
	FROM employeeAppAuthorization
	WHERE username = a.username
	  AND appname = a.appname
	  AND appcode = a.appcode
	  AND approle = a.approle
	  AND appseq = a.appseq
	  AND functionality = a.functionality)	  