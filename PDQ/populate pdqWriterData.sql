-- ADD departmentkey: ry1: 12, ry2: 16
--date/writer info
-- fuckit, i want username IN this TABLE, therefore need to populate tpEmployees first
--         see tpEmployees.sql IN this directory
DELETE FROM pdqWriterData;
TRY 
  INSERT INTO pdqWriterData (dateKey, theDate, payPeriodStart, payPeriodEnd, 
    payPeriodSeq, dayOfPayPeriod, dayName, payPeriodSelectFormat, departmentKey,
    serviceWriterKey, serviceWriterNumber, employeeNumber, firstName, lastName,
    fullName, username)
  SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, biweeklypayperiodenddate, 
    a.biweeklypayperiodsequence, a.dayinbiweeklypayperiod, a.dayname, x.ppsel,
    CASE b.storecode
      WHEN 'RY1' THEN 12
      WHEN 'RY2' THEN 16
    END AS departmentKey, 
    b.serviceWriterKey, b.writerNumber, c.employeenumber,
    c.firstName, c.lastName, trim(c.firstname) + ' ' + c.lastname AS fullName,
    (SELECT username FROM tpEmployees WHERE employeenumber = c.employeenumber)
  FROM dds.day a
  LEFT JOIN dds.dimServiceWriter b on a.thedate BETWEEN b.serviceWriterKeyFromDate AND b.serviceWriterKeyThruDate
    AND b.censusDept = 'QL'
    AND b.active = true
    AND b.employeenumber <> '126425' -- leave out ethan collings
  LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber -- for name only
    AND c.currentrow = true 
  LEFT JOIN (
        SELECT payperiodseq, TRIM(pt1) + ' ' + TRIM(pt2) AS ppsel
          FROM (  
          SELECT distinct payperiodstart, payperiodend, payperiodseq, 
            (SELECT trim(monthname) + ' '
              + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
              FROM dds.day
              WHERE thedate = a.payperiodstart) AS pt1,
            (SELECT trim(monthname) + ' ' 
              + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
              + TRIM(CAST(theyear AS sql_char))
              FROM dds.day
              WHERE thedate = a.payperiodend) AS pt2
          FROM tpdata a) y) x on a.biweeklypayperiodsequence = x.payperiodseq  
  WHERE a.thedate > '11/17/2013'
    AND a.thedate < curdate();
CATCH ALL 
  RAISE pdqWriterData(1, 'Base Row ' + __errtext);
END TRY;     

TRY   
-- clock hours day     
  UPDATE pdqWriterData
  SET regularHoursDay = x.reghours,
      otHoursDay = x.ot,
      vacationHoursDay = x.vac,
      ptoHoursDay = x.pto,
      holidayHoursDay = x.hol
  FROM (         
    SELECT e.*, f.reghours, f.ot, f.vac, f.pto, f.hol 
    FROM pdqWriterData e
    LEFT JOIN (
      SELECT a.thedate, a.serviceWriterKey,
        SUM(c.regularhours) AS regHours, SUM(c.overtimehours) AS OT, 
        SUM(c.vacationHours) AS VAC, SUM(c.ptohours) AS PTO, SUM(c.holidayhours) AS HOL 
      FROM pdqWriterData a
      LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber 
        AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
      LEFT JOIN dds.edwClockHoursFact c on a.datekey = c.datekey
        AND b.employeekey = c.employeekey
      GROUP BY a.thedate, a.serviceWriterKey) f 
        on e.thedate = f.thedate 
          AND e.servicewriterkey = f.servicewriterkey) x
  WHERE pdqWriterData.thedate = x.thedate
    AND pdqWriterData.departmentKey = x.departmentKey
    AND pdqWriterData.servicewriterNumber = x.servicewriterNumber;      
        
  UPDATE pdqWriterData
  SET regularHoursPPTD = x.reg
  FROM (
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.regularHoursDay, 
      SUM(b.regularHoursDay) AS reg
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.regularHoursDay) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey; 
    
  UPDATE pdqWriterData
  SET otHoursPPTD = x.ot
  FROM (
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.otHoursDay, 
      SUM(b.otHoursDay) AS ot
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.otHoursDay) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;     
    
  UPDATE pdqWriterData
  SET vacationHoursPPTD = x.vac
  FROM (
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.vacationHoursDay, 
      SUM(b.vacationHoursDay) AS vac
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.vacationHoursDay) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;   
    
  UPDATE pdqWriterData
  SET ptoHoursPPTD = x.pto
  FROM (
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.ptoHoursDay, 
      SUM(b.ptoHoursDay) AS pto
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.ptoHoursDay) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;       
    
  UPDATE pdqWriterData
  SET holidayHoursPPTD = x.hol
  FROM (
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.holidayHoursDay, 
      SUM(b.holidayHoursDay) AS hol
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.holidayHoursDay) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;        
CATCH ALL 
  RAISE pdqWriterData(2, 'ClockHours ' + __errtext);
END TRY;     

TRY   
-- lof count per day
  UPDATE pdqWriterData
  SET lofDay = x.lof
  FROM (
    SELECT coalesce(b.lofs, 0) AS LOF, a.thedate, a.serviceWriterKey 
    FROM pdqWriterData a
    LEFT JOIN (
      SELECT b.thedate, c.serviceWriterKey, COUNT(*) AS LOFs
      FROM dds.factRepairOrder a
      INNER JOIN dds.day b on a.closedatekey = b.datekey
      INNER JOIN dds.dimServiceWriter c on a.serviceWriterKey = c.ServiceWriterKey
      INNER JOIN dds.dimOpcode d on a.opcodeKey = d.opcodeKey
      WHERE b.yearmonth > 201310
        AND c.censusDept = 'QL'
        AND d.pdqcat1 = 'lof'    
      GROUP BY b.thedate, c.serviceWriterKey) b 
        on a.thedate = b.thedate
          AND a.serviceWriterKEy = b.serviceWriterKey) x  
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;         
    
  -- lof pptd
  UPDATE pdqWriterData
  SET lofPPTD = x.lof
  FROM (
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.lofday, 
      SUM(b.lofday) AS lof
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.lofday) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;  
CATCH ALL 
  RAISE pdqWriterData(3, 'LOF ' + __errtext);
END TRY;       
                   
TRY
-- metrics 
  UPDATE pdqWriterData
  SET rotateDay = x.rotates,
      airFilterDay = x.airfilters,
      nitrogenDay = x.nitrogen,
      alignmentDay = x.alignments
  FROM (    
    SELECT e.thedate, e.serviceWriterKey, coalesce(f.airfilter, 0) AS airfilters, 
      coalesce(f.alignment, 0) AS alignments, coalesce(f.nitrogen, 0) AS nitrogen, 
      coalesce(f.rotate, 0) AS rotates
    FROM pdqWriterData e
    LEFT JOIN (
      SELECT b.thedate, a.serviceWriterKey,
        SUM(CASE WHEN pdqcat2 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilter,
        SUM(CASE WHEN pdqcat2 = 'Alignment' THEN 1 ELSE 0 END) AS Alignment,
        SUM(CASE WHEN pdqcat2 = 'Nitrogen' THEN 1 ELSE 0 END) AS Nitrogen,
        SUM(CASE WHEN pdqcat2 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
      FROM dds.factRepairOrder a
      INNER JOIN dds.day b on a.closedatekey = b.datekey
      INNER JOIN dds.dimopcode c on a.opcodekey = c.opcodekey
      INNER JOIN dds.dimServiceWriter d on a.serviceWriterKey = d.serviceWriterKey
      WHERE b.yearmonth > 201310
        AND c.pdqcat1 = 'Other' 
      GROUP BY b.thedate, a.serviceWriterKey) f 
        on e.thedate = f.thedate
         AND e.serviceWriterKey = f.serviceWriterKey) x
    WHERE pdqWriterData.theDate = x.theDate
      AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;          
CATCH ALL 
  RAISE pdqWriterData(4, 'Metrics Day ' + __errtext);
END TRY;                                                                                      

TRY
-- metrics pptd
  UPDATE pdqWriterData
  SET rotatePPTD = x.rotates
  FROM (  
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.rotateDay, 
      SUM(b.rotateDay) AS rotates
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.rotateDay) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;   
    
  UPDATE pdqWriterData
  SET airFilterPPTD = x.airfilters
  FROM (  
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.airFilterDay, 
      SUM(b.airFilterDay) AS airfilters
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.airFilterDay) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;      
    
  UPDATE pdqWriterData
  SET nitrogenPPTD = x.nitrogen
  FROM (  
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.nitrogenDay, 
      SUM(b.nitrogenDay) AS nitrogen
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.nitrogenDay) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;      
    
  UPDATE pdqWriterData
  SET alignmentPPTD = x.alignments
  FROM (  
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.alignmentDay, 
      SUM(b.alignmentDay) AS alignments
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.alignmentDay) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;      
    
  UPDATE pdqWriterData
  SET extraCreditPPTD = x.extraCredit
  FROM (  
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.extraCreditDay, 
      SUM(b.extraCreditDay) AS extraCredit
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.extraCreditDay) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;        
     
CATCH ALL 
  RAISE pdqWriterData(5, 'Metrics PPTD ' + __errtext);
END TRY; 

-- penetration PPTD
TRY
  UPDATE pdqWriterData
  SET rotatePenetrationPPTD = x.rotate,
      airFilterPenetrationPPTD = x.airfilter,
      nitrogenPenetrationPPTD = x.nitrogen,
      alignmentPenetrationPPTD = x.alignment
  FROM (    
  SELECT thedate, serviceWriterKey, 
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * rotatePPTD/lofPPTD, 2) 
    END AS rotate,
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * airFilterPPTD/lofPPTD, 2) 
    END AS airfilter,  
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * nitrogenPPTD/lofPPTD, 2) 
    END AS nitrogen,  
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * alignmentPPTD/lofPPTD, 2) 
    END AS alignment    
  FROM pdqWriterData) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;
CATCH ALL 
  RAISE pdqWriterData(6, 'Penetration PPTD ' + __errtext);
END TRY; 

-- email capture
TRY
-- this IS pretty good, NOT EXACTLY the same AS the email capture page, 
-- pretty fucking close though
UPDATE pdqWriterData
SET emailCaptureRate = x.email
FROM ( 
  SELECT thedate, servicewriterkey, fullname, round(1.0 * valid/theCount, 2) AS email
  FROM (  
    SELECT a.thedate, a.serviceWriterKey, a.fullName, 
      count(coalesce(
        CASE hasValidEmail
          WHEN true THEN 1
          ELSE 0
        END, 0)) AS theCount, 
      sum(coalesce(
        CASE hasValidEmail
          WHEN true THEN 1
          ELSE 0
        END, 0)) AS valid     
    FROM pdqWriterData a    
    LEFT JOIN cstfbEmailData b on a.fullName = b.employeeName
      AND b.storecode = 
        CASE a.departmentKey
          WHEN 12 THEN 'RY1'
          WHEN 16 THEN 'RY2'
        END 
      AND b.transactionDate BETWEEN a.theDate - 90 AND a.thedate
    GROUP BY thedate, serviceWriterKey, fullName) y) x  
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;        
CATCH ALL 
  RAISE pdqWriterData(7, 'email Capture ' + __errtext);
END TRY; 

-- labor sales
TRY
  UPDATE pdqWriterData
  SET laborSalesDay = x.laborSales
  FROM (
    SELECT datekey, a.serviceWriterKey, coalesce(SUM(b.laborSales), 0) AS laborSales
    FROM pdqWriterData a
    LEFT JOIN dds.factRepairOrder b on a.datekey = b.closeDateKey
      AND a.serviceWriterKey = b.serviceWriterKey 
    WHERE b.serviceTypeKey = (
      SELECT serviceTypeKey
      FROM dds.dimServiceType
      WHERE serviceTypeCode = 'MR')  
    AND b.paymentTypeKey NOT IN (
      SELECT paymentTypeKey 
      FROM dds.dimPaymentType
      WHERE paymentTypeCode IN ('I','W'))  
    GROUP BY thedate, datekey, a.serviceWriterKey) x  
    WHERE pdqWriterData.dateKey = x.dateKey
      AND pdqWriterData.serviceWriterKey = x.serviceWriterKey;  
  UPDATE pdqWriterData
  SET laborSalesPPTD = x.laborSales
  FROM (  
    SELECT a.thedate, a.servicewriterkey, a.payperiodseq, a.laborSalesDay, 
      SUM(b.laborSalesDay) AS laborSales
    FROM pdqWriterData a, pdqWriterData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.serviceWriterKey = b.serviceWriterKey
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.servicewriterkey, a.payperiodseq, a.laborSalesDay) x
  WHERE pdqWriterData.theDate = x.theDate
    AND pdqWriterData.serviceWriterKey = x.serviceWriterKey; 
CATCH ALL 
  RAISE pdqWriterData(8, 'Labor Sales ' + __errtext);
END TRY; 

-- points / level
TRY 
UPDATE pdqWriterData
SET 
  rotatePointsPPTD = (
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'rotate' 
      AND rotatePenetrationPPTD BETWEEN minimum AND maximum),
  airFilterPointsPPTD = (    
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'airfilter' 
      AND airFilterPenetrationPPTD BETWEEN minimum AND maximum), 
  nitrogenPointsPPTD = (   
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'nitrogen' 
      AND nitrogenPenetrationPPTD BETWEEN minimum AND maximum),  
  alignmentPointsPPTD = (
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'alignment' 
      AND alignmentPenetrationPPTD BETWEEN minimum AND maximum),   
  extraCreditPointsPPTD = (
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'extraCredit' 
      AND extraCreditPPTD BETWEEN minimum AND maximum), 
  emailCapturePointsPPTD = (    
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'email' 
      AND emailCaptureRate BETWEEN minimum AND maximum);  
      
UPDATE pdqWriterData
SET totalPointsPPTD = rotatePointsPPTD + airFilterPointsPPTD +  nitrogenPointsPPTD +
  alignmentPointsPPTD + extraCreditPointsPPTD + emailCapturePointsPPTD;         

CATCH ALL 
  RAISE pdqWriterData(9, 'Points ' + __errtext);
END TRY; 

-- level/commissionRate pptd
TRY

UPDATE pdqWriterData
SET levelPPTD = (
  SELECT level
  FROM pdqWriterCommissionMatrix
  WHERE totalPointsPPTD BETWEEN minPoints AND maxPoints);

UPDATE pdqWriterData
SET commissionRatePPTD = (
  SELECT commissionRate
  FROM pdqWriterCommissionMatrix
  WHERE totalPointsPPTD BETWEEN minPoints AND maxPoints); 


CATCH ALL 
  RAISE pdqWriterData(10, 'Commission ' + __errtext);
END TRY;