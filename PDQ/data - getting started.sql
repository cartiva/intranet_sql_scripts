SELECT a.storecode, b.thedate, a.ro, c.description, c.writernumber, d.opcode
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.closedatekey = b.datekey
INNER JOIN dds.dimServiceWriter c on a.serviceWriterKey = c.ServiceWriterKey
INNER JOIN dds.dimOpcode d on a.opcodeKey = d.opcodeKey
WHERE b.yearmonth > 201309
  AND c.censusDept = 'QL'
  AND d.pdqcat1 = 'lof'  

-- lof count  
SELECT a.storecode, b.thedate, c.description, c.writernumber, COUNT(*)
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.closedatekey = b.datekey
INNER JOIN dds.dimServiceWriter c on a.serviceWriterKey = c.ServiceWriterKey
INNER JOIN dds.dimOpcode d on a.opcodeKey = d.opcodeKey
WHERE b.yearmonth > 201310
  AND c.censusDept = 'QL'
  AND d.pdqcat1 = 'lof'    
GROUP BY a.storecode, b.thedate, c.description, c.writernumber

-- base row
-- 1 row per day per writer
-- what i really want IS any writer that was "active" during the payperiod
-- DO NOT know IF this gives me that OR NOT, assume so, for now

-- ADD departmentkey: ry1: 12, ry2: 16
DROP TABLE #base;
SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, biweeklypayperiodenddate, 
  a.biweeklypayperiodsequence, a.dayinbiweeklypayperiod, a.dayname, 
  b.storecode, b.serviceWriterKey, b.writerNumber, 
  trim(c.firstname) + ' ' + c.lastname AS name, c.employeenumber
INTO #base
FROM dds.day a
LEFT JOIN dds.dimServiceWriter b on a.thedate BETWEEN b.serviceWriterKeyFromDate AND b.serviceWriterKeyThruDate
  AND b.censusDept = 'QL'
  AND b.active = true
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber -- for name only
  AND c.currentrow = true 
WHERE a.yearmonth > 201310
  AND a.thedate < curdate();
  
-- clock hours      
DROP TABLE #base1;     
SELECT e.*, f.reghours, f.ot, f.vac, f.pto, f.hol 
INTO #base1
FROM #base e
LEFT JOIN (
  SELECT a.thedate, a.serviceWriterKey,
    SUM(c.regularhours) AS regHours, SUM(c.overtimehours) AS OT, 
    SUM(c.vacationHours) AS VAC, SUM(c.ptohours) AS PTO, SUM(c.holidayhours) AS HOL 
  FROM #base a
  LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber 
    AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  LEFT JOIN dds.edwClockHoursFact c on a.datekey = c.datekey
    AND b.employeekey = c.employeekey
  GROUP BY a.thedate, a.serviceWriterKey) f on e.thedate = f.thedate AND e.servicewriterkey = f.servicewriterkey;
  

-- lof count per day
DROP TABLE #base2;  
SELECT a.*, coalesce(b.lofs, 0) AS LOF
INTO #base2 
FROM #base1 a
LEFT JOIN (
  SELECT b.thedate, c.serviceWriterKey, COUNT(*) AS LOFs
  FROM dds.factRepairOrder a
  INNER JOIN dds.day b on a.closedatekey = b.datekey
  INNER JOIN dds.dimServiceWriter c on a.serviceWriterKey = c.ServiceWriterKey
  INNER JOIN dds.dimOpcode d on a.opcodeKey = d.opcodeKey
  WHERE b.yearmonth > 201310
    AND c.censusDept = 'QL'
    AND d.pdqcat1 = 'lof'    
  GROUP BY b.thedate, c.serviceWriterKey) b 
    on a.thedate = b.thedate
      AND a.serviceWriterKEy = b.serviceWriterKey;

-- metrics 
DROP TABLE #base3;
SELECT e.*, coalesce(f.airfilter, 0) AS airfilters, 
  coalesce(f.alignment, 0) AS alignments, coalesce(f.nitrogen, 0) AS nitrogen, 
  coalesce(f.rotate, 0) AS rotates
INTO #base3
FROM #base2 e
LEFT JOIN (
  SELECT b.thedate, a.serviceWriterKey,
    SUM(CASE WHEN pdqcat2 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilter,
    SUM(CASE WHEN pdqcat2 = 'Alignment' THEN 1 ELSE 0 END) AS Alignment,
    SUM(CASE WHEN pdqcat2 = 'Nitrogen' THEN 1 ELSE 0 END) AS Nitrogen,
    SUM(CASE WHEN pdqcat2 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
  FROM dds.factRepairOrder a
  INNER JOIN dds.day b on a.closedatekey = b.datekey
  INNER JOIN dds.dimopcode c on a.opcodekey = c.opcodekey
  INNER JOIN dds.dimServiceWriter d on a.serviceWriterKey = d.serviceWriterKey
  WHERE b.yearmonth > 201310
    AND c.pdqcat1 = 'Other' 
  GROUP BY b.thedate, a.serviceWriterKey) f 
    on e.thedate = f.thedate
     AND e.serviceWriterKey = f.serviceWriterKey
     
SELECT * FROM #base2    