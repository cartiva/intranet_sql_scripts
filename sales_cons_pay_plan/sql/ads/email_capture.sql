SELECT * FROM cstfbemaildata

-- first thoughts
SELECT year(TRANSACTIONdate) * 100 + month(transactiondate), employeename, COUNT(*),
  SUM(CASE hasvalidemail WHEN true THEN 1 ELSE 0 END) AS email, 
  CASE COUNT(*)
    WHEN 0 THEN 0
	ELSE round(SUM(CASE hasvalidemail WHEN true THEN 1.00 ELSE 0.00 END)/COUNT(*) * 1.00, 2)
  END AS email_perc
FROM cstfbemaildata a
WHERE subdepartment IN ('Used', 'New')
  AND transactiondate BETWEEN '11/01/2015' AND curdate()
GROUP BY storecode, year(TRANSACTIONdate) * 100 + month(transactiondate), employeename  

-- 90 day window
SELECT employeename, curdate() - 90, curdate(), COUNT(*),
  SUM(CASE WHEN hasvalidemail THEN 1 ELSE 0 END)  as email_capture,
  round(100.0 * SUM(CASE WHEN hasvalidemail THEN 1 ELSE 0 END)/COUNT(*), 1)
FROM cstfbemaildata a
WHERE a.transactiondate BETWEEN curdate() - 90 AND curdate()
  AND a.transactiontype = 'sale'
GROUP BY employeename, curdate() - 90, curdate() 

-- 2/14/16 Get Real
-- candidate key
SELECT transactionnumber, customername
FROM cstfbemaildata
GROUP BY transactionnumber, customername
HAVING COUNT(*) > 1

-- this doesn't WORK very well
SELECT *
FROM cstfbemaildata a
LEFT JOIN dds.edwEmployeeDim b on a.employeename = TRIM(b.firstname) + ' ' + b.lastname
  AND a.transactiondate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate

-- this IS probably good enuf for now  
-- SELECT transactionnumber, customername FROM (  
-- use this IN scpp_cstfb_email_data.py
SELECT a.transactiondate, a.storecode, a.subdepartment, a.employeename, 
  c.employeenumber,
  a.transactionnumber, a.transactiontype, a.customername, a.emailaddress,
  a.homephone, a.workphone, a.cellphone, a.hasvalidemail
FROM cstfbemaildata a
LEFT JOIN dds.factVehicleSale b on a.transactionnumber = b.stocknumber
  AND b.stocknumber NOT IN ('21008','21081a','23055a','23777c') -- goofy dups FROM factVehicleSale
LEFT JOIN dds.dimSalesPerson c on b.consultantkey = c.salespersonkey
WHERE a.transactiontype = 'sale'  
  AND a.transactionnumber <> ''
-- ) x GROUP BY transactionnumber, customername HAVING COUNT(*) > 1  

  