﻿select distinct stock_number
from scpp.xfm_deals a

-- exclude wholesale accounts
-- base query
select a.account_number, a.account_Desc, b.gtjrnl, b.gtdate, b.gtctl_, b.gttamt, d.*
-- select * 
from dds.ext_glpmast a
inner join dds.ext_glptrns b on a.account_number = b.gtacct
inner join (
  select distinct stock_number
  from scpp.xfm_deals a) c on b.gtctl_ = c.stock_number
inner join scpp.xfm_deals d on c.stock_number = d.stock_number
  and d.seq = (
    select max(seq)
    from scpp.xfm_deals
    where stock_number = d.stock_number)  
-- where d.stock_number in ('26679A','26043c') -- one whls one retail 
where a.year = 2016
  and a.account_type = '4' -- sales 
  and a.department in ('NC','UC')
  and a.account_desc NOt like '%WH%' -- exclude wholesale
  and a.account_sub_type in ('A','B') -- exclude honda
  and b.gtpost <> 'V'
  and b.gtjrnl in ('VSN','VSU')
  and b.gttamt < 0
  and b.gtdate between '01/01/2016' and '03/31/2016'

-- have multiple glptrns entries 
-- #1 eliminate voids - that narrows it down to just 26885
-- and gttamt < 0 gives me 0
-- expand date range to current_date and get 4
select string_agg('''' || stock_number || '''', ',') FROM (
select stock_number from (
select a.account_number, a.account_Desc, b.gtjrnl, b.gtdate, b.gtctl_, b.gttamt, d.*
-- select * 
from dds.ext_glpmast a
inner join dds.ext_glptrns b on a.account_number = b.gtacct
inner join (
  select distinct stock_number
  from scpp.xfm_deals a) c on b.gtctl_ = c.stock_number
inner join scpp.xfm_deals d on c.stock_number = d.stock_number
  and d.seq = (
    select max(seq)
    from scpp.xfm_deals
    where stock_number = d.stock_number)  
-- where d.stock_number in ('26679A','26043c') -- one whls one retail 
where a.year = 2016
  and a.account_type = '4'
  and a.department in ('NC','UC')
  and a.account_desc NOt like '%WH%'
  and b.gtjrnl in ('VSN','VSU')
  and b.gtdate between '01/01/2016' and current_date
  and b.gtpost <> 'V'
  and b.gttamt < 0
) x group by x.stock_number having count(*) > 1
) y


select max(run_date) from scpp.xfm_dealsselect a.account_number, a.account_Desc, b.gtjrnl, b.gtdate, b.gtctl_, b.gttamt, d.*

-- possible problems: multiple rows
select b.*
-- select * 
from dds.ext_glpmast a
inner join dds.ext_glptrns b on a.account_number = b.gtacct
where a.year = 2016
  and a.account_type = '4'
  and a.department in ('NC','UC')
  and a.account_desc NOt like '%WH%'
  and b.gtjrnl in ('VSN','VSU')
  and b.gtdate between '01/01/2016' and current_Date
  and b.gtctl_ in ('27752XA','26332R','27190XX','H7611')
  and b.gtpost <> 'V'
--  and b.gttamt < 0
order by gtctl_, gtseq_

-- wholesale accounts  
select *
from dds.ext_glpmast a
where a.year = 2016
  and a.account_type = '4'
  and a.department in ('NC','UC')
  and account_desc like '%WH%'

-- inventory accounts  -- don't look helpful
select *
from dds.ext_glpmast a
where a.year = 2016
  and a.account_type = '1' -- assets
  and a.department in ('NC','UC')
  and account_desc like '%INV%'
  and a.account_sub_type in ('A','B') -- exclude honda



select a.*, b.account_desc
from dds.ext_glptrns a
inner join dds.ext_glpmast b on a.gtacct = b.account_number
  and b.year = 2016
where gtctl_ = '27994'
order by gtdate, gtacct


-- 26885 all transactions
select b.*
-- select * 
from dds.ext_glpmast a
inner join dds.ext_glptrns b on a.account_number = b.gtacct
where a.year = 2016
  and a.account_type = '4'
  and a.department in ('NC','UC')
  and a.account_desc NOt like '%WH%'
  and b.gtjrnl in ('VSN','VSU')
--  and b.gtdate between '01/01/2016' and current_Date
  and b.gtctl_ = '26885'
--  and b.gtpost <> 'V'
--  and b.gttamt < 0
order by gtctl_, gtseq_  


-- 4/13
-- so i have created a table scpp.ext_glptrns_for_deals
-- left voids and positive amounts in for possible forensics when there are issues
--     FROM  rydedata.glptrns a
--     inner join rydedata.glpmast b on a.gtacct = b.account_number
--     where a.gtdate between '01/01/2016' and current_date
--         and b.year = 2016
--         and b.account_type = '4'
--         and b.department in ('NC','UC')
--         and b.account_desc not like '%WH%'
--         and b.account_sub_type in ('A','B')
--         -- and a.gtpost <> 'V'
--         -- and a.gttamt < 0
--         and a.gtjrnl in ('VSN','VSU')
-- 
-- this will need to be joined to ext_deals to generate a gl_date in xfm     
-- join will need to limit gtdate to date of bopmast table 
-- hmmm, yikes getting ugly
-- 
-- this gives no duplicates:
-- -- at this time ext has only thru bopmast_0131
-- select * 
-- from scpp.ext_Glptrns_for_deals
-- where gtdate < '02/01/16'
-- and gtpost <> 'V'
-- and gttamt < 0
-- AND gtctl_ in (
-- select gtctl_
-- from scpp.ext_glptrns_For_deals
-- where gtdate < '02/01/2016'
--   and gtdesc not like 'C/E%'
--   and gtpost <> 'V'
--   and gttamt < 0
-- group by gtctl_
-- having count(*) > 1)
-- order by gtctl_   

-- test for month date_capped <> month gtdate
-- select 'New', a.run_date, a.store_code, a.stock_number, a.vin,
--   a.customer_name, a.primary_sc, a.secondary_sc, a.record_status,
--   a.date_approved, a.date_Capped, a.origination_date, a.model_year,
--   a.make, a.model,
--   1, 'No Change', 'No Change', b.gtdate as gl_date
-- from scpp.ext_deals a
-- left join scpp.ext_glptrns_for_deals b on a.stock_number = b.gtctl_
--   and b.gtdate < '02/01/2016'
--   and b.gtdesc not like 'C/E%'
-- WHERE not exists (
--     select 1
--     from scpp.xfm_deals
--     where stock_number = a.stock_number)
-- and b.gtdate is not null 
-- and extract(month from date_capped) <> extract(month from gtdate)

greg''s attempt at getting a deal count from glptrns (Z:\E\Intranet SQL Scripts\sales_cons_pay_plan\deal_counts_greg.sql)
was real close: 
case when sum(gttamt) < 0 then 1 else -1 end as unitcount 
grouped by gttrn# & gtseq#

the flaw was, as jeri pointed out to me yesterday, the offsetting entries do not 
necessarily need to zero out (stk# 26332R)

so, instead of the above:
  case when sum(gttamt) < 0 then 1 else -1 end as unitcount 
do:  
  sum(case when gttamt < 0 then 1 else -1 end) as unit_count
so, the route i am going is that if:
-- 4/15 the customer part fell apart: C/E vs customer name
  in the same month
  for the same vehicle
  for the same customer
  there exists both a debit and a credit (normal balance for a sale account is credit, eg, negative)

select *
from scpp.ext_glptrns_for_Deals
where gtpost <> 'V'
  AND gtctl_ in (
    select gtctl_
    from scpp.ext_glptrns_for_Deals
    where gtpost <> 'V'
    group by gtctl_
    having count(*) > 1)
order by gtctl_


select * from scpp.ext_glptrns_for_Deals where gttamt > 0


select min(gtdate), max(gtdate) from scpp.ext_glptrns_for_deals

select * from scpp.ext_glptrns_for_deals where gtctl_ = '27215xx'

observations
  if a vehicle has debit entry (+) only,in a month it is an unwind: H8435
  forget the "customer", what i was thinking of was the account description
    does not work with C/E

-- i am liking this, so let's group it and see what we get    
select gtctl_, gtdate, gtdesc, extract(year from gtdate)::integer as the_year, 
  extract(month from gtdate)::integer as the_month,
  case when gttamt < 0 then 1 else -1 end as unit_count
from scpp.ext_glptrns_For_deals
where gtpost <> 'V'
  AND gtctl_ in (
    select gtctl_
    from scpp.ext_glptrns_for_Deals
    where gtpost <> 'V'
    group by gtctl_
    having count(*) > 1)   
order by gtctl_  

-- uh oh, 27579 shows -1 for april, wrong, think it's because
-- i am including the date in the grouping and should not be
select gtctl_, /*gtdate,*/ extract(year from gtdate) as the_year, 
  extract(month from gtdate) as the_month,
  sum(case when gttamt < 0 then 1 else -1 end) as unit_count
from scpp.ext_glptrns_For_deals
where gtpost <> 'V'
group by gtctl_, /*gtdate,*/ extract(year from gtdate), extract(month from gtdate) 
order by gtctl_

ok, i am good with this giving me the monthly count
but what i need is the date

select a.gtctl_, a.gtdate, a.gtdesc
from scpp.ext_Glptrns_for_deals a
inner join (
  select gtctl_, /*gtdate,*/ extract(year from gtdate) as the_year, 
    extract(month from gtdate) as the_month,
    sum(case when gttamt < 0 then 1 else -1 end) as unit_count
  from scpp.ext_glptrns_For_deals
  where gtpost <> 'V'
  group by gtctl_, /*gtdate,*/ extract(year from gtdate), extract(month from gtdate) ) b on a.gtctl_ = b.gtctl_
    and extract(year from a.gtdate) = b.the_year
    and extract(month from a.gtdate) = b.the_month
    and b.unit_count = 1
where a.gttamt < 0
  and a.gtpost <> 'V'    
order by a.gtctl_  

-- this is close, but there are a few dups
these are all corrections (in the same month) and as such, deal date is the min(gtdate)
execpt H8435, but this one is entries in different month (sold in jan, unw in feb, sold in mar)
and would show up in normal day to day processing
select *
from (
  select a.gtctl_, a.gtdate, a.gtdesc
  from scpp.ext_Glptrns_for_deals a
  inner join (
    select gtctl_, /*gtdate,*/ extract(year from gtdate) as the_year, 
      extract(month from gtdate) as the_month,
      sum(case when gttamt < 0 then 1 else -1 end) as unit_count
    from scpp.ext_glptrns_For_deals
    where gtpost <> 'V'
    group by gtctl_, /*gtdate,*/ extract(year from gtdate), extract(month from gtdate) ) b on a.gtctl_ = b.gtctl_
      and extract(year from a.gtdate) = b.the_year
      and extract(month from a.gtdate) = b.the_month
      and b.unit_count = 1
  where a.gttamt < 0
    and a.gtpost <> 'V') z
where gtctl_ in (      
  select gtctl_ from (
    select a.gtctl_, a.gtdate, a.gtdesc
    from scpp.ext_Glptrns_for_deals a
    inner join (
      select gtctl_, /*gtdate,*/ extract(year from gtdate) as the_year, 
        extract(month from gtdate) as the_month,
        sum(case when gttamt < 0 then 1 else -1 end) as unit_count
      from scpp.ext_glptrns_For_deals
      where gtpost <> 'V'
      group by gtctl_, /*gtdate,*/ extract(year from gtdate), extract(month from gtdate) ) b on a.gtctl_ = b.gtctl_
        and extract(year from a.gtdate) = b.the_year
        and extract(month from a.gtdate) = b.the_month
        and b.unit_count = 1
    where a.gttamt < 0
      and a.gtpost <> 'V') x group by gtctl_ having count(*) > 1)
order by gtctl_, gtdate  


-- so, i think this is it
-- the deal dates!
select a.gtctl_, a.gtdesc, min(a.gtdate) as gtdate
from scpp.ext_Glptrns_for_deals a
inner join (
  select gtctl_, /*gtdate,*/ extract(year from gtdate) as the_year, 
    extract(month from gtdate) as the_month,
    sum(case when gttamt < 0 then 1 else -1 end) as unit_count
  from scpp.ext_glptrns_For_deals
  where gtpost <> 'V'
  group by gtctl_, /*gtdate,*/ extract(year from gtdate), extract(month from gtdate) ) b on a.gtctl_ = b.gtctl_
    and extract(year from a.gtdate) = b.the_year
    and extract(month from a.gtdate) = b.the_month
    and b.unit_count = 1
where a.gttamt < 0
  and a.gtpost <> 'V'    
group by a.gtctl_, a.gtdesc

now it becomes how to integrate this date in the deal processing
at what point in the process do i attach the gl_date
initially, i am thinking as early as possible, which would be extract
that way it is simply avaiable and all subsequent processing can deal
with it as needs be

plus i will have access to the date consistently with ext_ for the
incremental backfilling

shit, except ext_ is against ubuntu not pg, but des insert into pg
so, after writing to ext_
do an update from ext_

hmm, first do a xfm_gltrns: no, just do the join to the above query

select *
from scpp.ext_deals e
left join (
  select a.gtctl_, a.gtdesc, min(a.gtdate) as gtdate
  from scpp.ext_Glptrns_for_deals a
  inner join (
    select gtctl_, extract(year from gtdate) as the_year, 
      extract(month from gtdate) as the_month,
      sum(case when gttamt < 0 then 1 else -1 end) as unit_count
    from scpp.ext_glptrns_For_deals
    where gtpost <> 'V'
    group by gtctl_, extract(year from gtdate), extract(month from gtdate) ) b on a.gtctl_ = b.gtctl_
      and extract(year from a.gtdate) = b.the_year
      and extract(month from a.gtdate) = b.the_month
      and b.unit_count = 1
  where a.gttamt < 0
    and a.gtpost <> 'V' 
    and a.gtdate <= '01/31/2016'   
  group by a.gtctl_, a.gtdesc) f on e.stock_number = f.gtctl_

select a.gtctl_, a.gtdesc, min(a.gtdate) as gtdate
from scpp.ext_Glptrns_for_deals a
inner join (
  select gtctl_, extract(year from gtdate) as the_year, 
    extract(month from gtdate) as the_month,
    sum(case when gttamt < 0 then 1 else -1 end) as unit_count
  from scpp.ext_glptrns_For_deals
  where gtpost <> 'V'
  group by gtctl_, extract(year from gtdate), extract(month from gtdate) ) b on a.gtctl_ = b.gtctl_
    and extract(year from a.gtdate) = b.the_year
    and extract(month from a.gtdate) = b.the_month
    and b.unit_count = 1
where a.gttamt < 0
  and a.gtpost <> 'V' 
  and a.gtdate <= '01/31/2016'   
group by a.gtctl_, a.gtdesc  

-- 4/14 in the pm
well that did not work out so well
27616 does not work
27579 from above
sold on 1/30
unwound on 2/3
resold on 2/5
the above query shows the feb transactions as 1/30
fuck fuck fuck fuck fuck fuck fuck

multiple entries per day
-- only one vehicle with 4 entries on a day: H7453A, 1/5/15
-- 13 with 3
-- 78 with 2
select *
from scpp.ext_glptrns_for_deals a
inner join (
  select gtctl_, gtdate, count(*)
  from scpp.ext_glptrns_for_deals
  where gtpost <> 'V'
  group by gtctl_, gtdate
  having count(*) > 1) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate


select a.gtctl_, a.gtdate, sum(a.gttamt) as gttamt, b.the_Count, gtdesc
from scpp.ext_glptrns_for_deals a
inner join (
  select gtctl_, gtdate, count(*) as the_count
  from scpp.ext_glptrns_for_deals
  where gtpost <> 'V'
  group by gtctl_, gtdate
  having count(*) > 1) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
group by a.gtctl_, a.gtdate, b.the_count, gtdesc having sum(a.gttamt) <> 0

-- here's a goofy one
-- red flag on 12/7, unwound ? and 12/10 unit count = 2?
select * from scpp.ext_Glptrns_for_deals where gtctl_ = 'H8576' and gtpost = 'Y'
--
the real issue, i am processing this stuff on a day by day basis, so i do not know
what will happen tomorrow
have to be able to interpret/decode multiple daily entries

that and the matching of info between bopmast and glptrns

am i mind fucking myself crossing streams between unit count and deal date
-- 
xfm
1 row per entry
select gttrn_, gtseq_, gtctl_, gtdate,
  case 
    when gttamt < 0 then 1
    else -1
  end as unit_count
from scpp.ext_glptrns_For_deals  
where gtpost = 'Y'
  and gtctl_ = '25128R'

so, this looks ok, but i am leary of run_date equaling gtdate
the gtdate could be back dated so on the run_date that a bopmast status changes
the gtdate could be before that
so do i use glptrns for changing the status of deals regardless of a matching
row from bopmast 

so, do i want to go down this path, generate a gl_unit_count for modifying deals

25128R
8/27  sold  +1
8/28  c/e    0  2 entries
9/10  unw?  -1  3 entries  
10/27 sold  +1  e entries

seems like it makes sense


1 row vehicle per day

select gtctl_, gtdate, sum(unit_count)
from (
  select gtctl_, gtdate, -- 7463
    case 
      when gttamt < 0 then 1
      else -1
    end as unit_count
  from scpp.ext_glptrns_For_deals  
  where gtpost = 'Y') a
group by gtctl_, gtdate


select * from scpp.ext_deals

select * from scpp.deals

select *
from scpp.xfm_Deals a
left join (
  select gtctl_, gtdate, sum(unit_count)
  from (
    select gtctl_, gtdate, -- 7463
      case 
        when gttamt < 0 then 1
        else -1
      end as unit_count
    from scpp.ext_glptrns_For_deals  
    where gtpost = 'Y') a
  group by gtctl_, gtdate) b on a.stock_number = b.gtctl_
    and b.gtdate <= a.run_date
where a.run_date = '02/01/2016'


select * 
from scpp.deals a
left join (
  select gtctl_, gtdate, sum(unit_count)
  from (
    select gtctl_, gtdate, -- 7463
      case 
        when gttamt < 0 then 1
        else -1
      end as unit_count
    from scpp.ext_glptrns_For_deals  
    where gtpost = 'Y') a
  group by gtctl_, gtdate) b on a.stock_number = b.gtctl_
    and b.gtdate <= a.run_date
--------------------------------------------------------------------------------------------------------
-- 4/15 ------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
have completely lost my way

fundamental problem to solve: deal date: can not rely on bopmast.date_capped, too often, that date 
reflects the physical entry date by f/i admin, while the gl_date can be back dated in that physical entry
eg date_capped: 4/1/16, gl_date (and actual deal/booking/cap date): 3/31/16

but the problem has come from the fact that there is no definitive indicator in glptrns for capping a deal,
that information is intermixed with re-invoicing, corrections, etc.

the goal is to be able to process the data on a daily basis, backfill simulation of that process is 
utilizing the nightly scrapes of bopmast (cdc) and limiting the glptrns data availalbe using the run date to match

select gtctl_, gttrn_, gtseq_, gtdtyp, gtdate, gtdesc, gttamt 
from scpp.ext_glptrns_for_deals where gtctl_ in ('27616','26885','H8711','26128R','H8576','26332R','27579') order by gtctl_, gttrn_, gtseq_


-- multiple entries vehicle/day
select a.gtctl_, a.gttrn_, a.gtseq_, a.gtdtyp, a.gtdate, a.gtdesc, a.gttamt, b.*
from scpp.ext_glptrns_For_deals a
inner join (
  select gtctl_, gtdate, count(*)
  from scpp.ext_glptrns_for_deals
  where gtpost = 'Y'
  group by gtctl_, gtdate
  having count(*) > 1) b on a.gtctl_ = b.gtctl_

-- oh shit, in processing do i limit glptrns to gtdate = to run date
-- no, unfortunately, can not do due to back dating of transactions
-- so is it the most recent transaction <= run date?
-- limit to stock_numbers rows for run date from xfm?
-- but what about those deals that only change in glptrns
-- sounding more and more like a xfm_glptrs table - new rows and changed rows - pk date/stock unit count could change as back dated entries are made
on a daily basis, i only care about those deals with multiple transactions that result in a gl_count balance

select a.gtctl_, a.gttrn_, a.gtseq_, a.gtdtyp, a.gtdate, a.gtdesc, a.gttamt
from scpp.ext_glptrns_For_deals a
where gtctl_ = 'H8072'

select a.gtctl_, a.gttrn_, a.gtseq_, a.gtdtyp, a.gtdate, a.gtdesc, a.gttamt, 
  case 
    when gttamt < 0 then 1
    else -1
  end as gl_unit_count
from scpp.ext_glptrns_for_Deals a
order by a.gtctl_


select gtctl_, gtdate, sum(unit_count)
from ( 
  select gtctl_, gtdate, 
    case 
      when gttamt < 0 then 1
      else -1
    end as unit_count
  from scpp.ext_glptrns_For_deals  
  where gtpost = 'Y'
    and gtctl_ in (
      select gtctl_
      from scpp.ext_glptrns_for_deals
      where gtpost = 'Y'
      group by gtctl_, gtdate
      having count(*) > 1)) c
group by gtctl_, gtdate having sum(unit_count) > 0   
order by gtctl_
  

  select gtctl_, gtdate, 
    case 
      when gttamt < 0 then 1
      else -1
    end as unit_count
  from scpp.ext_glptrns_For_deals  

select string_agg(''''||gtctl_||'''', ',')
from(
select gtctl_, gtdate
from (
  select a.gtctl_, a.gtdate, unit_count
  from ( -- deal/date with more than one entry/day - 93 rows
    select gtctl_, gtdate
    from scpp.ext_glptrns_for_deals
    where gtpost = 'Y'
    group by gtctl_, gtdate
    having count(*) > 1)  a
  left join ( -- 1 row for each transaction
    select gtctl_, gtdate, 
      case 
        when gttamt < 0 then 1
        else -1
      end as unit_count
    from scpp.ext_glptrns_For_deals) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate) c
group by gtctl_, gtdate having sum(unit_count) > 0) x   


select gtctl_, gttrn_, gtseq_, gtdtyp, gtdate, gtdesc, gttamt 
from scpp.ext_glptrns_for_deals 
where gtctl_ in ('23670A','23768','24038B','24040B','24449P','24843X','25007','25128R','26036','26196','26342','26355','27084','27174','27345','H8072','H8576') 
order by gtctl_, gttrn_, gtseq_

-- i think i am close but have confused myself again with what i am looking at

on any given day, there is a single unit count god damnit

-- all rows with unit_count based on polarity of gttamt
-- exclude voids fucker
select gtctl_, gtdate, 
  case 
    when gttamt < 0 then 1
    else -1
  end as unit_count
from scpp.ext_glptrns_for_deals 
where gtpost = 'Y'
order by gtctl_

-- one row per vehicle & day
-- my concern with this is the situation where, eg,
-- sold   5/21
-- unwind AND resell to different person 6/4
-- this query would show a net unit count of 0 on 6/4
-- part of me is saying oh well, it will not be perfect

select gtctl_, gtdate, 
  sum(case when gttamt < 0 then 1 else -1 end) as unit_count
from scpp.ext_glptrns_for_deals 
where gtpost = 'Y'
group by gtctl_, gtdate
order by gtctl_

-- so lets look at those 0 count examples
-- first there are only 76 of them
-- none of them appear to be unwinds and sale to someone else
-- ie fuck it, rare enuf anomaly that it need not be integrated in normal processing
-- but then, what are the anomalies that need to be flagged?
select a.gtctl_, a.gttrn_, a.gtseq_, a.gtdtyp, a.gtdate, a.gtdesc, a.gttamt 
from scpp.ext_glptrns_for_deals a
inner join (
  select gtctl_, gtdate, 
    sum(case when gttamt < 0 then 1 else -1 end) as unit_count
  from scpp.ext_glptrns_for_deals 
  where gtpost = 'Y'
  group by gtctl_, gtdate having sum(case when gttamt < 0 then 1 else -1 end) = 0) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
order by a.gtctl_

-- but then, what are the anomalies that need to be flagged?
-- this is the only one that comes to mind
-- these appear to be multiple sales or unwinds of the same vehicle on the same day
-- only 3 examples
-- if i know me, i'll wait for the RI to blow up or the end user to blow up
select a.gtctl_, a.gttrn_, a.gtseq_, a.gtdtyp, a.gtdate, a.gtdesc, a.gttamt 
-- select *
from scpp.ext_glptrns_for_deals a
inner join (
  select gtctl_, gtdate, 
    sum(case when gttamt < 0 then 1 else -1 end) as unit_count
  from scpp.ext_glptrns_for_deals 
  where gtpost = 'Y'
  group by gtctl_, gtdate having sum(case when gttamt < 0 then 1 else -1 end) > 1) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
order by a.gtctl_

going mind fuck on historical 2015 data
initial scrape was based on origination_date being > 20150000
so for those, the gl_count should be based on the latest date with a non zero sum(unit_count)?
is that my problem - not differentiating between historical backfill and real time ?

that and do i use gl_count as a basis for changing deal status? - i believe so, in real time
eg, on 3/31 bopmast shows accepted and no change, but gl shows capped

select * from scpp.ext_glptrns_for_deals where gtctl_ = '23786B'
select * from scpp.deals where stock_number = '23786B'
select * from scpp.xfm_deals where stock_number = '23786B'

so, i am tempted to not fuck with the historical data, simply go with the bopmast data
fuck the history, for the first load gl_date is null 

holy shit, do i maintain gl xfm history like for bopmast i do not want to
each day, period - thats it, what is the most recent gl_date and gl_count for each unit
oh shit, just on new units and changed units?
any way, go ahead and start over, blow out gl_date update in ext_deals for initial load

getting a bit ahead of myself 
before a fucking start blowing anything out, get back to formulating the appropriate query from gl


the xfm_deals new rows - with matching gl rows
the xfm_deals update rows with matching gl rows
the gl new rows
the gl update rows
do not know about all that
lets start with the xfm_glptrns first


select * from scpp.ext_glptrns_for_deals order by gtdate desc 

this might be stupid, but i am putting all the glptrns stuff in ext_deals.py for now for 
the simplicity of using the daily cuts to populate history
i will leave the existing 0131 shit in place and start with 0201

delete from scpp.xfm_deals where run_date > '01/31/2016';
delete from scpp.deals where run_date > '01/31/2016';
delete from scpp.ext_glptrns_for_deals where gtdate > '01/31/2016';

drop table if exists scpp.xfm_glptrns;
create table if not exists scpp.xfm_glptrns (
  row_type citext,
  stock_number citext not null,
  gl_date date not null,
  gl_count integer,
  run_date date not null,
  constraint xfm_glptrns_pk primary key (stock_number, gl_date));
-- initial load
insert into scpp.xfm_glptrns (row_type,stock_number,gl_date,gl_count,run_date)
select 'New',gtctl_, gtdate, 
  sum(case when gttamt < 0 then 1 else -1 end) as unit_count,
  '01/31/2016'
from scpp.ext_glptrns_for_deals 
where gtpost = 'Y'
group by gtctl_, gtdate;

insert into scpp.xfm_glptrns (row_type,stock_number,gl_date,gl_count,run_date)
select 'New',gtctl_, gtdate, 
  sum(case when gttamt < 0 then 1 else -1 end) as unit_count,
  '02/01/2016'
from scpp.ext_glptrns_for_deals a
where gtpost = 'Y'
  AND not exists (
    select 1
    from scpp.xfm_glptrns
    where stock_number = a.gtctl_
      and gl_date = a.gtdate)
group by gtctl_, gtdate;      

select *
from (
  select gtctl_, gtdate, 
    sum(case when gttamt < 0 then 1 else -1 end) as unit_count
  from scpp.ext_glptrns_for_deals a
  where gtpost = 'Y'
  group by gtctl_, gtdate) a      
inner join scpp.xfm_glptrns b on a.gtctl_ = b.stock_number and a.gtdate = b.gl_date and a.unit_count <> b.gl_count

select * from scpp.ext_glptrns_For_deals order by gtdate desc

select * from scpp.xfm_glptrns


alter table scpp.xfm_deals
add column gl_count integer;
-- xfm_deals.py
-- new rows
select 'New', a.run_date, a.store_code, a.stock_number, a.vin,
  a.customer_name, a.primary_sc, a.secondary_sc, a.record_status,
  a.date_approved, a.date_Capped, a.origination_date, a.model_year,
  a.make, a.model,
  1, 'No Change', 'No Change', b.gl_date, b.gl_count
from scpp.ext_deals a
left join scpp.xfm_glptrns b on a.run_date = b.run_date and a.stock_number = b.stock_number
WHERE not exists (
    select 1
    from scpp.xfm_deals
    where stock_number = a.stock_number);

-- xfm_deals.py
-- update rows, 
-- this where where we get kinky and need to include the gl data in the determination of changes
-- ie, if we have a gl_date we have a deal date
-- what we are tracking here is changes from ext compared to xfm
-- might not be too bad, just add gl_count to xfm_deals
-- yikes the stickler is in the logic that determines which deals get added as having changed
currently a row gets added for update if 
1. sc has changed
2. record_status has changed
add 3. if ext_.record_status <> 'U' and xfm_.gl_date is not null

-- xfm_deals_for_update.py
-- now i'm really fucking nervous
-- here is where the logic could get goofy

alter table scpp.xfm_deals_for_update
ADD column gl_year_month integer,
ADD column gl_count integer;
-- current logic
-- (pared down xfm_deals_for_update: just primary joins and logic)
select *
from (
  select *
  from scpp.xfm_deals 
  where run_date = (select max(xd.run_date) from scpp.xfm_Deals xd)
    and row_type = 'update') x
inner join scpp.deals y on x.stock_number = y.stock_number
  and (x.record_status <> y.deal_status or x.status_change = 'deleted')
  and y.seq = (
    select max(g.seq)
    from scpp.deals g
    where g.stock_number = y.stock_number) 
WHERE not ((x.record_status = 'Accepted' or x.record_status = 'None') and y.deal_status = 'Deleted')
  -- already busted down to status None, no need to process Delete in this case
  and not (x.status_change = 'Deleted' and y.deal_status = 'None')
  and x.status_change not in ('No Change', 'None to Accepted', 'Accepted to None')  
-- EX1
  and not (x.status_change = 'Accepted to Capped' and y.deal_status = 'Capped' and y.deal_date = x.gl_date)

-- 4/16 continuing, 
-- 0202
-- fucking 27379, deleted but shows gldate of 2/20
-- realized that due to the original cut of glptrns which was thru current date
-- there are xfm_deals rows with a gldate > 02/02
select * from scpp.xfm_deals where gl_date > '02/02/2016'
fuck it, lets hammer it and keep moving
update scpp.xfm_Deals -- only 6 rows
set gl_date = null,
    gl_count = null
where gl_date > '02/02/2016'

-- 0203
-- now how the fuck did xfm_glptrns get rows with gl_date > 02/03
-- with fucking run_dates of 02/01
-- same fucking thing, i think, from the original ext_glptrns thru 4/13
-- so
delete from scpp.xfm_glptrns where gl_date > '02/03/2016' 

select * from scpp.deals where stock_number = '27616'

select * from scpp.xfm_glptrns where gl_date > '02/03/2016'

select * from scpp.ext_glptrns_for_deals order by gtdate desc limit 100

-- fuck me start all over, no back fill
-- delete everything, start with 0201
delete from scpp.ext_glptrns_for_deals;
delete from scpp.xfm_glptrns;
delete from scpp.ext_deals;
delete from scpp.xfm_deals;
delete from scpp.xfm_deals_for_update;
delete from scpp.deals;

-- back to xfm logic
add 3. if ext_.record_status <> 'U' and xfm_.gl_date is not null

this does not work

27616: unwound on 2/3 rewound on 2/5 gl does not chnage until 2/9 ??
have to look at that, but
the logic creates a row in xfr_deals every day between 2/3 and 2/9 because 
record_status <> U and there is a gl_date (1/30) of when it was initially capped

what that logic was trying to capture was using the gl_date to trigger a capped status

that might work if run_date = gl_date and gl_count = 1

roll everything back to 2/3 and see - check out what is happening on 2/5, should be capped then

delete from scpp.xfm_glptrns where run_date > '02/01/2016';
delete from scpp.xfm_deals where run_date > '02/01/2016';
delete from scpp.deals where run_date > '02/01/2016';

here is the motherfucking problem, 27616 was reposted (capped) on 2/9, but the gldate is 2/5
and of course, bopmast shows date_capped as 2/9
fuck me how do i handle that, i am hoping this is somewhat of a rarity


-- oh boy , yet another fuckup
xfm_deals.py
  new rows get gl data by a join ext_deals to xfm_glptrns on stock# and run_date
  update rows get the gl data from the OLD FUCKING XFM ROW, instead should be from a 
    join to ext_deals just like new rows
done    

--???????????!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
what about no status change, but gl_date changes?  this feels like the deal where
the deal is capped, no status change, but sc changes, and now, date changes
hmmm, does the gl_date change from the existing gl_date
very possibly
can not really tell because xfm_ does not pick up deals where the only thing that changes is gl_date

alter table scpp.xfm_deals
add column gl_date_change boolean default False
add to xfm logic
OR c.gl_date <> b.gl_date


-- feb looks pretty good
-- on to march