﻿-- what i want to do is to turn the total pay calculation into a function
-- that way it can be fucking maintained in one place

do 
$$
declare 
  _employee_number citext;
  _year_month integer;
  _table citext;
begin
_employee_number := '17172';
_year_month := 201607;
_table = 'tmp';
drop table if exists _x;
create temporary table _x as
select year_month, employee_number, full_name, unit_count_x_per_unit, total_pay,
  case
    when unit_count_x_per_unit > total_pay then unit_count_x_per_unit
    else total_pay
  end as actual_pay
from (  
  select a.year_month, a.employee_number, a.full_name, a.unit_count_x_per_unit,
    case
      when unit_count_x_per_unit > guarantee then
        unit_count_x_per_unit + pto_pay + additional_comp
      else
        case
          when unit_count_x_per_unit + pto_pay > guarantee then
            unit_count_x_per_unit + pto_pay + additional_comp
          else -- guarantee
            case
              when pto_pay = 0 and pto_hours = 0 then
              -- this is the new hire pro-rate branch
                case
                  when full_months_employment <> 0 then
                    -- not first month of employment :: straight guarantee
                    guarantee - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
                  else
                    case
                      when hire_date <= b.sc_first_working_day_of_month then
                        -- started on or before 1st working day of month :: straight guarantee
                        guarantee - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
                    else
                      -- pro-rated guarantee
                      (guarantee * (
                        select count(*)
                        from dds.day
                        where thedate between a.hire_date and b.last_of_month
                          and dayofweek between 2 and 6
                          and holiday = false)/b.sc_working_days)
                      - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
                    end
                end
              -- no pto due but has pto hours
              when pto_pay = 0 and pto_hours <> 0 then
                (round((sc_working_days - pto_hours/8) * 1.0/sc_working_days, 4) * guarantee) -
                  (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) +
                  additional_comp
              else -- pto_pay <> 0
                round(
                  (round((sc_working_days - pto_hours/8) * 1.0/sc_working_days, 4) * guarantee) +
                    pto_hours * pto_rate -
                    (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) +
                    additional_comp, 2)
            end
          end
      end as total_pay
  from 
    case
      when _table = 'tmp' then scpp.tmp_s_c_data
      else scpp.sales_consultant_data a
    end
  inner join scpp.months b on a.year_month = b.year_month
    and b.year_month = _year_month
  where 
    case _employee_number
      when '1' then 1 = 1
      else a.employee_number = _employee_number
    end
    and 
      case _year_month
        when 666 then b.year_month = (select year_month from scpp.months where open_closed = 'open')
        else b.year_month = _year_month
      end) x;    
end
$$;

select * from _x;

 