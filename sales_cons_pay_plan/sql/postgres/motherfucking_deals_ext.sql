﻿-- the issue is dup stocknumbers
-- 26408C  jon:+ 1
-- select * from scpp.xfm_deals where stock_number = '26408c'  
-- This one is a bitch, started on 3/3 as record_key 34533
-- accepted on 3/8 as record_key 34610: but shows up as deleted because it is excluded from extract
--   because of 2 rows for stock# 26408C
-- capped on 3/9, but missed for the same reason
-- both rows persist in bopmast until 4/3 at which time only 34610 exists 

-- current query (that resulted in the above omission)
  --select '01/31/2016'::date as run_date, bopmast_company_number,
  select '%s', bopmast_company_number,
    bopmast_stock_number,
    bopmast_vin, bopmast_search_name,
    coalesce(primary_salespers, 'None'),
    coalesce(secondary_slspers2, 'None'),
    coalesce(record_status, 'None'), date_approved, date_capped, origination_date,
    coalesce(b.year::citext, 'UNK'), coalesce(b.make, 'UNK'),
    coalesce(b.model, 'UNK')
  --from test.ext_bopmast_0131 a
  from %s
  left join dds.ext_inpmast b on a.bopmast_stock_number = b.inpmast_stock_number
  where origination_date > 20150000
    and sale_type <> 'W'
    and bopmast_vin is not null
      and bopmast_stock_number in (
        select bopmast_stock_number
        --from test.ext_bopmast_0131
        from %s
        group by bopmast_stock_number
        having count(*) = 1)

select * from test.ext_bopmast_0310 where bopmast_stock_number = '26408c'

-- ok, here are all the dups     
  select record_key, bopmast_company_number,
    bopmast_stock_number,
    bopmast_vin, bopmast_search_name,
    coalesce(primary_salespers, 'None'),
    coalesce(secondary_slspers2, 'None'),
    coalesce(record_status, 'None'), date_approved, date_capped, origination_date,
    coalesce(b.year::citext, 'UNK'), coalesce(b.make, 'UNK'),
    coalesce(b.model, 'UNK')
  from test.ext_bopmast_0310 a
  left join dds.ext_inpmast b on a.bopmast_stock_number = b.inpmast_stock_number
  where origination_date > 20150000
    and sale_type <> 'W'
    and bopmast_vin is not null
      and bopmast_stock_number in (
        select bopmast_stock_number
        from test.ext_bopmast_0310
        group by bopmast_stock_number
        having count(*) > 1) 
order by bopmast_stock_number       

-- at this point i am thinking it is a union of the above query (no dups)
-- with a sanitized query of the dups: remove all rows with null record status
select *
from (
  select bopmast_company_number,
    bopmast_stock_number,
    bopmast_vin, bopmast_search_name,
    coalesce(primary_salespers, 'None'),
    coalesce(secondary_slspers2, 'None'),
    coalesce(record_status, 'None') as record_status, date_approved, date_capped, origination_date,
    coalesce(b.year::citext, 'UNK'), coalesce(b.make, 'UNK'),
    coalesce(b.model, 'UNK')
  from test.ext_bopmast_0310 a
  left join dds.ext_inpmast b on a.bopmast_stock_number = b.inpmast_stock_number
  where origination_date > 20150000
    and sale_type <> 'W'
    and bopmast_vin is not null
      and bopmast_stock_number in (
        select bopmast_stock_number
        from test.ext_bopmast_0310
        group by bopmast_stock_number
        having count(*) > 1)) z
where record_status <> 'none'
-- !! it works (or at least seems to)

                select bopmast_company_number,
                  bopmast_stock_number,
                  bopmast_vin, bopmast_search_name,
                  coalesce(primary_salespers, 'None'),
                  coalesce(secondary_slspers2, 'None'),
                  coalesce(record_status, 'None'), date_approved, date_capped, origination_date,
                  coalesce(b.year::citext, 'UNK'), coalesce(b.make, 'UNK'),
                  coalesce(b.model, 'UNK')
                from test.ext_bopmast_0310 a
                left join dds.ext_inpmast b on a.bopmast_stock_number = b.inpmast_stock_number
                where origination_date > 20150000
                  and sale_type <> 'W'
                  and bopmast_vin is not null
                    and bopmast_stock_number in (
                      select bopmast_stock_number
                      from test.ext_bopmast_0310
                      group by bopmast_stock_number
                      having count(*) = 1)
union 
select *
from (
  select bopmast_company_number,
    bopmast_stock_number,
    bopmast_vin, bopmast_search_name,
    coalesce(primary_salespers, 'None'),
    coalesce(secondary_slspers2, 'None'),
    coalesce(record_status, 'None') as record_status, date_approved, date_capped, origination_date,
    coalesce(b.year::citext, 'UNK'), coalesce(b.make, 'UNK'),
    coalesce(b.model, 'UNK')
  from test.ext_bopmast_0310 a
  left join dds.ext_inpmast b on a.bopmast_stock_number = b.inpmast_stock_number
  where origination_date > 20150000
    and sale_type <> 'W'
    and bopmast_vin is not null
      and bopmast_stock_number in (
        select bopmast_stock_number
        from test.ext_bopmast_0310
        group by bopmast_stock_number
        having count(*) > 1)) z
where record_status <> 'none'   
order by bopmast_stock_number                  


select bopmast_stock_number
from scpp.ext_deals
group by bopmast_stock_number
having count(*) > 1

-- 4/14 adding gl_date
alter table scpp.ext_deals
add column gl_date date;     

-- and the query to update
update scpp.ext_deals x
set gl_date = y.gtdate
from (
  select a.gtctl_, a.gtdesc, min(a.gtdate) as gtdate
  from scpp.ext_Glptrns_for_deals a
  inner join (
    select gtctl_, extract(year from gtdate) as the_year, 
      extract(month from gtdate) as the_month,
      sum(case when gttamt < 0 then 1 else -1 end) as unit_count
    from scpp.ext_glptrns_For_deals
    where gtpost <> 'V'
    group by gtctl_, extract(year from gtdate), extract(month from gtdate) ) b on a.gtctl_ = b.gtctl_
      and extract(year from a.gtdate) = b.the_year
      and extract(month from a.gtdate) = b.the_month
      and b.unit_count = 1
  where a.gttamt < 0
    and a.gtpost <> 'V' 
    and a.gtdate <= '01/31/2016'   
  group by a.gtctl_, a.gtdesc) y
where x.stock_number = y.gtctl_      

select * from scpp.ext_deals where date_capped <> gl_date order by origination_date desc

-- 4/15 
see all the angst in motherfucking accounting
i am dispensing with gl_date for the initial load of history
and going to implement it only on a daily basis