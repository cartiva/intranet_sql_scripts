﻿select full_name, employee_number, max(years_service) as years_service, sum(unit_count) 
from scpp.sales_Consultant_data a
where year_month < 201612
  and years_service > 1
  and exists (
    select 1
    from scpp.sales_consultant_data
    where employee_number = a.employee_number
      and year_month = 201612)
group by full_name, employee_number

-- 2016 (12/2/16)

-- december 2015 deals from arkona
create temp table arkona as
select '1112410' as employee_number, 12 as unit_count
union
select '189100' as employee_number, 14 as unit_count
union
select '161325' as employee_number, 17 as unit_count
union
select '123425' as employee_number, 13 as unit_count
union
select '128530' as employee_number, 21 as unit_count
union
select '1135518' as employee_number, 8 as unit_count
union
select '1147250' as employee_number, 19 as unit_count
union
select '1130690' as employee_number, 15 as unit_count
union
select '1132420' as employee_number, 51.5 as unit_count
union
select '148080' as employee_number, 18 as unit_count
union
select '1109815' as employee_number, 24 as unit_count
union
select '145840' as employee_number, 15 as unit_count
union
select '124120' as employee_number, 12.5 as unit_count;

ss

select * from scpp.deals where year_month = 201611 and employee_number = '189100' order by stock_number

select * from scpp.xfm_deals where stock_number = '29609a'
