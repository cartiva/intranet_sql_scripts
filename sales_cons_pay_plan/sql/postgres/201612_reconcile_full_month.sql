﻿end of year fiasco

the first thing i need is to get updated qualifiers from kd

if unit count < 11, qualifying does not matter, 
unit count = 10 qualified per unit = 2300 
which equals the guarantee

exclude flaat

select a.full_name, a.employee_number,
  a.csi_score, a.csi_qualified, 
  a.logged_score, a.logged_qualified,
  a.auto_alert_score, a.auto_alert_qualified, 
  a.unit_count, a.additional_comp, a.unit_count_x_per_unit, a.total_pay, b.*
from scpp.sales_consultant_data a
left join scpp.admin_notes b on a.employee_number = b.employee_number
  and a.year_month = b.year_month
where a.year_month = 201612
  and a.metrics_qualified = false  
  and a.unit_count > 10
  and a.employee_number <> '145840'
order by a.full_name  


-- what has been paid in december, commission and draw only
select e.full_name, e.employee_number, e.year_month, e.unit_count, e.total_pay as vision_total_pay, 
  f.arkona_comm_draw, f.arkona_total_gross, f.cat
from scpp.sales_consultant_data e
left join (
  select m.*, n.arkona_comm_draw, n.cat
  from ( -- pyhshdta
    select a.employee_, check_month, 201600 + check_month as year_month, sum(a.total_gross_pay) as arkona_total_gross
    from dds.ext_pyhshdta a
    where trim(a.distrib_code) = 'SALE'
      and a.payroll_cen_year = 116
      and a.payroll_ending_month = 12
--       and a.check_month = 12
      and a.company_number = 'RY1'
    group by a.employee_, check_month) m
  left join ( -- pyhscdta
    select employee_, check_month, 
      sum(case when q.code_id in ('79','78') then amount end) as arkona_comm_draw,
      string_agg(q.description || ':' || q.amount::citext, ' | ' order by q.description) as cat
    from ( 
      select a.employee_name, a.employee_, a.check_month, b.description, sum(b.amount) as amount, b.code_id
      --select distinct c.description
      from dds.ext_pyhshdta a
      inner join dds.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
        and a.company_number = b.company_number
        and a.employee_ = b.employee_number
        and b.code_type in ('0','1') -- 0,1: income, 2: deduction
        and b.code_id in ('79','78')
      where trim(a.distrib_code) = 'SALE'
        and a.payroll_cen_year = 116
      and a.payroll_ending_month = 12
--       and a.check_month = 12
        and a.company_number = 'RY1'
      group by a.employee_name, a.employee_, a.check_month, b.description, b.code_id) q
    group by  employee_, check_month) n on m.employee_ = n.employee_ and m.check_month = n.check_month) f on e.employee_number = f.employee_
      and e.year_month = f.year_month
where e.year_month = 201612
  and f.year_month is not null
order by e.full_name, e.year_month




select * from admin_notes