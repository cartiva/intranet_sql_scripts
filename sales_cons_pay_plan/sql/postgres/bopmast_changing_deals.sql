﻿-- -- -- capped deals in xfm (thru 1/31) disappeared from ext (thru 2/29)
-- -- -- h8435 comes back when it is resold, don't know wtf happened to it
-- -- -- 24162XX: at some point the stock number got deleted from the bopmast record
-- -- -- and it was not scraped because the stocknumber was null FFFUUUCCCKKK
-- -- -- for now, keep the not null in place, drive off the issue bridge when we get to it
-- -- select * 
-- -- from scpp.xfm_deals a  
-- -- where record_status = 'U'
-- --   AND not exists (
-- --     select *
-- --     from scpp.ext_deals
-- --     where stock_number = a.stock_number)
-- --  At this stage in the game, a disappearing stock number is the sign of an anomaly
-- --    not some normal change of status


-- even tho 5250 shows capped on 1/30, unwound on 2/3, 
-- accepted on 2/4 & 2/5, capped on 2/9
-- bopmast date capped does not change until 2/9, so date gets 
-- changed when re-capped by not removed in the interim

select date_capped
from test.ext_bopmast_0201
where bopmast_stock_number = '27616'

select date_capped
from test.ext_bopmast_0208
where bopmast_stock_number = '27616'

select date_capped
from test.ext_bopmast_0210
where bopmast_stock_number = '27616'

-- let's see what else changes
-- aha, record status changes * date_approved, possibly perfect
select *
from (
  select '0201', a.*
  from test.ext_bopmast_0201 a
  where bopmast_stock_number = '27616'
  union
  select '0208', b.*
  from test.ext_bopmast_0208 b
  where bopmast_stock_number = '27616'
  union
  select '0210', c.*
  from test.ext_bopmast_0210 c
  where bopmast_stock_number = '27616') x





select date_capped
from test.ext_bopmast_0201
where bopmast_stock_number = '27373'


select date_capped
from test.ext_bopmast_0223
where bopmast_stock_number = '27373'


-- 27307xx accpt: 3/19, cap: 3/21, unw: 3/22
select *
from (
  select '0319' as XYZ, record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0319
  where bopmast_stock_number = '27307XX'
  union
  select '0321', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0321
  where bopmast_stock_number = '27307XX'
  union
  select '0322', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0322
  where bopmast_stock_number = '27307XX'
  union
  select '0401', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0401
  where bopmast_stock_number = '27307XX') x
 order by XYZ 



-- 27616 acp: 1/30, cap: 1/30, unw: 2/3, acp: 2/4, acp: 2/5, cap: 2/9

select *
from (
  select '0201' as XYZ, record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0201
  where bopmast_stock_number = '27616'
  union
  select '0203', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0203
  where bopmast_stock_number = '27616'
  union
  select '0204', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0204
  where bopmast_stock_number = '27616'  
  union
  select '0205', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0205
  where bopmast_stock_number = '27616'    
  union
  select '0209', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0209
  where bopmast_stock_number = '27616') x
 order by XYZ 



select '02/29/2016'::date as run_date, bopmast_company_number, 
  bopmast_stock_number, 
  bopmast_vin, bopmast_search_name,
  coalesce(primary_salespers, 'None'),
  coalesce(secondary_slspers2, 'None'),  
  record_status, date_approved, date_capped, origination_date 
from test.ext_bopmast_0229
where origination_date > 20150000
  and sale_type <> 'W'
  and bopmast_stock_number is null
  and bopmast_vin is not null 

-- H8435 -- unwinds 2/5, disappears from bopmast on 2/10
-- ac: 1/22, cap: 1/26, unw: 2/5, ac: 3/7, cap: 3/11
select *
from (
  select '0122' as XYZ, record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0122
  where bopmast_stock_number = 'H8435'
  union
  select '0126', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0126
  where bopmast_stock_number = 'H8435'
  union
  select '0205', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0205
  where bopmast_stock_number = 'H8435'  
  union
  select '0206', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0206
  where bopmast_stock_number = 'H8435'    
  union
  select '0209', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0209
  where bopmast_stock_number = 'H8435'   
  union
  select '0210', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0210
  where bopmast_stock_number = 'H8435'   
  union
  select '0307', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0307
  where bopmast_stock_number = 'H8435' 
  union 
  select '0311', record_status, date_approved, date_capped, origination_date
  from test.ext_bopmast_0311
  where bopmast_stock_number = 'H8435') x
 order by XYZ   


  