﻿close january

update scpp.sales_consultant_data
set complete = true
where year_month = 201602;

select * from scpp.months where open_closed = 'open'

select scpp.open_new_month();

/*
restore to jan
update scpp.months set open_closed = 'closed' where year_month = 201602;
update scpp.months set open_closed = 'open' where year_month = 201601;
delete from scpp.per_unit_matrix where year_month = 201602;
delete from scpp.guarantee_matrix where year_month = 201602;
delete from scpp.metrics where year_month = 201602;
delete from scpp.pto_intervals where year_month = 201602;
delete from scpp.sales_consultant_data where year_month = 201602;
delete from scpp.sales_consultant_configuration where year_month = 201602;
delete from scpp.pay_plans where year_month = 201602;
delete from scpp.sales_consultants where extract(year from  hire_date) = 2016;

update scpp.months set open_closed = 'closed' where year_month = 201603;
delete from scpp.per_unit_matrix where year_month = 201603;
delete from scpp.guarantee_matrix where year_month = 201603;
delete from scpp.metrics where year_month = 201603;
delete from scpp.pto_intervals where year_month = 201603;
delete from scpp.sales_consultant_data where year_month = 201603;
delete from scpp.sales_consultant_configuration where year_month = 201603;
delete from scpp.pay_plans where year_month = 201603;

update scpp.months set open_closed = 'closed' where year_month <> 201601;
update scpp.months set open_closed = 'open' where year_month = 201601;
delete from scpp.per_unit_matrix where year_month <> 201601;
delete from scpp.guarantee_matrix where year_month <> 201601;
delete from scpp.metrics where year_month <> 201601;
delete from scpp.pto_intervals where year_month <> 201601;
delete from scpp.pto_hours where year_month <> 201601;
delete from scpp.sales_consultant_data where year_month <> 201601;
delete from scpp.sales_consultant_configuration where year_month <> 201601;
delete from scpp.pay_plans where year_month <> 201601;
delete from scpp.sales_consultants where extract(year from  hire_date) = 2016;

update scpp.sales_consultant_data x
set unit_count = z.unit_count
from (
  select employee_number, year_month, sum(unit_count) as unit_count
  from scpp.deals
  where year_month = 201601
  group by employee_number, year_month) z
where x.employee_number = z.employee_number
  and x.year_month = z.year_month;


select * from scpp.months where open_closed = 'open'

select * from open_new_month()

-- delete from scpp.sales_Consultants where employee_number in ('137220','1106223','174021')        
*/
--4/18  close april open march
-- restore to marck
select * from scpp.months where open_closed = 'open'

delete from scpp.xfm_glptrns where run_date > '03/31/2016';
delete from scpp.xfm_deals where run_date > '03/31/2016'; 
delete from scpp.sales_consultant_data where year_month > 201603;
delete from scpp.pto_hours where year_month > 201603;
delete from scpp.pto_intervals where year_month > 201603;
delete from scpp.guarantee_individual where year_month > 201603;
delete from scpp.deals where year_month > 201603;
delete from scpp.admin_notes where year_month > 201603;
delete from scpp.sales_consultant_configuration where year_month > 201603;
delete from scpp.per_unit_matrix where year_month > 201603;
delete from scpp.guarantee_matrix where year_month > 201603;
delete from scpp.pay_plans where year_month > 201603;
delete FROM scpp.metrics where year_month > 201603;
update scpp.months set open_closed = 'closed' where year_month <> 201603;
update scpp.months set open_closed = 'open' where year_month = 201603;

select * from scpp.months where open_closed = 'open'

select * from scpp.open_new_month()

delete 
from scpp.sales_consultant_data
where employee_number in (
  select employee_number
  from scpp.sales_consultants a
  where a.store_code = 'RY2');

delete 
from scpp.pto_intervals
where employee_number in (
  select employee_number
  from scpp.sales_consultants a
  where a.store_code = 'RY2');  

delete 
from scpp.sales_consultant_configuration
where employee_number in (
  select employee_number
  from scpp.sales_consultants a
  where a.store_code = 'RY2');  

delete 
from scpp.sales_consultants a
  where a.store_code = 'RY2'  

  
  
select* from scpp.months where open_closed = 'open'

select * from scpp.sales_consultants where extract(year from  hire_date) = 2016

select * from scpp.sales_consultant_data

step thru february

run scripts:
  updates table sales_consultants
    ext_sales_consultants.py
    xfm_sales_consultants.py
    load_sales_consultants.py

  

select * from scpp.months where open_closed = 'open'  

select * from scpp.sales_consultant_configuration

select * from scpp.sales_consultants

-- 4/8/16 a clean start (make January 2016 the open month)
--< get rid of ry2 consultants -------------------------------------<
delete 
-- select *
from scpp.pto_intervals
where employee_number in (
  select employee_number
  from scpp.sales_consultants
  where store_code = 'RY2');

delete 
-- select *
from scpp.deals
where employee_number in (
  select employee_number
  from scpp.sales_consultants
  where store_code = 'RY2');  

delete 
-- select *
from scpp.sales_consultant_configuration
where employee_number in (
  select employee_number
  from scpp.sales_consultants
  where store_code = 'RY2');  
  
delete 
-- select *
from scpp.sales_consultants
where employee_number in (
  select employee_number
  from scpp.sales_consultants
  where store_code = 'RY2');  
--/> get rid of ry2 consultants -----------------------------------/>
--< get rid of post january hires ----------------------------------<
delete from scpp.sales_consultant_data where employee_number in ( 
delete from scpp.sales_consultant_configuration where employee_number in ( 
select employee_number from scpp.sales_consultants where hire_date > '01/31/2016')
delete from scpp.sales_consultants where hire_date > '01/31/2016'
--/> get rid of post january hires --------------------------------/>
-- add wendi wheeler  
insert into scpp.sales_consultant_configuration (employee_number,year_month,
  pay_plan_name,draw,full_months_employment,years_service)
select a.employee_number, b.year_month, 'Standard', 500,
  case
    when b.seq - c.seq = 0 then 0
    else b.seq - c.seq - 1
  end as full_months_employed,
	(b.last_of_month - a.hire_date)/365 as years_service
from scpp.sales_consultants a
left join scpp.months b on 1 = 1
  and b.year_month = 201601
 left join scpp.months c on a.hire_date between c.first_of_month and c.last_of_month
where a.full_name = 'Wendi Wheeler';  

insert into scpp.sales_consultant_data (year_month,store_code,full_name,employee_number,
  hire_date,pay_plan_name,draw,draw_paid,full_months_employment,years_service,
  guarantee,store_name)
select 201601, b.store_code, b.full_name, a.employee_number, b.hire_Date, 
  a.pay_plan_name, a.draw, 0 as draw_paid, aa.full_months_employment, 
  aa.years_service, 
case 
  when aa.full_months_employment > 3 then d.guarantee
  else d.guarantee_new_hire
end as guarantee,
case b.store_code
  when 'RY1' then 'GM'
  when 'RY2' then 'Honda Nissan'
end as store_name
-- select *
from scpp.sales_consultant_configuration a
inner join scpp.sales_consultant_configuration aa on a.employee_number = aa.employee_number
  and aa.year_month = 201601
inner join scpp.sales_consultants b on a.employee_number = b.employee_number    
inner join scpp.pay_plans c on c.year_month = 201601
inner join scpp.guarantee_matrix d on c.year_month = d.year_month
  and c.pay_plan_name = d.pay_plan_name
where a.employee_number = '1148583'

PTO_INTERVALS missing from/thru dates
delete from scpp.pto_intervals
ALTER TABLE scpp.pto_intervals DROP COLUMN from_year_month, DROP COLUMN thru_year_month;

alter table scpp.per_unit_matrix
rename column map to tier_1;
alter table scpp.per_unit_matrix
rename column tar to tier_2;

run:  ext_phshdta.py
      xfm_pto_intervals.py
      load_pto_intervals.py
      ext_pto_hours
      load_pto_hours

update scpp.sales_consultant_data 
set metrics_qualified = false,
    csi_Score = 0,
    csi_qualified = false,
    email_score = 0,
    email_qualified = false,
    logged_score = 0,
    logged_qualified = false,
    auto_alert_score = 0,
    auto_alert_qualified = false,
    unit_count_ovr = 0,
    additional_comp = 0;

run:  ext_email_capture
      load_email_capture
      ext_deals
      xfm_deals
      load_deals

--< total pay ------------------------------------------------------------------------------<
    update scpp.sales_consultant_data w
    set total_pay = x.total_pay
    from (
      select a.year_month, a.employee_number,
        case
          when unit_count_x_per_unit > guarantee then
            unit_count_x_per_unit + pto_pay + additional_comp
          else
            case
              when unit_count_x_per_unit + pto_pay > guarantee then
                unit_count_x_per_unit + pto_pay + additional_comp
              else -- guarantee
                case
                  when pto_pay = 0 and pto_hours = 0 then
                    guarantee - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
                  -- no pto due but has pto hours
                  when pto_pay = 0 and pto_hours <> 0 then
                    (round((sc_working_days - pto_hours/8) * 1.0/sc_working_days, 4) * guarantee) -
                      (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + 
                      additional_comp
                  else -- pto_pay <> 0
                    round(
                      (round((sc_working_days - pto_hours/8) * 1.0/sc_working_days, 4) * guarantee) +
                        pto_hours * pto_rate -
                        (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) +
                        additional_comp, 2)
                end
              end
          end as total_pay    
      from scpp.sales_consultant_data a
      inner join scpp.months b on a.year_month = b.year_month
        and b.open_closed = 'open') x
    where w.year_month = x.year_month
      and w.employee_number = x.employee_number    



    update scpp.sales_consultant_data set pto_pay = round(pto_rate * pto_hours, 2)

    -- ********************************** --

    the other question, not comfortable with how pto pay being updated perhaps
    update_sales_consultant_data.py
    1st query: updates sales_consultant_data.pto_rate
    no commit yet
    2nd query: updates sales_consultant_data.pto_pay based on value of rate in s_c_data
    -- the flaat question
    -- pto hours = 120
    -- pto rate = 34.34
    -- pto pay = 4120.80
    -- units = 2
    -- per unit = 200
    -- guarantee = 2300
    -- working days in month = 20
    -- 
    -- what should the total pay be?
    -- 
    -- $4520.80 : pto pay + units * per unit
    -- 
    -- or 
    -- 
    -- $4695.80 : ((working days - pto days)/working days) * guarantee + pto pay

--/> total pay -----------------------------------------------------------------------------/>  

ok, january should be ok, do open new month and start running the scripts

update scpp.sales_consultant_data set complete = true;

select * from scpp.open_new_month()

select * from scpp.months where open_closed = 'open'
select * from scpp.sales_Consultant_data where year_month = 201602;
select * from scpp.sales_Consultant_configuration where year_month = 201602;
select count(*) from scpp.sales_Consultant_configuration where year_month = 201602; -- 26
select count(*) from scpp.sales_Consultant_data where year_month = 201602; -- 26
after ext/xfm/load_s_c
select count(*) from scpp.sales_Consultant_configuration where year_month = 201602; -- 29
select count(*) from scpp.sales_Consultant_data where year_month = 201602; -- 29
select * from scpp.pto_intervals where year_month = 201602;
select * from scpp.pto_hours where year_month = 201602;

select * from scpp.open_new_month()

4/10

0220 load_ failing with Key (year_month, employee_number)=(101, 17190) is not present in table "sales_consultant_configuration".

the deal that is failing is 27273, see the script motherfucking_deals_xfm
the processing skipped the fact that 27273 unwound on 2/16 (pankratz) and disappeared from
bopmast, now it is back as an accepted deal (carpenter) and the script is trying
to process it has having changed from capped (pankratz) to accepted, which is not
quite reality

other suspected fucked up unwinds: 26902D, 25628

why did 25838B get updated? does not appear that it should have

xfm_deals gets anything that has changed period, maybe go back to categorize those changes?
question becomes one of defining what goes into scpp.deals
any capped deal
any deal that was capped that goes to accepted or none