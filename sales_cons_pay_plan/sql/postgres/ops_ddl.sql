﻿drop table if exists ops.tasks;
create table if not exists ops.tasks (
  task citext,
  frequency citext not null,
  constraint tasks_pk primary key (task));

drop table if exists ops.task_dependencies;
create table if not exists ops.task_dependencies (
  predecessor citext not null references ops.tasks (task),
  successor citext not null references ops.tasks (task),
  constraint task_dependencies_pk primary key (predecessor,successor));

drop table if exists ops.task_log;
create table if not exists ops.task_log (
  run serial unique,
  task citext not null references ops.tasks (task),
  run_date date not null,
  run_start_ts timestamptz not null,
  run_complete_ts timestamptz,
  run_status citext not null,
  message citext,
  constraint task_log_pk primary key (task,run_date,run_start_ts));

insert into ops.tasks (task, frequency)
values 
  ('ext_pdptdet', 'Daily'),
  ('ext_pdpphdr', 'Daily'),
  ('ext_pdppdet', 'Daily'),
  ('ext_pdpmast', 'Daily'),
  ('ext_sypffxmst_cdc', 'Daily'),
  ('ext_glpmast_cdc', 'Daily'),
  ('ext_ffpxrefdta_cdc', 'Daily'),
  ('ext_bopmast_cdc', 'Daily'),
  ('ext_glptrns_cdc', 'Daily'),
  ('ext_glptrns_for_deals', 'Daily'),
  ('xfm_deals_for_update', 'Daily'),
  ('ext_sales_consultants','Daily'), 
  ('xfm_sales_consultants','Daily'),  
  ('load_sales_consultants','Daily'),  
  ('ext_pyhshdta','Daily'), 
  ('xfm_pto_intervals','Daily'), 
  ('load_pto_intervals','Daily'), 
  ('ext_pto_hours','Daily'),   
  ('load_pto_hours', 'Daily'),
  ('ext_email_capture','Daily'),     
  ('ext_deals','Daily'), 
  ('xfm_deals', 'Daily'),
  ('load_deals', 'Daily'),  
  ('update_sales_consultant_data', 'Daily'); 
-- 4/15
delete from ops.task_log where task = 'ext_glptrns_for_deals';
delete from ops.tasks where task = 'ext_glptrns_for_deals';


delete from ops.task_Dependencies where predecessor = 'xfm_deals' and successor = 'load_deals'   
insert into ops.task_dependencies (predecessor, successor)
values
  ('ext_sales_consultants','xfm_sales_consultants'),
  ('xfm_sales_consultants','load_sales_consultants'),  
  ('ext_pyhshdta','xfm_pto_intervals'),
  ('load_sales_consultants','xfm_pto_intervals'),
  ('xfm_pto_intervals','load_pto_intervals'),
  ('ext_pto_hours','load_pto_hours'),
  ('ext_glptrns_for_deals', 'ext_deals'),  
  ('ext_deals','xfm_deals'),  
  ('xfm_deals','xfm_deals_for_update'),
  ('xfm_deals_for_update','load_deals'),  
  ('load_sales_consultants','update_sales_consultant_data'),
  ('load_pto_intervals','update_sales_consultant_data'),
  ('load_pto_hours','update_sales_consultant_data'),
  ('ext_email_capture','update_sales_consultant_data'),
  ('load_deals','update_sales_consultant_data');
-- 4/15
delete from ops.task_Dependencies where predecessor = 'ext_glptrns_for_deals';

delete from ops.task_Dependencies where successor = 'load_email_capture';
update ops.task_dependencies set predecessor = 'ext_email_capture'
  where predecessor = 'load_email_capture';
delete from ops.tasks where task = 'load_email_capture';

select *
-- delete
from ops.task_log  

any predecessor for this task that does not have a log entry for run_date where run_complete_ts is not null
  and is Finshed/Pass
-- these are the predecessors for no tasks
select * 
from ops.tasks a
where not exists (
  select 1
  from ops.task_dependencies
  where predecessor = a.task)
-- these are all top level tasks, no dependencies
select * from ops.tasks a
where not exists (
  select 1 from ops.task_dependencies where successor = a.task)

select count(*) 
-- select *
from ops.task_dependencies a
where a.successor = 'xfm_pto_intervals'
  and not exists (
    select 1 
    from ops.task_log
    where task = a.predecessor
      and run_date = current_date
      and run_complete_ts is not null
      and run_status = 'Finished')







 