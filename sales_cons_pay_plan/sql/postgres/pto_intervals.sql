﻿-- pto_intervals
/*
  insert into scpp.pto_intervals
  select f.employee_number, _opened_month,
    (select year_month 
      from scpp.months
      WHERE seq = (
        select seq - 12
        from scpp.months
        where f.most_recent_anniv between first_of_month and last_of_month)) as pto_period_from,
    (select year_month 
      from scpp.months
      WHERE seq = (
        select seq -1
        from scpp.months
        where f.most_recent_anniv between first_of_month and last_of_month)) as pto_period_thru  
  from (
    select a.employee_number, 
      (select max(thedate)
        from dds.day
        where monthofyear = extract(month from c.hire_Date)
          and dayofmonth = extract(day from c.hire_date)
          and thedate < (select first_of_month from scpp.months where year_month = _opened_month)) as most_recent_anniv     
    from scpp.sales_consultant_configuration a
    inner join scpp.months b on a.year_month = b.year_month
      and b.year_month = _closed_month
    inner join scpp.sales_consultants c on a.employee_number = c.employee_number
      and c.term_date > b.last_of_month 
    left join scpp.months d on 1 = 1 
      and d.year_month = _opened_month 
    left join scpp.months e on c.hire_Date between e.first_of_month and e.last_of_month) f;
*/

-- this looks good
-- when hire_date = most_recent_anniversary: no pto due, therefor rate = 0
with open_month as (
  select *
  from scpp.months
  where open_closed = 'open')
--  where year_month = 201601)
select f.employee_number, 
  (select year_month from open_month), f.most_recent_anniv, f.hire_date,
  (select first_of_month 
    from scpp.months
    WHERE seq = (
      select seq - 12
      from scpp.months
      where f.most_recent_anniv between first_of_month and last_of_month)) as pto_period_from,
  (select last_of_month
    from scpp.months
    WHERE seq = (
      select seq -1
      from scpp.months
      where f.most_recent_anniv between first_of_month and last_of_month)) as pto_period_thru        
from (        
  select a.employee_number, x.most_recent_anniv, c.hire_date 
  from scpp.sales_consultant_configuration a
  inner join scpp.sales_consultants c on a.employee_number = c.employee_number
    and c.term_date > (select last_of_month from open_month)
  left join scpp.months d on 1 = 1 
    and d.year_month = (select year_month from open_month)
  left join scpp.months e on c.hire_Date between e.first_of_month and e.last_of_month
  left join  lateral (
    select max(thedate) as most_recent_anniv
    from dds.day
    where monthofyear = extract(month from c.hire_Date)
      and dayofmonth = extract(day from c.hire_date)
      and thedate < (select first_of_month from scpp.months where year_month = (select year_month from open_month))) x on 1 =1
  where a.year_month = (select year_month from open_month)) f 
where not exists (
  select 1
  from scpp.pto_intervals
  where employee_number = f.employee_number  
    and year_month = (select year_month from open_month))
