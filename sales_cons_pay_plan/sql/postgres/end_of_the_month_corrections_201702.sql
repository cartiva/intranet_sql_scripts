﻿-- checked and ok (sc_changes_on_deals.sql)
-- 2/21/17 missed 29340
--   unwound 1/13/17
--   capped to new customer 2/3/17
--   why does this not pick it up
--   i believe xfm_deals_for_update miss it because, if that is not run every day
--   which, it really is not, the selection is limited :
--     where a.run_date = (select max(xd.run_date) from scpp.xfm_deals xd) 
--   the run date for 29340 is 2/3/17, so if not run until 2/4, it missed it
--   ok, that is fucked up, but should stop being an issue once i fucking get the pipeline going
why did this script not pick it up?
because the query on deals is limited to current month, and most recent 29340 row in deals is 201701
/*
2/26 and this is even worse, 29805B, already sold and unw in feb by foster, then resold in feb by mulhern
literally stumbled across this one, the unw was already in the Already Verified list.
i would never have caught the mulhern sale
so why the fuck did it not pick up 29805b
it is in xfm_deals
but not in scpp.xfm_deals_for_update
wy dat?

2 possible approaches: make the population of xfm_deals_for_update better
  or make the test better
  
Theory 1: in xfm deals:
  sc_change: PSC from FOS to MUL
  status_change: No Change 
  gl_date: 2/21 which is different than deals.run_date
  gl_date_change = t
  date_capped = 2/21 which is different than deals.run_date


xfm_deals_for_updates filter: 
  x: scpp.xfm_deals where:
    run_date = max(run_date) from xfm_deals, ie most recent run date only
    row_type = update
    seq = max(seq) for the stock_number
    consultant exists in scpp.sales_consultants and is not olderbak or garceau
  y: scpp.deals inner join where: 
    deal_status <> x.record_status or x.status_change = deleted
    seq = max(seq) for the stock_number
WHERE:
  1. NOT((x.record_status = 'Accepted' or x.record_status = 'None') and y.deal_status = 'Deleted')
  2. AND NOT(x.status_change = 'Deleted' and y.deal_status = 'None')
  3. AND x.status_change not in ('No Change', 'None to Accepted', 'Accepted to None')
  4. AND NOT(x.status_change = 'Accepted to Capped' and y.deal_status = 'Capped' and y.deal_date = x.gl_date)
  5. AND NOT(x.status_change = 'Capped to Accepted' and y.deal_status = 'Deleted')

                      X               Y
record/deal status  Capped          Deleted
gl/deal date        2/21/17         2/10/17            
status_change       No Change

#3 is the one that eliminates 29085B: x.status_change = No Change
ok, good enuf for now, just fix it manually

insert into scpp.deals
select 201702, (select employee_number from scpp.sales_consultants where last_name = 'mulhern'),
  stock_number, 1.0, run_date, customer_name, model_year, make, model, 'Dealertrack',
  3, null::citext, 'Capped', run_date
from scpp.xfm_deals
where stock_number = '29805b'
  and seq = 5
  
this situation will have to be addressed in the refactoring for the new pay plan/fact_vehicle_sale

shit, same thing with 29897B, sold and unw by Seay, sold by eden, eden sale does not show up,
oops, not yet, deal not capped yet
*/   
 
select *
from (
  select r.*, 
    case s.secondary_sc
      when 'None' then t.full_name || ':' || 1.0::citext
      else t.full_name || ':' || 0.5::citext || ',' || u.full_name || ':' || 0.5::citext
    end as xfm_deals
  from ( -- r
    select a.stock_number, string_agg(b.full_name || ':' || unit_count::citext, ',') as deals
    from scpp.deals a
    inner join scpp.sales_consultants b on a.employee_number = b.employee_number
    where year_month = 201702
    group by a.stock_number) r
  left join scpp.xfm_deals s on r.stock_number = s.stock_number
    and s.seq = (
      select max(seq)
      from scpp.xfm_deals
      where stock_number = s.stock_number)  
  left join scpp.sales_consultants t on 
    case 
      when s.store_code = 'RY1' then s.primary_sc = t.ry1_id
      when s.store_code = 'RY2' then s.primary_sc = t.ry2_id 
    end 
  left join scpp.sales_consultants u on 
    case 
      when s.store_code = 'RY1' then s.secondary_sc = u.ry1_id
      when s.store_code = 'RY2' then s.secondary_sc = u.ry2_id 
    end) z
where deals <> xfm_deals   
order by stock_number


-- this one is the persistent stocknumber disappeared from bopmast pain in the ass
-- that until i fix that issue has to be fixed every fucking day

28873A, 28108, 28511: all have disappeared their stocknumber
as of 2/17/17 these are filtered out in scpp\xfm_deals.py
no longer any need to do the cleanup
-- select *
-- from scpp.deals
-- where stock_number = '28873a'
-- 
-- select *
-- from scpp.xfm_deals
-- where stock_number = '28873a'
-- 
-- select *
-- from scpp.deals
-- where stock_number = '28108'
-- 
-- select *
-- from scpp.xfm_deals
-- where stock_number = '28108'
-- 
-- delete from scpp.xfm_deals where stock_number = '28108' and seq = 3;
-- delete from scpp.deals where stock_number = '28108' and seq = 2;
-- delete from scpp.xfm_deals where stock_number = '28873A' and seq = 3;
-- delete from scpp.deals where stock_number = '28873A' and seq = 2;

Already Verified:
28747A: sold & unw feb seay
29318B: sold & unw feb, craig croaker
29391C: sold & unw feb, nicholas bellmore, sold feb loven
29465: sold & unw feb bellmore
29653: sold & unw feb craig croaker
29719: ok, croaker/pearson
29805B: sold & unw feb, sam foster, sold in feb by mulhern
29897B: sold & unw feb, bryn seay, resold by eden in feb, not capped
29908: sold jan unw feb  tanner trosen
29910XXA: .5 croaker, .5 warmack
30375: sold feb, psc changed from trosen to eden after capped
30040XXB: sold dec, unw jan, jesse chavez
30064XXB: sold & unw feb sam foster, sold dockendorf
30264XXA: .5 dockendorf, .5 janzen
30516: .5 haley .5 stadstad
H8507B: sold jan unw feb eden

-- select * from scpp.xfm_deals where stock_number = '30375'
-- select * from scpp.deals where stock_number = '30375'
-- select * from scpp.sales_consultants where last_name in ('trosen','eden')
-- update scpp.deals set employee_number = '137220' where stock_number = '30375' 

select * from scpp.xfm_deals where stock_number = '29719'
select * from scpp.deals where stock_number = '29719'

