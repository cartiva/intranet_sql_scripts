﻿-- checked and ok (sc_changes_on_deals.sql)

select *
from (
  select r.*, 
    case s.secondary_sc
      when 'None' then t.full_name || ':' || 1.0::citext
      else t.full_name || ':' || 0.5::citext || ',' || u.full_name || ':' || 0.5::citext
    end as xfm_deals
  from (
    select a.stock_number, string_agg(b.full_name || ':' || unit_count::citext, ',') as deals
    from scpp.deals a
    inner join scpp.sales_consultants b on a.employee_number = b.employee_number
    where year_month = 201612
    group by a.stock_number) r
  left join scpp.xfm_deals s on r.stock_number = s.stock_number
    and s.seq = (
      select max(seq)
      from scpp.xfm_deals
      where stock_number = s.stock_number)  
  left join scpp.sales_consultants t on 
    case 
      when s.store_code = 'RY1' then s.primary_sc = t.ry1_id
      when s.store_code = 'RY2' then s.primary_sc = t.ry2_id 
    end 
  left join scpp.sales_consultants u on 
    case 
      when s.store_code = 'RY1' then s.secondary_sc = u.ry1_id
      when s.store_code = 'RY2' then s.secondary_sc = u.ry2_id 
    end) z
where deals <> xfm_deals   
order by stock_number

-- this one is the persistent stocknumber disappeared from bopmast pain in the ass
-- that until i fix that issue has to be fixed every fucking day
select *
from scpp.deals
where stock_number = '28873a'

select *
from scpp.xfm_deals
where stock_number = '28873a'

delete from scpp.xfm_deals where stock_number = '28873A' and seq = 3;
delete from scpp.deals where stock_number = '28873A' and seq = 2;

-- -- sc changed from yunker to carlson 2 days after deal capped
-- select *
-- from scpp.xfm_deals
-- where stock_number = '28940r'
-- 
-- select *
-- from scpp.deals
-- where stock_number = '28940r'
-- 
-- update scpp.deals
-- set employee_number = (
--   select employee_number
--   from scpp.sales_consultants
--   where ry1_id = 'cca')
-- where stock_number = '28940R';



-- already verified:
28703RA
28833
29062
29189B
29221
29351A
29500
29545xxb
29830A
30140
