﻿
-- now, with the same base query, the dough
-- this works for sales amounts, need to add cogs, gross, & f/i
-- ben says include accessories, but like f/i, this will be a separate data set
-- looks good
-- shit no need for separate queries
-- everything good for dough
drop table if exists dough;
create temp table dough as
select store, page, line, line_label, control, 
  round(sum(case when account_type_code in ('4','7') then amount else 0 end), 0) as sales,
  round(sum(case when account_type_code in ('5','9') then amount else 0 end), 0) as cogs,
  round(sum(amount), 0) as gross
-- select  -- new/used
--   sum(case when page = 16 then amount else 0 end) as used,
--   sum(case when page <> 16 then amount else 0 end) as new
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.doc, c.account, a.amount, d.account_type_code
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201701
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description, e.account_type_code
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201701
      and (
        (b.page between 5 and 15) -- new cars -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)
        or
        (b.page = 17 and b.line between 1 and 20))-- f/i       
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code in('4','7','5', '9') -- include Other Income type accounts to include accessories
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store in ('ry1', 'ry2' )) d on c.account = d.gl_account
  where a.post_status = 'Y'  ) h  
group by store, page, line, line_label, control
order by store, page, line, control

select * from dough

create temp table vehicles as
-- this is the list of vehicles for which there are transactions in the january statement
select a.control
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join ( -- d: fs gm_account page/line/acct description
  select f.store, d.gl_account, b.page, b.line, b.line_label, e.description, e.account_type_code
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201701
    and (
      (b.page between 5 and 13) -- new cars -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
      or
      (b.page = 16 and b.line between 1 and 14)
      or
      (b.page = 17 and b.line between 1 and 20))-- f/i       
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.account_type_code in('4','7','5', '9') -- include Other Income type accounts to include accessories
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
    and f.store in ('ry1', 'ry2' )) d on c.account = d.gl_account
where a.post_status = 'Y' 
group by a.control 

-- there will be some wierdness
-- eg 24068xx 884 ins chargeback (185101) deal from 201503
select a.*, b.*
from vehicles a
left join scpp.xfm_deals b on a.control = b.stock_number
inner join (
  select stock_number, max(seq) as seq
  from scpp.xfm_deals
  group by stock_number) c on b.stock_number = c.stock_number
    and b.seq = c.seq
order by a.control


find the consultant for the vehicle
this is going to be messy
charge back on a stocknumber sold 4 months ago, 2 months ago it unwound and has subsequently been
resold to someone else by a different consultant

accessories/aftermarket is confusing me too much
accessories: ry1: P5L47 Acct 1457001/1657001 
                  P14L46 Acct 144500/164500
             ry2: P7L47 Acct 2457001/2657001
                  P14L46 Acct 244500/244500
              



select aa.store, aa.page, aa.line, aa.line_label, aa.control, 
  -aa.sales as sales, -aa.cogs as cogs, -aa.gross as gross, b.primary_sc, secondary_sc
from dough aa
left join  vehicles a on aa.control = a.control
left join (
  select * 
  from scpp.xfm_deals t
  where seq = (
    select max(seq)
    from scpp.xfm_deals
    where stock_number = t.stock_number
      and run_date < '02/01/2017')) b on a.control = b.stock_number
where b.stock_number is not null      
order by aa.store, aa.page, aa.line, aa.line_label

select aa.store, aa.page, aa.line, aa.line_label, aa.control, 
  -aa.sales as sales, -aa.cogs as cogs, -aa.gross as gross, b.primary_sc, secondary_sc
  
select primary_sc,
  sum(case when page between 5 and 15 then -gross end) as nc_gross, 
  sum(case when page = 16 then -gross end) as uc_gross, 
  sum(case when page = 17 and line between 1 and 9 then -gross end) as nc_fi_gross, 
  sum(case when page = 17 and line between 11 and 19 then -gross end) as uc_fi_gross,
  sum(-gross) as total_gross
from dough aa
left join  vehicles a on aa.control = a.control
left join (
  select * 
  from scpp.xfm_deals t
  where seq = (
    select max(seq)
    from scpp.xfm_deals
    where stock_number = t.stock_number
      and run_date < '02/01/2017')) b on a.control = b.stock_number
where b.stock_number is not null      
group by primary_sc
order by primary_sc

-- consultant by vehicle
select primary_sc, aa.control,
  sum(case when page between 5 and 15 then -gross end) as nc_gross, 
  sum(case when page = 16 then -gross end) as uc_gross, 
  sum(case when page = 17 and line between 1 and 9 then -gross end) as nc_fi_gross, 
  sum(case when page = 17 and line between 11 and 19 then -gross end) as uc_fi_gross,
  sum(-gross) as total_gross
from dough aa
left join  vehicles a on aa.control = a.control
left join (
  select * 
  from scpp.xfm_deals t
  where seq = (
    select max(seq)
    from scpp.xfm_deals
    where stock_number = t.stock_number
      and run_date < '02/01/2017')) b on a.control = b.stock_number
where b.stock_number is not null      
group by primary_sc, aa.control
order by primary_sc, aa.control


-- the spreadsheet
select lastname, firstname, sum(new_count) as new_count, sum(used_count) as used_count, sum(new_count + used_count) as total_count,
  sum(nc_gross) as nc_gross, sum(uc_gross) as uc_gross, sum(nc_fi_gross) as nc_fi_gross,
  sum(uc_fi_gross) as uc_fi_gross, sum(total_gross) as total_gross
from (  
  select *
  from (
    select primary_sc,
      sum(case when page between 5 and 15 then -gross end) as nc_gross, 
      sum(case when page = 16 then -gross end) as uc_gross, 
      sum(case when page = 17 and line between 1 and 9 then -gross end) as nc_fi_gross, 
      sum(case when page = 17 and line between 11 and 19 then -gross end) as uc_fi_gross,
      sum(-gross) as total_gross
    from dough aa
    left join  vehicles a on aa.control = a.control
    left join (
      select * 
      from scpp.xfm_deals t
      where seq = (
        select max(seq)
        from scpp.xfm_deals
        where stock_number = t.stock_number
          and run_date < '02/01/2017')) b on a.control = b.stock_number
    where b.stock_number is not null      
    group by primary_sc) w
  left join scpp.unit_count x on w.primary_sc = x.sc_id
  left join (
    select salespersonid, lastname, firstname
    -- select *
    from ads.dim_salesperson 
    where salespersontypecode = 's' 
      and left(employeenumber, 1) = '1'
    group by salespersonid, lastname, firstname) y on w.primary_sc = y.salespersonid
  where y.salespersonid is not null) z  
group by lastname, firstname
order by lastname, firstname


-- uh oh
CAR;30094B;;-1216;;975;-241
CAR;30036XXA;;3796;;;3796


-- consultant by vehicle
select *  
from dough aa
where aa.control = '30486' 
order by aa.control, aa.page, aa.line


select d.page, d.line, d.line_label, d.gl_account, cc.journal,
  a.control, a.doc, a.amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal cc on a.journal_key = cc.journal_key
inner join ( -- d: fs gm_account page/line/acct description
  select d.gl_account, b.page, b.line, b.line_label--, e.description, e.account_type_code
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201701
    and (
      (b.page between 5 and 15) -- new cars -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
      or
      (b.page = 16 and b.line between 1 and 14)
      or
      (b.page = 17 and b.line between 1 and 20))   
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) d on c.account = d.gl_account    
where a.post_status = 'Y'
  and a.control = '30486' 
order by page, line, gl_account  









