﻿ -- back fill november config
 -- back fill november config
 -- oops, still need sc data table


-- pto_rate
-- full_months_employement
-- years_of_service

-- these queries are going to be for backfilling, real time will be a little different
-- hopefully, mostly just substituing open year_month for hard coded year_month
-- November
-- need the year_month of hire_date
/* 
!!!!!!!!!!!!

1/8/16 executive decision
generated  some s_c_config data for greg
oct, nov, dec, jan 
just: name, months emp, years serv
      put off pto rate for now
!!!!!!!!!!!!
*/

-- need placeholder for pay plans in previous months
delete from scpp.pay_plans where year_month in (201510, 201511, 201512);
--select * from scpp.pay_plans
insert into scpp.pay_plans (pay_plan_name, year_month)
values 
('Old', 201510),
('Old', 201511),
('Old', 201512);

with
  y_month as (
    SELect 201512),
  t_date as (
    select max(thedate) as term_date
    from dds.day
    where yearmonth = (select * from y_month)), 
  h_date AS (
    SELECT (term_date + interval '1 day')::date as hire_date FROM t_date)      
--select a.*, b.term_date, c.hire_date from y_month a, t_date b, h_date c
-- add $1 to guarantee for old pay plans to satisfy the check constraint
insert into scpp.sales_consultant_configuration (employee_number,
   year_month, pay_plan_name, full_months_employment,
   years_service, guarantee)
select a.employee_number, b.year_month, 'Old',
  case
    when b.seq - c.seq = 0 then 0
    else b.seq - c.seq - 1
  end as_full_months_employed,
	(b.last_of_month - a.hire_date)/365 as years_service,
	1
-- select *	
from scpp.sales_consultants a
left join scpp.months b on 1 = 1
  and b.year_month = (select * from y_month)
left join scpp.months c on a.hire_date between c.first_of_month and c.last_of_month
where term_date > (select term_date from t_date)
  and hire_date < (select hire_date from h_date);
  
-- and now, the open/current month
-- do all as standard, update chris garceau afterwards
-- delete from scpp.sales_consultant_configuration where year_month = 201601;
with
  y_month as (
    SELect 201601),
  t_date as (
    select max(thedate) as term_date
    from dds.day
    where yearmonth = (select * from y_month)), 
  h_date AS (
    SELECT (term_date + interval '1 day')::date as hire_date FROM t_date)  
insert into scpp.sales_consultant_configuration (employee_number,
   year_month, pay_plan_name, draw, full_months_employment,
   years_service, guarantee)
select a.employee_number, b.year_month, 'Standard', 500,
  case
    when b.seq - c.seq = 0 then 0
    else b.seq - c.seq - 1
  end as_full_months_employed,
	(b.last_of_month - a.hire_date)/365 as years_service,
  case
    when  b.seq - c.seq >= 4 then 2300
    else 2600
  end as guarantee
-- select *	
from scpp.sales_consultants a
left join scpp.months b on 1 = 1
  and b.year_month = (select * from y_month)
left join scpp.months c on a.hire_date between c.first_of_month and c.last_of_month
where term_date > (select term_date from t_date)
  and hire_date < (select hire_date from h_date);
 
update scpp.sales_consultant_configuration
set pay_plan_name = 'Finance',
    base_salary = 2000,
    guarantee = 0
where employee_number = (
  select employee_number
  from scpp.sales_consultants
  where first_name = 'chris'
    and last_name = 'garceau')
    and year_month = 201601;


/*
pto_rate
*/

select *
from dds.ext_pyhshdta
where employee_ = '128530'



select employee_, sum(total_gross_pay) as total_gross,
  to_date(
    case when length(payroll_ending_month::text) = 1 then '0' 
      || payroll_ending_month::text else payroll_ending_month::text end
    ||
    case when length(payroll_ending_day::text) = 1 then '0' 
      || payroll_ending_day::text else payroll_ending_day::text end
    ||
    case when length(payroll_ending_year::text) = 1 then '0' 
      || payroll_ending_year::text else payroll_ending_year::text end 
    , 'MMDDYY') as payroll_ending_date
from dds.ext_pyhshdta
-- where employee_ = '128530'
group by employee_, payroll_ending_year, payroll_ending_month, 
  payroll_ending_day
ORDER BY employee_, payroll_ending_year, payroll_ending_month, 
  payroll_ending_day  

-- so, here are the last 2 years of sc pay by month

select 100 * (payroll_ending_year + 2000) + payroll_ending_month as year__month,
  employee_, sum(total_gross_pay) as total_gross
from dds.ext_pyhshdta
group by 100 * (payroll_ending_year + 2000) + payroll_ending_month, employee_
order by employee_, 100 * (payroll_ending_year + 2000) + payroll_ending_month desc 

need to do some grouping to get rid of voids

select b.store_code, b.employee_number, b.full_name, b.hire_Date, b.term_date, 
  100 * (payroll_ending_year + 2000) + payroll_ending_month as year__month,
  payroll_ending_day, sum(total_gross_pay) as total_gross_pay,
  sum(case when payroll_ending_day < 20 then total_gross_pay end) as draw
from dds.ext_pyhshdta a
inner join scpp.sales_consultants b on a.employee_ = b.employee_number
group by b.store_code, b.employee_number, b.full_name, b.hire_Date, b.term_date, 
  100 * (payroll_ending_year + 2000) + payroll_ending_month,
  payroll_ending_day
order by b.full_name, 100 * (payroll_ending_year + 2000) + payroll_ending_month desc 

select b.store_code, b.employee_number, b.full_name, 
  100 * (payroll_ending_year + 2000) + payroll_ending_month as year__month,
  sum(total_gross_pay) as month_total_gross_pay,
  sum(case when payroll_ending_day < 20 then total_gross_pay end) as draw
from dds.ext_pyhshdta a
inner join scpp.sales_consultants b on a.employee_ = b.employee_number
group by b.store_code, b.employee_number, b.full_name, 
  100 * (payroll_ending_year + 2000) + payroll_ending_month
order by b.full_name, 100 * (payroll_ending_year + 2000) + payroll_ending_month desc   
  




/*
what i need now is how to determine for a sales consultant, what was his last
anniversary date
pto_rate applies for an entire year, calculated once at anniv date for the
entire upcoming year
based on previous 12 months pay
12 months pay/52/5 = per day

1/14/16: thinking in the config table, for each month, what is the period of time for
determining pto rate, except, yuk, i don't know, seems goofy


*/

-- this should be good, but adding the left join to scpp.months makes it run for fucking ever
-- added indices to scpp.months and this now works well enuf
select *
from (
  select a.store_code, a.full_name, a.hire_date, b.years_service, 
    extract(month from a.hire_Date),
    (select max(thedate)
      from dds.day
      where monthofyear = extract(month from a.hire_Date)
        and dayofmonth = extract(day from a.hire_date)
        and thedate < current_date) as most_recent_anniv, 
    c.*
  from scpp.sales_consultants a
  inner join scpp.sales_consultant_configuration b on a.employee_number= b.employee_number
  inner join scpp.months c on b.year_month = c.year_month
    and c.open_closed = 'open'
  where b.years_service > 0) d 
left join scpp.months e on d.most_recent_anniv between e.first_of_month and e.last_of_month  

-- believe this query will generate the pto_rate_period for any given MONTH
-- 
select d.*, e.year_month + 1 as most_rec_anniv_year_month, f.year_month
from (
  select a.store_code, a.employee_number, a.full_name, a.hire_date, b.years_service, b.full_months_employment,
    extract(month from a.hire_Date),
    (select max(thedate)
      from dds.day
      where monthofyear = extract(month from a.hire_Date)
        and dayofmonth = extract(day from a.hire_date)
        and thedate < current_date) as most_recent_anniv, 
    c.year_month as open_year_month
  from scpp.sales_consultants a
  inner join scpp.sales_consultant_configuration b on a.employee_number= b.employee_number
  inner join scpp.months c on b.year_month = c.year_month
    and c.open_closed = 'open') d
left join scpp.months e on d.most_recent_anniv between e.first_of_month and e.last_of_month  
left join scpp.months f on e.seq = f.seq + 12   
order by employee_number
order by d.years_service, d.hire_date desc

/*
1/25/16
i continue to struggle with the pto shit, when do i determine what the pto period and rate are
for a sales consultant and where/when do i store/calculate it

so i guess i am leaning towards when the sales_consultant_configuration record is created
when is that done
presumably when the gsm closes/submits payroll for a consultant

so, figure it for dec 2015, jan 2016

sam foster
201512
hiredate 11/17/14
becomes eligible for pto when full_months_emp >= 12
for the period 11/17/15 thru 11/30/15 technically due pto, but not granted until 12/1
because don't have 12 full months of earnings from which to generate an avg

*/

select a.store_code, a.employee_number, a.last_name, a.first_name,
  a.hire_date, b.year_month, 
  case
    when b.seq - c.seq = 0 then 0
    else b.seq - c.seq - 1
  end as full_months_employed,
	(b.last_of_month - a.hire_date)/365 as years_service,
  (select max(thedate)
    from dds.day
    where monthofyear = extract(month from a.hire_Date)
      and dayofmonth = extract(day from a.hire_date)
      and thedate < current_date) as most_recent_anniv  
-- select *	
from scpp.sales_consultants a
left join scpp.months b on 1 = 1
  and b.year_month = 201512
left join scpp.months c on a.hire_date between c.first_of_month and c.last_of_month
where term_date > '12/31/2015'
  and hire_date < '01/01/2016';

/*
-- i believe this actually gives me the the pto period for any given year_month
select e.*, g.year_month as pto_period_from_year_month
from (
  select d.*,
    (select distinct yearmonth from dds.day where thedate = d.most_recent_anniv) as pto_period_thru_year_month
  from (
    select a.store_code, a.employee_number, a.last_name, a.first_name,
      a.hire_date, b.year_month, 
      case
        when b.seq - c.seq = 0 then 0
        else b.seq - c.seq - 1
      end as full_months_employed,
      (b.last_of_month - a.hire_date)/365 as years_service,
      (select max(thedate)
        from dds.day
        where monthofyear = extract(month from a.hire_Date)
          and dayofmonth = extract(day from a.hire_date)
          and thedate < current_date) as most_recent_anniv  
    -- select *	
    from scpp.sales_consultants a
    left join scpp.months b on 1 = 1
      and b.year_month = 201512
    left join scpp.months c on a.hire_date between c.first_of_month and c.last_of_month
    where term_date > '12/31/2015'
      and hire_date < '01/01/2016') d 
  where full_months_employed >= 12) e
left join scpp.months f on e.pto_period_thru_year_month = f.year_month
left join scpp.months g on f.seq = g.seq + 11
order by full_months_employed
*/
drop table if exists scpp.pto_intervals;
create table if not exists scpp.pto_intervals (
  employee_number citext not null references scpp.sales_consultants,
  year_month integer not null references scpp.months,
  from_year_month integer,
  thru_year_month integer,
constraint pto_intervals_pk primary key (employee_number, year_month));  

-- need to add a unique index on sales_consultant_configuration (year_month, employee_number)
alter table scpp.sales_Consultant_Data
  add constraint sales_consultant_data_fkey 
  foreign key(year_month, employee_number)
  references scpp.sales_consultant_configuration(year_month, employee_number);
-- 3/4 change foreign key
alter table scpp.pto_intervals
drop constraint pto_intervals_year_month_fkey;
  alter table scpp.pto_intervals
  add constraint pto_intervals_fkey
  foreign key(year_month, employee_number)
  references scpp.sales_consultant_configuration(year_month, employee_number);


with 
  y_month as (
    SELect 201601),
  t_date as (
    select max(thedate) as term_date
    from dds.day
    where yearmonth = (select * from y_month)), 
  h_date AS (
    SELECT (term_date + interval '1 day')::date as hire_date FROM t_date)    
insert into scpp.pto_intervals (employee_number, year_month, from_year_month, thru_year_month)
select d.employee_number, d.year_month,
  (select year_month 
    from scpp.months
    WHERE seq = (
      select seq - 12
      from scpp.months
      where d.most_recent_anniv between first_of_month and last_of_month)) as pto_period_from,
  (select year_month 
    from scpp.months
    WHERE seq = (
      select seq -1
      from scpp.months
      where d.most_recent_anniv between first_of_month and last_of_month)) as pto_period_thru    
from (
  select a.store_code, a.employee_number, a.last_name, a.first_name,
    a.hire_date, b.year_month, 
    case
      when b.seq - c.seq = 0 then 0
      else b.seq - c.seq - 1
    end as full_months_employed,
    (b.last_of_month - a.hire_date)/365 as years_service,
    (select max(thedate)
      from dds.day
      where monthofyear = extract(month from a.hire_Date)
        and dayofmonth = extract(day from a.hire_date)
        and thedate < (select first_of_month from scpp.months where year_month = (select * from y_month))) as most_recent_anniv  
  -- select *	
  from scpp.sales_consultants a    
  left join scpp.months b on 1 = 1
    and b.year_month = (select * from y_month)
  left join scpp.months c on a.hire_date between c.first_of_month and c.last_of_month
  where term_date > (select term_date from t_date)
    and hire_date < (select hire_date from h_date)) d
where full_months_employed >= 12;   
-- --better yet:
-- with 
--   y_month as (
--     SELect 201601),
--   t_date as (
--     select max(thedate) as term_date
--     from dds.day
--     where yearmonth = (select * from y_month)), 
--   h_date AS (
--     SELECT (term_date + interval '1 day')::date as hire_date FROM t_date)      
-- --select a.*, b.term_date, c.hire_date from y_month a, t_date b, h_date c
-- 
-- --select e.*, g.year_month as pto_period_from_year_month
-- --insert into scpp.pto_intervals (employee_number, year_month, from_year_month, thru_year_month)
-- select e.employee_number, e.year_month, g.year_month as pto_period_from_year_month, pto_period_thru_year_month
-- from (
--   select d.*,
--     (select distinct yearmonth from dds.day where thedate = d.most_recent_anniv) as pto_period_thru_year_month
--   from (
--     select a.store_code, a.employee_number, a.last_name, a.first_name,
--       a.hire_date, b.year_month, 
--       case
--         when b.seq - c.seq = 0 then 0
--         else b.seq - c.seq - 1
--       end as full_months_employed,
--       (b.last_of_month - a.hire_date)/365 as years_service,
--       (select max(thedate)
--         from dds.day
--         where monthofyear = extract(month from a.hire_Date)
--           and dayofmonth = extract(day from a.hire_date)
--           and thedate < current_date) as most_recent_anniv  
--     -- select *	
--     from scpp.sales_consultants a
--     left join scpp.months b on 1 = 1
--       and b.year_month = (select * from y_month)
--     left join scpp.months c on a.hire_date between c.first_of_month and c.last_of_month
--     where term_date > (select term_date from t_date)
--       and hire_date < (select hire_date from h_date)) d 
--   where full_months_employed >= 12) e
-- left join scpp.months f on e.pto_period_thru_year_month = f.year_month
-- left join scpp.months g on f.seq = g.seq + 11;



select a.*, b.month_total_gross_pay, b.year_month as check_month
from scpp.pto_intervals a
left join (
  select b.store_code, b.employee_number, b.full_name, 
    100 * (payroll_ending_year + 2000) + payroll_ending_month as year_month,
    sum(total_gross_pay) as month_total_gross_pay,
    sum(case when payroll_ending_day < 20 then total_gross_pay end) as draw
  from dds.ext_pyhshdta a
  inner join scpp.sales_consultants b on a.employee_ = b.employee_number
  group by b.store_code, b.employee_number, b.full_name, 
    100 * (payroll_ending_year + 2000) + payroll_ending_month) b on a.employee_number = b.employee_number
      and b.year_month between a.from_year_month and a.thru_year_month
where a.year_month = 201511      
order by a.employee_number, b.year_month

this should return 12 for each consultant
so, what i will do to accomodate these
pto_rate = sum(gross)/(4.33*12)/5
1130690: larry stadstad (8) previous 4 months as honda employee 2130690
222300: bill bushaw (9) he was not paid in jan, mar, feb 2015
1135518: jeff tarr
150040: chris garceau ??
1124625: Bryn Seay (11) even tho he started in aug, no check until sep??


select employee_number, count(*) from (
select a.*, b.month_total_gross_pay
from scpp.pto_intervals a
left join (
  select b.store_code, b.employee_number, b.full_name, 
    100 * (payroll_ending_year + 2000) + payroll_ending_month as year_month,
    sum(total_gross_pay) as month_total_gross_pay,
    sum(case when payroll_ending_day < 20 then total_gross_pay end) as draw
  from dds.ext_pyhshdta a
  inner join scpp.sales_consultants b on a.employee_ = b.employee_number
  group by b.store_code, b.employee_number, b.full_name, 
    100 * (payroll_ending_year + 2000) + payroll_ending_month) b on a.employee_number = b.employee_number
      and b.year_month between a.from_year_month and a.thru_year_month
where a.year_month = 201601 ) x group by employee_number having count(*) <> 12
--- order by a.employee_number, b.year_month

for any given month, determine the relevant pto period for calculating rate
store in scpp.pto_intervals
For that month, use the interval in scpp.pto_intervals to determine total gross wages (dds.ext_pyhshdta)
(total gross wages)/(4.3333 * months paid)/5/8 = pto hourly rate

update scpp.sales_consultant_configuration
set pto_rate = z.pto_rate
from (
  select year_month, employee_number, count(*), sum(month_total_gross_pay),
    round(sum(month_total_gross_pay)/(4.3333 * count(*))/5/8, 2) as pto_rate
  from (
    select a.*, b.month_total_gross_pay
    from scpp.pto_intervals a
    left join (
      select b.store_code, b.employee_number, b.full_name, 
        100 * (payroll_ending_year + 2000) + payroll_ending_month as year_month,
        sum(total_gross_pay) as month_total_gross_pay,
        sum(case when payroll_ending_day < 20 then total_gross_pay end) as draw
      from dds.ext_pyhshdta a
      inner join scpp.sales_consultants b on a.employee_ = b.employee_number
      group by b.store_code, b.employee_number, b.full_name, 
        100 * (payroll_ending_year + 2000) + payroll_ending_month) b on a.employee_number = b.employee_number
          and b.year_month between a.from_year_month and a.thru_year_month) x group by year_month, employee_number) z
where scpp.sales_consultant_configuration.employee_number = z.employee_number
  and scpp.sales_consultant_configuration.year_month = z.year_month;


      

/*
-- real time updates:
1. open month

 */
select *
from scpp.pay_plans




-- 2/10/16 deals
select year_month, b.employee_number, stock_number,
  case
    when secondary_sc = '' then 1
    else .5
  end as unit_count,
  deal_date, customer_name, model_year, make, model, 'normal'
from scpp.tmp_deals a
inner join scpp.tmp_sales_consultants b on a.store_code = b.store_code
  and a.primary_sc = b.sales_consultant_id
-- left join scpp.tmp_sales_consultants c on a.store_code = c.store_code
--   and a.secondary_sc = c.sales_consultant_id;
union
select year_month, c.employee_number, stock_number,
  .5 AS unit_count,
  deal_date, customer_name, model_year, make, model, 'normal'
from scpp.tmp_deals a
-- left join scpp.tmp_sales_consultants b on a.store_code = b.store_code
--   and a.primary_sc = b.sales_consultant_id
inner join scpp.tmp_sales_consultants c on a.store_code = c.store_code
  and a.secondary_sc = c.sales_consultant_id
where secondary_sc <> '';

drop table if exists scpp.jan_sales_sheets;
create table if not exists scpp.jan_sales_sheets(
  sales_consultant citext,
  stock_number citext);
insert into scpp.jan_sales_sheets values('steve','22545XXZ');
insert into scpp.jan_sales_sheets values('dale','24746RB');
insert into scpp.jan_sales_sheets values('tanner','24765RA');
insert into scpp.jan_sales_sheets values('nate','24920ayc');
insert into scpp.jan_sales_sheets values('steve','24949B');
insert into scpp.jan_sales_sheets values('craig','24949c');
insert into scpp.jan_sales_sheets values('logain','25010ra');
insert into scpp.jan_sales_sheets values('BRYN','25067XXRA');
insert into scpp.jan_sales_sheets values('chris','25075XX');
insert into scpp.jan_sales_sheets values('dale','25152A');
insert into scpp.jan_sales_sheets values('dale','25152B');
insert into scpp.jan_sales_sheets values('craig','25208d');
insert into scpp.jan_sales_sheets values('steve','25276B');
insert into scpp.jan_sales_sheets values('craig','25359ab');
insert into scpp.jan_sales_sheets values('scott','25607a');
insert into scpp.jan_sales_sheets values('jim','25636b');
insert into scpp.jan_sales_sheets values('sam','25667A');
insert into scpp.jan_sales_sheets values('arden','25703b');
insert into scpp.jan_sales_sheets values('tanner','25706bb');
insert into scpp.jan_sales_sheets values('eric','25723');
insert into scpp.jan_sales_sheets values('jesse','25806A');
insert into scpp.jan_sales_sheets values('steve','25806B');
insert into scpp.jan_sales_sheets values('eric','25855a');
insert into scpp.jan_sales_sheets values('dale','25857B');
insert into scpp.jan_sales_sheets values('brett','25867b');
insert into scpp.jan_sales_sheets values('eric','25896');
insert into scpp.jan_sales_sheets values('steve','25901C');
insert into scpp.jan_sales_sheets values('jared','25928b');
insert into scpp.jan_sales_sheets values('larry','25967r');
insert into scpp.jan_sales_sheets values('jim','25991');
insert into scpp.jan_sales_sheets values('nate','26006a');
insert into scpp.jan_sales_sheets values('jared','26049a');
insert into scpp.jan_sales_sheets values('tanner','26052a');
insert into scpp.jan_sales_sheets values('joe','26074');
insert into scpp.jan_sales_sheets values('logain','26074A');
insert into scpp.jan_sales_sheets values('eric','26074b');
insert into scpp.jan_sales_sheets values('tanner','26084C');
insert into scpp.jan_sales_sheets values('nate','26117b');
insert into scpp.jan_sales_sheets values('brad','26132');
insert into scpp.jan_sales_sheets values('larry','26210b');
insert into scpp.jan_sales_sheets values('jesse','26223B');
insert into scpp.jan_sales_sheets values('steve','26225A');
insert into scpp.jan_sales_sheets values('chris','26228XXA');
insert into scpp.jan_sales_sheets values('brad','26228xxb');
insert into scpp.jan_sales_sheets values('steve','26229XX');
insert into scpp.jan_sales_sheets values('chris','26229XXA');
insert into scpp.jan_sales_sheets values('aj','26242b');
insert into scpp.jan_sales_sheets values('scott','26251');
insert into scpp.jan_sales_sheets values('jim','26271r');
insert into scpp.jan_sales_sheets values('joe','26298xxa');
insert into scpp.jan_sales_sheets values('jesse','26302A');
insert into scpp.jan_sales_sheets values('chris','26318A');
insert into scpp.jan_sales_sheets values('craig','26330a');
insert into scpp.jan_sales_sheets values('eric','26330b');
insert into scpp.jan_sales_sheets values('logain','26336aa');
insert into scpp.jan_sales_sheets values('jim','26345');
insert into scpp.jan_sales_sheets values('sam','26346A');
insert into scpp.jan_sales_sheets values('jared','26347c');
insert into scpp.jan_sales_sheets values('BRYN','26351');
insert into scpp.jan_sales_sheets values('jeff p','26355r');
insert into scpp.jan_sales_sheets values('nate','26358pa');
insert into scpp.jan_sales_sheets values('tanner','26371xx');
insert into scpp.jan_sales_sheets values('craig','26385b');
insert into scpp.jan_sales_sheets values('jeff p','26391a');
insert into scpp.jan_sales_sheets values('chris','26401A');
insert into scpp.jan_sales_sheets values('joe','26406a');
insert into scpp.jan_sales_sheets values('jared','26408a');
insert into scpp.jan_sales_sheets values('scott','26408b');
insert into scpp.jan_sales_sheets values('eric','26441');
insert into scpp.jan_sales_sheets values('chris','26457');
insert into scpp.jan_sales_sheets values('BRYN','26462A');
insert into scpp.jan_sales_sheets values('aj','26462b');
insert into scpp.jan_sales_sheets values('jeff p','26469b');
insert into scpp.jan_sales_sheets values('aj','26496a');
insert into scpp.jan_sales_sheets values('dale','26496B');
insert into scpp.jan_sales_sheets values('brett','26500');
insert into scpp.jan_sales_sheets values('jeff p','26523');
insert into scpp.jan_sales_sheets values('sam','26531A');
insert into scpp.jan_sales_sheets values('jared','26565a');
insert into scpp.jan_sales_sheets values('jim','26582xx');
insert into scpp.jan_sales_sheets values('joe','26620');
insert into scpp.jan_sales_sheets values('scott','26631a');
insert into scpp.jan_sales_sheets values('tanner','26637a');
insert into scpp.jan_sales_sheets values('chris','26652');
insert into scpp.jan_sales_sheets values('steve','26666A');
insert into scpp.jan_sales_sheets values('chris','26683A');
insert into scpp.jan_sales_sheets values('chris','26686A');
insert into scpp.jan_sales_sheets values('steve','26686A');
insert into scpp.jan_sales_sheets values('craig','26688a');
insert into scpp.jan_sales_sheets values('chris','26690');
insert into scpp.jan_sales_sheets values('craig','26699b');
insert into scpp.jan_sales_sheets values('craig','26717a');
insert into scpp.jan_sales_sheets values('tanner','26718a');
insert into scpp.jan_sales_sheets values('jared','26731b');
insert into scpp.jan_sales_sheets values('craig','26734xx');
insert into scpp.jan_sales_sheets values('aj','26740xxa');
insert into scpp.jan_sales_sheets values('aj','26740xxb');
insert into scpp.jan_sales_sheets values('aj','26772a');
insert into scpp.jan_sales_sheets values('jared','26773b');
insert into scpp.jan_sales_sheets values('sam','26785A');
insert into scpp.jan_sales_sheets values('chris','26799A');
insert into scpp.jan_sales_sheets values('chris','26802XX');
insert into scpp.jan_sales_sheets values('scott','26836');
insert into scpp.jan_sales_sheets values('tanner','26852a');
insert into scpp.jan_sales_sheets values('jared','26864xxa');
insert into scpp.jan_sales_sheets values('tanner','26879p');
insert into scpp.jan_sales_sheets values('steve','26881A');
insert into scpp.jan_sales_sheets values('BRYN','26888A');
insert into scpp.jan_sales_sheets values('BRYN','26893');
insert into scpp.jan_sales_sheets values('brett','26894');
insert into scpp.jan_sales_sheets values('nate','26896b');
insert into scpp.jan_sales_sheets values('aj','26896c');
insert into scpp.jan_sales_sheets values('jim','26899a');
insert into scpp.jan_sales_sheets values('jim','26902b');
insert into scpp.jan_sales_sheets values('jared','26903a');
insert into scpp.jan_sales_sheets values('chris','26903AA');
insert into scpp.jan_sales_sheets values('tanner','26906a');
insert into scpp.jan_sales_sheets values('brad','26918a');
insert into scpp.jan_sales_sheets values('chris','26920B');
insert into scpp.jan_sales_sheets values('dale','26926A');
insert into scpp.jan_sales_sheets values('nate','26928xxa');
insert into scpp.jan_sales_sheets values('tanner','26932');
insert into scpp.jan_sales_sheets values('scott','26934');
insert into scpp.jan_sales_sheets values('logain','26935A');
insert into scpp.jan_sales_sheets values('aj','26938a');
insert into scpp.jan_sales_sheets values('aj','26942xx');
insert into scpp.jan_sales_sheets values('dale','26955xxa');
insert into scpp.jan_sales_sheets values('logain','26956xxa');
insert into scpp.jan_sales_sheets values('tanner','26956xxb');
insert into scpp.jan_sales_sheets values('craig','26959xx');
insert into scpp.jan_sales_sheets values('jim','26960xx');
insert into scpp.jan_sales_sheets values('jesse','26964xxb');
insert into scpp.jan_sales_sheets values('BRYN','26966A');
insert into scpp.jan_sales_sheets values('BRYN','26971A');
insert into scpp.jan_sales_sheets values('jared','26975p');
insert into scpp.jan_sales_sheets values('aj','26980a');
insert into scpp.jan_sales_sheets values('jared','26982');
insert into scpp.jan_sales_sheets values('sam','26985B');
insert into scpp.jan_sales_sheets values('jared','26993a');
insert into scpp.jan_sales_sheets values('chris','26997');
insert into scpp.jan_sales_sheets values('jesse','27001');
insert into scpp.jan_sales_sheets values('jared','27026xxa');
insert into scpp.jan_sales_sheets values('steve','27035');
insert into scpp.jan_sales_sheets values('tanner','27040a');
insert into scpp.jan_sales_sheets values('brad','27041a');
insert into scpp.jan_sales_sheets values('nate','27046xx');
insert into scpp.jan_sales_sheets values('craig','27049xx');
insert into scpp.jan_sales_sheets values('jesse','27064xx');
insert into scpp.jan_sales_sheets values('craig','27067xx');
insert into scpp.jan_sales_sheets values('BRYN','27067XXA');
insert into scpp.jan_sales_sheets values('sam','27069XX');
insert into scpp.jan_sales_sheets values('nate','27074p');
insert into scpp.jan_sales_sheets values('chris','27082');
insert into scpp.jan_sales_sheets values('BRYN','27085');
insert into scpp.jan_sales_sheets values('larry','27101');
insert into scpp.jan_sales_sheets values('craig','27105a');
insert into scpp.jan_sales_sheets values('brad','27110a');
insert into scpp.jan_sales_sheets values('craig','27116xxa');
insert into scpp.jan_sales_sheets values('scott','27119');
insert into scpp.jan_sales_sheets values('jared','27125');
insert into scpp.jan_sales_sheets values('arden','27130a');
insert into scpp.jan_sales_sheets values('jared','27137a');
insert into scpp.jan_sales_sheets values('brett','27155');
insert into scpp.jan_sales_sheets values('steve','27157B');
insert into scpp.jan_sales_sheets values('arden','27180b');
insert into scpp.jan_sales_sheets values('joe','27188');
insert into scpp.jan_sales_sheets values('eric','27188x');
insert into scpp.jan_sales_sheets values('aj','27188xa');
insert into scpp.jan_sales_sheets values('nate','27198xx');
insert into scpp.jan_sales_sheets values('tanner','27207p');
insert into scpp.jan_sales_sheets values('larry','27216xa');
insert into scpp.jan_sales_sheets values('nate','27224');
insert into scpp.jan_sales_sheets values('eric','27232');
insert into scpp.jan_sales_sheets values('dale','27236');
insert into scpp.jan_sales_sheets values('jeff p','27243');
insert into scpp.jan_sales_sheets values('brett','27244');
insert into scpp.jan_sales_sheets values('BRYN','27246A');
insert into scpp.jan_sales_sheets values('craig','27248a');
insert into scpp.jan_sales_sheets values('larry','27255a');
insert into scpp.jan_sales_sheets values('sam','27260A');
insert into scpp.jan_sales_sheets values('tanner','27260b');
insert into scpp.jan_sales_sheets values('sam','27261');
insert into scpp.jan_sales_sheets values('sam','27281');
insert into scpp.jan_sales_sheets values('jared','27291pa');
insert into scpp.jan_sales_sheets values('logain','27304');
insert into scpp.jan_sales_sheets values('jeff p','27304a');
insert into scpp.jan_sales_sheets values('aj','27308xx');
insert into scpp.jan_sales_sheets values('sam','27321XX');
insert into scpp.jan_sales_sheets values('craig','27326xx');
insert into scpp.jan_sales_sheets values('chris','27336P');
insert into scpp.jan_sales_sheets values('nate','27340a');
insert into scpp.jan_sales_sheets values('joe','27341');
insert into scpp.jan_sales_sheets values('jeff p','27342');
insert into scpp.jan_sales_sheets values('jim','27342xx');
insert into scpp.jan_sales_sheets values('jim','27347');
insert into scpp.jan_sales_sheets values('joe','27350');
insert into scpp.jan_sales_sheets values('jared','27357');
insert into scpp.jan_sales_sheets values('scott','27361');
insert into scpp.jan_sales_sheets values('larry','27362');
insert into scpp.jan_sales_sheets values('craig','27367');
insert into scpp.jan_sales_sheets values('jeff p','27373');
insert into scpp.jan_sales_sheets values('tanner','27378');
insert into scpp.jan_sales_sheets values('nate','27385p');
insert into scpp.jan_sales_sheets values('craig','27387p');
insert into scpp.jan_sales_sheets values('eric','27389');
insert into scpp.jan_sales_sheets values('eric','27391a');
insert into scpp.jan_sales_sheets values('craig','27409');
insert into scpp.jan_sales_sheets values('jesse','27420');
insert into scpp.jan_sales_sheets values('tanner','27425x');
insert into scpp.jan_sales_sheets values('craig','27428p');
insert into scpp.jan_sales_sheets values('jesse','27429X');
insert into scpp.jan_sales_sheets values('arden','27430x');
insert into scpp.jan_sales_sheets values('aj','27450x');
insert into scpp.jan_sales_sheets values('BRYN','27453P');
insert into scpp.jan_sales_sheets values('scott','27455');
insert into scpp.jan_sales_sheets values('craig','27459x');
insert into scpp.jan_sales_sheets values('larry','27460');
insert into scpp.jan_sales_sheets values('dale','27460A');
insert into scpp.jan_sales_sheets values('jim','27461xx');
insert into scpp.jan_sales_sheets values('jeff p','27465xx');
insert into scpp.jan_sales_sheets values('craig','27465xxa');
insert into scpp.jan_sales_sheets values('craig','27502x');
insert into scpp.jan_sales_sheets values('scott','27504');
insert into scpp.jan_sales_sheets values('brett','27525');
insert into scpp.jan_sales_sheets values('chris','27560X');
insert into scpp.jan_sales_sheets values('sam','27562A');
insert into scpp.jan_sales_sheets values('brad','27596x');
insert into scpp.jan_sales_sheets values('larry','27597');
insert into scpp.jan_sales_sheets values('joe','27599');
insert into scpp.jan_sales_sheets values('craig','27616');
insert into scpp.jan_sales_sheets values('brett','27617');
insert into scpp.jan_sales_sheets values('larry','h8448');
insert into scpp.jan_sales_sheets values('larry','h8490');
insert into scpp.jan_sales_sheets values('larry','h8540');
insert into scpp.jan_sales_sheets values('larry','h8711');


select sales_consultant, count(*)
select * 
from scpp.jan_sales_sheets
group by sales_consultant
order by sales_consultant

select *
from (
  select bopmast_stock_number, primary_salespers, secondary_slspers2
  -- select count(*)
  from foreign_table a
  where date_capped between '2016-01-01' and '2016-01-31'
    and sale_type <> 'w'
    and bopmast_company_number = 'RY1') r
full outer join scpp.jan_sales_sheets s on r.bopmast_stock_number = s.stock_number
where r.bopmast_stock_number is null or s.stock_number is null
  


select bopmast_company_number, primary_salespers, count(*)
-- select count(*)
from foreign_table a
where date_capped between '2016-01-01' and '2016-01-31'
  and sale_type <> 'w'
group by bopmast_company_number, primary_salespers
order by bopmast_company_number, primary_salespers


select x.*, sum(unit_count) over(partition by consultant order by consultant) as total
from (
  select bopmast_company_number, bopmast_stock_number, primary_salespers as consultant,
    date_capped, 
  CASE 
    when secondary_slspers2 is null then 1.0 
    else 0.5
  end as unit_count
  from foreign_table a
  where date_capped between '2016-01-01' and '2016-01-31'
    and sale_type <> 'w'
  union
  select bopmast_company_number, bopmast_stock_number, secondary_slspers2 as consultant,
    date_capped, 0.5 as unit_count
  from foreign_table a
  where date_capped between '2016-01-01' and '2016-01-31'
    and sale_type <> 'w'
    and secondary_slspers2 is not null) x  
order by bopmast_company_number, consultant, date_capped    



select *
from (
  select x.*, sum(unit_count) over(partition by consultant order by consultant) as total
  from (
    select bopmast_company_number, bopmast_stock_number, primary_salespers as consultant,
      date_capped, date_approved,
    CASE 
      when secondary_slspers2 is null then 1.0 
      else 0.5
    end as unit_count
    from foreign_table a
    where date_capped between '2016-01-01' and '2016-01-31'
      and sale_type <> 'w'
      and bopmast_company_number = 'RY1'
    union
    select bopmast_company_number, bopmast_stock_number, secondary_slspers2 as consultant,
      date_capped, date_approved, 0.5 as unit_count
    from foreign_table a
    where date_capped between '2016-01-01' and '2016-01-31'
      and sale_type <> 'w'
      and secondary_slspers2 is not null
      and bopmast_company_number = 'RY1') x) r
full outer join scpp.jan_sales_sheets s on r.bopmast_stock_number = s.stock_number
where (r.bopmast_stock_number is null or s.stock_number is null)
  and (sales_consultant = 'craig' or consultant = 'CRO')


select sales_consultant, count(*)
from scpp.jan_Sales_sheets
group by sales_Consultant




select * from foreign_table limit 100