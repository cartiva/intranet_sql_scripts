﻿-- admin page persist edit change state
earned      1700  (unit_count * per_unit)
guarantee   2300
pto          800
total pay   2500

-- additional stipulations
if pay is based on guarantee and the emp has valid pto
  total_pay = ((actual days worked/working days) * guarantee) + pto_hours * pto_rate

New Hire: if hired on 10th pay = 2/3 guarantee
                      20th pay = 1/3 guarantee

                      
select * from scpp.get_consultant_profile(162);

select * from scpp.tmp_s_c_data

unpaid time off only comes into play when pay is based on guarantee

pay based on guarantee:
  deduct unpaid time off
  pto pay removed from guarantee

select *
from scpp.months a
left join dds.day b on a.year_month = b.yearmonth


select r.*,
  case
    when earned > guarantee then
      earned + pto_pay + additional_comp
    else
      case
        when earned + pto_pay > guarantee then
          earned + pto_pay + additional_comp
        else 
          guarantee - (unpaid_time_off * guar_per_hour) + additional_comp
        end
    end as total_pay
    
from (
  select a.full_name, a.unit_count_x_per_unit as earned, a.guarantee, 
    a.pto_hours, a.pto_rate, a.pto_pay, a.additional_comp, a.unpaid_time_off,
    b.sc_working_days, round(a.guarantee/b.sc_working_days/8.0, 2) as guar_per_hour
  from scpp.sales_consultant_data a
  inner join scpp.months b on a.year_month = b.year_month
    and b.open_closed = 'open') r

    

    